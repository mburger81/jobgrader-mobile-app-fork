# [1.0.0-154](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-135...1.0.0-154) (2023-08-07)


### Bug Fixes

* **api:** get the right basictoken from storage ([fe22310](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fe22310cc657b0770214e98b1143d4cf0d618196))
* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **authentication:** bug fix for correct username and incorrect password ([8eb0493](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8eb0493a51b6442e19cbcabd3d96bc9a41dc3a09))
* **barcode:** open settings app if the user has not consented to camera usage ([6f779be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6f779be00544493664cee6ff074ad0532f50d734))
* **browse-mode:** better UX when the user is in browse mode, correct DP and alert windows are displayed ([380c733](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/380c73314463d1155bef00d99f55d903de7ac2b1))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **camera:** add pwa-elements to support camera on browser ([0cb3ec9](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0cb3ec90ae9780314aeb7d12e7cdc3718f8ffc40))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **inappbrowser:** post KYC processes and landing page ([21b9387](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/21b93876dce1c83295301efe2bf427cc8066b461))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **key:** do not stop the lockservice on kyc process ([fb9eb92](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fb9eb92a25cf4d048e3caabfec0477d3d14c2510))
* **kyc:** forward to veriff-thanks page also on pwa app ([eee67d6](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/eee67d6680bc7a151cab878c907b55c590603563))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **kyc:** post inappbrowser close event on low end android devices ([a3a3de6](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a3a3de67ac614c97e1a75e338c3a912971996e8a))
* **kyc:** remove navigation elements from DID_RETRIEVE state handling ([555dec6](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/555dec64ba8ddfadf9ace0616a1f6591462da91c))
* **kyc:** wait all new kyc data are retrieved before we emit dataProcessedAfterKyc event ([52081df](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/52081df25f4e0ab876a4344ea165928ca1b31c5e))
* **lock-screen, login:** hide CTA description when spinner is loaded ([4d7aabd](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4d7aabd4bfac11a12dd4305867b8dfec2b695ba2))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **storage:** wait until all storages are ready ([ff5b447](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ff5b447335ec1b0703b23bcdd6901888cf7c5060))
* **store:** clear entire store ([cdd1f9c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cdd1f9c96e1cc0b189041f4094eb9b409d7eadd0))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **tabs:** update active tabs depending on router ([d345586](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d345586dd27e552fce12c28d38cf7f46a53adc18))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **user-display-picture:** issue with changing uploaded DP in the home page ([f3dde30](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f3dde30715dd28f06e46c0cc7965984ed5cd8287))
* **vc:** dont't store dupplicated vc's ([fca3460](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fca34607005d91c671ffd6738ddc3f5d3cac6a03))
* **walkthrough:** last slide drag event is handled ([9303fd0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9303fd08c9b73cf71d66d689199591e0b09a860e))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))
* **wallet-list:** minor bug fix ([cd686f5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cd686f599da4f8ea1d05d1e1df925359133b1cea))


### Features

* **kyc:** use @capacitor/browser for android ([0c21aff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0c21affea3324ddafbaa5a7490c3761188805c54))
* **storage:** add kyc media ([f4884bb](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f4884bb0adbd86a7cdba981dc213c69282b7904d))
* **storage:** add user photo ([89b0c95](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/89b0c95965363774474cbe8f2e239bfe5a4d0a58))
* **storage:** add vault secrets ([08cadbf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/08cadbf9f4998ca8e885281ebda73b9da5305a56))
* **storage:** use own branch per storage & wait angular for localForage to be ready ([879334d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/879334d5eb82cf9619680007f901afce05ddd1de))
* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-135](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-102...1.0.0-135) (2023-08-04)


### Bug Fixes

* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **authentication:** bug fix for correct username and incorrect password ([8eb0493](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8eb0493a51b6442e19cbcabd3d96bc9a41dc3a09))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **camera:** add pwa-elements to support camera on browser ([0cb3ec9](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0cb3ec90ae9780314aeb7d12e7cdc3718f8ffc40))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **inappbrowser:** post KYC processes and landing page ([21b9387](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/21b93876dce1c83295301efe2bf427cc8066b461))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **key:** do not stop the lockservice on kyc process ([fb9eb92](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fb9eb92a25cf4d048e3caabfec0477d3d14c2510))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **user-display-picture:** issue with changing uploaded DP in the home page ([f3dde30](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f3dde30715dd28f06e46c0cc7965984ed5cd8287))
* **walkthrough:** last slide drag event is handled ([9303fd0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9303fd08c9b73cf71d66d689199591e0b09a860e))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))
* **wallet-list:** minor bug fix ([cd686f5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cd686f599da4f8ea1d05d1e1df925359133b1cea))


### Features

* **kyc:** use @capacitor/browser for android ([0c21aff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0c21affea3324ddafbaa5a7490c3761188805c54))
* **storage:** add kyc media ([f4884bb](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f4884bb0adbd86a7cdba981dc213c69282b7904d))
* **storage:** add user photo ([89b0c95](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/89b0c95965363774474cbe8f2e239bfe5a4d0a58))
* **storage:** add vault secrets ([08cadbf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/08cadbf9f4998ca8e885281ebda73b9da5305a56))
* **storage:** use own branch per storage & wait angular for localForage to be ready ([879334d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/879334d5eb82cf9619680007f901afce05ddd1de))
* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-134](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-102...1.0.0-134) (2023-08-03)


### Bug Fixes

* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **authentication:** bug fix for correct username and incorrect password ([8eb0493](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8eb0493a51b6442e19cbcabd3d96bc9a41dc3a09))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **camera:** add pwa-elements to support camera on browser ([0cb3ec9](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0cb3ec90ae9780314aeb7d12e7cdc3718f8ffc40))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **inappbrowser:** post KYC processes and landing page ([21b9387](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/21b93876dce1c83295301efe2bf427cc8066b461))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **key:** do not stop the lockservice on kyc process ([fb9eb92](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fb9eb92a25cf4d048e3caabfec0477d3d14c2510))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **user-display-picture:** issue with changing uploaded DP in the home page ([f3dde30](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f3dde30715dd28f06e46c0cc7965984ed5cd8287))
* **walkthrough:** last slide drag event is handled ([9303fd0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9303fd08c9b73cf71d66d689199591e0b09a860e))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))
* **wallet-list:** minor bug fix ([cd686f5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cd686f599da4f8ea1d05d1e1df925359133b1cea))


### Features

* **kyc:** use @capacitor/browser for android ([0c21aff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0c21affea3324ddafbaa5a7490c3761188805c54))
* **storage:** use own branch per storage & wait angular for localForage to be ready ([879334d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/879334d5eb82cf9619680007f901afce05ddd1de))
* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-127](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-102...1.0.0-127) (2023-08-01)


### Bug Fixes

* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))


### Features

* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-100](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-67...1.0.0-100) (2023-07-11)


### Features
* **app:** migrate app from cordova to capacitor@5

* **ui:** improvements on the user journey


### Bug Fixes

* **bug:** Wallet Card color stays constant ([e3cd821](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e3cd8212a91b6bd078336b58872f239223480448))


# [1.0.0-67](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-66...1.0.0-67) (2023-07-05)


### Bug Fixes

* **chart:** use the right dataSet, remove plain js func ([644f430](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/644f43020302964349f44cac70e71ace019d60ec))
* **design:** SVG icons around the chart ([1e03d29](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/1e03d290ed65d5af494d326461f02f52cb664670))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([6175006](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6175006d909f16ab9ebfb25c79fc614880bb1236))



# [1.0.0-66](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-64...1.0.0-66) (2023-07-04)


### Bug Fixes

* **hcaptch:** catch exc like challenge-closed ([923c3b8](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/923c3b821031e0b00e27c8bc3dda948d9eff831b))
* **payments:** don't add account if no publicKey present ([5279c96](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/5279c966d1ad7a6a5454ed925140968f92d6ebe7))
* **payments:** don't pass obsolete params ([f324811](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f324811ccff875e17c606337f8ae3a39ede0b0a6))



# [1.0.0-66](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-64...1.0.0-66) (2023-07-04)


### Bug Fixes

* **hcaptch:** catch exc like challenge-closed ([923c3b8](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/923c3b821031e0b00e27c8bc3dda948d9eff831b))
* **payments:** don't add account if no publicKey present ([5279c96](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/5279c966d1ad7a6a5454ed925140968f92d6ebe7))
* **payments:** don't pass obsolete params ([f324811](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f324811ccff875e17c606337f8ae3a39ede0b0a6))



# 1.0.0-64 (2023-06-27)


### Bug Fixes

* **all:** remove unused animated header ([7155e3f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7155e3f80a2a63eecdf39511ca2655a1a089ad98))
* **chat:** Removal of chat related calls from all pages ([a53ffb0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a53ffb01c066923ded406d52c50dd402902d63c0))
* **cordova:** type safe if deeplinks plugin is not available ([5067194](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/5067194aa8e2a9e3ee6cba67b328bd5ac23ec299))
* **crypto-currency:** catch exception on setUserCurrenciesListInStorage to not break app ([9276891](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9276891b9a9bb506bd7273813ec11e05ea56bef6))
* **findings-AM:** fix citiziens on kyc ([f105c3b](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f105c3b859a6e2f6cf09157de13368b0047dcd55))
* **findings-ON:** fix some bugs on camera and upload picture ([2a906a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/2a906a4f6bb0d26a861985a5dd831e51976c03f5))
* **findings-ON:** don't reload KYC media every time ([ae26db2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ae26db275785085ee90378bd8dd3dc142096d4db))
* **findings-ON:** minor fix on wallet and hcaptch page ([337dc39](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/337dc397d5e633ebb12f6cf8f7b947c8b7b9ab13))
* **findings-ON:** update human protcol with right symbol ([9443e44](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9443e44e41e32219878b94da4f2d846a61a33a16))
* **login:** prevent spinner to disappear before redirect on main page ([aa2b0e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/aa2b0e124ec4b516facb187f0c5ccd1ff1003cb0))
* **login:** safe close of OTP modal ([8869e23](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8869e232063fd87b89ef747401d3281be3763077))
* **payments:** after deleting wallet return back to payments page ([1c783bf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/1c783bf5b235d7ad377446b15ed70d2bbca4e02d))
* **payments:** cleanup curriences and removed wallets ([d41ce70](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d41ce70d76350ac4882a129d9b16fce6a5a4ac78))
* **payments:** don't set click event on chart segement ([d94a86a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d94a86a46a8516716242d70289f4281f379c71a4))
* **payments:** fix flickering issue on the d3 chart and token list ([6586f6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6586f6ca813a6309ee0dca2547d14285505851f8))
* **payments:** fix flickering with performance boost ([adec0aa](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/adec0aa06bb012e468f03ebc9cfad8f04eb66080))
* **payments:** fix some bugs on the wallet ([9c48971](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9c48971c74dcc5cb44889dab6f6c558c172ed2fc))
* **sign-up:** check for undefined ([de10276](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/de10276269e098f6c972bb02b443ba7969420eca))
* **sign-up:** set the focus on username field at the right point ([df84480](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df84480d39bb6881c00e465ab77e1bfcd8ffacf0))
* **tab-home:** obsolet code which raises an exc ([86eadaf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/86eadafc6dfdd066f62847a472907ebd5f6a8ab9))
* **tab-home:** use ionViewDidEnter instead customized onEnter method from even customized IonPage which was called more then just once ([84d6dc2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/84d6dc273ed1183d7ae65422efbcf290a5d23fbf))


### Features

* **payments:** add Account model ([4ebb94a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4ebb94ae1d59fcaa5aab16e13c86a41700f06292))
* **sentry:** add sentry error log ([513efea](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/513efea7fa15c4319f2da8bfdb7bdf92d722b77a))



