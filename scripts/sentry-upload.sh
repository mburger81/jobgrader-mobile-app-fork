#!/bin/bash

PACKAGE_VERSION=$1

echo 'RELEASE VERSION '$PACKAGE_VERSION' TO SENTRY'
echo '=================================================================='

npx @sentry/cli releases -o blockchain-helix -p jobgrader-app new $PACKAGE_VERSION
npx @sentry/cli releases -o blockchain-helix -p jobgrader-app files $PACKAGE_VERSION upload-sourcemaps ./www
#npx @sentry/cli releases -o blockchain-helix -p jobgrader-app finalize $PACKAGE_VERSION


echo 'SENTRY UPLOAD DONE'
