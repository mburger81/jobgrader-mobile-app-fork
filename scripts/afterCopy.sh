#!/bin/bash

CAPACITOR_PLATFORM_NAME=$1

echo 'AFTER COPY START'
if [ $CAPACITOR_PLATFORM_NAME == "android" ]
then
    rm android/app/src/main/assets/public/*.map
    rm android/app/src/main/assets/public/sql-wasm.wasm
fi

if [ $CAPACITOR_PLATFORM_NAME == "ios" ]
then
    rm ios/App/App/public/*.map
    rm ios/App/App/public/sql-wasm.wasm
fi

echo 'AFTER COPY DONE'
