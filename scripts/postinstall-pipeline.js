const fs = require('fs');

const filesEdit = [
    {
        location: 'package.json',
        removeContent: /\"fsevents\": \"\^1.2.13\",/g,
        replaceContent: ''
    }
]
filesEdit.forEach(k => {
    console.log(k)
    fs.readFile(k.location, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        const result = data.replace(k.removeContent, k.replaceContent);
    
        fs.writeFile(k.location, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
        console.log('Finished patch');
    });
})

// console.log('Executing patching Cordova Firebase');
// const fFireBase = 'plugins/cordova-plugin-firebase/scripts/after_prepare.js';
// fs.readFile(fFireBase, 'utf8', function (err,data) {
//     if (err) {
//         return console.log(err);
//     }
//     const result = data.replace(
//         'fs.readFileSync("platforms/android/res/values/strings.xml").toString();',
//         'fs.readFileSync("platforms/android/app/src/main/res/values/strings.xml").toString();')
//         .replace(
//         'fs.writeFileSync("platforms/android/res/values/strings.xml", strings);',
//         'fs.writeFileSync("platforms/android/app/src/main/res/values/strings.xml", strings);'
//     );
//
//     fs.writeFile(fFireBase, result, 'utf8', function (err) {
//         if (err) return console.log(err);
//     });
//     console.log('Finished executing patching Cordova Firebase');
// });

