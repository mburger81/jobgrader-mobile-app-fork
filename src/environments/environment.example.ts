export const environment = {
    production: true,
    helixEnv: '',
    icloud: '',
    apiUrl: '',
    chatServerUrl: '',
    chatUsernameSuffix: '',
    marketplace: '',
    blue: '',
    authadaiOS: '',
    authadaAndroid: '',
    helixTicketShop: '',
    did: {
      veriff: '',
      verifeye: '',
      authada: '',
      ondato: ''
    },
    verificationProd: true, // Set it to false when testing is done
    sentry: {
      url: '',
    },
    firebase: {
      apiKey: "",
      authDomain: "",
      projectId: "",
      storageBucket: "",
      messagingSenderId: "",
      appId: "",
      measurementId: "",
      vapidKey: ""
    },
    fortune: {
      url: ''
    },
    googleAuth: {
      scopes: [""],
      serverClientId: "",
      clientId: "",
      iosClientId: "",
      androidClientId: "",
      forceCodeForRefreshToken: true
    }
  };
