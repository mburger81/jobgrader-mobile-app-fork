import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import '@angular/compiler';
// import * as Sentry from "@sentry/capacitor";
import * as SentryAngular from "@sentry/angular-ivy";
import { CaptureConsole as CaptureConsoleIntegration } from "@sentry/integrations";
import { enableAkitaProdMode, persistState, selectPersistStateInit } from '@datorama/akita';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';
import * as localForage from 'localforage';
import capacitorDataStorgeSQLiteDriver from './app/shared/localforage-capdatastoragesqlitedriver/localforage-capdatastoragesqlitedriver';
import { Capacitor } from '@capacitor/core';
import { defineCustomElements as jeepSqlite} from 'jeep-sqlite/loader';

import packageJson from '../package.json';

import { defineCustomElements as pwaElements } from '@ionic/pwa-elements/loader';
import { ReplaySubject } from 'rxjs';

declare var console: any;

if (environment.production) {
  enableProdMode();
  enableAkitaProdMode();
  // const console: any = {};
  console.log = function() {};
  // console.error = function() {};
  console.warn = function() {};
} else {
  const initialConsole = (window as any).console;
  const shouldBeFiltered = function (...params) {
    return (arguments as any).filter(a => a.indexOf('kill()')).length > 0
  };
  const console: any = {};
  console.log = function() {
    if (shouldBeFiltered(arguments)) {
      return;
    } else {
      initialConsole.log.apply(window, arguments);
    }
  };
  console.error = function() {
    if (shouldBeFiltered(arguments)) {
      return;
    } else {
      initialConsole.error.apply(window, arguments);
    }
  };
  console.warn = function() {
    if (shouldBeFiltered(arguments)) {
      return;
    } else {
      initialConsole.warn.apply(window, arguments);
    }
  };
}
(window as any).console = console;

export class FileReaderA extends window.FileReader {
	constructor() {
		super();
		const zoneOriginalInstance = (this as any)['__zone_symbol__originalInstance'];
		return zoneOriginalInstance || this;
	}
}

window.FileReader = FileReaderA;

if (!Capacitor.isNativePlatform()) {
  jeepSqlite(window);
}
// (Capacitor.isNativePlatform() ? Sentry : SentryAngular).init(
SentryAngular.init(
  {
    dsn: environment.sentry.url,
    integrations: [
      new SentryAngular.BrowserTracing({
        // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
        tracePropagationTargets: [
          "localhost", environment.apiUrl
        ],
        routingInstrumentation: SentryAngular.routingInstrumentation,
      }),
      new CaptureConsoleIntegration(
        {
          levels: [ 'error' ]
        }
      ),
      // new Sentry.Replay()
    ],
    release: packageJson.version,
    sendDefaultPii: false,
    // beforeSend(event) {
    //   return null;
    // },
    // Performance Monitoring
    tracesSampleRate: 1.0, // Capture 100% of the transactions, reduce in production!
    // Session Replay
    replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
    replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
    // enableNative: false
  },
  // Forward the init method from @sentry/angular
  // (Capacitor.isNativePlatform() ? SentryAngular.init : null)
);


// IMPORTANT: Akitas selectPersistStateInit does not work as expected
// when you have more then one persisteState
// we can not distinguish which state is ready when even is fired
// for this reason we made a workaround and wait until all 5 are ready
// and emit then a custom event
const _persistStateReady = new ReplaySubject(1);

export function persistStateReady() {
  return _persistStateReady.asObservable();
}

let countPersistStateReady = 0;
selectPersistStateInit()
  .subscribe(
    () => {
      countPersistStateReady++;

      if (countPersistStateReady === 5) {
        _persistStateReady.next(true);
      }

    }
  )


localForage.defineDriver(
  capacitorDataStorgeSQLiteDriver,
  () => {

    localForage.config({
      driver: [ capacitorDataStorgeSQLiteDriver._driver, localForage.INDEXEDDB, localForage.LOCALSTORAGE ],
      name: '_jobgrader',
      version: 1.0,
      storeName: 'akita-storage',
    });

})
.finally(
  () =>
    {

      const secureStoragePersistStorage = persistState({
        include: ['secure-storage'],
        key: 'secure-storage',
        storage: localForage,
      });

      const walletsPersistStorage = persistState({
        include: ['wallets'],
        key: 'wallets',
        storage: localForage,
      });

      const vaultSecretsPersistStorage = persistState({
        include: ['vault-secrets'],
        key: 'vault-secrets',
        storage: localForage,
      });

      const kycMediaPersistStorage = persistState({
        include: ['kyc-media'],
        key: 'kyc-media',
        storage: localForage,
      });

      const userPhotoPersistStorage = persistState({
        include: ['user-photo'],
        key: 'user-photo',
        storage: localForage,
      });

      const providers = [
        { provide: 'persistStorage', useValue: secureStoragePersistStorage, multi: true },
        { provide: 'persistStorage', useValue: walletsPersistStorage, multi: true },
        { provide: 'persistStorage', useValue: vaultSecretsPersistStorage, multi: true },
        { provide: 'persistStorage', useValue: kycMediaPersistStorage, multi: true },
        { provide: 'persistStorage', useValue: userPhotoPersistStorage, multi: true },
      ];


      platformBrowserDynamic(providers).bootstrapModule(AppModule)
          .catch(err => console.log(err));


      // Call the element loader after the platform has been bootstrapped
      pwaElements(window);

    }
);
