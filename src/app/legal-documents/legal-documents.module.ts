import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
// import { TooltipsModule } from 'ionic-tooltips';
import { LegalDocumentsPage } from './legal-documents.page';
import { DocumentModalPageModule } from './document-modal/document-modal.module';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    component: LegalDocumentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    // TooltipsModule,
    JobHeaderModule,
    DocumentModalPageModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [LegalDocumentsPage]
})
export class LegalDocumentsPageModule {}
