import { Component, Input, OnInit, HostListener } from '@angular/core';
import { Platform, ModalController, ToastController, ActionSheetController, AlertController } from '@ionic/angular';
import { KycProvider, KycVendors } from '../../kyc/enums/kyc-provider.enum';
import { KycState } from '../../kyc/enums/kyc-state.enum';
import { environment } from '../../../environments/environment';
import { KycService } from 'src/app/kyc/services/kyc.service';
import { SignProviderService, ProviderDocument } from '../../core/providers/sign/sign-provider.service';
import { LockService } from '../../lock-screen/lock.service';
import { AlertsProviderService } from 'src/app/core/providers/alerts/alerts-provider.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { TimesService } from '../../core/providers/times/times.service';
import { DatePipe } from '@angular/common';
import { LoaderProviderService } from '../../core/providers/loader/loader-provider.service';
import { User } from '../../core/models/User';
import { Trust } from '../../core/models/Trust';
import { from } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { ApiProviderService } from '../../core/providers/api/api-provider.service';
import { AppStateService } from 'src/app/core/providers/app-state/app-state.service';
import { UserProviderService } from  'src/app/core/providers/user/user-provider.service';
import { combineLatest, interval } from 'rxjs';
import { SecureStorageKey } from '../../core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../../core/providers/secure-storage/secure-storage.service';
import { CryptoProviderService } from '../../core/providers/crypto/crypto-provider.service';
import { SettingsService, Settings } from '../../core/providers/settings/settings.service';
import { Clipboard } from '@capacitor/clipboard';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { uniq } from 'lodash';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { KycPins } from 'src/app/core/providers/device/udid.enum';
const vc = require('@digitalbazaar/vc');
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { GdriveService } from 'src/app/core/providers/cloud/gdrive.service';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { KycMedia } from '../../core/providers/state/kyc-media/kyc-media.model';
import { KycMediaServiceAkita } from '../../core/providers/state/kyc-media/kyc-media.service';
import { DropboxService } from 'src/app/core/providers/cloud/dropbox.service';
import { KycmediaService } from 'src/app/core/providers/kycmedia/kycmedia.service';

declare var window: any;
declare var iCloudDocStorage: any;

interface CountryDisplay {
  countryofbirth?: string;
  citizenship?: string;
  country?: string;
  identificationissuecountry?: string;
  driverlicencecountry?: string;
  passportissuecountry?: string;
  residencepermitissuecountry?: string;
}

interface FormElements {
  number?: string;
  issuedate?: string;
  expirydate?: string;
  issuecountry?: string;
}

@Component({
  selector: 'app-document-modal',
  templateUrl: './document-modal.component.html',
  styleUrls: ['./document-modal.component.scss'],
})
export class DocumentModalPageComponent implements OnInit {
  @Input() data: any;

  public countryDisplay: CountryDisplay = {};
  public editMode = false;
  public editForm: UntypedFormGroup;
  public saveObject: FormElements = {};
  public countryList = this.signService.countries;
  public user: User = {};
  public vcQrCodeValue: Array<string> = [];
  public vpQrCodeValue: Array<string> = [];
  public vcToggleView: boolean = false;
  public vpToggleView: boolean = false;
  private static defaultImage = '../../../assets/job-user.svg';
  public profilePictureSrc = DocumentModalPageComponent.defaultImage;
  private browser: any;
  private browserOptions = 'footer=yes,hideurlbar=no,footercolor=#BF7B54,hidenavigationbuttons=no,presentationstyle=pagesheet';
  public sliderOptions = {
    initialSlide: 0,
    speed: 500
  };
  private datePipe = new DatePipe('en-US');
  // public variableTimestamp: string = this.datePipe.transform(new Date(), 'dd.MM.yyyy HH:mm:ss');
  public variableTimestamp: Date = new Date();
  private timestampObserver = interval(1000);
  private timestampSubscription = this.timestampObserver.subscribe(() => this.changeTimestamp());

  public resetDeleteMode: boolean = false;

  constructor(private _ModalController: ModalController,
    private kycService: KycService,
    public signService: SignProviderService,
    private lockService: LockService,
    public timesService: TimesService,
    private toastController: ToastController,
    private loader: LoaderProviderService,
    
    private settingsService: SettingsService,
    private apiProviderService: ApiProviderService,
    private appStateService: AppStateService,
    private userProviderService: UserProviderService,
    private alertsProviderService: AlertsProviderService,
    private _ActionSheetController: ActionSheetController,
    private crypto: CryptoProviderService,
    private secureStorage: SecureStorageService,
    private platform: Platform,
    private iab: InAppBrowser,
    private _FileOpener: FileOpener,
    private safariViewController: SafariViewController,
    private translateProviderService: TranslateProviderService,
    private _NetworkService: NetworkService,
    private _SocialSharing: SocialSharing,
    private _File: File,
    private _GDrive: GdriveService,
    private _Dropbox: DropboxService,
    private _AlertController: AlertController,
    private kycMediaServiceAktia: KycMediaServiceAkita,
    private kycMediaService: KycmediaService
    ) {

      // if (this.data && this.data.showAsInfoPage) {
      //   alert(1)
      //   return;
      // }

      // this.editForm = new FormGroup({
      //   number: new FormControl('', [Validators.required]),
      //   issuedate: new FormControl('', [Validators.required]),
      //   expirydate: new FormControl('', [Validators.required]),
      //   issuecountry: new FormControl('' , [Validators.required]),
      // })
      // this.userProviderService.getUser().then(user => this.user = user)
    }

  ngOnInit() {

    if (this.data && this.data.showAsInfoPage) { return; }

    if(this.data.header) {
      this.data.header = this.data.header.replace("- ", ""); // Note: Added to display the DE translation of the Residence Permit correctly.
    }

    this.editForm = new UntypedFormGroup({
      number: new UntypedFormControl('', [Validators.required]),
      issuedate: new UntypedFormControl('', [Validators.required]),
      expirydate: new UntypedFormControl('', [Validators.required]),
      issuecountry: new UntypedFormControl('' , [Validators.required]),
    })
    this.userProviderService.getUser().then(user => this.user = user);
    this.data.vc = this.data.vc.filter(v => !!v);
    this.data.vc.forEach(k => {
      if(Object.keys(k).length != 0 && (this.data.numberStatus == 1 || this.data.expirydateStatus == 1 || this.data.issuecountryStatus == 1)) {
        var qr = k
        if(!!k.id && !!k.credentialSubject.id) {
          var id = k.id;
          var holder = k.credentialSubject.id;
          var vp = vc.createPresentation({k, id, holder})
          this.vpQrCodeValue.push(JSON.stringify(vp))
        }
        if(!!qr.proof) { delete qr.proof }
        this.vcQrCodeValue.push(JSON.stringify(qr))
      }
    })

    this.resetDeleteMode = ((this.data.numberStatus == 1) && (this.data.expirydateStatus == 1) && (this.data.issuecountryStatus == 1));

  }

  changeTimestamp() {
    // this.variableTimestamp = this.datePipe.transform(new Date(), 'dd.MM.yyyy HH:mm:ss');
    this.variableTimestamp = new Date();
  }

  renew() {
    // console.log('Renew mode activated!');
    this.alertsProviderService.alertCreate(this.translateProviderService.instant('PERSONALDETAILS.confirm-renew'),'',this.translateProviderService.instant('PERSONALDETAILS.message-renew'),
      [
        {
          text: this.translateProviderService.instant('SIGNSTEPEIGHT.selectOk'), handler: async () => {
          // console.log('ok');
          await this.presentToast(this.translateProviderService.instant('CONTACT.title'))
        }
      },
        { text: this.translateProviderService.instant('SIGNSTEPEIGHT.selectCancel'), handler: () => { console.log('cancel') }}
      ]);
  }

  delete() {
    this.alertsProviderService.alertCreate(this.translateProviderService.instant('PERSONALDETAILS.confirm-delete'),this.translateProviderService.instant('PERSONALDETAILS.header-delete'),this.translateProviderService.instant('PERSONALDETAILS.message-delete'),
      [
        {
          text: this.translateProviderService.instant('SIGNSTEPEIGHT.selectOk'), handler: async () => {
          await this.loader.loaderCreate();
          console.log('ok');
          var docType = this.data.documentType.replace('_','');
          var kycMedia: KycMedia[] = this.kycMediaServiceAktia.getAllKycMedia();
          var filteredMedia = kycMedia.filter(kc => kc.documentType != docType);
          var sessionIdArray = Array.from(kycMedia.filter(kcm => kcm.documentType == docType), kc => (kc as any).sessionId);
          this.kycMediaServiceAktia.setKycMedia(filteredMedia);
          sessionIdArray.forEach(async si => {
          await this.apiProviderService.deleteKycMedia(si).catch(async e => {
              await this.loader.loaderDismiss();
              await this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));
            })
            await this.loader.loaderDismiss();
            await this.presentToast(this.translateProviderService.instant('LEGALDOCUMENTS.kyc-media-removed'));
          })
          this.editForm.controls.number.setValue('');
          this.editForm.controls.issuedate.setValue(null);
          this.editForm.controls.expirydate.setValue(null);
          this.editForm.controls.issuecountry.setValue('');
          await this.finishSaving();
        }
      },
        { text: this.translateProviderService.instant('SIGNSTEPEIGHT.selectCancel'), handler: () => { console.log('cancel') }}
      ]);
  }

  public async showActionSheet(key: string) {
    var stringToDisplay = (key == 'vc') ? this.vcQrCodeValue : this.vpQrCodeValue
    const alert = await this._ActionSheetController.create({
      mode: 'md',
      header: this.translateProviderService.instant('CERTIFICATES.chooseFormat'),
      buttons: [
        { text: 'JSON String', icon: 'code', handler: async () => {
        await this.alertsProviderService.alertCreate('', '', JSON.stringify(stringToDisplay),[this.translateProviderService.instant('GENERAL.ok')])
        }},
        { text: 'QR Code', icon: 'qr-code', handler: () => {
          this.vcToggleView = (key == 'vc');
          this.vpToggleView = (key == 'vp');
        }},
        { text: this.translateProviderService.instant('GENERAL.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('Cancel') } }
      ]
    });
    await alert.present();
  }

  async close() {
    this.timestampSubscription.unsubscribe();
    await this._ModalController.dismiss();
  }

  copyCoupons(code: string, url: string) {
    // console.log(code);
    Clipboard.write({ string: code }).then(() => {
      this.presentToast(`Coupon code ${code}${this.translateProviderService.instant('SETTINGSACCOUNT.copied')}!`);
      this._ModalController.dismiss();
      if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
        this.showSafariInstance(url);
      } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system', this.browserOptions);
      } else if ( !this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system');
      }
    })
    
  }

  showSafariInstance(url: string) {
    this.safariViewController.isAvailable().then((available: boolean) => {
            if ( available ) {
                this.safariViewController.show({
                    url,
                    hidden: false,
                    animated: true,
                    transition: 'slide',
                    enterReaderModeIfAvailable: false,
                    tintColor: '#54BF7B'
                })
                    .subscribe((result: any) => {
                        },
                        (error: any) => console.error(error));
            } else {
                this.browser = this.iab.create(url, '_self', this.browserOptions);
            }
        }
    );
}

  edit(s: string){
    // console.log(s);
    this.editMode = true;
    this.fillModel(this.saveObject);
    this.setFormDataFromStorage(this.saveObject);
    // console.log(this.data);
    // console.log(this.saveObject);
    // console.log(this.editForm);
  }

  fillModel(model: FormElements) {
    try {
        model.number = this.data.number;
        model.issuedate = this.data.issuedate ? (new Date(this.data.issuedate)).toISOString() : null;
        model.expirydate = this.data.expirydate ? (new Date(this.data.expirydate)).toISOString() : null;
        model.issuecountry = this.data.issuecountry;
    } catch (e) {
        console.log(e);
    }
  }

  private setFormDataFromStorage(model: FormElements): void {
    if ( model && model.number ) { this.editForm.controls.number.setValue(model.number); }
    if ( model && model.issuedate ) { this.editForm.controls.issuedate.setValue(model.issuedate); }
    if ( model && model.expirydate ) { this.editForm.controls.expirydate.setValue(model.expirydate); }
    if ( model && model.issuecountry ) { this.editForm.controls.issuecountry.setValue(model.issuecountry); }
  }

  cancel(){
    this.editMode = false;
  }

  async save(){

    if(!this._NetworkService.checkConnection()) {
      this.toastController.create({
          message: this.translateProviderService.instant('NETWORK.checkConnection'),
          duration: 3000,
          position: 'top',
      }).then(toast => {
          toast.present();
      });
      return;
    }

    console.log('save works');
    this.editForm.markAsDirty();

    if ( !this.editForm.valid ) {
        this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.fillToast'));
        return false;
    }

    if ((this.editForm.controls.number.value).length >=50){
        return this.presentToast(this.translateProviderService.instant('PERSONALDETAILS.idcardnumber'));
    }

    if ( this.editForm.controls.issuedate.value && this.editForm.controls.expirydate.value ) {
        if ( (this.getFormattedDay(this.editForm.controls.issuedate.value)
            > this.getFormattedDay(this.editForm.controls.expirydate.value)) ) {
            return this.presentToast(this.translateProviderService.instant('SIGNSTEPEIGHT.dateError'));
        }
    }

    this.editMode = false;

    await this.finishSaving();
  }

  async finishSaving() {
    await this.loader.loaderCreate();
      const processedJSON = await this.processJSON();
      // console.log('processedJSON: ' + JSON.stringify(processedJSON));
      from(this.apiProviderService.saveUser(this.appStateService.basicAuthToken, processedJSON))
          .pipe(
              switchMap(_ => combineLatest(
                  [from(this.apiProviderService.getTrustData(this.appStateService.basicAuthToken, this.user.userid)),
                  from(this.apiProviderService.getUserByIdSecure(this.appStateService.basicAuthToken, this.user.userid))
                  ])
              ),
              catchError(error => {
                  console.log(error);
                  this.loader.loaderDismiss();
                  return error;
              }),
              take(1)
          ).subscribe(([trust, user]: [Trust, User]) => {
          this.loader.loaderDismiss();

          if (this.data.documentType == 'PASSPORT') {
            this.settingsService.set(Settings.passportSet, true.toString(), true);
          }
          if (this.data.documentType == 'ID_CARD') {
            this.settingsService.set(Settings.idCardSet, true.toString(), true);
          }
          if (this.data.documentType == 'DRIVERS_LICENSE') {
            this.settingsService.set(Settings.driverLicenseSet, true.toString(), true);
          }
          if (this.data.documentType == 'RESIDENCE_PERMIT') {
            this.settingsService.set(Settings.residencePermitSet, true.toString(), true);
          }
          this.secureStorage.setValue(SecureStorageKey.userData, JSON.stringify(user)).then(() => {})
          this.secureStorage.setValue(SecureStorageKey.userTrustData, JSON.stringify(trust)).then(() => {})
          this.user = Object.assign({}, user, this.userProviderService.mapTrustData(trust));
          this.editMode = false;
          this._ModalController.dismiss();
      }, _ => void this.loader.loaderDismiss());
  }

  async processJSON(): Promise<any> {
    var token = await this.secureStorage.getValue(SecureStorageKey.basicAuthToken, false);
    var symmetricKey = await this.crypto.returnUserSymmetricKey();
    var chatPublicKey = await this.secureStorage.getValue(SecureStorageKey.chatPublicKey, false);
    var chatPrivateKey = await this.secureStorage.getValue(SecureStorageKey.chatPrivateKey, false);
    var encryptedChatPrivateKey = await this.crypto.symmetricEncrypt(chatPrivateKey, symmetricKey);
    var object = {
        user: {
            username: window.atob(token).split(':')[0],
            password: null,
            email: this.user.email,
            id: this.user.userid,
            chatPublicKey: chatPublicKey,
            encryptedChatPrivateKey: encryptedChatPrivateKey
        },
        basicdata: {
            userid: this.user.userid,
            firstname: this.user.firstname,
            lastname: this.user.lastname,
            title: this.user.title,
            dateofbirth: this.user.dateofbirth,
            gender: this.user.gender,
            phone: this.user.phone,
            email: this.user.email,
            callname: this.user.callname,
            street: this.user.street,
            city: this.user.city,
            state: this.user.state,
            zip: this.user.zip,
            maidenname: this.user.maidenname,
            middlename: this.user.middlename,
            cityofbirth: this.user.cityofbirth,
            maritalstatus: this.user.maritalstatus,
            termsofuse: this.user.termsofuse,
            termsofusethirdparties: this.user.termsofusethirdparties,
            countryofbirth: this.user.countryofbirth,
            country: this.user.country,
            citizenship: this.user.citizenship,
            identificationdocumenttype: this.user.identificationdocumenttype,
            identificationdocumentnumber: this.user.identificationdocumentnumber,
            identificationissuecountry: this.user.identificationissuecountry,
            identificationissuedate: this.user.identificationissuedate,
            identificationexpirydate: this.user.identificationexpirydate,
            driverlicencedocumentnumber: this.user.driverlicencedocumentnumber,
            driverlicencecountry: this.user.driverlicencecountry,
            driverlicenceissuedate: this.user.driverlicenceissuedate,
            driverlicenceexpirydate: this.user.driverlicenceexpirydate,
            passportnumber: this.user.passportnumber,
            passportissuecountry: this.user.passportissuecountry,
            passportissuedate: this.user.passportissuedate,
            passportexpirydate: this.user.passportexpirydate,
            residencepermitnumber: this.user.residencepermitnumber,
            residencepermitissuecountry: this.user.residencepermitissuecountry,
            residencepermitissuedate: this.user.residencepermitissuedate,
            residencepermitexpirydate: this.user.residencepermitexpirydate,
        }
    };
    if(this.data.documentType == 'PASSPORT'){
      object.basicdata.identificationdocumentnumber = this.user.identificationdocumentnumber;
      object.basicdata.identificationissuecountry = this.user.identificationissuecountry;
      object.basicdata.identificationissuedate = this.user.identificationissuedate;
      object.basicdata.identificationexpirydate = this.user.identificationexpirydate;
      object.basicdata.driverlicencedocumentnumber = this.user.driverlicencedocumentnumber;
      object.basicdata.driverlicencecountry = this.user.driverlicencecountry;
      object.basicdata.driverlicenceissuedate = this.user.driverlicenceissuedate;
      object.basicdata.driverlicenceexpirydate = this.user.driverlicenceexpirydate;
      object.basicdata.passportnumber = this.editForm.controls.number.value;
      object.basicdata.passportissuecountry = this.editForm.controls.issuecountry.value;
      object.basicdata.passportissuedate = !this.editForm.controls.issuedate.value ? null : this.getFormattedDay(this.editForm.controls.issuedate.value);
      object.basicdata.passportexpirydate = !this.editForm.controls.expirydate.value ? null : this.getFormattedDay(this.editForm.controls.expirydate.value);
      object.basicdata.residencepermitnumber = this.user.residencepermitnumber;
      object.basicdata.residencepermitissuecountry = this.user.residencepermitissuecountry;
      object.basicdata.residencepermitissuedate = this.user.residencepermitissuedate;
      object.basicdata.residencepermitexpirydate = this.user.residencepermitexpirydate;
    }
    if(this.data.documentType == 'DRIVERS_LICENSE'){
      object.basicdata.identificationdocumentnumber = this.user.identificationdocumentnumber;
      object.basicdata.identificationissuecountry = this.user.identificationissuecountry;
      object.basicdata.identificationissuedate = this.user.identificationissuedate;
      object.basicdata.identificationexpirydate = this.user.identificationexpirydate;
      object.basicdata.driverlicencedocumentnumber = this.editForm.controls.number.value;
      object.basicdata.driverlicencecountry = this.editForm.controls.issuecountry.value;
      object.basicdata.driverlicenceissuedate = !this.editForm.controls.issuedate.value ? null : this.getFormattedDay(this.editForm.controls.issuedate.value);
      object.basicdata.driverlicenceexpirydate = !this.editForm.controls.expirydate.value ? null : this.getFormattedDay(this.editForm.controls.expirydate.value);
      object.basicdata.passportnumber = this.user.passportnumber;
      object.basicdata.passportissuecountry = this.user.passportissuecountry;
      object.basicdata.passportissuedate = this.user.passportissuedate;
      object.basicdata.passportexpirydate = this.user.passportexpirydate;
      object.basicdata.residencepermitnumber = this.user.residencepermitnumber;
      object.basicdata.residencepermitissuecountry = this.user.residencepermitissuecountry;
      object.basicdata.residencepermitissuedate = this.user.residencepermitissuedate;
      object.basicdata.residencepermitexpirydate = this.user.residencepermitexpirydate;
    }
    if(this.data.documentType == 'RESIDENCE_PERMIT'){
      object.basicdata.identificationdocumentnumber = this.user.identificationdocumentnumber;
      object.basicdata.identificationissuecountry = this.user.identificationissuecountry;
      object.basicdata.identificationissuedate = this.user.identificationissuedate;
      object.basicdata.identificationexpirydate = this.user.identificationexpirydate;
      object.basicdata.driverlicencedocumentnumber = this.user.driverlicencedocumentnumber;
      object.basicdata.driverlicencecountry = this.user.driverlicencecountry;
      object.basicdata.driverlicenceissuedate = this.user.driverlicenceissuedate;
      object.basicdata.driverlicenceexpirydate = this.user.driverlicenceexpirydate;
      object.basicdata.passportnumber = this.user.passportnumber;
      object.basicdata.passportissuecountry = this.user.passportissuecountry;
      object.basicdata.passportissuedate = this.user.passportissuedate;
      object.basicdata.passportexpirydate = this.user.passportexpirydate;
      object.basicdata.residencepermitnumber = this.editForm.controls.number.value;
      object.basicdata.residencepermitissuecountry = this.editForm.controls.issuecountry.value;
      object.basicdata.residencepermitissuedate = !this.editForm.controls.issuedate.value ? null : this.getFormattedDay(this.editForm.controls.issuedate.value);
      object.basicdata.residencepermitexpirydate = !this.editForm.controls.expirydate.value ? null : this.getFormattedDay(this.editForm.controls.expirydate.value);
    }
    if(this.data.documentType == 'ID_CARD'){
      object.basicdata.identificationdocumentnumber = this.editForm.controls.number.value;
      object.basicdata.identificationissuecountry = this.editForm.controls.issuecountry.value;
      object.basicdata.identificationissuedate = !this.editForm.controls.issuedate.value ? null : this.getFormattedDay(this.editForm.controls.issuedate.value);
      object.basicdata.identificationexpirydate = !this.editForm.controls.expirydate.value ? null : this.getFormattedDay(this.editForm.controls.expirydate.value);
      object.basicdata.driverlicencedocumentnumber = this.user.driverlicencedocumentnumber;
      object.basicdata.driverlicencecountry = this.user.driverlicencecountry;
      object.basicdata.driverlicenceissuedate = this.user.driverlicenceissuedate;
      object.basicdata.driverlicenceexpirydate = this.user.driverlicenceexpirydate;
      object.basicdata.passportnumber = this.user.passportnumber;
      object.basicdata.passportissuecountry = this.user.passportissuecountry;
      object.basicdata.passportissuedate = this.user.passportissuedate;
      object.basicdata.passportexpirydate = this.user.passportexpirydate;
      object.basicdata.residencepermitnumber = this.user.residencepermitnumber;
      object.basicdata.residencepermitissuecountry = this.user.residencepermitissuecountry;
      object.basicdata.residencepermitissuedate = this.user.residencepermitissuedate;
      object.basicdata.residencepermitexpirydate = this.user.residencepermitexpirydate;
    }
    // console.log(JSON.stringify(object))
    return object;
  }

  private async initiateOndato(): Promise<void> {
    const provider = environment.verificationProd ? KycProvider.ONDATO_PROD : KycProvider.ONDATO_TEST;
    await this.kycService.addInitialState(provider);
    // console.log(provider);
    if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
      await this.alreadyRetrieved(provider);
    } else {
      this.kycService.patchState(provider, KycState.KYC_WAITING);
      await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {});
    }
  }

  async showVerificationOptions() {
    var val = await this.kycService.isUserAllowedToUseChatMarketplace();

      // this._SecureStorageService.getValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, false).then((val) => {

    if(val) {
      var placeholder = environment.production ? '' : KycPins.veriff;
      var alerto = await this._AlertController.create({
        mode: 'ios',
        header: this.translateProviderService.instant('KYC.KYCPIN.header'),
        message: this.translateProviderService.instant('KYC.KYCPIN.message1'),
        inputs: [ { name: 'kycpin', placeholder: placeholder, type: 'tel'} ],
        buttons: [ {
          text: this.translateProviderService.instant('SETTINGS.ok'),
          handler: async (data) => {
              (data.kycpin == KycPins.veriff) ? await this.initiateOndato() :
                this._AlertController.create({
                  mode: 'ios',
                  message: this.translateProviderService.instant('KYC.KYCPIN.message2'),
                  buttons: [ { text: this.translateProviderService.instant('SETTINGS.ok'), role: 'cancel', handler: () => { } } ]
                }).then((al) => al.present());
            }
          }, { text: this.translateProviderService.instant('SETTINGS.cancel'), role: 'cancel', handler: () => { } }
        ]
      })
        await alerto.present();

    } else {
      // this.selectOndato();
      await this.initiateOndato();
    }

  }

  async goToVerificationOptions(documentType: string, countryCode: string) {

    // var verifficationAsUsual = async () => {
      // if (!documentType || !countryCode) {
      //   await this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.fillToast'))
      //   return;
      // }

      if(!this._NetworkService.checkConnection()) {
        this._NetworkService.addOfflineFooter('ion-footer');
        await this.presentToast(this.translateProviderService.instant('NETWORK.checkConnection'));
        return;
      } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
      }

      const kycWithVeriff = async (code2: string) => {
        const state = this.getStateArray(documentType);
        const provider = environment.verificationProd ? state.prod : state.test;
        await this.kycService.addInitialState(provider);

        code2 = code2.toLowerCase();
        this.lockService.addResume();

        if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
          await this.alreadyRetrieved(provider);
        } else {
          this.kycService.patchState(provider, KycState.KYC_WAITING);
          await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {type: documentType, country: code2});
        }

        await this._ModalController.dismiss();
      }

      const kycWithVerifeye = async (code2: string) => {
        // console.log(documentType);
        const state = this.getVerifeyeStateArray(documentType);
        const provider = environment.verificationProd ? state.prod : state.test;
        // console.log(state)
        // console.log(provider)
        await this.kycService.addInitialState(provider);
        // let {code2}: {code2: string} = this.signService.countries.find(c => c.code3 === countryCode);
        code2 = !!code2 ? code2.toLowerCase() : null;
        this.lockService.addResume();
        if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
          await this.alreadyRetrieved(provider);
        } else {
          this.kycService.patchState(provider, KycState.KYC_WAITING);
          await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {});
        }
        await this._ModalController.dismiss();
      }

      const proceedAsUsual = async () => {
        if(documentType == 'DRIVERS_LICENSE'){

          await this.initiateOndato()

        }
        else {

          await kycWithVerifeye(null);

        }
      }


      const pinEntry = () => {
        var placeholder = environment.production ? '' : KycPins.veriff;
        this._AlertController.create({
          mode: 'ios',
          header: this.translateProviderService.instant('KYC.KYCPIN.header'),
          message: this.translateProviderService.instant('KYC.KYCPIN.message1'),
          inputs: [ { name: 'kycpin', placeholder: placeholder, type: 'tel'} ],
          buttons: [ {
            text: this.translateProviderService.instant('SETTINGS.ok'),
            handler: (data) => {
                (data.kycpin == KycPins.veriff) ? proceedAsUsual() :
                  this._AlertController.create({
                    message: this.translateProviderService.instant('KYC.KYCPIN.message2'),
                    buttons: [ { text: this.translateProviderService.instant('SETTINGS.ok'), role: 'cancel', handler: () => { } } ]
                  }).then((al) => al.present());
              }
            }, { text: this.translateProviderService.instant('SETTINGS.cancel'), role: 'cancel', handler: () => { } }
          ]
        }).then((alert) => {
          alert.present();
        })
      }

      // pinEntry();
    // }


    if(await this.kycService.isUserAllowedToUseChatMarketplace()) {
      pinEntry();
    } else {
      await this._ModalController.dismiss();
      // await this.initiateOndato();
      // await this.initiateVerifeye();
      proceedAsUsual();
    }
}

public getVerifeyeStateArray(docType: string): {prod: KycProvider, test: KycProvider}{
  switch (docType) {
    case 'PASSPORT':
      return {
        prod: KycProvider.VERIFEYE_PROD_PASSPORT,
        test: KycProvider.VERIFEYE_TEST_PASSPORT
      };
    case 'ID_CARD':
      return {
        prod: KycProvider.VERIFEYE_PROD_IDCARD,
        test: KycProvider.VERIFEYE_TEST_IDCARD
      };
    case 'RESIDENCE_PERMIT':
      return {
        prod: KycProvider.VERIFEYE_PROD_RESIDENCEPERMIT,
        test: KycProvider.VERIFEYE_TEST_RESIDENCEPERMIT
      };
    case 'DRIVERS_LICENSE':
      return {
        prod: KycProvider.VERIFEYE_PROD_DRIVINGLICENCE,
        test: KycProvider.VERIFEYE_TEST_DRIVINGLICENCE
      };
  }
}

public getStateArray(docType: string): {prod: KycProvider, test: KycProvider}{
  switch (docType) {
    case 'PASSPORT':
      return {
        prod: KycProvider.VERIFF_PROD_PASSPORT,
        test: KycProvider.VERIFF_TEST_PASSPORT
      };
    case 'RESIDENCE_PERMIT':
      return {
        prod: KycProvider.VERIFF_PROD_RESIDENCEPERMIT,
        test: KycProvider.VERIFF_TEST_RESIDENCEPERMIT
      };
    case 'ID_CARD':
      return {
        prod: KycProvider.VERIFF_PROD_IDCARD,
        test: KycProvider.VERIFF_TEST_IDCARD
      };
    case 'DRIVERS_LICENSE':
      return {
        prod: KycProvider.VERIFF_PROD_DRIVINGLICENCE,
        test: KycProvider.VERIFF_TEST_DRIVINGLICENCE
      };
  }
}

private async alreadyRetrieved(provider: KycProvider): Promise<void> {
  await this.alertsProviderService.alertCreate(
      this.translateProviderService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.header`),
      this.translateProviderService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.sub-header`),
      this.translateProviderService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.message`),
      [
        {
          text: this.translateProviderService.instant('GENERAL.ok'),
          handler: () => {}
        }]
  );
}

private async stillRunningAlert(provider: KycProvider): Promise<void> {
  await this.alertsProviderService.alertCreate(
      this.translateProviderService.instant(`KYC.messages.${provider}_RUNNING.header`),
      this.translateProviderService.instant(`KYC.messages.${provider}_RUNNING.sub-header`),
      this.translateProviderService.instant(`KYC.messages.${provider}_RUNNING.message`),
      [
        {
          text: this.translateProviderService.instant('GENERAL.ok'),
          handler: () => {}
        }]
  );
}

private async unsupported(){
  await this.alertsProviderService.alertCreate('','',this.translateProviderService.instant('KYC.UNSUPPORTED'),
          [
            {
              text: this.translateProviderService.instant('GENERAL.ok'),
              handler: () => {}
            }]
        );
  }

  processCountryDisplay(countryCode: string) {
    let temp_country = this.signService.countries.find(d => d.code3 === countryCode);
    return (!!temp_country && !!countryCode) ? this.translateProviderService.instant(temp_country["countryTranslateKey"]) : countryCode;
  }

  private getFormattedDay(date: string): number {
      const ms = this.datePipe.transform(new Date(date), 'SSS');
      const t = this.datePipe.transform(new Date(date), 'E MMM dd yyyy HH:mm:ss zzzz');
      const nd = +new Date(t).setMilliseconds(parseInt(ms));
      // console.log('Input date value: ' + date);
      // console.log('Transformed without ms: ' + t);
      // console.log('ms: ' + ms);
      // console.log('Transformed with ms: ' + nd);
      return nd;
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
        message,
        duration: 2000,
        position: 'top',
    });
    toast.present();
  }

  async saveAsFile(choice: string) {

    var qrSet = (choice == 'vc') ? this.vcQrCodeValue[0] : this.vpQrCodeValue[0];
    var prefix = (choice == 'vc') ? '' : 'vp-';
    const notifText = (choice == 'vc') ? this.translateProviderService.instant('CLOUDBACKUP.vc-success') : this.translateProviderService.instant('CLOUDBACKUP.vp-success');
    console.log(qrSet);

    var path = null;

    if( this.platform.is('ios') && this.platform.is('hybrid') ) {
      path = this._File.documentsDirectory;
    } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
      path = this._File.dataDirectory;
    }
    var parsed = null;
    try {
      parsed = JSON.parse(qrSet);
    } catch(e) {
      parsed = qrSet;
    }
    console.log(parsed);

    var fileName = `${prefix}${parsed.id.replaceAll(":","-")}.json`;
    var fullPath = path + fileName;

    var blobUrl = URL.createObjectURL(new Blob([qrSet], {type: "application/json"}));

    if(this.platform.is('hybrid')) {
      if(this.platform.is('ios')) {
        await this._File.writeFile(path, fileName, new Blob([qrSet], {type: "application/json"}), { replace: true });

        try {
          var presentToastOpener = () => {
            iCloudDocStorage.fileList("Cloud", (a) => {
              var aaa = Array.from(a, aaaa => Object.keys(aaaa)[0]);
              var check = aaa.find(aa => aa.includes(fileName) );
              if(check) {
                this.presentToastWithOpen(notifText, check);
              }
            }, (b) => {
              console.log("b", b);
            })
          }

          iCloudDocStorage.initUbiquitousContainer(environment.icloud, (s) => {
            iCloudDocStorage.syncToCloud(fullPath, (ss) => {
              presentToastOpener();
            }, (e) => {
              presentToastOpener();
            });
          }, (e) => {
            console.log("initUbiquitousContainer error",e)
          });

        } catch(e) {
          console.log(e);
          await this._SocialSharing.shareWithOptions({
            subject: fileName,
            files: [ path + fileName ]
          });
        }


        if(this._GDrive.user) {
          if(this._GDrive.user.email) {
            await this._GDrive.init([{
              fileName, contents: qrSet
            }], notifText);
          }
        }

      }
      else if(this.platform.is('android')) {
        await this._File.writeFile(path, fileName, new Blob([qrSet], {type: "application/json"}), { replace: true });

        try{

          await this._GDrive.init([{
            fileName, contents: qrSet
          }], notifText);
          await this.presentToast(notifText);
            
        } catch(e) {
          await this._SocialSharing.shareWithOptions({
            subject: fileName,
            files: [ path + fileName ]
          });
        }

      }

    } else {
      this.browser = this.iab.create(blobUrl, '_blank');
      
      await this._GDrive.init([{
        fileName, contents: qrSet
      }], notifText);

      await this._Dropbox.backupFiles([{
        fileName, contents: qrSet
      }], notifText);
    }

  }

  listDir(path) {
    window.resolveLocalFileSystemURL(path,
      (fileSystem) => {
        var reader = fileSystem.createReader();
        reader.readEntries( (entries) => {
            console.log(entries);
          },
          (err) => {
            console.log(err);
          }
        );
      }, (err) => {
        console.log(err);
      }
    );
  }

  listDirPromise (path: string): Promise<any> {
    return new Promise((resolve) => {
      window.resolveLocalFileSystemURL(path,
        (fileSystem) => {
          var reader = fileSystem.createReader();
          reader.readEntries( (entries) => {
              resolve(entries);
            },
            (err) => {
              console.log(err);
            }
          );
        }, (err) => {
          console.log(err);
        }
      );
    })
  }

  presentToastWithOpen(message: string, filePath: string) {
    this.toastController.create({
      position: 'top',
      duration: 3000,
      message,
      buttons: [
        {
          side: 'end',
          icon: 'folder',
          text: this.translateProviderService.instant('APPSTORE.open'),
          handler: async () => {
              this._FileOpener.open(filePath, "application/json").then((aaa) => {
                console.log("aaa", aaa);
              }).catch(bbb => {
                console.log("bbb", bbb);
              })
            }
        }
      ]
    }).then(toast => toast.present())
  }

  uploadImages(index: any, content: any) {
    this._AlertController.create({
      header: this.translateProviderService.instant('KYCMEDIABACKUP.header'),
      message: this.translateProviderService.instant('KYCMEDIABACKUP.message'),
      buttons: [
        {
          text: this.translateProviderService.instant('SETTINGS.yes'),
          handler: () => {
            this.kycMediaService.uploadMediaOnCloud(`${this.data.documentType}-${index+1}.png`, content);
          }
        },{
          text: this.translateProviderService.instant('SETTINGS.no'),
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    }).then(al => {
      al.present();
    })
    
  }

  dateTimeManager1(event: any) {
    this.saveObject.issuedate = event.detail.value;
    this.editForm.controls.issuedate.setValue(this.saveObject.issuedate);
  }
  dateTimeManager2(event: any) {
    this.saveObject.expirydate = event.detail.value;
    this.editForm.controls.expirydate.setValue(this.saveObject.expirydate);
  }

  @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        if ( !targetElement ) {
            return;
        }
        const ids = ['select-flag-6'];
        if ( ids.indexOf(targetElement.id) !== -1 ) {
            this.signService.addSelectBoxAdditionalElements();
        }
    }

}
