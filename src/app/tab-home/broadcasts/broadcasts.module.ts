import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { BroadcastsComponent } from './broadcasts.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule, 
    IonicModule, 
    SharedModule,
    TranslateModule.forChild()
    ],
  declarations: [ BroadcastsComponent ],
//   entryComponents: [ FilebackupDisclaimerComponent ],
  exports: [ BroadcastsComponent ]
})
export class BroadcastsModule {}
