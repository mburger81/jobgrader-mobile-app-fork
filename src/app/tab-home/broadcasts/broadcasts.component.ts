import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BroadcastService } from 'src/app/core/providers/broadcast/broadcast.service';

@Component({
  selector: 'app-broadcasts',
  templateUrl: './broadcasts.component.html',
  styleUrls: ['./broadcasts.component.scss'],
})
export class BroadcastsComponent  implements OnInit {

  constructor(
    public _BroadcastService: BroadcastService,
    private _ModalController: ModalController
  ) { }

  ngOnInit() {
    // this._Socket.init();
  }

  close() {
    this._ModalController.dismiss();
  }


}
