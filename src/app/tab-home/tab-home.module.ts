import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { TabHomePage } from './tab-home.page';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';
import { SharedModule } from '../shared/shared.module';
import { UserNavigationComponent } from '../user-navigation/user-navigation.component';
import { JobHeaderModule } from '../job-header/job-header.module';
import { BroadcastsModule } from './broadcasts/broadcasts.module';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthenticationGuard],
    component: TabHomePage,
    resolve: {
      user: UserProviderResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    BroadcastsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [TabHomePage, UserNavigationComponent],
  providers: [SafariViewController]
})
export class TabHomePageModule {}
