import { Component } from '@angular/core';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-feedback-thanks',
  templateUrl: './feedback-thanks.page.html',
  styleUrls: ['./feedback-thanks.page.scss'],
})
export class FeedbackThanksPage {

  constructor(
      public apiProviderService: ApiProviderService,
      public authenticationProviderService: AuthenticationProviderService,
      private nav: NavController) {
      }

  /** Callback for navigation home */
  public navigateToHome(): void {
    this.nav.navigateRoot('/dashboard');
  }

}
