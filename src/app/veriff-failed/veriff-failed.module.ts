import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { IonicModule } from '@ionic/angular';

import { SharedModule } from '../shared/shared.module';
import { VeriffFailedPage } from './veriff-failed.page';

const routes: Routes = [
  {
    path: '',
    component: VeriffFailedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [VeriffFailedPage]
})
export class VeriffFailedPageModule {}
