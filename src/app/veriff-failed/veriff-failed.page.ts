import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-veriff-failed',
  templateUrl: './veriff-failed.page.html',
  styleUrls: ['./veriff-failed.page.scss'],
})
export class VeriffFailedPage implements OnInit {

  veriffSessionId: string;
  private ngOnInitExecuted: any;

  constructor(
    private nav: NavController,
    ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  private async componentInit() {}

  navigateToHome() {
    this.nav.navigateRoot('/dashboard');
  }

}
