import { AfterViewInit, Component, ElementRef, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController, NavController, NavParams, Platform, ToastController } from '@ionic/angular';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { UtilityService } from '../core/providers/utillity/utility.service';
import { Keyboard } from '@capacitor/keyboard';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { switchMap, take } from 'rxjs/operators';
import { BiometricService } from '../core/providers/biometric/biometric.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { User } from '../core/models/User';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { Subject } from 'rxjs';
import { NetworkService } from '../core/providers/network/network-service';
import { PasswordStrengthInfo } from '../model/model/passwordStrengthInfo';
// import { ChatService } from '../core/providers/chat/chat.service';
import { SecurePinService } from '../core/providers/secure-pin/secure-pin.service';
import { CodeInputComponent } from 'angular-code-input';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { Capacitor } from '@capacitor/core';

enum LockScreenFieldNames {
    password = 'password',
    email = 'email'
}

// enum ChoicePassValues {
//     securePin = 'securePin',
//     password = 'password',
//     biometric = 'biometric'
// }

@Component({
    selector: 'app-lock-screen',
    templateUrl: './lock-screen.page.html',
    styleUrls: ['./lock-screen.page.scss'],
})
export class LockScreenPage implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('codeInput') codeInput: CodeInputComponent;

    public static LOCK_ADD_RESUME = new Subject();
    public static LOCK_REMOVE_RESUME = new Subject();
    // public securePinForm: FormGroup;
    public loginForm: UntypedFormGroup;
    // public choicePassForm: FormGroup;
    public pinVisibility = true;
    public loginModePIN: boolean = false;
    public encryptedPIN: boolean = false;
    // public choicePass = [
    //     {
    //         label: this.translateProviderService.instant('PIN.option-pin'),
    //         icon: 'lock-closed-outline',
    //         name: 'securePin',
    //         value: ChoicePassValues.securePin
    //     },
    //     {
    //         label: this.translateProviderService.instant('PIN.option-password'),
    //         icon: 'document-lock-outline',
    //         name: 'password',
    //         value: ChoicePassValues.password
    //     }
    //     // ,{
    //     //     toggle: false,
    //     //     label: 'Biometric',
    //     //     icon: 'finger-print-outline',
    //     //     name: 'biometric',
    //     //     value: ChoicePassValues.biometric
    //     // }
    // ];
    public fieldNames: typeof LockScreenFieldNames = LockScreenFieldNames;
    public fingerprintIsAvailable = false;
    public fingerPrintFailureCount = 0;
    public authProcessing = false;
    public notValidForm = false;
    public userCallname: string;
    public notUniqueUsername = false;
    private additionalStyleSheetId: string;

    displayLoadingDots = false;

    public passwordStrengthInfo: PasswordStrengthInfo = {
        password1Type: "password"
    }

    constructor(
        private router: Router,
        private userProviderService: UserProviderService,
        private toastController: ToastController,
        private translateProviderService: TranslateProviderService,
        private authenticationProviderService: AuthenticationProviderService,
        private utilityService: UtilityService,
        private elemRef: ElementRef,
        private loader: LoaderProviderService,
        private params: NavParams,
        private modalCtrl: ModalController,
        private platform: Platform,
        private biometricService: BiometricService,
        private appStateService: AppStateService,
        private navController: NavController,
        private secureStorageService: SecureStorageService,
        private crypto: CryptoProviderService,
        private _NetworkService: NetworkService,
        // public _ChatService: ChatService,
        private themeSwitcherService: ThemeSwitcherService,
        private _SecurePinService: SecurePinService
    ) {
        this.appStateService.isAuthorized = false;
    }

    /** Helper to get the abstract control */
    public get f(): {[key: string]: AbstractControl} {
        return this.loginForm.controls;
    }

    /** Callback for login */
    public async submitLogin(password: string): Promise<void> {
        const username = await this.secureStorageService.getValue(SecureStorageKey.userName, false);
        const basicAuthToken = await this.crypto.generateBasicAuthToken(username, password);
        const authTokenFromStorage = await this.secureStorageService.getValue(SecureStorageKey.basicAuthToken, false);

        this.submitLoginInternal(basicAuthToken).then(success => {
            // if (success) {
            //     if(this._NetworkService.checkConnection()) {
            //         this._ChatService.connect(true);
            //     }
            // }
        })
    }

    /** @inheritDoc */
    public ngAfterViewInit(): void {
        this.authenticationProviderService.logout();
        if (this.platform.is('android')) {
            document.addEventListener('backbutton', (event) => {
                event.preventDefault();
                return;
            }, false);
        }
        this.biometricService.biometricAvailable$().pipe(take(1))
            .subscribe(isAvailable => {
                this.fingerprintIsAvailable = isAvailable;
                if (isAvailable) {
                    this.useBiometric();
                }
            });
    }

    /** @inheritDoc */
    public async ngOnInit() {
        this.appStateService.isLocked = true;
        this.loginForm = new UntypedFormGroup({
            email: new UntypedFormControl('', [
                Validators.required,
                // Validators.pattern('^[A-Za-z0-9!?@#$%^&*)(+=._-]+$')
            ]),
            password: new UntypedFormControl('', [
                Validators.required,
                // Validators.pattern('^[A-Za-z0-9!?@#$%^&*)(+=._-]+$')
            ])
        });

        var pin = await this.secureStorageService.getValue(SecureStorageKey.securePINEncrypted, false);
        this.loginModePIN = !!pin;
        this.encryptedPIN = !!pin;

        this.authProcessing = false;
        this.markFormValidity(true);

        const tagName = this.elemRef.nativeElement.tagName.toLowerCase();
        const styles: string =
            this.utilityService.calculateWhiteCircleCssParams(`${tagName} .page-content:after`, 70);
        UtilityService.addStyleToHead(styles, this.additionalStyleSheetId = `${tagName}CircleStyle`);
        var userDataString = await this.secureStorageService.getValue(SecureStorageKey.userData, false);
        const userData: User = userDataString ? JSON.parse(userDataString) : {};
        this.userCallname = userData ? userData.callname : undefined;

    }

    /** @inheritDoc */
    public ngOnDestroy(): void {
        UtilityService.removeStyleFromHead(this.additionalStyleSheetId);
    }

    /** Callback for showing the biometric */
    public useBiometric(): void {
        console.log('LockScreenPage: useBiometric() called');
        LockScreenPage.LOCK_REMOVE_RESUME.next(0);
        console.log('LockScreenPage: publish subject to remove the resume');
        UtilityService.setTimeout(500)
            .pipe(
                switchMap(_ => this.biometricService.biometricShow()),
                take(1)
            ).subscribe((basicAuthToken) => {
                // Successfully passed the biometric
                this.submitLoginInternal(basicAuthToken).then(success => {
                    this.fingerprintIsAvailable = !success && this.fingerPrintFailureCount < 3;
                    this.fingerPrintFailureCount++;
                    // if (success) {
                    //     if(this._NetworkService.checkConnection()){
                    //         this._ChatService.connect(true);
                    //     }
                    // }
                });
            }, _ => {
                // Error while authorize via biometric
                this.fingerPrintFailureCount++;
            });
    }

    /** Callback for forget password */
    public forgotPassword(): void {
        this.appStateService.isAuthorized = false;
        this.navController.navigateRoot('/reset-password').then(() => {
            this.modalCtrl.dismiss();
        });
    }

    /** Callback for changing the user */
    public logout(): void {
        this.appStateService.isAuthorized = false;
        this.navController.navigateRoot('/login');
        this.modalCtrl.dismiss();
    }

    /** Helper to mark the form as valid */
    public markFormValidity(val: boolean): void {
        this.notValidForm = val;
    }

    private async presentToast(message: string) {
        const toast = await this.toastController.create({
            message,
            duration: 2000,
            position: 'top',
        });
        await toast.present();
    }

    private submitLoginInternal(basicAuthToken: string): Promise<boolean> {
        return new Promise(async resolve => {
            if (!this._NetworkService.checkConnection()) {
                var passwordHash = await this.secureStorageService.getValue(SecureStorageKey.passwordHash, false);
                if(!passwordHash) {
                    await this.presentToast(this.translateProviderService.instant('NETWORK.checkConnection'));
                    return;
                }
                var nonce = await this.secureStorageService.getValue(SecureStorageKey.nonce, false);
                // console.log(this.loginForm.value.password);
                if(this.crypto.generateHash(this.loginForm.value.password, nonce) == passwordHash) {
                    console.log("Offline Login Going On");
                    this._NetworkService.addOfflineFooter('ion-footer');

                    this.appStateService.isAuthorized = true;
                    this.appStateService.basicAuthToken = basicAuthToken;

                    // await this.loader.loaderCreate();
                    this.displayLoadingDots = true;

                    Capacitor.isPluginAvailable('Keyboard') && Keyboard.hide();
                    if ( this.platform.is('android') ) {
                        document.removeEventListener('backbutton', (event) => {
                            event.preventDefault();
                            return;
                        }, false);
                    }
                    if (await this.modalCtrl.getTop()) {
                        try {
                            await this.modalCtrl.dismiss();
                        } catch (e) {}
                    }
                    LockScreenPage.LOCK_ADD_RESUME.next(0);
                    // await this.loader.loaderDismiss();
                    this.displayLoadingDots = false;
                    resolve(true);
                } else {
                    await this.presentToast(this.translateProviderService.instant('LOGIN.onlyWrongPassword'));
                    return;
                }
            } else {
                console.log("Online Login Going On");
                this._NetworkService.removeOfflineFooter('ion-footer');
                this.loginForm.markAsDirty();
                this.authProcessing = true;
                // await this.loader.loaderCreate();
                this.displayLoadingDots = true;
                this.markFormValidity(true);
                if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
                this.authenticationProviderService.login(basicAuthToken, true).then(async success => {
                    // this.lockService.addResume();
                    LockScreenPage.LOCK_ADD_RESUME.next(0);
                    if (!success) {
                        // await this.loader.loaderDismiss();
                        this.displayLoadingDots = false;
                        // alert(this.translateProviderService.instant('SIGNSTEPFIVE.loginStatus'));
                        return resolve(false);
                    }
                    if ( this.platform.is('android') ) {
                        document.removeEventListener('backbutton', (event) => {
                            event.preventDefault();
                            return;
                        }, false);
                    }
                    if (await this.modalCtrl.getTop()) {
                        try {
                            await this.modalCtrl.dismiss();
                        } catch (e) {}
                    }
                    LockScreenPage.LOCK_ADD_RESUME.next(0);
                    // await this.loader.loaderDismiss();
                    this.displayLoadingDots = false;
                    resolve(true);
                }, async _ => {
                    this.markFormValidity(false);
                    this.authProcessing = false;
                    // alert(this.translateProviderService.instant('SIGNSTEPFIVE.loginStatus'));
                    // await this.loader.loaderDismiss();
                    this.displayLoadingDots = false;
                    resolve(false);
                });
            }
        });
    }

    private async submitLoginInternalSecurePin(basicAuthToken: string) {

            if (!this._NetworkService.checkConnection()) {
                console.log("Offline Login Going On");
                this._NetworkService.addOfflineFooter('ion-footer');
                // this.loginForm.markAsDirty();
                // this.authProcessing = true;

                this.appStateService.isAuthorized = true;
                this.appStateService.basicAuthToken = basicAuthToken;

                // await this.loader.loaderCreate();
                this.displayLoadingDots = true;
                // this.markFormValidity(true);
                if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
                if ( this.platform.is('android') ) {
                    document.removeEventListener('backbutton', (event) => {
                        event.preventDefault();
                        return;
                    }, false);
                }
                if (await this.modalCtrl.getTop()) {
                    try {
                        await this.modalCtrl.dismiss();
                    } catch (e) {}
                }
                LockScreenPage.LOCK_ADD_RESUME.next(0);
                // await this.loader.loaderDismiss();
                this.displayLoadingDots = false;
                return true;
            } else {
                console.log("Online Login Going On");
                this._NetworkService.removeOfflineFooter('ion-footer');
                this.loginForm.markAsDirty();
                this.authProcessing = true;
                // await this.loader.loaderCreate();
                this.displayLoadingDots = true;
                this.markFormValidity(true);
                if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
                var success = await this.authenticationProviderService.login(basicAuthToken, true);
                    // this.lockService.addResume();
                LockScreenPage.LOCK_ADD_RESUME.next(0);
                if (!success) {
                    // await this.loader.loaderDismiss();
                    this.displayLoadingDots = false;
                    // alert(this.translateProviderService.instant('SIGNSTEPFIVE.loginStatus'));
                    return false;
                }
                if ( this.platform.is('android') ) {
                    document.removeEventListener('backbutton', (event) => {
                        event.preventDefault();
                        return;
                    }, false);
                }
                if (await this.modalCtrl.getTop()) {
                    try {
                        await this.modalCtrl.dismiss();
                    } catch (e) {}
                }
                LockScreenPage.LOCK_ADD_RESUME.next(0);
                // await this.loader.loaderDismiss();
                this.displayLoadingDots = false;
                return true;

            }

    }

    togglePasswordVisibility() {
        this.passwordStrengthInfo.password1Type = (this.passwordStrengthInfo.password1Type == 'text') ? 'password' : 'text';
    }

    togglePinVisibility() {
        console.log("Papaute!");
        this.pinVisibility = !this.pinVisibility;
    }

    toggleLoginMode() {
        this.loginModePIN = !this.loginModePIN;
    }

    onCodeChanged(code: string){
        // console.log(code);
    }

    async onCodeCompleted(code: string){
        console.log("Lock Screen: onCodeCompleted");
        var authTokenFromStorage =  await this.secureStorageService.getValue(SecureStorageKey.basicAuthToken, false);
        console.log("Lock Screen: authTokenFromStorage", authTokenFromStorage);
        var encryptedPIN = await this.secureStorageService.getValue(SecureStorageKey.securePINEncrypted, false);
        console.log("Lock Screen: encryptedPIN", encryptedPIN);
        if(encryptedPIN) {
            var checkPin = await this._SecurePinService.checkPIN(code, encryptedPIN);
            console.log("Lock Screen: checkPin", checkPin);
            if(checkPin) {
                var success = await this.submitLoginInternalSecurePin(authTokenFromStorage);
                console.log("Lock Screen: success", success);
                if (success) {
                    if (!this._NetworkService.checkConnection()) {

                        this.appStateService.isAuthorized = true;
                        this.appStateService.basicAuthToken = authTokenFromStorage;

                        this.codeInput.reset();

                    } else {
                        // this._ChatService.connect(true);
                        try {
                            this.codeInput.reset();
                        } catch(e) {
                            console.log(e);
                        }

                    }
                }

            } else {
                this.codeInput.reset();
                this.presentToast(this.translateProviderService.instant('PIN.incorrectPIN'));

            }
        } else {
            this.presentToast(this.translateProviderService.instant('PIN.difMode'));
        }

    }

}
