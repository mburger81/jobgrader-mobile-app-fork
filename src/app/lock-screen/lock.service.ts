import { Injectable, OnDestroy } from '@angular/core';
import { interval, of, Subject, Subscription, timer } from 'rxjs';
import { delay, distinctUntilChanged, filter, map, take, takeUntil } from 'rxjs/operators';
import { ModalController, Platform } from '@ionic/angular';
import { LockScreenPage } from './lock-screen.page';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { NavigationEnd, Router } from '@angular/router';
import { UtilityService } from '../core/providers/utillity/utility.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { environment } from '../../environments/environment';
import { ChatService } from '../core/providers/chat/chat.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';

@Injectable()
export class LockService implements OnDestroy {
    public static BLACK_LIST = [
        'login',
        'onboarding',
        'sign-up/step-1',
        'sign-up/step-9',
        'sign-up/step-2',
        'sign-up/step-3',
        'sign-up/step-4'
    ];
    private static LOCK_AFTER_MS_INACTIVITY = 5 * 60 * 1000; // 100;

    private destroy$ = new Subject();
    private unsubscribeAll$ = new Subject();
    private resumeSubscription: Subscription;
    public resumeAdded = false;
    public current: number;
    private lockSubscription: Subscription;

    constructor(
        private modelCtrl: ModalController,
        public _ChatService: ChatService,
        private _SecureStorageService: SecureStorageService,
        private appStateService: AppStateService,
        private platform: Platform,
        private router: Router,
        private api: ApiProviderService) {
        // Readopting for platform resume
        this.addResume();
        this.appStateService.asObservable()
            .pipe(
                map(({isAuthorized}) => isAuthorized),
                distinctUntilChanged(),
                filter(isAuthorized => !isAuthorized),
                takeUntil(this.unsubscribeAll$)
            ).subscribe(_ => {
            console.log('lockService app state isAuthorized false --> Stop the lock');
            this.stop();
        });

        // Handling for login page get visible
        router.events.subscribe(val => {
            if (val instanceof NavigationEnd) {
                if (val.url.indexOf('login') > -1) {
                    this.listenAuthorize();
                }
            }
        });
        LockScreenPage.LOCK_ADD_RESUME.asObservable()
            .subscribe(_ => {
                    this.addResume();
            });
        LockScreenPage.LOCK_REMOVE_RESUME.asObservable()
            .subscribe(_ => this.removeResume());
        UtilityService.setTimeout(1000).subscribe(_ => {
            if (!this.isBlacklisted) {
                this.lock();
            }
        });
    }

    /** Handler for adding resume callback */
    public addResume(): void {
        if (!this.resumeAdded) {
            this.resumeAdded = true;
            UtilityService.setTimeout(700).subscribe(_ => {
                console.log('lockService add resume');
                if ( this.resumeSubscription ) {
                    this.resumeSubscription.unsubscribe();
                }
                this.resumeSubscription = this.platform.resume.asObservable()
                    .pipe(
                        filter(_ => !this.appStateService.isLocked && this.appStateService.isAuthorized),
                        takeUntil(this.unsubscribeAll$)
                    ).subscribe(() => {
                        this.lock();
                    });
            });
        }
    }

    /** Handler for removing resume callback */
    public removeResume(): void {
        if (this.resumeSubscription) {
            this.resumeSubscription.unsubscribe();
        }
        console.log('lockService remove resume');
        this.resumeAdded = false;
    }

    /** @inheritDoc */
    public ngOnDestroy() {
        this.unsubscribeAll$.next(0);
    }

    /** Method to start the timer */
    public async start(ms?: number): Promise<void> {
        this.appStateService.isLocked = false;
        this.current = LockService.LOCK_AFTER_MS_INACTIVITY / 1000;
        console.log('started watch current = ' + this.current);
        this.lockSubscription = interval(1000)
            .pipe(
                filter(_ => {
                    --this.current;
                    return this.current <= 0;
                }),
                filter(_ => !this.appStateService.isLocked),
                take(1),
                takeUntil(this.destroy$),
                takeUntil(this.unsubscribeAll$)
            ).subscribe(val => {
                this.lock();
            });
    }

    /** Method to manually restart the timer */
    public restart(ms?: number): void {
        this.destroy$.next(0);
        if (
            !this.appStateService.isAuthorized ||
            this.isBlacklisted
        ) {
            return;
        }
        this.start(ms);
    }

    /** Stops the timer */
    public stop() {
        console.log('lockService lock stopped');
        this.destroy$.next(0);
    }

    private async lock(): Promise<void> {
        // if(!environment.production) {
        //     return;
        // }

        if(!this.appStateService.isAuthorized) {
            return;
        }

        var lockScreenToggle = await this._SecureStorageService.getValue(SecureStorageKey.lockScreenToggle, false);

        if( lockScreenToggle == "true" ) {
            return; // if set to true then disable lock screen, enable otherwise
        }

        if (this.appStateService.isLocked ||
            this.isBlacklisted) {
            return;
        }
        console.log('lockService lock method called');        
        this.appStateService.isLocked = true;
        try {
            if (this.appStateService.isAuthorized) {
                await this.api.logout();
            }
        } catch (e) {
        }
        
        // Disconnect the chat after we lock the app, to avoid auto-reconnect attempts.
        this.appStateService.isAuthorized = false;
        this._ChatService.disconnect();


        if (this.lockSubscription) {
            console.log('lockService unsubscribing from lock subscription');
            this.lockSubscription.unsubscribe();
        }       
        try {
            const modal: HTMLIonModalElement = await this.modelCtrl.create({
                id: 'LockScreenPage',
                component: LockScreenPage,
                backdropDismiss: false,
                keyboardClose: true,
                cssClass: 'lock-screen'
            });
            await modal.present();
            console.log('lock page shown');
            this.listenAuthorize();
        } catch (e) {
            console.error('Something went wrong while creating the modal', e);
            throw new Error(e);
        }
    }

    private listenAuthorize(): void {
        of(0).pipe(delay(300), take(1)).subscribe(_ => {
            if (this.appStateService.isAuthorized === false) {
                console.log('Listening for authorization');
                this.appStateService.asObservable()
                    .pipe(
                        filter(state => state.isAuthorized),
                        take(1)
                    )
                    .subscribe(_ => {
                        console.log('Successfully authorized');
                        this.appStateService.isLocked = false;
                        console.log('appState was', this.appStateService.getValue())
                        this.restart();
                    });
            }
        });
    }

    private get isBlacklisted(): boolean {
        return LockService.BLACK_LIST.filter(b => window.location.pathname.indexOf(b) > -1).length > 0;
    }

    public radioGroupChange(event) {
        console.log(event);
    }
}
