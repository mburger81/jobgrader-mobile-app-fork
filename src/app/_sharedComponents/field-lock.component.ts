import { Component, Input, NgModule } from '@angular/core';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
// import { TooltipsModule } from 'ionic-tooltips';

@Component({
    selector: 'field-lock',
    templateUrl: './field-lock.component.html',
    styleUrls: ['./field-lock.component.scss'],
})
export class FieldLockComponent {
    @Input() property1: any;
    @Input() property2: any;
    @Input() match: boolean;

    public tooltipEvent: 'click' | 'press' | 'hover' = 'click';
    public duration: number = 3000;
    public leftPosition: string = "left";

    constructor(
        public translate: TranslateProviderService
    ) {
    }

    getToolTip() {
        return (this.property1 == null) ? this.translate.instant("TOOLTIP.na") : (this.translate.instant("TOOLTIP.status" + this.property1)).replace(/\ /g,"_");
    }

}
