import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { WalletGenerationPage } from './wallet-generation.page';


const routes: Routes = [
  {
    path: '',
    component: WalletGenerationPage,
    children: [
      {
        path: 'step-1',
        loadChildren: () => import('../wallet-generation/components/step-1/step-1.module').then(m => m.WalletGenerationStepOnePageModule)
      },
      {
        path: 'step-2',
        loadChildren: () => import('../wallet-generation/components/step-2/step-2.module').then(m => m.WalletGenerationStepTwoPageModule)
      },
      {
        path: 'step-2-1',
        loadChildren: () => import('../wallet-generation/components/step-2-1/step-2-1.module').then(m => m.WalletGenerationStepTwoOnePageModule)
      },
      {
        path: 'step-3',
        loadChildren: () => import('../wallet-generation/components/step-3/step-3.module').then(m => m.WalletGenerationStepThreePageModule)
      },
      // {
      //   path: 'step-4',
      //   loadChildren: '../wallet-generation/components/step-4/step-4.module#WalletGenerationStepFourPageModule'
      // },
      {
        path: 'step-5',
        loadChildren: () => import('../wallet-generation/components/step-5/step-5.module').then(m => m.WalletGenerationStepFivePageModule)
      },
      {
        path: 'step-4-1',
        loadChildren: () => import('../wallet-generation/components/step-4-1/step-4-1.module').then(m => m.WalletGenerationStepFourOnePageModule)
      },
      {
        path: 'step-4-2',
        loadChildren: () => import('../wallet-generation/components/step-4-2/step-4-2.module').then(m => m.WalletGenerationStepFourTwoPageModule)
      },
      {
        path: '',
        redirectTo: '/wallet-generation/step-1',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WalletGenerationPage]
})
export class WalletGenerationPageModule {}
