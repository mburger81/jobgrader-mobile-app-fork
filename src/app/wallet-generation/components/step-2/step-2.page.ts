import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';

@Component({
    selector: 'app-wallet-generation-step-2',
    templateUrl: './step-2.page.html',
    styleUrls: ['./step-2.page.scss']
})

export class WalletGenerationStepTwoPage {

    public tncURL = 'https://jobgrader.app/agb-web3-wallet-en';

    public activateButton = false;
    public legal1: boolean = false;
    public legal2: boolean = false;
    public legal3: boolean = false;

    constructor(
        private _NavController: NavController,
        private _Translate: TranslateProviderService,
    ) {
    }

    ionViewWillEnter() {
        this.legal1 = false;
        this.legal2 = false;
        this.legal3 = false;
        this.activateButton = false;

        this._Translate.getLangFromStorage().then(lang => {
            this.tncURL = (lang == 'de') ? 'https://jobgrader.app/agb-web3-wallet-de' : 'https://jobgrader.app/agb-web3-wallet-en';
        })

    }

    updateLegalCheckbox() {
        console.log(this.legal1);
        console.log(this.legal2);
        console.log(this.legal3);
        this.activateButton = (this.legal1 && this.legal2 && this.legal3);
    }

    deny() {
        // this._NavController.back();
        this._NavController.navigateBack('/wallet-generation/step-1');
    }

    confirm() {
        this._NavController.navigateForward('/wallet-generation/step-3?stage=4');
    }

}