import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
// const bip = require('bip39');

@Component({
    selector: 'app-wallet-generation-step-1',
    templateUrl: './step-1.page.html',
    styleUrls: ['./step-1.page.scss']
})

export class WalletGenerationStepOnePage {

    private source: string;
    public icon = "../../../../assets/web3_intro_1.png";

    constructor(
        private _NavController: NavController,
        private _Router: Router,
    ) {
    }

    ionViewWillEnter() {
        let checker = this._Router.parseUrl(this._Router.url).queryParams;
        this.source = !!checker.source ? checker.source : null;
    }

    deny() {
        if(this.source == 'registration') {
            this._NavController.navigateBack('/dashboard/tab-home');
        } else {
            this._NavController.navigateBack('/dashboard/payments');
        }

    }

    confirm() {
        this._NavController.navigateForward('/wallet-generation/step-2');
    }

}