import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-wallet-generation-step-2-1',
    templateUrl: './step-2-1.page.html',
    styleUrls: ['./step-2-1.page.scss']
})

export class WalletGenerationStepTwoOnePage {
    constructor(
        private _NavController: NavController
    ) {
    }

    deny() {
        // this._NavController.back();
        this._NavController.navigateBack('/wallet-generation/step-2');
    }

    confirm() {
        this._NavController.navigateForward('/wallet-generation/step-3?stage=4');
    }
}