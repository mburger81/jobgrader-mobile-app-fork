import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';

@Component({
    selector: 'app-wallet-generation-step-4-1',
    templateUrl: './step-4-1.page.html',
    styleUrls: ['./step-4-1.page.scss']
})

export class WalletGenerationStepFourOnePage {

    constructor(
        private _NavController: NavController,
        private _Router: Router,
        private _Translate: TranslateProviderService
    ) {
    }

    private seedPhrase: string;
    private bothFiles: any;
    private stage: string;
    public displayItems = [];

    public heading: string = '';
    public body: string = '';

    ionViewWillEnter() {

        this.displayItems = [];
        let checker = this._Router.parseUrl(this._Router.url).queryParams;
        this.stage = checker.stage;

        try {
            var tryparse = decodeURIComponent(checker.data);
        } catch(e) {
            tryparse = checker.data;
        }

        if(this.stage != "4" && this.stage != "5") {
            this.seedPhrase = CryptoJS.AES.decrypt(tryparse, UDIDNonce.energy).toString(CryptoJS.enc.Utf8);
        } else {
            var combinedData = CryptoJS.AES.decrypt(tryparse, UDIDNonce.energy).toString(CryptoJS.enc.Utf8);
            console.log("combinedData", combinedData);
            this.bothFiles = JSON.parse(combinedData);
            this.seedPhrase = (this.stage == "4") ? this.bothFiles.verified : this.bothFiles.unverified;
        }

        this.heading = (this.stage == "3") ? this._Translate.instant('WEB3WALLET.STEP4.heading') : this._Translate.instant(`WEB3WALLET.STEP4.heading-${this.stage}`);
        this.body = (this.stage == "3") ? this._Translate.instant('WEB3WALLET.STEP4.body-4') : this._Translate.instant(`WEB3WALLET.STEP4.body-4-${this.stage}`);

        var f = this.seedPhrase.split(" ");
        var i = 1;
        f.forEach(ff => {
            this.displayItems.push({ index: i, value: ff })
            i++;
        })
    }

    deny() {
        if(this.stage) {
            if(this.stage == "3") {
                this._NavController.navigateBack(`/dashboard/payments`);
            } if(this.stage == "5") {
                this._NavController.navigateBack(`/kyc?source=wallet`);
            } else {
                this._NavController.navigateBack(`/wallet-generation/step-3?stage=${this.stage}`);
            }
        } else {
            this._NavController.navigateBack(`/wallet-generation/step-3`);
        }
    }

    confirm() {
        // console.log(this.stage);
        // console.log(this.seedPhrase);
        // console.log(this.bothFiles);
        if(this.stage != "4" && this.stage != "5") {
            this._NavController.navigateForward(`/wallet-generation/step-4-2?stage=${this.stage}&data=${encodeURIComponent(CryptoJS.AES.encrypt(this.seedPhrase, UDIDNonce.energy).toString())}`)
        } else {
            this._NavController.navigateForward(`/wallet-generation/step-4-2?stage=${this.stage}&data=${encodeURIComponent(CryptoJS.AES.encrypt(JSON.stringify(this.bothFiles), UDIDNonce.energy).toString())}`)
        }
    }

}