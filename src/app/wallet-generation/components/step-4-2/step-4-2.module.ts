
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { WalletGenerationStepFourTwoPage } from './step-4-2.page';

const routes: Routes = [
  {
    path: '',
    component: WalletGenerationStepFourTwoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [WalletGenerationStepFourTwoPage]
})
export class WalletGenerationStepFourTwoPageModule {}
