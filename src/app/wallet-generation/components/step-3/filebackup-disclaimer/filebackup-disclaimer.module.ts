import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { FilebackupDisclaimerComponent } from './filebackup-disclaimer.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule, 
    IonicModule, 
    SharedModule,
    TranslateModule.forChild()
    ],
  declarations: [ FilebackupDisclaimerComponent ],
//   entryComponents: [ FilebackupDisclaimerComponent ],
  exports: [ FilebackupDisclaimerComponent ]
})
export class FilebackupDisclaimerModule {}
