import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-filebackup-disclaimer',
  templateUrl: './filebackup-disclaimer.component.html',
  styleUrls: ['./filebackup-disclaimer.component.scss'],
})
export class FilebackupDisclaimerComponent implements OnInit {

  constructor(
    private _ModalController: ModalController
  ) { }

  ngOnInit() {}

  close() {
    this._ModalController.dismiss({ value: true });
  }

  dismiss() {
    this._ModalController.dismiss();
  }

}
