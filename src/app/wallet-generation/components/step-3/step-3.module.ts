
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { WalletGenerationStepThreePage } from './step-3.page';
import { KeyExportModule } from 'src/app/sign-up/components/step-5/key-export/key-export.module';
import { FilebackupDisclaimerModule } from './filebackup-disclaimer/filebackup-disclaimer.module';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';

const routes: Routes = [
  {
    path: '',
    component: WalletGenerationStepThreePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    KeyExportModule,
    FilebackupDisclaimerModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [
    File,
    FileOpener
  ],
  declarations: [WalletGenerationStepThreePage]
})
export class WalletGenerationStepThreePageModule {}
