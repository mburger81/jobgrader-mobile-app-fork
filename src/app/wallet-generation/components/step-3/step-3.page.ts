import { Component } from '@angular/core';
import { ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import * as CryptoJS from 'crypto-js';
import { ethers } from 'ethers';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { KeyExportComponent } from 'src/app/sign-up/components/step-5/key-export/key-export.component';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { Router } from '@angular/router';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { ActivityKeys, UserActivitiesService } from 'src/app/core/providers/user-activities/user-activities.service';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { environment } from 'src/environments/environment';
import { GdriveService, GDriveBackupObject } from 'src/app/core/providers/cloud/gdrive.service';
import { DropboxService } from 'src/app/core/providers/cloud/dropbox.service';

const bip = require('bip39');

declare var iCloudDocStorage: any;

@Component({
    selector: 'app-wallet-generation-step-3',
    templateUrl: './step-3.page.html',
    styleUrls: ['./step-3.page.scss']
})

export class WalletGenerationStepThreePage {

    private seedPhrase: string;
    private bothSteps;
    public stage: string;

    private path;

    displayLoadingDots = false;

    constructor(
        private _NavController: NavController,
        private _ModalController: ModalController,
        private _SecureStorageService: SecureStorageService,
        private _CryptoProviderService: CryptoProviderService,
        private _ApiProviderService: ApiProviderService,
        private _ToastController: ToastController,
        private _Router: Router,
        private _File: File,
        private _GDrive: GdriveService,
        private _FileOpener: FileOpener,
        private _Platform: Platform,
        private _Translate: TranslateProviderService,
        private _InAppBrowser: InAppBrowser,
        private _Dropbox: DropboxService,
        private _LoaderProviderService: LoaderProviderService,
        private _UserActivity: UserActivitiesService,
    ) {
    }

    public ionViewWillEnter() {
        let checker = this._Router.parseUrl(this._Router.url).queryParams;
        this.stage = checker.stage;
        if(this.stage != "4") {
            this.seedPhrase = bip.generateMnemonic();
        } else {
            this.bothSteps = {
                verified: bip.generateMnemonic(),
                unverified: bip.generateMnemonic()
            }
        }
    }

    deny() {
        // this._NavController.back();
        this._NavController.navigateBack('/wallet-generation/step-2');
    }

    confirm() {
        console.log('confirm!');
    }

    async cloudBackup() {

        if(this.stage != "4") {
            this._ModalController.create({
                component: KeyExportComponent,
                componentProps: {
                    data: this.seedPhrase,
                    new: true,
                    exception: true
                }
            }).then(modal => {
                modal.onDidDismiss().then(async (data) => {
                    console.log(data);
                    var wallet = ethers.Wallet.fromMnemonic(this.seedPhrase);
                    console.log(this.stage);
                    if(this.stage == "1") {
                        await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, this.seedPhrase);
                        await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, wallet.address);
                        await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, wallet.privateKey);
                        await this._ApiProviderService.saveUserconsent({
                            web3wallet_lostinfo: true,
                            web3wallet_exposeinfo: true,
                            web3wallet_secretinfo: true,
                            web3wallet_activationdate: +new Date()
                        }).catch(e => {
                            console.log('Error with the saveUserconsent endpoint');
                            console.log(e);
                        })
                        await this._ApiProviderService.saveUserWeb3WalletAddress(wallet.address).catch(e => {
                            console.log('Error with the saveUserWeb3WalletAddress endpoint');
                            console.log(e);
                        });
                        this._NavController.navigateForward(`/wallet-generation/step-5?mode=file&stage=1&status=cloud`);
                    } else if(this.stage == "2") {
                        var im = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
                        var imW = !!im ? JSON.parse(im): [];
                        var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
                        var random = Math.floor(Math.random() * colorschemes.length);
                        var color = colorschemes[random];
                        imW.push({
                            heading: 'Unverified Wallet',
                            mnemonic: this.seedPhrase,
                            publicKey: wallet.address,
                            creationDate: +new Date(),
                            color
                        })
                        await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(imW));
                        this._NavController.navigateForward(`/wallet-generation/step-5?mode=file&stage=2&status=cloud`);
                    }

                })
                modal.present();
            })
        } else {

            // await this._LoaderProviderService.loaderCreate();
            this.displayLoadingDots = true;

            var verifiedWallet = ethers.Wallet.fromMnemonic(this.bothSteps.verified);
            var unverifiedWallet = ethers.Wallet.fromMnemonic(this.bothSteps.unverified);

            var verifiedWalletAddress = verifiedWallet.address;
            var unverifiedWalletAddress = unverifiedWallet.address;

            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, this.bothSteps.verified);
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, verifiedWalletAddress);
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, verifiedWallet.privateKey);
            await this._ApiProviderService.saveUserconsent({
                web3wallet_lostinfo: true,
                web3wallet_exposeinfo: true,
                web3wallet_secretinfo: true,
                web3wallet_activationdate: +new Date()
            }).catch(e => {
                console.log('Error with the saveUserconsent endpoint');
                console.log(e);
            })
            await this._ApiProviderService.saveUserWeb3WalletAddress(verifiedWalletAddress).catch(e => {
                console.log('Error with the saveUserWeb3WalletAddress endpoint');
                console.log(e);
            });

            // var im = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
            // var imW = !!im ? JSON.parse(im): [];
            var imW = [];
            var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
            var random = Math.floor(Math.random() * colorschemes.length);
            var color = colorschemes[random];
            imW.push({
                heading: 'Unverified Wallet',
                mnemonic: this.bothSteps.unverified,
                publicKey: unverifiedWalletAddress,
                creationDate: +new Date(),
                color
            })
            await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(imW));

            var username = await this._SecureStorageService.getValue(SecureStorageKey.userName, false);

            var verifiedWalletAddressFileName = `helixid-userkey-${username}-${verifiedWalletAddress}.json`;
            var unverifiedWalletAddressFileName = `helixid-userkey-${username}-${unverifiedWalletAddress}.json`;

            var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();

            var verifiedWalletEncryptedMnemonic1 = CryptoJS.AES.encrypt(JSON.stringify(this.bothSteps.verified), backupSymmetricKey).toString();
            var verifiedWalletEncryptedMnemonic2 = JSON.stringify(CryptoJS.AES.encrypt(verifiedWalletEncryptedMnemonic1, UDIDNonce.userKey).toString());
            var blobUrl1 = URL.createObjectURL(new Blob([verifiedWalletEncryptedMnemonic2], {type: "application/json"}));

            var unverifiedWalletEncryptedMnemonic1 = CryptoJS.AES.encrypt(JSON.stringify(this.bothSteps.unverified), backupSymmetricKey).toString();
            var unverifiedWalletEncryptedMnemonic2 = JSON.stringify(CryptoJS.AES.encrypt(unverifiedWalletEncryptedMnemonic1, UDIDNonce.userKey).toString());
            var blobUrl2 = URL.createObjectURL(new Blob([unverifiedWalletEncryptedMnemonic2], {type: "application/json"}));

            if( this._Platform.is('ios') && this._Platform.is('hybrid') ) {
                this.path = this._File.documentsDirectory;
            } else if ( this._Platform.is('android') && this._Platform.is('hybrid') ) {
                this.path = this._File.externalDataDirectory;
            }

            const openModal = async () => {
                const modal = await this._ModalController.create({
                    component: KeyExportComponent,
                    componentProps: {
                        stage: this.stage,
                        bothFiles: this.bothSteps,
                        new: true,
                        exception: true
                    }
                })
                modal.onDidDismiss().then(() => {
                    this._NavController.navigateForward(`/wallet-generation/step-5?stage=${this.stage}&mode=file&status=local`);
                })
                await modal.present();
            }

            var verifiedWalletAddressFullPath = this.path + verifiedWalletAddressFileName;
            var unverifiedWalletAddressFullPath = this.path + unverifiedWalletAddressFileName;

            if(this._Platform.is('hybrid')) {
                if(this._Platform.is('ios')) {
                    await this._File.writeFile(this.path, verifiedWalletAddressFileName, verifiedWalletEncryptedMnemonic2, { replace: true });
                    await this._File.writeFile(this.path, unverifiedWalletAddressFileName, unverifiedWalletEncryptedMnemonic2, { replace: true });


                    try {
                        var presentToastOpener = (fileName) => {
                            iCloudDocStorage.fileList("Cloud", (a) => {
                            var aaa = Array.from(a, aaaa => Object.keys(aaaa)[0]);
                            var check = aaa.find(aa => aa.includes(fileName) );
                            if(check) {
                                this.presentToastWithOpen(this._Translate.instant('BACKUP.cloud-local'), check);
                            }
                            }, (b) => {
                            console.log("b", b);
                            })
                        }

                        iCloudDocStorage.initUbiquitousContainer(environment.icloud, (s) => {
                        iCloudDocStorage.syncToCloud(verifiedWalletAddressFullPath, async (ss) => {
                            await this._UserActivity.updateActivity(ActivityKeys.VERIFIED_BACKUP_CLOUD);
                            iCloudDocStorage.syncToCloud(unverifiedWalletAddressFullPath, async (sss) => {
                                await this._UserActivity.updateActivity(ActivityKeys.UNVERIFIED_BACKUP_CLOUD);

                                // await this._LoaderProviderService.loaderDismiss();
                                // this.displayLoadingDots = false;

                                presentToastOpener(verifiedWalletAddressFileName);
                                this._NavController.navigateForward(`/wallet-generation/step-5?stage=${this.stage}&mode=file&status=cloud`);
                                }, (e) => {
                                    console.log(e);
                                });
                            }, async (e) => {
                                console.log(e);
                                await this._UserActivity.updateActivity(ActivityKeys.VERIFIED_BACKUP_LOCAL);
                                await this._UserActivity.updateActivity(ActivityKeys.UNVERIFIED_BACKUP_LOCAL);

                                // await this._LoaderProviderService.loaderDismiss();
                                // this.displayLoadingDots = false;

                                this.presentToast(this._Translate.instant('BACKUP.only-local'));
                                await openModal();
                            });
                        }, async (e) => {
                            console.log(e);
                            await this._UserActivity.updateActivity(ActivityKeys.VERIFIED_BACKUP_LOCAL);
                            await this._UserActivity.updateActivity(ActivityKeys.UNVERIFIED_BACKUP_LOCAL);

                            // await this._LoaderProviderService.loaderDismiss();
                            // this.displayLoadingDots = false;

                            this.presentToast(this._Translate.instant('BACKUP.only-local'));
                            await openModal();
                        });

                    } catch(e) {
                        console.log(e);
                        await this._UserActivity.updateActivity(ActivityKeys.VERIFIED_BACKUP_LOCAL);
                        await this._UserActivity.updateActivity(ActivityKeys.UNVERIFIED_BACKUP_LOCAL);

                        // await this._LoaderProviderService.loaderDismiss();
                        // this.displayLoadingDots = false;

                        this.presentToast(this._Translate.instant('BACKUP.only-local'));
                        await openModal();
                    }

                    if(this._GDrive.user) {
                        if(this._GDrive.user.email) {
                            this._GDrive.init([{
                                fileName: verifiedWalletAddressFileName,
                                contents: verifiedWalletEncryptedMnemonic2
                            },{
                                fileName: unverifiedWalletAddressFileName,
                                contents: unverifiedWalletEncryptedMnemonic2
                            }], this._Translate.instant('CLOUDBACKUP.wallet-success'));
                        }
                    }

                }
            else if(this._Platform.is('android')) {
                try{
                    await this._File.writeFile(this.path, verifiedWalletAddressFileName, verifiedWalletEncryptedMnemonic2, { replace: true });
                    await this._File.writeFile(this.path, unverifiedWalletAddressFileName, unverifiedWalletEncryptedMnemonic2, { replace: true });
                    await this._UserActivity.updateActivity(ActivityKeys.VERIFIED_BACKUP_LOCAL);
                    await this._UserActivity.updateActivity(ActivityKeys.UNVERIFIED_BACKUP_LOCAL);
                    
                    this._GDrive.init([{
                        fileName: verifiedWalletAddressFileName,
                        contents: verifiedWalletEncryptedMnemonic2
                    },{
                        fileName: unverifiedWalletAddressFileName,
                        contents: unverifiedWalletEncryptedMnemonic2
                    }], this._Translate.instant('CLOUDBACKUP.wallet-success'));
                    
                    // this.presentToast(this._Translate.instant('BACKUP.only-local'));
                } catch(e) {
                    console.log(e);
                }

                await openModal();

                }
            } else {
                var browser1 = this._InAppBrowser.create(blobUrl1, '_system');
                var browser2 = this._InAppBrowser.create(blobUrl2, '_system');

                await this._UserActivity.updateActivity(ActivityKeys.VERIFIED_BACKUP_CLOUD);
                await this._UserActivity.updateActivity(ActivityKeys.UNVERIFIED_BACKUP_CLOUD);


                this._GDrive.init([{
                    fileName: verifiedWalletAddressFileName,
                    contents: verifiedWalletEncryptedMnemonic2
                }, {
                    fileName: unverifiedWalletAddressFileName,
                    contents: unverifiedWalletEncryptedMnemonic2
                }], this._Translate.instant('CLOUDBACKUP.wallet-success'));

                // await this._LoaderProviderService.loaderDismiss();

                this._NavController.navigateForward(`/wallet-generation/step-5?stage=${this.stage}&mode=file&status=cloud`);
            }

            this._Dropbox.backupFiles([{
                fileName: verifiedWalletAddressFileName,
                contents: verifiedWalletEncryptedMnemonic2
            },{
                fileName: unverifiedWalletAddressFileName,
                contents: unverifiedWalletEncryptedMnemonic2
            }], this._Translate.instant('CLOUDBACKUP.wallet-success'));

        }


    }

    paperBackup() {
        if(this.stage != "4") {
            this._NavController.navigateForward(`/wallet-generation/step-4-1?stage=${this.stage}&data=${encodeURIComponent(CryptoJS.AES.encrypt(this.seedPhrase, UDIDNonce.energy).toString())}`);
        } else {
            this._NavController.navigateForward(`/wallet-generation/step-4-1?stage=${this.stage}&data=${encodeURIComponent(CryptoJS.AES.encrypt(JSON.stringify(this.bothSteps), UDIDNonce.energy).toString())}`);
        }
    }

    presentToast(message: string) {
        this._ToastController.create({
            message,
            duration: 2000,
            position: 'top'
        }).then(toast => {
            toast.present();
        })
    }

    presentToastWithOpen(message: string, filePath: string) {
        this._ToastController.create({
          position: 'top',
          duration: 5000,
          message,
          buttons: [
            {
              side: 'end',
              icon: 'folder',
              text: this._Translate.instant('APPSTORE.open'),
              handler: () => {
                this._FileOpener.open(filePath, "application/json").then((aaa) => {
                    console.log("aaa", aaa);
                  }).catch(bbb => {
                    console.log("bbb", bbb);
                  })
              },
            }
          ]
        }).then(toast => toast.present())
      }

}