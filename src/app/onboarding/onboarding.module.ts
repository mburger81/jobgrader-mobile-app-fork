import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
import { StreamingMedia } from '@awesome-cordova-plugins/streaming-media/ngx';
import { OnboardingPage } from './onboarding.page';
import { TrackingPermissionModule } from  './tracking-permission/tracking-permission.module';

const routes: Routes = [
  {
    path: '',
    component: OnboardingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IonicModule,
    SharedModule,
    TrackingPermissionModule,
    TranslateModule.forChild(),
  ],
  providers: [NativeAudio, StreamingMedia],
  declarations: [OnboardingPage]
})
export class OnboardingPageModule {}
