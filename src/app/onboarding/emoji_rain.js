function emojiRain() {}

emojiRain.prototype.start = function start() {    
    const rand = (m, M) => Math.random() * (M - m) + m,
        PI = Math.PI,
        TAU = PI * 2,
        width = window.innerWidth,
        height = window.innerHeight,
        ctx = document.getElementById('cvs').getContext('2d'),
        items = [],
        Item = function () {
            this.h = 32;
            this.w = 32;
            this.IMG = new Image();
            this.IMG.src = 'https://i.postimg.cc/W4K55yds/smiling-face-with-sunglasses-1f60e-32.png';
            this.start();
            return this;
        };

    Item.prototype.start = function () {
        this.x = rand(0, width - this.w / 2);
        this.y = rand(0, height);
        this.angle = rand(0, TAU);
        this.speed = rand(0.01, 0.05);
    }

    Item.prototype.move = function () {
        if (this.y < 0) {
            this.start();
            this.y = +this.h + height;
        }
        this.y -= this.speed / 0.015;
        this.angle += this.speed;
        this.angle %= TAU;

        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.angle);
        ctx.drawImage(this.IMG, -this.w / 2, -this.h / 2);
        ctx.restore();
    };

    ctx.canvas.width = width;
    ctx.canvas.height = height;

    for (var i = 0; i < 20; i++) items.push(new Item());

    (function loop() {
        ctx.clearRect(0, 0, width, height);
        items.forEach(Item => Item.move());
        requestAnimationFrame(loop);
      }());
};
var rainModule = new emojiRain();

export { rainModule };