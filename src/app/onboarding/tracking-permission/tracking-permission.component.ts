import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { AdjustService } from '../../core/providers/adjust/adjust.service';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';
import { TranslateProviderService } from '../../core/providers/translate/translate-provider.service';
import { App } from '@capacitor/app';

declare var cordova: any;

@Component({
  selector: 'app-tracking-permission',
  templateUrl: './tracking-permission.component.html',
  styleUrls: ['./tracking-permission.component.scss'],
})
export class TrackingPermissionComponent implements OnInit {

  public toggle: boolean = false;

  constructor(
    private _ModalController: ModalController,
    private _AdjustService: AdjustService,
    private _TranslateProviderService: TranslateProviderService,
    private _OpenNativeSettings: OpenNativeSettings,
    private _AlertController: AlertController
  ) { }

  async ngOnInit() {
    this.toggle = await this._AdjustService.isUserYetToAllowTracking();
  }

  close() {
    this._ModalController.dismiss();
  }

  async allowPermission() {
    console.log('Asking for permission');
    this.toggle = await this._AdjustService.isUserYetToAllowTracking();
    console.log("allowPermission: " + this.toggle);
    if(this.toggle){
      this._AdjustService.snippet();
      this.close();
    } else {
      this.presentAlertConfirm();
    }
    // this.toggle = await this._AdjustService.isUserYetToAllowTracking();
  }

  public presentAlertConfirm() {
    this._AlertController.create({
        mode: 'ios',
        header: this._TranslateProviderService.instant('USERTRACKING.tracking-permission-false.header'),
        message: this._TranslateProviderService.instant('USERTRACKING.tracking-permission-false.message'),
        buttons: [
        {
            text: this._TranslateProviderService.instant('USERTRACKING.tracking-permission-false.settings'),
            handler: () => {
                this._OpenNativeSettings.open('tracking')
                .then(_ => { App.exitApp(); } )
                .catch(e => {
                  console.log(e)
                  this._OpenNativeSettings.open('privacy')
                  .then(_ => { App.exitApp(); } )
                  .catch(e => {
                    console.log(e)
                  })
                })
            }
        },
        {
            text: this._TranslateProviderService.instant('USERTRACKING.tracking-permission-false.cancel'),
            role: 'cancel',
            handler: () => {
              this.close();
            }
        }]
    }).then(alert => alert.present())
}

}
