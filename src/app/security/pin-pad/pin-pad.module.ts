import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { PinPadComponent } from './pin-pad.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { TooltipsModule } from 'ionic-tooltips';
import { CodeInputModule } from 'angular-code-input';

@NgModule({
    imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    // TooltipsModule,
    SharedModule,
    CodeInputModule,
    TranslateModule.forChild()
  ],
  declarations: [PinPadComponent],
//   entryComponents: [PinPadComponent],
  exports: [PinPadComponent]
})
export class PinPadModule {}
