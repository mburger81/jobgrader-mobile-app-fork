import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { PinPadModule } from './pin-pad/pin-pad.module';
import { SecurityPage } from './security.page';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
    {
        path: '',
        component: SecurityPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PinPadModule,
        RouterModule.forChild(routes),
        TranslateModule,
        JobHeaderModule,
        SharedModule
    ],
    declarations: [SecurityPage]
})
export class SecurityPageModule {
}
