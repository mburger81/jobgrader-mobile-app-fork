import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { WalletSettingsPage } from './wallet-settings.page';
import { SharedModule } from '../../shared/shared.module';
import { KeyExportModule } from 'src/app/sign-up/components/step-5/key-export/key-export.module';
import { ImportUserKeyModule } from 'src/app/login/import-userkey/import-userkey.module';
import { TranslateModule } from '@ngx-translate/core';
import { ShowSeedModule } from './show-seed/show-seed.module';
import { ReceiveCryptoModule } from '../crypto-account-page/receive-crypto/receive-crypto.module';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    component: WalletSettingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    KeyExportModule,
    TranslateModule,
    ImportUserKeyModule,
    ReceiveCryptoModule,
    ShowSeedModule,
    JobHeaderModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    File,
    FileOpener,
  ],
  declarations: [WalletSettingsPage]
})
export class WalletSettingsPageModule {}
