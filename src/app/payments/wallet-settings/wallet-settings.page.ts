import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import * as Identicon from 'identicon.js';
import { UserProviderService } from 'src/app/core/providers/user/user-provider.service';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { KeyExportComponent } from 'src/app/sign-up/components/step-5/key-export/key-export.component';
import { ImportUserkeyComponent } from 'src/app/login/import-userkey/import-userkey.component';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { ethers } from 'ethers';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { ShowSeedComponent } from './show-seed/show-seed.component';
import { ReceiveCryptoComponent } from '../crypto-account-page/receive-crypto/receive-crypto.component';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { environment } from 'src/environments/environment';
import { UserPhotoServiceAkita } from '../../core/providers/state/user-photo/user-photo.service';
import { GdriveService } from 'src/app/core/providers/cloud/gdrive.service';
import { DropboxService } from 'src/app/core/providers/cloud/dropbox.service';

declare var iCloudDocStorage: any;

@Component({
  selector: 'app-wallet-settings',
  templateUrl: './wallet-settings.page.html',
  styleUrls: ['./wallet-settings.page.scss'],
})
export class WalletSettingsPage {

  public powerUserMode: boolean = false;

  public profilePictureSrc;
  private checker;
  private mnemonic;
  private privateKey;
  public heading = '';

  public activateCloudBackup = false;
  public activateFileBackup = false;
  public activatePaperBackup = false;
  public activateSeedphrase = false;
  public activateRecoverWallet = false;
  public activateRenameWallet = false;
  public activateDeleteWallet = false;
  public activatemakeIDTetheredWallet = false;
  public activateCopyAddress = true;
  public activatePrivatekey = false;

  private primary = false;

  constructor(
    private _Router: Router,
    private _NavController: NavController,
    private _AlertController: AlertController,
    private _Translate: TranslateProviderService,
    private _SecureStorageService: SecureStorageService,
    private _ToastController: ToastController,
    private _ModalController: ModalController,
    private _UserProviderService: UserProviderService,
    private _ApiProviderService: ApiProviderService,
    private _CryptoProviderService: CryptoProviderService,
    private _Platform: Platform,
    private _File: File,
    private _Dropbox: DropboxService,
    private _FileOpener: FileOpener,
    private _GDrive: GdriveService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
    this.checker = this._Router.parseUrl(this._Router.url).queryParams;
   }

  public async ionViewWillEnter() {
      console.log(this.checker);
      var powerUserMode = await this._SecureStorageService.getValue(SecureStorageKey.powerUserMode, false);
      this.powerUserMode = (powerUserMode == 'true');
      var userData = await this._UserProviderService.getUser();
      var web3WalletPublicKey = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
      
      if(web3WalletPublicKey == this.checker.address) {
        this.heading = `${userData.callname}`;
        var photo = this.userPhotoServiceAkita.getPhoto();
        this.profilePictureSrc = !!photo ? photo : '../../../assets/job-user.svg';
        var web3WalletMnemonic = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false);
        if(web3WalletMnemonic) {
          this.activateCloudBackup = true;
          this.activateFileBackup = true;
          this.activatePaperBackup = true;
          this.activateSeedphrase = true;
          this.activatePrivatekey = true;
          this.primary = true;
          this.mnemonic = web3WalletMnemonic;
          this.privateKey = ethers.Wallet.fromMnemonic(web3WalletMnemonic).privateKey;
        } else {
          this.activateRecoverWallet = true;
        }
      } else {
        this.activateRenameWallet = true;
        this.activateDeleteWallet = true;
        this.profilePictureSrc =`data:image/png;base64,${new Identicon(this.checker.address, 420).toString()}`;
        var im = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
        var imW = !!im ? JSON.parse(im) : [];
        var check = imW.find(k => k.publicKey == this.checker.address);
        if(check) {
          this.heading = check.heading;
          if(!!check.mnemonic) {
            this.activateCloudBackup = true;
            this.activateFileBackup = true;
            this.activatePaperBackup = true;
            this.activateSeedphrase = true;
            this.mnemonic = check.mnemonic;
            this.activatemakeIDTetheredWallet = true;
            this.activatePrivatekey = true;
            this.privateKey = ethers.Wallet.fromMnemonic(check.mnemonic).privateKey;
          }
          //  else {
          //   this.activateRecoverWallet = true;
          // }
        }
      }
  }

  showPrivatekey() {
    console.log("showPrivatekey()");
    this._AlertController.create({
      message: this._Translate.instant('EXPORTKEY.alert-heading-modified'),
      inputs: [{
        type: 'password',
        placeholder: "...",
        name: 'password'
      }],
      buttons: [
        {
          text: this._Translate.instant('BUTTON.CANCEL'),
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: this._Translate.instant('GENERAL.ok'),
          handler: async (data) => {
            var plaintextPassword = data.password;
            var passwordHash = await this._SecureStorageService.getValue(SecureStorageKey.passwordHash, false);
            var nonce = await this._SecureStorageService.getValue(SecureStorageKey.nonce, false);
            if(this._CryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {
              this._ModalController.create({
                component: ShowSeedComponent,
                componentProps: {
                  privateKey: this.privateKey,
                  mnemonic: null,
                  heading: this.heading,
                  publicKey: this.checker.address
                }
              }).then(modal => {
                modal.onDidDismiss().then(() => {})
                modal.present();
              })
            } else {

              this.presentToast(this._Translate.instant('EXPORTKEY.incorrect-password'));
            }
          }
        }
      ]
    }).then(alerti => alerti.present())
  }

  showSeed() {
    this._AlertController.create({
      message: this._Translate.instant('EXPORTKEY.alert-heading-modified'),
      inputs: [{
        type: 'password',
        placeholder: "...",
        name: 'password'
      }],
      buttons: [
        {
          text: this._Translate.instant('BUTTON.CANCEL'),
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: this._Translate.instant('GENERAL.ok'),
          handler: async (data) => {
            var plaintextPassword = data.password;
            var passwordHash = await this._SecureStorageService.getValue(SecureStorageKey.passwordHash, false);
            var nonce = await this._SecureStorageService.getValue(SecureStorageKey.nonce, false);
            if(this._CryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {
              this._ModalController.create({
                component: ShowSeedComponent,
                componentProps: {
                  privateKey: null,
                  mnemonic: this.mnemonic,
                  heading: this.heading,
                  publicKey: this.checker.address
                }
              }).then(modal => {
                modal.onDidDismiss().then(() => {})
                modal.present();
              })
            } else {

              this.presentToast(this._Translate.instant('EXPORTKEY.incorrect-password'));
            }
          }
        }
      ]
    }).then(alerti => alerti.present())
  }

  async cloudBackup() {
    var wallet = ethers.Wallet.fromMnemonic(this.mnemonic);
    var username = await this._SecureStorageService.getValue(SecureStorageKey.userName, false);
    var userPublicKey = wallet.address;
    var fileName = `helixid-userkey-${username}-${userPublicKey}.json`;
    

    var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();
    var encryptedMnemonic1 = CryptoJS.AES.encrypt(JSON.stringify(this.mnemonic), backupSymmetricKey).toString();
    var encryptedMnemonic2 = JSON.stringify(CryptoJS.AES.encrypt(encryptedMnemonic1, UDIDNonce.userKey).toString());
    // var blobUrl = URL.createObjectURL(new Blob([encryptedMnemonic2], {type: "application/json"}));

    if(this._Platform.is('ios') && this._Platform.is('hybrid')) {
      var path = this._File.documentsDirectory;
      var fullPath = path + fileName;

      await this._File.writeFile(path, fileName, encryptedMnemonic2, { replace: true });

      try {
        var presentToastOpener = () => {
          iCloudDocStorage.fileList("Cloud", (a) => {
            var aaa = Array.from(a, aaaa => Object.keys(aaaa)[0]);
            var check = aaa.find(aa => aa.includes(fileName) );
            if(check) {
              this.presentToastWithOpen(this._Translate.instant('CLOUDBACKUP.wallet-success'), check);
            }
          }, (b) => {
            console.log("b", b);
          })
        }
  
        iCloudDocStorage.initUbiquitousContainer(environment.icloud, (s) => {
          iCloudDocStorage.syncToCloud(fullPath, (s) => {
            presentToastOpener();
          }, (e) => {
            presentToastOpener();
          });
        }, (e) => {
          console.log(e)
        });
      } catch(e) {
        console.log(e);
      }

      if(this._GDrive.user) {
        if(this._GDrive.user.email) {
          this._GDrive.init([{
            fileName, contents: encryptedMnemonic2
          }], this._Translate.instant('CLOUDBACKUP.wallet-success')); 
        }
      }

    } else {

      if(this._Platform.is('hybrid') && this._Platform.is('android')) {
        
        var path = this._File.dataDirectory;
        var fullPath = path + fileName;

        await this._File.writeFile(path, fileName, encryptedMnemonic2, { replace: true });
      
      } 

      this._GDrive.init([{
        fileName, contents: encryptedMnemonic2
      }], this._Translate.instant('CLOUDBACKUP.wallet-success'));

    }

    this._Dropbox.backupFiles([{
      fileName, contents: encryptedMnemonic2
    }], this._Translate.instant('CLOUDBACKUP.wallet-success'))

  
}

  presentToastWithOpen(message: string, filePath: string) {
    this._ToastController.create({
      position: 'top',
      duration: 3000,
      message,
      buttons: [
        {
          side: 'end',
          icon: 'folder',
          text: this._Translate.instant('APPSTORE.open'),
          handler: () => {
            this._FileOpener.open(filePath, "application/json").then((aaa) => {
              console.log("aaa", aaa);
            }).catch(bbb => {
              console.log("bbb", bbb);
            })
          },
        }
      ]
    }).then(toast => toast.present())
  }

  goBack() {
    // this._NavController.back();
    this._NavController.navigateBack(`/dashboard/payments/crypto-account-page?data=${encodeURIComponent(
      JSON.stringify({
        account: {
          heading: this.heading,
          primary: this.primary,
          publicKey: this.checker.address,
        },
      })
    )}`);
  }

  goToPayments() {
    this._NavController.navigateBack(`/dashboard/payments`);
  }


  copyAddress() {
    this._ModalController.create({
      component: ReceiveCryptoComponent,
      componentProps: {
        data: this.checker.address
      }
    }).then(modal => {
      modal.onDidDismiss().then(() => {

      })
      modal.present();
    })
  }

  async makeIDTetheredWallet() {

    await this._ApiProviderService.checkForIDLinkWalletUpdate(this.checker.address);
    this.goBack();

  }

  fileBackup() {
    this._AlertController.create({
      message: this._Translate.instant('EXPORTKEY.alert-heading-modified'),
      inputs: [{
        type: 'password',
        placeholder: "...",
        name: 'password'
      }],
      buttons: [
        {
          text: this._Translate.instant('BUTTON.CANCEL'),
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: this._Translate.instant('GENERAL.ok'),
          handler: async (data) => {
            var plaintextPassword = data.password;
            var passwordHash = await this._SecureStorageService.getValue(SecureStorageKey.passwordHash, false);
            var nonce = await this._SecureStorageService.getValue(SecureStorageKey.nonce, false);
            if(this._CryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {
              this._ModalController.create({
                component: KeyExportComponent,
                componentProps: {
                  data: this.mnemonic
                }
              }).then(modal => {
                modal.onDidDismiss().then(() => {})
                modal.present();
              })
            } else {

              this.presentToast(this._Translate.instant('EXPORTKEY.incorrect-password'));
            }
          }
        }
      ]
    }).then(alerti => alerti.present())

  }

  paperBackup() {

    this._AlertController.create({
      message: this._Translate.instant('EXPORTKEY.alert-heading-modified'),
      inputs: [{
        type: 'password',
        placeholder: "...",
        name: 'password'
      }],
      buttons: [
        {
          text: this._Translate.instant('BUTTON.CANCEL'),
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: this._Translate.instant('GENERAL.ok'),
          handler: async (data) => {
            var plaintextPassword = data.password;
            var passwordHash = await this._SecureStorageService.getValue(SecureStorageKey.passwordHash, false);
            var nonce = await this._SecureStorageService.getValue(SecureStorageKey.nonce, false);
            if(this._CryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {
              this._NavController.navigateForward(`/wallet-generation/step-4-1?stage=3&data=${encodeURIComponent(CryptoJS.AES.encrypt(this.mnemonic, UDIDNonce.energy).toString())}`);
            } else {

              this.presentToast(this._Translate.instant('EXPORTKEY.incorrect-password'));
            }
          }
        }
      ]
    }).then(alerti => alerti.present())
  }

  recoverWallet() {
    console.log("recoverWallet()");
    this._ModalController.create({
        component: ImportUserkeyComponent,
        componentProps: {
            initial: true
        }
    }).then(modal => {
      modal.onDidDismiss().then(async (data) => {
        if(data.data.value) {
            this._NavController.navigateForward('/dashboard/payments');
        }
      })
      modal.present();
    })

  }

  async renameWallet() {
    console.log("renameWalelt()");
    const alert = await this._AlertController.create({
      mode: 'ios',
      header: this._Translate.instant('WALLET.renameWallet'),
      message: this._Translate.instant('WALLET.renameWalletConfirm'),
      inputs: [{ type: 'text', placeholder: 'Satoshi...', name: 'name' }],
      buttons: [{
        text: this._Translate.instant('SETTINGS.yes'),
        handler: async (data) => {
          var wallets = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
          var walletsParse = !!wallets ? JSON.parse(wallets) : [];

          var headings = Array.from(walletsParse, k => (k as any).heading);
          var check = walletsParse.find(w => w.publicKey == this.checker.address);

          if(check) {

            if(!data.name || data.name == '') {
              this.presentToast(this._Translate.instant('WALLETSETTINGS.errors.address-changed'));
              return;
            }

            if(headings.includes(data.name)) {
              this.presentToast(this._Translate.instant('WALLETSETTINGS.errors.name-used'));
              return;
            }

            if(data.name.trim().length > 20) {
              this.presentToast(this._Translate.instant('WALLETSETTINGS.errors.invalid-length'));
              return;
            }

            walletsParse.find(w => w.publicKey == this.checker.address).heading = data.name;

            await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(walletsParse));
            this.presentToast(this._Translate.instant('WALLET.walletRenamed'));
            this.goBack();
          }


        }
      },{
        text: this._Translate.instant('SETTINGS.no'),
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await alert.present();
  }

  async deleteWallet() {
    const alert = await this._AlertController.create({
      mode: 'ios',
      header: this._Translate.instant('WALLET.deleteWallet'),
      message: this._Translate.instant('WALLET.deleteWalletConfirm'),
      buttons: [{
        text: this._Translate.instant('SETTINGS.yes'),
        handler: async () => {
          var wallets = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
          var walletsParse = !!wallets ? JSON.parse(wallets) : [];
          var filtered = walletsParse.filter(w => w.publicKey !== this.checker.address);
          await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(filtered));
          this.presentToast(this._Translate.instant('WALLET.walletDeleted'));
          this.goToPayments();
        }
      },{
        text: this._Translate.instant('SETTINGS.no'),
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await alert.present();
  }

  presentToast(message: string) {
    this._ToastController.create({
      message,
      position: 'top',
      duration: 2000
    }).then(toast => toast.present())
  }

}
