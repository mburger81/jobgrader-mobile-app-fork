import { Component, Input, OnInit } from '@angular/core';
import { Clipboard } from '@capacitor/clipboard';
import { ModalController, ToastController } from '@ionic/angular';
import { Subscription, interval } from 'rxjs';
import { ContactService } from 'src/app/contact/shared/contact.service';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-show-seed',
  templateUrl: './show-seed.component.html',
  styleUrls: ['./show-seed.component.scss'],
})
export class ShowSeedComponent implements OnInit {

  @Input() mnemonic: string;
  @Input() privateKey: string;
  @Input() heading: string;
  @Input() publicKey: string;

  encryptedMnemonic: string;
  public qrCodeIsHidden = false;
  private timerSubscription: Subscription;
  qrCodeCaptionCountdown;

  constructor(
    private _ModalController: ModalController,
    private _ToastController: ToastController,
    private _Translate: TranslateProviderService,
    private _ContactService: ContactService,
    
  ) { }

  ngOnInit() {
    this.startQrTimer();
  }

  close() {
    this.stopQrTimer();
    this._ModalController.dismiss();
  }

  copy() {
    if(!!this.mnemonic) {
      Clipboard.write({ string: this.mnemonic }).then(() => {
        this.presentToast(this._Translate.instant('WALLET.seedPhraseCopied'));
      })
    }
    if(!!this.privateKey) {
      Clipboard.write({ string: this.privateKey }).then(() => {
        this.presentToast(this._Translate.instant('WALLET.privateKeyCopied'));
      })
    }
  }

  presentToast(message) {
    this._ToastController.create({
      message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => {
      toast.present();
    })
  }

  private stopQrTimer(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  private async startQrTimer(): Promise<void> {

    const INTERVAL = 1000;
    let countdown = 0;
    let validity: number;
    let timeout: number;
    let encryptedContactIdAsString: string;

    
    const heading = this.heading;
    const publicKey = this.publicKey;

    if(!!this.mnemonic) {
      var mnemonic = CryptoJS.AES.encrypt(JSON.stringify(this.mnemonic), UDIDNonce.userKey).toString();
      var encryptedQRString = JSON.stringify({ heading, publicKey, mnemonic });
    } else {
      var privateKey = CryptoJS.AES.encrypt(JSON.stringify(this.privateKey), UDIDNonce.userKey).toString();
      var encryptedQRString = JSON.stringify({ heading, publicKey, privateKey });
    }

    const data = await this._ContactService.getQRCode(encryptedQRString, new Date(), 'importwallet');

    validity = data.validity;
    timeout = data.timeout;
    encryptedContactIdAsString = data.encryptedContactIdAsString;

    var url = `${encryptedContactIdAsString}`;
    this.encryptedMnemonic = url;
    console.log(this.encryptedMnemonic);

    const timer = interval(INTERVAL);
    this.timerSubscription = timer.subscribe(() => {
      countdown++;

      if (countdown === validity - timeout || countdown === validity - timeout + 1) {
        this.qrCodeIsHidden = true;
        this.qrCodeCaptionCountdown = this._Translate.instant("QRSCAN.new-generation");
        return;
      }

      if (countdown === validity) {
        this._ContactService.getQRCode(encryptedQRString, new Date(), 'importwallet').then(data => {
          validity = data.validity;
          var url = `${data.encryptedContactIdAsString}`;
          this.encryptedMnemonic = url;
          this.qrCodeIsHidden = false;
        });

        countdown = 0;
      }

      this.qrCodeCaptionCountdown = this._Translate.instant("QRSCAN.expiry-notice.part1")+ `${validity - timeout - countdown}s` + this._Translate.instant("QRSCAN.expiry-notice.part2");
    });
  }

}
