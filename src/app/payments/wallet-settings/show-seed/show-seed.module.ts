import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { ShowSeedComponent } from './show-seed.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    NgxQRCodeModule,
    TranslateModule.forChild()
  ],
  providers: [],
  declarations: [ShowSeedComponent],
  // entryComponents: [ShowSeedComponent],
  exports: [ShowSeedComponent]
})
export class ShowSeedModule {}
