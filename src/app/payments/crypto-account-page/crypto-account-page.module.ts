import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { CryptoAccountPagePage } from "./crypto-account-page.page";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "../../shared/shared.module";
import { NgxQRCodeModule } from "@techiediaries/ngx-qrcode";
import { PaymentModalModule } from "../payment-modal/payment-modal.module";
import { TransactionDetailModule } from "../transaction-detail/transaction-detail.module";
import { SocialSharing } from "@awesome-cordova-plugins/social-sharing/ngx";
import { KeyExportModule } from "src/app/sign-up/components/step-5/key-export/key-export.module";
import { SendCryptoModule } from "./send-crypto/send-crypto.module";
import { ReceiveCryptoModule } from "./receive-crypto/receive-crypto.module";
import { BuyCryptoModule } from "./buy-crypto/buy-crypto.module";
import { SafariViewController } from "@awesome-cordova-plugins/safari-view-controller/ngx";
import { CurrenciesComponent } from "./components/currencies/currencies.component";
import { AddCustomTokenComponent } from "./components/add-custom-token/add-custom-token.component";
import { DonutChartComponent } from "./components/donut-chart/donut-chart.component";
import { CryptoPricesListComponent } from "./components/crypto-prices-list/crypto-prices-list.component";
import { LineChartComponent } from "./components/line-chart/line-chart.component";
import { JobHeaderModule } from "src/app/job-header/job-header.module";


const routes: Routes = [
  {
    path: '',
    component: CryptoAccountPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    NgxQRCodeModule,
    PaymentModalModule,
    KeyExportModule,
    TransactionDetailModule,
    ReactiveFormsModule,
    SendCryptoModule,
    ReceiveCryptoModule,
    BuyCryptoModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  providers: [SocialSharing, SafariViewController],
  declarations: [
    CryptoAccountPagePage,
    CurrenciesComponent,
    AddCustomTokenComponent,
    DonutChartComponent,
    CryptoPricesListComponent,
    LineChartComponent
  ]
})
export class CryptoAccountPagePageModule {}
