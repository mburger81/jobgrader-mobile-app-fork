import { Component, Input, OnInit } from '@angular/core';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { Clipboard } from '@capacitor/clipboard';
import { ModalController, ToastController } from '@ionic/angular';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { LockService } from 'src/app/lock-screen/lock.service';

@Component({
  selector: 'app-receive-crypto',
  templateUrl: './receive-crypto.component.html',
  styleUrls: ['./receive-crypto.component.scss'],
})
export class ReceiveCryptoComponent implements OnInit {

  @Input() data: any;

  public buttons = {
    copy: '../../../../assets/crypto/copy.svg',
    tag: '../../../../assets/crypto/tag.svg',
    share: '../../../../assets/crypto/share.svg'
  }


  constructor(
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService,
    
    private _ToastController: ToastController,
    private lockService: LockService,
    private _SocialSharing: SocialSharing
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  copyAddress() {
    console.log(this.data);
    Clipboard.write({ string: this.data }).then(() => {
      this.presentToast(this._Translate.instant('WALLET.addressCopied'));
    })
  }


  socialSharing() {
    this.lockService.removeResume();
    this._SocialSharing.share(this.data);
    this.lockService.addResume();
  }

  // private async solanaInit(phrase: string) {

  //   var seed = await bip.mnemonicToSeed(phrase);
  //   var uix = Uint8Array.from(seed);
  //   uix = uix.slice(0,32);
  //   // var uix = new Uint8Array(seed.toJSON().data.slice(0,32))
  //   var solana = solanaWeb3.Keypair.fromSeed(uix);

  //   return solana;

  // }

  // public async toggleSolana() {

  //   var decryptedMnemonic = this.decryptMnemonic(this.data.account.mnemonic);
  //   var wallet = this._NftService.returnWallet(decryptedMnemonic);
  //   var solanaKeyPair =  await this.solanaInit((wallet as Wallet).mnemonic.phrase);

  //   console.log(solanaKeyPair.publicKey.toBase58());

  // }

  private presentToast(message: string) {
    this._ToastController.create({
      message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

  close() {
    this._ModalController.dismiss();
  }

}
