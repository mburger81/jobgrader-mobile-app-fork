import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ThemeSwitcherService } from "src/app/core/providers/theme/theme-switcher.service";
import { CryptoCurrency } from "src/app/core/providers/wallet-connect/constants";
import { ChartService } from "../../services/chart.service";


@Component({
  selector: "app-donut-chart",
  templateUrl: "./donut-chart.component.html",
  styleUrls: ["./donut-chart.component.scss"],
})
export class DonutChartComponent implements OnInit {

  @Output("onCryptoNavigationHistory") onCryptoNavigationHistory =
    new EventEmitter<any>();

  constructor(private _Theme: ThemeSwitcherService, private chartService: ChartService) {}

  ngOnInit() {
   
  }

 
}
