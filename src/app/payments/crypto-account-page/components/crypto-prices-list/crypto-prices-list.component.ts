import { Component, Input, OnInit } from "@angular/core";
import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import { ChartService } from "../../services/chart.service";
import { fiatCurrencies } from "src/app/core/providers/nft/fiat-currencies";

@Component({
  selector: "app-crypto-prices-list",
  templateUrl: "./crypto-prices-list.component.html",
  styleUrls: ["./crypto-prices-list.component.scss"],
})
export class CryptoPricesListComponent implements OnInit {
  @Input("data") data: any[] = [];
  @Input("balances") balances: any;

  price_data: any[] = [];
  currency = "EUR";
  symbol = "€";
  data_ready: boolean = false;
  constructor(private _SecureStorageService: SecureStorageService, public chartService: ChartService) {
  }

  getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  };

  getReadableBalance(a: any) {
    return Number(a).toLocaleString();
  }

  async ngOnInit() {
    console.log("price_data", this.data);
    var currency = await this._SecureStorageService.getValue(SecureStorageKey.currency, false);
    this.currency = !!currency ? currency : this.currency;
    this.symbol = fiatCurrencies.find(fi => fi.currency_code == this.currency).currency_symbol;
    this.data.forEach((ele, index) => {
      let price_data = [
        { date: "2015-01-01", value: this.getRandomInt(10, 20) },
        { date: "2016-01-01", value: this.getRandomInt(10, 20) },
        { date: "2017-01-01", value: this.getRandomInt(10, 20) },
        { date: "2018-01-01", value: this.getRandomInt(10, 20) },
        { date: "2019-01-01", value: this.getRandomInt(10, 20) },
        { date: "2020-01-01", value: this.getRandomInt(10, 20) },
      ];

      this.price_data.push(price_data);
    });
    this.data_ready = true;
  }

  trackByData(index, item) {
    return item.chainid + '-' + item.currency;
  }

}
