import { AfterViewInit, Component, Input, OnDestroy, OnInit } from "@angular/core";
// import * as d3 from "d3";
// export type DataType = { year: number; value: number };
@Component({
  selector: "app-line-chart",
  templateUrl: "./line-chart.component.html",
  styleUrls: ["./line-chart.component.scss"],
})
export class LineChartComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input("id_index") id_index: number = 0;
  @Input("data") data: any[] = [];

  constructor() {
  }

  ngOnInit() {}
  ngAfterViewInit(): void {
    // console.log("drawing for", "#line-chart-" + this.id_index);
    this.lineChart("#line-chart-" + this.id_index);
  }
  lineChart(id: string) {
    let d3 = (window as any).d3;

    d3.select(id).selectAll("*").remove();

    // console.log('After removal', d3.select(id));
    let margin = { top: 10, right: 30, bottom: 30, left: 60 },
      width = 460 - margin.left - margin.right,
      height = 400 - margin.top - margin.bottom;

    width = 40;
    height = 15;
    // console.log("width and height", width, height);
    setTimeout(() => {
      const svg = d3
        .select(id)
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("background", "white")
        .append("g");

      let transformDate = (d) => {
        let timeParse = d3.time.format("%Y-%m-%d").parse;
        return { date: timeParse(d.date), value: d.value };
      };

      let data = this.data.map(transformDate);

      const x = d3.scale
        .linear()
        .domain(
          d3.extent(data, (d) => {
            return d.date;
          })
        )
        .range([0, width]);

      const y = d3.scale
        .linear()
        .domain([
          0,
          d3.max(data, (d) => {
            return +d.value;
          }),
        ])
        .range([height, 0]);

      svg
        .append("path")
        .datum(data)
        .attr("fill", "none")
        .attr(
          "stroke",
          data[data.length - 1].value > data[data.length - 2].value
            ? "green"
            : "red"
        )
        .attr("stroke-width", 1.5)
        .attr(
          "d",
          d3.svg
            .line()
            .x((d) => {
              return x(d.date);
            })
            .y((d) => {
              return y(d.value);
            })
        );
    }, 1000);
  }
  ngOnDestroy(): void {

    console.log('destrouing');
    let d3 = (window as any).d3;

    d3.select("#line-chart-"+ this.id_index).remove();


  }

}
