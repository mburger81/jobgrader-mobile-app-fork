import {
  Component,
  OnDestroy,
} from "@angular/core";
import {
  ModalController,
  ToastController,
  NavController,
  AlertController,
  Platform,
} from "@ionic/angular";
import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import {
  UntypedFormGroup,
  UntypedFormControl,
} from "@angular/forms";
import { BigNumberish, ethers, Wallet } from "ethers";
import { DatePipe } from "@angular/common";
import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
import { Router } from "@angular/router";
import { ApiProviderService } from "src/app/core/providers/api/api-provider.service";
import { TranslateProviderService } from "src/app/core/providers/translate/translate-provider.service";
import { ThemeSwitcherService } from "src/app/core/providers/theme/theme-switcher.service";
import {
  Contracts,
  CryptoCurrency,
  CryptoCurrencyColors,
  CryptoNetworkNames,
  CryptoNetworks,
  EthereumNetworks,
} from "src/app/core/providers/wallet-connect/constants";
import * as Identicon from "identicon.js";
import { SendCryptoComponent } from "./send-crypto/send-crypto.component";
import { ReceiveCryptoComponent } from "./receive-crypto/receive-crypto.component";
import { NetworkService } from "src/app/core/providers/network/network-service";
import { fiatCurrencies } from "src/app/core/providers/nft/fiat-currencies";
import { InAppBrowser } from "@awesome-cordova-plugins/in-app-browser/ngx";
import { SafariViewController } from "@awesome-cordova-plugins/safari-view-controller/ngx";
import { Clipboard } from '@capacitor/clipboard';
import { CurrenciesComponent } from "./components/currencies/currencies.component";
import { AddCustomTokenComponent } from "./components/add-custom-token/add-custom-token.component";
import { CryptoCurrencyService, CurrencyToNameMapper, CurrencyToRPCMapper } from "src/app/payments/crypto-account-page/services/crypto-currency.service";
import { ChartService } from "./services/chart.service";
import { VaultSecretKeys, VaultService } from "src/app/core/providers/vault/vault.service";
import { MeldService } from "src/app/core/providers/meld/meld.service";
import { uniq, uniqBy } from 'lodash';
import { WalletsQuery } from "../../shared/state/wallets/wallets.query";
import { Observable, Subject, takeUntil } from "rxjs";
import { WalletsService } from "../../shared/state/wallets/wallets.service";
import { LoaderProviderService } from "src/app/core/providers/loader/loader-provider.service";
import { UserPhotoServiceAkita } from "../../core/providers/state/user-photo/user-photo.service";
import { TransakService } from "src/app/core/providers/transak/transak.service";
import { NftService } from "src/app/core/providers/nft/nft.service";
// import { LiFiWidget, WidgetConfig } from '@lifi/widget';

// import * as React from 'react';
// import * as ReactDOM from 'react-dom';

const bip = require("bip39");
const abi = require('./abi.json');

let INIT_SIZE_MAX;

@Component({
  selector: "app-crypto-account-page",
  templateUrl: "./crypto-account-page.page.html",
  styleUrls: ["./crypto-account-page.page.scss"],
})
export class CryptoAccountPagePage implements OnDestroy {
  wallet_address_index: number = -1;
  wallet_currencies: any = {};
  wallet_currencies_data: any[] = [];

  cryptoBalance;
  cryptoConversions;
  all_chart_data_ready = false;
  public icon;
  public data;
  public viewChoice = "accountInfo";

  private DEFAULT_FIAT = "EUR";
  public FIAT_SYMBOL = "€";
  public CONVERTED_FIAT = "";

  public qrCodeValue = null;
  public loadingFinished = false;
  public profilePictureSrc =
    "../../../assets/job-user.svg";
  public sum: Number = null;
  public form = new UntypedFormGroup({
    to: new UntypedFormControl("", []),
    amount: new UntypedFormControl("", []),
    currency: new UntypedFormControl("", []),
  });
  public currencySet = [
    CryptoCurrency.MATIC,
    CryptoCurrency.EVE,
    CryptoCurrency.ETH,
    CryptoCurrency.GNOSIS,
  ];
  private datePipe = new DatePipe("en-US");
  private browser;
  private username;
  public buttons = {
    copy: "../../../assets/crypto/vector/copy.svg",
    tag: "../../../assets/crypto/vector/tag.svg",
    share: "../../../assets/crypto/vector/share.svg",
  };

  public testTrackingAccounts = ["0x1AC76Ec4c02c5488E8DcB892272e9E284d5Fe295"];

  public chartData = [];

  public requestsSent = [
    // {
    //   userPublicKey: "0xd78D4Bdf7AB5231676c698C8fE538cafA2912777",
    //   amount: "8.11",
    //   currency: "MATIC",
    //   timestamp: +new Date(),
    //   status: 0,
    //   pendingProcesses: [],
    //   privateKey: null,
    //   paymentRequestId: '73207b58-647e-41e6-9e2c-1e60a5ea4048'
    // },
    // {
    //   userPublicKey: "0xe564D4Bdf7AB5231676c698C8fE538cafA291277",
    //   amount: "9.88",
    //   currency: "EVE",
    //   timestamp: +new Date(),
    //   status: 1,
    //   pendingProcesses: [],
    //   privateKey: null
    // },
    // {
    //   userPublicKey: "0xf89D4Bdf7AB5231676c698C8fE538cafA2912651",
    //   amount: "5.18",
    //   currency: "ETH",
    //   timestamp: +new Date(),
    //   status: -1,
    //   pendingProcesses: [],
    //   privateKey: null
    // },
  ];

  browserOptions =
    "zoom=no,footer=no,hideurlbar=yes,footercolor=#0021FF,hidenavigationbuttons=yes,presentationstyle=pagesheet";

  private animate = true;
  private notifier = new Subject();

  randomValueArray;

  constructor(
    public chartService: ChartService,
    public cryptoCurrencyService: CryptoCurrencyService,
    private _ToastController: ToastController,
    private _SecureStorageService: SecureStorageService,
    private _NavController: NavController,
    private modalCtrl: ModalController,
    private _ApiProviderService: ApiProviderService,
    private _Translate: TranslateProviderService,
    public _Theme: ThemeSwitcherService,
    private _NetworkService: NetworkService,
    private _Router: Router,
    private _Platform: Platform,
    private _InAppBrowser: InAppBrowser,
    private _SafariViewController: SafariViewController,
    
    private _Vault: VaultService,
    private _Loader: LoaderProviderService,
    private _Meld: MeldService,
    private _NftService: NftService,
    private _Transak: TransakService,
    private walletsQuery: WalletsQuery,
    private walletsService: WalletsService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
    let checker = this._Router.parseUrl(this._Router.url).queryParams;
    this.data = JSON.parse(checker.data);
    this.chartService.data = this.data;

  }

  ngOnDestroy() {
    this.notifier.next(null);
    this.notifier.complete();
  }

  public navigateToCryptoHistory(currency: string) {
    // console.log("navigateToCryptoHistory", currency);
    this._NavController.navigateForward(
      `/dashboard/payments/network-account-details?address=${
        this.data.account.publicKey
      }&heading=${this.data.account.heading}&primary=${!!this.data.account
        .primary}&currency=${currency}`
    );
  }

  public displayPictureOptions() {
    console.log("Icon");
  }

  async ionViewDidEnter() {
    await this.onPageRefresh();
  }

  async onPageRefresh() {
    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter("ion-footer");
    } else {
      this._NetworkService.removeOfflineFooter("ion-footer");
    }

    this.loadingFinished = false;

    var currency = await this._SecureStorageService.getValue(
      SecureStorageKey.currency,
      false
    );

    this.DEFAULT_FIAT = !!currency ? currency : this.DEFAULT_FIAT;

    console.log("this.DEFAULT_FIAT", this.DEFAULT_FIAT);

    this.FIAT_SYMBOL = fiatCurrencies.find(fi => fi.currency_code == this.DEFAULT_FIAT).currency_symbol;

    console.log("this.FIAT_SYMBOL", this.FIAT_SYMBOL);

    if (this.data.account.primary) {
      const photo = this.userPhotoServiceAkita.getPhoto();
      this.profilePictureSrc = !!photo ? photo : this.profilePictureSrc;
    } else {
      this.profilePictureSrc = `data:image/png;base64,${new Identicon(
        this.data.account.publicKey,
        420
      ).toString()}`;
    }

    this.username = await this._SecureStorageService.getValue(
      SecureStorageKey.userName,
      false
    );

    var accountsString1 = await this._SecureStorageService.getValue(
      SecureStorageKey.web3WalletPublicKey,
      false
    );

    if (accountsString1 == this.data.account.publicKey) {
      this.data.account.mnemonic = await this._SecureStorageService.getValue(
        SecureStorageKey.web3WalletMnemonic,
        false
      );
    } else {
      var accountString2 = await this._SecureStorageService.getValue(
        SecureStorageKey.importedWallets,
        false
      );
      var importedAccounts = !!accountString2 ? JSON.parse(accountString2) : [];
      // console.log(importedAccounts);
      var check = importedAccounts.find(
        (k) => k.publicKey == this.data.account.publicKey
      );
      if (check) {
        if (check.mnemonic) {
          this.data.account.mnemonic = check.mnemonic;
        }
      }
    }

    var cryptoPaymentRequestsSent = await this._SecureStorageService.getValue(
      SecureStorageKey.cryptoPaymentRequestsSent,
      false
    );
    this.requestsSent = !!cryptoPaymentRequestsSent
      ? JSON.parse(cryptoPaymentRequestsSent)
      : this.requestsSent;

    var cryptoBalanceString = await this._SecureStorageService.getValue(
      SecureStorageKey.cryptoBalance,
      false
    );
    this.cryptoBalance = !!cryptoBalanceString
      ? JSON.parse(cryptoBalanceString)
      : {};

    var cryptoConversions = await this._SecureStorageService.getValue(
      SecureStorageKey.cryptoConversions,
      false
    );

    this.cryptoConversions = !!cryptoConversions ? JSON.parse(cryptoConversions) : {};

    let wallet_currencies =
      await this.cryptoCurrencyService.getWalletCurrencies(
        this.data.account.publicKey
      );

    if(wallet_currencies) {
      this.wallet_currencies = wallet_currencies;
    } else {
      await this.cryptoCurrencyService.setInitialWalletData(
        this.data.account.publicKey
        // ,
        // this.cryptoCurrencyService.all_wallets_currencies_data
      );
      wallet_currencies =
      await this.cryptoCurrencyService.getWalletCurrencies(
        this.data.account.publicKey
      );
      this.wallet_currencies = wallet_currencies;
    }

    // console.log("this.wallet_currencies", this.wallet_currencies);

    var crob = this.cryptoBalance[this.data.account.publicKey];

    // console.log("crob", crob);

    this.data.account.network = [];

    this.initialNetworkList(crob);

    this.randomValueArray = Array.from({length: this.data.account.network.length}, () => Math.floor(Math.random()*(this.data.account.network.length)));

    this.walletsQuery.selectEntity(this.data.account.publicKey)
      .pipe(
        takeUntil(this.notifier)
      )
      .subscribe(
        (wallet) => {
          if (!wallet) {

            this.CONVERTED_FIAT = `${this.FIAT_SYMBOL} ${Math.floor(0 * 100) / 100}`;

            this.chartData = Array.from(this.data.account.network, (k, i) => {
              return {
                color: (k as any).color,
                count: (!!(k as any).balance ? (Number((k as any).balance) * this.cryptoConversions[`${(k as any).currency}/${this.DEFAULT_FIAT}`]) : 0),
                size: INIT_SIZE_MAX - 2 * this.randomValueArray[i],
                icon: (k as any).icon,
              };
            });

            this.walletsService.add({ address: this.data.account.publicKey, convertedFiat: this.CONVERTED_FIAT, chartData: this.chartData });

          } else {

            this.chartService.d3Chart(wallet.convertedFiat, wallet.chartData, "#chart", this.animate);
            this.animate = false;

          }
      }
    );

    this.qrCodeValue = this.data.account.publicKey;

    this.setupConnectedAccounts(this.data.account.publicKey);

    // mburger: no need to call this here because its called even in setupConnectedAccounts
    // this.prepareWalletCurrenciesData();

    this.loadingFinished = true;
  }

  initialNetworkList(crob: any) {

    let checker = this.wallet_currencies.currencies_data.filter(we => !!we.active);
    let checkerArray = Array.from(checker, fff => (fff as any).currency);
    checkerArray = uniq(checkerArray);

    INIT_SIZE_MAX = 3 * (checkerArray.length + 2);

    for(let _ of checkerArray) {

      this.updateInitialNetworkList(crob, _);

    }

  }

  updateInitialNetworkList(crob: any, currency: string) {
    this.data.account.network.push({
      balance: !!crob
          ? !!crob[currency]
            ? crob[currency]
            : null
          : null,
        currency,
        chainid: null,
        ens: null,
        icon: (currency.includes("HMT")) ? `../../../assets/crypto/${currency}.png` : `../../../assets/crypto/vector/${currency}.svg`,
        color: CryptoCurrencyColors[currency],
        name: CurrencyToNameMapper[currency],
        loading: true,
    })
  }

  async generateConvertedCryptoFiat() {
    let net = this.data.account.network;
    let fiat = this.DEFAULT_FIAT;
    let sum = 0;

    // console.log("net", this.data.account.network);

    var t = Array.from(net, (k) => {
      return {
        currency: (k as any).currency,
        amount: !!(k as any).balance ? (k as any).balance : 0,
      };
    });

    t = t.filter((ttt) => ttt.currency != CryptoCurrency.EVE);

    for (let i = 0; i < t.length; i++) {
      // console.log("KKKK", t[i]);
      var key = `${t[i].currency}/${fiat}`;

      sum = sum + this.cryptoConversions[key] * Number(t[i].amount);
    }

    this.CONVERTED_FIAT = `${this.FIAT_SYMBOL} ${Math.floor(sum * 100) / 100}`;

    this.chartData = Array.from(this.data.account.network, (k, i) => {
      var g = {
        color: (k as any).color,
        count: !!(k as any).balance
          ? Number((k as any).balance) * con
          : 0,
        size: INIT_SIZE_MAX - 2 * this.randomValueArray[i],
        icon: (k as any).icon,
      };

      return g;
    });

    sum = 0;

    for (let i = 0; i < t.length; i++) {

      var key = `${t[i].currency}/${fiat}`;

      if(t[i].currency == CryptoCurrency.THXC) {

        var eth = await this._NftService.obtainTHXCtoETHPrice();
        var ethCon = await this._ApiProviderService.cryptoConversion(
          CryptoCurrency.ETH,
          fiat
        );

        var con = eth * ethCon;

      } else {
        con = await this._ApiProviderService.cryptoConversion(
          t[i].currency,
          fiat
        );
      }   

      this.cryptoConversions[key] = con;

      sum = sum + con * Number(t[i].amount);

      this.CONVERTED_FIAT = `${this.FIAT_SYMBOL} ${Math.floor(sum * 100) / 100}`;

      this.chartData = Array.from(this.data.account.network, (k, i) => {
        var g = {
          color: (k as any).color,
          count: (!!(k as any).balance ? (Number((k as any).balance) * this.cryptoConversions[`${(k as any).currency}/${this.DEFAULT_FIAT}`]) : 0),
          size: INIT_SIZE_MAX - 2 * this.randomValueArray[i],
          icon: (k as any).icon,
        };

        return g;
      });
    }

    await this.prepareWalletCurrenciesData();

    await this._SecureStorageService.setValue(SecureStorageKey.cryptoConversions, JSON.stringify(this.cryptoConversions));

}

async connectToThxcWallet(address: string) {
  var etherscanAPIKey = await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY);
  console.log("THXC GEORLI Init\n--------\n");
  const thxcContract = Contracts.THXC;
  const provider = EthereumNetworks.GOERLI;
  const contract = new ethers.Contract(thxcContract, abi);
  console.log("THXC Contract", contract);
  const thxc = contract.connect(new ethers.providers.EtherscanProvider(provider, etherscanAPIKey));
  console.log("THXC Connect", thxc);
  const thxcBalance = await thxc.balanceOf(address);
  console.log("THXC Balance", thxcBalance);
  const val = ethers.BigNumber.from(thxcBalance._hex);
  const readableBalance = ethers.utils.formatEther(val);

  this.data.account.network.find(c => c.currency == CryptoCurrency.THXC).balance = (Math.round(Number(readableBalance) * 10000000) / 10000000).toString() ;
  this.data.account.network.find(c => c.currency == CryptoCurrency.THXC).chainid = thxc.chainId;
  this.data.account.network.find(c => c.currency == CryptoCurrency.THXC).ens = provider;
  this.data.account.network.find(c => c.currency == CryptoCurrency.THXC).name = CryptoNetworkNames.THXC;
  this.data.account.network.find(c => c.currency == CryptoCurrency.THXC).loading = false;

  var cryptoBalanceString = await this._SecureStorageService.getValue(SecureStorageKey.cryptoBalance, false);
  var cryptoBalance = !!cryptoBalanceString ? JSON.parse(cryptoBalanceString) : {};

  if(!cryptoBalance[address]) {
    Object.assign(cryptoBalance, { [address] : {} });
  }
  Object.assign(cryptoBalance[address], { [CryptoCurrency.THXC]: this.data.account.network.find(c => c.currency == CryptoCurrency.THXC).balance } );
  await this._SecureStorageService.setValue(SecureStorageKey.cryptoBalance, JSON.stringify(cryptoBalance));

}

  async connectToHmtPolygonWallet(address: string) {

    console.log("HMT Polygon Init\n--------\n");
    const hmtContract = Contracts.HMT_POLYGON;
    const provider = CryptoNetworks.HMT_POLYGON;
    const contract = new ethers.Contract(hmtContract, abi);
    const hmt = contract.connect(ethers.getDefaultProvider(provider));
    const hmtBalance = await hmt.balanceOf(address);
    const val = ethers.BigNumber.from(hmtBalance._hex);
    const readableBalance = ethers.utils.formatEther(val);
    console.log(`HMT: ${readableBalance}`);

    this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_POLYGON).balance = (Math.round(Number(readableBalance) * 10000000) / 10000000).toString() ;
    this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_POLYGON).chainid = hmt.chainId;
    this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_POLYGON).ens = provider;
    this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_POLYGON).name = CryptoNetworkNames.HMT_POLYGON;
    this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_POLYGON).loading = false;

    var cryptoBalanceString = await this._SecureStorageService.getValue(SecureStorageKey.cryptoBalance, false);
    var cryptoBalance = !!cryptoBalanceString ? JSON.parse(cryptoBalanceString) : {};

    if(!cryptoBalance[address]) {
      Object.assign(cryptoBalance, { [address] : {} });
    }
    Object.assign(cryptoBalance[address], { [CryptoCurrency.HMT_POLYGON]: this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_POLYGON).balance } );
    await this._SecureStorageService.setValue(SecureStorageKey.cryptoBalance, JSON.stringify(cryptoBalance));

    // mburger: no need to call this here because its called in setupConnectedAccounts
    // await this.generateConvertedCryptoFiat();

}

async connectToHmtSkaleWallet(address: string) {
  console.log("HMT SKALE Init\n--------\n");

  const hmtContract = Contracts.HMT_SKALE;
  const provider = CryptoNetworks.HMT_SKALE;
  const contract = new ethers.Contract(hmtContract, abi);
  const hmt = contract.connect(ethers.getDefaultProvider(provider));
  const hmtBalance = await hmt.balanceOf(address);
  const val = ethers.BigNumber.from(hmtBalance._hex);
  const readableBalance = ethers.utils.formatEther(val);
  console.log(`HMT: ${readableBalance}`);

  this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_SKALE).balance = (Math.round(Number(readableBalance) * 10000000) / 10000000).toString() ;
  this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_SKALE).chainid = hmt.chainId;
  this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_SKALE).ens = provider;
  this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_SKALE).name = CryptoNetworkNames.HMT_SKALE;
  this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_SKALE).loading = false;

  var cryptoBalanceString = await this._SecureStorageService.getValue(SecureStorageKey.cryptoBalance, false);
  var cryptoBalance = !!cryptoBalanceString ? JSON.parse(cryptoBalanceString) : {};

  if(!cryptoBalance[address]) {
    Object.assign(cryptoBalance, { [address] : {} });
  }
  Object.assign(cryptoBalance[address], { [CryptoCurrency.HMT_SKALE]: this.data.account.network.find(c => c.currency == CryptoCurrency.HMT_SKALE).balance } );
  await this._SecureStorageService.setValue(SecureStorageKey.cryptoBalance, JSON.stringify(cryptoBalance));

  // mburger: no need to call this here because its called in setupConnectedAccounts
  // await this.generateConvertedCryptoFiat();
}

  async connectToWallet(
    address: string,
    provider: any,
    name: string,
    currency: string
  ) {
    let network;

    if (
      [
        EthereumNetworks.MAINNET,
        EthereumNetworks.KOVAN,
        EthereumNetworks.RINKEBY,
        EthereumNetworks.ROPSTEN,
      ].includes(provider)
    ) {
      network = new ethers.providers.EtherscanProvider(
        EthereumNetworks.MAINNET,
        await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY)
      );
    } else {
      network = ethers.getDefaultProvider(provider);
    }

    const balance = await network.getBalance(address);

    const val = ethers.BigNumber.from(balance._hex);
    const networkData = await network.getNetwork();
    var res = this.data.account.network.find((c) => c.currency == currency);

    res.balance = (
      Math.round(Number(ethers.utils.formatEther(val)) * 10000000) / 10000000
    ).toString();
    res.chainid = networkData.chainId;
    res.ens = provider;
    res.name = name;
    res.loading = false;
    var cryptoBalanceString = await this._SecureStorageService.getValue(
      SecureStorageKey.cryptoBalance,
      false
    );
    this.cryptoBalance = !!cryptoBalanceString
      ? JSON.parse(cryptoBalanceString)
      : {};

    if (!this.cryptoBalance[address]) {
      Object.assign(this.cryptoBalance, { [address]: {} });
    }
    Object.assign(this.cryptoBalance[address], { [currency]: res.balance });
    await this._SecureStorageService.setValue(
      SecureStorageKey.cryptoBalance,
      JSON.stringify(this.cryptoBalance)
    );

  }

  close() {
    this._NavController.navigateBack('/dashboard/payments');
  }

  toProfile() {
    this._NavController.navigateBack("/dashboard/tab-profile?source=web3");
  }

  async setupConnectedAccounts(address: string) {
    console.log('yyyyyyyyyy ->', address)

    let checker = this.wallet_currencies.currencies_data.filter(we => !!we.active);
    let checkerArray = Array.from(checker, fff => (fff as any).currency);
    checkerArray = checkerArray.filter(ca => !ca.includes("HMT"));
    checkerArray = checkerArray.filter(ca => !ca.includes("THXC"));
    checkerArray = uniq(checkerArray);

    this.connectToThxcWallet(address);
    this.connectToHmtPolygonWallet(address);
    this.connectToHmtSkaleWallet(address);

    // mburger: call this here instead of two times in
    // connectToHmtPolygonWallet and connectToHmtSkaleWallet
    await this.generateConvertedCryptoFiat();


    for(let _ of checkerArray) {
      await this.connectToWallet(address, CurrencyToRPCMapper[_], CurrencyToNameMapper[_], _);
    }


    await this.generateConvertedCryptoFiat();
    this.all_chart_data_ready = true;

    this.walletsService.update(this.data.account.publicKey, { convertedFiat: this.CONVERTED_FIAT, chartData: this.chartData });

  }

  presentToast(message: string) {
    this._ToastController
      .create({
        message: message,
        position: "bottom",
        duration: 2000,
      })
      .then((toast) => toast.present());
  }

  showContentOptions(currencySelected) {
    var cont = this.data.account.network.find(
      (k) => k.currency == currencySelected
    );
    var fincont = `${currencySelected} - ${cont.balance}`;
    return fincont;
  }

  async Buy() {
    var url = await this._Transak.openModalWindow(
      this.data.account.publicKey,
      this.currencySet.filter((k) => k != CryptoCurrency.EVE).join(","),
      CryptoCurrency.ETH
    );

    if (this._Platform.is("ios") && this._Platform.is("hybrid")) {
      this.showSafariInstance(url);
    } else if (this._Platform.is("android") && this._Platform.is("hybrid")) {
      this.browser = this._InAppBrowser.create(
        url,
        "_system",
        this.browserOptions
      );
    } else if (!this._Platform.is("hybrid")) {
      this.browser = this._InAppBrowser.create(url, "_system");
    }
  }

  Send() {
    this.modalCtrl
      .create({
        component: SendCryptoComponent,
        componentProps: {
          data: this.data,
        },
      })
      .then((modal) => {
        modal.onDidDismiss().then(() => {
          this.setupConnectedAccounts(this.data.account.publicKey);
        });
        modal.present();
      });
  }

  Receive() {
    this.modalCtrl
      .create({
        component: ReceiveCryptoComponent,
        componentProps: {
          data: this.data.account.publicKey,
        },
      })
      .then((modal) => {
        modal.onDidDismiss().then(() => {
          this.setupConnectedAccounts(this.data.account.publicKey);
        });
        modal.present();
      });
  }

  Swap() {
    console.log("swap()");
    // this.presentToast(this._Translate.instant("CONTACT.title"));

    // const widgetConfig: WidgetConfig = {
    //   containerStyle: {
    //     border: '1px solid rgb(234, 234, 234)',
    //     borderRadius: '16px',
    //   },
    //   integrator: "Jobgrader"
    // };

    // ReactDOM.render(
    //   <LiFiWidget config={widgetConfig} />
    //   );

    var a = document.createElement("a");
    a.href = "https://jumper.exchange/";
    a.style.display = "none";
    a.target = "_blank";
    a.click();
    a.remove();

  }

  showSafariInstance(url) {
    this._SafariViewController.isAvailable().then((available: boolean) => {
      if (available) {
        this._SafariViewController
          .show({
            url,
            hidden: false,
            animated: true,
            transition: "slide",
            enterReaderModeIfAvailable: false,
            tintColor: "#0021FF",
          })
          .subscribe(
            (result: any) => {},
            (error: any) => console.error(error)
          );
      } else {
        this.browser = this._InAppBrowser.create(
          url,
          "_self",
          this.browserOptions
        );
      }
    });
  }

  copyAddress() {
    console.log(this.data.account.publicKey);
    Clipboard.write({ string: this.data.account.publicKey }).then(() => {
      this.presentToast(this._Translate.instant("WALLET.addressCopied"));
    })
  }

  async openCurrenciesModal() {
    console.log("opening currencies modal");
    let modal = await this.modalCtrl.create({
      component: CurrenciesComponent,
      breakpoints: [0, 0.8, 1],
      initialBreakpoint: 0.8,
      componentProps: {
        wallet_address: this.data.account.publicKey,
      },
    });

    modal.onDidDismiss().then(async (resp) => {
      // await this.prepareWalletCurrenciesData();
      // this.setupConnectedAccounts(this.data.account.publicKey);
      // this.chartData = Array.from(this.data.account.network, (k, i) => {
      //   return {
      //     color: (k as any).color,
      //     count: !!(k as any).balance ? (Number((k as any).balance) * this.cryptoConversions[`${(k as any).currency}/${this.DEFAULT_FIAT}`] ): 0,
      //     size: INIT_SIZE_MAX - 2 * this.randomValueArray[i],
      //     icon: (k as any).icon,
      //   };
      // });
      // console.log("this.chartService.d3Chart 4", this.chartData);
      // this.chartService.d3Chart(this.CONVERTED_FIAT, this.chartData, "#chart", true);

      await this._Loader.loaderCreate();

      await this.onPageRefresh();

      await this._Loader.loaderDismiss();

    });

    await modal.present();
  }

  goToWalletSettings() {
    this._NavController.navigateForward(
      `/dashboard/payments/wallet-settings?address=${this.data.account.publicKey}`
    );
  }
  async prepareWalletCurrenciesData() {

    if (this.wallet_currencies && this.wallet_currencies.currencies_data) {
      this.wallet_address_index = this.wallet_currencies.wallet_address_index;
      let wallet_currencies_data = this.wallet_currencies.currencies_data;


      let new_data = [];
      this.data.account.network.forEach(async (element, i) => {
        let index = wallet_currencies_data.findIndex(
          (d) => d.currency === element.currency
        );
        if (index !== -1) {
          // console.log("element is", element);
          wallet_currencies_data[index] = {
            ...wallet_currencies_data[index],
            balance: element.balance,
            currency: element.currency,
            name: element.name,
            ens: element.ens,
            chainid: element.chainid,
            eurBalance: await this.chartService.getConversion(
              element.currency,
              this.DEFAULT_FIAT,
              element.balance
            ),
          };
          // console.log('preparing object', index,  wallet_currencies_data[index]);


        }
        if (i >= this.data.account.network.length - 1) {
          this.wallet_currencies_data = uniqBy(wallet_currencies_data.filter(we => !!we.active), 'currency') ;
          console.log("this.wallet_currencies_data", this.wallet_currencies_data);
          INIT_SIZE_MAX = 3 * (this.wallet_currencies_data.length + 2);

        }
      });

    } else {
      await this.cryptoCurrencyService.setInitialWalletData(
        this.data.account.publicKey
        // ,
        // this.cryptoCurrencyService.all_wallets_currencies_data
      );
      await this.prepareWalletCurrenciesData();
    }
  }

}
