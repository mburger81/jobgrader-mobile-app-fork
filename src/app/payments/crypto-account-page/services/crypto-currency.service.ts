import { Injectable } from "@angular/core";
// import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
// import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import { CryptoCurrency, CryptoNetworkNames, CryptoNetworks } from "src/app/core/providers/wallet-connect/constants";


export const CurrencyToNameMapper = {
  [CryptoCurrency.THXC]:              CryptoNetworkNames.THXC,
  [CryptoCurrency.HMT_POLYGON]:       CryptoNetworkNames.HMT_POLYGON,
  [CryptoCurrency.HMT_SKALE]:         CryptoNetworkNames.HMT_SKALE,
  [CryptoCurrency.EVE]:               CryptoNetworkNames.EVAN,
  [CryptoCurrency.ETH]:               CryptoNetworkNames.ETHEREUM,
  [CryptoCurrency.MATIC]:             CryptoNetworkNames.POLYGON,
  [CryptoCurrency.GNOSIS]:            CryptoNetworkNames.GNOSIS,
  [CryptoCurrency.ARETH]:             CryptoNetworkNames.ARBITRUM,    // Arbitrum
  [CryptoCurrency.BLUR]:              CryptoNetworkNames.BLUE,        // Blue token
  [CryptoCurrency.BNB_SMART]:         CryptoNetworkNames.BNB_SMART,   // BNB Smart Chain and BNB Beacon Chain
  [CryptoCurrency.BNB_BEACON]:        CryptoNetworkNames.BNB_BEACON,   // BNB Smart Chain and BNB Beacon Chain
  [CryptoCurrency.MKR]:               CryptoNetworkNames.MAKER_DAO,   // MakerDAO
  [CryptoCurrency.EGLD]:              CryptoNetworkNames.MULTIVERSX,  // MultiversX
  [CryptoCurrency.NEAR]:              CryptoNetworkNames.NEAR,        // Near
  [CryptoCurrency.OP]:                CryptoNetworkNames.OPTIMISM,    // Optimism
  [CryptoCurrency.XTZ]:               CryptoNetworkNames.TEZOS,       // Tezos
  [CryptoCurrency.UNI]:               CryptoNetworkNames.UNISWAP,     // Uniswap
  [CryptoCurrency.AAVE]:              CryptoNetworkNames.AAVE,        // Aave
  [CryptoCurrency.AE]:                CryptoNetworkNames.AE,          // Aeternity
  [CryptoCurrency.AION]:              CryptoNetworkNames.AION,        // Aion
  [CryptoCurrency.APE]:               CryptoNetworkNames.APE,         // Apecoin
  [CryptoCurrency.AURORA]:            CryptoNetworkNames.AURORA,      // Aurora
  [CryptoCurrency.AVAX]:              CryptoNetworkNames.AVAX,        // Avalanche
  [CryptoCurrency.AXS]:               CryptoNetworkNames.AXS,         // Axie Infinity
  [CryptoCurrency.BUSD]:              CryptoNetworkNames.BUSD,        // Binance USD
  [CryptoCurrency.CELO]:              CryptoNetworkNames.CELO,        // Celo
  [CryptoCurrency.LINK]:              CryptoNetworkNames.LINK,        // ChainLink
  [CryptoCurrency.CRO]:               CryptoNetworkNames.CRO,         // Cronos
  [CryptoCurrency.MANA]:              CryptoNetworkNames.MANA,        // Decentraland Token
  [CryptoCurrency.DOGE]:              CryptoNetworkNames.DOGE,        // Dogecoin
  [CryptoCurrency.DYDX]:              CryptoNetworkNames.DYDX,        // dYdX
  [CryptoCurrency.ENS]:               CryptoNetworkNames.ENS,         // Ethereum Name Service
  [CryptoCurrency.FTM]:               CryptoNetworkNames.FTM,         // Fantom Token
  [CryptoCurrency.FIL]:               CryptoNetworkNames.FIL,         // Filecoin
  [CryptoCurrency.FLOW]:              CryptoNetworkNames.FLOW,        // Flow
  [CryptoCurrency.IMX]:               CryptoNetworkNames.IMX,         // Immutable X
  [CryptoCurrency.CAKE]:              CryptoNetworkNames.CAKE,        // PancakeSwap Token
  [CryptoCurrency.SAND]:              CryptoNetworkNames.SAND,        // Sandbox Token
  [CryptoCurrency.TRON]:              CryptoNetworkNames.TRON,        // Tron Token
  [CryptoCurrency.TWT]:               CryptoNetworkNames.TWT,         // Trust Wallet Token
}

export const CurrencyToRPCMapper = {
  [CryptoCurrency.THXC]:              CryptoNetworks.THXC,
  [CryptoCurrency.HMT_POLYGON]:       CryptoNetworks.HMT_POLYGON,
  [CryptoCurrency.HMT_SKALE]:         CryptoNetworks.HMT_SKALE,
  [CryptoCurrency.EVE]:               CryptoNetworks.EVAN,
  [CryptoCurrency.ETH]:               CryptoNetworks.ETHEREUM,
  [CryptoCurrency.MATIC]:             CryptoNetworks.POLYGON,
  [CryptoCurrency.GNOSIS]:            CryptoNetworks.GNOSIS,
  [CryptoCurrency.ARETH]:             CryptoNetworks.ARBITRUM,    // Arbitrum
  [CryptoCurrency.BLUR]:              CryptoNetworks.BLUE,        // Blue token
  [CryptoCurrency.BNB_SMART]:         CryptoNetworks.BNB_SMART,   // BNB Smart Chain and BNB Beacon Chain
  [CryptoCurrency.BNB_BEACON]:        CryptoNetworks.BNB_BEACON,   // BNB Smart Chain and BNB Beacon Chain
  [CryptoCurrency.MKR]:               CryptoNetworks.MAKER_DAO,   // MakerDAO
  [CryptoCurrency.EGLD]:              CryptoNetworks.MULTIVERSX,  // MultiversX
  [CryptoCurrency.NEAR]:              CryptoNetworks.NEAR,        // Near
  [CryptoCurrency.OP]:                CryptoNetworks.OPTIMISM,    // Optimism
  [CryptoCurrency.XTZ]:               CryptoNetworks.TEZOS,       // Tezos
  [CryptoCurrency.UNI]:               CryptoNetworks.UNISWAP,     // Uniswap
  [CryptoCurrency.AAVE]:              CryptoNetworks.AAVE,        // Aave
  [CryptoCurrency.AE]:                CryptoNetworks.AE,          // Aeternity
  [CryptoCurrency.AION]:              CryptoNetworks.AION,        // Aion
  [CryptoCurrency.APE]:               CryptoNetworks.APE,         // Apecoin
  [CryptoCurrency.AURORA]:            CryptoNetworks.AURORA,      // Aurora
  [CryptoCurrency.AVAX]:              CryptoNetworks.AVAX,        // Avalanche
  [CryptoCurrency.AXS]:               CryptoNetworks.AXS,         // Axie Infinity
  [CryptoCurrency.BUSD]:              CryptoNetworks.BUSD,        // Binance USD
  [CryptoCurrency.CELO]:              CryptoNetworks.CELO,        // Celo
  [CryptoCurrency.LINK]:              CryptoNetworks.LINK,        // ChainLink
  [CryptoCurrency.CRO]:               CryptoNetworks.CRO,         // Cronos
  [CryptoCurrency.MANA]:              CryptoNetworks.MANA,        // Decentraland Token
  [CryptoCurrency.DOGE]:              CryptoNetworks.DOGE,        // Dogecoin
  [CryptoCurrency.DYDX]:              CryptoNetworks.DYDX,        // dYdX
  [CryptoCurrency.ENS]:               CryptoNetworks.ENS,         // Ethereum Name Service
  [CryptoCurrency.FTM]:               CryptoNetworks.FTM,         // Fantom Token
  [CryptoCurrency.FIL]:               CryptoNetworks.FIL,         // Filecoin
  [CryptoCurrency.FLOW]:              CryptoNetworks.FLOW,        // Flow
  [CryptoCurrency.IMX]:               CryptoNetworks.IMX,         // Immutable X
  [CryptoCurrency.CAKE]:              CryptoNetworks.CAKE,        // PancakeSwap Token
  [CryptoCurrency.SAND]:              CryptoNetworks.SAND,        // Sandbox Token
  [CryptoCurrency.TRON]:              CryptoNetworks.TRON,        // Tron Token
  [CryptoCurrency.TWT]:               CryptoNetworks.TWT,         // Trust Wallet Token
}


const default_currencies = [
  {
    name: "THXC",
    currency: CryptoCurrency.THXC,
    icon: "../../../../../assets/crypto/THXC.svg",
    active: true,
    disabled: true
  },
  {
    name: "Human Token (Polygon)",
    currency: CryptoCurrency.HMT_POLYGON,
    icon: "../../../../../assets/crypto/HMT_POLYGON.svg",
    active: true,
    disabled: true
  },
  {
    name: "Human Token (SKALE)",
    currency: CryptoCurrency.HMT_SKALE,
    icon: "../../../../../assets/crypto/HMT_SKALE.svg",
    active: true,
    disabled: true
  },
  {
    name: "Ethereum",
    currency: CryptoCurrency.ETH,
    icon: "../../../../../assets/crypto/vector/ETH.svg",
    active: true,
    disabled: true
  },
  {
    name: "Polygon",
    currency: CryptoCurrency.MATIC,
    icon: "../../../../../assets/crypto/vector/MATIC.svg",
    active: true,
    disabled: true
  },
  {
    name: "Gnosis",
    currency: CryptoCurrency.GNOSIS,
    icon: "../../../../../assets/crypto/vector/XDAI.svg",
    active: true,
    disabled: true
  },
  {
    name: "Optimism",
    currency: CryptoCurrency.OP,
    icon: "../../../../../assets/crypto/vector/OP.svg",
    active: true,
    disabled: true
  },

  // {
  //   name: "EVE (EPN Testnet)",
  //   currency: CryptoCurrency.EVE,
  //   icon: "../../../../../assets/crypto/EVE_TEST.svg",
  //   active: false,
  //   disabled: true
  // },
  {
    name: "EVE (EPN Mainnet)",
    currency: CryptoCurrency.EVE,
    icon: "../../../../../assets/crypto/vector/EVE.svg",
    active: false,
    disabled: false
  },

  {
    name: "Aurora",
    currency: CryptoCurrency.AURORA,
    icon: "../../../../../assets/crypto/vector/AURORA.svg",
    active: false,
    disabled: false
  },
  {
    name: "Avalanche",
    currency: CryptoCurrency.AVAX,
    icon: "../../../../../assets/crypto/vector/AVAX.svg",
    active: false,
    disabled: false
  },

  {
    name: "Celo",
    currency: CryptoCurrency.CELO,
    icon: "../../../../../assets/crypto/vector/CELO.svg",
    active: false,
    disabled: false
  },

  {
    name: "Cronos",
    currency: CryptoCurrency.CRO,
    icon: "../../../../../assets/crypto/vector/CRO.svg",
    active: false,
    disabled: false
  },

  {
    name: "Fantom Token",
    currency: CryptoCurrency.FTM,
    icon: "../../../../../assets/crypto/vector/FTM.svg",
    active: false,
    disabled: false
  },
  {
    name: "Filecoin",
    currency: CryptoCurrency.FIL,
    icon: "../../../../../assets/crypto/vector/FIL.svg",
    active: false,
    disabled: false
  },

  // Additional Ones

   {
    name: "BNB Smart Chain",
    currency: CryptoCurrency.BNB_SMART,
    icon: "../../../../../assets/crypto/vector/BNB.svg",
    active: false,
    disabled: true
  },
  {
    name: "BNB Beacon Chain",
    currency: CryptoCurrency.BNB_BEACON,
    icon: "../../../../../assets/crypto/vector/BNB.svg",
    active: false,
    disabled: true
  },
  {
    name: "MakerDAO",
    currency: CryptoCurrency.MKR,
    icon: "../../../../../assets/crypto/vector/MKR.svg",
    active: false,
    disabled: true
  },
  {
    name: "MultiversX",
    currency: CryptoCurrency.EGLD,
    icon: "../../../../../assets/crypto/vector/EGLD.svg",
    active: false,
    disabled: true
  },
  {
    name: "Near Token", // Error RPC Method call
    currency: CryptoCurrency.NEAR,
    icon: "../../../../../assets/crypto/vector/NEAR.svg",
    active: false,
    disabled: true
  },
  {
    name: "Tezos",
    currency: CryptoCurrency.XTZ,
    icon: "../../../../../assets/crypto/vector/XTZ.svg",
    active: false,
    disabled: true
  },
  {
    name: "Uniswap",
    currency: CryptoCurrency.UNI,
    icon: "../../../../../assets/crypto/vector/UNI.svg",
    active: false,
    disabled: true
  },
  {
    name: "ChainLink",
    currency: CryptoCurrency.LINK,
    icon: "../../../../../assets/crypto/vector/LINK.svg",
    active: false,
    disabled: true
  },
  {
    name: "Aave",
    currency: CryptoCurrency.AAVE,
    icon: "../../../../../assets/crypto/vector/AAVE.svg",
    active: false,
    disabled: true
  },
  {
    name: "Aeternity",
    currency: CryptoCurrency.AE,
    icon: "../../../../../assets/crypto/vector/AE.svg",
    active: false,
    disabled: true
  },
  {
    name: "Aion",
    currency: CryptoCurrency.AION,
    icon: "../../../../../assets/crypto/vector/AION.svg",
    active: false,
    disabled: true
  },
  {
    name: "Apecoin",
    currency: CryptoCurrency.APE,
    icon: "../../../../../assets/crypto/vector/APE.svg",
    active: false,
    disabled: true
  },
  {
    name: "Axie Infinity",
    currency: CryptoCurrency.AXS,
    icon: "../../../../../assets/crypto/vector/AXS.svg",
    active: false,
    disabled: true
  },
  {
    name: "Binance USD",
    currency: CryptoCurrency.BUSD,
    icon: "../../../../../assets/crypto/vector/BUSD.svg",
    active: false,
    disabled: true
  },
  {
    name: "Flow",
    currency: CryptoCurrency.FLOW,
    icon: "../../../../../assets/crypto/vector/FLOW.svg",
    active: false,
    disabled: true
  },
  {
    name: "Immutable X",
    currency: CryptoCurrency.IMX,
    icon: "../../../../../assets/crypto/vector/IMX.svg",
    active: false,
    disabled: true
  },
  {
    name: "PancakeSwap",
    currency: CryptoCurrency.CAKE,
    icon: "../../../../../assets/crypto/vector/CAKE.svg",
    active: false,
    disabled: true
  },
  {
    name: "Sandbox Token",
    currency: CryptoCurrency.SAND,
    icon: "../../../../../assets/crypto/vector/SAND.svg",
    active: false,
    disabled: true
  },
  {
    name: "Tron Token",
    currency: CryptoCurrency.TRON,
    icon: "../../../../../assets/crypto/vector/TRX.svg",
    active: false,
    disabled: true
  },
  {
    name: "Trust Wallet Token",
    currency: CryptoCurrency.TWT,
    icon: "../../../../../assets/crypto/vector/TWT.svg",
    active: false,
    disabled: true
  },
  {
    name: "Decentraland Token",
    currency: CryptoCurrency.MANA,
    icon: "../../../../../assets/crypto/vector/MANA.svg",
    active: false,
    disabled: true
  },
  {
    name: "Dogecoin",
    currency: CryptoCurrency.DOGE,
    icon: "../../../../../assets/crypto/vector/DOGE.svg",
    active: false,
    disabled: true
  },
  {
    name: "dYdX",
    currency: CryptoCurrency.DYDX,
    icon: "../../../../../assets/crypto/vector/DYDX.svg",
    active: false,
    disabled: true
  },
  {
    name: "Ethereum Name Service",
    currency: CryptoCurrency.ENS,
    icon: "../../../../../assets/crypto/vector/ENS.svg",
    active: false,
    disabled: true
  },
];
@Injectable({
  providedIn: "root",
})
export class CryptoCurrencyService {
  private all_wallets_currencies_data = [];

  constructor(
    // private secureStorage: SecureStorageService
  ) {
    // this.initAllWalletsCurrencies();
  }
  // async initAllWalletsCurrencies() {
  //   let data_from_storage = await this.getUserCurrenciesListFromStorage();
  //   if (data_from_storage) {
  //     let parsed_data = JSON.parse(data_from_storage);
  //     this.all_wallets_currencies_data = parsed_data;
  //   }
  // }

  async setInitialWalletData(wallet_address: string) {
    let data = [
      {
        [wallet_address]: [...default_currencies],
      },
    ];
    // data = [...previous_data, ...data];
    data = [...this.all_wallets_currencies_data, ...data];
    console.log('setting wallet currencies', data);
    await this.setUserCurrenciesListInStorage(data);
  }
  async setUserCurrenciesListInStorage(data: any[]) {
    // try {
    //   await this.secureStorage.setValue(
    //     SecureStorageKey.walletCurrencies,
    //     JSON.stringify(data)
    //   );
    // } catch (err) {
    //   // mburger: TODO we need to resolve the exceeded quote issue
    //   // when stored value is to big
    //   console.info(err);
    // }

    // await this.initAllWalletsCurrencies();
    this.all_wallets_currencies_data = data;
  }

  getUserCurrenciesListFromStorage() {
    // return this.secureStorage.getValue(SecureStorageKey.walletCurrencies, false);
    return this.all_wallets_currencies_data;
  }

  async getWalletCurrencies(wallet_address: string) {
    let wallet_address_index = -1;
    let result = [];
    let data_from_storage = await this.getUserCurrenciesListFromStorage();
    if (data_from_storage) {
      // let parsed_data = JSON.parse(data_from_storage);
      let parsed_data = data_from_storage;
      // console.log("parsed -darta", parsed_data);
      parsed_data.forEach((ele, index) => {
        Object.keys(ele).forEach((addressKey, index2) => {
          if (addressKey == wallet_address) {
            wallet_address_index = index;
            Object.keys(ele[addressKey]).forEach((currencyKey) => {
              result.push(ele[addressKey][currencyKey]);
            });
          }
        });
      });
      if (!result.length) {
        return null;
      }
    } else {
      return null;
    }
    return {
      currencies_data: [...result],
      wallet_address_index: wallet_address_index,
    };
  }
}


/*
1INCH.svg
2KEY.svg
AAVE.svg
ABBC.svg
ABT.svg
ACT.svg
ADA.svg
ADD.svg
ADK.svg
ADX.svg
AE.svg
AEON.svg
AGIX.svg
AGRS.svg
AION.svg
AKRO.svg
AKT.svg
ALGO.svg
ALPHA.svg
AMB.svg
AMP.svg
AMPL.svg
ANKR.svg
ANT.svg
AOA.svg
APL.svg
APPC.svg
AR.svg
ARDR.svg
ARK.svg
ARNX.svg
ARPA.svg
ARRR.svg
ASAFE.svg
AST.svg
ATOM.svg
AVAX.svg
BAKE.svg
BAL.svg
BAND.svg
BAT.svg
BCD.svg
BCH.svg
BCN.svg
BCPT.svg
BEAM.svg
BELA.svg
BF.svg
BIFI.svg
BITB.svg
BIX.svg
BLK.svg
BLOCK.svg
BLZ.svg
BMC.svg
BNANA.svg
BNB.svg
BNK.svg
BNT.svg
BNTY.svg
BOND.svg
BOS.svg
BRD.svg
BSD.svg
BST.svg
BSV.svg
BTC.svg
BTCP.svg
BTCZ.svg
BTDX.svg
BTG.svg
BTM.svg
BTMX.svg
BTO.svg
BTS.svg
BTT.svg
BTX.svg
BUSD.svg
BZRX.svg
CAKE.svg
CCE.svg
CDAI.svg
CDT.svg
CEL.svg
CELO.svg
CENNZ.svg
CETH.svg
CHAT.svg
CKB.svg
CLAM.svg
CLOAK.svg
CMM.svg
CMT.svg
CND.svg
COB.svg
COLX.svg
COMP.svg
COTI.svg
CREAM.svg
CRPT.svg
CRV.svg
CRW.svg
CS.svg
CTXC.svg
CUSDC.svg
CUSDT.svg
CVC.svg
CVT.svg
DAI.svg
DASH.svg
DAT.svg
DATA.svg
DBC.svg
DCN.svg
DCR.svg
DDX.svg
DENT.svg
DFI.svg
DGB.svg
DGD.svg
DIVI.svg
DLT.svg
DMD.svg
DMT.svg
DNT.svg
DOCK.svg
DOGE.svg
DOT.svg
DRC.svg
DRG.svg
DRGN.svg
DRT.svg
DTA.svg
DTH.svg
EBST.svg
ECA.svg
EDG.svg
EGLD.svg
EGT.svg
EKT.svg
ELA.svg
ELF.svg
ELLA.svg
ELONGATE.svg
EMC.svg
EMC2.svg
ENG.svg
ENJ.svg
EOS.svg
ESBC.svg
ETC.svg
ETH.svg
ETN.svg
ETP.svg
ETZ.svg
EVE.svg
EVX.svg
EWT.svg
EXP.svg
FACE.svg
FCT.svg
FET.svg
FIL.svg
FJC.svg
FLOW.svg
FLUX.svg
FOAM.svg
FRAX.svg
FSN.svg
FTC.svg
FTM.svg
FTT.svg
FUEL.svg
FXS.svg
GAME.svg
GAS.svg
GBX.svg
GBYTE.svg
GIN.svg
GLM.svg
GLS.svg
GNO.svg
GRC.svg
GRIN.svg
GRS.svg
GRT.svg
GSC.svg
GTO.svg
GUSD.svg
GVT.svg
GXC.svg
HAI.svg
HAKKA.svg
HBAR.svg
HC.svg
HEDG.svg
HEX.svg
HIVE.svg
HNS.svg
HNT.svg
HORD.svg
HOT.svg
HPB.svg
HT.svg
HTML.svg
HUSD.svg
HUSH.svg
HYDRO.svg
ICP.svg
ICX.svg
IDEX.svg
IGNIS.svg
INB.svg
INJ.svg
INK.svg
ION.svg
IOP.svg
IOST.svg
IOTX.svg
IQ.svg
IQN.svg
IRIS.svg
ITC.svg
JST.svg
KARMA.svg
KAT.svg
KAVA.svg
KBC.svg
KCS.svg
KEEP.svg
KIN.svg
KLAY.svg
KMD.svg
KNDC.svg
KRB.svg
KSM.svg
LA.svg
LBC.svg
LCC.svg
LEO.svg
LIKE.svg
LINK.svg
LKK.svg
LOOM.svg
LPT.svg
LRC.svg
LRG.svg
LSK.svg
LTC.svg
LTO.svg
LUN.svg
LUNA.svg
LYM.svg
MAID.svg
MANA.svg
MATIC.svg
MCO.svg
MDA.svg
MDS.svg
MDT.svg
MED.svg
MEETONE.svg
META.svg
MIOTA.svg
MIR.svg
MITH.svg
MKR.svg
MLN.svg
MNS.svg
MOAC.svg
MOF.svg
MONA.svg
MPH.svg
MSR.svg
MTH.svg
MTL.svg
MWC.svg
MXC.svg
MYST.svg
NANO.svg
NAS.svg
NAV.svg
NCASH.svg
NCT.svg
NEAR.svg
NEBL.svg
NEO.svg
NEST.svg
NEU.svg
NEW.svg
NEXO.svg
NGC.svg
NIM.svg
NIOX.svg
NLC2.svg
NLG.svg
NMC.svg
NMR.svg
NOIA.svg
NPXS.svg
NU.svg
NULS.svg
NXS.svg
NXT.svg
OAX.svg
OCEAN.svg
ODE.svg
OGN.svg
OK.svg
OKB.svg
OM.svg
OMG.svg
OMNI.svg
ONE.svg
ONG.svg
ONT.svg
ORBS.svg
OST.svg
OURO.svg
OXT.svg
PART.svg
PASC.svg
PAX.svg
PAXG.svg
PAY.svg
PAYX.svg
PBTC.svg
PCX.svg
PEARL.svg
PERL.svg
PINK.svg
PIRL.svg
PIVX.svg
PLBT.svg
PLR.svg
PLU.svg
PMA.svg
PNK.svg
PNT.svg
POE.svg
POLIS.svg
POLY.svg
POT.svg
POWR.svg
PPC.svg
PPP.svg
PPT.svg
PRE.svg
PRO.svg
PROM.svg
PRQ.svg
PTOY.svg
QASH.svg
QKC.svg
QLC.svg
QNT.svg
QRL.svg
QSP.svg
QTUM.svg
QUN.svg
RCN.svg
RDD.svg
RDN.svg
REN.svg
REP.svg
REQ.svg
REV.svg
RIF.svg
RISE.svg
RLC.svg
ROSE.svg
RSR.svg
RSV.svg
RUFF.svg
RUNE.svg
RVN.svg
RYO.svg
SALT.svg
SAN.svg
SAND.svg
SBD.svg
SC.svg
SCRT.svg
SEELE.svg
SFI.svg
SFP.svg
SHIB.svg
SHIFT.svg
SHIP.svg
SIB.svg
SKY.svg
SLR.svg
SLS.svg
SMART.svg
SNGLS.svg
SNOW.svg
SNT.svg
SNX.svg
SOC.svg
SOL.svg
SOUL.svg
SPANK.svg
SPHTX.svg
SRM.svg
SRN.svg
STAK.svg
STAKE.svg
START.svg
STEEM.svg
STORJ.svg
STQ.svg
STRAX.svg
STRONG.svg
STX.svg
SUB.svg
SUMO.svg
SUSHI.svg
SUTER.svg
SWT.svg
SWTH.svg
SXP.svg
SYS.svg
TAU.svg
TBX.svg
TCH.svg
TEL.svg
TEN.svg
TERA.svg
TERN.svg
TFUEL.svg
THETA.svg
THR.svg
TIX.svg
TKN.svg
TKS.svg
TNB.svg
TNC.svg
TOMO.svg
TOR.svg
TORN.svg
TPAY.svg
TRAC.svg
TRB.svg
TRTL.svg
TRX.svg
TT.svg
TTT.svg
TUSD.svg
TZC.svg
UBQ.svg
UMA.svg
UNI.svg
UQC.svg
USDC.svg
USDT.svg
UTK.svg
VEIL.svg
VERI.svg
VET.svg
VIA.svg
VIBE.svg
VITE.svg
VLX.svg
VRC.svg
VSP.svg
VSYS.svg
VTC.svg
VTHO.svg
WAN.svg
WAVES.svg
WAXP.svg
WBTC.svg
WGR.svg
WICC.svg
WING.svg
WINGS.svg
WPR.svg
WTC.svg
XAS.svg
XAUT.svg
XBC.svg
XBY.svg
XCM.svg
XCP.svg
XDAI.svg
XDN.svg
XEM.svg
XHV.svg
XIN.svg
XLM.svg
XMR.svg
XMX.svg
XMY.svg
XOR.svg
XPA.svg
XPM.svg
XPR.svg
XRP.svg
XSR.svg
XTZ.svg
XUC.svg
XVG.svg
XWC.svg
YFI.svg
YOYOW.svg
ZB.svg
ZCL.svg
ZEC.svg
ZIL.svg
ZLW.svg
ZRX.svg
*/