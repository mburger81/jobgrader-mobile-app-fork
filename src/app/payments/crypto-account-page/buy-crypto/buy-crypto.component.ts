import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { TransakService } from 'src/app/core/providers/transak/transak.service';
import { CryptoCurrency, EthereumNetworks } from 'src/app/core/providers/wallet-connect/constants';
import { fiatCurrencies } from 'src/app/core/providers/nft/fiat-currencies';
import { MeldService } from 'src/app/core/providers/meld/meld.service';

@Component({
  selector: 'app-buy-crypto',
  templateUrl: './buy-crypto.component.html',
  styleUrls: ['./buy-crypto.component.scss'],
})
export class BuyCryptoComponent implements OnInit {
  @Input() data: any;

  public form = new UntypedFormGroup({
    crypto: new UntypedFormControl('',[]),
    fiat: new UntypedFormControl('',[]),
    amount: new UntypedFormControl('',[]),
    paymentOption: new UntypedFormControl('',[])
  });
  public currencySet = [ CryptoCurrency.MATIC, CryptoCurrency.ETH, CryptoCurrency.GNOSIS ];
  public fiatSet = [];
  public paymentOptions = [];

  public quote = "";
  public symbol = "";

  constructor(
    private _ModalController: ModalController,
    private _ToastController: ToastController,
    private _ApiProviderService: ApiProviderService,
    private _Meld: MeldService,
    private _Transak: TransakService
  ) { }

  ngOnInit() {

    this._Transak.getCountries()
    .then(r => console.log(r))
    .catch(e => console.log(e));

    // this._Transak.getCryptoCurrencies()
    // .then(r => console.log(r))
    // .catch(e => console.log(e));

    this._Transak.getFiatCurrencies()
      .then(r => {
        console.log(r.response);
        this.fiatSet = Array.from(r.response, (rr: any) => {
          return {
            symbol: rr.symbol, 
            name: rr.name,
            icon: rr.icon,
            paymentOptions: rr.paymentOptions
          }
        });
        
      })
      .catch(e => console.log(e));

    this._Transak.verifyCryptoWalletAddress(this.data, CryptoCurrency.ETH, EthereumNetworks.RINKEBY)
      .then(r => console.log(r))
      .catch(e => console.log(e));
  }

  returnOption(fiatChoice) {
   return `${fiatChoice.symbol} (${fiatChoice.name})`;
  }

  close() {
    this._ModalController.dismiss();
  }

  presentToast(message) {
    this._ToastController.create({
      message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present())
  }

  processPayment() {
    console.log(this.form.value);
  }

  onChange() {
    console.log(this.form.value);
    if(this.form.value.fiat && this.form.value.crypto) {

      this.paymentOptions = [];
      this.paymentOptions = this.fiatSet.find(k => k.symbol == this.form.value.fiat).paymentOptions;

      this._ApiProviderService.cryptoConversion(this.form.value.crypto, this.form.value.fiat).then(r => {
        console.log(r);
        this.symbol = fiatCurrencies.find(k => k.currency_code == this.form.value.fiat).currency_symbol;
        this.quote = (Math.floor((r * Number(this.form.value.amount)) * 100) / 100).toString();

      }).catch(e => {
        console.log(e);
      })

      if(this.form.value.paymentOption) {
        this._Transak.getPrice(this.form.value.fiat, this.form.value.crypto, this.form.value.amount, "ethereum").then(r => {
          console.log(r);
        }).catch(e => {
          console.log(e);
        })
      }

    }
  }

}
