export interface Account {
  heading: string;
  primary?: boolean;
  publicKey: string;
  balance?: string;
  nft_balance?: number;
  icon: string;
  creationDate: Date;
  mnemonic: string;
  color?: string;
}
