import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ethers } from 'ethers';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { CryptoCurrency, Explorers } from 'src/app/core/providers/wallet-connect/constants';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.scss'],
})
export class TransactionDetailComponent implements OnInit {
  @Input() data: any;

  public keyValue = [];
  private url;
  public service;
  public icon;
  private currency;
  public buttonText = '';

  private datePipe = new DatePipe('en-US');

  constructor(
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService,
    private _Router: Router
  ) {
   
   }

  ngOnInit() {
    console.log(this.data);
    var keys = Object.keys(this.data);
    let checker = this._Router.parseUrl(this._Router.url).queryParams;
    console.log(checker);
   
    if(this.data.chainId) {
      switch(this.data.chainId) {
        // Arbitrun: 42161 
        // BNB Smart Chain: 56 
        // Optimism: 10 
        case 508674158: this.currency = CryptoCurrency.EVE; this.icon = '../../../assets/crypto/vector/EVE.svg'; this.url = `${Explorers.EVAN_TEST}${this.data.hash}`; this.service = `evan testexplorer`; break;
        case 49262: this.currency = CryptoCurrency.EVE; this.icon = '../../../assets/crypto/vector/EVE.svg'; this.url = `${Explorers.EVAN}${this.data.hash}`; this.service = `evan explorer`; break;
        case 80001: this.currency = CryptoCurrency.MATIC; this.icon = '../../../assets/crypto/vector/MATIC.svg'; this.url = `${Explorers.POLYGON_TEST_MUMBAI}${this.data.hash}`; this.service = `mumbai-polygonscan`; break;
        case 137: this.currency = CryptoCurrency.MATIC; this.icon = '../../../assets/crypto/vector/MATIC.svg'; this.url = `${Explorers.POLYGON}${this.data.hash}`; this.service = `polygonscan`; break;
        case 100: this.currency = CryptoCurrency.GNOSIS; this.icon = '../../../assets/crypto/vector/XDAI.svg'; this.url = `${Explorers.GNOSIS}${this.data.hash}`; this.service = `gnosis`; break;
        case 4: this.currency = CryptoCurrency.ETH; this.icon = '../../../assets/crypto/vector/ETH.svg'; this.url = `${Explorers.ETHEREUM_TEST_RINKEBY}${this.data.hash}`; this.service = `rinkeby etherscan`; break;
        case 1: this.currency = CryptoCurrency.ETH; this.icon = '../../../assets/crypto/vector/ETH.svg'; this.url = `${Explorers.ETHEREUM}${this.data.hash}`; this.service = `etherscan`; break;
        default: this.currency = "";  this.icon = null ;this.url = `${Explorers.ETHEREUM}${this.data.hash}`; this.service = `etherscan`; break;
      }
    }

    if(checker.currency) {
      switch(checker.currency) {
        case CryptoCurrency.EVE: this.currency = CryptoCurrency.EVE; this.icon = '../../../assets/crypto/vector/EVE.svg'; this.url = (environment.production ? `${Explorers.EVAN}${this.data.hash}` : `${Explorers.EVAN_TEST}${this.data.hash}`) ; this.service = (environment.production ? `evan explorer` : `evan testexplorer`); break;
        case CryptoCurrency.MATIC: this.currency = CryptoCurrency.MATIC; this.icon = '../../../assets/crypto/vector/MATIC.svg'; this.url = (environment.production ? `${Explorers.POLYGON}${this.data.hash}` : `${Explorers.POLYGON_TEST_MUMBAI}${this.data.hash}`) ; this.service = (environment.production ? `polygonscan` : `mumbai-polygonscan`); break;
        case CryptoCurrency.GNOSIS: this.currency = CryptoCurrency.GNOSIS; this.icon = '../../../assets/crypto/vector/XDAI.svg'; this.url = `${Explorers.GNOSIS}${this.data.hash}`; this.service = `gnosis`; break;
        case CryptoCurrency.ETH: this.currency = CryptoCurrency.ETH; this.icon = '../../../assets/crypto/vector/ETH.svg'; this.url = (environment.production ? `${Explorers.ETHEREUM}${this.data.hash}` : `${Explorers.ETHEREUM_TEST_RINKEBY}${this.data.hash}`) ; this.service = (environment.production ? `etherscan` : `rinkeby etherscan`); break;
        default: this.currency = CryptoCurrency.ETH; this.icon = null ; this.url = `${Explorers.ETHEREUM}${this.data.hash}`; this.service = `etherscan`; break;
      }
    }
    

    if(keys.includes('from') && !!this.data['from']) { this.keyValue.push({key: this._Translate.instant('TRANSACTIONRECEIPT.from'), value: this.data['from']}) }
    if(keys.includes('to') && !!this.data['to']) { this.keyValue.push({key: this._Translate.instant('TRANSACTIONRECEIPT.to'), value: this.data['to']}) }
    if(keys.includes('value') && !!this.data['value'] && !!this.data['value'].hex) { this.keyValue.push({key: `${this._Translate.instant('TRANSACTIONRECEIPT.value')} ${this.currency}`, value: `${ethers.utils.formatEther(ethers.BigNumber.from(this.data['value'].hex))} ${this.currency}`}) }
    if(keys.includes('value') && !!this.data['value'] && !!this.data['value']._hex && !this.data['value'].hex) { this.keyValue.push({key: `${this._Translate.instant('TRANSACTIONRECEIPT.value')} ${this.currency}`, value: `${ethers.utils.formatEther(ethers.BigNumber.from(this.data['value']._hex))} ${this.currency}`}) }
    // if(keys.includes('gasPrice') && !!this.data['gasPrice'] && !!this.data['gasPrice'].hex) { this.keyValue.push({key: `${this._Translate.instant('TRANSACTIONRECEIPT.gasPrice')} ${this.currency}`, value: `${ethers.utils.formatEther(ethers.BigNumber.from(this.data['gasPrice'].hex))} ${this.currency}`}) }
    // if(keys.includes('gasPrice') && !!this.data['gasPrice'] && !!this.data['gasPrice']._hex && !this.data['gasPrice'].hex) { this.keyValue.push({key: `${this._Translate.instant('TRANSACTIONRECEIPT.gasPrice')} ${this.currency}`, value: `${ethers.utils.formatEther(ethers.BigNumber.from(this.data['gasPrice']._hex))} ${this.currency}`}) }
    if(keys.includes('timestamp') && !!this.data['timestamp']) { this.keyValue.push({key: this._Translate.instant('TRANSACTIONRECEIPT.timestamp'), value: this.datePipe.transform(this.data['timestamp'], 'yyyy-MM-dd HH:mm:ss')}) }
    if(keys.includes('hash') && !!this.data['hash']) { this.keyValue.push({key: this._Translate.instant('TRANSACTIONRECEIPT.hash'), value: this.data['hash']}) }

    console.log(this.currency);
    console.log(this.icon);
    console.log(this.service);
    console.log(this.url);

    this.buttonText = `${this._Translate.instant('TRANSACTIONRECEIPT.button1')} ${this.service} ${this._Translate.instant('TRANSACTIONRECEIPT.button2')}`;

  }

  viewOnExplorer() {
    var a = document.createElement("a");
    a.style.display = "none";
    a.target = "_blank";
    a.href = this.url;
    a.click();
    a.remove();
  }

  close() {
    this._ModalController.dismiss();
  }

}
