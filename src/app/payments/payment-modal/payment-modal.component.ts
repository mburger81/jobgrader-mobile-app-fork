import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController, AlertController } from '@ionic/angular';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment';
import { interval, Subscription } from 'rxjs';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { Clipboard } from '@capacitor/clipboard';

@Component({
  selector: 'app-payment-modal',
  templateUrl: './payment-modal.component.html',
  styleUrls: ['./payment-modal.component.scss'],
})
export class PaymentModalComponent implements OnInit {
@Input() data: any;
@Input() total: any;
@Input() done: boolean;

  public waiting: boolean = false;
  public icon;
  public datepipe = new DatePipe('en-US');
  private timestampObserver = interval(2000);
  private timestampSubscription: Subscription;
  private privateKey;
  public decrypted = null;
//   public decrypted = {
//     "trust":{
//        "firstname":{
//           "trustId":"27e98e85-ab25-446f-a526-71e10f971331",
//           "trustActive":true,
//           "trustedBy":"Verifeye",
//           "trustLevel":2,
//           "trustValue":1,
//           "created":1660596078276,
//           "modified":1660596570256,
//           "parameterId":null,
//           "userId":"832fc197-79de-4829-afdd-32bac6cb4bef",
//           "userDataFieldId":20001
//        },
//        "lastname":{
//           "trustId":"e7c148a9-0bba-4a48-9dba-219feff43c5a",
//           "trustActive":true,
//           "trustedBy":"Verifeye",
//           "trustLevel":2,
//           "trustValue":1,
//           "created":1660596078245,
//           "modified":1660596570228,
//           "parameterId":null,
//           "userId":"832fc197-79de-4829-afdd-32bac6cb4bef",
//           "userDataFieldId":20002
//        },
//        "dateofbirth":{
//           "trustId":"86c329b6-6b9b-4ad8-9d97-0d1c58d3341b",
//           "trustActive":true,
//           "trustedBy":"Verifeye",
//           "trustLevel":2,
//           "trustValue":1,
//           "created":1660596078290,
//           "modified":1660596570266,
//           "parameterId":null,
//           "userId":"832fc197-79de-4829-afdd-32bac6cb4bef",
//           "userDataFieldId":30001
//        }
//     },
//     "nationality":null,
//     "firstname":"Mariano",
//     "lastname":"Graf Von Plettenberg",
//     "dateofbirth":807840000000,
//     "checks":{
//        "KYC_AML":[
//           "vc:evan:core:0x45a5dafdc5fa0c766d8120293117379a46eeb13c11ceb9153ed0b0ef505a1963",
//           "vc:evan:core:0x4ff5fee90fbfdbc0bd0203ed5ee31662f1b96ad2c31cf940e18c7d6d4d0f7d38",
//           "vc:evan:core:0xac1ff72507d06f8339ce85b36850522c3420b6bc57dcb5cc9628dad9e7e66ffb",
//           "vc:evan:core:0xcff8b05a4f2bdd01dc2c705bca763d91b4cdb86cdb28d36e9f159518b496b1bf"
//        ]
//     }
//  };

  private dataAccessProcess = {
    STARTED: 1,
    ACCESS_GRANTED: 2,
    ACCESS_DENIED: 4,
    DATA_SAVED: 8,
    DATA_TRANSFERRED: 16,
    DATA_TIMEOUT: 32,
    TIMEOUT: 64
  }

  constructor(
    private _ModalController: ModalController,
    private _ApiProviderService: ApiProviderService,
    private _ToastController: ToastController,
    private _CryptoProviderService: CryptoProviderService,
    private _SecureStorageService: SecureStorageService,
    private _Translate: TranslateProviderService,
    
    private _AlertController: AlertController
  ) { }

  safeJSONparse(content: any) {
    let parse = null;
    try {
      parse = JSON.parse(content);
    }catch(e) {
      parse = content;
    }
    return parse;
  }

  ngOnInit() {
    console.log(this.data);
    this.icon = (this.data.currency == 'MATIC') ? `../../../assets/crypto/vector/${this.data.currency}.svg` : `../../../assets/crypto/vector/${this.data.currency}.svg`;
    console.log(this.data.pendingProcesses);
    this.decrypted = !!this.data.trustObject ? this.safeJSONparse(this.data.trustObject) : null;
    // if(this.data.status == 0) {
    //   if(this.data.pendingProcesses.length > 0) {
    //     this.waiting = true;
    //     this.privateKey = this.data.privateKey;
    //     this.timeSubscription(this.data.pendingProcesses[0]);
    //   }
    // }
  }

  check() {
    this.waiting = true;
    console.log("check()");
    console.log(this.data.userPublicKey);

    this._ApiProviderService.getUsernameFromEthAddress(this.data.userPublicKey).then(targetUsername => {
      const body = {
        "username": targetUsername,
        "collectionName": "transaction-confirmation",
        "webhookUrl": `${environment.apiUrl}/helix/test/dataaccesswebhook`,
        "check": [
            {
                "KYC_AML": {
                    "KYC_VC": true,
                    "paymentRequestId": this.data.paymentRequestId,
                    "timestamp": this.data.timestamp,
                    "amount": this.data.amount,
                    "currency": this.data.currency
                }
            }
        ]
      };
      console.log(body);
      this._ApiProviderService.initDataaccessprocess(body).then(response => {
        console.log(response);
        this.privateKey = response.privateKey;
        this.data.privateKey = this.privateKey;
        this.timeSubscription(response.dataAccessProcess.dataAccessProcessId);
      }).catch(error => {
        console.log(error);
        this.stop();
        this.presentToast(this._Translate.instant('LOADER.somethingWrong'));
      })
    }).catch(er => {
        console.log(er);
        this.stop();
        this.presentToast(this._Translate.instant('LOADER.somethingWrong'));
    })

  }

  timeSubscription(processId: string) {
    this.timestampSubscription = this.timestampObserver.subscribe(() => {
      this._ApiProviderService.fetchUserDecisionDataaccessprocess(processId).then(r => {
        console.log(r);
        this.data.status = 1;
        var checkIndex = this.total.findIndex(t => t.paymentRequestId == this.data.paymentRequestId);
        this.total[checkIndex].status = 1;
        this.stop();
        var encryptedData = r.encryptedData; // x+9+iNyy6ZocDMEaen1I8AQaAFSjF+pyhW6s33hlfkKWtGk9yXNy7M7gIri5RyInIsWjwFz/n7FkNJJ4b42A4CUUO9g05xwY6HoJp8RcxhIQtYN1IpnunJB7ebTuM4DyaOUeN0rtAzugtnGwtAii1JIH4JuwdhpnwFTlcbxSj7sk8QEtigPGZsze2gDqZM+J16Ush6frHJDm1kGJ8MEJ7rsqUjF+z3EKz+y7Q+6Rh4oP7tVcRVq70YJcf45LriAk0BnaaKCNNgQpKV3HhbI9rAwk9aafEQcdFsylKJ2JZMDpTjrUKfmsNXUrDwT3F+d/9iW2ixp80nBEIKwxd3b7CglfNe7hKsxFsEWLrbKAVxi82txU5rva8cjIT4l/UWSWY1ZQp/EPi4ZMm3Gm9DOK5xCWnpxQ5UlnK5DbmtB2oZ41jwcDPUhRiR7B3ar2v2cnyVKl1fmmrVciWOHgEl4xTu7QesaRAgPI6TI7koIUflLWm2hCZI+9xkC9o7xEYz1JxkoJXf+m/wbCiUeivqkMJi0SoTpxEzA5NuXLEYo00XSxbRPFWD15gy5HWj9XUmwGUvj3EYiBnG6tJCmBTQ4aiAE=

          this._CryptoProviderService.decryptAssymmetric(encryptedData, this.privateKey).then(decrypted => {
            console.log(decrypted);
            if(decrypted) {
              this.data.privateKey = null;
              this.data.pendingProcesses = [];
              this.decrypted = this.safeJSONparse(decrypted);
              this.total[checkIndex].trustObject = this.decrypted;
              this._SecureStorageService.setValue(SecureStorageKey.cryptoPaymentRequestsReceived, JSON.stringify(this.total)).then(() => {})
              // this._AlertController.create({
              //   header: "Decrypted Information",
              //   message: decrypted,
              //   buttons: [{
              //     text: 'Ok',
              //     role: 'cancel',
              //     handler: () => {}
              //   }]
              // }).then(alerti => alerti.present())
            }
          })

      }).catch(e => {
        console.log(e);
      })
    });
  }



  // {
  //   checks: "[{\"KYC_AML\":{\"KYC_VC\":true,\"timestamp\":1648904313026,\"amount\":\"4.59\",\"currency\":\"BTC\"}}]"
  //   collectionName: "dateofbirth"
  //   created: 1648904322444
  //   dataAccessProcessId: "ec905efd-caf5-4c32-af16-e335966d29a6"
  //   encryptedData: "x+9+iNyy6ZocDMEaen1I8AQaAFSjF+pyhW6s33hlfkKWtGk9yXNy7M7gIri5RyInIsWjwFz/n7FkNJJ4b42A4CUUO9g05xwY6HoJp8RcxhIQtYN1IpnunJB7ebTuM4DyaOUeN0rtAzugtnGwtAii1JIH4JuwdhpnwFTlcbxSj7sk8QEtigPGZsze2gDqZM+J16Ush6frHJDm1kGJ8MEJ7rsqUjF+z3EKz+y7Q+6Rh4oP7tVcRVq70YJcf45LriAk0BnaaKCNNgQpKV3HhbI9rAwk9aafEQcdFsylKJ2JZMDpTjrUKfmsNXUrDwT3F+d/9iW2ixp80nBEIKwxd3b7CglfNe7hKsxFsEWLrbKAVxi82txU5rva8cjIT4l/UWSWY1ZQp/EPi4ZMm3Gm9DOK5xCWnpxQ5UlnK5DbmtB2oZ41jwcDPUhRiR7B3ar2v2cnyVKl1fmmrVciWOHgEl4xTu7QesaRAgPI6TI7koIUflLWm2hCZI+9xkC9o7xEYz1JxkoJXf+m/wbCiUeivqkMJi0SoTpxEzA5NuXLEYo00XSxbRPFWD15gy5HWj9XUmwGUvj3EYiBnG6tJCmBTQ4aiAE="
  //   fieldList: "dateofbirth"
  //   fk_inquiryuser: "ce5fc524-3ee9-4d4b-91f1-f7aa98ced1a9"
  //   fk_user: "85804125-a0d0-4dae-850d-6e7ecc5a35c5"
  //   publicKey: "BC4FMEyzx05QtAo9MJxsdPlZrkNb+VeGp6tA+d+ROdPO83AhOWwz7BJr9LXQakqJ2bD/KyNW05VL/uwawXxXMa0="
  //   publicKeyDelegate: "0xA75Fd2Fefb1Ffe36b6525fd2E886768b1AB9aff7"
  //   sent: 1648904326632
  //   signature: "0x4fa0ec63ca8be39c249655598a059ec5e30672db5257aa51ffa43fd8e97b5b810f44539f1078c90f97cb26b78b1401eb606950e7449220c3f1aec73a1d9a01bf1c"
  //   status: 16
  //   webhookNextSendTimer: null
  //   webhookUrl: "https://appcluster.helixid.io/helix/test/dataaccesswebhook"
  // }

  presentToast(message: string) {
    this._ToastController.create({ message, duration: 2000, position: 'top' }).then(toast => toast.present());
  }

  stop() {
    this.waiting = false;
    this.timestampSubscription.unsubscribe();
    console.log("stop()");
  }

  close() {
    if(this.timestampSubscription){
      if(!this.timestampSubscription.closed) {
        this.timestampSubscription.unsubscribe();
      }
    }
    this._ModalController.dismiss();
  }

  copyDecryptedContent() {

    const copyText = `
    ${this._Translate.instant('TFRUSECASE.firstname')}\t${this.decrypted.firstname}\n
    ${this._Translate.instant('TFRUSECASE.verifiedAt')}\t${!!this.decrypted.trust.firstname ? (this.datepipe.transform(this.decrypted.trust.firstname.modified,'dd.MM.yyyy HH:mm')) : '-'}\n
    ${this._Translate.instant('TFRUSECASE.trustlevel')}\t${!!this.decrypted.trust.firstname ? this.decrypted.trust.firstname.trustLevel : '-'}\n

    ${this._Translate.instant('TFRUSECASE.lastname')}\t${this.decrypted.lastname}\n
    ${this._Translate.instant('TFRUSECASE.verifiedAt')}\t${!!this.decrypted.trust.lastname ? (this.datepipe.transform(this.decrypted.trust.lastname.modified,'dd.MM.yyyy HH:mm')) : '-'}\n
    ${this._Translate.instant('TFRUSECASE.trustlevel')}\t${!!this.decrypted.trust.lastname ? this.decrypted.trust.lastname.trustLevel : '-'}\n

    ${this._Translate.instant('TFRUSECASE.dateofbirth')}\t${this.decrypted.dateofbirth}\n
    ${this._Translate.instant('TFRUSECASE.verifiedAt')}\t${!!this.decrypted.trust.dateofbirth ? (this.datepipe.transform(this.decrypted.trust.dateofbirth.modified,'dd.MM.yyyy HH:mm')) : '-'}\n
    ${this._Translate.instant('TFRUSECASE.trustlevel')}\t${!!this.decrypted.trust.dateofbirth ? this.decrypted.trust.dateofbirth.trustLevel : '-'}\n

    ${this._Translate.instant('TFRUSECASE.amlCheck')}\t${this._Translate.instant('TFRUSECASE.trusted')}\n
    ${this._Translate.instant('TFRUSECASE.walletCheck')}\t${this._Translate.instant('TFRUSECASE.trusted')}\n
    ${this._Translate.instant('TFRUSECASE.processId')}\t${this.data.paymentRequestId}\n
    `;
    console.log(copyText);
    Clipboard.write({ string: copyText}).then(() => {
      this.presentToast('KYC-AML Status' + this._Translate.instant('SETTINGSACCOUNT.copied'));
    })

  }

}
