import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  AfterViewInit,
} from "@angular/core";
@Component({
  selector: "app-wallet-card",
  templateUrl: "./wallet-card.component.html",
  styleUrls: ["./wallet-card.component.scss"],
})
export class WalletCardComponent implements OnInit {
  @Output("onSettingsClick") onSettingsClick = new EventEmitter<any>();
  @Output("onShowAccount") onShowAccount = new EventEmitter<any>();
  @Output("onTrackNFT") onTrackNFT = new EventEmitter<any>();
  @Output("onNavigateClick") onNavigateClick = new EventEmitter<any>();
  @Output("onMoreClick") onMoreClick = new EventEmitter<any>();
  @Output("onCopyClick") onCopyClick = new EventEmitter<any>();
  @Input("symbol") symbol: any;
  @Input("account") account: any;
  @Input("indice") indice: any;
  @Input("color") color?: any;

  copyIcon = '../../assets/crypto/copy.svg';
  
  constructor() {
    
   }

  ngOnInit() {

    if(!this.color) {
      var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
      var random = Math.floor(Math.random() * colorschemes.length);
      this.color = colorschemes[random];
    }

    // this.verifiedWallet =    (!!this.account.primary   &&    !!this.account.mnemonic);
    // this.unverifiedWallet =  (!this.account.primary    &&    !!this.account.mnemonic);
    // this.inactiveWallet =    (!!this.account.primary   &&    !this.account.mnemonic);
    // this.monitoringWallet =  (!this.account.primary    &&    !this.account.mnemoni);

  }

  processDate(d) {
    return ('0' + (new Date(d).getMonth() + 1)).slice(-2) + '/' + (new Date(d).getFullYear().toString().slice(2));
  }

  flipMethod() {
    var elem = document.getElementById(`card-${this.account.publicKey}`);
    var card = elem.querySelector(`.creditcard`);
    
    if (card.classList.contains('flipped')) {
      card.classList.remove('flipped');
    } else {
      card.classList.add('flipped');
    }
  }

  emitSettingsClick() {
    this.onSettingsClick.emit();
  }
  emitShowAccount() {
    this.onShowAccount.emit();
  }
  emitTrackNFTS() {
    this.onTrackNFT.emit();
  }
  emitNavigation(path: string) {
    this.onNavigateClick.emit(path);
  }

  emitMoreClick() {
    this.onMoreClick.emit();
  }

  emitCopyClick() {
    this.onCopyClick.emit();
  }
}
