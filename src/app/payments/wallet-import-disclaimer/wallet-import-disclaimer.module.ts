import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { WalletImportDisclaimerComponent } from './wallet-import-disclaimer.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
  ],
  declarations: [WalletImportDisclaimerComponent],
//   entryComponents: [WalletImportDisclaimerComponent],
  exports: [WalletImportDisclaimerComponent]
})
export class WalletImportDisclaimerModule {}
