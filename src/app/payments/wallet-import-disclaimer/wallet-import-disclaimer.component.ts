import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-wallet-import-disclaimer',
  templateUrl: './wallet-import-disclaimer.component.html',
  styleUrls: ['./wallet-import-disclaimer.component.scss'],
})
export class WalletImportDisclaimerComponent implements OnInit {

  constructor(
    private _ModalController: ModalController
  ) { }

  ngOnInit() {}

  close() {
    this._ModalController.dismiss();
  }

}
