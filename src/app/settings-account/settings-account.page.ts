import { Component, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { AlertController, ToastController, NavController, Platform } from '@ionic/angular';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { AlertsProviderService } from '../core/providers/alerts/alerts-provider.service';
import { ResponseHandlerService } from '../core/providers/response/response-handler.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { Subject, combineLatest } from 'rxjs';
import { take, takeUntil, switchMap } from 'rxjs/operators';
import { Clipboard } from '@capacitor/clipboard';
import { User } from '../core/models/User';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { PushService } from '../core/providers/push/push.service';
import { SignProviderService } from '../core/providers/sign/sign-provider.service';
import { NetworkService } from '../core/providers/network/network-service';
import { KeyRecoveryBackupService } from '../core/providers/key-recovery-backup/key-recovery-backup.service';
import { ChatService } from '../core/providers/chat/chat.service';
import { environment } from '../../environments/environment';
import { GifSetting } from '../shared/components/image-selection/image-selection.component';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
// import { AdjustService } from '../core/providers/adjust/adjust.service';
import { BiometricService } from '../core/providers/biometric/biometric.service';
import { UtilityService } from '../core/providers/utillity/utility.service';
import { EmailComposer, EmailComposerOptions } from '@awesome-cordova-plugins/email-composer/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from '../core/providers/device/udid.enum';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

declare var window: any;

interface Crypto {
    mnemonic: string;
    firebase: string;
    devicePublicKeyDelegate: string;
    devicePrivateKeyDelegate: string;
    devicePrivateKeyEncryption: string;
    devicePublicKeyEncryption: string;
    chatPublicKey: string;
    chatPrivateKey: string;
    didDocument: string;
    verifiableCredentialEncrypted: string;
    userKeyEncryptedVC: string;
    userKeyEncryptedBackup: string;
    userKeyEncryptedUser: string;
    did: string;
    vcId: string;
    hashUDID: string;
    encryptedChatPrivateKey: string;
    userMnemonic: string;
    userPublicKey: string;
    userPrivateKey: string;
}

interface CallnameForm {
  callname: string;
}

@Component({
    selector: 'app-settings-account',
    templateUrl: './settings-account.page.html',
    styleUrls: ['./settings-account.page.scss'],
})
export class SettingsAccountPage implements OnInit, OnDestroy {

    private destroy$ = new Subject();

    // user: User = {};
    public powerUserMode: boolean = false;

    username = '';
    callname = '';
    public user: User = {};
    public callnameForm: CallnameForm;
    public prodEnv = environment.production;
    public editMode = false;
    public moreMode = false;
    public editForm: UntypedFormGroup;
    public crypto: Partial<Crypto> = {};
    public kycStatus: string;
    firebase_token = '';
    mnemonicExists = true;
    public isFaioAvailable = true;
    public viewLockedContent = false;

    private browser: InAppBrowserObject;
    browserOptions = '';
    profilePictureSrc;

    constructor(public secureStorageService: SecureStorageService,
                private alertController: AlertController,
                private userProviderService: UserProviderService,
                private authenticationProviderService: AuthenticationProviderService,
                private apiProviderService: ApiProviderService,
                private secureStorage: SecureStorageService,
                private alertsProviderService: AlertsProviderService,
                private responseHandler: ResponseHandlerService,
                private loaderProviderService: LoaderProviderService,
                private translateProviderService: TranslateProviderService,
                private appStateService: AppStateService,
                private cryptoProviderService: CryptoProviderService,
                private toastController: ToastController,
                private pushService: PushService,
                private signProviderService: SignProviderService,
                private nav: NavController,
                private _KeyRecoveryBackupService: KeyRecoveryBackupService,
                private _Platform: Platform,
                private chat: ChatService,
                private iab: InAppBrowser,
                private platform: Platform,
                private safariViewController: SafariViewController,
                // private _Adjust: AdjustService,
                private biometricService: BiometricService,
                private _EmailComposer: EmailComposer,
                private _InAppBrowser: InAppBrowser,
                private _SocialSharing: SocialSharing,
                private _File: File,
                private _NetworkService: NetworkService,
                private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) {

        this.pushService.newDataReceived$.asObservable()
            .pipe(takeUntil(this.destroy$))
            .subscribe(_ => this.readDataFromSecureStorage());
    }

    unlockContent() {
      if(this.isFaioAvailable) {
        this.faceIdToViewSecrets();
      } else {
        this.checkWithPassword();
      }
    }

    faceIdToViewSecrets() {
      UtilityService.setTimeout(500).pipe(switchMap(_ =>
        this.biometricService.biometricShow()),take(1)).subscribe(() => {
          this.viewLockedContent = true;
      }, _ => {
        this.viewLockedContent = false;
      });
    }

    checkWithPassword() {
      this.alertController.create({
        message: this.translateProviderService.instant('EXPORTKEY.alert-heading'),
        inputs: [{
          type: 'password',
          placeholder: "...",
          name: 'password'
        }],
        buttons: [
          {
            text: this.translateProviderService.instant('BUTTON.CANCEL'),
            role: 'cancel',
            handler: () => {
              this.viewLockedContent = false;
            }
          },
          {
            text: 'Ok',
            handler: async (data) => {
              var plaintextPassword = data.password;
              var passwordHash = await this.secureStorageService.getValue(SecureStorageKey.passwordHash, false);
              var nonce = await this.secureStorageService.getValue(SecureStorageKey.nonce, false);
              if(this.cryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {
                this.viewLockedContent = true;
              } else {
                this.viewLockedContent = false;
                this.presentToast(this.translateProviderService.instant('EXPORTKEY.incorrect-password'));
              }
            }
          }
        ]
      }).then(alerti => alerti.present())
    }

    async mailSecrets() {
      var securePINEncrypted = await this.secureStorage.getValue(SecureStorageKey.securePINEncrypted, false);
      if(securePINEncrypted) {
        var userData = await this.secureStorage.getValue(SecureStorageKey.userData, false);
        if(userData) {
          var user: User = JSON.parse(userData);
          var userKeySymmetric = await this.cryptoProviderService.returnUserSymmetricKey();
          var pin = await this.cryptoProviderService.symmetricDecrypt(securePINEncrypted, userKeySymmetric);
          var mnemonicEncryptionLevel1 = CryptoJS.AES.encrypt(this.crypto.mnemonic, pin).toString();
          var mnemonicEncryptionLevel2 = CryptoJS.AES.encrypt(mnemonicEncryptionLevel1, "083950eff9cd771218fdacb12d9258ab183523e7fffd9a54ba082ece29339ba6").toString();
          var privateKeyEncryptionLevel1 = CryptoJS.AES.encrypt(this.crypto.userPrivateKey, pin).toString();
          var privateKeyEncryptionLevel2 = CryptoJS.AES.encrypt(privateKeyEncryptionLevel1, "083950eff9cd771218fdacb12d9258ab183523e7fffd9a54ba082ece29339ba6").toString();

          let email: EmailComposerOptions = {
            to: user.email,
            subject: 'Exported Secrets',
            body: `
            <h2>Encrypted Mnemonic (Seed phrase)</h2><br>
            <p>${mnemonicEncryptionLevel2}</p><br><br>
            <h2>Encrypted Private Key</h2><br>
            <p>${privateKeyEncryptionLevel2}</p><br><br>
            `,
            isHtml: true
          };
          this._EmailComposer.open(email);
        }

      } else {
        this.presentToast('Please set up a secure 5-digit PIN first.')
      }
    }

    private confirmingForm() {
      return (formGroup: UntypedFormGroup) => {

      };
    }

    async ngOnInit() {

        this.editMode = false;
        this.moreMode = false;
        this.editForm = new UntypedFormGroup({
          callname: new UntypedFormControl('', [ Validators.required ] )},
          { validators: this.confirmingForm });
        this.profilePictureSrc = this.userPhotoServiceAkita.getPhoto();
        this.username = await this.userProviderService.getUsername();
        this.user = await this.userProviderService.getUser();
        console.log(this.user);
        this.callname = this.user.callname;
        this.readDataFromSecureStorage();
        console.log(this.crypto);
        this.isFaioAvailable = (await this.secureStorage.getValue(SecureStorageKey.faioEnabled, false) == 'true');
        console.log(this.isFaioAvailable);
        this.viewLockedContent = false;
    }

    /** @inheritDoc */
    public ngOnDestroy(): void {
      this.destroy$.next(0);
      this.destroy$.complete();
    }

    public ionViewWillEnter() {
        this.secureStorageService.getValue(SecureStorageKey.kycStatus, false).then(kycStatus => {
            this.kycStatus = kycStatus;
        });
        this.secureStorageService.getValue(SecureStorageKey.powerUserMode, false).then(powerUserMode => {
            this.powerUserMode = (powerUserMode == 'true')
        })
    }

    showVCSK() {
      this.cryptoProviderService.returnVCSymmetricKey().then(results => {
        alert(JSON.stringify(results));
      }).catch(e => {
        alert(JSON.stringify(e));
      });
    }

    showBackupSK() {
      this.cryptoProviderService.returnBackupSymmetricKey().then(results => {
        alert(JSON.stringify(results));
      }).catch(e => {
        alert(JSON.stringify(e));
      });
    }

    showUserSK() {
      this.cryptoProviderService.returnUserSymmetricKey().then(results => {
        alert(JSON.stringify(results));
      }).catch(e => {
        alert(JSON.stringify(e));
      });
    }

    async showVC() {
      const results = await this.cryptoProviderService.decryptVerifiableCredential();
      alert(JSON.stringify(results));
    }

    async showChatPassword() {
      const chatUser = await this.chat.getSecureStorageChatUser();
      const result = await this.chat.returnChatPassword(chatUser);
      alert(JSON.stringify(result));
    }

    async showDid() {
      const didDoc = await this.cryptoProviderService.decryptGenericData(await this.secureStorageService.getValue(SecureStorageKey.didDocument, false));
      if(!didDoc) {
        alert(this.translateProviderService.instant('SETTINGSACCOUNT.nodid'));
        return;
      }
      const decryptedDidDoc = await this.cryptoProviderService.decryptGenericData(didDoc);
      alert(JSON.stringify(decryptedDidDoc));
    }

    // async pushKeys() {
    //   await this._KeyRecoveryBackupService.pushKeys();
    // }

    // async getKeys() {
    //   await this._KeyRecoveryBackupService.getKeys();
    // }

    async generateNewKeys() {
      await this._KeyRecoveryBackupService.init();
      this.nav.navigateForward('/settings-account');
    }

    public async ionViewDidEnter() {
      // await this.loaderProviderService.loaderCreate()
      await this.ngOnInit();
      // await this.loaderProviderService.loaderDismiss()
    }

    showPlatform() {
      alert(JSON.stringify(this._Platform.platforms()))
    }

    goBackToSettings() {
      this.nav.navigateBack('/dashboard/tab-settings');
    }

    async deleteUser() {
        const alert = await this.alertController.create({
            mode: 'ios',
            header: '',
            message: this.translateProviderService.instant('SETTINGSACCOUNT.query'),
            buttons: [
                {
                    text: this.translateProviderService.instant('SETTINGS.no'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Deletion cancelled');
                    }
                }, {
                    text: this.translateProviderService.instant('SETTINGS.yes'),
                    cssClass: 'primary',
                    handler: () => {
                        console.log('Deletion confirmed');
                        this.confirmDeletion();
                    }
                }
            ]
        });
        await alert.present();
    }

    async onPress($event, notif: string, value: any) {
        await Clipboard.write({ string: value.toString() });
        await this.presentToast(notif + this.translateProviderService.instant('SETTINGSACCOUNT.copied'));
        // if (['didDocument', 'verifiableCredentialEncrypted'].includes(notif)) {
        //   const dummy = await this.cryptoProviderService.encryptGenericData(value.toString());
        //   const dummyDecrypt = await this.cryptoProviderService.decryptGenericData(dummy);
        //   alert('Encrypted: ' + dummy);
        //   alert('Decoded: ' + dummyDecrypt);
        //   await this.secureStorageService.setValue(SecureStorageKey.didDocument, '');
        //   await this.secureStorageService.setValue(SecureStorageKey.verifiableCredentialEncrypted, JSON.stringify([]));
        //   await this.secureStorageService.setValue(SecureStorageKey.userKeyEncryptedVC, '');
        //   await this.secureStorageService.setValue(SecureStorageKey.did, '');
        //   await this.secureStorageService.setValue(SecureStorageKey.vcId, '');
        // }
    }

    async presentToast(notif: string) {
        const toast = await this.toastController.create({
            message: notif,
            duration: 500,
            position: 'top'
        });
        toast.present();
    }

    async editCallname() {
      this.editMode = true;
      this.callnameForm = {
        callname: this.callname
      };
      this.editForm.controls.callname.setValue(this.callnameForm.callname);
    }

    toggleMore() {
      this.moreMode = !this.moreMode;
    }

    async saveCallname() {
      if (!this.editForm.valid) {
        this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.fillToast'));
        return;
      }
      if (this.editForm.value.callname.length >= 15) {
        this.presentToast(this.translateProviderService.instant('SETTINGSACCOUNT.longlength'));
        return;
      }

      await this.loaderProviderService.loaderCreate();

      const userName = await this.secureStorage.getValue(SecureStorageKey.userName, false);
      const userData = await this.secureStorage.getValue(SecureStorageKey.userData, false);
      // const symmetricKey = await this.cryptoProviderService.returnUserSymmetricKey();
      // const chatPrivateKey = await this.secureStorage.getValue(SecureStorageKey.chatPrivateKey, false);
      // const encryptedChatPrivateKey = await this.cryptoProviderService.symmetricEncrypt(chatPrivateKey, symmetricKey);
      // await this.userProviderService.getUser().then(async res => {
        this.user = JSON.parse(userData);
        const object = {
          user : {
            username: userName,
            password: null,
            email: this.user.email,
            id: this.user.userid,
            // chatPublicKey: this.crypto.chatPublicKey,
            // encryptedChatPrivateKey: encryptedChatPrivateKey
          },
          basicdata : {
            userid: this.user.userid,
            firstname : this.user.firstname,
            lastname : this.user.lastname,
            middlename: this.user.middlename,
            maidenname: this.user.maidenname,
            title : this.user.title,
            dateofbirth: this.user.dateofbirth,
            gender: this.user.gender,
            phone: this.user.phone,
            email: this.user.email,
            callname: this.editForm.value.callname.replace(/\s+/g, '').trim(), //
            street: this.user.street,
            city: this.user.city,
            state: this.user.state,
            zip: this.user.zip,
            maritalstatus: this.user.maritalstatus,
            termsofuse: this.user.termsofuse,
            termsofusethirdparties: this.user.termsofusethirdparties,
            cityofbirth: this.user.cityofbirth,
            countryofbirth: this.user.countryofbirth,
            country: this.user.country,
            citizenship: this.user.citizenship,
            identificationdocumenttype: this.user.identificationdocumenttype,
            identificationdocumentnumber: this.user.identificationdocumentnumber,
            identificationissuecountry: this.user.identificationissuecountry,
            identificationissuedate: this.user.identificationissuedate,
            identificationexpirydate: this.user.identificationexpirydate,
            driverlicencedocumentnumber: this.user.driverlicencedocumentnumber,
            driverlicencecountry: this.user.driverlicencecountry,
            driverlicenceissuedate: this.user.driverlicenceissuedate,
            driverlicenceexpirydate: this.user.driverlicenceexpirydate,
            passportnumber: this.user.passportnumber,
            passportissuedate: this.user.passportissuedate,
            passportexpirydate: this.user.passportexpirydate,
            passportissuecountry: this.user.passportissuecountry,
            residencepermitnumber: this.user.residencepermitnumber,
            residencepermitissuedate: this.user.residencepermitissuedate,
            residencepermitexpirydate: this.user.residencepermitexpirydate,
            residencepermitissuecountry: this.user.residencepermitissuecountry
          }
        };
        console.log('JSON sent for saving user data: ' + JSON.stringify(object));
        var res = await this.apiProviderService.saveUser(this.appStateService.basicAuthToken, object);
        console.log(res);
        if (res) {
          console.log('User Validation Response: ' + JSON.stringify(res));
          if (!!res.errors) {
            console.log(res.errors);
            console.log(res.errors.length);
            if ( res.errors.length == 0 ) {
              console.log('If gets executed');
            } else {
              console.log('Else gets executed');
              let text = '';
              for ( const i of res.errors ) {
                text = text + i.message + '\n';
              }
              this.presentToast(text);
            }
          }
          this.editMode = false;
          this.presentToast(this.translateProviderService.instant('PERSONALDETAILS.savedSuccess'));

          //
          var res2 = await this.apiProviderService.getUserByIdSecure(this.appStateService.basicAuthToken, this.user.userid)
          this.user = res2;
          this.callname = res2.callname;
          console.log('User Value: ' + JSON.stringify(this.user));
          //

        }
        // }).catch(e => {
        //   console.log(e);
        //   this.alertsProviderService.alertCreate('', '', this.responseHandler.getResponseTranslation(e), ['Ok']);
        //  });
      // });
        await this.loaderProviderService.loaderDismiss();
    }

    private async confirmDeletion() {
      if (!this._NetworkService.checkConnection()) {
        this._NetworkService.addOfflineFooter('ion-footer');
        return;
      } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
      }

        await this.loaderProviderService.loaderCreate();
        const basicAuthToken = await this.authenticationProviderService.getBasicAuthToken();
        let deleteResponse = null;
        try {
            deleteResponse = await this.apiProviderService.deleteUser(basicAuthToken);
            console.log(deleteResponse);
            await this.signProviderService.unbrandDeviceFromUser();
            this.appStateService.basicAuthToken = null;

            await this.loaderProviderService.loaderDismiss();
            this.nav.navigateRoot('/onboarding');

        } catch (e) {
            console.log(e);
            await this.loaderProviderService.loaderDismiss();
            await this.alertsProviderService.alertCreate('', '',this.responseHandler.getResponseTranslation(e), ['Ok']);
        }
    }

    async copyGifData() {
      var gif = this.userPhotoServiceAkita.getPhoto();
      await Clipboard.write({ string: gif });
      await this.presentToast('gif copied!')
    }

    async showGifSettings() {
      var g = await this.secureStorage.getValue(SecureStorageKey.gifSetting, false)
      var gifSetting: GifSetting = !!g ? JSON.parse(g) : {
          noOfFrames: 15,
          numFrames: 15,
          frameFloor: false,
          frameDuration: 0.05,
          interval: 0.05,
          gifWidth: 150,
          gifHeight: 150,
          videoDuration: 2,
          gifInterval: 0.0001,
          reverse: true
      };
      console.log(g)
      const alert = await this.alertController.create({
        mode: 'ios',
        header: 'GifSetting',
        // cssClass: 'gifSetting',
        message: '',
        inputs: [
          { name: 'frameDuration', label: 'frameDuration', placeholder: 'frameDuration: ' + gifSetting.frameDuration.toString(), type: 'number'},
          { name: 'frameFloor', label: 'frameFloor', placeholder: 'frameFloor: ' + gifSetting.frameFloor.toString(), type: 'text'},
          { name: 'gifHeight', label: 'gifHeight', placeholder: 'gifHeight: ' + gifSetting.gifHeight.toString(), type: 'number'},
          { name: 'gifWidth', label: 'gifWidth', placeholder: 'gifWidth: ' + gifSetting.gifWidth.toString(), type: 'number'},
          { name: 'interval', label: 'interval', placeholder: 'interval: ' + gifSetting.interval.toString(), type: 'number'},
          { name: 'noOfFrames', label: 'noOfFrames', placeholder: 'noOfFrames: ' + gifSetting.noOfFrames.toString(), type: 'number'},
          { name: 'numFrames', label: 'numFrames', placeholder: 'numFrames: ' + gifSetting.numFrames.toString(), type: 'number'},
          { name: 'videoDuration', label: 'videoDuration', placeholder: 'videoDuration: ' + gifSetting.videoDuration.toString(), type: 'number'},
          { name: 'gifInterval', label: 'gifInterval', placeholder: 'gifInterval: ' + gifSetting.gifInterval.toString(), type: 'number'},
          { name: 'reverse', label: 'reverse', placeholder: 'reverse: ' + gifSetting.reverse.toString(), type: 'text'},
        ],
        buttons: [{
            text: this.translateProviderService.instant('SETTINGS.yes'),
            cssClass: 'primary',
            handler: async (data) => {
              gifSetting = {
                noOfFrames: (data.noOfFrames == "" ? gifSetting.noOfFrames : Number(data.noOfFrames)),
                frameFloor: (data.frameFloor == "" ? gifSetting.frameFloor : (data.frameFloor == "true")),
                frameDuration: (data.frameDuration == "" ? gifSetting.frameDuration : Number(data.frameDuration)),
                numFrames: (data.numFrames == "" ? gifSetting.numFrames : Number(data.numFrames)),
                interval: (data.interval == "" ? gifSetting.interval : Number(data.interval)),
                gifWidth: (data.gifWidth == "" ? gifSetting.gifWidth : Number(data.gifWidth)),
                gifHeight: (data.gifHeight == "" ? gifSetting.gifHeight : Number(data.gifHeight)),
                videoDuration: (data.videoDuration == "" ? gifSetting.videoDuration : Number(data.videoDuration)),
                gifInterval: (data.gifInterval == "" ? gifSetting.gifInterval : Number(data.gifInterval)),
                reverse: (data.reverse == "" ? gifSetting.reverse : (data.reverse == "true")),
              }
              console.log(JSON.stringify(gifSetting))
              await this.secureStorage.setValue(SecureStorageKey.gifSetting, JSON.stringify(gifSetting))
            }
          },
          {
            text: this.translateProviderService.instant('SETTINGS.no'),
            role: 'cancel',
            handler: () => {}
          }
        ]
      });
      await alert.present();

      try {
        var elem = document.getElementsByTagName('ion-alert')[0].childNodes[2] as any
        elem.style.setProperty('overflow-y','scroll')
        elem.style.setProperty('max-height','75%')
      } catch(e) {
        console.log(e)
      }

  }

    private readDataFromSecureStorage(): void {
        combineLatest([
            // this.secureStorageService.getValueAsync$(SecureStorageKey.deviceMnemonic),
            this.secureStorageService.getValueAsync$(SecureStorageKey.firebase),
            // this.secureStorageService.getValueAsync$(SecureStorageKey.devicePrivateKeyDelegate),
            // this.secureStorageService.getValueAsync$(SecureStorageKey.devicePublicKeyDelegate),
            this.secureStorageService.getValueAsync$(SecureStorageKey.devicePrivateKeyEncryption),
            this.secureStorageService.getValueAsync$(SecureStorageKey.devicePublicKeyEncryption),
            this.secureStorageService.getValueAsync$(SecureStorageKey.chatPublicKey),
            this.secureStorageService.getValueAsync$(SecureStorageKey.chatPrivateKey),
            this.secureStorageService.getValueAsync$(SecureStorageKey.didDocument),
            this.secureStorageService.getValueAsync$(SecureStorageKey.verifiableCredentialEncrypted),
            this.secureStorageService.getValueAsync$(SecureStorageKey.userKeyEncryptedVC),
            this.secureStorageService.getValueAsync$(SecureStorageKey.userKeyEncryptedBackup),
            this.secureStorageService.getValueAsync$(SecureStorageKey.userKeyEncryptedUser),
            this.secureStorageService.getValueAsync$(SecureStorageKey.did),
            this.secureStorageService.getValueAsync$(SecureStorageKey.vcId),
            this.secureStorageService.getValueAsync$(SecureStorageKey.hashUDID),
            this.secureStorageService.getValueAsync$(SecureStorageKey.encryptedChatPrivateKey),
            this.secureStorageService.getValueAsync$(SecureStorageKey.userMnemonic),
            this.secureStorageService.getValueAsync$(SecureStorageKey.userPublicKey),
            this.secureStorageService.getValueAsync$(SecureStorageKey.userPrivateKey),
        ]).pipe(
            take(1),
            takeUntil(this.destroy$)
        ).subscribe((
            [
              // mnemonic,
              firebase,
              // devicePrivateKeyDelegate, devicePublicKeyDelegate,
                devicePrivateKeyEncryption, devicePublicKeyEncryption, chatPublicKey, chatPrivateKey
                , didDocument, verifiableCredentialEncrypted, userKeyEncryptedVC, userKeyEncryptedBackup, userKeyEncryptedUser, did, vcId, hashUDID, encryptedChatPrivateKey,
                userMnemonic, userPublicKey, userPrivateKey
            ]) => {
            this.crypto = {
                // mnemonic,
                firebase,
                // devicePrivateKeyDelegate,
                // devicePublicKeyDelegate,
                devicePrivateKeyEncryption,
                devicePublicKeyEncryption,
                chatPublicKey,
                chatPrivateKey,
                didDocument,
                verifiableCredentialEncrypted,
                userKeyEncryptedVC,
                userKeyEncryptedBackup,
                userKeyEncryptedUser,
                did,
                vcId,
                hashUDID,
                encryptedChatPrivateKey,
                userMnemonic, userPublicKey, userPrivateKey
            };
        });
    }

    navigateToKycLandingPage() {
      this.iabCreate('https://jobgrader.app/callbackurltest');
    }

    iabCreate(link: string) {
      if (this.platform.is('ios') && this.platform.is('hybrid')) {
        this.showSafariInstance(link);
      } else if (this.platform.is('android') && this.platform.is('hybrid')) {
        this.browser = this.iab.create(link, '_system', this.browserOptions);
      } else if (!this.platform.is('hybrid')) {
        this.browser = this.iab.create(link, '_system');
      }
    }

    showSafariInstance(url: string) {
      this.safariViewController.isAvailable().then((available: boolean) => {
            if (available) {
              this.safariViewController.show({
                url,
                hidden: false,
                animated: true,
                transition: 'slide',
                enterReaderModeIfAvailable: false,
                tintColor: '#54BF7B'
              })
              .subscribe((result: any) => { },
                (error: any) => console.error(error) );
            } else {
              this.browser = this.iab.create(url, '_self', this.browserOptions);
            }
          }
        );
    }

    testAdjust() {
      // this._Adjust.snippet();
    }

    testAltWalkthrough() {
      this.nav.navigateForward('/alt-walkthrough')
    }


    async exportKeys() {

      var mnemonic = this.crypto.userMnemonic;
      var encryptedMnemonic = JSON.stringify(CryptoJS.AES.encrypt(JSON.stringify(mnemonic), UDIDNonce.userKey).toString());

      let path = null;
      if( this._Platform.is('ios') && this._Platform.is('hybrid') ) {
        path = this._File.documentsDirectory;
      } else if ( this._Platform.is('android') && this._Platform.is('hybrid') ) {
        path = this._File.dataDirectory;
      }

      var blobUrl = URL.createObjectURL(new Blob([encryptedMnemonic], {type: "application/json"}));

      if(this._Platform.is('hybrid')) {
        var fileName = `helixid-userkey-${this.username}-${this.crypto.userPublicKey}.json`;
        var fullPath = path + fileName;

        this._File.writeFile(path,fileName, encryptedMnemonic, { replace: true }).then(() => {
          this._SocialSharing.shareWithOptions({
            subject: fileName,
            files: [ fullPath ]
          })
        }).catch(e => {
          console.log(e)
        })
      } else {
        this.browser = this._InAppBrowser.create(blobUrl, '_system');
      }

    }

}
