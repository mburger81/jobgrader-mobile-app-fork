import { AnimationBuilder, AnimationController, iosTransitionAnimation } from '@ionic/angular';
const animationCtrl = new AnimationController();

export const getIonPageElement = (element: HTMLElement) => {
  if (element.classList.contains('ion-page')) {
    return element;
  }

  const ionPage = element.querySelector(
    ':scope > .ion-page, :scope > ion-nav, :scope > ion-tabs'
  );
  if (ionPage) {
    return ionPage;
  }
  // idk, return the original element so at least something animates and we don't have a null pointer
  return element;
};
export const fancyAnimation = (_: HTMLElement, opts: any) => {
  const backDirection = opts.direction === 'back';
  const enteringEl = opts.enteringEl;
  const leavingEl = opts.leavingEl;

  const enteringPageEl = getIonPageElement(enteringEl);

  const rootTransition = animationCtrl.create();

  const enterTransition = animationCtrl.create();
  const leavingTransition = animationCtrl.create();

  leavingTransition.addElement(getIonPageElement(leavingEl)).duration(250);

  enterTransition
    .addElement(enteringPageEl)
    .duration(250)
    .fill('both')
    .beforeRemoveClass('ion-page-invisible');

  if (!backDirection) {
    enterTransition
      .beforeStyles({ border: 'thin solid black' })
      .keyframes([
        { offset: 0, transform: 'scale(0)' },
        { offset: 1, transform: 'scale(1)' }
      ])
      .afterClearStyles(['border']);

    leavingTransition.keyframes([
      { offset: 0, opacity: 1 },
      { offset: 1, opacity: 0.1 }
    ]);
  } else {
    enterTransition.keyframes([
      { offset: 0, opacity: 0.1 },
      { offset: 1, opacity: 1 }
    ]);

    leavingTransition
      .beforeStyles({ border: 'thin solid black' })
      .keyframes([
        { offset: 0, transform: 'scale(1)' },
        { offset: 1, transform: 'scale(0)' }
      ])
      .afterClearStyles(['border']);
  }

  rootTransition.addAnimation([enterTransition, leavingTransition]);

  return rootTransition;
};

export const modalEnterAnimation = (baseEl: any) => {
  const backdropAnimation = animationCtrl
    .create()
    .addElement(baseEl.querySelector('ion-backdrop')!)
    .fromTo('opacity', '0.01', '0.9')
    .duration(500);

  const wrapperAnimation = animationCtrl
    .create()
    .addElement(baseEl.querySelector('.modal-wrapper')!)
    .delay(500)
    .keyframes([
      { offset: 0, opacity: '0', transform: 'scale(0)' },
      { offset: 1, opacity: '0.99', transform: 'scale(1)' }
    ])
    .duration(250);

  return animationCtrl
    .create()
    .addElement(baseEl)
    .easing('ease-out')
    .addAnimation([backdropAnimation, wrapperAnimation]);
};
export const modalLeaveAnimation = (baseEl: any) => {
  return modalEnterAnimation(baseEl).direction('reverse');
};


import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerModule } from '@angular/platform-browser';
import { RouteReuseStrategy, Router } from '@angular/router';
// import { TooltipsModule } from 'ionic-tooltips';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { Drivers } from '@ionic/storage';
import { IonicStorageModule } from '@ionic/storage-angular';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { AppComponent } from './app.component';
import { FingerprintAIO } from '@awesome-cordova-plugins/fingerprint-aio/ngx';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { ApiModule as TestApiModule, Configuration, ConfigurationParameters } from './model';
import { environment } from '../environments/environment';
import { AppInitializerService } from './app-initializer.service';
import { CustomErrorHandler } from './core/providers/error/custom-error-handler.service';
import { IonicGestureConfig } from './core/providers/gestures/ionic-gesture-config';
import { LockScreenPageModule } from './lock-screen/lock-screen.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';
import { VeriffThanksPageModule } from './veriff-thanks/veriff-thanks.module';
import { Network } from '@awesome-cordova-plugins/network/ngx';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
// import { VideoEditor } from '@ionic-native/video-editor/ngx';
import { customPageAnimation } from './custom-animations/customPageAnimation';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
// import { SqlStorage } from '../sqlStorage/sqlStorage';
import { CodeInputModule } from 'angular-code-input';
// import { CardIO } from '@ionic-native/card-io/ngx';
import { Vibration } from '@awesome-cordova-plugins/vibration/ngx';
import { StreamingMedia } from '@awesome-cordova-plugins/streaming-media/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';

import { WcNetworkSelectionModule } from './core/providers/nft/wc-network-selection/wc-network-selection.module';
import { WcSessionRequestModule } from './core/providers/nft/wc-session-request/wc-session-request.module';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { Wc2SessionRequestModule } from './core/providers/nft/wc2-session-request/wc2-session-request.module';
import { OtpVerificationModule } from './core/providers/key-recovery-backup/otp-verification/otp-verification.module';
import { VerifiableModule } from './core/providers/barcode/verifiable/verifiable.module';
// import { AngularLineawesomeModule, LaIconLibrary } from "angular-line-awesome";
// import { lasCopy } from 'angular-line-awesome/icons';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import capacitorDataStorgeSQLiteDriver from './shared/localforage-capdatastoragesqlitedriver/localforage-capdatastoragesqlitedriver';


import * as SentryAngular from "@sentry/angular-ivy";
import { SQLiteService } from './services/sqlite.service';

export function apiConfigFactory(): Configuration {
    const params: ConfigurationParameters = {
        basePath: environment.apiUrl
    };
    return new Configuration(params);
}

export function languageFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        HammerModule,
        IonicModule.forRoot({
            navAnimation: customPageAnimation
        }),
        AppRoutingModule,
        IonicStorageModule.forRoot({
          name: '_jobgrader',
          version: 1.0,
          storeName: 'ionic-storage',
          driverOrder: [ capacitorDataStorgeSQLiteDriver._driver, Drivers.IndexedDB, Drivers.LocalStorage ],
        }),
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: languageFactory,
                deps: [HttpClient]
            }
        }),
        CoreModule,
        BrowserAnimationsModule,
        // TooltipsModule,
        TestApiModule.forRoot(apiConfigFactory),
        // TooltipsModule.forRoot(),
        LockScreenPageModule,
        WcNetworkSelectionModule,
        OtpVerificationModule,
        VerifiableModule,
        WcSessionRequestModule,
        Wc2SessionRequestModule,
        VeriffThanksPageModule,
        CodeInputModule,
        environment.production ? [] : AkitaNgDevtools.forRoot()
    ],
    providers: [
        AppVersion,
        // StatusBar,
        // Deeplinks,
        InAppBrowser,
        // CardIO,
        FingerprintAIO,
        EmailComposer,
        SQLiteService,
        { provide: HAMMER_GESTURE_CONFIG, useClass: IonicGestureConfig },
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        { provide: ErrorHandler, useClass: CustomErrorHandler },
        {
          provide: SentryAngular.TraceService,
          deps: [
            Router
          ]
        },
        {
          provide: APP_INITIALIZER,
          useFactory: () => () => {},
          deps: [
            SentryAngular.TraceService
          ],
          multi: true,
        },
        AppInitializerService,
        OpenNativeSettings,
        Network,
        // VideoEditor,
        StreamingMedia,
        File, FileOpener, Chooser,
        NativeAudio,
        // SqlStorage,
        Vibration,
        // Adjust,
        SafariViewController,
        Calendar,
        SocialSharing
        //  SealOneService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(library: LaIconLibrary) {
  //   library.addIcons([lasCopy])
  // }
}
