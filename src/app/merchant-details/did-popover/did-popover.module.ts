import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

import { DidPopoverComponent } from './did-popover.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        SharedModule,
        TranslateModule.forChild()
    ],
    providers: [],
    declarations: [DidPopoverComponent],
    exports: [DidPopoverComponent]
})
export class DidPopoverModule {}
