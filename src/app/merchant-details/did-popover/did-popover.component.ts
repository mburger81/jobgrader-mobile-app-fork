import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-did-popover',
  templateUrl: './did-popover.component.html',
  styleUrls: ['./did-popover.component.scss'],
})
export class DidPopoverComponent implements OnInit {
  @Input() data: any;
  @Input() heading: any;
  @Input() button?: boolean;
  @Input() buttonLink?: any;

  constructor(
    private _PopoverController: PopoverController,
  ) { }

  ngOnInit() {}
  
  close() {
    this._PopoverController.dismiss();
  }

  openInstagram() {

    if(this.buttonLink) {
      var a = document.createElement("a");
      a.href = this.buttonLink;
      a.style.display = "none";
      a.target = "_blank";
      a.click();
      a.remove();
    }

  }

}
