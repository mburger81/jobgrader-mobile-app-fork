import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { IonSlide, IonSlides, NavController, ModalController, Platform, AlertController, PopoverController, ToastController } from '@ionic/angular';
import { BehaviorSubject, fromEvent, interval, Subject, Subscription } from 'rxjs';
import { debounceTime, filter, switchMap, take, takeUntil } from 'rxjs/operators';
import { from } from 'rxjs';
import { UtilityService } from '../core/providers/utillity/utility.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MarketplaceService } from '../core/providers/marketplace/marketplace.service';
import { MerchantDetails } from '../core/providers/marketplace/marketplace-model';
import { abTestingExample, audioLabellingExample, captchaExample, formsExample, fortuneExample, priceGuessingExample, statisticsExample, videoLabellingExample } from './merchant-details.example';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { DocumentModalPageComponent } from '../legal-documents/document-modal/document-modal.component';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { ChatService } from '../core/providers/chat/chat.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { environment } from 'src/environments/environment';
import { SQLStorageService } from '../core/providers/sql/sqlStorage.service';
import { DidPopoverComponent } from './did-popover/did-popover.component';
import { BarcodeService } from '../core/providers/barcode/barcode.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { DeeplinkProviderService } from '../core/providers/deeplink/deeplink-provider.service';
import { UDIDNonce } from '../core/providers/device/udid.enum';
import { DatePipe } from '@angular/common';
import { KycService } from '../kyc/services/kyc.service';
import { EmailComposer, EmailComposerOptions } from '@awesome-cordova-plugins/email-composer/ngx';
import * as CryptoJS from 'crypto-js';
import { CalendarService } from '../core/providers/calendar/calendar.service';
import { ContactService } from '../contact/shared/contact.service';
import { User } from '../model';
import { NetworkService } from '../core/providers/network/network-service';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { KycProvider } from '../kyc/enums/kyc-provider.enum';
import { KycState } from '../kyc/enums/kyc-state.enum';

export enum MatomoTracking {
    PageView = 1,
    CTA1Click = 2,
    CTA2Click = 3
}

export enum EventRequirements {
    IDENTIFICATION_DOCUMENT = 'IDENTIFICATION_DOCUMENT',
    COVID19_VACCINATION = 'COVID19_VACCINATION',
    VALID_TICKET = 'VALID_TICKET',
    AGE_RESTRICTION_18 = 'AGE_RESTRICTION_18'
}

interface EventInformation {
    ticketVendorURL?: string;
    clientId?: string;
    userId?: string;
    eventId?: string;
    ticketId?: string;
    requisites?: Array<string>;
}

interface EventUseCase {
    ticketInformation?: any;
    vaccinationCertifiates?: boolean;
    testCertificates?: boolean;
    contactTracing?: any;
}

@Component({
    selector: 'app-merchant-details',
    templateUrl: './merchant-details.page.html',
    styleUrls: ['./merchant-details.page.scss']
})
export class MerchantDetailsPage implements OnInit, AfterViewInit, OnDestroy {
    private static INITIALLY_CARD_MARGIN_TOP = 80;
    private static INITIALLY_SLIDE_1_MARGIN_TOP = 90;
    private static SLIDE_2_CONTENT_INITIAL_HEIGHT_PX = 285;
    private static SLIDE_3_CONTENT_INITIAL_HEIGHT_PX = 388;
    private static SLIDE_23_AVATAR_INITIAL_TOP = 60;

    public tooltipEvent: 'click' | 'press' | 'hover' = 'click';
    public duration: number = 5000;
    public topPosition: string = "top";
    public energyAustralia: boolean = false;
    public eventRequisites = [];
    public isTicketValid: boolean = null;

    private address;
    private source;
    /** Behaviour subject to tell that the scroll edge is over drawn */
    public scrollEdgeOverdrawn$ = new BehaviorSubject<boolean>(false);

    public activeIndex = 0;
    @ViewChild(IonSlides) public slider: IonSlides;
    @ViewChild('page1Scroll') public page1Scroll: ElementRef;
    public cardMarginTop = MerchantDetailsPage.INITIALLY_CARD_MARGIN_TOP;
    public slide1MarginTop = MerchantDetailsPage.INITIALLY_SLIDE_1_MARGIN_TOP;
    public slide2ContentHeightPx = MerchantDetailsPage.SLIDE_2_CONTENT_INITIAL_HEIGHT_PX;
    public slide3ContentHeightPx = MerchantDetailsPage.SLIDE_3_CONTENT_INITIAL_HEIGHT_PX;
    public slide23AvatarTopPx = MerchantDetailsPage.SLIDE_23_AVATAR_INITIAL_TOP;
    public sliderOptions = {
        initialSlide: 0,
        speed: 500
    };

    private destroy$ = new Subject();
    public merchant: MerchantDetails;
    private browser: InAppBrowserObject;
    browserOptions = 'zoom=no,footer=no,hideurlbar=yes,footercolor=#BF7B54,hidenavigationbuttons=yes,presentationstyle=pagesheet';
    public hasSeenMerchantTutorial: boolean = true; // so that it doesn't flash on the screen
    private category: string;
    public qrCode: string = null;
    private datePipe = new DatePipe('en-US');
    public chatUsername: string = '';
    public userSelfQRCode: string = null;
    private timerSubscription: Subscription;
    public qrCodeCaptionCountdown: string = '';
    public qrCodeIsHidden = false;
    public displayableElements = [
        // {
        //     key: this.translateService.instant('OTC.CHECKBOX.firstname'),
        //     value: 'Ansik',
        //     trust: 1
        // }, {
        //     key: this.translateService.instant('OTC.CHECKBOX.lastname'),
        //     value: 'Mahapatra',
        //     trust: 1
        // }, {
        //     key: this.translateService.instant('OTC.CHECKBOX.dateofbirth'),
        //     value: this.datePipe.transform(new Date(1000000000000), 'yyyy-MM-dd'),
        //     trust: 0
        // }, {
        //     key: this.translateService.instant('PERSONALDETAILS.email'),
        //     value: 'ansik91@gmail.com',
        //     trust: 1
        // }, {
        //     key: this.translateService.instant('VERIFICATION.phone'),
        //     value: '+49-15163431898',
        //     trust: 0
        // }, {
        //     key: this.translateService.instant('OTC.QUESTIONS.vaccine'),
        //     value: true,
        //     trust: null
        // }, {
        //     key: this.translateService.instant('OTC.QUESTIONS.test'),
        //     value: false,
        //     trust: null
        // }, {
        //     key: this.translateService.instant('OTC.QUESTIONS.tickets'),
        //     value: null,
        //     trust: null
        // }
];
    displayTime: string;
    alertButtons: { text: any; handler: () => void; }[];
    displayAlert: boolean;
    timestampSubscription: any;
    timestampObserver: any;

    public allowNavigation = {
        videoLabelling: false,
        priceGuessing: false,
        fortune: false,
        forms: false,
        statistics: false,
        audioLabelling: false,
        abTesting: false,
        captcha: false,
    }

    constructor(
        private router: Router,
        public platform: Platform,
        public utils: UtilityService,
        private nav: NavController,
        private activatedRoute: ActivatedRoute,
        public _MarketPlaceService: MarketplaceService,
        private iab: InAppBrowser,
        private safariViewController: SafariViewController,
        private _ModalController: ModalController,
        private _LoaderProviderService: LoaderProviderService,
        private translateService: TranslateProviderService,
        public _SecureStorageService: SecureStorageService,
        public _ChatService: ChatService,
        public _ApiProviderService: ApiProviderService,
        public _AppStateService: AppStateService,
        public _AlertController: AlertController,
        private _SQLStorageService: SQLStorageService,
        private _PopoverController: PopoverController,
        public _BarcodeService: BarcodeService,
        private _UserProviderService: UserProviderService,
        private _CryptoProviderService: CryptoProviderService,
        private _KycService: KycService,
        private _CalendarService: CalendarService,
        private _DeeplinkProviderService: DeeplinkProviderService,
        private _ContactService: ContactService,
        private _EmailComposer: EmailComposer,
        private _ToastController: ToastController,
        private _Theme: ThemeSwitcherService,
        private _NetworkService: NetworkService
    ) {
    }

    ionViewWillEnter() {

        if( !this._NetworkService.checkConnection() ){
            this._NetworkService.addOfflineFooter('ion-footer');
        } else {
            this._NetworkService.removeOfflineFooter('ion-footer');
        }

        let checker = this.router.parseUrl(this.router.url).queryParams;
        console.log(checker);

        if(checker.address) {
            this.address = checker.address;
            console.log(this.address);
        }

        if(checker.source) {
            this.source = checker.source;
            console.log(this.source);
        }

        this._SecureStorageService.getValue(SecureStorageKey.hasSeenMerchantTutorial, false).then(hasSeenMerchantTutorial => {
            this.hasSeenMerchantTutorial = hasSeenMerchantTutorial == 'true';
            // this.hasSeenMerchantTutorial = false;   // debug
        });

        this.category = this.activatedRoute.snapshot.params.key;
        var merchantId = this.activatedRoute.snapshot.params.merchantId;

        console.log("merchantId", merchantId);

        if (merchantId == null) {
            return this.nav.navigateRoot('/dashboard');
        }

        switch(merchantId) {
            case '-1': this.merchant = Object.assign(videoLabellingExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_Video_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/CVAT-dark.svg' }); 
                this.allowNavigation.videoLabelling = true; 
                break;
            case '-2': this.merchant = Object.assign(priceGuessingExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_Search_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/PriceGuessing-dark.svg' }); 
                this.allowNavigation.priceGuessing = true; 
                break;
            case '-3': this.merchant = Object.assign(fortuneExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_Fortune_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/Fortune-dark.svg' }); 
                this.allowNavigation.fortune = true; 
                break;
            case '-4': this.merchant = Object.assign(formsExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_Form_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/FormElements-dark.svg' }); 
                this.allowNavigation.forms = true; 
                break;
            case '-5': this.merchant = Object.assign(statisticsExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_ContentModeration_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/Polls-dark.svg' }); 
                this.allowNavigation.statistics = true; 
                break;
            case '-6': this.merchant = Object.assign(audioLabellingExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_Audio_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/Audino-dark.svg' });
                this.allowNavigation.audioLabelling = true;  
                break;
            case '-7': this.merchant = Object.assign(abTestingExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_AB_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/AB-dark.svg' }); 
                this.allowNavigation.abTesting = true; 
                break;
            case '-8': this.merchant = Object.assign(captchaExample, { 
                productBackgroundImage: '../../assets/icon/job-categories/headers/Jobgrader_Captcha_cropped.gif', 
                merchantLogo: '../../assets/icon/job-categories/hCaptcha-dark.svg' }); 
                this.allowNavigation.captcha = true; 
                break;
            default: this.merchant = this._MarketPlaceService.getMerchantById(merchantId); break;
        }

        console.log("this.merchant", this.merchant);

        if (!this.merchant) {
            return this.nav.navigateRoot('/dashboard');
        }

        this.trackMatomo(MatomoTracking.PageView, `${environment.helixEnv} - ${merchantId}`);

        if(this.merchant.qrCode) {
            // console.log(this.merchant.qrCode);
            this.setQrCode();
        }

        if(environment.helixEnv == 'integration') {
            if(this.merchant.id == 144 || this.merchant.id == 151) {
                this.energyAustralia = true;
                this._UserProviderService.getUser().then(user => {
                    var ob = {
                        id: user.userid,
                        firstname: user.firstname,
                        lastname: user.lastname,
                        dob: user.dateofbirth
                    }
                    this._CryptoProviderService.symmetricEncrypt(JSON.stringify(ob), UDIDNonce.helix as any).then((s) => {
                        this.userSelfQRCode = s;
                    })
                })
            }
        }

        this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false).then(chatUserData => {
            var cud = !!chatUserData ? JSON.parse(chatUserData) : null;
            if(cud) {
                this.chatUsername = cud.username;
            }
        })

        interval(1000).pipe(
            take(10),
            filter(_ => !!this.page1Scroll),
            take(1),
            switchMap(_ => fromEvent(this.page1Scroll.nativeElement, 'scroll')),
            debounceTime(200),
            takeUntil(this.destroy$)
        ).subscribe((e: any) => {
            const {scrollTop} = e.target;
            this.scrollEdgeOverdrawn$.next(scrollTop > 17);
        });

        // UtilityService.setTimeout(2000).subscribe(_ => {
        //     try {

        //         this.slide2ContentHeightPx =
        //         (document.querySelector('.merchant-page2-cta-button-container') as any).offsetTop -
        //         (document.querySelector('.slide2 ion-card') as any).offsetTop - 50;
        //         this.slide3ContentHeightPx =
        //         (document.querySelector('.swiper-pagination') as any).offsetTop -
        //         ((document.querySelector('.slide3 ion-card') as any).offsetTop +
        //         (document.querySelector('.page-3-content') as any).offsetTop
        //         );
        //     }
        //     catch(e) {
        //         // error in slide rearrangement - should probably fix other than a timeout as above.
        //     }
        // });
    }

    setQrCode() {
        this._BarcodeService.setCertificates().then(() =>{
            this.processQrCode(this.merchant.qrCode, this.merchant.did).then(code => {
                // this.qrCode = code;
                if(code) {
                    this.startQrTimer(code);
                }
                // console.log("this.qrCode: " + this.qrCode);
                var gh = JSON.parse(JSON.parse(this.merchant.qrCode));
                this._KycService.isUserAllowedToUseChatMarketplace().then(idCheck => {
                    this._UserProviderService.getUser().then(user => {
                        this._BarcodeService.setHealthCertificates().then(() => {
                            var vaccinationCerts = this._BarcodeService.healthCertificates.filter(k => !!k.v);
                            vaccinationCerts = vaccinationCerts.filter(l => {
                                var ll = Math.max(...Array.from(l.v, m => m["dn"]));
                                var lm = Math.max(...Array.from(l.v, m => m["sd"]));
                                return ll == lm;
                            });
                            var testCerts = this._BarcodeService.healthCertificates.filter(k => !!(k as any).t);
                            testCerts = testCerts.filter(tt => {
                                var st1 = Array.from((tt as any).t, ttt => ttt["tr"]);
                                var st2 = Array.from((tt as any).t, ttt => ttt["r"]);
                                return (st1.includes("260415000") || st2.includes("n"));
                            });
                            testCerts = this.checkTestDateValidity(testCerts);
                            var vaccinationTestBoolean = (testCerts.length > 0 || vaccinationCerts.length > 0);
                            var checkName = (u1, u2) => {
                                var ps = (u) => { return u.toLowerCase().trim().replace(' ','').replace('ä','ae').replace('ö','oe').replace('ü','ue') };
                                var ps1 = ps(u1); var ps2 = ps(u2);
                                var minL = Math.min(ps1.length, ps2.length);
                                return (ps1.indexOf(ps2) > -1) || (ps2.indexOf(ps1) > -1);
                              }
                            // console.log(vaccinationCerts);
                            var checkBoolVaccinationCerts = (vaccinationCerts.length > 0) ? vaccinationCerts.map(vcse => ((user.dateofbirth == +new Date(vcse.dob)) && checkName(vcse.nam.gn, user.firstname) && checkName(vcse.nam.fn, user.lastname))).reduce((a, b) => (a || b)) : false;
                            var checkBoolTestCerts = (testCerts.length > 0) ? testCerts.map(tse => {
                                var checkOb = this.returnTestCertCleanInfo(tse);
                                return ((user.dateofbirth == checkOb.dob) && checkName(checkOb.fn, user.firstname) && checkName(checkOb.ln, user.lastname))
                            }).reduce((a, b) => (a || b)) : false;
                            // console.log(checkBoolVaccinationCerts);
                            // console.log(checkBoolTestCerts);
                            var ageCheck = (user.dateofbirthStatus == 1) ? (Math.floor(new Date().getFullYear() - new Date(user.dateofbirth).getFullYear()) >= 18) : false;
                            (gh as any).requisites.forEach(rq => {
                                this.eventRequisites.push({
                                    title: rq,
                                    value: (rq == EventRequirements.IDENTIFICATION_DOCUMENT) ? (idCheck && ((user.firstnameStatus == 1) && (user.lastnameStatus == 1) && (user.dateofbirthStatus == 1))) :
                                        (rq == EventRequirements.COVID19_VACCINATION ? (checkBoolVaccinationCerts || checkBoolTestCerts) :
                                        (rq == EventRequirements.VALID_TICKET ? !!this.qrCode : (rq == EventRequirements.AGE_RESTRICTION_18 ? ageCheck : false)))
                                })
                            });
                        })
                    })
                })
            });
        })
    }

    slideWillChange() {
        this.slider.ionSlideWillChange.pipe(
            switchMap(__ => from(this.slider.getActiveIndex())),
            takeUntil(this.destroy$)
        ).subscribe((activeIndex: number) => {
            if ( activeIndex === 1 || activeIndex === 2 || activeIndex === 4 || activeIndex === 5 ) {
                this.scrollEdgeOverdrawn$.next(false);
            }
        });
    }

    returnTestCertCleanInfo(tse: any) {
        var returnObject = {
            fn: null,
            ln: null,
            dob: null
        }
        if(tse.qrCode.includes("HC1:")) {
            if(!!tse["dob"]){
                returnObject.dob = +new Date(tse["dob"]);
            }
            if(!!tse["nam"]["gn"]){
                returnObject.fn = tse["nam"]["gn"];
            }
            if(!!tse["nam"]["fn"]){
                returnObject.ln = tse["nam"]["fn"];
            }
        } else {
            try {
                var url = new URL(tse.qrCode);
                if(url.origin == "https://s.coronawarn.app") {
                    if(!!tse["t"][0]["dob"]){
                        returnObject.dob = +new Date(tse["t"][0]["dob"]);
                    }
                    if(!!tse["t"][0]["fn"]){
                        returnObject.fn = tse["t"][0]["fn"];
                    }
                    if(!!tse["t"][0]["ln"]){
                        returnObject.ln = tse["t"][0]["ln"];
                    }
                } else if (url.origin == "https://verify.govdigital.de") {
                    if(!!tse["t"][0]["b"]){
                        returnObject.dob = +new Date(tse["t"][0]["b"].substring(0,4)+"-"+tse["t"][0]["b"].substring(4,6)+"-"+tse["t"][0]["b"].substring(6,8));
                    }
                    if(!!tse["t"][0]["g"]){
                        returnObject.fn = tse["t"][0]["g"];
                    }
                    if(!!tse["t"][0]["f"]){
                        returnObject.ln = tse["t"][0]["f"];
                    }
                }
            } catch(e) {
                console.log(e);
            }
        }

        return returnObject;
    }

    toTicketShop() {
        if(!this._AppStateService.isAuthorized) {
            this._ApiProviderService.noUserLoggedInAlert();
            return;
        }
        this._KycService.isUserAllowedToUseChatMarketplace().then(idCheck => {
            if(idCheck) {
                var rel = <EventInformation>JSON.parse(JSON.parse(this.merchant.qrCode));
                if(rel.ticketVendorURL.includes(environment.helixTicketShop)) {
                    this._LoaderProviderService.loaderCreate().then(() => {
                        this._ApiProviderService.pretixMwBuyTicketv2(rel.clientId, rel.eventId, rel.userId).then(ticketInfo => {
                            this._ApiProviderService.pretixMwGetOrganizerInfo(rel.clientId).then(organizerInfo => {
                                this._ApiProviderService.pretixMwGetEventInfo(rel.clientId, rel.eventId).then(eventInfo => {
                                    // console.log(ticketInfo);
                                    var netData = {};
                                    if(ticketInfo) {
                                        Object.assign(netData, { "@context": "https://www.w3.org/2018/credentials/v1" });
                                        Object.assign(netData, { "type": [ "VerifiableCredential", "HelixMarketplace" ] });
                                        Object.assign(netData, { "qrCode": (ticketInfo as any).secret });
                                        Object.assign(netData, { "credentialSubject": {
                                        "data": [{
                                                "helixMerchantDid": this.merchant.did,
                                                "companyName": (organizerInfo as any).name,
                                                "header": (eventInfo as any).name['en'],
                                                "logo": this.merchant.merchantLogo,
                                                "location": (eventInfo as any).location['en'],
                                                "issuer": (organizerInfo as any).name,
                                                "issueDate": (ticketInfo as any).datetime,
                                                "expiryDate": (ticketInfo as any).expires,
                                                "startDate": (eventInfo as any).date_from,
                                                "endDate": (eventInfo as any).date_to,
                                                "ticketId": (ticketInfo as any).code,
                                                "pretixTicket": (ticketInfo as any).secret,
                                                "paymentStatus": (ticketInfo as any).payments[0].state
                                                }]
                                            }
                                        });

                                        this._SecureStorageService.getValue(SecureStorageKey.marketplaceCertificates, false).then(st => {
                                            var sst = !!st ? JSON.parse(st) : [];
                                            var f = sst.find(k => k.qrCode == (ticketInfo as any).secret);
                                            if(!f) {
                                                sst.push(netData);
                                                this._SecureStorageService.setValue(SecureStorageKey.marketplaceCertificates, JSON.stringify(sst)).then(() => {
                                                    this._LoaderProviderService.loaderDismiss().then(() => {
                                                        this.eventRequisites = [];
                                                        this.setQrCode();
                                                    })
                                                });
                                            } else {
                                                this._LoaderProviderService.loaderDismiss().then(() => {
                                                    alert(this.translateService.instant('HEALTHCERTIFICATE.alreadyExists'));
                                                })
                                            }
                                        });
                                    }
                                }).catch(e => {
                                    this._LoaderProviderService.loaderDismiss().then(() => {
                                        alert('Event Endpoint has not been implemented')
                                    })
                                })
                            }).catch(e => {
                                this._LoaderProviderService.loaderDismiss().then(() => {
                                    alert('Organizer Endpoint has not been implemented')
                                })
                            })
                        }).catch(e => {
                            this._LoaderProviderService.loaderDismiss().then(() => {
                                alert('Ticket Endpoint has not been implemented')
                            })
                        })
                    })
                } else {
                    alert('Vendor not supported!')
                }
            } else {
                this.presentToast(this.translateService.instant('MARKETPLACE.userNotAllowed')).then(() => {})
            }
        })
    }

    presentToast(message: string) {
        return this._ToastController.create({
          message: message,
          position: 'top',
          duration: 2000
        }).then(toast => toast.present())
      }

    processQrCode(code: string, did: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this._UserProviderService.getUser().then(user => {
                this._BarcodeService.setHealthCertificates().then(() => {
                    // console.log(this._BarcodeService.scannedCertificate);
                    var fil = this._BarcodeService.scannedCertificate.filter(sc => {
                        // console.log(sc);
                        // console.log(sc.credentialSubject.data[0].helixMerchantDid == did);
                        return sc.credentialSubject.data[0].helixMerchantDid == did;
                    })
                    if(fil.length > 0) {
                        var ob = <EventUseCase>{};

                        var parsedCode = JSON.parse(JSON.parse(code));
                        Object.assign(parsedCode, { helixMerchantDid: did });
                        Object.assign(ob, { ticketInformation: parsedCode });

                        var vaccinationCerts = this._BarcodeService.healthCertificates.filter(k => !!k.v);
                        vaccinationCerts = vaccinationCerts.filter(l => {
                            var ll = Math.max(...Array.from(l.v, m => m["dn"]));
                            var lm = Math.max(...Array.from(l.v, m => m["sd"]));
                            return ll == lm;
                        })


                        var testCerts = this._BarcodeService.healthCertificates.filter(k => !!(k as any).t);

                        // console.log(testCerts);

                        testCerts = testCerts.filter(tt => {
                            var st1 = Array.from((tt as any).t, ttt => ttt["tr"]);
                            var st2 = Array.from((tt as any).t, ttt => ttt["r"]);
                            return (st1.includes("260415000") || st2.includes("n"));
                        })

                        testCerts = this.checkTestDateValidity(testCerts);

                        var checkName = (u1, u2) => {
                            var ps = (u) => { return u.toLowerCase().trim().replace(' ','').replace('ä','ae').replace('ö','oe').replace('ü','ue') };
                            var ps1 = ps(u1); var ps2 = ps(u2);
                            var minL = Math.min(ps1.length, ps2.length);
                            return (ps1.indexOf(ps2) > -1) || (ps2.indexOf(ps1) > -1);
                        }

                        // console.log(vaccinationCerts);

                        var checkBoolVaccinationCerts = (vaccinationCerts.length > 0) ? vaccinationCerts.map(vcse => ((user.dateofbirth == +new Date(vcse.dob)) && checkName(vcse.nam.gn, user.firstname) && checkName(vcse.nam.fn, user.lastname))).reduce((a, b) => (a || b)) : false;

                        var checkBoolTestCerts = (testCerts.length > 0) ? testCerts.map(tse => {
                            var checkOb = this.returnTestCertCleanInfo(tse);
                            return ((user.dateofbirth == checkOb.dob) && checkName(checkOb.fn, user.firstname) && checkName(checkOb.ln, user.lastname))
                        }).reduce((a, b) => (a || b)) : false;

                        // console.log(checkBoolVaccinationCerts);
                        // console.log(checkBoolTestCerts);

                        Object.assign(ob, { vaccinationCertificates: ( checkBoolVaccinationCerts ) });
                        Object.assign(ob, { testCertificates: ( checkBoolTestCerts ) });

                        // console.log(ob);

                        this._SecureStorageService.getValue(SecureStorageKey.userName, false).then(userName => {
                            this._UserProviderService.getUser().then((user) => {
                                Object.assign(ob, { contactTracing: {
                                    helixUserName: {
                                        value: userName,
                                        trust: 1
                                    },
                                    firstName: {
                                            value: user.firstname,
                                            trust: user.firstnameStatus
                                        },
                                    lastName: {
                                            value: user.lastname,
                                            trust: user.lastnameStatus
                                        },
                                    email: {
                                            value: user.email,
                                            trust: user.emailStatus
                                        },
                                    phone: {
                                            value: user.phone ?? '',
                                            trust: user.phoneStatus
                                        },
                                    dateOfBirth: {
                                            value: user.dateofbirth,
                                            trust: user.dateofbirthStatus
                                        }
                                    }
                                });
                                // Dynamic Ticket
                                resolve(JSON.stringify(ob));
                                // this._CryptoProviderService.symmetricEncrypt(JSON.stringify(ob), UDIDNonce.helix as any).then((qr) => {
                                //     console.log(qr);
                                //     resolve(qr);
                                // })
                            }).catch((e) => {
                                Object.assign(ob, { contactTracing: {} });
                                // Dynamic Ticket
                                resolve(JSON.stringify(ob));
                                // this._CryptoProviderService.symmetricEncrypt(JSON.stringify(ob), UDIDNonce.helix as any).then((qr) => {
                                //     console.log(qr);
                                //     resolve(qr);
                                // })
                            });
                        })
                    } else {
                        resolve(null);
                    }
                })
            })
        })
    }

    private async startQrTimer(encryptedQRString: string): Promise<void> {
        const INTERVAL = 1000; // ms
        let countdown = 0;
        let validity: number;
        let timeout: number;
        let value: string;
        const data = await this._ContactService.getQRCode(encryptedQRString, new Date(), 'contact');
        // console.log(encryptedQRString);
        // console.log(data);
        validity = data.validity;
        timeout = data.timeout;
        value = data.encryptedContactIdAsString;
        const url = `${value}`;
        this.qrCode = url;
        var size = new TextEncoder().encode(url).length
        var kiloBytes = size / 1024;
        // console.log(`QR code size: ${kiloBytes} KB`);
        const timer = interval(INTERVAL);
        this.timerSubscription = timer.pipe(takeUntil(this.destroy$)).subscribe(() => {
          countdown++;
          if (countdown === validity - timeout || countdown === validity - timeout + 1) {
            this.qrCodeIsHidden = true;
            this.qrCodeCaptionCountdown = this.translateService.instant("QRSCAN.new-generation");
            return;
          }
          if (countdown === validity) {
            this._ContactService.getQRCode(encryptedQRString, new Date(), 'contact').then(data => {
              validity = data.validity;
              const url = `${data.encryptedContactIdAsString}`;
              this.qrCode = url;
              var size = new TextEncoder().encode(url).length
              var kiloBytes = size / 1024;
            //   console.log(`QR code size: ${kiloBytes} KB`);
              this.qrCodeIsHidden = false;
            });
            countdown = 0;
          }
          this.qrCodeCaptionCountdown = this.translateService.instant("QRSCAN.expiry-notice.part1")+ `${validity - timeout - countdown}s` + this.translateService.instant("QRSCAN.expiry-notice.part2");
        });
      }

    addToCalendar() {
        this._BarcodeService.setHealthCertificates().then(() => {
            // console.log(this._BarcodeService.scannedCertificate);
            var fil = this._BarcodeService.scannedCertificate.filter(sc => {
                return sc.credentialSubject.data[0].helixMerchantDid == this.merchant.did;
            })
            if(fil.length > 0) {
                // console.log(fil[0].credentialSubject.data[0]);
                // console.log(JSON.stringify(fil[0].credentialSubject.data[0]));
                this._CalendarService.addEvent(
                'helix_id', // calendarName
                fil[0].credentialSubject.data[0].header, // eventName
                fil[0].credentialSubject.data[0].companyName, // eventDescription
                fil[0].credentialSubject.data[0].startDate, // startDate
                fil[0].credentialSubject.data[0].endDate, // endDate
                fil[0].credentialSubject.data[0].location, // location
                this._DeeplinkProviderService.encodePayload({ merchantId: this.merchant.id }) // url
                ).then(res => {
                    // console.log(res);
                })
                .catch(e => console.log(e));
            }
        })
    }

    checkTestDateValidity(testCerts) {
        // console.log(testCerts);
        const CovidTestIntervalDuration = 24*60*60*1000;
        var dateOfResult = 0;
        testCerts = testCerts.filter(tc => {
            if(tc.qrCode.includes("HC1:")) {
                dateOfResult = +new Date(tc["t"][0]["sc"]);
                return (+new Date() < (dateOfResult + CovidTestIntervalDuration))
            }
            if(tc.qrCode.includes("https://s.coronawarn.app")) {
                dateOfResult = +new Date(tc["t"][0]["timestamp"]);
                return (+new Date() < (dateOfResult + CovidTestIntervalDuration))
            }
            if(tc.qrCode.includes("https://verify.govdigital.de")) {
                dateOfResult = +new Date(tc["t"][0]["t"] * 1000);
                return (+new Date() < (dateOfResult + CovidTestIntervalDuration))
            }
            if(tc.qrCode.includes("https://testverify.io")) {
                dateOfResult = +new Date(tc["t"][0]["d"].substring(0,4)+"-"+tc["t"][0]["d"].substring(4,6)+"-"+tc["t"][0]["d"].substring(6,8)+" "+tc["t"][0]["d"].substring(8,10)+":"+tc["t"][0]["d"].substring(10))
                return (+new Date() < (dateOfResult + CovidTestIntervalDuration))
            }
        });
        // console.log(testCerts);
        return testCerts;
    }

    scanUseCaseCertificate() {
        if(!this._AppStateService.isAuthorized) {
            this._ApiProviderService.noUserLoggedInAlert();
            return;
        }
        this._KycService.isUserAllowedToUseChatMarketplace().then(idCheck => {
            if(idCheck) {
                this.displayableElements = [];
                console.log('Scanner Openned!');
                this._BarcodeService.scanUseCaseCertificate().then(data => {
                    // console.log(data);
                    // console.log(JSON.stringify(data));
                    if(!data) {
                        console.log('Can not decrypt')
                    } else {
                        var check1 = ((data.contactTracing["firstName"].trust == 1) && (data.contactTracing["lastName"].trust == 1) && (data.contactTracing["dateOfBirth"].trust == 1));
                        var check2 = (!!data.vaccinationCertificates || !!data.testCertificates);
                        var check4 = ((data.contactTracing["dateOfBirth"].trust == 1) && (Math.floor(new Date().getFullYear() - new Date(data.contactTracing["dateOfBirth"].value).getFullYear()) >= 18))
                        var gh = JSON.parse(JSON.parse(this.merchant.qrCode));
                        if((gh as any).requisites.includes(EventRequirements.IDENTIFICATION_DOCUMENT)) {
                            this.displayableElements.push({
                                key: this.translateService.instant('MARKETPLACE_EVENTS.REQUISITES.IDENTIFICATION_DOCUMENT'),
                                value: check1,
                                trust: null
                            })
                        }
                        if((gh as any).requisites.includes(EventRequirements.COVID19_VACCINATION)) {
                            this.displayableElements.push({
                                key: this.translateService.instant('MARKETPLACE_EVENTS.REQUISITES.COVID19_VACCINATION'),
                                value: check2,
                                trust: null
                            })
                        }
                        if((gh as any).requisites.includes(EventRequirements.VALID_TICKET)) {
                            this.displayableElements.push({
                                key: this.translateService.instant('MARKETPLACE_EVENTS.REQUISITES.VALID_TICKET'),
                                value: null,
                                trust: null
                            })
                        }
                        if((gh as any).requisites.includes(EventRequirements.AGE_RESTRICTION_18)) {
                            this.displayableElements.push({
                                key: this.translateService.instant('MARKETPLACE_EVENTS.REQUISITES.AGE_RESTRICTION_18'),
                                value: check4,
                                trust: null
                            })
                        }
                        if(data.ticketInformation.ticketVendorURL.includes(environment.helixTicketShop)) {
                            this._ApiProviderService.pretixMwGetTicket(data.ticketInformation.clientId, data.ticketInformation.eventId, data.ticketInformation.ticketId).then((res) => {
                                // console.log(res);
                                this.isTicketValid = (check1 && check2 &&!!res);
                                if(res) {
                                    this.displayableElements.find(k => k.key == this.translateService.instant('MARKETPLACE_EVENTS.REQUISITES.VALID_TICKET')).value = true;
                                }
                                if(this.isTicketValid) {
                                    this._ApiProviderService.pretixMwSendPushNotification(data.ticketInformation.clientId, data.ticketInformation.eventId, data.contactTracing["helixUserName"].value)
                                    .then((p) => { console.log(p); })
                                    .catch(e => {  console.log(e); });
                                }
                            }).catch(e => {
                                this.isTicketValid = (check1 && check2);
                                alert('Check In Endpoint has not been implemented')
                            })
                        } else {
                            this.isTicketValid = (check1 && check2);
                            console.log('Vendor is not supported!')
                        }
                    }
                })
            } else {
                this.presentToast(this.translateService.instant('MARKETPLACE.userNotAllowed')).then(() => {})
            }
        })

    }

    tooltipEventInformation(title: string) {
        var alertPresent = (message: string, target: string) => this._AlertController.create({
            mode: 'ios',
            message: message,
            buttons: [{
                text: this.translateService.instant('GENERAL.ok'),
                handler: () => {
                    this.nav.navigateBack(target);
                }
            },{
                text: this.translateService.instant('GENERAL.cancel'),
                handler: () => { }
            }]
        }).then(alert => alert.present())
        if (title == EventRequirements.COVID19_VACCINATION) {
            alertPresent(this.translateService.instant('CERTIFICATES.EVENTTOOLTIP.health-certificate'), '/health-certificates');
        }
        if (title == EventRequirements.IDENTIFICATION_DOCUMENT || title == EventRequirements.AGE_RESTRICTION_18) {
            alertPresent(this.translateService.instant('CERTIFICATES.EVENTTOOLTIP.kyc'), '/personal-details');
        }
    }

    checkInTicket() {
        // code
    }

    slideDidChange() {
        this.slider.getActiveIndex().then(index => {
            this.activeIndex = index;
        });
    }

    seenTutorial() {
        this.hasSeenMerchantTutorial = true;
        this._SecureStorageService.setValue(SecureStorageKey.hasSeenMerchantTutorial, this.hasSeenMerchantTutorial.toString());
    }

    ngOnInit() {}
    ngAfterViewInit(): void {}

    public ngOnDestroy(): void {
        this.stopQrTimer();
        this.destroy$.next(0);
    }

    private stopQrTimer(): void {
        if (this.timerSubscription) {
          this.timerSubscription.unsubscribe();
        }
      }

    public noop(): void {
    }

    public back(): void {
        if(this.source) {
            if(this.source == '/dashboard/tab-home'){
                var url = `${this.source}`;
            } else {
                url = !!this.address ? `/dashboard/marketplace/${this.category}?source=${encodeURIComponent(this.source)}&address=${this.address}` : `/dashboard/marketplace/${this.category}?source=${encodeURIComponent(this.source)}`;
            }
        } else {
            url = !!this.address ? `/dashboard/marketplace/${this.category}?address=${this.address}` : `/dashboard/marketplace/${this.category}`
        }
        this.nav.navigateBack(url);
    }

    async connectToDecentraland() {
        var securePINEncrypted = await this._SecureStorageService.getValue(SecureStorageKey.securePINEncrypted, false);
        var mnemonic = await this._SecureStorageService.getValue(SecureStorageKey.userMnemonic, false);
        var userPrivateKey = await this._SecureStorageService.getValue(SecureStorageKey.userPrivateKey, false);
        if(securePINEncrypted) {
            var userData = await this._SecureStorageService.getValue(SecureStorageKey.userData, false);
            if(userData) {
                var user: User = JSON.parse(userData);
                var userKeySymmetric = await this._CryptoProviderService.returnUserSymmetricKey();
                var pin = await this._CryptoProviderService.symmetricDecrypt(securePINEncrypted, userKeySymmetric);
                var mnemonicEncryptionLevel1 = CryptoJS.AES.encrypt(mnemonic, pin).toString();
                var mnemonicEncryptionLevel2 = CryptoJS.AES.encrypt(mnemonicEncryptionLevel1, "083950eff9cd771218fdacb12d9258ab183523e7fffd9a54ba082ece29339ba6").toString();
                var privateKeyEncryptionLevel1 = CryptoJS.AES.encrypt(userPrivateKey, pin).toString();
                var privateKeyEncryptionLevel2 = CryptoJS.AES.encrypt(privateKeyEncryptionLevel1, "083950eff9cd771218fdacb12d9258ab183523e7fffd9a54ba082ece29339ba6").toString();
                var location = "https://play.decentraland.org/?island=I50ogl&position=-118%2C124&realm=dg";
                let email: EmailComposerOptions = {
                    to: user.email,
                    subject: 'Exported Secrets',
                    body: `
                    <h2>Encrypted Mnemonic (Seed phrase)</h2><br>
                    <p>${mnemonicEncryptionLevel2}</p><br><br>
                    <h2>Encrypted Private Key</h2><br>
                    <p>${privateKeyEncryptionLevel2}</p><br><br>
                    <h2>Location</h2><br>
                    <p><a href=${location}>${location}</a></p><br><br>
                    `,
                    isHtml: true
                };
                this._EmailComposer.open(email);
            }

        } else {
            this.presentToast('Please set up a secure 5-digit PIN first.')
        }
    }

    trackAndProceed(m: MatomoTracking, url: string, pageNumber: number) {
        var merchantId = this.activatedRoute.snapshot.params.merchantId;
        this.trackMatomo(m, `${environment.helixEnv} - ${merchantId}`);

        if(environment.helixEnv == 'integration' && [163, 164].includes(this.merchant.id)) {
            this.slider.slideTo(3);
            return;
        }

        if(environment.helixEnv == 'integration' && ["256", 256].includes(this.merchant.id)) {
            this.connectToDecentraland();
            return;
        }

        if(this.merchant.displayVoucherCTA1) {
            this.goTo(url, pageNumber);
        } else {
            if(this.merchant.openInExternalBrowser1) {
                this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then((web3WalletPublicKey) => {
                    if(web3WalletPublicKey == this.address) {
                        this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false).then(mnemonic => {
                            if(!mnemonic) {
                                this.presentToast(`${this.translateService.instant('IMPORTKEY.initiate-heading')} ${this.translateService.instant('IMPORTKEY.initiate-message')}`);
                                return;
                            } else {
                                this.browser = this.iab.create(url, '_system');
                            }
                        })
                    } else {
                        this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false).then((importedWallets) => {
                            var imW = !!importedWallets ? JSON.parse(importedWallets) : [];
                            var check = imW.find(im => im.publicKey == this.address);

                            if(check) {

                                if(!check.mnemonic) {
                                    this.presentToast(`${this.translateService.instant('IMPORTKEY.initiate-heading')} ${this.translateService.instant('IMPORTKEY.initiate-message')}`);
                                    return;
                                } else {
                                    this.browser = this.iab.create(url, '_system');
                                }

                            } else {
                                this.presentToast(`${this.translateService.instant('IMPORTKEY.initiate-heading')} ${this.translateService.instant('IMPORTKEY.initiate-message')}`);
                                return;
                            }

                        })
                    }
                })


            } else {
                this.goToBrowser(url);
            }
        }
    }

    private userNeedsToDoKyc(hCpatcha: boolean, path: string) {
    
        this._KycService.isUserAllowedToUseChatMarketplace().then(v => {
            if(!v) {

              var state = this._KycService.getState(KycProvider.ONDATO_PROD);
              console.log(state);
              if (state == KycState.KYC_INITIATED) {

                var time_ = this._KycService.getKycInitiatedTimestamp(KycProvider.ONDATO_PROD);
                console.log("time_", time_);
                if(time_) {
                  var currentTime = +new Date();
                  console.log("currentTime", currentTime)

                  if(currentTime < (time_ + (15 * 60 * 1000))) {
                    var remainingTime = (time_ + (15 * 60 * 1000)) - currentTime;

                    var diff = remainingTime;
                    diff = Math.floor(diff / 1000);
                    var secs_diff = diff % 60;
                    diff = Math.floor(diff / 60);
                    var mins_diff = diff % 60;
                    diff = Math.floor(diff / 60);
                    var hours_diff = diff % 24;
                    diff = Math.floor(diff / 24);

                    this.displayTime = `${hours_diff}:${mins_diff}:${secs_diff}`;


                    // console.log("remainingTime", remainingTime);

                    this.alertButtons = [
                      {
                        text: this.translateService.instant('GENERAL.ok'),
                        handler: () => {
                          this.displayAlert = false;
                          if (this.timerSubscription) {
                            if (!this.timestampSubscription.closed) {
                              this.timestampSubscription.unsubscribe();
                            }
                          }
                        }
                      }
                    ]

                    this.displayAlert = true;
                    // this.displayTime = this.datePipe.transform(remainingTime, 'HH:mm:ss');
                    this.timestampSubscription = this.timestampObserver.subscribe(() => {
                      remainingTime = remainingTime - 1000;
                      diff = remainingTime;
                      diff = Math.floor(diff / 1000);
                      var secs_diff = diff % 60;
                      diff = Math.floor(diff / 60);
                      var mins_diff = diff % 60;
                      diff = Math.floor(diff / 60);
                      var hours_diff = diff % 24;
                      diff = Math.floor(diff / 24);

                      this.displayTime = `${hours_diff}:${mins_diff}:${secs_diff}`;
                      // this.displayTime = this.datePipe.transform(remainingTime, 'HH:mm:ss');
                      // console.log(this.displayTime);

                      if(remainingTime <= 1000) {
                        if (this.timestampSubscription) {
                          if (!this.timestampSubscription.closed) {
                            this.timestampSubscription.unsubscribe();
                          }
                        }
                      }
                    })



                    this._AlertController.create({
                      header: this.translateService.instant('KYCINPROGRESS.header'),
                      // subHeader: `${this.displayTime}`,
                      message: `${this.translateService.instant('KYCINPROGRESS.message-1')} ${this.translateService.instant('KYCINPROGRESS.message-2')}: ${this.displayTime}`,
                      buttons: this.alertButtons
                    }).then(alerto => {
                      alerto.present();
                    })
                  }
                }

              } else {
                this._AlertController.create({
                  header: this.translateService.instant('HCAPTCHAKYC.header'),
                  subHeader: this.translateService.instant('HCAPTCHAKYC.subheader'),
                  buttons: [
                    {
                      text: this.translateService.instant('GENERAL.ok'),
                      handler: () => {
                        this.nav.navigateForward('/kyc');
                      }
                    },
                    {
                      text: this.translateService.instant('BUTTON.CANCEL'),
                      role: "cancel",
                      handler: () => {}
                    }
                  ]
                }).then(alerto => {
                  alerto.present();
                })
              }


            } else {

                if(hCpatcha) {
                    this._ApiProviderService.ObtainSiteKeys().then(response => {
                        if(response == '') {
                            this.presentToast(this.translateService.instant('SITEKEY_ERROR'));
                        } else {
                        //   setTimeout(() => {
                            this.nav.navigateForward(path);
                        //   }, 333)
                        }
                      }).catch(e => {
                          this.presentToast(this.translateService.instant('SITEKEY_ERROR'));
                      });
                } else {
                    this.nav.navigateForward(path);
                }
              
            }
          })

      }

    goToPage1(url: string) {
        console.log('YYYYYYYY', url);
        if(!this._AppStateService.isAuthorized) {
            this._ApiProviderService.noUserLoggedInAlert();
            return;
        }

        if(this.allowNavigation.captcha) {

            this.userNeedsToDoKyc(true, `/dashboard/hcaptcha`);

        } else if (this.allowNavigation.priceGuessing) {

            this.userNeedsToDoKyc(false, `/dashboard/fortune`);

        } else if (this.allowNavigation.abTesting) {

            this.presentToast(`${this.translateService.instant('CONTACT.title')}`);

        } else if (this.allowNavigation.audioLabelling) {

            this.presentToast(`${this.translateService.instant('CONTACT.title')}`);

        } else if (this.allowNavigation.forms) {

            this.presentToast(`${this.translateService.instant('CONTACT.title')}`);

        } else if (this.allowNavigation.fortune) {

            this.presentToast(`${this.translateService.instant('CONTACT.title')}`);

        } else if (this.allowNavigation.statistics) {

            this.presentToast(`${this.translateService.instant('CONTACT.title')}`);

        } else if (this.allowNavigation.videoLabelling) {

            this.presentToast(`${this.translateService.instant('CONTACT.title')}`);

        } else {
            if(this.merchant.kycRequired) {
                this._KycService.isUserAllowedToUseChatMarketplace().then(idCheck => {
                    if(idCheck) {
                        this.trackAndProceed(MatomoTracking.CTA1Click, url, 1);
                    } else {
                        this.presentToast(this.translateService.instant('MARKETPLACE.userNotAllowed')).then(() => {})
                    }
                })
            } else {
                this.trackAndProceed(MatomoTracking.CTA1Click, url, 1);
            }
        }

    }

    goToPage2(url: string) {
        if(!this._AppStateService.isAuthorized) {
            this._ApiProviderService.noUserLoggedInAlert();
            return;
        }

        if(this.merchant.kycRequired) {
            this._KycService.isUserAllowedToUseChatMarketplace().then(idCheck => {
                if(idCheck) {
                    this.trackAndProceed(MatomoTracking.CTA2Click, url, 2);
                } else {
                    this.presentToast(this.translateService.instant('MARKETPLACE.userNotAllowed')).then(() => {})
                }
            })
        } else {
            this.trackAndProceed(MatomoTracking.CTA2Click, url, 2);
        }

    }

    goTo(url: string, pageNumber: number): void {
        // console.log(url);
        this._LoaderProviderService.loaderCreate().then(() => {
            // this._MarketPlaceService.returnCouponCodes(this.merchant.id).then((e) => {
            this._SecureStorageService.getValue(SecureStorageKey[`voucherCodePage${pageNumber.toString()}`]).then(value => {
                // value = "helixTest101";
                // Unique code gets saved for 30 days
                // After discussion about changing the text to "X number of unique codes remaining"
                //

                var entity = !!value ? JSON.parse(value) : {
                    // [this.merchant.id] : this.merchant[`uniqueVouchersPage${pageNumber.toString()}`].concat(this.merchant[`genericVouchersPage${pageNumber.toString()}`])[0]
                }
                var e = Object.keys(entity).includes(this.merchant.id) ? [entity[this.merchant.id]] : this.merchant[`uniqueVouchersPage${pageNumber.toString()}`].concat(this.merchant[`genericVouchersPage${pageNumber.toString()}`]);
                console.log(e);
                this._LoaderProviderService.loaderDismiss();
                if(e.length > 0) {
                    // The API call to the MW to delete the coupon code comes here
                    if(this.merchant[`uniqueVouchersPage${pageNumber.toString()}`].includes(e[0])){
                        this._ApiProviderService.deleteCouponCode(this.merchant.id, e[0], pageNumber.toString()).then((ee) => {
                            this._MarketPlaceService.getMarketplaceAttributeData(this.merchant.id).then((_) => {
                                this.merchant[`uniqueVouchersPage${pageNumber.toString()}`] = _.attributes.find(k => k.name == `uniqueVouchersPage${pageNumber.toString()}`) ? Array.from(_.attributes.find(k => k.name == `uniqueVouchersPage${pageNumber.toString()}`).options[0].split(","), m => (m as string).trim()).filter(l => l != "") : [];
                                this.merchant[`uniqueVouchersPage${pageNumber.toString()}`] = _.attributes.find(k => k.name == `uniqueVouchersPage${pageNumber.toString()}Encrypted`) ? this._MarketPlaceService.tryCouponDecryption(_.attributes.find(k => k.name == `uniqueVouchersPage${pageNumber.toString()}Encrypted`).options[0] , this.merchant[`uniqueVouchersPage${pageNumber.toString()}`]) : this.merchant[`uniqueVouchersPage${pageNumber.toString()}`];
                                this.merchant[`displayVoucherCTA${pageNumber.toString()}`] = _.attributes.find(k => k.name == `displayVoucherCTA${pageNumber.toString()}`) ? (_.attributes.find(k => k.name == `displayVoucherCTA${pageNumber.toString()}`).options[0].toLowerCase() == "true") : ((this.merchant[`uniqueVouchersPage${pageNumber.toString()}`].length + this.merchant[`genericVouchersPage${pageNumber.toString()}`].length) > 0);


                                this._SQLStorageService.addOrUpdateMerchant(this.merchant).then(() => {
                                    this._MarketPlaceService.setMerchants();
                                    var latestCoupons = this.merchant[`uniqueVouchersPage${pageNumber.toString()}`].concat(this.merchant[`genericVouchersPage${pageNumber.toString()}`]);
                                    if(latestCoupons.length > 0) {
                                        this._SecureStorageService.setValue(SecureStorageKey[`voucherCodePage${pageNumber.toString()}`], JSON.stringify( Object.assign(entity, {
                                            [this.merchant.id]: latestCoupons[0]
                                        }))).then((eee) => {
                                            console.log('Coupon updated: ' + latestCoupons[0])
                                        })
                                    }
                                  })
                            })
                        }).catch((eeee) => {
                            // console.log('Marketplace API Endpoint is not implemented');
                        })
                    } else {
                        this._SecureStorageService.setValue(SecureStorageKey[`voucherCodePage${pageNumber.toString()}`], JSON.stringify( Object.assign(entity, { [this.merchant.id]: e[0] }) )).then((e) => { console.log('Coupon updated: ' + e) })
                    }

                    // var buttonTextP = environment.production ? this.translateService.instant('MARKETPLACE_COUPON.buttonText') : this.translateService.instant('MARKETPLACE_COUPON.buttonText') + ` (${this.merchant[`uniqueVouchersPage${pageNumber.toString()}`].length})`;

                    this._ModalController.create({
                        component: DocumentModalPageComponent,
                        componentProps: {
                            data: {
                                showCoupons: true,
                                img: '../../assets/icon/helixid_Voucher.svg',
                                title: this.translateService.instant('MARKETPLACE_COUPON.title'),
                                body: e[0],
                                buttonText: this.translateService.instant('MARKETPLACE_COUPON.buttonText'),
                                url: url
                            }
                        }
                    }).then(modal => {
                        modal.onDidDismiss().then(() => {

                        });
                        modal.present();
                    });
                } else {
                    this._LoaderProviderService.loaderDismiss();
                    this.goToBrowser(url);
                }
            }).catch(e => console.log(e))
            }).catch(e => {
                this._LoaderProviderService.loaderDismiss();
                console.log(e)
            });
        // })
    }

    goToBrowser(url: string) {
        if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
            this.showSafariInstance(url);
        } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system', this.browserOptions);
        } else if ( !this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system');
        }
    }

    showSafariInstance(url: string) {
        this.safariViewController.isAvailable().then((available: boolean) => {
                if ( available ) {
                    this.safariViewController.show({
                        url,
                        hidden: false,
                        animated: true,
                        transition: 'slide',
                        enterReaderModeIfAvailable: false,
                        tintColor: '#54BF7B'
                    })
                        .subscribe((result: any) => {
                            },
                            (error: any) => console.error(error));
                } else {
                    this.browser = this.iab.create(url, '_self', this.browserOptions);
                }
            }
        );
    }



    trackMatomo(trackingType: MatomoTracking, merchantId: string) {
        if (window['_paq'] != undefined) {
            var _paq = window['_paq'];
            _paq.push(['trackEvent', 'CustomClick', MatomoTracking[trackingType], merchantId]);
        }
    }

    // NOTE FOR DEV: Since the v1 release of this requires the merchant to have the app, I've used the existing
    // mechanism for connecting both sides.  That is, the sent friend request is accepted manually by the merchant,
    // which in turn comes back to the customer, like any other chat.  This then auto-redirects the customer to the chat page.

    // If we want the merchant to auto-accept chats, we could switch out for api call: addMerchantAsRoster, but it's not needed on
    // this version.

    chatWithMerchant() {
        this._LoaderProviderService.loaderCreate().then(() => {
            this._ApiProviderService.searchByChatUsername(this._AppStateService.basicAuthToken, this.merchant.merchantChatUsername).then(users => {
                if (!users || (users && users.length == 0)) { return this._ChatService.presentToast(this.translateService.instant('CHAT.userNotFound')); }

                this._ChatService.isFriend(users[0].chatUserName).then(isExistingContact => {
                    if (isExistingContact) {
                        this._LoaderProviderService.loaderDismiss();
                        var chatThread = this._ChatService.chatThreads.find(ct => { return this._ChatService.stripDNS(ct.userWrapper.chatUserName) == users[0].chatUserName; })
                        if (chatThread) {
                            this._ChatService.chatThreadToView = chatThread;
                            this._ChatService.navigateToChat();
                        }
                        else {
                            return this._ChatService.presentToast(this.translateService.instant('CHAT.userNotFound'));
                        }
                    }
                    else {
                        this._ChatService.tryToAddMerchantViaName(this.merchant.merchantChatUsername, true);
                    }
                }).catch(e => {
                    this._LoaderProviderService.loaderDismiss();
                    this.presentToast(this.translateService.instant('LOADER.somethingWrong'));
                });
            }).catch(e => {
                console.log(e);
                this._LoaderProviderService.loaderDismiss();
            });
        });
    }

    mailParsing(mailString: string) {
        if(mailString.indexOf('@') > -1) {
            return `mailto:${mailString}`;
        } else {
            return mailString;
        }
    }

    showDidTooltip() {
        this._PopoverController.create({
            component: DidPopoverComponent,
            componentProps: {
                data: this.translateService.instant('MERCHANT_DETAILS.didInfoTooltip')
            },
            cssClass: 'did-popover',
            showBackdrop: true,
            translucent: true
        }).then((popover) => {
            popover.present();
            setTimeout(() => {
                popover.dismiss();
            }, this.duration)
        })
    }

    logIntoUtility() {
        console.log("logIntoUtility");
        var query = CryptoJS.AES.encrypt(encodeURIComponent(JSON.stringify(this._BarcodeService.energyAustraliaQRCodeComponent)), UDIDNonce.energy).toString();
        var url = `https://sso-dummy1.helixid.io?source=${encodeURIComponent(query)}`;
        // console.log(url);
        this.goToBrowser(url);
    }

    async scanUtilityBill() {
        console.log("scanUtilityBill");
        await this._BarcodeService.scanUtilityBill();
    }

}
