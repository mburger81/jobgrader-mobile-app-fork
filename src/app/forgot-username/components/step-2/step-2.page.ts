import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LockService } from 'src/app/lock-screen/lock.service';

@Component({
  selector: 'app-forgot-username-step-2',
  templateUrl: './step-2.page.html',
  styleUrls: ['./step-2.page.scss']
})
export class ForgotUsernameStepTwoPage implements OnInit {

  success: boolean;
  private ngOnInitExecuted: any;

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private lockService: LockService
  ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async goToLogin() {
    await this.nav.navigateRoot('/login?from=forgot-username');
    this.lockService.addResume();
  }

  private async componentInit() {
    setTimeout(() => this.detectChanges());
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }
}
