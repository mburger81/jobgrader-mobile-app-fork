import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';

@Component({
  selector: 'app-data-sharing-modal',
  templateUrl: './data-sharing-modal.component.html',
  styleUrls: ['./data-sharing-modal.component.scss'],
})
export class DataSharingModalComponent implements OnInit {
  @Input() data: any;

  public keyValue = [];
  private datePipe = new DatePipe('en-US');

  constructor(
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService 
  ) { }

  ngOnInit() {
    // var keys = Object.keys(this.data);
    var keys = ['dataAccessProcessId', 'timestamp', 'username', 'fields', 'checks', 'status'];

    keys.forEach(key => {
      if(!['publicKey'].includes(key)){
        if(key == 'timestamp') {
          this.keyValue.push({
            key: this._Translate.instant(`DATAPROCESS.${key}`),
            value: this.datePipe.transform(this.data[key], 'dd.MM.yyyy HH:mm')
          })
        } else if(key == 'status') {
          this.keyValue.push({
            key: this._Translate.instant(`DATAPROCESS.${key}`),
            value: ((this.data[key] == 0) ? this._Translate.instant(`DATAPROCESS.pending`) : ((this.data[key] == 1) ? this._Translate.instant(`DATAPROCESS.confirmed`) : this._Translate.instant(`DATAPROCESS.rejected`)))
          })
        } else {
          this.keyValue.push({
            key: this._Translate.instant(`DATAPROCESS.${key}`),
            value: this.data[key]
          })
        }
      }
    })
  }

  close() {
    this._ModalController.dismiss();
  }

}
