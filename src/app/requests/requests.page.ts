import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { DataSharingService, StoredRequest } from '../core/providers/data-sharing/data-sharing.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { BarcodeService } from '../core/providers/barcode/barcode.service';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { NetworkService } from '../core/providers/network/network-service';
import { KycService } from '../kyc/services/kyc.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage {

  public profilePictureSrc = '../../assets/job-user.svg';
  public displayVerifiedTick = false;
  public requests: Array<StoredRequest> = [
  // {
  //   "username":"test6",
  //   "dataAccessProcessId":"17580e95-80af-4af4-948a-63c46acb2f9d",
  //   "publicKey":"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhL+9HxmoLmJ/AD5za+iaa7yR8d0cRYVfzuMZp5Sp0u3uvLT7P/Tn1Xyrr2jX9/Lim3ATk3QC/f+gtn3V7HwfcYwSZ2f8tjz+mehrE0/v+crPbrhFEUOVGlurGwesflMOwvv3/8Nmdl7s0PJ1nUCKcmQO+ZsMN6PBXoyUOjbgdtr9+WJs0Hsm18ULekmyqLgSm79Bct7PUKPqHLlXj3Vh0pmpBnKCTU9Q3qEqMm51sInY+APLzlm77TIKOmk6JnAwa8TKdQfk7tMI9nrDUfiH/zweCVV9z7Tn4+Ng6BeEWv4fZgfVfZpgL1sqqldMd51wIRKyR/zeOaw4Di9k26BMkwIDAQAB",
  //   "fields": [ "firstname", "dateofbirth", "lastname" ],
  //   "checks": "[]",
  //   "timestamp": 1651487258361,
  //   "status": 0
  // },
  // {
  //   "username":"test7",
  //   "dataAccessProcessId":"5d0e2b99-658d-4c00-859f-1b6b92c46c32",
  //   "publicKey":"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhL+9HxmoLmJ/AD5za+iaa7yR8d0cRYVfzuMZp5Sp0u3uvLT7P/Tn1Xyrr2jX9/Lim3ATk3QC/f+gtn3V7HwfcYwSZ2f8tjz+mehrE0/v+crPbrhFEUOVGlurGwesflMOwvv3/8Nmdl7s0PJ1nUCKcmQO+ZsMN6PBXoyUOjbgdtr9+WJs0Hsm18ULekmyqLgSm79Bct7PUKPqHLlXj3Vh0pmpBnKCTU9Q3qEqMm51sInY+APLzlm77TIKOmk6JnAwa8TKdQfk7tMI9nrDUfiH/zweCVV9z7Tn4+Ng6BeEWv4fZgfVfZpgL1sqqldMd51wIRKyR/zeOaw4Di9k26BMkwIDAQAB",
  //   "fields": [ "firstname", "dateofbirth", "lastname" ],
  //   "checks": "[]",
  //   "timestamp": 1651487257686,
  //   "status": 1
  // },{
  //   "username":"test8",
  //   "dataAccessProcessId":"46ba54f4-d486-4c3f-b2bc-b609af585eef",
  //   "publicKey":"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhL+9HxmoLmJ/AD5za+iaa7yR8d0cRYVfzuMZp5Sp0u3uvLT7P/Tn1Xyrr2jX9/Lim3ATk3QC/f+gtn3V7HwfcYwSZ2f8tjz+mehrE0/v+crPbrhFEUOVGlurGwesflMOwvv3/8Nmdl7s0PJ1nUCKcmQO+ZsMN6PBXoyUOjbgdtr9+WJs0Hsm18ULekmyqLgSm79Bct7PUKPqHLlXj3Vh0pmpBnKCTU9Q3qEqMm51sInY+APLzlm77TIKOmk6JnAwa8TKdQfk7tMI9nrDUfiH/zweCVV9z7Tn4+Ng6BeEWv4fZgfVfZpgL1sqqldMd51wIRKyR/zeOaw4Di9k26BMkwIDAQAB",
  //   "fields": [ "gender", "passportissuecountry", "passportexpirydate" ],
  //   "checks": "[]",
  //   "timestamp": 1651487259225,
  //   "status": -1
  // }
]

  constructor(
    private nav: NavController,
    private _SecureStorageService: SecureStorageService,
    private _DataSharingService: DataSharingService,
    private _TranslateProviderService: TranslateProviderService,
    private _BarcodeService: BarcodeService,
    private _ImageSelectionService: ImageSelectionService,
    private _KycService: KycService,
    private _NetworkService: NetworkService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
   }

  ionViewWillEnter() {

    if( !this._NetworkService.checkConnection() ){
        this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    const photo = this.userPhotoServiceAkita.getPhoto();
    this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg';


    this._KycService.isUserAllowedToUseChatMarketplace().then(displayVerifiedTick => {
      this.displayVerifiedTick = displayVerifiedTick;
    })

    this.setRequests().then(() => { console.log('Requests have been set') });
  }

  private sortRequests(ar: any) {
    if(ar.length > 1) {
      var tA1 = Array.from(ar, k => (k as any).timestamp);
      tA1.sort().reverse();
      var rr = [];
      for(let ia1 = 0; ia1 < tA1.length; ia1++) {
        var ta11 = tA1[ia1];
        rr.push(ar.find(rrd => rrd.timestamp == ta11));
      }
      return rr;
    } else {
      return ar;
    }
  }

  private async setRequests() {
    var dataSharingRequests = await this._SecureStorageService.getValue(SecureStorageKey.dataSharingRequestsReceived, false);
    this.requests = !!dataSharingRequests ? JSON.parse(dataSharingRequests) : this.requests;
    this.requests = this.sortRequests(this.requests);
    this.requests.forEach(r => {
      r.translatedFields = r.fields.map((g) => { return this._TranslateProviderService.instant(`BASICDATA.${g}`)}).join(", ")
    })
  }

  async show(data: StoredRequest) {
    if(data.status == 0) {
      this.nav.navigateForward(`/data-sharing-signature?data=${encodeURIComponent(JSON.stringify(data))}`)
    } else {
      await this._DataSharingService.signRequestedData(data.dataAccessProcessId, data.username, data.publicKey, data.fields, data.checks, data.status, data.timestamp);
      await this.setRequests();
    }
  }

  goBack() {
    this.nav.navigateBack('/dashboard/personal-details');
  }

  displayPictureOptions() {
    this._ImageSelectionService.showChangePicture().then(photo => {
      this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg'
    });
  }

  scanRequest() {

    this._BarcodeService.scanDataSharingRequest().then(request => {
      var ob = {
        dataAccessProcessId: request.dataAccessProcessId,
        username: request.username,
        publicKey: request.publicKey,
        fields: request.fields,
        checks: request.checks,
        status: request.status
      };
      this.nav.navigateForward(`/data-sharing-signature?data=${encodeURIComponent(JSON.stringify(ob))}`);
    });
  }

}

