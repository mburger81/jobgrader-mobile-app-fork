import { Component, OnInit } from '@angular/core';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { Feedback } from '../core/models/Feedback';
import { UntypedFormGroup, Validators, UntypedFormControl } from '@angular/forms';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { Platform, NavController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { EmailComposer, EmailComposerOptions } from '@awesome-cordova-plugins/email-composer/ngx';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {

  userImage;

  constructor(
    private translateProviderService: TranslateProviderService,
    private apiProviderService: ApiProviderService,
    private loader: LoaderProviderService,
    private platform: Platform,
    private toastController: ToastController,
    private secureStorage: SecureStorageService,
    private nav: NavController,
    private _AppStateService: AppStateService,
    private _EmailComposer: EmailComposer,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
   }
   public keyboardIsOpened = false;
   public keyboardHeight = 0;

   public isAndroid: boolean;

  feedback: Feedback = {};
  feedbackFormGroup = new UntypedFormGroup({
    subject: new UntypedFormControl(''),
    message: new UntypedFormControl('', [ Validators.pattern('^[A-Za-z0-9!?@#$%^&*)(+=._-]+$') ]),
  }, {
    validators: this.confirmingForm
  });

  feedbacktypes = ['HIP_FDB_CMPL', 'HIP_FDB_BG', 'HIP_FDB_FTR', 'HIP_FDB_MPRMNT', 'HIP_FDB_SGGSTN', 'HIP_FDB_PRDCT'];

  async ngOnInit() {
    if (!this.platform.is('android')) {
      this.isAndroid = false;

      window.addEventListener('keyboardWillShow', (event: any) => {
        this.keyboardIsOpened = true;
        this.keyboardHeight = event.keyboardHeight;
      });

      window.addEventListener('keyboardWillHide', () => {
        this.keyboardIsOpened = false;
        this.keyboardHeight = 0;
      });
    } else {
      this.isAndroid = true;
      this.keyboardHeight = 0;

      window.addEventListener('keyboardWillShow', (event: any) => {
        this.keyboardIsOpened = true;
      });

      window.addEventListener('keyboardWillHide', () => {
        this.keyboardIsOpened = false;
      });
    }

    this.userImage = this.userPhotoServiceAkita.getPhoto();
  }

  private confirmingForm() {
    return (feedbackFormGroup: UntypedFormGroup) => {

    };
  }

  goBackToSettings() {
    this.nav.navigateBack('/dashboard/tab-settings');
    // this.nav.back();
  }

  async presentToast(message: string) {
    // const translations = {
    //   en: 'All fields should be checked',
    //   de: 'All fields should be checked',
    //   hi: 'All fields should be checked'
    // };

    const toast = await this.toastController.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  async sendFeedback() {
    await this.loader.loaderCreate();
    // console.log('UntypedFormControl: ' + JSON.stringify(this.feedbackUntypedFormGroup));
    if((!this.feedbackFormGroup.controls.subject.value || this.feedbackFormGroup.controls.subject.value == '') &&
    (!this.feedbackFormGroup.controls.message.value || (this.feedbackFormGroup.controls.message.value).trim() == '')){
      await this.loader.loaderDismiss();
      return await this.presentToast(this.translateProviderService.instant('FEEDBACK.error.both'));
    }
    if(!this.feedbackFormGroup.controls.subject.value || this.feedbackFormGroup.controls.subject.value == ''){
      await this.loader.loaderDismiss();
      return await this.presentToast(this.translateProviderService.instant('FEEDBACK.error.subject'));
    }
    if(!this.feedbackFormGroup.controls.message.value || (this.feedbackFormGroup.controls.message.value).trim() == ''){
      await this.loader.loaderDismiss();
      return await this.presentToast(this.translateProviderService.instant('FEEDBACK.error.message'));
    }

    this.feedback = {
      subject: this.feedbackFormGroup.controls.subject.value,
      message: (this.feedbackFormGroup.controls.message.value).trim()
    };

    if(this._AppStateService.isAuthorized) {
      console.log('Model: ' + JSON.stringify(this.feedback));
      await this.apiProviderService.sendFeedback(this.feedback);
      await this.loader.loaderDismiss();
      this.nav.navigateForward('/feedback-thanks');
    } else {
      await this.loader.loaderDismiss();
      let email: EmailComposerOptions = {
        to: 'feedback@blockchain-helix.com',
        subject: this.feedback.subject,
        body: this.feedback.message,
        isHtml: true
      };
      this._EmailComposer.open(email);
      // this._EmailComposer.getClients().then(apps => {
      //   console.log(apps);
      //   for(var app in apps) {
      //     console.log(app);
      //     this._EmailComposer.hasClient(app).then((isAvailable) => {
      //       console.log(isAvailable);
      //       if(isAvailable) {
      //         this._EmailComposer.open(email);
      //       }
      //     })
      //   }
      // })
    }
  }
}
