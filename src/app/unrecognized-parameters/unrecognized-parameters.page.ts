import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-unrecognized-parameters',
  templateUrl: './unrecognized-parameters.page.html',
  styleUrls: ['./unrecognized-parameters.page.scss'],
})
export class UnrecognizedParametersPage {

  constructor(
    private nav: NavController) {
  }

  goToPageHome() {
    this.nav.navigateRoot('/dashboard/tab-home');
  }

}
