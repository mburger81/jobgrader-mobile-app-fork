import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { AppStateService } from '../providers/app-state/app-state.service';
import { cloneDeep } from 'lodash';
import { environment } from '../../../environments/environment';

/**
 * HTTP interceptor that currently
 * only shows error toasts in case
 * an http request failed
 */
@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(private appState: AppStateService) {}

    /**
     * Request and response interceptor, to do
     * add some loading queue here
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const {headers}: {headers: HttpHeaders} =  request;
        if (headers.has('Authorization') || request.url.startsWith(environment.fortune.url)) {
            return next.handle(request);
        } else {
            let newHeaders: HttpHeaders = new HttpHeaders();
            const basicAuthToken: string = this.appState.basicAuthToken;
            if (basicAuthToken && !this.whiteList.filter(w => request.url.indexOf(w) !== -1).length) {
                newHeaders = newHeaders.append(
                    'Authorization', `Basic ${basicAuthToken}`
                );
            }
            request = request.clone({headers: newHeaders, withCredentials: true});
            return next.handle(request);
        }
    }

    private get whiteList(): string[] {
        return [
            'checkusername'
        ];
    }

}
