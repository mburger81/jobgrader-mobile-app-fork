import { Injectable } from '@angular/core';
import { Network } from '@awesome-cordova-plugins/network/ngx';
import { ToastController, Platform } from '@ionic/angular';
import { TranslateProviderService } from '../translate/translate-provider.service';

declare let window: any;

@Injectable()
export class NetworkService {

  private loadingOverlay: any;

  constructor(
      private _Platform: Platform,
      private _Translate: TranslateProviderService,
      private _ToastController: ToastController,
      private _Network: Network) {
    }

    isUndefined(type) {
        return typeof type === "undefined";
    }

    isDesktop() {
        return this.isUndefined(window.cordova);
    }

    checkConnection() {
        if (this.isDesktop()) { return true; }

        var networkType = (this._Network.type) ? this._Network.type : null;
        if (networkType == null || networkType == 'none') {
            // this._ToastController.create({
            //     message: this._Translate.instant('NETWORK.checkConnection'),
            //     duration: 3000,
            //     position: 'top',
            // }).then(toast => {
            //     toast.present();
            // });

            return false;
        }
        return true;
    }

    addOfflineFooter(tagName: string) {
        var footer = document.getElementsByTagName(tagName);
        for (var i = 0; i < footer.length; i++ ) {
            var base = footer[i];
            var className = `offline-${tagName}-${i}`;
            var check = document.getElementsByClassName(className);
            if(!check || check.length == 0) {
                var bar = document.createElement('DIV');
                bar.className = className;
                bar.style.background = 'white';
                bar.style.textAlign = 'center';
                bar.style.height = '30px';
                bar.style.width = '100%';
                bar.style.color = 'red';
                bar.innerText = `Offline`;
                bar.style.marginTop = '-8px';
                bar.style.fontSize = '15px';
                bar.animate([ { transform: 'translateY(30px)' }, { transform: 'translateY(0px)' } ],{ duration: 500, iterations: 1 });
                bar.onclick = () => this.removeOfflineFooter(tagName);
                base.append(bar);
            }
        }
    }

    removeOfflineFooter(tagName: string) {
        var footer = document.getElementsByTagName(tagName);
        for (var i = 0; i < footer.length; i++) {
            var className = `offline-${tagName}-${i}`;
            var bar = document.getElementsByClassName(className);
            if(bar && bar.length > 0) {
                for(var j = 0; j < bar.length; j++) {
                    var b = bar[j];
                    b.animate([ { transform: 'translateY(0px)' }, { transform: 'translateY(30px)' } ],{ duration: 500, iterations: 1 });
                    setTimeout(() => { b.remove(); },500);
                }
            }
        }
        // if(footer.length == 0) {
        //     [0,1,2,3].forEach(l => {
        //         var f = document.getElementsByClassName(`offline-ion-footer-${l}`);
        //         for (var h=0; h<f.length; h++) {
        //             f[h].animate([ { transform: 'translateY(0px)' }, { transform: 'translateY(20px)' } ],{ duration: 500, iterations: 1 });
        //             setTimeout(() => { f[h].remove(); },500);
        //         }
        //     })
        // }
    }
}