import { Injectable } from '@angular/core';
import * as vade from './pkg/vade_evan';
// import * as vade from '@evan.network/api-blockchain-core/libs/vade/pkg-browser/vade_evan';

@Injectable({
  providedIn: 'root'
})
export class VadeService {

  constructor() { }

  public init() {

    console.log(vade);
    // vade.did_resolve("did:evan:testcore:0x46d7684e9fA2A2f96552427A51fF78642a09DA07",{}).then(v => {
    //   console.log(v);
    // }).catch(e => {
    //   console.log(e);
    // });

    // vade.did_resolve("did:ethr:mainnet:0x3b0BC51Ab9De1e5B7B6E34E5b960285805C41736",{}).then(v => {
    //   console.log(v);
    // }).catch(e => {
    //   console.log(e);
    // });

    // vade.did_resolve("did:evan:0xF4a6eA9978A30a0c66c286FD9e361981dA53277c",{}).then(v => {
    //   console.log(v);
    // }).catch(e => {
    //   console.log(e);
    // });

    // vade.
    // Check for DID methods and if they are supported by the vade library

    vade.did_resolve("did:dock:5DvefSmDBQonMjv6DxK2mVk2AjzFbVDCmRerMtfM9BwxMmFY",{}).then(v => {
      console.log("did:dock:5DvefSmDBQonMjv6DxK2mVk2AjzFbVDCmRerMtfM9BwxMmFY");
      console.log(v);
    }).catch(e => {
      console.log(e);
    })

    vade.did_resolve("did:evan:zkp:0x03d57c17c1202a0c859bc45afb0b102bcfe73ba51be137095fd3d70c91b68e03",{}).then(v => {
      console.log("did:evan:zkp:0x03d57c17c1202a0c859bc45afb0b102bcfe73ba51be137095fd3d70c91b68e03");
      console.log(v);
    }).catch(e => {
      console.log(e);
    });

    // vade.ensure_whitelisted("did:evan:0xF4a6eA9978A30a0c66c286FD9e361981dA53277c", "674f62416c557e9748655147e70deb160ec43353317a9f4100d4fe299ac1e4e8", "did:evan:0xF4a6eA9978A30a0c66c286FD9e361981dA53277c", {}).then(b => console.log(b));

    // vade.whitelist_identity("did:evan:0xF4a6eA9978A30a0c66c286FD9e361981dA53277c", "674f62416c557e9748655147e70deb160ec43353317a9f4100d4fe299ac1e4e8", "did:evan:0xF4a6eA9978A30a0c66c286FD9e361981dA53277c", {}).then(b => console.log(b));

    // vade.did_create("did:evan", JSON.stringify({
    //     privateKey:"674f62416c557e9748655147e70deb160ec43353317a9f4100d4fe299ac1e4e8",
    //     identity:"did:evan:0xF4a6eA9978A30a0c66c286FD9e361981dA53277c"
    //   }), "",{}).then(res => { 
    //     console.log(res); 
    //     vade.did_resolve(res,{}).then(v => {
    //       console.log(v);
    //     }).catch(e => {
    //       console.log(e);
    //     });
    // }).catch(e => {
    //   console.log(e);
    // });
    

  }
}
