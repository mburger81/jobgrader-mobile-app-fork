/* tslint:disable */
/* eslint-disable */
/**
*/
export function set_panic_hook(): void;
/**
* @param {string} log_level
*/
export function set_log_level(log_level: string): void;
/**
* @param {string} did_or_method
* @param {any} config
* @returns {any}
*/
export function did_resolve(did_or_method: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function did_create(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function did_update(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function didcomm_receive(options: string, payload: string, config: any): any;
/**
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function didcomm_send(options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} custom_func_name
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function run_custom_function(did_or_method: string, custom_func_name: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_create_credential_definition(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_create_credential_offer(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_create_credential_proposal(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_create_credential_schema(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_create_revocation_registry_definition(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_update_revocation_registry(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_issue_credential(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_finish_credential(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_present_proof(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_request_credential(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_request_proof(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_revoke_credential(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {any} config
* @returns {any}
*/
export function vc_zkp_verify_proof(did_or_method: string, options: string, payload: string, config: any): any;
/**
* @param {string} func_name
* @param {string} did_or_method
* @param {string} options
* @param {string} payload
* @param {string} custom_func_name
* @param {any} config
* @returns {any}
*/
export function execute_vade(func_name: string, did_or_method: string, options: string, payload: string, custom_func_name: string, config: any): any;
/**
* Indicates the status returned from `PoKOfSignatureProof`
*/
export enum PoKOfSignatureProofStatus {
/**
* The proof verified
*/
  Success,
/**
* The proof failed because the signature proof of knowledge failed
*/
  BadSignature,
/**
* The proof failed because a hidden message was invalid when the proof was created
*/
  BadHiddenMessage,
/**
* The proof failed because a revealed message was invalid
*/
  BadRevealedMessage,
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly set_panic_hook: () => void;
  readonly set_log_level: (a: number, b: number) => void;
  readonly did_resolve: (a: number, b: number, c: number) => number;
  readonly did_create: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly did_update: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly didcomm_receive: (a: number, b: number, c: number, d: number, e: number) => number;
  readonly didcomm_send: (a: number, b: number, c: number, d: number, e: number) => number;
  readonly run_custom_function: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number) => number;
  readonly vc_zkp_create_credential_definition: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_create_credential_offer: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_create_credential_proposal: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_create_credential_schema: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_create_revocation_registry_definition: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_update_revocation_registry: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_issue_credential: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_finish_credential: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_present_proof: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_request_credential: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_request_proof: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_revoke_credential: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly vc_zkp_verify_proof: (a: number, b: number, c: number, d: number, e: number, f: number, g: number) => number;
  readonly execute_vade: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number) => number;
  readonly ffi_select: (a: number, b: number) => number;
  readonly ffi_path_compile: (a: number) => number;
  readonly ffi_select_with_compiled_path: (a: number, b: number) => number;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
  readonly __wbindgen_export_2: WebAssembly.Table;
  readonly _dyn_core__ops__function__FnMut__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__h1adf74df2ce76015: (a: number, b: number, c: number) => void;
  readonly _dyn_core__ops__function__FnMut__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__he55dde7206a70ba8: (a: number, b: number, c: number) => void;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_exn_store: (a: number) => void;
  readonly wasm_bindgen__convert__closures__invoke2_mut__h85817cb4277064e9: (a: number, b: number, c: number, d: number) => void;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
