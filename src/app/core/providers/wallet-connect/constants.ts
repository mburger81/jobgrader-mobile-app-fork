export enum CryptoNetworks {
    THXC = "https://polygon-mumbai.infura.io/v3/4458cf4d1689497b9a38b1d6bbf05e78",
    HMT_POLYGON = "https://polygon-rpc.com",
    HMT_SKALE = "https://mainnet.skalenodes.com/v1/wan-red-ain",
    EVAN_TEST = "https://testcore.evan.network",
    EVAN = "https://core.evan.network",
    POLYGON_TEST_MUMBAI = "https://polygon-mumbai.infura.io/v3/4458cf4d1689497b9a38b1d6bbf05e78",
    POLYGON = "https://polygon-rpc.com",
    GNOSIS = "https://rpc.gnosischain.com",
    ETHEREUM = "https://mainnet.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
    ETHEREUM_TEST_RINKEBY = "https://rinkeby.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
    ETHEREUM_TEST_KOVAN = "https://kovan.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
    // Fixed
    ARBITRUM = "https://1rpc.io/arb", // Arbitrum
    BLUE = "Blue Token", // Blue token
    BNB_SMART = "https://1rpc.io/bnb", // BNB Smart Chain
    BNB_BEACON = "BNB Beacon Chain", // BNB Beacon Chain
    MAKER_DAO = "Maker Dao", // MakerDAO
    MULTIVERSX = "MultiversX", // MultiversX
    NEAR = "https://rpc.mainnet.near.org", // Near
    OPTIMISM = "https://mainnet.optimism.io", // Optimism
    TEZOS = "Tezos Token", // Tezos
    UNISWAP = "Uniswap", // Uniswap
    // Optional
    AAVE = "Aave",
    AE = "Aeternity",
    AION = "Aion",
    APE = "Apecoin",
    AURORA = "https://mainnet.aurora.dev",
    AVAX = "https://api.avax.network/ext/bc/C/rpc",
    AXS = "Axie Infinity",
    BUSD = "Binance USD",
    CELO = "https://forno.celo.org",
    LINK = "ChainLink",
    CRO = "https://evm-cronos.crypto.org",
    MANA = "Decentraland Token",
    DOGE = "Dogecoin",
    DYDX = "dYdX",
    ENS = "Ethereum Name Service",
    FTM = "https://rpc.ankr.com/fantom/",
    FIL = "https://api.node.glif.io/rpc/v1",
    FLOW = "Flow",
    IMX = "Immutable X",
    CAKE = "PancakeSwap Token",
    SAND = "Sandbox Token",
    TRON = "https://bscscan.com/address/0x85eac5ac2f758618dfa09bdbe0cf174e7d574d5b",
    TWT = "Trust Wallet Token",
}

export enum CryptoNetworkNames {
    THXC = "The Helix Coin",
    HMT_POLYGON = "Human Token (Polygon)",
    HMT_SKALE = "Human Token (SKALE)",
    POLYGON_TEST_MUMBAI = "Polygon Mumbai-Testnet",
    POLYGON = "Polygon Mainnet",
    EVAN_TEST = "EPN Testnet",
    EVAN = "EPN Mainnet",
    ETHEREUM_TEST_RINKEBY = "Ethereum Testnet - rinkeby",
    ETHEREUM_TEST_KOVAN = "Ethereum Testnet - kovan",
    ETHEREUM_TEST_ROPSTEN = "Ethereum Testnet - ropsten",
    ETHEREUM = "Ethereum Mainnet",
    GNOSIS = "Gnosis Chain",
    // Fixed
    ARBITRUM = "Arbitrum Mainnet", // Arbitrum
    ARBITRUM_GOERLI_TESTNET = "Arbitrum Goerli Testnet", // Arbitrum Goerli Testnet
    BLUE = "Blue Token", // Blue token
    BNB_SMART = "BNB Smart Chain", // BNB Smart Chain and BNB Beacon Chain
    BNB_BEACON = "BNB Beacon Chain", // BNB Smart Chain and BNB Beacon Chain
    MAKER_DAO = "Maker Dao", // MakerDAO
    MULTIVERSX = "MultiversX", // MultiversX
    NEAR = "Near Token", // Near
    OPTIMISM = "Optimism Token", // Optimism
    TEZOS = "Tezos Token", // Tezos
    UNISWAP = "Uniswap", // Uniswap
    // Optional
    AAVE = "Aave",
    AE = "Aeternity",
    AION = "Aion",
    APE = "Apecoin",
    AURORA = "Aurora",
    AVAX = "Avalanche",
    AXS = "Axie Infinity",
    BUSD = "Binance USD",
    CELO = "Celo",
    LINK = "ChainLink",
    CRO = "Cronos",
    MANA = "Decentraland Token",
    DOGE = "Dogecoin",
    DYDX = "dYdX",
    ENS = "Ethereum Name Service",
    FTM = "Fantom Token",
    FIL = "Filecoin",
    FLOW = "Flow",
    IMX = "Immutable X",
    CAKE = "PancakeSwap Token",
    SAND = "Sandbox Token",
    TRON = "Tron Token",
    TWT = "Trust Wallet Token",
}

export enum EthereumNetworks {
    RINKEBY = "rinkeby",
    KOVAN = "kovan",
    ROPSTEN = "ropsten",
    GOERLI = "goerli",
    MAINNET = "mainnet"
}

export enum CryptoCurrency {
    THXC = "THXC",
    HMT_POLYGON = "HMT_POLYGON",
    HMT_SKALE = "HMT_SKALE",
    MATIC = "MATIC",
    EVE = "EVE",
    ETH = "ETH",
    GNOSIS = "XDAI",
    // Fixed
    ARETH = "ARETH", // Arbitrum
    BLUR = "BLUR", // Blue token
    BNB_BEACON = "BNB_BEACON", // BNB Smart Chain and BNB Beacon Chain
    BNB_SMART = "BNB_SMART", // BNB Smart Chain and BNB Beacon Chain
    MKR = "MKR", // MakerDAO
    EGLD = "EGLD", // MultiversX
    NEAR = "NEAR", // Near
    OP = "OP", // Optimism
    XTZ = "XTZ", // Tezos
    UNI = "UNI", // Uniswap
    // Optional
    AAVE = "AAVE", // Aave
    AE = "AE", // Aeternity
    AION = "AION", // Aion
    APE = "APE", // Apecoin
    AURORA = "AURORA", // Aurora
    AVAX = "AVAX", // Avalanche
    AXS = "AXS", // Axie Infinity
    BUSD = "BUSD", // Binance USD
    CELO = "CELO", // Celo
    LINK = "LINK", // ChainLink
    CRO = "CRO", // Cronos
    MANA = "MANA", // Decentraland Token
    DOGE = "DOGE", // Dogecoin
    DYDX = "DYDX", // dYdX
    ENS = "ENS", // Ethereum Name Service
    FTM = "FTM", // Fantom Token
    FIL = "FIL", // Filecoin
    FLOW = "FLOW", // Flow
    IMX = "IMX", // Immutable X
    CAKE = "CAKE", // PancakeSwap Token
    SAND = "SAND", // Sandbox Token
    TRON = "TRON", // Tron Token
    TWT = "TWT", // Trust Wallet Token
}

export enum CryptoCurrencyColors {
    THXC = "#FF9C00",
    HMT_POLYGON = "#4B00FF",
    HMT_SKALE = "#4B00FF",
    MATIC = "#8247E5",
    EVE = "#003399",
    ETH = "#8C8C8C",
    XDAI = "#48A9A6",
    // Fixed
    ARETH = "#269FF0", // Arbitrum
    BLUR = "#D04C18", // Blur token
    BNB = "#F0B90E", // BNB Smart Chain and BNB Beacon Chain
    MKR = "#52AC9E", // MakerDAO
    EGLD = "#22F7DD", // MultiversX
    NEAR = "#D6D6D6", // Near
    OP = "#FE0221", // Optimism
    XTZ = "#0263FF", // Tezos
    UNI = "#EC83B8", // Uniswap
       // Optional
    AAVE = "#35B3C3", // Aave
    AE = "#CF4471", // Aeternity
    AION = "#000000", // Aion
    APE = "#0649D4", // Apecoin
    AURORA = "#6FD24D", // Aurora
    AVAX = "#E84042", // Avalanche
    AXS = "#04A1F0", // Axie Infinity
    BUSD = "#F0B90E", // Binance USD
    CELO = "#FCFE53", // Celo
    LINK = "#325DD1", // ChainLink
    CRO = "#2B3865", // Cronos
    MANA = "#FF6B58", // Decentraland Token
    DOGE = "#B39135", // Dogecoin
    DYDX = "#171523", // dYdX
    ENS = "#6B9DF6", // Ethereum Name Service
    FTM = "#1969FF", // Fantom Token
    FIL = "#0190FF", // Filecoin
    FLOW = "#03EF8A", // Flow
    IMX = "#464849", // Immutable X
    CAKE = "#D1884F", // PancakeSwap Token
    SAND = "#00AEEE", // Sandbox Token
    TRON = "#FF0112", // Tron Token
    TWT = "#3375BB", // Trust Wallet Token
}

export enum Explorers {
    EVAN_TEST = "https://testexplorer.evan.network/tx/",
    EVAN = "https://explorer.evan.network/tx/",
    POLYGON_TEST_MUMBAI = "https://mumbai.polygonscan.com/tx/",
    POLYGON = "https://polygonscan.com/tx/",
    GNOSIS = "https://blockscout.com/xdai/mainnet/tx/",
    ETHEREUM = "https://etherscan.io/tx/",
    ETHEREUM_TEST_RINKEBY = "https://rinkeby.etherscan.io/tx/",
    // Fixed
    ARBITRUM = "Arbitrum Mainnet", // Arbitrum
    ARBITRUM_GOERLI_TESTNET = "Arbitrum Goerli Testnet", // Arbitrum Goerli Testnet
    BLUE = "Blue Token", // Blue token
    BNB_SMART = "BNB Smart Chain", // BNB Smart Chain and BNB Beacon Chain
    BNB_BEACON = "BNB Beacon Chain", // BNB Smart Chain and BNB Beacon Chain
    MAKER_DAO = "Maker Dao", // MakerDAO
    MULTIVERSX = "MultiversX", // MultiversX
    NEAR = "Near Token", // Near
    OPTIMISM = "Optimism Token", // Optimism
    TEZOS = "Tezos Token", // Tezos
    UNISWAP = "Uniswap", // Uniswap
    // Optional
}

export enum Contracts {
    THXC = "0x6f9fbd3d6eccf1401bd1a2b55f4b3495cacb7ee4", //"0x23df05d0c9889136f7da21548810674b7846bb0a",
    HMT_POLYGON = "0xc748B2A084F8eFc47E086ccdDD9b7e67aEb571BF",
    HMT_SKALE = "0x6E5FF61Ea88270F6142E0E0eC8cbe9d67476CbCd",
    LIQUIDITY = "0x787B732a8c0E62320Db2bb13492b7227c5cf9523"
}
/*
Arbitrum
Blur
BNB Beacon Chain
BNB Smart Chain
Ethereum
Gnosis Chain
Maker
MultiversX
Near
Optimism
Polygon
Tezos
Uniswap


optional


Aave
Aeternity
Aion
Apecoin
Aurora
Avalanche
Axie Infinity
Binance USD
Celo
Chainlink
Cronos
Decentraland
Dogecoin
dYdX
Ethereum Name Service
Fantom
Filecoin
Flow
Immutable X
PancakeSwap
Polygon zkEVM
The Sandbox
Tron
Trust Wallet
*/