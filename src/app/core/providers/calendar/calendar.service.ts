import { Injectable } from '@angular/core';
import { Calendar, CalendarOptions } from '@awesome-cordova-plugins/calendar/ngx';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  constructor(
    private calendar: Calendar
  ) { }

  public init(calendarName: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.calendar.hasWritePermission().then(permission => {
        if(permission) {
          this.calendar.createCalendar(calendarName).then((res) => {
            resolve(res);
          }).catch(e => {
            reject(e);
          })
        } else {
          this.calendar.requestWritePermission().then((val) => {
            // console.log(val);
            this.calendar.createCalendar(calendarName).then((res) => {
              resolve(res);
            }).catch(e => {
              reject(e);
            })
          }).catch(e => {
            reject(e);
          })
        }
      }).catch(e => {
        reject(e);
      })
    })
  }

  private getTimezoneString(location: string) {
    return 'Europe/Berlin';
    // Need to be expanded more based on the location values provided
  }

  public addEvent(calendarName: string, eventName: string, eventDescription: string, startDate: string, endDate: string, location: string, url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      var options = this.calendar.getCalendarOptions();
      options.calendarName = calendarName;
      options.url = `helix-id://helix-id.com/ticket?string=${url}`;
      // console.log(JSON.stringify(options));
      var startDateDateFormat = new Date(startDate);
      var endDateDateFormat = new Date(endDate);
      var modifiedStartDateDateFormat = new Date(startDateDateFormat.getTime() + startDateDateFormat.getTimezoneOffset() * 60 * 1000);
      var modifiedEndDateDateFormat = new Date(endDateDateFormat.getTime() + endDateDateFormat.getTimezoneOffset() * 60 * 1000);
      // console.log("Start Date: "); console.log(startDateDateFormat); console.log(modifiedStartDateDateFormat);
      // console.log("End Date: ");   console.log(endDateDateFormat);   console.log(modifiedEndDateDateFormat);
      this.calendar.createEventInteractivelyWithOptions(eventName, location, eventDescription, modifiedStartDateDateFormat, modifiedEndDateDateFormat, options).then((success) => {
        // console.log(success);
        resolve(success)
      }).catch(e => {
        console.log(e);
        this.init(calendarName).then((res) => {
          this.calendar.createEventInteractivelyWithOptions(eventName, location, eventDescription, modifiedStartDateDateFormat, modifiedEndDateDateFormat, options).then((success) => {
            // console.log(success);
            resolve(success)
          }).catch(e => {
            console.log(e);
            reject(e);
          })
        })
      })
    })
  }
 }
