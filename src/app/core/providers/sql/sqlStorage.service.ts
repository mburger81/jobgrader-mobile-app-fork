import { Injectable } from '@angular/core';
import { ChatThread, ChatMessage, ComboUserWrapper, RosterItem, ChatThreadRequest } from '../chat/chat-model';
import { MerchantDetails } from '../marketplace/marketplace-model';
import { SqlStorage } from '../../../../sqlStorage/sqlStorage';
import { SQLiteService } from '../../../services/sqlite.service';



@Injectable()
export class SQLStorageService {

  private _storage;
  public dbIsLocked = false;


  constructor(
    // private _storage: SqlStorage
      private sqliteService: SQLiteService
  ) {
  }

  async init() {
    this._storage = new SqlStorage(this.sqliteService);
    await this._storage.init();
    return this.makeTables();
  }

  makeTables() {
    var p = [];

    // Chat tables
    p.push(this.createTable('chatThreadRequests', 'threadGuid TEXT, fromUsername TEXT, toUsername TEXT, chatThreadRequestObj TEXT'));
    p.push(this.createTable('chatThreads', 'threadGuid TEXT, withUsername TEXT, threadObj TEXT, markedAsUnread INTEGER'));
    p.push(this.createTable('chatThreadMessages', 'threadGuid TEXT, messageGuid TEXT, messageObj TEXT, markedAsUnread INTEGER'));
    p.push(this.createTable('chatThreadPhotos', 'threadGuid TEXT, photo TEXT'));
    p.push(this.createTable('draftMessages', 'threadGuid TEXT, messageText TEXT'));

    // Marketplace tables
    p.push(this.createTable('marketplaceMerchants', 'merchantId TEXT, merchantName TEXT, productName TEXT, productCategories TEXT, merchantDetailsObj TEXT'));

    return Promise.all(p);
  }

  clearTables(table?: string) {
    var tables: string[] = [];
    var p = [];
    // mburger TODO: with the new capacitor plugin there is problem I have to clearify with owner
    // so for now I disable the clearTables logic
    // if (table != null) {
    //   tables = [table];
    // }
    // else {
    //   tables = [
    //     'chatThreadRequests',
    //     'chatThreads',
    //     'chatThreadMessages',
    //     'chatThreadPhotos',
    //     'marketplaceMerchants',
    //     'draftMessages'
    //   ];
    // }

    // tables.forEach(tableName => {
    //   p.push(this.query('DELETE FROM ' + tableName, null, tableName + " data deleted."));
    // });

    return Promise.all(p);
  }

  createTable(tableName: string, columnsSql: string) {
    var sql = "CREATE TABLE IF NOT EXISTS " + tableName + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + columnsSql + ")";
    return this.query(sql, null, tableName + " init db.");
  }

  internal_consoleLog(label: string, msg: any, stringify: boolean) {
    if (!msg) { return; }
    if (console && console.log) {
      console.log(label + ((stringify) ? JSON.stringify(msg) : msg));
    }
  }

  logInfo(msg: any) {
    this.internal_consoleLog("*** Info: ", msg, false);
  }

  logError(sql, error: any) {
    this.internal_consoleLog("*** Error: ", error, true);
    this.internal_consoleLog("*** Sql: ", sql, false);
  }

  query(sql: string, params?: any, infoMsg?: string): Promise<any> {
    return new Promise(resolve => {
      this._storage.query(sql, params).then(
        (data) => {
          this.logInfo(infoMsg);
          resolve(data);
        },
        (error) => {
          this.logError(sql, error);
          resolve(null);
        }
      );
    });
  }


  generateUniqueId() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
  }

  hasRows(data: any) {
    return data?.length > 0;
    // return (data && data.res?.rows.length > 0);
  }


  // ******************* Methods here *******************


  getMarketPlaceMerchants(): Promise<MerchantDetails[]> {
    return new Promise(resolve => {
      var sql = "SELECT * FROM marketplaceMerchants";
      this.query(sql).then(data => {
        var list: MerchantDetails[] = [];
        if (this.hasRows(data)) {
          for (var i = 0; i < data.length; i++) {
            var merchant = <MerchantDetails>this.parseJSONSafe(data[i].merchantDetailsObj);
            if (merchant) {
              list.push(merchant);
            }
          }
        }
        resolve(list);
      });
    });
  }

  searchMarketPlaceMerchants(search: string): Promise<MerchantDetails[]> {
    return new Promise(resolve => {
      search = `%${search}%`;
      var sql = "SELECT * FROM marketplaceMerchants WHERE merchantName LIKE ? OR productName LIKE ? OR productCategories LIKE ?";
      var params = [search, search, search];
      this.query(sql, params).then(data => {
        var list: MerchantDetails[] = [];
        if (this.hasRows(data)) {
          for (var i = 0; i < data.length; i++) {
            var merchant = <MerchantDetails>this.parseJSONSafe(data[i].merchantDetailsObj);
            if (merchant) {
              list.push(merchant);
            }
          }
        }
        resolve(list);
      });
    });
  }

  addOrUpdateMerchant(merchant: MerchantDetails): Promise<boolean> {
    return new Promise(resolve => {
      this.isExistingMerchant(merchant.id).then(isExisting => {
        if (!isExisting) {
          this.addMerchant(merchant.id, merchant).then(newNumericId => {
            resolve(newNumericId != null);
          });
        }
        else {
          this.updateMerchant(merchant.id, merchant).then(merchantWasUpdated => {
            resolve(merchantWasUpdated);
          });
        }
      });
    });
  }

  addMerchant(merchantId: string, merchant: MerchantDetails): Promise<number> {
    return new Promise(resolve => {
      var data: any;
      try {
        data = JSON.stringify(merchant);
        var sql = `INSERT INTO marketplaceMerchants(merchantId, merchantName, productName, productCategories, merchantDetailsObj)
                    SELECT ?, ?, ?, ?, ?
                    WHERE NOT EXISTS(SELECT 1 FROM marketplaceMerchants WHERE merchantId = ?);`;
        var params = [merchantId, merchant.merchantName, merchant.productName, JSON.stringify(merchant.productCategories), data, merchantId];
        this.query(sql, params).then((data) => {

          // mburger TODO: right now this is not working for the capacitor sqlite plugin, but newNumericId is even not cared
          // so right now we just don't care about it
          var newNumericId = (data && data.res && data.res.insertId != null && data.res.insertId != 0) ? data.res.insertId : null;

          if (newNumericId != null) {
            console.log(`*** Inserted new merchant for merchantId: ${merchantId}, numericId: ${newNumericId}`);
          }
          else {
            console.log(`Ignored duplicate merchant merchantId: ${merchantId} `);
          }

          resolve(newNumericId);
        });
      }
      catch(e) {
        console.log('*** addMerchant stringiy err: ***');
        console.log(e);
        console.log(merchant);
        resolve(null);
      }
    });
  }

  updateMerchant(merchantId: string, merchant: MerchantDetails): Promise<boolean> {
    return new Promise(resolve => {
      var data: any;
      try {
        data = JSON.stringify(merchant);
        var sql = `UPDATE marketplaceMerchants
                    SET
                      merchantName = ?,
                      productName = ?,
                      productCategories = ?,
                      merchantDetailsObj = ?
                    WHERE merchantId = ?`;

        var params = [merchant.merchantName, merchant.productName, JSON.stringify(merchant.productCategories), data, merchantId];
        this.query(sql, params).then((data) => {
          console.log(`*** Updated  merchantId: ${merchantId}`);
          resolve(true);
        });
      }
      catch(e) {
        console.log('*** addMerchant stringiy err: ***');
        console.log(e);
        console.log(merchant);
        resolve(false);
      }
    });
  }

  isExistingMerchant(merchantId: string): Promise<boolean> {
    return new Promise(resolve => {
      var sql = "SELECT * FROM marketplaceMerchants WHERE merchantId = ? LIMIT 1";
      this.query(sql, [merchantId]).then(data => {
        var exists = this.hasRows(data);
        resolve(exists);
      });
    });
  }

  getChatThreadRequests(): Promise<ChatThreadRequest[]> {
    return new Promise(resolve => {
      this.query("SELECT * FROM chatThreadRequests").then(data => {
        var list: ChatThreadRequest[] = [];
        if (this.hasRows(data)) {
          for (var i = 0; i < data.length; i++) {
            var chatThreadRequest = <ChatThreadRequest>this.parseJSONSafe(data[i].chatThreadRequestObj);
            if (chatThreadRequest) {
              list.push(chatThreadRequest);
            }
          }
        }
        resolve(list);
      });
    });
  }

  addChatThreadRequest(newChatThreadRequest: ChatThreadRequest): Promise<boolean>  {
    return new Promise(resolve => {
      try {
        var from = (newChatThreadRequest.from) ? newChatThreadRequest.from : null;
        var to = (newChatThreadRequest.to) ? newChatThreadRequest.to : null;
        var data = JSON.stringify(newChatThreadRequest);
        var sql = `INSERT INTO chatThreadRequests(threadGuid, fromUsername, toUsername, chatThreadRequestObj)
                    SELECT ?, ?, ?, ?
                    WHERE NOT EXISTS(SELECT 1 FROM chatThreadRequests WHERE threadGuid = ?);`;
        var params = [newChatThreadRequest.guid, from, to, data, newChatThreadRequest.guid];
        this.query(sql, params).then((data) => {
          var wasInserted = (data && data.res && data.res.insertId != null);
          console.log(wasInserted ?
          `*** Inserted new ChatThreadRequest for thread: ${newChatThreadRequest.guid}, from: ${from}, to: ${to}` :
          `Ignored duplicate ChatThreadRequest id: ${newChatThreadRequest.guid}, from: ${from}, to: ${to}`)
          resolve(wasInserted);
        });
      }
      catch(e) {
        console.log('*** addChatThreadRequest stringiy err: ***');
        console.log(e);
        console.log(newChatThreadRequest);
        resolve(false);
      }
    });
  }


  addMessage(threadGuid: string, chatMessage: ChatMessage): Promise<number> {
    return new Promise(resolve => {
      // If we generated a message on the device, there will be no id from the xmpp server.
      if (chatMessage.id == null) {
        chatMessage.id = this.generateUniqueId();
      }
      var data: any;
      try {
        data = JSON.stringify(chatMessage);
        var sql = `INSERT INTO chatThreadMessages(threadGuid, messageGuid, messageObj, markedAsUnread)
                    SELECT ?, ?, ?, ?
                    WHERE NOT EXISTS(SELECT 1 FROM chatThreadMessages WHERE messageGuid = ?);`;
        var params = [threadGuid, chatMessage.id, data, (chatMessage.markedAsUnread) ? 1 : 0, chatMessage.id];
        this.query(sql, params).then((data) => {
          var newNumericId = (data && data.res && data.res.insertId != null && data.res.insertId != 0) ? data.res.insertId : null;
          chatMessage.numericId = newNumericId;

          if (chatMessage.numericId != null) {
            console.log(`*** Inserted new message for thread: ${threadGuid}, numericId: ${chatMessage.numericId}`);
            console.log(`*** NumericId: ${chatMessage.numericId}, data: ${data}`);
          }
          else {
            console.log(`Ignored duplicate message id: ${chatMessage.id} `);
          }

          resolve(newNumericId);
        });
      }
      catch(e) {
        console.log('*** addMessage stringiy err: ***');
        console.log(e);
        console.log(chatMessage);
        resolve(null);
      }
    });
  }

  addNewThreadsWithHistory(chatThreads: ChatThread[]): Promise<void> {
    // Workaround: In case this is called with broken data.
    chatThreads = chatThreads.filter(ct => { return ct.userWrapper && ct.userWrapper.chatUserName != null; })

    return new Promise(resolveAll => {
      this.dbIsLocked = true;
      this.clearTables().then(() => {
        if (chatThreads && chatThreads.length > 0) {
          var p = [];
          chatThreads.forEach(thread => {
            p.push(this.addNewThread(thread));
          });

          Promise.all(p).then(() => {
            this.dbIsLocked = false;
            resolveAll();
          });
        }
        else {
          this.dbIsLocked = false;
          resolveAll();
        }
      });
    });
  };

  addNewThread(chatThread: ChatThread): Promise<boolean> {
    return new Promise(resolveAll => {
      var wasAdded: boolean = false;
      var existingThread: boolean = false;

      var preCheck = new Promise<void>(resolve => {
        this.query("SELECT * FROM chatThreads WHERE withUsername = ?",
          [chatThread.userWrapper.chatUserName]).then(data => {
          existingThread = this.hasRows(data);
          resolve();
        });
      });

      var a = preCheck.then(() => {
        return new Promise<void>(resolve => {
          if (existingThread) {
            resolve();
          }
          else if (chatThread.userWrapper.displayPicture) {
            this.query("INSERT INTO chatThreadPhotos (threadGuid, photo) VALUES (?, ?)",
              [chatThread.guid, chatThread.userWrapper.displayPicture],
              `*** Inserted new photo for thread: ${chatThread.guid}`).then(() => {
              resolve();
            });
          }
          else {
            resolve();
          }
        });
      })

      var b = a.then(() => {
        return new Promise<void>(resolve => {
          if (existingThread) {
            resolve();
          }
          else if (chatThread.messages && chatThread.messages.length > 0) {
            var p = [];
            chatThread.messages.forEach(m => {
              m.parsedMessage = this.parseJSONSafe(m.message);
              p.push(this.addMessage(chatThread.guid, m));
            });

            Promise.all(p).then(() => {
              resolve();
            });
          }
          else {
            resolve();
          }
        });
      });

      b.then(() => {
        if (existingThread) {
          resolveAll(wasAdded);
        }
        else {
          // save out the thread, but without serialising the photo, for speed.
          var base64 = (chatThread.userWrapper.displayPicture) ? chatThread.userWrapper.displayPicture : null;
          chatThread.userWrapper.displayPicture = null;

          var messages = (chatThread.messages && chatThread.messages.length > 0) ? chatThread.messages : [];
          chatThread.messages = [];

          var data: any;
          try {
            data = JSON.stringify(chatThread);
            this.query("INSERT INTO chatThreads (threadGuid, withUsername, threadObj, markedAsUnread) VALUES (?, ?, ?, ?)",
              [chatThread.guid, chatThread.userWrapper.chatUserName, data, (chatThread.markedAsUnread) ? 1 : 0],
              `*** Inserted new thread: ${chatThread.guid}`).then(() => {
              chatThread.userWrapper.displayPicture = base64; // put it back on, so it's still in the contact-list.
              chatThread.messages = messages; // put them back on as they're already loaded.
              wasAdded = true;
              resolveAll(wasAdded);
            });
          }
          catch(e) {
            console.log('*** addNewThread stringiy err: ***');
            console.log(e);
            console.log(chatThread);
            resolveAll(wasAdded);
          }
        }
      });
    });
  }

  updateAndSetPhotoByThreadGuid(thread: ChatThread, photo: string): Promise<boolean> {
    return new Promise(resolve => {
      this.query("UPDATE chatThreadPhotos SET photo = ? WHERE threadGuid = ?",
        [photo, thread.guid],
        `*** Updated thread photo ${thread.guid}.`).then(() => {
          thread.userWrapper.displayPicture = photo;
        resolve(true);
      });
    });
  }

  updateThreadMarkedUnread(threadGuid: string): Promise<void> {
    return new Promise(resolve => {
      this.query("UPDATE chatThreads SET markedAsUnread = 1 WHERE threadGuid = ?",
        [threadGuid],
        `*** Updated thread ${threadGuid} markedAsUnread as: ${threadGuid}`).then(() => {
        resolve();
      });
    });
  }

  updateDraftMessage(threadGuid: string, msg: string): Promise<void> {
    return new Promise(resolve => {
      this.query("DELETE FROM draftMessages WHERE threadGuid = ?", [threadGuid]).then(() => {
        if (msg && msg.trim().length > 0) {
          this.query("INSERT INTO draftMessages (threadGuid, messageText) VALUES(?, ?)",
            [threadGuid, msg],
            `*** Updated thread draft message ${threadGuid}.`).then(() => {
            resolve();
          });
        }
        else {
          resolve();
        }
      });
    });
  }

  updateMessagesMarkAsRead(messages: ChatMessage[]): Promise<void> {
    return new Promise(resolve => {
      if (messages && messages.length > 0) {
        messages.forEach(m => {
          m.parsedMessage = this.parseJSONSafe(m.message);
          m.markedAsUnread = false;
          this.query("UPDATE chatThreadMessages SET messageObj = ?, markedAsUnread = ? WHERE messageGuid = ?",
            [JSON.stringify(m), 0, m.id],
            `*** Updated message ${m.id} markedAsUnread as: ${m.markedAsUnread}`);
        });
        // we don't wanna wait for this to return.
        resolve();
      }
      else {
        resolve();
      }
    });
  }

  getThreadUnreadCount(threadGuid: string): Promise<number> {
    return new Promise(resolve => {
      this.query(`SELECT COUNT(id) as 'unreadCount' FROM chatThreadMessages WHERE threadGuid = ? AND markedAsUnread = 1`,
        [threadGuid]).then(data => {
          var unreadCount = (this.hasRows(data)) ? data[0].unreadCount : 0;
          resolve(unreadCount);
        });
    });
  }

  getDraftMessageForThread(threadGuid: string): Promise<string> {
    return new Promise(resolve => {
      this.query(`SELECT messageText as 'messageText' FROM draftMessages WHERE threadGuid = ?`,
        [threadGuid]).then(data => {
          var messageText = (this.hasRows(data)) ? data[0].messageText : null;
          resolve(messageText);
        });
    });
  }

  getChatThreadGuidByWithUsernameStripped(usernameStripped: string): Promise<string> {
    return new Promise(resolve => {
      this.query("SELECT * FROM chatThreads WHERE withUsername = ?",
        [usernameStripped]).then(data => {
          var threadGuid = (this.hasRows(data)) ? data[0].threadGuid : null;
          resolve(threadGuid);
      });
    });
  }

  getChatThreadsWithLastMessage(threadGuid?: string): Promise<ChatThread[]> {
    return new Promise(resolve => {

      var sql = (threadGuid) ? "SELECT * FROM chatThreads WHERE threadGuid = ?" : "SELECT * FROM chatThreads";
      var params = (threadGuid) ? [threadGuid] : null;

      this.query(sql, params).then(data => {
        var list: ChatThread[] = [];
        if (this.hasRows(data)) {
          for (var i = 0; i < data.length; i++) {
            var thread = this.parseJSONSafe(data[i].threadObj);
            if (thread) {
              thread.markedAsUnread = (data[i].markedAsUnread == 1) ? true : false;
              list.push(thread);
            }
          }
        }

        if (list && list.length > 0) {
          var p = [];
          list.forEach(thread => {
            p.push(this.getThreadUnreadCount(thread.guid).then(unreadCount => {
              thread.unreadCount = unreadCount;
              if (thread.unreadCount == 0) {
                thread.markedAsUnread = false;
              }
            }));

            p.push(this.getContactPhoto(thread.guid).then(photo => {
              if (photo) {
                thread.userWrapper.displayPicture = (photo) ? photo : null;
              }
            }));

            p.push(this.getLastMessageInThread(thread.guid).then(message => {
              if (message) {
                thread.messages.push(message);
              }
            }));
          });
          Promise.all(p).then(() => {
            resolve(list);
          });
        }
        else {
          resolve(list);
        }

      });
    });
  }

  getAllMessagesInThread(guid: string): Promise<ChatMessage[]> {
    return new Promise(resolve => {
      this.query("SELECT * FROM chatThreadMessages WHERE threadGuid = ? ORDER BY id ASC", [guid]).then(data => {
        var list: ChatMessage[] = [];
        if (this.hasRows(data)) {
          for (var i = 0; i < data.length; i++) {
            var message = this.parseJSONSafe(data[i].messageObj);
            if (message) {
              message.numericId = data[i].id;
              message.parsedMessage = this.parseJSONSafe(message.message);
              message.markedAsUnread = (data[i].markedAsUnread == 1) ? true : false;
              list.push(message);
            }
          }
        }
        resolve(list);
      });
    });
  }

  getLastMessageInThread(guid: string): Promise<ChatMessage> {
    return new Promise(resolve => {
      this.query("SELECT * FROM chatThreadMessages WHERE threadGuid = ? ORDER BY id DESC LIMIT 1", [guid]).then(data => {
        var message = (this.hasRows(data)) ? this.parseJSONSafe(data[0].messageObj) : null;
        if (message) {
          message.numericId = data[0].id;
          message.parsedMessage = this.parseJSONSafe(message.message);
          message.markedAsUnread = (data[0].markedAsUnread == 1) ? true : false;
        }
        resolve(message);
      });
    });
  }

  getXMessagesInThread(guid: string, firstId: number, messageFetchSize: number): Promise<ChatMessage[]> {
    return new Promise(resolve => {
      var sql = (firstId == null) ?
        `SELECT * FROM (SELECT * FROM chatThreadMessages WHERE threadGuid = ? ORDER BY id DESC LIMIT ?) ORDER BY id ASC;` :
        `SELECT * FROM (SELECT * FROM chatThreadMessages WHERE threadGuid = ? AND id < ? ORDER BY id DESC LIMIT ?) ORDER BY id ASC;`;
      var params =  (firstId == null) ?
        [guid, messageFetchSize] :
        [guid, firstId, messageFetchSize];

      this.query(sql, params).then(data => {
        var list: ChatMessage[] = [];
        if (this.hasRows(data)) {
          for (var i = 0; i < data.length; i++) {
            var message = this.parseJSONSafe(data[i].messageObj);
            if (message) {
              message.numericId = data[i].id;
              message.parsedMessage = this.parseJSONSafe(message.message);
              message.markedAsUnread = (data[i].markedAsUnread == 1) ? true : false;
              list.push(message);
            }
          }
        }
        resolve(list);
      });
    });
  }

  getContactPhoto(guid: string): Promise<string> {
    return new Promise(resolve => {
      this.query("SELECT * FROM chatThreadPhotos WHERE threadGuid = ?", [guid]).then(data => {
        let photo = (this.hasRows(data)) ? data[0].photo : null;
        resolve(photo);
      });
    });
  }

  deleteThreadAndAnyFriendRequestsByGuid(guid: string): Promise<boolean> {
    return new Promise(resolve => {
      var p = [];
      p.push(this.query("DELETE FROM chatThreadRequests WHERE threadGuid = ?", [guid], `*** Deleted chatThreadRequest: ${guid}`));
      p.push(this.query("DELETE FROM chatThreads WHERE threadGuid = ?", [guid], `*** Deleted thread: ${guid}`));
      p.push(this.query("DELETE FROM chatThreadMessages WHERE threadGuid = ?", [guid], `*** Deleted messages for thread: ${guid}`));
      p.push(this.query("DELETE FROM chatThreadPhotos WHERE threadGuid = ?", [guid], `*** Deleted photo for thread: ${guid}`));

      Promise.all(p).then(() => {
        resolve(true);
      });
    });
  }

  deleteFriendRequestOnlyByUsername(username: string): Promise<boolean> {
    return new Promise(resolve => {
      var p = [];
      p.push(this.query("DELETE FROM chatThreadRequests WHERE fromUsername = ?", [username], `*** Deleted chatThreadRequest from : ${username}`));
      p.push(this.query("DELETE FROM chatThreadRequests WHERE toUsername = ?", [username], `*** Deleted chatThreadRequest to : ${username}`));

      Promise.all(p).then(() => {
        resolve(true);
      });
    });
  }

  parseJSONSafe(inputData: any) {
    try {
      return JSON.parse(inputData);
    }
    catch(e) {
      console.log('*** parseJSONSafe err: ***');
      console.log(e);
      return null;
    }
  }

}