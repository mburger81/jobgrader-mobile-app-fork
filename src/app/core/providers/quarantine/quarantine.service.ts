import { Injectable } from '@angular/core';
import { ApiProviderService } from '../api/api-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';

@Injectable({
  providedIn: 'root'
})
export class QuarantineService {



  private sampleQuestions = [
      {
         "seq":1,
         "id":"01",
         "text":"Welcome! What brought you here?",
         "answerType":"radio",
         "options":[
            {
               "text":"self infected",
               "id":"01A"
            },
            {
               "text":"close person contact",
               "id":"01B"
            },
            {
               "text":"more distant contact",
               "id":"01C"
            }
         ]
      },
      {
         "seq":2,
         "id":"02",
         "text":"To which group of people do you belong?",
         "answerType":"radio",
         "options":[
            {
               "text":"systemically important person",
               "id":"02A"
            },
            {
               "text":"children and young people",
               "id":"02B"
            },
            {
               "text":"all other groups of person",
               "id":"02C"
            }
         ]
      },
      {
         "seq":3,
         "id":"03",
         "text":"What is your current vaccination or recovery status?",
         "answerType":"userCovidData",
         "options":[
            {
               "text":"How many EU-approved vaccines have you received so far?",
               "type":"number",
               "min": 0,
               "max": 4,
               "id":"03A"
            },
            {
               "text":"When was your last vaccination?",
               "type":"datepicker",
               "id":"03B"
            },
            {
               "text":"Is your recovery longer than 3 months?",
               "type": "yesno",
               "id":"03C"
            }
         ]
      },
      // {
      //    "seq":5,
      //    "id":"05",
      //    "text":"Question 5",
      //    "answerType":"checkbox",
      //    "options":[
      //       {
      //          "text":"Choice 1",
      //          "id":"05A"
      //       },
      //       {
      //          "text":"Choice 2",
      //          "id":"05B"
      //       }
      //    ]
      // },
      {
         "seq":4,
         "id":"04",
         "text":"Almost Done!",
         "answerType":"datepicker",
         "options": [{
            "text": "When did you make your positive test?",
            "id": "04A"
         }, {
            "text": "When did you have contact with the positive tested person?",
            "id": "04B"
         }]
      }
    ];

  private sampleUserResponse = [
    {
         "id":"01",
         "answers":[
            "01A"
         ]
      },
      {
         "id":"02",
         "answers":[
            "02A",
            "02B"
         ]
      }
   ];

  private sampleMWResponse = [
      {
         "seq":1,
         "htmlContent":"<h1>Quarantine for 10 days</h1><p><ul><li>Free testing after 7 days with PCR or antigen test (Schnelltest)</li><li>Medical staff can test free after 7 days without having 48h no symptoms and need to have a PCR Test. No antigen test allowed.</li><li>Children and Infants, which go to school, kindergarten, kita, etc. can test free after 5 days with PCR or antigen test (Schnelltest)</li></ul><h1>No Quarantine</h1><p><ul><li>After a positive antigen test, the infection must be confirmed by PCR test. The quarantine may be interrupted for this purpose. If the PCR test is negative, the quarantine ends.</li></ul></p>"
      },
      {
         "seq":2,
         "htmlContent":"<h1>No quarantine when</h1><p><ul><li>Booster Vaccination (= 3 vaccination)</li><li>Twice vaccinated plus recovered (any order)</li><li>Freshly completely vaccinated (less than 3 months, from date of 2 vaccination)</li><li>Freshly recovered (less than 3 months, from the date of the positive test)</li><li>Recovered plus freshly vaccinated once (less than 3 months, from the date of the positive test)</li></ul></p><h1>Quarantine for</h1><p><ul><li>Contact person with complete vaccination older than 3 months </li><li>Contact person with one vaccination </li><li>Contact person with recovery older than 3 months</li><li>Free testing after 7 days with PCR test or antigen test</li><li>Medical staff can test free after 7 days without having 48h no symptoms and need to have a PCR Test. No antigen test allowed.</li><li>Children and Infants, which go to school, kindergarten, kita, etc. can test free after 5 days with PCR or antigen test (Schnelltest)</li></ul></p>"
      }
   ]

  constructor(
    private _ApiProviderService: ApiProviderService,
    private _SecureStorageService: SecureStorageService
  ) { }

  public getQuestions(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._SecureStorageService.getValue(SecureStorageKey.language, false).then((language) => {
        this._ApiProviderService.getQuarantineQuestions(language).then(res => resolve(res)).catch(e => resolve(this.sampleQuestions))
      })
    })
  }

  public getMWResponse(body: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this._SecureStorageService.getValue(SecureStorageKey.language, false).then((language) => {
         this._ApiProviderService.postQuarantineAnswers((body ?? this.sampleUserResponse), language).then(res => resolve(res)).catch(e => resolve(this.sampleMWResponse))
      })
    })
  }

  public getCountryStateStatus(country: string, state: string): Promise<any> {
   return new Promise((resolve, reject) => {
     this._ApiProviderService.isCountryStateSupportedForQuarantine(country, state).then(res => resolve(res)).catch(e => resolve((country == 'DEU') && (state == 'HE')))
   })
 }

}
