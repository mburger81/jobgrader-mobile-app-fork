import { Injectable } from '@angular/core';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { FingerprintAIO, FingerprintOptions } from '@awesome-cordova-plugins/fingerprint-aio/ngx';
import { from, combineLatest, forkJoin, Observable } from 'rxjs';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { map, switchMap, take } from 'rxjs/operators';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { Platform } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class BiometricService {


    constructor(
        private secureStorageService: SecureStorageService,
        private faio: FingerprintAIO,
        private translateService: TranslateProviderService,
        private platform: Platform
    ) {
    }

    /** Checks whether or not fingerprint/face recognition is possible */
    public biometricAvailable$(): Observable<boolean> {
        return forkJoin(
            from(this.secureStorageService.getValue(SecureStorageKey.userName, false)),
            from(this.secureStorageService.getValue(SecureStorageKey.basicAuthToken, false)),
            from(this.faio.isAvailable()),
            from(this.secureStorageService.getValue(SecureStorageKey.faioEnabled, false))
        ).pipe(
            map(([
                     username,
                     password,
                     isAvailable,
                     turnedOn]) => {
                return !!username && !!password && !!isAvailable && turnedOn === 'true';
            }),
            take(1)
        );
    }

    public biometricShow(): Observable<string> {
        const options: FingerprintOptions = {
            title: this.translateService.instant('BIOMETRIC.title'),
            description: this.translateService.instant('BIOMETRIC.description_' + (this.platform.is('ios') ? 'ios' : 'android')),
            fallbackButtonTitle: this.translateService.instant('BIOMETRIC.fallbackButtonTitle'),
            cancelButtonTitle: this.translateService.instant('GENERAL.cancel')
        };
        console.log('biometricService show biometric');
        return from(this.faio.show(options)).pipe(
            switchMap(_ => this.secureStorageService.getValueAsync$(SecureStorageKey.basicAuthToken))
        );
    }
}
