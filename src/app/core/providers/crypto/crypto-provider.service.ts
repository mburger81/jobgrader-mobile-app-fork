import { Injectable, OnDestroy } from "@angular/core";
import { ec } from "elliptic"; // Needs to stay here
import { SecureStorageService } from "../secure-storage/secure-storage.service";
import { SecureStorageKey } from "../secure-storage/secure-storage-key.enum";
import { distinctUntilChanged, filter, map, Subject } from "rxjs";
import { ethers } from "ethers";
import { AppStateService } from "../app-state/app-state.service";
import CryptoJS from "crypto-js";
import {
  ApiProviderService,
} from "../api/api-provider.service";
import { TranslateProviderService } from "../translate/translate-provider.service";
import { Hex2iResult } from "../chat/chat-model";
// import Web3 from "web3";
import { VaultService } from "../vault/vault.service";
import * as SentryAngular from "@sentry/angular-ivy";

const eccrypto = require("@toruslabs/eccrypto");
const crypto = require("crypto");
const bip = require("bip39");

@Injectable({
  providedIn: "root",
})
export class CryptoProviderService implements OnDestroy {
  private static context = "Ed25519VerificationKey2018";
  private ec = ec;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private secureStorageService: SecureStorageService,
    private appStateService: AppStateService,
    private translate: TranslateProviderService,
    private _Vault: VaultService,
    private apiService: ApiProviderService
  ) {}

  public async init() {
    this.appStateService
      .asObservable()
      .pipe(
        map((state) => {
          return state.isAuthorized;
        }),
        distinctUntilChanged(),
        filter((isAuthorized) => isAuthorized === false)
      )
      .subscribe(this.encryptData.bind(this));
    this.appStateService
      .asObservable()
      .pipe(
        map((state) => {
          return state.isAuthorized;
        }),
        distinctUntilChanged(),
        filter((isAuthorized) => isAuthorized === true)
      )
      .subscribe(this.decryptData.bind(this));
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    this.destroy$.next(void 0);
  }

  checkIfArrayContainsBoolean(array: Array<any>) {
    for (var i = 0; i < array.length; i++) {
      if (typeof array[i] == "boolean") {
        return false;
      }
    }
  }

  /**
   * getUserKey() handles the process of the checking if there's a need to fetch the user's encryptedSymmetricKey
   * and proceeds with fetching it if required
   * Move that to cryptoservice
   */

  public async getUserSymmetricKeyIfNeeded(keyName?: string): Promise<void> {
    // if (!window.cordova) {
    //     return;
    // }
    return new Promise((resolve, reject) => {
      this.secureStorageService
        .getValue(SecureStorageKey.deviceInfo, false)
        .then((deviceId) => {
          this.secureStorageService
            .getValue(SecureStorageKey.userData, false)
            .then(async (userData) => {
              if (!deviceId) {
                // console.log(userData)
                if (userData) {
                  var deviceList = await this.apiService.getUserDevices(
                    this.appStateService.basicAuthToken,
                    JSON.parse(userData).userid
                  );
                  if (deviceList.length) {
                    deviceId = deviceList[0].deviceId;
                    await this.secureStorageService.setValue(
                      SecureStorageKey.deviceInfo,
                      deviceId
                    );
                  }
                }
              }
              // console.log(deviceId)
              // console.log("keyName: " + keyName);
              var userKeyEncrypted: any = "";
              if (!!keyName) {
                switch (keyName) {
                  case "vcKey":
                    userKeyEncrypted = await this.secureStorageService.getValue(
                      SecureStorageKey.userKeyEncryptedVC,
                      false
                    );
                    break;
                  case "backupKey":
                    userKeyEncrypted = await this.secureStorageService.getValue(
                      SecureStorageKey.userKeyEncryptedBackup,
                      false
                    );
                    break;
                  case "userKey":
                    userKeyEncrypted = await this.secureStorageService.getValue(
                      SecureStorageKey.userKeyEncryptedUser,
                      false
                    );
                    break;
                }
              } else {
                userKeyEncrypted =
                  this.checkIfArrayContainsBoolean([
                    await this.secureStorageService.getValue(
                      SecureStorageKey.userKeyEncryptedVC,
                      false
                    ),
                    await this.secureStorageService.getValue(
                      SecureStorageKey.userKeyEncryptedBackup,
                      false
                    ),
                    await this.secureStorageService.getValue(
                      SecureStorageKey.userKeyEncryptedUser,
                      false
                    ),
                  ]) == false
                    ? false
                    : "";
              }
              if (!!userKeyEncrypted) {
                // console.log(`${keyName} ${userKeyEncrypted} already exists`);
                resolve();
              }
              var res = await this.apiService.getDeviceRegistrationStatus(
                deviceId,
                null,
                null,
                null,
                keyName
              );
              // console.log(res);
              if (res) {
                if (!!res.vcKey) {
                  await this.secureStorageService.setValue(
                    SecureStorageKey.userKeyEncryptedVC,
                    res.vcKey
                  );
                }
                if (!!res.backupKey) {
                  await this.secureStorageService.setValue(
                    SecureStorageKey.userKeyEncryptedBackup,
                    res.backupKey
                  );
                }
                if (!!res.userKey) {
                  await this.secureStorageService.setValue(
                    SecureStorageKey.userKeyEncryptedUser,
                    res.userKey
                  );
                }
                if (!!res.keys) {
                  if (!!res.keys.vcKey) {
                    await this.secureStorageService.setValue(
                      SecureStorageKey.userKeyEncryptedVC,
                      res.keys.vcKey
                    );
                  }
                  if (!!res.keys.backupKey) {
                    await this.secureStorageService.setValue(
                      SecureStorageKey.userKeyEncryptedBackup,
                      res.keys.backupKey
                    );
                  }
                  if (!!res.keys.userKey) {
                    await this.secureStorageService.setValue(
                      SecureStorageKey.userKeyEncryptedUser,
                      res.keys.userKey
                    );
                  }
                } else {
                  console.error("Empty userKey object");
                }
              }
              // console.log('This method has succesffuly executed itself')
              resolve();
            });
        });
    });
  }

  /** Method to generate device key pair */
  public async generateDeviceEncryptionKeyPair() {
    const devicePrivateKey: any = eccrypto.generatePrivate();
    const devicePublicKey: any = eccrypto.getPublic(devicePrivateKey);
    const devicePrivateKeyEncryption =
      this.uInt8ArrayToBase64(devicePrivateKey);
    const devicePublicKeyEncryption = this.uInt8ArrayToBase64(devicePublicKey);
    //
    try {
      await this.secureStorageService.setValue(
        SecureStorageKey.devicePrivateKeyEncryption,
        devicePrivateKeyEncryption
      );
      await this.secureStorageService.setValue(
        SecureStorageKey.devicePublicKeyEncryption,
        devicePublicKeyEncryption
      );
    } catch (e) {
      throw new Error("There was an error generating the device key pair");
    }
    // console.log(this.secureStorageService.hexi2(devicePrivateKeyEncryption));
  }

  // async connectToNetwork(wallet: Wallet) {
  //   const polygon = true
  //     ? CryptoNetworks.POLYGON
  //     : CryptoNetworks.POLYGON_TEST_MUMBAI;
  //   const evan = environment.production
  //     ? CryptoNetworks.EVAN
  //     : CryptoNetworks.EVAN_TEST;
  //   const ether = environment.production
  //     ? EthereumNetworks.MAINNET
  //     : EthereumNetworks.RINKEBY;

  //   const net1 = new ethers.providers.EtherscanProvider(
  //     ether,
  //     await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY)
  //   );
  //   const net2 = new ethers.providers.Web3Provider(evan as any);
  //   const net3 = new ethers.providers.Web3Provider(polygon as any);

  //   const wa1 = wallet.connect(net1);
  //   const wa2 = wallet.connect(net2);
  //   const wa3 = wallet.connect(net3);

  //   var ob = {};

  //   try {
  //     var network1 = await net1.getNetwork();
  //     var bal1 = await wa1.getBalance();

  //     ob = Object.assign(ob, {
  //       [CryptoCurrency.ETH]: (
  //         Math.round(
  //           Number(ethers.utils.formatEther(ethers.BigNumber.from(bal1._hex))) *
  //             10000000
  //         ) / 10000000
  //       ).toString(),
  //     });
  //   } catch (e) {
  //     console.log(e);
  //   }

  //   try {
  //     var network2 = await net2.getNetwork();
  //     var bal2 = await wa2.getBalance();

  //     ob = Object.assign(ob, {
  //       [CryptoCurrency.EVE]: (
  //         Math.round(
  //           Number(ethers.utils.formatEther(ethers.BigNumber.from(bal2._hex))) *
  //             10000000
  //         ) / 10000000
  //       ).toString(),
  //     });
  //   } catch (e) {
  //     console.log(e);
  //   }

  //   try {
  //     var network3 = await net3.getNetwork();
  //     var bal3 = await wa3.getBalance();

  //     ob = Object.assign(ob, {
  //       [CryptoCurrency.MATIC]: (
  //         Math.round(
  //           Number(ethers.utils.formatEther(ethers.BigNumber.from(bal3._hex))) *
  //             10000000
  //         ) / 10000000
  //       ).toString(),
  //     });
  //   } catch (e) {
  //     console.log(e);
  //   }

  //   var p = await this.secureStorageService.getValue(
  //     SecureStorageKey.cryptoBalance,
  //     false
  //   );
  //   var pp = !!p ? JSON.parse(p) : {};
  //   pp[wallet.address] = ob;

  //   await this.secureStorageService.setValue(
  //     SecureStorageKey.cryptoBalance,
  //     JSON.stringify(pp)
  //   );

  //   console.log("Wallet connection is successful");
  // }

  /** Generate the device key pair */
  public async generateDeviceKeyPair() {
    const mnemonic = bip.generateMnemonic(256);

    // const wallet = ethers.Wallet.fromMnemonic(mnemonic);
    // const wallet = new Web3(environment.polygonNetwork).eth.accounts.create("256");
    // console.log(wallet);

    // const devicePublicKeyDelegate = await wallet.getAddress();
    // const mnemonic = wallet.mnemonic;
    // const devicePrivateKeyDelegate = wallet.privateKey;
    try {
      // await this.secureStorageService.setValue(SecureStorageKey.devicePublicKeyDelegate, devicePublicKeyDelegate);
      // await this.secureStorageService.setValue(SecureStorageKey.devicePrivateKeyDelegate, devicePrivateKeyDelegate);
      // await this.secureStorageService.setValue(SecureStorageKey.deviceMnemonic, mnemonic);
    } catch (e) {
      throw new Error("There was an error storing the device key pair");
    }
  }

  public async generateUserKeyPair() {
    const userMnemonic = bip.generateMnemonic(256);
    const wallet = ethers.Wallet.fromMnemonic(userMnemonic);

    const userPublicKey = wallet.address;
    const userPrivateKey = wallet.privateKey;
    try {
      await this.secureStorageService.setValue(
        SecureStorageKey.userPublicKey,
        userPublicKey
      );
      await this.secureStorageService.setValue(
        SecureStorageKey.userPrivateKey,
        userPrivateKey
      );
      await this.secureStorageService.setValue(
        SecureStorageKey.userMnemonic,
        userMnemonic
      );
    } catch (e) {
      throw new Error("There was an error storing the device key pair");
    }

    // try {
    //     await this.connectToNetwork(wallet);
    // } catch(ee) {
    //     console.log(ee);
    // }
  }

  // public async generateWeb3Wallet() {
  //   const userMnemonic = bip.generateMnemonic();

  //   const wallet = ethers.Wallet.fromMnemonic(userMnemonic);
  //   // const wallet = ethers.utils.HDNode.fromMnemonic(userMnemonic);

  //   const userPublicKey = wallet.address;
  //   const userPrivateKey = wallet.privateKey;
  //   try {
  //     await this.secureStorageService.setValue(
  //       SecureStorageKey.web3WalletPublicKey,
  //       userPublicKey
  //     );
  //     await this.secureStorageService.setValue(
  //       SecureStorageKey.web3WalletPrivateKey,
  //       userPrivateKey
  //     );
  //     await this.secureStorageService.setValue(
  //       SecureStorageKey.web3WalletMnemonic,
  //       userMnemonic
  //     );
  //   } catch (e) {
  //     throw new Error("There was an error storing the device key pair");
  //   }

  //   try {
  //     await this.connectToNetwork(wallet);
  //   } catch (ee) {
  //     console.log(ee);
  //   }
  // }

  /** Generates nonce */
  public generateNonce(len: number): string {
    return this.makeWord(len);
  }

  /** Generates hash from word and nonce */
  public generateHash(word: string, nonce: string): string {
    const hash = CryptoJS.MD5(`${word}${nonce}`).toString();
    return hash;
  }

  /** Encrypts a password with the helix public key */
  public async encryptPassword(password: string): Promise<Buffer> {
    console.log("******Testing Android 4********");
    var helixPublicKey = await this.secureStorageService.getValue(
      SecureStorageKey.helixPublicKey,
      false
    );
    var base64regex =
      /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
    if (!helixPublicKey) {
      helixPublicKey = await this.apiService.getHelixPublicKeyPromise();
      await this.secureStorageService.setValue(
        SecureStorageKey.helixPublicKey,
        helixPublicKey
      );
    } else if (base64regex.test(helixPublicKey)) {
      helixPublicKey = await this.apiService.getHelixPublicKeyPromise();
      await this.secureStorageService.setValue(
        SecureStorageKey.helixPublicKey,
        helixPublicKey
      );
    }
    console.log("******Testing Android 5********");
    console.log("helixPublicKey: ", helixPublicKey);
    if (helixPublicKey) {
      const publicKey = Buffer.from(helixPublicKey, "hex");
      console.log("******Testing Android 5.1********");
      try {
        var encryptedPassword = await eccrypto.encrypt(
          publicKey,
          Buffer.from(password)
        );
      } catch (e) {
        console.log(e);
      }
      console.log("******Testing Android 5.2********");
      const encryptedPasswordUnion = Buffer.concat([
        encryptedPassword.iv,
        encryptedPassword.ephemPublicKey,
        encryptedPassword.ciphertext,
        encryptedPassword.mac,
      ]);
      console.log("******Testing Android 5.3********");
      return encryptedPasswordUnion;
    } else {
      return Promise.reject();
    }
  }

  /** Encrypts a password with helix public key and encodes the byte array as base64 */
  public async encryptPasswordAsBase64(password: string): Promise<string> {
    const encryptPasswordUnion = await this.encryptPassword(password);
    return this.uInt8ArrayToBase64(encryptPasswordUnion);
  }

  /** Generates the basic auth token as encrypted base64 string */
  public async generateBasicAuthToken(
    username: string,
    password: string
  ): Promise<string> {
    const encryptedPasswordBase64 = await this.encryptPasswordAsBase64(
      password
    );
    const word = `${username}:${encryptedPasswordBase64}`;
    const authToken = window.btoa(word);
    return authToken;
  }

  public uInt8ArrayToBase64(bytes: Uint8Array): string {
    let binary = "";
    const len = bytes.length;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  /***************Private************/

  public bufferToBase64(buf) {
    const binstr = Array.prototype.map
      .call(buf, (ch) => {
        return String.fromCharCode(ch);
      })
      .join("");
    return btoa(binstr);
  }

  public base64ToUint8Array(base64: string): Uint8Array {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes;
  }

  public toBuffer(ab): Buffer {
    const buf = Buffer.alloc(ab.byteLength);
    const view = new Uint8Array(ab);
    for (let i = 0; i < buf.length; ++i) {
      buf[i] = view[i];
    }
    return buf;
  }

  public i2hex(i) {
    return ("0" + i.toString(16)).slice(-2);
  }

  public hex2i(hexString): Hex2iResult {
    var obj = <Hex2iResult>{
      error: false,
      result: null,
    };

    try {
      obj.result = new Uint8Array(
        hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16))
      );
    } catch (e) {
      obj.error = "hex2i err: " + JSON.stringify(e);
    }

    return obj;
  }

  hex2string(hexString) {
    return decodeURIComponent(
      hexString.replace("0x", "").replace(/../g, "%$&")
    );
  }

  private stringToUint(s: string): Uint8Array {
    s = btoa(unescape(encodeURIComponent(s)));
    const charList = s.split("");
    const uintArray = [];
    for (let i = 0; i < charList.length; i++) {
      uintArray.push(charList[i].charCodeAt(0));
    }
    return new Uint8Array(uintArray);
  }

  private uintToString(uintArray: Uint8Array) {
    const encodedString = String.fromCharCode.apply(null, uintArray);
    const decodedString = decodeURIComponent(escape(atob(encodedString)));
    return decodedString;
  }

  private regenerateEcies(el) {
    if (el.ephemPublicKey) {
      return {
        ciphertext: this.toBuffer(new Uint8Array(el.ciphertext.data).buffer),
        ephemPublicKey: this.toBuffer(
          new Uint8Array(el.ephemPublicKey.data).buffer
        ),
        iv: this.toBuffer(new Uint8Array(el.iv.data).buffer),
        mac: this.toBuffer(new Uint8Array(el.mac.data).buffer),
      };
    } else {
      return undefined;
    }
  }

  private async encryptData(): Promise<void> {
    const userData: string = await this.secureStorageService.getValue(
      SecureStorageKey.userData,
      false
    );
    // const didDocument: string = await this.secureStorageService.getValue(SecureStorageKey.didDocument, false);
    // const verifiableCredentialEncrypted: string = await this.secureStorageService.getValue(SecureStorageKey.verifiableCredentialEncrypted, false);
    const devicePublicKeyEncryption: any =
      await this.secureStorageService.getValue(
        SecureStorageKey.devicePublicKeyEncryption,
        false
      );
    const devicePrivateKeyEncryption: any =
      await this.secureStorageService.getValue(
        SecureStorageKey.devicePrivateKeyEncryption,
        false
      );
    if (userData) {
      if (devicePublicKeyEncryption) {
        const privateKey = this.base64ToUint8Array(devicePrivateKeyEncryption);
        const publicKey = this.base64ToUint8Array(devicePublicKeyEncryption);
        const ecies = await eccrypto.encrypt(
          this.toBuffer(publicKey.buffer),
          Buffer.from(userData),
          { ephemPrivateKey: this.toBuffer(privateKey.buffer) }
        );
        await this.secureStorageService.setValue(
          SecureStorageKey.userData,
          JSON.stringify(ecies)
        );
      }
    }
  }

  private async decryptData(): Promise<void> {
    const encryptedUserData = await this.secureStorageService.getValue(
      SecureStorageKey.userData,
      false
    );
    // const encryptedDidDocument = await this.secureStorageService.getValue(SecureStorageKey.didDocument, false);
    // const encryptedVCDocument = await this.secureStorageService.getValue(SecureStorageKey.verifiableCredentialEncrypted, false);
    const devicePrivateKeyEncryption: any =
      await this.secureStorageService.getValue(
        SecureStorageKey.devicePrivateKeyEncryption,
        false
      );
    if (encryptedUserData) {
      const ecies = this.regenerateEcies(JSON.parse(encryptedUserData));
      if (ecies && ecies.iv) {
        if (devicePrivateKeyEncryption) {
          const privateKey = this.base64ToUint8Array(
            devicePrivateKeyEncryption
          );
          const userData = await eccrypto.decrypt(
            this.toBuffer(privateKey.buffer),
            ecies
          );
          await this.secureStorageService.setValue(
            SecureStorageKey.userData,
            new TextDecoder().decode(userData)
          );
        }
      }
    }
  }

  async encryptGenericData(genericData: string) {
    // const userData: string = await this.secureStorageService.getValue(SecureStorageKey.userData, false);
    if (genericData) {
      const devicePublicKeyEncryption: any =
        await this.secureStorageService.getValue(
          SecureStorageKey.devicePublicKeyEncryption,
          false
        );
      if (devicePublicKeyEncryption) {
        const devicePrivateKeyEncryption: any =
          await this.secureStorageService.getValue(
            SecureStorageKey.devicePrivateKeyEncryption,
            false
          );
        const privateKey = this.base64ToUint8Array(devicePrivateKeyEncryption);
        const publicKey = this.base64ToUint8Array(devicePublicKeyEncryption);
        const ecies = await eccrypto.encrypt(
          this.toBuffer(publicKey.buffer),
          Buffer.from(genericData),
          { ephemPrivateKey: this.toBuffer(privateKey.buffer) }
        );
        return JSON.stringify(ecies);
      }
    }
    return genericData;
  }

  async decryptGenericData(genericData: string) {
    // const eciesString = await this.secureStorageService.getValue(SecureStorageKey.userData, false);
    if (genericData) {
      const ecies = this.regenerateEcies(JSON.parse(genericData));
      if (ecies && ecies.iv) {
        const devicePrivateKeyEncryption: any =
          await this.secureStorageService.getValue(
            SecureStorageKey.devicePrivateKeyEncryption,
            false
          );
        if (devicePrivateKeyEncryption) {
          const privateKey = this.base64ToUint8Array(
            devicePrivateKeyEncryption
          );
          const decryptedData = await eccrypto.decrypt(
            this.toBuffer(privateKey.buffer),
            ecies
          );
          // await this.secureStorageService.setValue(SecureStorageKey.userData, new TextDecoder().decode(userData));
          return new TextDecoder().decode(decryptedData);
        }
      }
    }
    return genericData;
  }

  encryptGenericDataWithThirdPartyPublicKey(
    genericData: string,
    pubKey: string
  ): Promise<string> {
    return new Promise((resolve) => {
      const publicKey = Buffer.from(
        this.base64ToHex(this.base64urlTobase64(pubKey)),
        "hex"
      );
      // console.log("pubKey"); console.log(pubKey);
      // console.log("publicKey"); console.log(publicKey);
      const dataBuffer = Buffer.from(genericData);
      // console.log("genericData"); console.log(genericData);
      // console.log("dataBuffer"); console.log(dataBuffer);
      eccrypto
        .encrypt(publicKey, dataBuffer)
        .then((ecies) => {
          resolve(
            this.uInt8ArrayToBase64(
              Buffer.concat([
                ecies.iv,
                ecies.ephemPublicKey,
                ecies.ciphertext,
                ecies.mac,
              ])
            )
          );
        })
        .catch((e) => {
          console.log(e);
          resolve(genericData);
        });
    });
  }

  async decryptDataSharingData(genericData: string) {
    if (genericData) {
      const devicePrivateKeyEncryption: any =
        await this.secureStorageService.getValue(
          SecureStorageKey.devicePrivateKeyEncryption,
          false
        );
      if (devicePrivateKeyEncryption) {
        var base64 = this.base64urlTobase64(genericData);
        var encryptedByte = this.base64ToUint8Array(base64);
        var arrayByteObject = {
          iv: encryptedByte.slice(0, 16),
          v: encryptedByte.slice(16, 16 + 65),
          cipherText: encryptedByte.slice(16 + 65, encryptedByte.length - 32),
          t: encryptedByte.slice(encryptedByte.length - 32),
        };
        var hexByteObjectEncrypted = {
          iv: Buffer.from(
            this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.iv)),
            "hex"
          ),
          ephemPublicKey: Buffer.from(
            this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.v)),
            "hex"
          ),
          ciphertext: Buffer.from(
            this.base64ToHex(
              this.uInt8ArrayToBase64(arrayByteObject.cipherText)
            ),
            "hex"
          ),
          mac: Buffer.from(
            this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.t)),
            "hex"
          ),
        };
        var privKey = Buffer.from(
          this.base64ToHex(devicePrivateKeyEncryption),
          "hex"
        );
        const decryptedData = await eccrypto.decrypt(
          privKey,
          hexByteObjectEncrypted
        );
        return new TextDecoder().decode(decryptedData);
      }
    }
    return genericData;
  }

  async decryptAssymmetric(genericData: string, privateKey: string) {
    var base64 = this.base64urlTobase64(genericData);
    var encryptedByte = this.base64ToUint8Array(base64);
    var arrayByteObject = {
      iv: encryptedByte.slice(0, 16),
      v: encryptedByte.slice(16, 16 + 65),
      cipherText: encryptedByte.slice(16 + 65, encryptedByte.length - 32),
      t: encryptedByte.slice(encryptedByte.length - 32),
    };
    var hexByteObjectEncrypted = {
      iv: Buffer.from(
        this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.iv)),
        "hex"
      ),
      ephemPublicKey: Buffer.from(
        this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.v)),
        "hex"
      ),
      ciphertext: Buffer.from(
        this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.cipherText)),
        "hex"
      ),
      mac: Buffer.from(
        this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.t)),
        "hex"
      ),
    };
    var privKey = Buffer.from(this.base64ToHex(privateKey), "hex");
    const decryptedData = await eccrypto.decrypt(
      privKey,
      hexByteObjectEncrypted
    );
    return new TextDecoder().decode(decryptedData);
  }

  async symmetricEncrypt(text?: any, symmetricKey?: any): Promise<string> {
    return new Promise((resolve, reject) => {
      if (!symmetricKey || symmetricKey === "") {
        console.error("Empty userSymmetricKey");
        reject();
      }
      try {
        var key = Buffer.from(symmetricKey, "hex");
        // var iv = crypto.randomBytes(16);
        var iv = crypto.randomBytes(12);
        // var cipher = crypto.createCipheriv('aes256', key, iv);
        var cipher = crypto.createCipheriv("aes-256-gcm", key, iv);
        var ciphered = cipher.update(text, "utf8");
        ciphered = Buffer.concat([ciphered, cipher.final()]);
        var authTag = cipher.getAuthTag();
        var ivHex = iv.toString("hex");
        var encryptedHex = ciphered.toString("hex");
        var authTagHex = authTag.toString("hex");
        var chiperMsgHex = "0c" + ivHex + encryptedHex + authTagHex;
        var chiperMsgHextoBase64 = Buffer.from(chiperMsgHex, "hex").toString(
          "base64"
        );
        var chiperMsgHextoBase64URL =
          this.base64Tobase64url(chiperMsgHextoBase64);
        // console.log("chatPrivateKey: "+ text);
        // console.log("chiperMsgHextoBase64URL: "+ chiperMsgHextoBase64URL);
        resolve(chiperMsgHextoBase64URL);
      } catch (e) {
        console.log(e);
        reject();
      }
    });
  }

  async beginDecryptionProcessToObtainSymmetricKey(
    base64url: string,
    mode: string
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      this.secureStorageService
        .getValue(SecureStorageKey.devicePrivateKeyEncryption, false)
        .then((devicePrivateKeyEncryption) => {
          if (!base64url) {
            console.log(this.translate.instant("SETTINGSACCOUNT.case1"));
            reject(this.translate.instant("SETTINGSACCOUNT.case1"));
          }
          if (!devicePrivateKeyEncryption) {
            console.log(this.translate.instant("SETTINGSACCOUNT.case3"));
            reject(this.translate.instant("SETTINGSACCOUNT.case3"));
          }
          // console.log(mode)
          // console.log("devicePrivateKeyEncryption: " + devicePrivateKeyEncryption);
          // console.log("devicePrivateKeyEncryption Hex: " + this.base64ToHex(devicePrivateKeyEncryption));
          // console.log("base64url: " + base64url);
          var base64 = this.base64urlTobase64(base64url);
          // console.log("base64: " + base64);
          var encryptedByte = this.base64ToUint8Array(base64);
          // console.log(encryptedByte);
          // console.log(JSON.stringify(encryptedByte));
          var arrayByteObject = {
            iv: encryptedByte.slice(0, 16),
            v: encryptedByte.slice(16, 16 + 65),
            cipherText: encryptedByte.slice(16 + 65, encryptedByte.length - 32),
            t: encryptedByte.slice(encryptedByte.length - 32),
          };
          // console.log(arrayByteObject);
          // console.log(JSON.stringify(arrayByteObject));
          var hexByteObjectEncrypted = {
            iv: Buffer.from(
              this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.iv)),
              "hex"
            ),
            ephemPublicKey: Buffer.from(
              this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.v)),
              "hex"
            ),
            ciphertext: Buffer.from(
              this.base64ToHex(
                this.uInt8ArrayToBase64(arrayByteObject.cipherText)
              ),
              "hex"
            ),
            mac: Buffer.from(
              this.base64ToHex(this.uInt8ArrayToBase64(arrayByteObject.t)),
              "hex"
            ),
          };
          // console.log(hexByteObjectEncrypted);
          // console.log(JSON.stringify(hexByteObjectEncrypted));
          var privKey = Buffer.from(
            this.base64ToHex(devicePrivateKeyEncryption),
            "hex"
          );
          // console.log("privKey: " + privKey);
          eccrypto
            .decrypt(privKey, hexByteObjectEncrypted)
            .then((plaintext) => {
              var plainBuffer = Buffer.from(plaintext);
              var symmetricKey = plainBuffer.toString("hex");
              // console.log(`${mode}: ${symmetricKey}`);
              resolve(symmetricKey);
            })
            .catch(async (e) => {
              console.log(e);
              // console.log('Unable to decrypt');
              await this.sendBadMacLog(mode, e);
              reject();
            });
        });
    });
  }

  public async sendBadMacLog(mode: string, e: any) {
    // var feedback: RemoteLog = {
    //   subject: await this.secureStorageService.getValue(
    //     SecureStorageKey.userName,
    //     false
    //   ),
    //   message: JSON.stringify(e),
    //   category: `BAD_MAC_ERROR : ${mode} : ${window.location.href}`,
    //   level: RemoteLogLevel.error,
    //   deviceId: await this.symmetricEncrypt(
    //     await this.secureStorageService.getValue(
    //       SecureStorageKey.deviceInfo,
    //       false
    //     ),
    //     UDIDNonce.helix
    //   ),
    // };

    // mburger TODO: we need maybe send the error to sentry?
    // check with ansik if how this error is happening and what we need to do
    // await this.apiService.sendRemoteErrorLog(feedback);

    try {
      SentryAngular.captureException(e.originalError || e);
    } catch (error) {
      console.error('CryptoProviderService#sendBadMacLog; error', error);
    }
  }

  public async returnVCSymmetricKey() {
    await this.getUserSymmetricKeyIfNeeded("vcKey");
    var base64url = await this.secureStorageService.getValue(
      SecureStorageKey.userKeyEncryptedVC,
      false
    );
    return await this.beginDecryptionProcessToObtainSymmetricKey(
      base64url,
      "returnVCSymmetricKey"
    );
  }

  public async returnBackupSymmetricKey() {
    await this.getUserSymmetricKeyIfNeeded("backupKey");
    var base64url = await this.secureStorageService.getValue(
      SecureStorageKey.userKeyEncryptedBackup,
      false
    );
    return await this.beginDecryptionProcessToObtainSymmetricKey(
      base64url,
      "returnBackupSymmetricKey"
    );
  }

  public async returnUserSymmetricKey() {
    await this.getUserSymmetricKeyIfNeeded("userKey");
    var base64url = await this.secureStorageService.getValue(
      SecureStorageKey.userKeyEncryptedUser,
      false
    );
    return await this.beginDecryptionProcessToObtainSymmetricKey(
      base64url,
      "returnUserSymmetricKey"
    );
  }

  async symmetricDecrypt(text: string, symmetricKey: string) {
    var hexwith0C = this.base64ToHex(this.base64urlTobase64(text));
    var byteArrayResult = this.hex2i(hexwith0C.slice(2));

    if (byteArrayResult.error) {
      console.log(byteArrayResult.error);
    }

    var byteArrayObject = {
      iv: byteArrayResult.result.slice(0, 12),
      ciphertext: byteArrayResult.result.slice(
        12,
        byteArrayResult.result.length - 16
      ),
      authTag: byteArrayResult.result.slice(byteArrayResult.result.length - 16),
    };
    var byteArrayObjectHex = {
      iv: Buffer.from(
        this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.iv)),
        "hex"
      ),
      ciphertext: Buffer.from(
        this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.ciphertext)),
        "hex"
      ),
      authTag: Buffer.from(
        this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.authTag)),
        "hex"
      ),
    };
    var key = Buffer.from(symmetricKey, "hex");
    var decipher = crypto.createDecipheriv(
      "aes-256-gcm",
      key,
      byteArrayObjectHex.iv
    );
    decipher.setAuthTag(byteArrayObjectHex.authTag);
    let decrypted = decipher.update(
      byteArrayObjectHex.ciphertext,
      "hex",
      "utf8"
    );
    decrypted += decipher.final();
    return decrypted;
  }

  async decryptVerifiableCredential(vcEncrypted?: string) {
    const index = 0;

    const verifiableCredentialEncryptedStringified =
      await this.secureStorageService.getValue(
        SecureStorageKey.verifiableCredentialEncrypted,
        false
      );

    if (
      !verifiableCredentialEncryptedStringified ||
      verifiableCredentialEncryptedStringified === "" ||
      verifiableCredentialEncryptedStringified === "[]" ||
      !Array.isArray(JSON.parse(verifiableCredentialEncryptedStringified))
    ) {
      return this.translate.instant("SETTINGSACCOUNT.case2");
    }

    const verifiableCredentialEncryptedArray = JSON.parse(
      verifiableCredentialEncryptedStringified
    );
    const verifiableCredentialEncrypted = vcEncrypted
      ? vcEncrypted
      : verifiableCredentialEncryptedArray[index] || "";

    const symmetricKey = await this.returnVCSymmetricKey();

    const returnObject = {
      symmetricKey,
      vc: await this.symmetricDecrypt(
        verifiableCredentialEncrypted,
        symmetricKey
      ),
    };
    return returnObject;
  }

  public base64Tobase64url(input) {
    if (input) {
      input = input.replace(/\+/g, "-").replace(/\//g, "_");
    }
    return input;
  }

  public base64urlTobase64(input) {
    if (input) {
      input = input.replace(/-/g, "+").replace(/_/g, "/");

      // Pad out with standard base64 required padding characters
      const pad = input.length % 4;
      if (pad) {
        if (pad === 1) {
          throw new Error(
            "InvalidLengthError: Input base64url string is the wrong length to determine padding"
          );
        }
        input += new Array(5 - pad).join("=");
      }
    }
    return input;
  }

  public base64ToHex(str) {
    const raw = atob(str);
    let result = "";
    for (let i = 0; i < raw.length; i++) {
      const hex = raw.charCodeAt(i).toString(16);
      result += hex.length === 2 ? hex : "0" + hex;
    }
    return result.toUpperCase();
  }

  private makeWord(length: number): string {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  public async signMessage(message: string) {
    var key = await this.secureStorageService.getValue(
      SecureStorageKey.userMnemonic,
      false
    );
    // if(!key) {
    //     key = await this.secureStorageService.getValue(SecureStorageKey.deviceMnemonic, false);
    // }
    var wallet = await ethers.Wallet.fromMnemonic(key);
    var signedMessage = wallet.signMessage(message);
    return signedMessage;
  }
}
