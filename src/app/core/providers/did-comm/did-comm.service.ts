import { Injectable } from "@angular/core";
import { ec } from "elliptic"; // Needs to stay here
import { SecureStorageService } from "../secure-storage/secure-storage.service";
import { SecureStorageKey } from "../secure-storage/secure-storage-key.enum";
import { DIDComm } from "@hearro/didcomm";


@Injectable({
  providedIn: 'root'
})
export class DidCommService {
  private ec = ec;
  private packUnpack: DIDComm;

  constructor(
    private secureStorageService: SecureStorageService,
  ) {
    this.init();
  }

  public async init() {
    this.packUnpack = new DIDComm();
    await (this.packUnpack as any).ready;
  }

  /** Method to generate user specific chat key pair */
  public async generateChatKeyPair() {
    const keyPair = await this.packUnpack.generateKeyPair();
    try {
      await this.secureStorageService.setValue(
        SecureStorageKey.chatPrivateKey,
        Array.from(keyPair.privateKey).map(this.i2hex).join("")
      );
      await this.secureStorageService.setValue(
        SecureStorageKey.chatPublicKey,
        Array.from(keyPair.publicKey).map(this.i2hex).join("")
      );
    } catch (e) {
      throw new Error("There was an error generating the chat key pair");
    }
  }

  /** Method to generate thread specific chat key pair */
  public generateThreadSpecificChatKeyPair(): Promise<{
    publicKey: string;
    privateKey: string;
  }> {
    return new Promise((resolve, reject) => {
      this.packUnpack
        .generateKeyPair()
        .then((keyPair) => {
          resolve({
            publicKey: Array.from(keyPair.publicKey).map(this.i2hex).join(""),
            privateKey: Array.from(keyPair.privateKey).map(this.i2hex).join(""),
          });
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  public i2hex(i) {
    return ("0" + i.toString(16)).slice(-2);
  }

}
