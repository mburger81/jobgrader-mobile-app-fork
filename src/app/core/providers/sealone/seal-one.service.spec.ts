import { TestBed } from '@angular/core/testing';

import { SealOneService } from './seal-one.service';

describe('SealOneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SealOneService = TestBed.get(SealOneService);
    expect(service).toBeTruthy();
  });
});
