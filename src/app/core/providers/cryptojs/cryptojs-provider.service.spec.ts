import { TestBed } from '@angular/core/testing';

import { CryptojsProviderService } from './cryptojs-provider.service';

describe('CryptojsProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CryptojsProviderService = TestBed.get(CryptojsProviderService);
    expect(service).toBeTruthy();
  });
});
