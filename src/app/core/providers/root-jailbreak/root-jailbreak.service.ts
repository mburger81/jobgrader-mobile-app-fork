import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { App } from '@capacitor/app';

declare var navigator: any;
declare var window: any;
declare var cordova: any;
declare var IRoot: any;

@Injectable({
  providedIn: 'root'
})
export class RootJailbreakService {

  constructor(
    private _Platform: Platform,
    private _TranslateProviderService: TranslateProviderService
  ) { }

  init() {
    if(this._Platform.is('hybrid')) {

      // console.log("Jailbreak Root testing");

      IRoot.isRooted((rooted) => {
        // console.log("rooted");
        // console.log(rooted);
        if(rooted) {
          if(this._Platform.is('ios')) {
            alert(this._TranslateProviderService.instant('JAILBREAK.ios'));
          }
          else if(this._Platform.is('android')) {
            alert(this._TranslateProviderService.instant('JAILBREAK.android'));
          }
          App.exitApp();
        } else {
          if(this._Platform.is('ios')) {
            console.log("iOS device is NOT jailbroken");
          }
          else if(this._Platform.is('android')) {
            console.log("Android device is NOT rooted");
          }
        }
      }, (error) => {
        // console.log("error");
        console.log(error);
      });

      // if(this._Platform.is('ios')) {
      //   console.log("Jailbreak testing");
      //   (cordova.plugins as any).jailbreakdetection.isJailbroken(
      //     (jailbroken) => {
      //       console.log("jailbroken");
      //       console.log(jailbroken);
      //       if(jailbroken) {
      //         alert("You are using a jailbroken iOS device");
      //         App.exitApp();
      //       } else {
      //         console.log("iOS device is NOT jailbroken");
      //       }
      //   },
      //     (error) => { console.log("jailbroken error"); console.log(error) });
      // } else if(this._Platform.is('android')) {
      //   console.log("Root-detection testing");
      //   (cordova.plugins as any).diagnostic.isDeviceRooted(
      //     (rooted) => {

      //       console.log("rooted");
      //       console.log(rooted);

      //       if(rooted) {
      //         alert("You are using a rooted Android device");
      //         App.exitApp();
      //       } else {
      //         console.log("Android device is NOT rooted");
      //       }

      //     },
      //     (error) => { console.log("rooted error"); console.log(error) })
      // }
    } else {
      console.log("jailbroken rooted error");
    }
  }

}
