import { Injectable } from '@angular/core';
import * as Identicon from 'identicon.js';
// import { EvmChain } from "@moralisweb3/common-evm-utils";
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { CryptoCurrency } from '../wallet-connect/constants';

const axios = require("axios");
const MORALIS_BASE = "https://deep-index.moralis.io/api/v2";
const Moralis = require("moralis").default;

export interface MoralisMetadataResponse {
  token_address: string;
  token_id: string;
  amount: string;
  owner_of: string;
  token_hash: string;
  block_number_minted: string;
  block_number: string;
  transfer_index: Array<number>;
  contract_type: string;
  name: string;
  symbol: string;
  token_uri: string;
  metadata: string;
  last_token_uri_sync: string;
  last_metadata_sync: string;
}

export interface NFTHistory {
  from: string;
  to: string;
  fromImage: string;
  toImage: string;
  amount: string;
  value: string;
  timestamp: any;
}

export interface CelebrityNft {
  name: string;
  address: string;
  profileImage: string;
}

export interface CelebrityNftDetails {
  name: string;
  displayImage: string;
  network: string;
  descriptionText: string;
  creationDate: number;
  modificationDate: number;
  contractAddress: string;
  tokenId: string;
  rank: number;
  internalInformation: any;
  releaseStatus: boolean;
  visibility: boolean;
}

interface MoralisResponse {
  total: number;
  page: number;
  page_size: number;
  cursor: number;
  result: Array<MoralisResult>;
  status: string;
}

interface MoralisResult {
  token_address: string;
  token_id: string;
  owner_of: string;
  block_number: string;
  block_number_minted: string;
  token_hash: string;
  amount: string;
  contract_type: string;
  name: string;
  symbol: string;
  token_uri: string;
  metadata: string;
  last_token_uri_sync: string;
  last_metadata_sync: string;
}

@Injectable({
  providedIn: 'root'
})
export class MoralisService {

  private supportedChains = [
    { name: "eth"	, chain_hex: 0x1	, chain_int: 1, currency: CryptoCurrency.ETH },
    // { name: "goerli"	, chain_hex: 0x5	, chain_int: 5 },
    { name: "polygon"	, chain_hex: 0x89	, chain_int: 137, currency: CryptoCurrency.MATIC },
    // { name: "mumbai"	, chain_hex: 0x13881	, chain_int: 80001 },
    { name: "bsc"	, chain_hex: 0x38	, chain_int: 56, currency: CryptoCurrency.BNB_SMART  },
    // { name: "bsc testnet"	, chain_hex: 0x61	, chain_int: 97 },
    { name: "avalanche"	, chain_hex: 0xA86A	, chain_int: 43114, currency: CryptoCurrency.AVAX },
    // { name: "avalanche testnet"	, chain_hex: 0xA869	, chain_int: 43113 },
    { name: "fantom"	, chain_hex: 0xFA	, chain_int: 250, currency: CryptoCurrency.FTM },
    { name: "cronos"	, chain_hex: 0x19	, chain_int: 25, currency: CryptoCurrency.CRO },
    // { name: "cronos testnet"	, chain_hex: 0x152	, chain_int: 338 }
  ]

  constructor(
    private _Vault: VaultService
  ) {}

  //  init() {
  //   this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
  //     try {
  //       Moralis.start({
  //         apiKey: MORALIS_API_KEY,
  //       });
  //     } catch(e) {
  //       console.log(e);
  //     }
  //   })
  //  }
  
   async getBalanceMoralis(address: string, currency: string) {
    var ob = this.supportedChains.find(sc => sc.currency == currency);
    const balance = await Moralis.EvmApi.balance.getNativeBalance({ address, chain: ob.chain_hex });
    console.log(currency, balance.jsonResponse.balance);
    return balance.jsonResponse.balance;  
   }


  fetchNfts(address: string): Promise<Array<CelebrityNftDetails>> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
        const options = {
          method: 'GET',
          url: `${MORALIS_BASE}/${address}/nft`,
          params: {chain: 'eth', format: 'decimal'},
          headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
        }
        axios.request(options).then(async response => {
          var r = await this.processResponse(response.data);
          resolve(r);
        }).catch(e => {
          reject(e);
        })
      })
    })
  }

  fetchNftMetadata(contractAddress: string, tokenId: string): Promise<CelebrityNftDetails> {
    
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
        const options = {
          method: 'GET',
          url: `${MORALIS_BASE}/nft/${contractAddress}/${tokenId}`,
          params: {chain: 'eth', format: 'decimal'},
          headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
        }
        axios.request(options).then(response => {
          var r = response.data;
          var rr = this.processMetadataResponse(r);
          resolve(rr);
        }).catch(e => {
          console.log(e);
          resolve(this.processMetadataResponse(null));
        })
      })
    })
  }

  getNFTTransfers(contractAddress: string, tokenId: string): Promise<Array<NFTHistory>> {
    
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
        const options = {
          method: 'GET',
          url: `${MORALIS_BASE}/nft/${contractAddress}/${tokenId}/transfers`,
          params: {chain: 'eth', format: 'decimal'},
          headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
        }
        axios.request(options).then(response => {
          var r = response.data;
          resolve(this.processNFTHistoryResponse(r.result));
        }).catch(e => {
          console.log(e);
        })
      })
    })
  }

  getNFTTokenIdOwners(contractAddress: string, tokenId: string, address: string): Promise<CelebrityNftDetails> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
        const options = {
          method: 'GET',
          url: `${MORALIS_BASE}/nft/${contractAddress}/${tokenId}/owners`,
          params: {chain: 'eth', format: 'decimal'},
          headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
        }
        axios.request(options).then(response => {
          var r = response.data;
          var rr = Array.from(r.result, k => (k as any).owner_of);
          if(rr.includes(address)) {
            resolve(this.processMetadataResponse(r.result[0]));
          } else {
            resolve(this.processMetadataResponse(null));
          }
        }).catch(e => {
          console.log(e);
          this.fetchNftMetadata(contractAddress, tokenId).then(resolution => {
            console.log(resolution);
            resolve(resolution);
          })
        })
      })
    })
  }

  processNFTHistoryResponse(sample: Array<any>): Array<NFTHistory> {
    var res = [];
    for(let i=0; i<sample.length; i++) {
      res.push(<NFTHistory>{
        from: sample[i].from_address,
        fromImage: `data:image/png;base64,${new Identicon(sample[i].from_address, 420).toString()}`,
        to: sample[i].to_address,
        toImage: `data:image/png;base64,${new Identicon(sample[i].to_address, 420).toString()}`,
        amount: sample[i].amount,
        value: sample[i].value,
        timestamp: sample[i].block_timestamp
      })
    }
    return res;
  }

  processMetadataResponse(sample: MoralisMetadataResponse): CelebrityNftDetails {
    if(!sample) {
      return <CelebrityNftDetails>{
        name: null,
        displayImage: null,
        network: null,
        descriptionText: null,
        creationDate: null,
        modificationDate: null,
        contractAddress: null,
        tokenId: null,
        rank: 0,
        internalInformation: {},
        releaseStatus: true,
        visibility: false
      }
    }
    var gg = {
      name: null,
      description: '',
      attributes: null,
      image: null
    };
    try {
      gg = Object.assign(gg, JSON.parse(sample.metadata));
    } catch (e) {
      console.log(e);
    }
    var g = <CelebrityNftDetails> {
      name: !!gg.name ? gg.name : `${sample.name} #${sample.token_id}`,
      displayImage: (gg.image.includes("ipfs://ipfs/") ? gg.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/") : (gg.image.includes("ipfs://") ? gg.image.replace("ipfs://","https://ipfs.io/ipfs/") : gg.image)),
      network: sample.contract_type,
      descriptionText: gg.description,
      contractAddress: sample.token_address,
      tokenId: sample.token_id,
      creationDate: +new Date(sample.last_metadata_sync),
      modificationDate: +new Date(new Date(sample.last_token_uri_sync)),
      rank: 1,
      internalInformation: !!gg.attributes ? { attributes: gg.attributes } : {},
      releaseStatus: true,
      visibility: true
    } 
    return g;
  }

  processResponse(sample: MoralisResponse): Promise<Array<CelebrityNftDetails>> {
    return new Promise((resolve, reject) => {
      var ret = [];
      
      // var tokenUris = Array.from(sample.result, r => r.token_uri);
      // console.log(tokenUris);        

      for(let i = 0; i< sample.result.length; i++) {
        // "token_address":"0xf3fcfef635f2fbfeac1d6f0dc35296666457d422",
        // "token_id":"415",
        // "owner_of":"0x590b504cc56cfd189bc8153ceaf941efce6e98f3",
        // "block_number":"15600442",
        // "block_number_minted":"15600442",
        // "token_hash":"4bd8ff01cff4ce1490874e8361e897c2",
        // "amount":"1",
        // "contract_type":"ERC1155",
        // "name":"Art DragonBall Legend Origin",
        // "symbol":"Art DragonBall Legend Origin",
        // "token_uri":"https://dragonballnft.shop/json/415",
        // "metadata":null,
        // "last_token_uri_sync":"2022-09-24T03:18:37.022Z",
        // "last_metadata_sync":"2022-09-24T03:19:46.435Z"

        // if(sample.result[i].token_uri.includes("ipfs://")) {
          // console.log(sample.result[i].token_uri);
          // sample.result[i].token_uri = sample.result[i].token_uri.replace("ipfs://","https://ipfs.io/ipfs/");
          // console.log(sample.result[i].token_uri);
        // }

        
        // else {
          if(sample.result[i].token_uri) {
            const options = {
              method: 'GET',
              url: `${sample.result[i].token_uri}`,
              headers: {accept: 'application/json'}
            }
    
            // const options = {
            //   method: 'GET',
            //   url: `${MORALIS_BASE}/${sample.result[i].token_address}/${sample.result[i].token_id}`,
            //   params: {chain: 'eth', format: 'decimal'},
            //   headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
            // }
    
            axios.request(options).then(details => {
              // console.log(details.data);
              var f = <CelebrityNftDetails>{
                name: !!details.data.name ? details.data.name : sample.result[i].name,
                displayImage: !!details.data.image ? (details.data.image.includes("ipfs://ipfs/") ? details.data.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/") : (details.data.image.includes("ipfs://") ? details.data.image.replace("ipfs://","https://ipfs.io/ipfs/") : details.data.image)) : details.data,
                network: sample.result[i].contract_type,
                descriptionText: !!details.data.description ? details.data.description : sample.result[i].name,
                contractAddress: sample.result[i].token_address,
                tokenId: sample.result[i].token_id,
                creationDate: +new Date(sample.result[i].last_token_uri_sync),
                modificationDate: +new Date(new Date(sample.result[i].last_metadata_sync)),
                rank: 1,
                internalInformation: !!details.data.attributes ? { attributes: details.data.attributes } : {},
                releaseStatus: true,
                visibility: true
              };
              ret.push(f);
            }).catch(e => {
              if(sample.result[i].metadata) {
                var metadata = JSON.parse(sample.result[i].metadata);
                // console.log(metadata);
                var f = <CelebrityNftDetails>{
                  name: metadata.name,
                  displayImage: (metadata.image.includes("ipfs://ipfs/")) ? (metadata.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/")) : (metadata.image.includes("ipfs://") ? metadata.image.replace("ipfs://","https://ipfs.io/ipfs/") : metadata.image),
                  network: sample.result[i].contract_type,
                  descriptionText: metadata.description,
                  contractAddress: sample.result[i].token_address,
                  tokenId: sample.result[i].token_id,
                  creationDate: +new Date(sample.result[i].last_token_uri_sync),
                  modificationDate: +new Date(new Date(sample.result[i].last_metadata_sync)),
                  rank: 1,
                  internalInformation: !!metadata.attributes ? { attributes: metadata.attributes } : {},
                  releaseStatus: true,
                  visibility: true
                };
                ret.push(f);
              }
              // reject(e);
            })
          } else {
            console.log("No token_uri");
            if(sample.result[i].metadata) {
              var metadata = JSON.parse(sample.result[i].metadata);
              console.log(metadata);
              var f = <CelebrityNftDetails>{
                name: metadata.name,
                displayImage: (metadata.image.includes("ipfs://ipfs/")) ? (metadata.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/")) : (metadata.image.includes("ipfs://") ? metadata.image.replace("ipfs://","https://ipfs.io/ipfs/") : metadata.image),
                network: sample.result[i].contract_type,
                descriptionText: metadata.description,
                contractAddress: sample.result[i].token_address,
                tokenId: sample.result[i].token_id,
                creationDate: +new Date(sample.result[i].last_token_uri_sync),
                modificationDate: +new Date(new Date(sample.result[i].last_metadata_sync)),
                rank: 1,
                internalInformation: !!metadata.attributes ? { attributes: metadata.attributes } : {},
                releaseStatus: true,
                visibility: true
              };
              ret.push(f);
            } 
          }
        }       
      // }
      resolve(ret);
    })
  }

}
