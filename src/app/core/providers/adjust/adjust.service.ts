import { Injectable } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { environment } from '../../../../environments/environment';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { App } from '@capacitor/app';

declare var window: any;
declare var Adjust: any;
declare var cordova: any;
declare var AdjustConfig: any;
declare var AdjustEvent: any;

export enum AppTrackingAuthorizationStatus {
  ATTrackingManagerAuthorizationStatusNotDetermined = 0,
  ATTrackingManagerAuthorizationStatusRestricted = 1,
  ATTrackingManagerAuthorizationStatusDenied = 2,
  ATTrackingManagerAuthorizationStatusAuthorized = 3
}

@Injectable({
  providedIn: 'root'
})
export class AdjustService {

  private API_TOKEN = 'sh5fsjuy8dts';

  constructor(
    private alertController: AlertController,
    private translateProviderService: TranslateProviderService,
    private openNativeSettings: OpenNativeSettings,
    private secureStorage: SecureStorageService,
    private platform: Platform
  ) { }

  public snippet() {
    if(this.platform.is('hybrid')) {

      var adjustEvent = new AdjustEvent;
      adjustEvent.eventToken="2bb0ll";

      var adjust = window.Adjust;
      var initiateTracking = () => {
        adjust.trackEvent(adjustEvent);
        adjust.getSdkVersion((version) => {
          // console.log(`Adjust SDK version: ${version}`)
        });
        adjust.getSdkPrefix((prefix) => {
          // console.log(`Adjust SDK prefix: ${prefix}`)
        });
        adjust.isEnabled((isEnabled) => {
          // console.log(`Adjust isEnabled?: ${isEnabled}`)
        });
        adjust.getAdid((adId)=> {
          // console.log(`Adjust AdId: ${adId}`)
        }); // 788ff12e0cc076cab90814bec2ca9b5f
        adjust.getIdfa((idfa)=> {
          // console.log(`Adjust IDFA: ${idfa}`)
        });
        adjust.getAmazonAdId(amazonId => {
          // console.log(`Adjsut Amazon AdId: ${amazonId}`)
        });
        adjust.getGoogleAdId(googleId => {
          // console.log(`Adjust Google AdId: ${googleId}`)
        });
      }

      if(this.platform.is('ios')) {
        adjust.getAppTrackingAuthorizationStatus(appTrackingStatus => console.log(`Adjust iOS App Tracking Status: ${appTrackingStatus}`));
        adjust.requestTrackingAuthorizationWithCompletionHandler(status => {
          // console.log(status);
          var appTrackingAuthorizationStatus = {
             "0": "ATTrackingManagerAuthorizationStatusNotDetermined",
             "1": "ATTrackingManagerAuthorizationStatusRestricted",
             "2": "ATTrackingManagerAuthorizationStatusDenied",
             "3": "ATTrackingManagerAuthorizationStatusAuthorized"
          }
          // alert(`Tracking Status: ${status}: ${appTrackingAuthorizationStatus[status.toString()]}`);
          // if(["1", "2" ].includes(status.toString())) {
          //     this.presentAlertConfirm();
          // }

          initiateTracking();

          // if(status.toString() == "3") {
          //   initiateTracking();
          // }

        })
      } else {

        initiateTracking();

        var uuid = this.generateUUID();
        adjust.setReferrer(`random-referrer-ansik-${uuid}`);
        // console.log(`Adjust Android App referrer set: random-referrer-ansik-${uuid}`);
      }

      this.secureStorage.getValue(SecureStorageKey.firebase, false).then(firebase => {
        var token = this.generateUUID();
        var finalToken = !!firebase ? firebase : `random-token-ansik-${token}`;
        adjust.setPushToken(finalToken); // Setting the firebase Token
        // console.log(`Adjust pushToken set to: ${finalToken}`);
      })

    }
  }

  public hasUserAllowedTracking(): Promise<boolean> {
    return new Promise((resolve) => {
      if(this.platform.is('hybrid')) {
        var adjust = window.Adjust;
        if(this.platform.is('ios')) {
          adjust.getAppTrackingAuthorizationStatus(appTrackingStatus => {
            // console.log(`Adjust iOS App Tracking Status: ${appTrackingStatus}`)
            resolve(appTrackingStatus.toString() == "3");
          });
        } else {
          resolve(true);
        }
      } else {
        resolve(true);
      }
    })
  }

  public isUserYetToAllowTracking(): Promise<boolean> {
    return new Promise((resolve) => {
      var adjust = window.Adjust;
      if(this.platform.is('ios')) {
        adjust.getAppTrackingAuthorizationStatus(appTrackingStatus => {
          // console.log(`Adjust iOS App Tracking Status: ${appTrackingStatus}`)
          resolve(appTrackingStatus.toString() == "0");
        });
      } else {
        resolve(false);
      }
    })
  }

  public init() {
    if(this.platform.is('hybrid')) {
      var env = environment.production ? 'production' : 'sandbox'
      var adjustConfig = new AdjustConfig(this.API_TOKEN, env);
      adjustConfig.setSendInBackground(true);
      adjustConfig.setLogLevel(AdjustConfig.LogLevelWarn);
      adjustConfig.setDefaultTracker('2bb0ll');
      window.Adjust.create(adjustConfig);
    }
  }

  private generateUUID() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
      d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  public presentAlertConfirm() {
    this.alertController.create({
        mode: 'ios',
        header: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.header'),
        message: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.message'),
        buttons: [
        {
            text: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.settings'),
            handler: () => {
                this.openNativeSettings.open('tracking')
                .then(_ => { App.exitApp(); } )
                .catch(e => {
                  console.log(e)
                  this.openNativeSettings.open('privacy')
                  .then(_ => { App.exitApp(); } )
                  .catch(e => {
                    console.log(e)
                  })
                })
            }
        },
        {
            text: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.cancel'),
            role: 'cancel',
            handler: () => {
            }
        }]
    }).then(alert => alert.present())
}
}
