import { TestBed } from '@angular/core/testing';

import { KycmediaService } from './kycmedia.service';

describe('KycmediaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KycmediaService = TestBed.get(KycmediaService);
    expect(service).toBeTruthy();
  });
});
