import { Injectable } from '@angular/core';
// import { SecureStorageService } from '../secure-storage/secure-storage.service';
// import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { ApiProviderService } from '../api/api-provider.service';

@Injectable({
  providedIn: 'root'
})
export class SecurePinService {

  constructor(
    // private _SecureStorageService: SecureStorageService,
    // private _SecureStorageKey: SecureStorageKey,
    private _CryptoProviderService: CryptoProviderService,
    private _ApiProviderService: ApiProviderService
  ) { }

  public encryptPIN(pin: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this._CryptoProviderService.returnUserSymmetricKey().then(key => {
        this._CryptoProviderService.symmetricEncrypt(pin, key).then((encrypted) => {
          resolve(encrypted);
        }).catch(() => {
          reject()
        })
      }).catch(() => {
        reject()
      })
    })
  }

  public pushEncryptedPIN(pin: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.encryptPIN(pin).then((encryptedPIN) => {
        this._ApiProviderService.postSecurePIN(encryptedPIN).then(() => {
          resolve(encryptedPIN);
        }).catch((e) => {
          reject()
        })
      }).catch((e) => {
        reject()
      })
    })
  }

  public fetchEncryptedPIN(): Promise<string> {
    return new Promise((resolve, reject) => {
      this._ApiProviderService.getSecurePIN().then(encryptedPIN => {
        // this._SecureStorageService.setValue(SecureStorageKey.securePINEncrypted, encryptedPIN).then(() => {
          resolve(encryptedPIN);
        // }).catch(() => {
        //   reject()
        // })
      }).catch(() => {
        reject()
      })
    })
  }

  public checkPIN(enteredPIN: string, encryptedPIN: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this._CryptoProviderService.returnUserSymmetricKey().then(key => {
        this._CryptoProviderService.symmetricDecrypt(encryptedPIN, key).then((pin) => {
          (enteredPIN == pin) ? resolve(true) : resolve(false);
        }).catch(() => {
          reject()
        })
      }).catch(() => {
        reject()
      })
    })
  }

}
