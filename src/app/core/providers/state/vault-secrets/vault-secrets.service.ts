import { Injectable } from '@angular/core';


// custom imports
import { VaultSecretsStore } from './vault-secrets.store';
import { VaultSecrets } from './vault-secrets.model';


@Injectable({ providedIn: 'root' })
export class VaultSecretsServiceAkita {

  constructor(private store: VaultSecretsStore) {
  }

  getValue(key: string): string {
    return this.store.getValue()[key];
  }

  update(state: Partial<VaultSecrets>) {
    this.store.update(state);
  }

  public clear() {
    let ob = this.store.getValue();

    Object.entries(ob).forEach(([key, value]) => {
      ob[key] = null;
    });

    this.store.update(ob);
  }
}
