import { Injectable } from '@angular/core';
import { Store, StoreConfig, UpdateStateCallback } from '@datorama/akita';


// custom imports
import { createVaultSecrets, VaultSecrets,  } from './vault-secrets.model';


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'vault-secrets' })
export class VaultSecretsStore extends Store<VaultSecrets> {

  constructor() {
    super(createVaultSecrets());
  }

}
