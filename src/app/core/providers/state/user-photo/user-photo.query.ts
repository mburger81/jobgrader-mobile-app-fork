import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';


// custom imports
import { UserPhoto } from './user-photo.model';
import { UserPhotoStore } from './user-photo.store';


@Injectable({ providedIn: 'root' })
export class UserPhotoQuery extends Query<UserPhoto> {

  constructor(protected store: UserPhotoStore) {
    super(store);
  }

}
