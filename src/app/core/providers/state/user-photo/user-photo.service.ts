import { Injectable } from '@angular/core';


// custom imports
import { UserPhotoStore } from './user-photo.store';


@Injectable({ providedIn: 'root' })
export class UserPhotoServiceAkita {

  constructor(private store: UserPhotoStore) {
  }

  getPhoto(): string {
    return this.store.getValue().photo
  }

  updatePhoto(photo: string) {
    this.store.update({ photo });
  }

  public clear() {
    let ob = this.store.getValue();

    Object.entries(ob).forEach(([key, value]) => {
      ob[key] = null;
    });

    this.store.update(ob);
  }
}
