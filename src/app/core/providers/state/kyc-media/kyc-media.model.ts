export interface KycMedia {
  id: number,
  image: string,
  imageIdentifier: string,
  documentType: string,
  sessionId: string,
  lastmodified: string,
  verifiedBy: string,
}

export function createKycMedia(): KycMedia {
  return {

  } as KycMedia;
}
