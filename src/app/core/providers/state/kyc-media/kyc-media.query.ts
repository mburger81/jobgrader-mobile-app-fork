import { Injectable } from '@angular/core';
import { Query, QueryEntity } from '@datorama/akita';


// custom imports
import { KycMediaState, KycMediaStore } from './kyc-media.store';


@Injectable({ providedIn: 'root' })
export class KycMediaQuery extends QueryEntity<KycMediaState> {

  constructor(protected store: KycMediaStore) {
    super(store);
  }

}
