import { Injectable } from '@angular/core';
import { DarkMode, IsDarkModeResult } from '@aparajita/capacitor-dark-mode';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { Settings, SettingsService } from '../settings/settings.service';
import { EventsService, EventsList } from '../events/events.service';
import { AppStateService } from '../app-state/app-state.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeSwitcherService {

  private currentTheme: 'light' | 'dark';

  constructor(
    private secureStorage: SecureStorageService,
    private setting: SettingsService,
    private _EventsService: EventsService,
    private _AppStateService: AppStateService
  ) {


  }

  themeEventSubscription() {
    this._EventsService.subscribe(EventsList.themeChange, (theme) => {
      console.log(theme);
      this.setTheme(theme);
    })
  }

  getCurrentTheme() {

    return this.currentTheme;

  }

  saveTheme(theme: string ){

    if(this._AppStateService.basicAuthToken) {
      this.setting.set(Settings.themeMode, theme, true);
    } else {
      this.secureStorage.setValue(SecureStorageKey.themeMode, theme);
    }

  }

  getSavedTheme(): void {
    this.secureStorage.getValue(SecureStorageKey.themeMode, false).then( (themeName: string) => {
        if(themeName){
          this.setTheme(themeName);
        }
        else {
          this.setTheme('system');
        }
    });
  }

  setTheme(themeName: string): void {
    // console.log('ThemeSwitcher#setTheme; themeName:', themeName);

    if (themeName == 'system') {

        this.setSystemTheme();

    } else {

        if (themeName === 'dark') {

          this.currentTheme = 'dark';
          DarkMode.update({ dark: true });

        } else if (themeName === 'light') {

          this.currentTheme = 'light';
          DarkMode.update({ dark: false });

        } else {

          this.setSystemTheme();

        }

    }

    //Save the newly changed theme
    this.saveTheme(this.currentTheme);

  }

  private setSystemTheme() {

    DarkMode.isDarkMode()
      .then(
        (res: IsDarkModeResult) => {
          if (res.dark) {

            this.setTheme('dark');

          } else {

            this.setTheme('light');

          }
        }
      )
      .catch(
        (error) => {
          console.error(error);

          this.setTheme('light');
        }
      );

  }
}
