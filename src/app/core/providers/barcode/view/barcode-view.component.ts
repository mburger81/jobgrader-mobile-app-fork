import { Component } from '@angular/core';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
import { ModalController } from '@ionic/angular';
import { TranslateProviderService } from '../../translate/translate-provider.service';
import { LockService } from 'src/app/lock-screen/lock.service';


@Component({
  selector: 'app-barcode-view',
  templateUrl: './barcode-view.component.html',
  styleUrls: ['./barcode-view.component.scss'],
})
export class BarcodeViewComponent {

  heading;

  constructor(
    private modalController: ModalController,
    private lockService: LockService,
    private translate: TranslateProviderService
  ) {
    this.heading = this.translate.instant('CHAT.scan-qr-code');
    this.lockService.removeResume();
  }

  async cancel() {
    // console.log('BarcodeViewComponent#cancel');

    await BarcodeScanner.stopScan();

    this.lockService.addResume();

    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
