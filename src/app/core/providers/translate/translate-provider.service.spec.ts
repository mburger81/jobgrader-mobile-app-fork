import { TestBed } from '@angular/core/testing';

import { TranslateProviderService } from './translate-provider.service';

describe('TranslateProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TranslateProviderService = TestBed.get(TranslateProviderService);
    expect(service).toBeTruthy();
  });
});
