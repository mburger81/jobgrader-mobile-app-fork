export interface AppState {
    isAuthorized?: boolean;
    isLocked?: boolean;
    basicAuthToken?: string;
}
