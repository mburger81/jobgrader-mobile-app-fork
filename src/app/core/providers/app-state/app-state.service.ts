import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AppState } from './app-state.interface';

@Injectable()
export class AppStateService extends BehaviorSubject<AppState> {

  constructor() {
    super({});
  }

  /** Checks wether or not the user is authenticated */
  public get isAuthorized(): boolean {
    return super.getValue().isAuthorized;
  }

  /** Checks whether or not app is locked */
  public get isLocked(): boolean {
    return super.getValue().isLocked;
  }

  /** Setter for the is authenticated state */
  public set isLocked(isLocked: boolean) {
    super.next({
      ...super.getValue(),
      isLocked
    });
  }

  /**
   * Retrieves the access token from the parent
   * behaviour subject
   */
  public get basicAuthToken(): string {
    return super.getValue().basicAuthToken;
  }

  /** Sets the access token on app state */
  public set basicAuthToken(basicAuthToken: string) {
    super.next({
      ...super.getValue(),
      basicAuthToken
    });
  }

  /** Setter for the is authenticated state */
  public set isAuthorized(isAuthorized: boolean) {
    if (isAuthorized !== this.isAuthorized) {
      super.next({
        ...super.getValue(),
        isAuthorized,
        isLocked: false
      });
    }
  }

  /** Value getter */
  public get stateValue(): {[key: string]: any} {
    return super.getValue();
  }
}
