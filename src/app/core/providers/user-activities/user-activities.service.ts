import { Injectable } from '@angular/core';
import { ApiProviderService } from '../api/api-provider.service';
import { DeviceService } from '../device/device.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../secure-storage/secure-storage.service';

export enum ActivityKeys {
  USER_CREATED = 'USER_CREATED',
  VERIFIED_BACKUP_CLOUD = 'VERIFIED_BACKUP_CLOUD',
  UNVERIFIED_BACKUP_CLOUD = 'UNVERIFIED_BACKUP_CLOUD',
  VERIFIED_BACKUP_LOCAL = 'VERIFIED_BACKUP_LOCAL',
  UNVERIFIED_BACKUP_LOCAL = 'UNVERIFIED_BACKUP_LOCAL',
  VERIFIED_BACKUP_PAPER = 'VERIFIED_BACKUP_PAPER',
  UNVERIFIED_BACKUP_PAPER = 'UNVERIFIED_BACKUP_PAPER',
}

export interface UserActivity {
  timestamp: any;
  activity: ActivityKeys;
  deviceId?: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserActivitiesService {

  constructor(
    private _SecureStorage: SecureStorageService,
    private _ApiProvider: ApiProviderService,
    private _Device: DeviceService
  ) { }

  getActivityFromStorage(activity: ActivityKeys): Promise<UserActivity> {
    return new Promise((resolve, reject) => {
      this._SecureStorage.getValue(SecureStorageKey.userActivities, false).then((userActivities) => {
        var v: Array<UserActivity> = !!userActivities ? JSON.parse(userActivities): [];
        var check = v.find(vv => vv.activity == activity);
        resolve(check);
      })
    })
  }

  updateActivity(activity: ActivityKeys): Promise<void> {
    return new Promise((resolve, reject) => {
      this._SecureStorage.getValue(SecureStorageKey.userActivities, false).then((activities) => {
        var v: Array<UserActivity> = !!activities ? JSON.parse(activities): [];

        this._Device.getDeviceId().then((deviceId) => {
          var ob = { 
            timestamp: +new Date(), 
            activity,
            deviceId 
          };
          v.push(ob);
          this._ApiProvider.updateUserActivities(ob).then(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          }).catch(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          })
          
        }).catch(e => {
          var ob = { 
            timestamp: +new Date(), 
            activity
          };
          v.push(ob);
          this._ApiProvider.updateUserActivities(ob).then(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          }).catch(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          })
        })
      })
    })
  }

  fetchActivities(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._ApiProvider.fetchUserActivities().then((response) => {
        var parsedResponse = <Array<UserActivity>>response.content.map(r => {
          return {
            activity: r.activity,
            timestamp: r.timestamp,
            deviceId: r.deviceId
          }
        })
        this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(parsedResponse)).then(() => {
          resolve();
        }).catch(e => {
          resolve();
        })
      }).catch(e => {
        resolve();
      })
    })
  }

  getActivities(): Promise<Array<UserActivity>> {
    return new Promise((resolve, reject) => {
      this._SecureStorage.getValue(SecureStorageKey.userActivities, false).then((activities) => {
        var v: Array<UserActivity> = !!activities ? JSON.parse(activities): [];
        resolve(v.reverse());
      })
    })
  }

  getTest(): Promise<Array<UserActivity>> {
    return new Promise((resolve, reject) => {
      
        var v: Array<UserActivity> = [{
          timestamp: +new Date(),
          activity: ActivityKeys.USER_CREATED
        },{
          timestamp: +new Date(),
          activity: ActivityKeys.VERIFIED_BACKUP_CLOUD
        },{
          timestamp: +new Date(),
          activity: ActivityKeys.UNVERIFIED_BACKUP_CLOUD
        }]
        resolve(v);
      
    })
  }

}
