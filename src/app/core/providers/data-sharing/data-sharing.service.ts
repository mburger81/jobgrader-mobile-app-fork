import { Injectable } from '@angular/core';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { ApiProviderService } from '../api/api-provider.service';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { DataSharingModalComponent } from 'src/app/requests/data-sharing-modal/data-sharing-modal.component';
const eccrypto = require('@toruslabs/eccrypto');
const crypto = require('crypto');

export interface StoredRequest {
  username: string;
  dataAccessProcessId: string;
  publicKey: string;
  status: number;
  fields: Array<string>;
  checks: string;
  timestamp: number;
  translatedFields? : string;
}

export interface PaymentRequests {
  userPublicKey: string;
  amount: string;
  currency: string;
  timestamp: number;
  status: number;
  pendingProcesses: Array<String>;
  privateKey: string;
  paymentRequestId: string;
  trustObject?: any;
}

export enum Checks {
  minagecheck = 'minagecheck',
  agecheck = 'agecheck',
  KYC_AML = 'KYC_AML'
}

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {

  constructor(
    private _LoaderProviderService: LoaderProviderService,
    private toastController: ToastController,
    private translateProviderService: TranslateProviderService,
    private alertController: AlertController,
    private apiProviderService: ApiProviderService,
    private secureStorageService: SecureStorageService,
    private _CryptoProviderService: CryptoProviderService,
    private _ModalController: ModalController
  ) { }

  generateDummyPublicKey() {
    return this._CryptoProviderService.uInt8ArrayToBase64(eccrypto.getPublic(eccrypto.generatePrivate()))
  }

  getTranslations(key: string) {
    return this.translateProviderService.instant(`BASICDATA.${key}`).toString();
  }

  errorHandling (e: any) {
    console.log(e);
    this._LoaderProviderService.loaderDismiss().then(() => {
      this.toastController.create({ 
        message: this.translateProviderService.instant('LOADER.somethingWrong'),
        duration: 2000, position: 'top'
     }).then(toast => toast.present())
    })
}

  async signRequestedData(processId: string, userName: string, publicKey: string, fields: Array<string>, checks: string, status: number, timestamp: number): Promise<void> {
    // processId = !!processId ? processId : "d0efea35-91c5-4b93-8f29-32ac8107d13e";
    // publicKey = this.generateDummyPublicKey();
    var storedRequestsString = await this.secureStorageService.getValue(SecureStorageKey.dataSharingRequestsReceived, false);
    var storedRequests: Array<StoredRequest> = !!storedRequestsString ? JSON.parse(storedRequestsString) : [];
    var translatedFields = fields.map((g) => { return this.translateProviderService.instant(`BASICDATA.${g}`)}).join(", ");
    // var translatedFields = "Data";
    // console.log(translatedFields);
    
    return new Promise((resolve, reject) => {
        var status0 = () => this.alertController.create({
            mode: 'ios',
            header: this.translateProviderService.instant('REQUESTS.status0.header'),
            message: `${this.translateProviderService.instant('REQUESTS.status0.message.part1')} ${userName} ${this.translateProviderService.instant('REQUESTS.status0.message.part2')} ${translatedFields}.`,
            buttons: [
                {
                    text: this.translateProviderService.instant('REQUESTS.buttons.confirm'),
                    handler: () => {
                      this.confirm({
                        username: userName,
                        dataAccessProcessId: processId,
                        publicKey: publicKey,
                        fields: fields,
                        status: 0,
                        timestamp: +new Date(),
                        checks: checks
                      }).then(() => {
                        resolve()
                      })
                    }
                },
                {
                    text: this.translateProviderService.instant('REQUESTS.buttons.deny'),
                    handler: () => {
                      this.deny({
                        username: userName,
                        dataAccessProcessId: processId,
                        publicKey: publicKey,
                        fields: fields,
                        status: 0,
                        timestamp: +new Date(),
                        checks: checks
                      }).then(() => {
                        resolve()
                      })
                    }
                },
                {
                    role: 'cancel',
                    text: this.translateProviderService.instant('REQUESTS.buttons.cancel'),
                    handler: () => {
                      this.cancel({
                        username: userName,
                        dataAccessProcessId: processId,
                        publicKey: publicKey,
                        fields: fields,
                        status: 0,
                        timestamp: +new Date(),
                        checks: checks
                      }).then(() => {
                        resolve()
                      })
                    }
                }
            ]
        }).then(alerti => {
            alerti.present();
        })
        var status1 = () => this.alertController.create({
          mode: 'ios',
          header: this.translateProviderService.instant('REQUESTS.status1.header'),
          message: `${this.translateProviderService.instant('REQUESTS.status1.message.part1')} ${userName} ${this.translateProviderService.instant('REQUESTS.status1.message.part2')} ${translatedFields}.`,
          buttons: [
              {
                  role: 'cancel',
                  text: this.translateProviderService.instant('REQUESTS.buttons.ok'),
                  handler: () => {
                      resolve();
                  }
              }
          ]
        }).then(alerti => {
            alerti.present();
        })

        var status1modal = () => {
          this._ModalController.create({
            component: DataSharingModalComponent,
            componentProps: {
              data: {
                username: userName,
                dataAccessProcessId: processId,
                publicKey: publicKey,
                fields: fields,
                status: status,
                timestamp: timestamp,
                checks: checks
              }
            }
          }).then(modal => modal.present())
        }

        var statusm1 = () => this.alertController.create({
          mode: 'ios',
          header: this.translateProviderService.instant('REQUESTS.statusm1.header'),
          message: `${this.translateProviderService.instant('REQUESTS.statusm1.message.part1')} ${userName} ${this.translateProviderService.instant('REQUESTS.statusm1.message.part2')} ${translatedFields}.`,
          buttons: [
              {
                  role: 'cancel',
                  text: this.translateProviderService.instant('REQUESTS.buttons.ok'),
                  handler: () => {
                      resolve();
                  }
              }
          ]
        }).then(alerti => {
            alerti.present();
        })

        var statusm1modal = () => {
          this._ModalController.create({
            component: DataSharingModalComponent,
            componentProps: {
              data: {
                username: userName,
                dataAccessProcessId: processId,
                publicKey: publicKey,
                fields: fields,
                status: status,
                timestamp: timestamp,
                checks: checks
              }
            }
          }).then(modal => modal.present())
        }

        if(!status || status == 0) {
          status0();
        }
        if(status == 1) {
          status1modal();
        }
        if(status == -1) {
          statusm1modal();
        }
    })
  }

  public async confirm(requestData: StoredRequest): Promise<void> {
    var processId = requestData.dataAccessProcessId;
    var userName = requestData.username;
    var publicKey = requestData.publicKey;
    var fields = requestData.fields;
    var checks = requestData.checks;
    var storedRequestsString = await this.secureStorageService.getValue(SecureStorageKey.dataSharingRequestsReceived, false);
    var storedRequests: Array<StoredRequest> = !!storedRequestsString ? JSON.parse(storedRequestsString) : [];
    var translatedFields = fields.map((g) => { return this.translateProviderService.instant(`BASICDATA.${g}`)}).join(", ");
    // var translatedFields = "Data";
    console.log(translatedFields);
    return new Promise((resolve, reject) => {
      this._LoaderProviderService.loaderCreate().then(() => {
        this.secureStorageService.getValue(SecureStorageKey.deviceInfo, false).then((deviceId) => {
          this.apiProviderService.confirmDataaccessprocess(processId, deviceId).then((encryptedUserData) => {
              console.log("encryptedUserData");
              console.log(encryptedUserData);
                  this._CryptoProviderService.decryptDataSharingData(encryptedUserData).then((decryptedData) => {
                    console.log("decryptedData");
                    console.log(decryptedData);
                    var processObject = JSON.parse(decryptedData);
                    delete (processObject as any).requestingUser;
                    this.sharingChecks(checks).then(checkResponse => {
                      processObject = Object.assign(processObject, { checks: checkResponse });
                      this._CryptoProviderService.encryptGenericDataWithThirdPartyPublicKey(JSON.stringify(processObject), publicKey).then((sendInfo) => {
                        console.log("sendInfo: ");
                        console.log(sendInfo);
                        this._CryptoProviderService.signMessage(JSON.stringify(processObject)).then((signature) => {
                          // sendInfo = Object.assign(sendInfo, { proof: signature });
                          this.apiProviderService.sendDataaccessprocess(processId, sendInfo, signature).then((responseMw) => {
                            console.log("responseMw 1");
                            console.log(responseMw);
                              this._LoaderProviderService.loaderDismiss().then(() => {
                                var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
                                if( check == -1 ) {
                                  storedRequests.push({
                                    username: userName,
                                    dataAccessProcessId: processId,
                                    publicKey: publicKey,
                                    fields: fields,
                                    checks: checks,
                                    timestamp: +new Date(),
                                    status: 1
                                  })
                                } else {
                                  storedRequests[check].status = 1;
                                }
                                this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
                                  resolve();
                                })
                              })
                          }).catch((e)=> { 
                            var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
                            if( check == -1 ) {
                              storedRequests.push({
                                username: userName,
                                dataAccessProcessId: processId,
                                publicKey: publicKey,
                                fields: fields,
                                checks: checks,
                                timestamp: +new Date(),
                                status: 0
                              }) 
                            }
                            this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
                              this.errorHandling(e);
                              resolve();
                            })
                          })
                        }) 
                      }).catch((e)=> { this.errorHandling(e) })
                    }).catch(e => { this.errorHandling(e) })
                  }).catch((e) => { 
                      var stringifiedData = JSON.stringify(encryptedUserData);
                      console.log("stringifiedData: ");
                      console.log(stringifiedData);
                      this._CryptoProviderService.encryptGenericDataWithThirdPartyPublicKey(stringifiedData, publicKey).then((sendInfo) => {
                          console.log("sendInfo: ");
                          console.log(sendInfo);
                          this.apiProviderService.sendDataaccessprocess(processId, sendInfo).then((responseMw) => {
                            console.log("responseMw 2");
                            console.log(responseMw);
                              this._LoaderProviderService.loaderDismiss().then(() => {
                                var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
                                if( check == -1 ) {
                                  storedRequests.push({
                                    username: userName,
                                    dataAccessProcessId: processId,
                                    publicKey: publicKey,
                                    fields: fields,
                                    checks: checks,
                                    timestamp: +new Date(),
                                    status: 1
                                  })
                                } else {
                                  storedRequests[check].status = 1;
                                }
                                this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
                                  resolve();
                                })
                              })
                          }).catch((e)=> { 
                            var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
                            if( check == -1 ) {
                              storedRequests.push({
                                username: userName,
                                dataAccessProcessId: processId,
                                publicKey: publicKey,
                                fields: fields,
                                checks: checks,
                                timestamp: +new Date(),
                                status: 0
                              }) 
                            }
                            this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
                              this.errorHandling(e);
                              resolve();
                            })
                          })
                      }).catch((e)=> { this.errorHandling(e) })
                  })
              // }).catch((e)=> { errorHandling(e) })
          }).catch((e)=> { 
            var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
            if( check == -1 ) {
              storedRequests.push({
                username: userName,
                dataAccessProcessId: processId,
                publicKey: publicKey,
                fields: fields,
                checks: checks,
                timestamp: +new Date(),
                status: 0
              }) 
            }
            this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
              this.errorHandling(e);
              resolve();
            })
          })
        })
      })
    })
  }

  public async deny(requestData: StoredRequest): Promise<void> {
    var processId = requestData.dataAccessProcessId;
    var userName = requestData.username;
    var publicKey = requestData.publicKey;
    var fields = requestData.fields;
    var checks = requestData.checks;
    var storedRequestsString = await this.secureStorageService.getValue(SecureStorageKey.dataSharingRequestsReceived, false);
    var storedRequests: Array<StoredRequest> = !!storedRequestsString ? JSON.parse(storedRequestsString) : [];
    var translatedFields = fields.map((g) => { return this.translateProviderService.instant(`BASICDATA.${g}`)}).join(", ");
    return new Promise((resolve, reject) => {
      this._LoaderProviderService.loaderCreate().then(() => {
        this.apiProviderService.denyDataaccessprocess(processId).then((res) => {
            console.log(res);
            this._LoaderProviderService.loaderDismiss().then(() => {
              var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
              if( check == -1 ) {
                storedRequests.push({
                  username: userName,
                  dataAccessProcessId: processId,
                  publicKey: publicKey,
                  fields: fields,
                  checks: checks,
                  timestamp: +new Date(),
                  status: -1
                })
              } else {
                storedRequests[check].status = -1;
              }
              this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
                resolve();
              })
            })
        }).catch((e)=> { 
          var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
          if( check == -1 ) {
            storedRequests.push({
              username: userName,
              dataAccessProcessId: processId,
              publicKey: publicKey,
              fields: fields,
              checks: checks,
              timestamp: +new Date(),
              status: 0
            })
          }
          this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
            this.errorHandling(e);
            resolve();
          })
        })
    })
    })
  }

  public async cancel(requestData: StoredRequest): Promise<void> {
    var processId = requestData.dataAccessProcessId;
    var userName = requestData.username;
    var publicKey = requestData.publicKey;
    var fields = requestData.fields;
    var checks = requestData.checks;
    var storedRequestsString = await this.secureStorageService.getValue(SecureStorageKey.dataSharingRequestsReceived, false);
    var storedRequests: Array<StoredRequest> = !!storedRequestsString ? JSON.parse(storedRequestsString) : [];
    var translatedFields = fields.map((g) => { return this.translateProviderService.instant(`BASICDATA.${g}`)}).join(", ");
    return new Promise((resolve, reject) => {
      var check = storedRequests.findIndex(sr => sr.dataAccessProcessId == processId);
      if( check == -1 ) {
        storedRequests.push({
          username: userName,
          dataAccessProcessId: processId,
          publicKey: publicKey,
          fields: fields,
          checks: checks,
          timestamp: +new Date(),
          status: 0
        })
      }
      this.secureStorageService.setValue(SecureStorageKey.dataSharingRequestsReceived, JSON.stringify(storedRequests)).then(() => {
        resolve();
      })
    })
  }

  public generateSharableDataObject(trustList: Array<string>): Promise<any> {
    return new Promise((resolve, reject) => {
      this.secureStorageService.getValue(SecureStorageKey.userData, false).then((userData) => {
        this.secureStorageService.getValue(SecureStorageKey.userTrustData, false).then((userTrustData) => {
          var ob = {};
          var data = !!userData ? JSON.parse(userData): {};
          console.log(data);
          var trust = !!userTrustData ? JSON.parse(userTrustData): {};
          console.log(trust);
          trustList.forEach(tl => {
            if(!!data[tl]) {
              ob = Object.assign(ob, {[tl]: data[tl]})
            }
            if(!!trust[tl]) {
              ob = Object.assign(ob, { trust: { [tl]: trust[tl] }})
            }
            if(trustList.indexOf(tl) == trustList.length - 1) {
              resolve(ob);
            }
          })
        }).catch(e => {
          reject(e);
        })
      }).catch(e => {
        reject(e);
      })
    })
  }

  symmetricDecrypt(text: string, symmetricKey: string) {
    var hexwith0C = this._CryptoProviderService.base64ToHex(this._CryptoProviderService.base64urlTobase64(text));
    var byteArrayResult = this._CryptoProviderService.hex2i(hexwith0C.slice(2));

    if (byteArrayResult.error) {
        console.log(byteArrayResult.error);
    }

    var byteArrayObject = {
        iv: byteArrayResult.result.slice(0, 12),
        ciphertext: byteArrayResult.result.slice(12, byteArrayResult.result.length - 16),
        authTag: byteArrayResult.result.slice(byteArrayResult.result.length - 16)
    };
    var byteArrayObjectHex = {
        iv: Buffer.from(this._CryptoProviderService.base64ToHex(this._CryptoProviderService.uInt8ArrayToBase64(byteArrayObject.iv)), 'hex'),
        ciphertext: Buffer.from(this._CryptoProviderService.base64ToHex(this._CryptoProviderService.uInt8ArrayToBase64(byteArrayObject.ciphertext)), 'hex'),
        authTag: Buffer.from(this._CryptoProviderService.base64ToHex(this._CryptoProviderService.uInt8ArrayToBase64(byteArrayObject.authTag)), 'hex')
    };
    var key = Buffer.from(symmetricKey, 'hex');
    var decipher = crypto.createDecipheriv('aes-256-gcm', key, byteArrayObjectHex.iv);
    decipher.setAuthTag(byteArrayObjectHex.authTag);
    let decrypted = decipher.update(byteArrayObjectHex.ciphertext, 'hex', 'utf8');
    decrypted += decipher.final();
    return decrypted;
}

  public sharingChecks(checks: string): Promise<any> {
    return new Promise((resolve, reject) => {
      var c = !!checks ? JSON.parse(checks): [];
      this.secureStorageService.getValue(SecureStorageKey.userData, false).then((userData) => {
        this.secureStorageService.getValue(SecureStorageKey.userTrustData, false).then((userTrustData) => {
          this.secureStorageService.getValue(SecureStorageKey.vcId, false).then((vcIds) => {
            var data = !!userData ? JSON.parse(userData) : {};
            var trust = !!userTrustData ? JSON.parse(userTrustData) : {};
            var ccc = {};

            var agecheck = c.find(cc => Object.keys(cc)[0] == Checks.agecheck);
            var minagecheck = c.find(cc => Object.keys(cc)[0] == Checks.minagecheck);
            var kycAmlCheck = c.find(cc => Object.keys(cc)[0] == Checks.KYC_AML);

            console.log(agecheck);
            console.log(minagecheck);

            var getAge = function(dateString) {
              var today = new Date();
              var birthDate = new Date(dateString);
              var age = today.getFullYear() - birthDate.getFullYear();
              var m = today.getMonth() - birthDate.getMonth();
              if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                  age--;
              }
              return age;
            }

            var userAge = 0;

            if(!!data.dateofbirth && !!trust.dateofbirth) {
              if(trust.dateofbirth.trustActive && trust.dateofbirth.trustValue == 1) {
                userAge = getAge(data.dateofbirth);
              }
            }
            

            if(agecheck) {
              if( Object.keys(agecheck).length > 0) {
                var minage = (agecheck as any).agecheck.minage;
                var maxage = (agecheck as any).agecheck.maxage;
    
                ccc = Object.assign(ccc, { agecheck: false })
    
                if (userAge >= minage && userAge <= maxage) {
                  ccc = Object.assign(ccc, { agecheck: true })
                }
    
              }
            }

            if(minagecheck) {
              if( Object.keys(minagecheck).length > 0 ) {
                var age = (minagecheck as any).minagecheck.age;
    
                ccc = Object.assign(ccc, { minagecheck: false })
    
                if (userAge >= age) {
                  ccc = Object.assign(ccc, { minagecheck: true })
                }
                
              }
            }

            if(kycAmlCheck) {

             ccc = Object.assign(ccc, { KYC_AML: !!vcIds ? JSON.parse(vcIds) : [] })

            }

            resolve(ccc);
          })  
        })  
      })
    })
    
  }

  

}
