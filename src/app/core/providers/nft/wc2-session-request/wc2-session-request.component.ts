import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, PopoverController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateProviderService } from '../../translate/translate-provider.service';
import { CryptoCurrency, CryptoNetworkNames, CryptoNetworks } from '../../wallet-connect/constants';
import { SecureStorageKey } from '../../secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../../secure-storage/secure-storage.service';
import { environment } from 'src/environments/environment';
import { DidPopoverComponent } from 'src/app/merchant-details/did-popover/did-popover.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-wc2-session-request',
  templateUrl: './wc2-session-request.component.html',
  styleUrls: ['./wc2-session-request.component.scss'],
})
export class Wc2SessionRequestComponent implements OnInit {
  @Input() data: any;
  @Input() namespaces?: any;
  @Input() request: any;
  @Input() additionalTexts?: any;
  @Input() selfAddress: any;
  @Input() targetMerchantAccount: any;

  public image = null;
  public displayData: any;
  public buttonText1 = '';
  public heading = '';
  public message = '';
  public parsed = {
    timestamp: 0,
    trust: {},
    basicdata: {}
  };
  public trustFields = [];
  public basicdataFields = [];
  public icons = [];
  public networkName = CryptoNetworkNames.ETHEREUM;
  public networkValue = CryptoNetworks.ETHEREUM;
  public currency = CryptoCurrency.ETH;

  private datePipe = new DatePipe('en-US');

  public keyTranslations = {
    'firstname': this._Translate.instant('WEB2.FIELDS.firstname'),
    'lastname': this._Translate.instant('WEB2.FIELDS.lastname'),
    'email': this._Translate.instant('WEB2.FIELDS.email'),
    'dateofbirth': this._Translate.instant('WEB2.FIELDS.dateofbirth'),
    'cityofbirth': this._Translate.instant('WEB2.FIELDS.cityofbirth'),
    'countryofbirth':this._Translate.instant('WEB2.FIELDS.countryofbirth'),
    'citizenship': this._Translate.instant('WEB2.FIELDS.citizenship'),
    'street': this._Translate.instant('WEB2.FIELDS.street'),
    'city':this._Translate.instant('WEB2.FIELDS.city'),
    'zip': this._Translate.instant('WEB2.FIELDS.zip'),
    'country': this._Translate.instant('WEB2.FIELDS.country'),
    'identificationdocumentnumber' : this._Translate.instant('WEB2.FIELDS.identificationdocumentnumber'),
    'identificationissuecountry' : this._Translate.instant('WEB2.FIELDS.identificationissuecountry'),
    'identificationissuedate' : this._Translate.instant('WEB2.FIELDS.identificationissuedate'),
    'identificationexpirydate' : this._Translate.instant('WEB2.FIELDS.identificationexpirydate'),
    'driverlicencedocumentnumber' : this._Translate.instant('WEB2.FIELDS.driverlicencedocumentnumber'),
    'driverlicencecountry' : this._Translate.instant('WEB2.FIELDS.driverlicencecountry'),
    'driverlicenceissuedate' : this._Translate.instant('WEB2.FIELDS.driverlicenceissuedate'),
    'driverlicenceexpirydate' : this._Translate.instant('WEB2.FIELDS.driverlicenceexpirydate'),
    'passportnumber' : this._Translate.instant('WEB2.FIELDS.passportnumber'),
    'passportissuecountry' : this._Translate.instant('WEB2.FIELDS.passportissuecountry'),
    'passportissuedate' : this._Translate.instant('WEB2.FIELDS.passportissuedate'),
    'passportexpirydate' : this._Translate.instant('WEB2.FIELDS.passportexpirydate'),
    'residencepermitnumber' : this._Translate.instant('WEB2.FIELDS.residencepermitnumber'),
    'residencepermitissuecountry' : this._Translate.instant('WEB2.FIELDS.residencepermitissuecountry'),
    'residencepermitissuedate' : this._Translate.instant('WEB2.FIELDS.residencepermitissuedate'),
    'residencepermitexpirydate' : this._Translate.instant('WEB2.FIELDS.residencepermitexpirydate'),
    'displaypicture': this._Translate.instant('WEB2.FIELDS.displaypicture'),
  }

  constructor(
    private _ModalController: ModalController,
    public _DomSanitizer: DomSanitizer,
    private _ActionSheetController: ActionSheetController,
    private _SecureStorageService: SecureStorageService,
    private _PopoverController: PopoverController,
    private _Translate: TranslateProviderService
  ) { }

  ngOnInit() {

    console.log(this.data);


    let chains = !!this.namespaces ? 
      this.namespaces.eip155.chains : (
        !!this.data.requiredNamespaces.eip155 ? 
          this.data.requiredNamespaces.eip155.chains : 
          this.data.optionalNamespaces.eip155.chains
          ); // namespace
    console.log(chains);
    let chainIds = chains.map(f => f.replace("eip155:", ""));
    console.log(chainIds);
    for(let chainId of chainIds) {
      console.log(chainId);
      switch(chainId) {
        case "1": this.icons.push('assets/crypto/vector/ETH.svg'); break;
        case "4": this.icons.push('assets/crypto/vector/ETH.svg'); break;
        case "100": this.icons.push('assets/crypto/vector/XDAI.svg'); break;
        case "137": this.icons.push('assets/crypto/vector/MATIC.svg'); break;
        case "508674158": this.icons.push('assets/crypto/vector/EVE.svg'); break;
        case "49262": this.icons.push('assets/crypto/vector/EVE.svg'); break;
        case '80001': this.icons.push('assets/crypto/vector/MATIC.svg'); break;
        default: this.icons.push('assets/crypto/vector/ETH.svg'); break;
      }
    }

    if(this.request == 'session_proposal') {
      this.displayData = this.data.proposer.metadata;
      this.image = this.displayData.icons[0];
      this.heading = this.displayData.name;
      this.buttonText1 = this._Translate.instant('WALLET.connect');
      this.message = this.displayData.description;
    }

    if(['personal_sign', 'eth_sendTransaction', 'eth_signTransaction', 'eth_sign', 'eth_signTypedData'].includes(this.request)) {
      this.displayData = this.data;
      this.image = this.displayData.icons[0];
      this.heading = this.displayData.name;
      this.buttonText1 = this._Translate.instant('WALLET.sign');
      this.message = `${this.additionalTexts.header}: ${this.additionalTexts.message}`;
    }

    if(this.request == 'helix_kyc') {
      this.displayData = this.data;
      this.image = this.displayData.icons[0];
      this.heading = this.displayData.name;
      this.buttonText1 = this._Translate.instant('WALLET.sign');
      this.message = `${this.additionalTexts.header}`;
      this.parsed = JSON.parse(this.additionalTexts.message);
      console.log(this.parsed);
      this.trustFields = Object.keys(this.parsed.trust);
      this.basicdataFields = Object.keys(this.parsed.basicdata);
    }

    console.log("🚀 ~ file: wc2-session-request.component.ts:136 ~ Wc2SessionRequestComponent ~ ngOnInit ~ this.displayData:", this.displayData);

  }

  returnEmoji(v: boolean) {
    return v ? '✅' : '❌';
  }

  parseValue(key: string, value: string) {
    if([ 'dateofbirth', 'identificationissuedate', 'identificationexpirydate', 'driverlicenceissuedate', 'driverlicenceexpirydate', 'passportissuedate', 'passportexpirydate', 'residencepermitissuedate', 'residencepermitexpirydate' ].includes(key)) {
      return this.datePipe.transform(value, 'yyyy-MM-dd')
    } else {
      return value;
    }
    
  }

  moreInfo() {
    var a = document.createElement("a");
    a.style.display = "none";
    a.href = !!this.displayData.peerMeta ? this.displayData.peerMeta.url : this.displayData._peerMeta.url;
    a.target = "_blank";
    a.click();
    a.remove();
  }

  add() {
    this.close(true);
  }

  cancel() {
    this.close(false);
  }

  close(value: boolean) {
    this._ModalController.dismiss({value, networkName: this.networkName, networkValue: this.networkValue});
  }

  tooltipEventInformation() {
    this._PopoverController.create({
      component: DidPopoverComponent,
      componentProps: {
          data: this._Translate.instant('WALLET.tooltip')
      },
      cssClass: 'did-popover',
      showBackdrop: true,
      translucent: true
    }).then((popover) => {
        popover.present();
        setTimeout(() => {
            popover.dismiss();
        }, 10000)
    })
  }

}
