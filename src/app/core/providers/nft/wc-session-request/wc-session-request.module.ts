import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { WcSessionRequestComponent } from './wc-session-request.component';
import { DidPopoverModule } from 'src/app/merchant-details/did-popover/did-popover.module';
const routes: Routes = [
  {
    path: '',
    component: WcSessionRequestComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    DidPopoverModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
  ],
  declarations: [WcSessionRequestComponent]
})
export class WcSessionRequestModule {}
