import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, PopoverController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateProviderService } from '../../translate/translate-provider.service';
import { CryptoCurrency, CryptoNetworkNames, CryptoNetworks } from '../../wallet-connect/constants';
import { SecureStorageKey } from '../../secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../../secure-storage/secure-storage.service';
import { environment } from 'src/environments/environment';
import { DidPopoverComponent } from 'src/app/merchant-details/did-popover/did-popover.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-wc-session-request',
  templateUrl: './wc-session-request.component.html',
  styleUrls: ['./wc-session-request.component.scss'],
})
export class WcSessionRequestComponent implements OnInit {
@Input() data: any;
@Input() request: any;
@Input() additionalTexts?: any;
@Input() selfAddress: any;
@Input() targetMerchantAccount: any;

  public image = null;
  public displayData: any;
  public buttonText1 = '';
  public heading = '';
  public message = '';
  public parsed = {
    timestamp: 0,
    trust: {},
    basicdata: {}
  };
  public trustFields = [];
  public basicdataFields = [];
  public icon = '../../../../../assets/crypto/vector/ETH.svg';
  public networkName = CryptoNetworkNames.ETHEREUM;
  public networkValue = CryptoNetworks.ETHEREUM;
  public currency = CryptoCurrency.ETH;

  private datePipe = new DatePipe('en-US');

  public keyTranslations = {
    'firstname': this._Translate.instant('WEB2.FIELDS.firstname'),
    'lastname': this._Translate.instant('WEB2.FIELDS.lastname'),
    'email': this._Translate.instant('WEB2.FIELDS.email'),
    'dateofbirth': this._Translate.instant('WEB2.FIELDS.dateofbirth'),
    'cityofbirth': this._Translate.instant('WEB2.FIELDS.cityofbirth'),
    'countryofbirth':this._Translate.instant('WEB2.FIELDS.countryofbirth'),
    'citizenship': this._Translate.instant('WEB2.FIELDS.citizenship'),
    'street': this._Translate.instant('WEB2.FIELDS.street'),
    'city':this._Translate.instant('WEB2.FIELDS.city'),
    'zip': this._Translate.instant('WEB2.FIELDS.zip'),
    'country': this._Translate.instant('WEB2.FIELDS.country'),
    'identificationdocumentnumber' : this._Translate.instant('WEB2.FIELDS.identificationdocumentnumber'),
    'identificationissuecountry' : this._Translate.instant('WEB2.FIELDS.identificationissuecountry'),
    'identificationissuedate' : this._Translate.instant('WEB2.FIELDS.identificationissuedate'),
    'identificationexpirydate' : this._Translate.instant('WEB2.FIELDS.identificationexpirydate'),
    'driverlicencedocumentnumber' : this._Translate.instant('WEB2.FIELDS.driverlicencedocumentnumber'),
    'driverlicencecountry' : this._Translate.instant('WEB2.FIELDS.driverlicencecountry'),
    'driverlicenceissuedate' : this._Translate.instant('WEB2.FIELDS.driverlicenceissuedate'),
    'driverlicenceexpirydate' : this._Translate.instant('WEB2.FIELDS.driverlicenceexpirydate'),
    'passportnumber' : this._Translate.instant('WEB2.FIELDS.passportnumber'),
    'passportissuecountry' : this._Translate.instant('WEB2.FIELDS.passportissuecountry'),
    'passportissuedate' : this._Translate.instant('WEB2.FIELDS.passportissuedate'),
    'passportexpirydate' : this._Translate.instant('WEB2.FIELDS.passportexpirydate'),
    'residencepermitnumber' : this._Translate.instant('WEB2.FIELDS.residencepermitnumber'),
    'residencepermitissuecountry' : this._Translate.instant('WEB2.FIELDS.residencepermitissuecountry'),
    'residencepermitissuedate' : this._Translate.instant('WEB2.FIELDS.residencepermitissuedate'),
    'residencepermitexpirydate' : this._Translate.instant('WEB2.FIELDS.residencepermitexpirydate'),
    'displaypicture': this._Translate.instant('WEB2.FIELDS.displaypicture'),
  }

  constructor(
    private _ModalController: ModalController,
    public _DomSanitizer: DomSanitizer,
    private _ActionSheetController: ActionSheetController,
    private _SecureStorageService: SecureStorageService,
    private _PopoverController: PopoverController,
    private _Translate: TranslateProviderService
  ) { }

  ngOnInit() {
    console.log(this.data);

    this.displayData = this.data.params[0];
    this.image = this.displayData.peerMeta.icons[0];
    this.heading = this.displayData.peerMeta.name;

    if(this.request == 'session_request') {
      this.buttonText1 = this._Translate.instant('WALLET.connect');
      this.message = this.displayData.peerMeta.description;
    }

    if(this.request == 'wallet_switchEthereumChain') {
      this.buttonText1 = this._Translate.instant('WALLET.change');
      this.message = `${this.additionalTexts.header}: ${this.additionalTexts.message}`;
    }

    if(this.request == 'personal_sign') {
      this.buttonText1 = this._Translate.instant('WALLET.sign');
      this.message = `${this.additionalTexts.header}: ${this.additionalTexts.message}`;
    }

    if(this.request == 'eth_sign') {
      this.buttonText1 = this._Translate.instant('WALLET.sign');
      this.message = `${this.additionalTexts.header}: ${this.additionalTexts.message}`;
    }

    if(this.request == 'eth_signTypedData') {
      this.buttonText1 = this._Translate.instant('WALLET.sign');
      this.message = `${this.additionalTexts.header}: ${this.additionalTexts.message}`;
    }

    if(this.request == 'helix_kyc') {
      this.buttonText1 = this._Translate.instant('WALLET.acceptSign');
      this.message = `${this.additionalTexts.header}`;
      this.parsed = JSON.parse(this.additionalTexts.message);
      console.log(this.parsed);
      this.trustFields = Object.keys(this.parsed.trust);
      this.basicdataFields = Object.keys(this.parsed.basicdata);
    }

    if(this.request == 'eth_sendTransaction') {
      this.buttonText1 = this._Translate.instant('WALLET.sign');
      this.message = `${this.additionalTexts.header}: ${this.additionalTexts.message}`;
    }

    if(this.request == 'eth_signTransaction') {
      this.buttonText1 = this._Translate.instant('WALLET.sign');
      this.message = `${this.additionalTexts.header}: ${this.additionalTexts.message}`;
    }

    console.log(this.displayData);
    
  }

  returnEmoji(v: boolean) {
    return v ? '✅' : '❌';
  }

  parseValue(key: string, value: string) {
    if([ 'dateofbirth', 'identificationissuedate', 'identificationexpirydate', 'driverlicenceissuedate', 'driverlicenceexpirydate', 'passportissuedate', 'passportexpirydate', 'residencepermitissuedate', 'residencepermitexpirydate' ].includes(key)) {
      return this.datePipe.transform(value, 'yyyy-MM-dd')
    } else {
      return value;
    }
    
  }

  moreInfo() {
    var a = document.createElement("a");
    a.style.display = "none";
    a.href = !!this.displayData.peerMeta ? this.displayData.peerMeta.url : this.displayData._peerMeta.url;
    a.target = "_blank";
    a.click();
    a.remove();
  }

  add() {
    this.close(true);
  }

  cancel() {
    this.close(false);
  }

  close(value: boolean) {
    this._ModalController.dismiss({value, networkName: this.networkName, networkValue: this.networkValue});
  }

  changeNetwork() {

    this._SecureStorageService.getValue(SecureStorageKey.powerUserMode, false).then(powerUserMode => {
      var allowableNetworks = (powerUserMode == true.toString() || !environment.production ) ? [{
        name: CryptoNetworkNames.ETHEREUM,
        value: CryptoNetworks.ETHEREUM,
        currency: CryptoCurrency.ETH,
        image: '../../../../../assets/crypto/vector/ETH.svg'
      },{
        name: CryptoNetworkNames.ETHEREUM_TEST_KOVAN,
        value: CryptoNetworks.ETHEREUM_TEST_KOVAN,
        currency: CryptoCurrency.ETH,
        image: '../../../../../assets/crypto/vector/ETH.svg'
      },{
        name: CryptoNetworkNames.ETHEREUM_TEST_RINKEBY,
        value: CryptoNetworks.ETHEREUM_TEST_RINKEBY,
        currency: CryptoCurrency.ETH,
        image: '../../../../../assets/crypto/vector/ETH.svg'
      },{
        name: CryptoNetworkNames.POLYGON,
        value: CryptoNetworks.POLYGON,
        currency: CryptoCurrency.MATIC,
        image: '../../../../../assets/crypto/vector/MATIC.svg'
      },{
        name: CryptoNetworkNames.POLYGON_TEST_MUMBAI,
        value: CryptoNetworks.POLYGON_TEST_MUMBAI,
        currency: CryptoCurrency.MATIC,
        image: '../../../../../assets/crypto/vector/MATIC.svg'
      },{
        name: CryptoNetworkNames.EVAN,
        value: CryptoNetworks.EVAN,
        currency: CryptoCurrency.EVE,
        image: '../../../../../assets/crypto/vector/EVE.svg'
      },{
        name: CryptoNetworkNames.EVAN_TEST,
        value: CryptoNetworks.EVAN_TEST,
        currency: CryptoCurrency.EVE,
        image: '../../../../../assets/crypto/vector/EVE.svg'
      }] : [{
        name: CryptoNetworkNames.ETHEREUM,
        value: CryptoNetworks.ETHEREUM,
        currency: CryptoCurrency.ETH,
        image: '../../../../../assets/crypto/vector/ETH.svg'
      },{
        name: CryptoNetworkNames.POLYGON,
        value: CryptoNetworks.POLYGON,
        currency: CryptoCurrency.MATIC,
        image: '../../../../../assets/crypto/vector/MATIC.svg'
      }];

      var buttons: any = Array.from(allowableNetworks, k => { 
        return { 
          text: k.name, 
          icon: k.image, 
          handler: () => {
            this.icon = k.image;
            this.networkName = k.name;
            this.networkValue = k.value;
            this.currency = k.currency;
          }
        } 
      });
      buttons.push({ text: 'Cancel', role: 'cancel', handler: () => { console.log('cancelled'); } });
      console.log(buttons);

    // text?: string;
    // role?: 'cancel' | 'destructive' | 'selected' | string;
    // icon?: string;
    // cssClass?: string | string[];
    // handler?: () => boolean | void | Promise<boolean | void>;
  
      this._ActionSheetController.create({
        mode: 'md',
        header: this._Translate.instant('WALLET.networkAlert.header'),
        buttons
      }).then(sheet => {
        sheet.onDidDismiss().then(data => {
          console.log(data);
        })
        sheet.present();
      })
    })

    
  }

  tooltipEventInformation() {
    this._PopoverController.create({
      component: DidPopoverComponent,
      componentProps: {
          data: this._Translate.instant('WALLET.tooltip')
      },
      cssClass: 'did-popover',
      showBackdrop: true,
      translucent: true
    }).then((popover) => {
        popover.present();
        setTimeout(() => {
            popover.dismiss();
        }, 10000)
    })
  }

}
