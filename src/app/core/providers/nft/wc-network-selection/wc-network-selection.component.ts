import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { DidPopoverComponent } from 'src/app/merchant-details/did-popover/did-popover.component';
import { TranslateProviderService } from '../../translate/translate-provider.service';
import { CryptoNetworks } from '../../wallet-connect/constants';

@Component({
  selector: 'app-wc-network-selection',
  templateUrl: './wc-network-selection.component.html',
  styleUrls: ['./wc-network-selection.component.scss'],
})
export class WcNetworkSelectionComponent implements OnInit {
  @Input() data: any;

  public defaulNetwork = CryptoNetworks.ETHEREUM;

  constructor(
    private _ModalController: ModalController,
    private _PopoverController: PopoverController,
    private _Translate: TranslateProviderService
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  add() {
    this.close(true);
  }

  cancel() {
    this.close(false);
  }

  changeDefaultNetwork(event: any) {
    this.defaulNetwork = event.detail.value;
  }

  close(value: boolean) {
    this._ModalController.dismiss({value, network: this.defaulNetwork});
  }

  tooltipEventInformation() {
    this._PopoverController.create({
      component: DidPopoverComponent,
      componentProps: {
          data: this._Translate.instant('WALLET.tooltip')
      },
      cssClass: 'did-popover',
      showBackdrop: true,
      translucent: true
    }).then((popover) => {
        popover.present();
        setTimeout(() => {
            popover.dismiss();
        }, 10000)
    })
  }

}
