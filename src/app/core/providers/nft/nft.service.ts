import { Injectable } from '@angular/core';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { Contract, ethers, Wallet } from 'ethers';
// import { interval, Subscription } from 'rxjs';
import { BarcodeService } from '../barcode/barcode.service';
import { environment } from 'src/environments/environment';
import { WalletConnectService } from '../wallet-connect/wallet-connect.service';
import { Contracts, CryptoCurrency, CryptoNetworks, EthereumNetworks } from '../wallet-connect/constants';
import { ToastController } from '@ionic/angular';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { GAS_USED } from '../gas/gas.service';

const axios = require("axios");
const Web3 = require('web3');

const abi = require('./abi.json');
const thxcabi = require('./thxcabi.json');
declare var window: any;

export interface OpenSeaAsset {
  token_id: string; // The token ID of the NFT
  image_url: string; // An image for the item. Note that this is the cached URL we store on our end. The original image url is image_original_url
  background_color: string; // The background color to be displayed with the item
  name: string; // Name of the item
  external_link: string; // External link to the original website for the item
  asset_contract: string; // Dictionary of data on the contract itself (see asset contract section)
  owner: string; // Dictionary of data on the owner (see account section)
  traits: string; // A list of traits associated with the item (see traits section)
  last_sale: string; // When this item was last sold (null if there was no last sale)
}

export interface OpenSeaEvent {
  event_type: string; // Describes the event type. This can be: created for new auctions, successful for sales, cancelled for cancelled auctions, bid_entered, bid_withdrawn, transfer, offer_entered, or approve
  asset: string; // A subfield containing a simplified version of the Asset or Asset Bundle on which this event happened
  asset_bundle: string; // A subfield containing a simplified version of the Asset or Asset Bundle on which this event happened
  created_date: string; // When the event was recorded
  from_account: string; // The accounts associated with this event.
  to_account: string; // The accounts associated with this event.
  is_private: string; // A boolean value that is true if the sale event was a private sale
  payment_token: string; // The payment asset used in this transaction, such as ETH, WETH or DAI
  quantity: string; // The amount of the item that was sold. Applicable for semi-fungible assets
  total_price: string; // The total price that the asset was bought for. This includes any royalties that might have been collected
}

export interface OpenSeaAccount {
  address: string; // The wallet address that uniquely identifies this account.
  user: string; // An object containing username, a string for the the OpenSea username associated with the account. Will be null if the account owner has not yet set a username on OpenSea.
  config: string; // A string representing public configuration options on the user's account, including verified and moderator for OpenSea verified accounts and OpenSea community support staff.
}

export interface OpenSeaCollection {
  name: string; // The collection name. Typically derived from the first contract imported to the collection but can be changed by the user
  external_link: string; // External link to the original website for the collection
  description: string; // Description for the model
  slug: string; // The collection slug that is used to link to the collection on OpenSea. This value can change by the owner but must be unique across all collection slugs in OpenSea
  image_url: string; // An image for the collection. Note that this is the cached URL we store on our end. The original image url is image_original_url
  banner_image_url: string; // Image used in the horizontal top banner for the collection.
  dev_seller_fee_basis_points: string; // The collector's fees that get paid out to them when sales are made for their collections
  safelist_request_status: string; // The collection's approval status within OpenSea. Can be not_requested (brand new collections), requested (collections that requested safelisting on our site), approved (collections that are approved on our site and can be found in search results), and verified (verified collections)
  payout_address: string; // The payout address for the collection's royalties
  primary_asset_contracts: string; // A list of the contracts that are associated with this collection
  traits: string; // A dictionary listing all the trait types available within this collection
  payment_tokens: string; // The payment tokens accepted for this collection
  editors: string; // Approved editors on this collection.
  stats: string; // A dictionary containing some sales statistics related to this collection, including trade volume and floor prices
}


export enum OpenSeaMethodTypes {
  ASSETS = "assets",
  EVENTS = "events",
  COLLECTION = "collection",
  COLLECTIONS = "collections",
  BUNDLES = "bundles",
  ASSET_CONTRACT = "asset_contract",
}


const OPENSEA_API_ROOT_URL = "https://api.opensea.io/api/v1";
const OPENSEA_TESTNET_API_ROOT_URL = "https://testnets-api.opensea.io/api/v1";

const OPENSEA_API_ROOT_URL_2 = "https://api.opensea.io/api/v2";
const OPENSEA_TESTNET_API_ROOT_URL_2 = "https://testnets-api.opensea.io/api/v2";

const OpenSeaSupportedChains = [
  'ethereum',
  'matic',
  'klaytn',
  'bsc',
  'arbitrum',
  'arbitrum_nova',
  'avalanche',
  'optimism',
  'solana',
  'base',
  'zora'
];

var targetMerchantAccount = "";


var INITIAL_STATE = {
  loading: false,
  scanner: false,
  connector: null,
  uri: "",
  peerMeta: {
    description: "",
    url: "",
    icons: [],
    name: "",
    ssl: false,
  },
  connected: false,
  chainId: null,
  accounts: [],
  address: "",
  activeIndex: null,
  requests: [],
  results: [],
  payload: null,
};

@Injectable({
  providedIn: 'root'
})
export class NftService {

  constructor(
    private _SecureStorageService: SecureStorageService,
    private _BarcodeService: BarcodeService,
    private _ToastController: ToastController,
    private _Vault: VaultService,
    private _Translate: TranslateProviderService,
    private _WalletConnectProvider: WalletConnectService
  ) { }

  

  // returnEVEGasPrice(): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     axios.get(`https://api.polygonscan.com/api?module=gastracker&action=gasoracle&apikey=${POLYGONSCAN_API_KEY}`)
  //       .then(res => resolve(res.data))
  //       .catch(err => reject(err));
  //   })
  // }

  // returnXDAIGasPrice(): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     axios.get(`https://api.polygonscan.com/api?module=gastracker&action=gasoracle&apikey=${POLYGONSCAN_API_KEY}`)
  //       .then(res => resolve(res.data))
  //       .catch(err => reject(err));
  //   })
  // }


  obtainDecentralandCollectibles(account: string): Promise<any> {
    return new Promise((resolve, reject) => {
      axios.get(`https://nft-api.decentraland.org/v1/nfts?first=24&skip=0&sortBy=newest&owner=${account}`)
      .then(response => resolve(response.data))
      .catch(e => reject(e));
    })
  }

  obtainDecentralandCollectibleDetail(contractAddress: string): Promise<any> {
    return new Promise((resolve, reject) => {
      axios.get(`https://nft-api.decentraland.org/v1/collections?contractAddress=${contractAddress}`)
      .then(response => resolve(response.data))
      .catch(e => reject(e));
    })
  }

  

  openseaAPICall(method: OpenSeaMethodTypes, path?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      var url = !!path ? `${OPENSEA_API_ROOT_URL}/${method}${path}` : `${OPENSEA_API_ROOT_URL}/${method}`;
      this._Vault.getSecret(VaultSecretKeys.OPENSEA_API_KEY).then(OPENSEA_API_KEY => {
        axios.get(url,{
          headers: {
            "X-API-KEY": OPENSEA_API_KEY,
            "Accept": "application/json"
          }
        }).then(response => {
          resolve(response.data);
        }).catch(e => {
          reject(e);
        })
      })
    })
  }

  openseaAPICall2(address: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      // https://api.opensea.io/v2/chain/{chain}/account/{address}/nfts
      var nfts = [];
      
      var OPENSEA_API_KEY = await this._Vault.getSecret(VaultSecretKeys.OPENSEA_API_KEY);

      for(let i=0; i<OpenSeaSupportedChains.length; i++) {
        var chain = OpenSeaSupportedChains[i];
        var url = `${OPENSEA_API_ROOT_URL_2}/chain/${chain}/account/${address}/nfts`;
        try {
          var response = await axios.get(url,{
            headers: {
              "X-API-KEY": OPENSEA_API_KEY,
              "Accept": "application/json"
            }
          })

          for(let j=0; j<response.data.nfts.length; j++) {

            if(!!response.data.nfts[j].metadata_url) {
              try {
                var metadata = await axios.get(response.data.nfts[j].metadata_url);
                var attributes = metadata.data.attributes;
                console.log("attributes", attributes);
              } catch(e) {
                attributes = [];
              }
              
            } else {
              attributes = [];
            }

            if(!!response.data.nfts[j].permalink) {
              var permalink = response.data.nfts[j].permalink;
            } else {
              permalink = `https://opensea.io/assets/${chain}/${response.data.nfts[j].contract}/${response.data.nfts[j].identifier}`;
            }

            response.data.nfts[j] = Object.assign(response.data.nfts[j], { attributes, permalink });

          }

          nfts = nfts.concat(response.data.nfts);

        } catch(e) {
          console.log(e);
        }
      }
      
      resolve(nfts);
      
    })
  }

  openseaTestNetAPICall(method: OpenSeaMethodTypes, path?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      var url = !!path ? `${OPENSEA_TESTNET_API_ROOT_URL}/${method}${path}` : `${OPENSEA_TESTNET_API_ROOT_URL}/${method}`;
      axios.get(url,{
        headers: {
          "Accept": "application/json"
        }
      }).then(response => {
        resolve(response.data);
      }).catch(e => {
        reject(e);
      })
    })
  }

 
  async scanWalletConnect(uri: string, address?: string) {
    if(!uri) {
      uri = await this._BarcodeService.scanWalletConnectQRCode();
      console.log(uri);
    }
    try {
      if(!address) {
        await this._WalletConnectProvider.init(uri);
      } else {
        await this._WalletConnectProvider.init(uri, address);
      }
      
      } catch(e) {
        console.log(e);
        alert(e);
      }
  }

  returnWallet(mnemonic: string) {
    try {
      var wallet = ethers.Wallet.fromMnemonic((mnemonic as any).phrase);
      return wallet;
    } catch(e) {
      try {
        var wallet = ethers.Wallet.fromMnemonic(mnemonic);
        return wallet;
      } catch(ee) {
        return false;
      }
    }
  }

 

  returnPOAPMintedNFT(address: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const testAddress = "0x301330402489B0aD7D01FD075D3e79dEdDaf73Fa";
      // axios.get(`https://frontend.poap.tech/actions/scan/${testAddress}`).then(res => {
        axios.get(`https://frontend.poap.tech/actions/scan/${address}`).then(res => {
        resolve(res.data);
      }).catch(e => {
        reject(e);
      })
    })
  }

  async returnEtherJSGasPrice(currency: string, from: string, to: string, amount: string) {

    const etherscanAPIKey = await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY);

    const polygon = environment.production ? CryptoNetworks.POLYGON : CryptoNetworks.POLYGON_TEST_MUMBAI;
    const evan = environment.production ? CryptoNetworks.EVAN : CryptoNetworks.EVAN_TEST;
    const ether = environment.production ? EthereumNetworks.MAINNET : EthereumNetworks.RINKEBY;

    const network = (currency == CryptoCurrency.MATIC) ? ethers.getDefaultProvider(polygon) : 
      ((currency == CryptoCurrency.EVE) ? ethers.getDefaultProvider(evan) : 
      ((currency == CryptoCurrency.GNOSIS) ? ethers.getDefaultProvider(CryptoNetworks.GNOSIS) : new ethers.providers.EtherscanProvider(ether, etherscanAPIKey)));
    
      var gasPrice = await network.estimateGas({
        from: from,
        to: to,
        value: ethers.utils.parseEther(amount.toString()),
        gasLimit: ethers.utils.hexlify(21000), // 100000,
        // gasPrice: ethers.utils.parseUnits('10', 'gwei'),
        nonce: await network.getTransactionCount(from, "latest"),
      })
    
    
    var gasPrice_GWEI = ethers.utils.formatUnits(gasPrice, "gwei");

    console.log(gasPrice);
    // console.log(gasPrice_GWEI);

    // return `${gasPrice_Eth} ${currency}`;
    return `${gasPrice_GWEI} ${currency}`;
  }

  presentToast(message: string) {
    this._ToastController.create({
      message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present())
  }

  async sendEtherJSTransaction(wallet: Wallet, from: string, to: string, amount: string, currency: string, gasPrice?: string) {
    console.log(`Sending ${amount} ${currency} to ${to}`);

    const etherscanAPIKey = await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY);

    const polygon = environment.production ? CryptoNetworks.POLYGON : CryptoNetworks.POLYGON_TEST_MUMBAI;
    const evan = environment.production ? CryptoNetworks.EVAN : CryptoNetworks.EVAN_TEST;
    const ether = environment.production ? EthereumNetworks.MAINNET : EthereumNetworks.RINKEBY;

    switch(currency) {
      case CryptoCurrency.ETH:            
        var network: any = new ethers.providers.EtherscanProvider(ether, etherscanAPIKey); 
        var wa2: any = wallet.connect(network);
        break;
      case CryptoCurrency.MATIC:          
        network = ethers.getDefaultProvider(polygon); 
        wa2 = wallet.connect(network);
        break;
      case CryptoCurrency.EVE:            
        network = ethers.getDefaultProvider(evan); 
        wa2 = wallet.connect(network);
        break;
      case CryptoCurrency.GNOSIS:         
        network = ethers.getDefaultProvider(CryptoNetworks.GNOSIS); 
        wa2 = wallet.connect(network);
        break;
      case CryptoCurrency.OP:             
        network = new ethers.providers.EtherscanProvider(ether, etherscanAPIKey); 
        wa2 = wallet.connect(network);
        break;
      case CryptoCurrency.HMT_POLYGON:    
        network = new ethers.providers.EtherscanProvider(ether, etherscanAPIKey); 
        var hmtContract = Contracts.HMT_POLYGON;
        var provider = CryptoNetworks.HMT_POLYGON;
        var contract = new ethers.Contract(hmtContract, abi);
        wa2 = contract.connect(ethers.getDefaultProvider(provider));
        break;
      case CryptoCurrency.HMT_SKALE:      
        network = new ethers.providers.EtherscanProvider(ether, etherscanAPIKey); 
        hmtContract = Contracts.HMT_SKALE;
        provider = CryptoNetworks.HMT_SKALE;
        contract = new ethers.Contract(hmtContract, abi);
        wa2  = contract.connect(ethers.getDefaultProvider(provider));
        break;
      case CryptoCurrency.THXC:      
        network = new ethers.providers.EtherscanProvider(ether, etherscanAPIKey); 
        hmtContract = Contracts.THXC
        provider = CryptoNetworks.THXC;
        contract = new ethers.Contract(hmtContract, abi);
        wa2  = contract.connect(ethers.getDefaultProvider(provider));
        break;
      default: 
        network = new ethers.providers.EtherscanProvider(ether, etherscanAPIKey); 
        wa2 = wallet.connect(network);
        break;
    }
    
    // const network = (currency == CryptoCurrency.MATIC) ? ethers.getDefaultProvider(polygon) : 
    //   ((currency == CryptoCurrency.EVE) ? ethers.getDefaultProvider(evan) : 
    //   ((currency == CryptoCurrency.GNOSIS) ? ethers.getDefaultProvider(CryptoNetworks.GNOSIS) : new ethers.providers.EtherscanProvider(ether, etherscanAPIKey)));
    
    // const wa2 = wallet.connect(network);

    console.log(gasPrice);
    var g = !!gasPrice ? ethers.utils.parseEther((Number(gasPrice)/GAS_USED[currency]).toFixed(10)) : await network.getGasPrice();
    console.log(g);

    var txObject = {
      from: from,
      to: to,
      value: ethers.utils.parseEther(amount.toString()),
      nonce: await network.getTransactionCount(from, "latest"),
      gasLimit: ethers.utils.hexlify(21000), // 100000
      gasPrice: g //await network.getGasPrice() 
    }
    
    console.log(txObject);

    var tx = null;
    
    try {
      tx = await wa2.sendTransaction(txObject);
    } catch(err) {
      console.log(err);
      try{
        var key = err.toString().slice(err.toString().indexOf("code=") + 5).split(",")[0];
        this.presentToast(this._Translate.instant(`ETHERRORS.${key}`));
      } catch(ef) {
        console.log(ef);
      }
      return null;
    }


    var tx_mod = Object.assign(tx, { timestamp: +new Date() });

    var cps = await this._SecureStorageService.getValue(SecureStorageKey.cryptoPaymentsSent, false);
    var cpsParse = !!cps ? JSON.parse(cps) : [];
    cpsParse.push(tx_mod);
    await this._SecureStorageService.setValue(SecureStorageKey.cryptoPaymentsSent, JSON.stringify(cpsParse));

    console.log(tx);
  
    return tx;

  }

  receiveTransaction(wallet: Wallet, from: string, amount: string, currency: string) {
    console.log(`Receiving ${amount} ${currency} from ${from}`);
  }

  async obtainTHXCtoETHPrice() {
    const etherscanAPIKey = await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY);

    const contractAddress = Contracts.THXC;
    const lpAddress = Contracts.LIQUIDITY;
    
    const liquidityabi = [{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"token0","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"name":"factory","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"getPair","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getReserves","outputs":[{"internalType":"uint112","name":"_reserve0","type":"uint112"},{"internalType":"uint112","name":"_reserve1","type":"uint112"},{"internalType":"uint32","name":"_blockTimestampLast","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"}];
    
    const provider = new ethers.providers.EtherscanProvider(EthereumNetworks.GOERLI, etherscanAPIKey);
    
    const thxcContract = new ethers.Contract(contractAddress, thxcabi, provider);
    const uniswapPairContract = new ethers.Contract(lpAddress, liquidityabi, provider);

    const getReserves = await uniswapPairContract.getReserves();
    const decimals = await thxcContract.decimals();

    const [
        reserve0, 
        reserve1,
        reserve2
    ] = getReserves;

    const tokenPrice = Number(reserve1) / Number(reserve0) * (10 ** Number(decimals)); 
    const inEth = ethers.utils.formatEther(Math.round(tokenPrice).toString());

    return Number(inEth);
  }

  fetchHistoricalPrices(crypto: string, target: string) {
    // Use this URL instead, this is what their official website uses: https://api.coinmarketcap.com/data-api/v3/cryptocurrency/detail/chart?id=1&range=1D
    // res.data.points
    const url = (environment.production) ? `https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/historical` : `https://sandbox-api.coinmarketcap.com/v1/cryptocurrency/quotes/historical`;
    
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.COINMARKETCAPAPI_KEY).then(api_key => {
        axios.get(url,{
          headers: {
            "X-CMC_PRO_API_KEY": api_key,
            "Accept": "application/json"
          },
          params: { 
            date: +new Date(),
            start: 1,
            limit: 100,
            convert: `${crypto},${target}`
           },
        })
          .then(res => resolve(res.data))
          .catch(err => reject(err));
      })
     
    })
  }
  

}
