import { Injectable, Injector, OnDestroy } from '@angular/core';
import { DatePipe, DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { interval, of, Subject, SubscriptionLike } from 'rxjs';
import { delay, filter, take, takeUntil } from 'rxjs/operators';
import { User } from '../../models/User';
import { PersonalInformation, SignUp } from '../../models/SignUp';
import { ApiProviderService } from '../api/api-provider.service';
import { StorageKeys, StorageProviderService } from '../storage/storage-provider.service';
import { UserProviderService } from '../user/user-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { KycService } from '../../../kyc/services/kyc.service';
import { AppStateService } from '../app-state/app-state.service';
import { Settings, SettingsService } from '../settings/settings.service';
import { ImageSelectionPayload } from '../../../shared/components/image-selection/image-selection.component';
import { TranslateService } from '@ngx-translate/core';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { UtilityService } from '../utillity/utility.service';
import { sortBy } from 'lodash';
import { NavController, Platform } from '@ionic/angular';
import { DeviceService } from '../device/device.service';
import { SQLStorageService } from '../sql/sqlStorage.service';
import { KycVendors } from '../../../kyc/enums/kyc-provider.enum';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { VaultSecretsServiceAkita } from '../state/vault-secrets/vault-secrets.service';
import { KycMediaServiceAkita } from '../state/kyc-media/kyc-media.service';
import { UserPhotoServiceAkita } from '../state/user-photo/user-photo.service';
import { WalletsService } from '../../../shared/state/wallets/wallets.service';


export enum CountryMode {
    STANDARD,
    DRIVERLICENSE,
    IDCARD,
    IDENTITY
}

export interface ProviderDocument {
    provider: KycVendors;
    documents: Array<string>;
}

@Injectable({
    providedIn: 'root'
})
export class SignProviderService implements OnDestroy {

    public registerSuccess = true;
    public personalInfoSuccess = true;
    public signUpForm: SignUp = {
        username: '',
        password: '',
        confirm_password: '',
        first_name: '',
        // middle_name: '',
        last_name: '',
        email_id: '',
        OTP: ''
    };

    public imageSelectionPayload: ImageSelectionPayload;

    public personalInformation: PersonalInformation = {
        gender: '',
        maritalstatus: '',
        date_of_birth: '',
        place_of_birth: '',
        province_of_birth: '',
        country_of_birth: '',
        citizenship: '',
        address_residence: '',
        street_number_residence: '',
        postal_code_residence: '',
        city_residence: '',
        province_residence: '',
        country_residence: '',
        passport_number: '',
        passport_issue_date: '',
        passport_expiry_date: '',
        passport_issue_country: '',
        dl_number: '',
        dl_issue_date: '',
        dl_expiry_date: '',
        dl_issue_country: '',
        idcard_number: '',
        idcard_issue_date: '',
        idcard_expiry_date: '',
        idcard_issue_country: '',
        residence_permit_number: '',
        residence_permit_issue_date: '',
        residence_permit_expiry_date: '',
        residence_permit_issue_country: '',
    };

    public countries: { country: string; code2: string; code3: string; countryTranslateKey: string; originTranslateKey: string; }[] = [
        {
            country: 'Afghanistan',
            code2: 'AF',
            code3: 'AFG',
            countryTranslateKey: 'COUNTRIES.NAME_AFG',
            originTranslateKey: 'COUNTRIES.ORIGIN_AFG'
        },
        {
            country: 'Aland Islands',
            code2: 'AX',
            code3: 'ALA',
            countryTranslateKey: 'COUNTRIES.NAME_ALA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ALA'
        },
        {
            country: 'Albania',
            code2: 'AL',
            code3: 'ALB',
            countryTranslateKey: 'COUNTRIES.NAME_ALB',
            originTranslateKey: 'COUNTRIES.ORIGIN_ALB'
        },
        {
            country: 'Algeria',
            code2: 'DZ',
            code3: 'DZA',
            countryTranslateKey: 'COUNTRIES.NAME_DZA',
            originTranslateKey: 'COUNTRIES.ORIGIN_DZA'
        },
        {
            country: 'American Samoa',
            code2: 'AS',
            code3: 'ASM',
            countryTranslateKey: 'COUNTRIES.NAME_ASM',
            originTranslateKey: 'COUNTRIES.ORIGIN_ASM'
        },
        {
            country: 'Andorra',
            code2: 'AD',
            code3: 'AND',
            countryTranslateKey: 'COUNTRIES.NAME_AND',
            originTranslateKey: 'COUNTRIES.ORIGIN_AND'
        },
        {
            country: 'Angola',
            code2: 'AO',
            code3: 'AGO',
            countryTranslateKey: 'COUNTRIES.NAME_AGO',
            originTranslateKey: 'COUNTRIES.ORIGIN_AGO'
        },
        {
            country: 'Anguilla',
            code2: 'AI',
            code3: 'AIA',
            countryTranslateKey: 'COUNTRIES.NAME_AIA',
            originTranslateKey: 'COUNTRIES.ORIGIN_AIA'
        },
        {
            country: 'Antarctica',
            code2: 'AQ',
            code3: 'ATA',
            countryTranslateKey: 'COUNTRIES.NAME_ATA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ATA'
        },
        {
            country: 'Antigua and Barbuda',
            code2: 'AG',
            code3: 'ATG',
            countryTranslateKey: 'COUNTRIES.NAME_ATG',
            originTranslateKey: 'COUNTRIES.ORIGIN_ATG'
        },
        {
            country: 'Argentina',
            code2: 'AR',
            code3: 'ARG',
            countryTranslateKey: 'COUNTRIES.NAME_ARG',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARG'
        },
        {
            country: 'Armenia',
            code2: 'AM',
            code3: 'ARM',
            countryTranslateKey: 'COUNTRIES.NAME_ARM',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARM'
        },
        {
            country: 'Aruba',
            code2: 'AW',
            code3: 'ABW',
            countryTranslateKey: 'COUNTRIES.NAME_ABW',
            originTranslateKey: 'COUNTRIES.ORIGIN_ABW'
        },
        {
            country: 'Australia',
            code2: 'AU',
            code3: 'AUS',
            countryTranslateKey: 'COUNTRIES.NAME_AUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_AUS'
        },
        {
            country: 'Austria',
            code2: 'AT',
            code3: 'AUT',
            countryTranslateKey: 'COUNTRIES.NAME_AUT',
            originTranslateKey: 'COUNTRIES.ORIGIN_AUT'
        },
        {
            country: 'Azerbaijan',
            code2: 'AZ',
            code3: 'AZE',
            countryTranslateKey: 'COUNTRIES.NAME_AZE',
            originTranslateKey: 'COUNTRIES.ORIGIN_AZE'
        },
        {
            country: 'Bahamas (the)',
            code2: 'BS',
            code3: 'BHS',
            countryTranslateKey: 'COUNTRIES.NAME_BHS',
            originTranslateKey: 'COUNTRIES.ORIGIN_BHS'
        },
        {
            country: 'Bahrain',
            code2: 'BH',
            code3: 'BHR',
            countryTranslateKey: 'COUNTRIES.NAME_BHR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BHR'
        },
        {
            country: 'Bangladesh',
            code2: 'BD',
            code3: 'BGD',
            countryTranslateKey: 'COUNTRIES.NAME_BGD',
            originTranslateKey: 'COUNTRIES.ORIGIN_BGD'
        },
        {
            country: 'Barbados',
            code2: 'BB',
            code3: 'BRB',
            countryTranslateKey: 'COUNTRIES.NAME_BRB',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRB'
        },
        {
            country: 'Belarus',
            code2: 'BY',
            code3: 'BLR',
            countryTranslateKey: 'COUNTRIES.NAME_BLR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLR'
        },
        {
            country: 'Belgium',
            code2: 'BE',
            code3: 'BEL',
            countryTranslateKey: 'COUNTRIES.NAME_BEL',
            originTranslateKey: 'COUNTRIES.ORIGIN_BEL'
        },
        {
            country: 'Belize',
            code2: 'BZ',
            code3: 'BLZ',
            countryTranslateKey: 'COUNTRIES.NAME_BLZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLZ'
        },
        {
            country: 'Benin',
            code2: 'BJ',
            code3: 'BEN',
            countryTranslateKey: 'COUNTRIES.NAME_BEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BEN'
        },
        {
            country: 'Bermuda',
            code2: 'BM',
            code3: 'BMU',
            countryTranslateKey: 'COUNTRIES.NAME_BMU',
            originTranslateKey: 'COUNTRIES.ORIGIN_BMU'
        },
        {
            country: 'Bhutan',
            code2: 'BT',
            code3: 'BTN',
            countryTranslateKey: 'COUNTRIES.NAME_BTN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BTN'
        },
        {
            country: 'Bolivia (Plurinational State of)',
            code2: 'BO',
            code3: 'BOL',
            countryTranslateKey: 'COUNTRIES.NAME_BOL',
            originTranslateKey: 'COUNTRIES.ORIGIN_BOL'
        },
        {
            country: 'Bonaire, Sint Eustatius and Saba',
            code2: 'BQ',
            code3: 'BES',
            countryTranslateKey: 'COUNTRIES.NAME_BES',
            originTranslateKey: 'COUNTRIES.ORIGIN_BES'
        },
        {
            country: 'Bosnia and Herzegovina',
            code2: 'BA',
            code3: 'BIH',
            countryTranslateKey: 'COUNTRIES.NAME_BIH',
            originTranslateKey: 'COUNTRIES.ORIGIN_BIH'
        },
        {
            country: 'Botswana',
            code2: 'BW',
            code3: 'BWA',
            countryTranslateKey: 'COUNTRIES.NAME_BWA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BWA'
        },
        {
            country: 'Bouvet Island',
            code2: 'BV',
            code3: 'BVT',
            countryTranslateKey: 'COUNTRIES.NAME_BVT',
            originTranslateKey: 'COUNTRIES.ORIGIN_BVT'
        },
        {
            country: 'Brazil',
            code2: 'BR',
            code3: 'BRA',
            countryTranslateKey: 'COUNTRIES.NAME_BRA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRA'
        },
        {
            country: 'British Indian Ocean Territory (the)',
            code2: 'IO',
            code3: 'IOT',
            countryTranslateKey: 'COUNTRIES.NAME_IOT',
            originTranslateKey: 'COUNTRIES.ORIGIN_IOT'
        },
        {
            country: 'Brunei Darussalam',
            code2: 'BN',
            code3: 'BRN',
            countryTranslateKey: 'COUNTRIES.NAME_BRN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRN'
        },
        {
            country: 'Bulgaria',
            code2: 'BG',
            code3: 'BGR',
            countryTranslateKey: 'COUNTRIES.NAME_BGR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BGR'
        },
        {
            country: 'Burkina Faso',
            code2: 'BF',
            code3: 'BFA',
            countryTranslateKey: 'COUNTRIES.NAME_BFA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BFA'
        },
        {
            country: 'Burundi',
            code2: 'BI',
            code3: 'BDI',
            countryTranslateKey: 'COUNTRIES.NAME_BDI',
            originTranslateKey: 'COUNTRIES.ORIGIN_BDI'
        },
        {
            country: 'Cabo Verde',
            code2: 'CV',
            code3: 'CPV',
            countryTranslateKey: 'COUNTRIES.NAME_CPV',
            originTranslateKey: 'COUNTRIES.ORIGIN_CPV'
        },
        {
            country: 'Cambodia',
            code2: 'KH',
            code3: 'KHM',
            countryTranslateKey: 'COUNTRIES.NAME_KHM',
            originTranslateKey: 'COUNTRIES.ORIGIN_KHM'
        },
        {
            country: 'Cameroon',
            code2: 'CM',
            code3: 'CMR',
            countryTranslateKey: 'COUNTRIES.NAME_CMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_CMR'
        },
        {
            country: 'Canada',
            code2: 'CA',
            code3: 'CAN',
            countryTranslateKey: 'COUNTRIES.NAME_CAN',
            originTranslateKey: 'COUNTRIES.ORIGIN_CAN'
        },
        {
            country: 'Cayman Islands (the)',
            code2: 'KY',
            code3: 'CYM',
            countryTranslateKey: 'COUNTRIES.NAME_CYM',
            originTranslateKey: 'COUNTRIES.ORIGIN_CYM'
        },
        {
            country: 'Central African Republic (the)',
            code2: 'CF',
            code3: 'CAF',
            countryTranslateKey: 'COUNTRIES.NAME_CAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_CAF'
        },
        {country: 'Chad', code2: 'TD', code3: 'TCD', countryTranslateKey: 'COUNTRIES.NAME_TCD', originTranslateKey: 'COUNTRIES.ORIGIN_TCD'},
        {
            country: 'Chile',
            code2: 'CL',
            code3: 'CHL',
            countryTranslateKey: 'COUNTRIES.NAME_CHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHL'
        },
        {
            country: 'China',
            code2: 'CN',
            code3: 'CHN',
            countryTranslateKey: 'COUNTRIES.NAME_CHN',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHN'
        },
        {
            country: 'Christmas Island',
            code2: 'CX',
            code3: 'CXR',
            countryTranslateKey: 'COUNTRIES.NAME_CXR',
            originTranslateKey: 'COUNTRIES.ORIGIN_CXR'
        },
        {
            country: 'Cocos (Keeling) Islands (the)',
            code2: 'CC',
            code3: 'CCK',
            countryTranslateKey: 'COUNTRIES.NAME_CCK',
            originTranslateKey: 'COUNTRIES.ORIGIN_CCK'
        },
        {
            country: 'Colombia',
            code2: 'CO',
            code3: 'COL',
            countryTranslateKey: 'COUNTRIES.NAME_COL',
            originTranslateKey: 'COUNTRIES.ORIGIN_COL'
        },
        {
            country: 'Comoros (the)',
            code2: 'KM',
            code3: 'COM',
            countryTranslateKey: 'COUNTRIES.NAME_COM',
            originTranslateKey: 'COUNTRIES.ORIGIN_COM'
        },
        {
            country: 'Congo (the Democratic Republic of the)',
            code2: 'CD',
            code3: 'COD',
            countryTranslateKey: 'COUNTRIES.NAME_COD',
            originTranslateKey: 'COUNTRIES.ORIGIN_COD'
        },
        {
            country: 'Congo (the)',
            code2: 'CG',
            code3: 'COG',
            countryTranslateKey: 'COUNTRIES.NAME_COG',
            originTranslateKey: 'COUNTRIES.ORIGIN_COG'
        },
        {
            country: 'Cook Islands (the)',
            code2: 'CK',
            code3: 'COK',
            countryTranslateKey: 'COUNTRIES.NAME_COK',
            originTranslateKey: 'COUNTRIES.ORIGIN_COK'
        },
        {
            country: 'Costa Rica',
            code2: 'CR',
            code3: 'CRI',
            countryTranslateKey: 'COUNTRIES.NAME_CRI',
            originTranslateKey: 'COUNTRIES.ORIGIN_CRI'
        },
        {
            country: 'Cote d`Ivoire',
            code2: 'CI',
            code3: 'CIV',
            countryTranslateKey: 'COUNTRIES.NAME_CIV',
            originTranslateKey: 'COUNTRIES.ORIGIN_CIV'
        },
        {
            country: 'Croatia',
            code2: 'HR',
            code3: 'HRV',
            countryTranslateKey: 'COUNTRIES.NAME_HRV',
            originTranslateKey: 'COUNTRIES.ORIGIN_HRV'
        },
        {country: 'Cuba', code2: 'CU', code3: 'CUB', countryTranslateKey: 'COUNTRIES.NAME_CUB', originTranslateKey: 'COUNTRIES.ORIGIN_CUB'},
        {
            country: 'Curacao',
            code2: 'CW',
            code3: 'CUW',
            countryTranslateKey: 'COUNTRIES.NAME_CUW',
            originTranslateKey: 'COUNTRIES.ORIGIN_CUW'
        },
        {
            country: 'Cyprus',
            code2: 'CY',
            code3: 'CYP',
            countryTranslateKey: 'COUNTRIES.NAME_CYP',
            originTranslateKey: 'COUNTRIES.ORIGIN_CYP'
        },
        {
            country: 'Czech Republic (the)',
            code2: 'CZ',
            code3: 'CZE',
            countryTranslateKey: 'COUNTRIES.NAME_CZE',
            originTranslateKey: 'COUNTRIES.ORIGIN_CZE'
        },
        {
            country: 'Denmark',
            code2: 'DK',
            code3: 'DNK',
            countryTranslateKey: 'COUNTRIES.NAME_DNK',
            originTranslateKey: 'COUNTRIES.ORIGIN_DNK'
        },
        {
            country: 'Djibouti',
            code2: 'DJ',
            code3: 'DJI',
            countryTranslateKey: 'COUNTRIES.NAME_DJI',
            originTranslateKey: 'COUNTRIES.ORIGIN_DJI'
        },
        {
            country: 'Dominica',
            code2: 'DM',
            code3: 'DMA',
            countryTranslateKey: 'COUNTRIES.NAME_DMA',
            originTranslateKey: 'COUNTRIES.ORIGIN_DMA'
        },
        {
            country: 'Dominican Republic (the)',
            code2: 'DO',
            code3: 'DOM',
            countryTranslateKey: 'COUNTRIES.NAME_DOM',
            originTranslateKey: 'COUNTRIES.ORIGIN_DOM'
        },
        {
            country: 'Ecuador',
            code2: 'EC',
            code3: 'ECU',
            countryTranslateKey: 'COUNTRIES.NAME_ECU',
            originTranslateKey: 'COUNTRIES.ORIGIN_ECU'
        },
        {
            country: 'Egypt',
            code2: 'EG',
            code3: 'EGY',
            countryTranslateKey: 'COUNTRIES.NAME_EGY',
            originTranslateKey: 'COUNTRIES.ORIGIN_EGY'
        },
        {
            country: 'El Salvador',
            code2: 'SV',
            code3: 'SLV',
            countryTranslateKey: 'COUNTRIES.NAME_SLV',
            originTranslateKey: 'COUNTRIES.ORIGIN_SLV'
        },
        {
            country: 'Equatorial Guinea',
            code2: 'GQ',
            code3: 'GNQ',
            countryTranslateKey: 'COUNTRIES.NAME_GNQ',
            originTranslateKey: 'COUNTRIES.ORIGIN_GNQ'
        },
        {
            country: 'Eritrea',
            code2: 'ER',
            code3: 'ERI',
            countryTranslateKey: 'COUNTRIES.NAME_ERI',
            originTranslateKey: 'COUNTRIES.ORIGIN_ERI'
        },
        {
            country: 'Estonia',
            code2: 'EE',
            code3: 'EST',
            countryTranslateKey: 'COUNTRIES.NAME_EST',
            originTranslateKey: 'COUNTRIES.ORIGIN_EST'
        },
        {
            country: 'Ethiopia',
            code2: 'ET',
            code3: 'ETH',
            countryTranslateKey: 'COUNTRIES.NAME_ETH',
            originTranslateKey: 'COUNTRIES.ORIGIN_ETH'
        },
        {
            country: 'Falkland Islands (the) [Malvinas]',
            code2: 'FK',
            code3: 'FLK',
            countryTranslateKey: 'COUNTRIES.NAME_FLK',
            originTranslateKey: 'COUNTRIES.ORIGIN_FLK'
        },
        {
            country: 'Faroe Islands (the)',
            code2: 'FO',
            code3: 'FRO',
            countryTranslateKey: 'COUNTRIES.NAME_FRO',
            originTranslateKey: 'COUNTRIES.ORIGIN_FRO'
        },
        {country: 'Fiji', code2: 'FJ', code3: 'FJI', countryTranslateKey: 'COUNTRIES.NAME_FJI', originTranslateKey: 'COUNTRIES.ORIGIN_FJI'},
        {
            country: 'Finland',
            code2: 'FI',
            code3: 'FIN',
            countryTranslateKey: 'COUNTRIES.NAME_FIN',
            originTranslateKey: 'COUNTRIES.ORIGIN_FIN'
        },
        {
            country: 'France',
            code2: 'FR',
            code3: 'FRA',
            countryTranslateKey: 'COUNTRIES.NAME_FRA',
            originTranslateKey: 'COUNTRIES.ORIGIN_FRA'
        },
        {
            country: 'French Guiana',
            code2: 'GF',
            code3: 'GUF',
            countryTranslateKey: 'COUNTRIES.NAME_GUF',
            originTranslateKey: 'COUNTRIES.ORIGIN_GUF'
        },
        {
            country: 'French Polynesia',
            code2: 'PF',
            code3: 'PYF',
            countryTranslateKey: 'COUNTRIES.NAME_PYF',
            originTranslateKey: 'COUNTRIES.ORIGIN_PYF'
        },
        {
            country: 'French Southern Territories (the)',
            code2: 'TF',
            code3: 'ATF',
            countryTranslateKey: 'COUNTRIES.NAME_ATF',
            originTranslateKey: 'COUNTRIES.ORIGIN_ATF'
        },
        {
            country: 'Gabon',
            code2: 'GA',
            code3: 'GAB',
            countryTranslateKey: 'COUNTRIES.NAME_GAB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GAB'
        },
        {
            country: 'Gambia (the)',
            code2: 'GM',
            code3: 'GMB',
            countryTranslateKey: 'COUNTRIES.NAME_GMB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GMB'
        },
        {
            country: 'Georgia',
            code2: 'GE',
            code3: 'GEO',
            countryTranslateKey: 'COUNTRIES.NAME_GEO',
            originTranslateKey: 'COUNTRIES.ORIGIN_GEO'
        },
        {
            country: 'Germany',
            code2: 'DE',
            code3: 'DEU',
            countryTranslateKey: 'COUNTRIES.NAME_DEU',
            originTranslateKey: 'COUNTRIES.ORIGIN_DEU'
        },
        {
            country: 'Ghana',
            code2: 'GH',
            code3: 'GHA',
            countryTranslateKey: 'COUNTRIES.NAME_GHA',
            originTranslateKey: 'COUNTRIES.ORIGIN_GHA'
        },
        {
            country: 'Gibraltar',
            code2: 'GI',
            code3: 'GIB',
            countryTranslateKey: 'COUNTRIES.NAME_GIB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GIB'
        },
        {
            country: 'Greece',
            code2: 'GR',
            code3: 'GRC',
            countryTranslateKey: 'COUNTRIES.NAME_GRC',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRC'
        },
        {
            country: 'Greenland',
            code2: 'GL',
            code3: 'GRL',
            countryTranslateKey: 'COUNTRIES.NAME_GRL',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRL'
        },
        {
            country: 'Grenada',
            code2: 'GD',
            code3: 'GRD',
            countryTranslateKey: 'COUNTRIES.NAME_GRD',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRD'
        },
        {
            country: 'Guadeloupe',
            code2: 'GP',
            code3: 'GLP',
            countryTranslateKey: 'COUNTRIES.NAME_GLP',
            originTranslateKey: 'COUNTRIES.ORIGIN_GLP'
        },
        {country: 'Guam', code2: 'GU', code3: 'GUM', countryTranslateKey: 'COUNTRIES.NAME_GUM', originTranslateKey: 'COUNTRIES.ORIGIN_GUM'},
        {
            country: 'Guatemala',
            code2: 'GT',
            code3: 'GTM',
            countryTranslateKey: 'COUNTRIES.NAME_GTM',
            originTranslateKey: 'COUNTRIES.ORIGIN_GTM'
        },
        {
            country: 'Guernsey',
            code2: 'GG',
            code3: 'GGY',
            countryTranslateKey: 'COUNTRIES.NAME_GGY',
            originTranslateKey: 'COUNTRIES.ORIGIN_GGY'
        },
        {
            country: 'Guinea',
            code2: 'GN',
            code3: 'GIN',
            countryTranslateKey: 'COUNTRIES.NAME_GIN',
            originTranslateKey: 'COUNTRIES.ORIGIN_GIN'
        },
        {
            country: 'Guinea-Bissau',
            code2: 'GW',
            code3: 'GNB',
            countryTranslateKey: 'COUNTRIES.NAME_GNB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GNB'
        },
        {
            country: 'Guyana',
            code2: 'GY',
            code3: 'GUY',
            countryTranslateKey: 'COUNTRIES.NAME_GUY',
            originTranslateKey: 'COUNTRIES.ORIGIN_GUY'
        },
        {
            country: 'Haiti',
            code2: 'HT',
            code3: 'HTI',
            countryTranslateKey: 'COUNTRIES.NAME_HTI',
            originTranslateKey: 'COUNTRIES.ORIGIN_HTI'
        },
        {
            country: 'Heard Island and McDonald Islands',
            code2: 'HM',
            code3: 'HMD',
            countryTranslateKey: 'COUNTRIES.NAME_HMD',
            originTranslateKey: 'COUNTRIES.ORIGIN_HMD'
        },
        {
            country: 'Holy See (the)',
            code2: 'VA',
            code3: 'VAT',
            countryTranslateKey: 'COUNTRIES.NAME_VAT',
            originTranslateKey: 'COUNTRIES.ORIGIN_VAT'
        },
        {
            country: 'Honduras',
            code2: 'HN',
            code3: 'HND',
            countryTranslateKey: 'COUNTRIES.NAME_HND',
            originTranslateKey: 'COUNTRIES.ORIGIN_HND'
        },
        {
            country: 'Hong Kong',
            code2: 'HK',
            code3: 'HKG',
            countryTranslateKey: 'COUNTRIES.NAME_HKG',
            originTranslateKey: 'COUNTRIES.ORIGIN_HKG'
        },
        {
            country: 'Hungary',
            code2: 'HU',
            code3: 'HUN',
            countryTranslateKey: 'COUNTRIES.NAME_HUN',
            originTranslateKey: 'COUNTRIES.ORIGIN_HUN'
        },
        {
            country: 'Iceland',
            code2: 'IS',
            code3: 'ISL',
            countryTranslateKey: 'COUNTRIES.NAME_ISL',
            originTranslateKey: 'COUNTRIES.ORIGIN_ISL'
        },
        {
            country: 'India',
            code2: 'IN',
            code3: 'IND',
            countryTranslateKey: 'COUNTRIES.NAME_IND',
            originTranslateKey: 'COUNTRIES.ORIGIN_IND'
        },
        {
            country: 'Indonesia',
            code2: 'ID',
            code3: 'IDN',
            countryTranslateKey: 'COUNTRIES.NAME_IDN',
            originTranslateKey: 'COUNTRIES.ORIGIN_IDN'
        },
        {
            country: 'Iran (Islamic Republic of)',
            code2: 'IR',
            code3: 'IRN',
            countryTranslateKey: 'COUNTRIES.NAME_IRN',
            originTranslateKey: 'COUNTRIES.ORIGIN_IRN'
        },
        {country: 'Iraq', code2: 'IQ', code3: 'IRQ', countryTranslateKey: 'COUNTRIES.NAME_IRQ', originTranslateKey: 'COUNTRIES.ORIGIN_IRQ'},
        {
            country: 'Ireland',
            code2: 'IE',
            code3: 'IRL',
            countryTranslateKey: 'COUNTRIES.NAME_IRL',
            originTranslateKey: 'COUNTRIES.ORIGIN_IRL'
        },
        {
            country: 'Isle of Man',
            code2: 'IM',
            code3: 'IMN',
            countryTranslateKey: 'COUNTRIES.NAME_IMN',
            originTranslateKey: 'COUNTRIES.ORIGIN_IMN'
        },
        {
            country: 'Israel',
            code2: 'IL',
            code3: 'ISR',
            countryTranslateKey: 'COUNTRIES.NAME_ISR',
            originTranslateKey: 'COUNTRIES.ORIGIN_ISR'
        },
        {
            country: 'Italy',
            code2: 'IT',
            code3: 'ITA',
            countryTranslateKey: 'COUNTRIES.NAME_ITA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ITA'
        },
        {
            country: 'Jamaica',
            code2: 'JM',
            code3: 'JAM',
            countryTranslateKey: 'COUNTRIES.NAME_JAM',
            originTranslateKey: 'COUNTRIES.ORIGIN_JAM'
        },
        {
            country: 'Japan',
            code2: 'JP',
            code3: 'JPN',
            countryTranslateKey: 'COUNTRIES.NAME_JPN',
            originTranslateKey: 'COUNTRIES.ORIGIN_JPN'
        },
        {
            country: 'Jersey',
            code2: 'JE',
            code3: 'JEY',
            countryTranslateKey: 'COUNTRIES.NAME_JEY',
            originTranslateKey: 'COUNTRIES.ORIGIN_JEY'
        },
        {
            country: 'Jordan',
            code2: 'JO',
            code3: 'JOR',
            countryTranslateKey: 'COUNTRIES.NAME_JOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_JOR'
        },
        {
            country: 'Kazakhstan',
            code2: 'KZ',
            code3: 'KAZ',
            countryTranslateKey: 'COUNTRIES.NAME_KAZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_KAZ'
        },
        {
            country: 'Kenya',
            code2: 'KE',
            code3: 'KEN',
            countryTranslateKey: 'COUNTRIES.NAME_KEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_KEN'
        },
        {
            country: 'Kiribati',
            code2: 'KI',
            code3: 'KIR',
            countryTranslateKey: 'COUNTRIES.NAME_KIR',
            originTranslateKey: 'COUNTRIES.ORIGIN_KIR'
        },
        {
            country: 'Korea (the Democratic People`s Republic of)',
            code2: 'KP',
            code3: 'PRK',
            countryTranslateKey: 'COUNTRIES.NAME_PRK',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRK'
        },
        {
            country: 'Korea (the Republic of)',
            code2: 'KR',
            code3: 'KOR',
            countryTranslateKey: 'COUNTRIES.NAME_KOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_KOR'
        },
        {
            country: 'Kosovo',
            code2: 'XK',
            code3: 'XKK',
            countryTranslateKey: 'COUNTRIES.NAME_XKK',
            originTranslateKey: 'COUNTRIES.ORIGIN_XKK'
        },
        {
            country: 'Kuwait',
            code2: 'KW',
            code3: 'KWT',
            countryTranslateKey: 'COUNTRIES.NAME_KWT',
            originTranslateKey: 'COUNTRIES.ORIGIN_KWT'
        },
        {
            country: 'Kyrgyzstan',
            code2: 'KG',
            code3: 'KGZ',
            countryTranslateKey: 'COUNTRIES.NAME_KGZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_KGZ'
        },
        {
            country: 'Lao People`s Democratic Republic',
            code2: 'LA',
            code3: 'LAO',
            countryTranslateKey: 'COUNTRIES.NAME_LAO',
            originTranslateKey: 'COUNTRIES.ORIGIN_LAO'
        },
        {
            country: 'Latvia',
            code2: 'LV',
            code3: 'LVA',
            countryTranslateKey: 'COUNTRIES.NAME_LVA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LVA'
        },
        {
            country: 'Lebanon',
            code2: 'LB',
            code3: 'LBN',
            countryTranslateKey: 'COUNTRIES.NAME_LBN',
            originTranslateKey: 'COUNTRIES.ORIGIN_LBN'
        },
        {
            country: 'Lesotho',
            code2: 'LS',
            code3: 'LSO',
            countryTranslateKey: 'COUNTRIES.NAME_LSO',
            originTranslateKey: 'COUNTRIES.ORIGIN_LSO'
        },
        {
            country: 'Liberia',
            code2: 'LR',
            code3: 'LBR',
            countryTranslateKey: 'COUNTRIES.NAME_LBR',
            originTranslateKey: 'COUNTRIES.ORIGIN_LBR'
        },
        {
            country: 'Libya',
            code2: 'LY',
            code3: 'LBY',
            countryTranslateKey: 'COUNTRIES.NAME_LBY',
            originTranslateKey: 'COUNTRIES.ORIGIN_LBY'
        },
        {
            country: 'Liechtenstein',
            code2: 'LI',
            code3: 'LIE',
            countryTranslateKey: 'COUNTRIES.NAME_LIE',
            originTranslateKey: 'COUNTRIES.ORIGIN_LIE'
        },
        {
            country: 'Lithuania',
            code2: 'LT',
            code3: 'LTU',
            countryTranslateKey: 'COUNTRIES.NAME_LTU',
            originTranslateKey: 'COUNTRIES.ORIGIN_LTU'
        },
        {
            country: 'Luxembourg',
            code2: 'LU',
            code3: 'LUX',
            countryTranslateKey: 'COUNTRIES.NAME_LUX',
            originTranslateKey: 'COUNTRIES.ORIGIN_LUX'
        },
        {
            country: 'Macao',
            code2: 'MO',
            code3: 'MAC',
            countryTranslateKey: 'COUNTRIES.NAME_MAC',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAC'
        },
        {
            country: 'Macedonia (the former Yugoslav Republic of)',
            code2: 'MK',
            code3: 'MKD',
            countryTranslateKey: 'COUNTRIES.NAME_MKD',
            originTranslateKey: 'COUNTRIES.ORIGIN_MKD'
        },
        {
            country: 'Madagascar',
            code2: 'MG',
            code3: 'MDG',
            countryTranslateKey: 'COUNTRIES.NAME_MDG',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDG'
        },
        {
            country: 'Malawi',
            code2: 'MW',
            code3: 'MWI',
            countryTranslateKey: 'COUNTRIES.NAME_MWI',
            originTranslateKey: 'COUNTRIES.ORIGIN_MWI'
        },
        {
            country: 'Malaysia',
            code2: 'MY',
            code3: 'MYS',
            countryTranslateKey: 'COUNTRIES.NAME_MYS',
            originTranslateKey: 'COUNTRIES.ORIGIN_MYS'
        },
        {
            country: 'Maldives',
            code2: 'MV',
            code3: 'MDV',
            countryTranslateKey: 'COUNTRIES.NAME_MDV',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDV'
        },
        {country: 'Mali', code2: 'ML', code3: 'MLI', countryTranslateKey: 'COUNTRIES.NAME_MLI', originTranslateKey: 'COUNTRIES.ORIGIN_MLI'},
        {
            country: 'Malta',
            code2: 'MT',
            code3: 'MLT',
            countryTranslateKey: 'COUNTRIES.NAME_MLT',
            originTranslateKey: 'COUNTRIES.ORIGIN_MLT'
        },
        {
            country: 'Marshall Islands (the)',
            code2: 'MH',
            code3: 'MHL',
            countryTranslateKey: 'COUNTRIES.NAME_MHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_MHL'
        },
        {
            country: 'Martinique',
            code2: 'MQ',
            code3: 'MTQ',
            countryTranslateKey: 'COUNTRIES.NAME_MTQ',
            originTranslateKey: 'COUNTRIES.ORIGIN_MTQ'
        },
        {
            country: 'Mauritania',
            code2: 'MR',
            code3: 'MRT',
            countryTranslateKey: 'COUNTRIES.NAME_MRT',
            originTranslateKey: 'COUNTRIES.ORIGIN_MRT'
        },
        {
            country: 'Mauritius',
            code2: 'MU',
            code3: 'MUS',
            countryTranslateKey: 'COUNTRIES.NAME_MUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_MUS'
        },
        {
            country: 'Mayotte',
            code2: 'YT',
            code3: 'MYT',
            countryTranslateKey: 'COUNTRIES.NAME_MYT',
            originTranslateKey: 'COUNTRIES.ORIGIN_MYT'
        },
        {
            country: 'Mexico',
            code2: 'MX',
            code3: 'MEX',
            countryTranslateKey: 'COUNTRIES.NAME_MEX',
            originTranslateKey: 'COUNTRIES.ORIGIN_MEX'
        },
        {
            country: 'Micronesia (Federated States of)',
            code2: 'FM',
            code3: 'FSM',
            countryTranslateKey: 'COUNTRIES.NAME_FSM',
            originTranslateKey: 'COUNTRIES.ORIGIN_FSM'
        },
        {
            country: 'Moldova (the Republic of)',
            code2: 'MD',
            code3: 'MDA',
            countryTranslateKey: 'COUNTRIES.NAME_MDA',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDA'
        },
        {
            country: 'Monaco',
            code2: 'MC',
            code3: 'MCO',
            countryTranslateKey: 'COUNTRIES.NAME_MCO',
            originTranslateKey: 'COUNTRIES.ORIGIN_MCO'
        },
        {
            country: 'Mongolia',
            code2: 'MN',
            code3: 'MNG',
            countryTranslateKey: 'COUNTRIES.NAME_MNG',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNG'
        },
        {
            country: 'Montenegro',
            code2: 'ME',
            code3: 'MNE',
            countryTranslateKey: 'COUNTRIES.NAME_MNE',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNE'
        },
        {
            country: 'Montserrat',
            code2: 'MS',
            code3: 'MSR',
            countryTranslateKey: 'COUNTRIES.NAME_MSR',
            originTranslateKey: 'COUNTRIES.ORIGIN_MSR'
        },
        {
            country: 'Morocco',
            code2: 'MA',
            code3: 'MAR',
            countryTranslateKey: 'COUNTRIES.NAME_MAR',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAR'
        },
        {
            country: 'Mozambique',
            code2: 'MZ',
            code3: 'MOZ',
            countryTranslateKey: 'COUNTRIES.NAME_MOZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_MOZ'
        },
        {
            country: 'Myanmar',
            code2: 'MM',
            code3: 'MMR',
            countryTranslateKey: 'COUNTRIES.NAME_MMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_MMR'
        },
        {
            country: 'Namibia',
            code2: 'NA',
            code3: 'NAM',
            countryTranslateKey: 'COUNTRIES.NAME_NAM',
            originTranslateKey: 'COUNTRIES.ORIGIN_NAM'
        },
        {
            country: 'Nauru',
            code2: 'NR',
            code3: 'NRU',
            countryTranslateKey: 'COUNTRIES.NAME_NRU',
            originTranslateKey: 'COUNTRIES.ORIGIN_NRU'
        },
        {
            country: 'Nepal',
            code2: 'NP',
            code3: 'NPL',
            countryTranslateKey: 'COUNTRIES.NAME_NPL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NPL'
        },
        {
            country: 'Netherlands (the)',
            code2: 'NL',
            code3: 'NLD',
            countryTranslateKey: 'COUNTRIES.NAME_NLD',
            originTranslateKey: 'COUNTRIES.ORIGIN_NLD'
        },
        {
            country: 'New Caledonia',
            code2: 'NC',
            code3: 'NCL',
            countryTranslateKey: 'COUNTRIES.NAME_NCL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NCL'
        },
        {
            country: 'New Zealand',
            code2: 'NZ',
            code3: 'NZL',
            countryTranslateKey: 'COUNTRIES.NAME_NZL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NZL'
        },
        {
            country: 'Nicaragua',
            code2: 'NI',
            code3: 'NIC',
            countryTranslateKey: 'COUNTRIES.NAME_NIC',
            originTranslateKey: 'COUNTRIES.ORIGIN_NIC'
        },
        {
            country: 'Niger (the)',
            code2: 'NE',
            code3: 'NER',
            countryTranslateKey: 'COUNTRIES.NAME_NER',
            originTranslateKey: 'COUNTRIES.ORIGIN_NER'
        },
        {
            country: 'Nigeria',
            code2: 'NG',
            code3: 'NGA',
            countryTranslateKey: 'COUNTRIES.NAME_NGA',
            originTranslateKey: 'COUNTRIES.ORIGIN_NGA'
        },
        {country: 'Niue', code2: 'NU', code3: 'NIU', countryTranslateKey: 'COUNTRIES.NAME_NIU', originTranslateKey: 'COUNTRIES.ORIGIN_NIU'},
        {
            country: 'Norfolk Island',
            code2: 'NF',
            code3: 'NFK',
            countryTranslateKey: 'COUNTRIES.NAME_NFK',
            originTranslateKey: 'COUNTRIES.ORIGIN_NFK'
        },
        {
            country: 'Northern Mariana Islands (the)',
            code2: 'MP',
            code3: 'MNP',
            countryTranslateKey: 'COUNTRIES.NAME_MNP',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNP'
        },
        {
            country: 'Norway',
            code2: 'NO',
            code3: 'NOR',
            countryTranslateKey: 'COUNTRIES.NAME_NOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_NOR'
        },
        {country: 'Oman', code2: 'OM', code3: 'OMN', countryTranslateKey: 'COUNTRIES.NAME_OMN', originTranslateKey: 'COUNTRIES.ORIGIN_OMN'},
        {
            country: 'Pakistan',
            code2: 'PK',
            code3: 'PAK',
            countryTranslateKey: 'COUNTRIES.NAME_PAK',
            originTranslateKey: 'COUNTRIES.ORIGIN_PAK'
        },
        {
            country: 'Palau',
            code2: 'PW',
            code3: 'PLW',
            countryTranslateKey: 'COUNTRIES.NAME_PLW',
            originTranslateKey: 'COUNTRIES.ORIGIN_PLW'
        },
        {
            country: 'Palestine, State of',
            code2: 'PS',
            code3: 'PSE',
            countryTranslateKey: 'COUNTRIES.NAME_PSE',
            originTranslateKey: 'COUNTRIES.ORIGIN_PSE'
        },
        {
            country: 'Panama',
            code2: 'PA',
            code3: 'PAN',
            countryTranslateKey: 'COUNTRIES.NAME_PAN',
            originTranslateKey: 'COUNTRIES.ORIGIN_PAN'
        },
        {
            country: 'Papua New Guinea',
            code2: 'PG',
            code3: 'PNG',
            countryTranslateKey: 'COUNTRIES.NAME_PNG',
            originTranslateKey: 'COUNTRIES.ORIGIN_PNG'
        },
        {
            country: 'Paraguay',
            code2: 'PY',
            code3: 'PRY',
            countryTranslateKey: 'COUNTRIES.NAME_PRY',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRY'
        },
        {country: 'Peru', code2: 'PE', code3: 'PER', countryTranslateKey: 'COUNTRIES.NAME_PER', originTranslateKey: 'COUNTRIES.ORIGIN_PER'},
        {
            country: 'Philippines (the)',
            code2: 'PH',
            code3: 'PHL',
            countryTranslateKey: 'COUNTRIES.NAME_PHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_PHL'
        },
        {
            country: 'Pitcairn',
            code2: 'PN',
            code3: 'PCN',
            countryTranslateKey: 'COUNTRIES.NAME_PCN',
            originTranslateKey: 'COUNTRIES.ORIGIN_PCN'
        },
        {
            country: 'Poland',
            code2: 'PL',
            code3: 'POL',
            countryTranslateKey: 'COUNTRIES.NAME_POL',
            originTranslateKey: 'COUNTRIES.ORIGIN_POL'
        },
        {
            country: 'Portugal',
            code2: 'PT',
            code3: 'PRT',
            countryTranslateKey: 'COUNTRIES.NAME_PRT',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRT'
        },
        {
            country: 'Puerto Rico',
            code2: 'PR',
            code3: 'PRI',
            countryTranslateKey: 'COUNTRIES.NAME_PRI',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRI'
        },
        {
            country: 'Qatar',
            code2: 'QA',
            code3: 'QAT',
            countryTranslateKey: 'COUNTRIES.NAME_QAT',
            originTranslateKey: 'COUNTRIES.ORIGIN_QAT'
        },
        {
            country: 'Réunion',
            code2: 'RE',
            code3: 'REU',
            countryTranslateKey: 'COUNTRIES.NAME_REU',
            originTranslateKey: 'COUNTRIES.ORIGIN_REU'
        },
        {
            country: 'Romania',
            code2: 'RO',
            code3: 'ROU',
            countryTranslateKey: 'COUNTRIES.NAME_ROU',
            originTranslateKey: 'COUNTRIES.ORIGIN_ROU'
        },
        {
            country: 'Russian Federation (the)',
            code2: 'RU',
            code3: 'RUS',
            countryTranslateKey: 'COUNTRIES.NAME_RUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_RUS'
        },
        {
            country: 'Rwanda',
            code2: 'RW',
            code3: 'RWA',
            countryTranslateKey: 'COUNTRIES.NAME_RWA',
            originTranslateKey: 'COUNTRIES.ORIGIN_RWA'
        },
        {
            country: 'Saint Barthélemy',
            code2: 'BL',
            code3: 'BLM',
            countryTranslateKey: 'COUNTRIES.NAME_BLM',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLM'
        },
        {
            country: 'Saint Helena, Ascension and Tristan da Cunha',
            code2: 'SH',
            code3: 'SHN',
            countryTranslateKey: 'COUNTRIES.NAME_SHN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SHN'
        },
        {
            country: 'Saint Kitts and Nevis',
            code2: 'KN',
            code3: 'KNA',
            countryTranslateKey: 'COUNTRIES.NAME_KNA',
            originTranslateKey: 'COUNTRIES.ORIGIN_KNA'
        },
        {
            country: 'Saint Lucia',
            code2: 'LC',
            code3: 'LCA',
            countryTranslateKey: 'COUNTRIES.NAME_LCA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LCA'
        },
        {
            country: 'Saint Martin (French part)',
            code2: 'MF',
            code3: 'MAF',
            countryTranslateKey: 'COUNTRIES.NAME_MAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAF'
        },
        {
            country: 'Saint Pierre and Miquelon',
            code2: 'PM',
            code3: 'SPM',
            countryTranslateKey: 'COUNTRIES.NAME_SPM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SPM'
        },
        {
            country: 'Saint Vincent and the Grenadines',
            code2: 'VC',
            code3: 'VCT',
            countryTranslateKey: 'COUNTRIES.NAME_VCT',
            originTranslateKey: 'COUNTRIES.ORIGIN_VCT'
        },
        {
            country: 'Samoa',
            code2: 'WS',
            code3: 'WSM',
            countryTranslateKey: 'COUNTRIES.NAME_WSM',
            originTranslateKey: 'COUNTRIES.ORIGIN_WSM'
        },
        {
            country: 'San Marino',
            code2: 'SM',
            code3: 'SMR',
            countryTranslateKey: 'COUNTRIES.NAME_SMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_SMR'
        },
        {
            country: 'Sao Tome and Principe',
            code2: 'ST',
            code3: 'STP',
            countryTranslateKey: 'COUNTRIES.NAME_STP',
            originTranslateKey: 'COUNTRIES.ORIGIN_STP'
        },
        {
            country: 'Saudi Arabia',
            code2: 'SA',
            code3: 'SAU',
            countryTranslateKey: 'COUNTRIES.NAME_SAU',
            originTranslateKey: 'COUNTRIES.ORIGIN_SAU'
        },
        {
            country: 'Senegal',
            code2: 'SN',
            code3: 'SEN',
            countryTranslateKey: 'COUNTRIES.NAME_SEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SEN'
        },
        {
            country: 'Serbia',
            code2: 'RS',
            code3: 'SRB',
            countryTranslateKey: 'COUNTRIES.NAME_SRB',
            originTranslateKey: 'COUNTRIES.ORIGIN_SRB'
        },
        {
            country: 'Seychelles',
            code2: 'SC',
            code3: 'SYC',
            countryTranslateKey: 'COUNTRIES.NAME_SYC',
            originTranslateKey: 'COUNTRIES.ORIGIN_SYC'
        },
        {
            country: 'Sierra Leone',
            code2: 'SL',
            code3: 'SLE',
            countryTranslateKey: 'COUNTRIES.NAME_SLE',
            originTranslateKey: 'COUNTRIES.ORIGIN_SLE'
        },
        {
            country: 'Singapore',
            code2: 'SG',
            code3: 'SGP',
            countryTranslateKey: 'COUNTRIES.NAME_SGP',
            originTranslateKey: 'COUNTRIES.ORIGIN_SGP'
        },
        {
            country: 'Sint Maarten (Dutch part)',
            code2: 'SX',
            code3: 'SXM',
            countryTranslateKey: 'COUNTRIES.NAME_SXM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SXM'
        },
        {
            country: 'Slovakia',
            code2: 'SK',
            code3: 'SVK',
            countryTranslateKey: 'COUNTRIES.NAME_SVK',
            originTranslateKey: 'COUNTRIES.ORIGIN_SVK'
        },
        {
            country: 'Slovenia',
            code2: 'SI',
            code3: 'SVN',
            countryTranslateKey: 'COUNTRIES.NAME_SVN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SVN'
        },
        {
            country: 'Solomon Islands',
            code2: 'SB',
            code3: 'SLB',
            countryTranslateKey: 'COUNTRIES.NAME_SLB',
            originTranslateKey: 'COUNTRIES.ORIGIN_SLB'
        },
        {
            country: 'Somalia',
            code2: 'SO',
            code3: 'SOM',
            countryTranslateKey: 'COUNTRIES.NAME_SOM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SOM'
        },
        {
            country: 'South Africa',
            code2: 'ZA',
            code3: 'ZAF',
            countryTranslateKey: 'COUNTRIES.NAME_ZAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_ZAF'
        },
        {
            country: 'South Georgia and the South Sandwich Islands',
            code2: 'GS',
            code3: 'SGS',
            countryTranslateKey: 'COUNTRIES.NAME_SGS',
            originTranslateKey: 'COUNTRIES.ORIGIN_SGS'
        },
        {
            country: 'South Sudan',
            code2: 'SS',
            code3: 'SSD',
            countryTranslateKey: 'COUNTRIES.NAME_SSD',
            originTranslateKey: 'COUNTRIES.ORIGIN_SSD'
        },
        {
            country: 'Spain',
            code2: 'ES',
            code3: 'ESP',
            countryTranslateKey: 'COUNTRIES.NAME_ESP',
            originTranslateKey: 'COUNTRIES.ORIGIN_ESP'
        },
        {
            country: 'Sri Lanka',
            code2: 'LK',
            code3: 'LKA',
            countryTranslateKey: 'COUNTRIES.NAME_LKA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LKA'
        },
        {
            country: 'Sudan (the)',
            code2: 'SD',
            code3: 'SDN',
            countryTranslateKey: 'COUNTRIES.NAME_SDN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SDN'
        },
        {
            country: 'Suriname',
            code2: 'SR',
            code3: 'SUR',
            countryTranslateKey: 'COUNTRIES.NAME_SUR',
            originTranslateKey: 'COUNTRIES.ORIGIN_SUR'
        },
        {
            country: 'Svalbard and Jan Mayen',
            code2: 'SJ',
            code3: 'SJM',
            countryTranslateKey: 'COUNTRIES.NAME_SJM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SJM'
        },
        {
            country: 'Swaziland',
            code2: 'SZ',
            code3: 'SWZ',
            countryTranslateKey: 'COUNTRIES.NAME_SWZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_SWZ'
        },
        {
            country: 'Sweden',
            code2: 'SE',
            code3: 'SWE',
            countryTranslateKey: 'COUNTRIES.NAME_SWE',
            originTranslateKey: 'COUNTRIES.ORIGIN_SWE'
        },
        {
            country: 'Switzerland',
            code2: 'CH',
            code3: 'CHE',
            countryTranslateKey: 'COUNTRIES.NAME_CHE',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHE'
        },
        {
            country: 'Syrian Arab Republic',
            code2: 'SY',
            code3: 'SYR',
            countryTranslateKey: 'COUNTRIES.NAME_SYR',
            originTranslateKey: 'COUNTRIES.ORIGIN_SYR'
        },
        {
            country: 'Taiwan (Province of China)',
            code2: 'TW',
            code3: 'TWN',
            countryTranslateKey: 'COUNTRIES.NAME_TWN',
            originTranslateKey: 'COUNTRIES.ORIGIN_TWN'
        },
        {
            country: 'Tajikistan',
            code2: 'TJ',
            code3: 'TJK',
            countryTranslateKey: 'COUNTRIES.NAME_TJK',
            originTranslateKey: 'COUNTRIES.ORIGIN_TJK'
        },
        {
            country: 'Tanzania, United Republic of',
            code2: 'TZ',
            code3: 'TZA',
            countryTranslateKey: 'COUNTRIES.NAME_TZA',
            originTranslateKey: 'COUNTRIES.ORIGIN_TZA'
        },
        {
            country: 'Thailand',
            code2: 'TH',
            code3: 'THA',
            countryTranslateKey: 'COUNTRIES.NAME_THA',
            originTranslateKey: 'COUNTRIES.ORIGIN_THA'
        },
        {
            country: 'Timor-Leste',
            code2: 'TL',
            code3: 'TLS',
            countryTranslateKey: 'COUNTRIES.NAME_TLS',
            originTranslateKey: 'COUNTRIES.ORIGIN_TLS'
        },
        {country: 'Togo', code2: 'TG', code3: 'TGO', countryTranslateKey: 'COUNTRIES.NAME_TGO', originTranslateKey: 'COUNTRIES.ORIGIN_TGO'},
        {
            country: 'Tokelau',
            code2: 'TK',
            code3: 'TKL',
            countryTranslateKey: 'COUNTRIES.NAME_TKL',
            originTranslateKey: 'COUNTRIES.ORIGIN_TKL'
        },
        {
            country: 'Tonga',
            code2: 'TO',
            code3: 'TON',
            countryTranslateKey: 'COUNTRIES.NAME_TON',
            originTranslateKey: 'COUNTRIES.ORIGIN_TON'
        },
        {
            country: 'Trinidad and Tobago',
            code2: 'TT',
            code3: 'TTO',
            countryTranslateKey: 'COUNTRIES.NAME_TTO',
            originTranslateKey: 'COUNTRIES.ORIGIN_TTO'
        },
        {
            country: 'Tunisia',
            code2: 'TN',
            code3: 'TUN',
            countryTranslateKey: 'COUNTRIES.NAME_TUN',
            originTranslateKey: 'COUNTRIES.ORIGIN_TUN'
        },
        {
            country: 'Turkey',
            code2: 'TR',
            code3: 'TUR',
            countryTranslateKey: 'COUNTRIES.NAME_TUR',
            originTranslateKey: 'COUNTRIES.ORIGIN_TUR'
        },
        {
            country: 'Turkmenistan',
            code2: 'TM',
            code3: 'TKM',
            countryTranslateKey: 'COUNTRIES.NAME_TKM',
            originTranslateKey: 'COUNTRIES.ORIGIN_TKM'
        },
        {
            country: 'Turks and Caicos Islands (the)',
            code2: 'TC',
            code3: 'TCA',
            countryTranslateKey: 'COUNTRIES.NAME_TCA',
            originTranslateKey: 'COUNTRIES.ORIGIN_TCA'
        },
        {
            country: 'Tuvalu',
            code2: 'TV',
            code3: 'TUV',
            countryTranslateKey: 'COUNTRIES.NAME_TUV',
            originTranslateKey: 'COUNTRIES.ORIGIN_TUV'
        },
        {
            country: 'Uganda',
            code2: 'UG',
            code3: 'UGA',
            countryTranslateKey: 'COUNTRIES.NAME_UGA',
            originTranslateKey: 'COUNTRIES.ORIGIN_UGA'
        },
        {
            country: 'Ukraine',
            code2: 'UA',
            code3: 'UKR',
            countryTranslateKey: 'COUNTRIES.NAME_UKR',
            originTranslateKey: 'COUNTRIES.ORIGIN_UKR'
        },
        {
            country: 'United Arab Emirates (the)',
            code2: 'AE',
            code3: 'ARE',
            countryTranslateKey: 'COUNTRIES.NAME_ARE',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARE'
        },
        {
            country: 'United Kingdom of Great Britain and Northern Ireland (the)',
            code2: 'GB',
            code3: 'GBR',
            countryTranslateKey: 'COUNTRIES.NAME_GBR',
            originTranslateKey: 'COUNTRIES.ORIGIN_GBR'
        },
        {
            country: 'United States Minor Outlying Islands (the)',
            code2: 'UM',
            code3: 'UMI',
            countryTranslateKey: 'COUNTRIES.NAME_UMI',
            originTranslateKey: 'COUNTRIES.ORIGIN_UMI'
        },
        {
            country: 'United States of America (the)',
            code2: 'US',
            code3: 'USA',
            countryTranslateKey: 'COUNTRIES.NAME_USA',
            originTranslateKey: 'COUNTRIES.ORIGIN_USA'
        },
        {
            country: 'Uruguay',
            code2: 'UY',
            code3: 'URY',
            countryTranslateKey: 'COUNTRIES.NAME_URY',
            originTranslateKey: 'COUNTRIES.ORIGIN_URY'
        },
        {
            country: 'Uzbekistan',
            code2: 'UZ',
            code3: 'UZB',
            countryTranslateKey: 'COUNTRIES.NAME_UZB',
            originTranslateKey: 'COUNTRIES.ORIGIN_UZB'
        },
        {
            country: 'Vanuatu',
            code2: 'VU',
            code3: 'VUT',
            countryTranslateKey: 'COUNTRIES.NAME_VUT',
            originTranslateKey: 'COUNTRIES.ORIGIN_VUT'
        },
        {
            country: 'Venezuela (Bolivarian Republic of)',
            code2: 'VE',
            code3: 'VEN',
            countryTranslateKey: 'COUNTRIES.NAME_VEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_VEN'
        },
        {
            country: 'Viet Nam',
            code2: 'VN',
            code3: 'VNM',
            countryTranslateKey: 'COUNTRIES.NAME_VNM',
            originTranslateKey: 'COUNTRIES.ORIGIN_VNM'
        },
        {
            country: 'Virgin Islands (British)',
            code2: 'VG',
            code3: 'VGB',
            countryTranslateKey: 'COUNTRIES.NAME_VGB',
            originTranslateKey: 'COUNTRIES.ORIGIN_VGB'
        },
        {
            country: 'Virgin Islands (U.S.)',
            code2: 'VI',
            code3: 'VIR',
            countryTranslateKey: 'COUNTRIES.NAME_VIR',
            originTranslateKey: 'COUNTRIES.ORIGIN_VIR'
        },
        {
            country: 'Wallis and Futuna',
            code2: 'WF',
            code3: 'WLF',
            countryTranslateKey: 'COUNTRIES.NAME_WLF',
            originTranslateKey: 'COUNTRIES.ORIGIN_WLF'
        },
        {
            country: 'Western Sahara',
            code2: 'EH',
            code3: 'ESH',
            countryTranslateKey: 'COUNTRIES.NAME_ESH',
            originTranslateKey: 'COUNTRIES.ORIGIN_ESH'
        },
        {
            country: 'Yemen',
            code2: 'YE',
            code3: 'YEM',
            countryTranslateKey: 'COUNTRIES.NAME_YEM',
            originTranslateKey: 'COUNTRIES.ORIGIN_YEM'
        },
        {
            country: 'Zambia',
            code2: 'ZM',
            code3: 'ZMB',
            countryTranslateKey: 'COUNTRIES.NAME_ZMB',
            originTranslateKey: 'COUNTRIES.ORIGIN_ZMB'
        },
        {
            country: 'Zimbabwe',
            code2: 'ZW',
            code3: 'ZWE',
            countryTranslateKey: 'COUNTRIES.NAME_ZWE',
            originTranslateKey: 'COUNTRIES.ORIGIN_ZWE'
        }
    ];

    public idCardSupportedCountries: { country: string; code2: string; code3: string; countryTranslateKey: string; originTranslateKey: string; }[] = [
        // {  country: 'Afghanistan',  code2: 'AF',  code3: 'AFG',  countryTranslateKey: 'COUNTRIES.NAME_AFG',  originTranslateKey: 'COUNTRIES.ORIGIN_AFG' },
        {
            country: 'Aland Islands',
            code2: 'AX',
            code3: 'ALA',
            countryTranslateKey: 'COUNTRIES.NAME_ALA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ALA'
        },
        {
            country: 'Albania',
            code2: 'AL',
            code3: 'ALB',
            countryTranslateKey: 'COUNTRIES.NAME_ALB',
            originTranslateKey: 'COUNTRIES.ORIGIN_ALB'
        },
        {
            country: 'Algeria',
            code2: 'DZ',
            code3: 'DZA',
            countryTranslateKey: 'COUNTRIES.NAME_DZA',
            originTranslateKey: 'COUNTRIES.ORIGIN_DZA'
        },
        {
            country: 'American Samoa',
            code2: 'AS',
            code3: 'ASM',
            countryTranslateKey: 'COUNTRIES.NAME_ASM',
            originTranslateKey: 'COUNTRIES.ORIGIN_ASM'
        },
        // {  country: 'Andorra',  code2: 'AD',  code3: 'AND',  countryTranslateKey: 'COUNTRIES.NAME_AND',  originTranslateKey: 'COUNTRIES.ORIGIN_AND'},
        {
            country: 'Angola',
            code2: 'AO',
            code3: 'AGO',
            countryTranslateKey: 'COUNTRIES.NAME_AGO',
            originTranslateKey: 'COUNTRIES.ORIGIN_AGO'
        },
        // {  country: 'Anguilla',  code2: 'AI',  code3: 'AIA',  countryTranslateKey: 'COUNTRIES.NAME_AIA',  originTranslateKey: 'COUNTRIES.ORIGIN_AIA'},
        {
            country: 'Antarctica',
            code2: 'AQ',
            code3: 'ATA',
            countryTranslateKey: 'COUNTRIES.NAME_ATA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ATA'
        },
        {
            country: 'Antigua and Barbuda',
            code2: 'AG',
            code3: 'ATG',
            countryTranslateKey: 'COUNTRIES.NAME_ATG',
            originTranslateKey: 'COUNTRIES.ORIGIN_ATG'
        },
        {
            country: 'Argentina',
            code2: 'AR',
            code3: 'ARG',
            countryTranslateKey: 'COUNTRIES.NAME_ARG',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARG'
        },
        {
            country: 'Armenia',
            code2: 'AM',
            code3: 'ARM',
            countryTranslateKey: 'COUNTRIES.NAME_ARM',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARM'
        },
        {
            country: 'Aruba',
            code2: 'AW',
            code3: 'ABW',
            countryTranslateKey: 'COUNTRIES.NAME_ABW',
            originTranslateKey: 'COUNTRIES.ORIGIN_ABW'
        },
        {
            country: 'Australia',
            code2: 'AU',
            code3: 'AUS',
            countryTranslateKey: 'COUNTRIES.NAME_AUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_AUS'
        },
        {
            country: 'Austria',
            code2: 'AT',
            code3: 'AUT',
            countryTranslateKey: 'COUNTRIES.NAME_AUT',
            originTranslateKey: 'COUNTRIES.ORIGIN_AUT'
        },
        {
            country: 'Azerbaijan',
            code2: 'AZ',
            code3: 'AZE',
            countryTranslateKey: 'COUNTRIES.NAME_AZE',
            originTranslateKey: 'COUNTRIES.ORIGIN_AZE'
        },
        // {  country: 'Bahamas (the)',  code2: 'BS',  code3: 'BHS',  countryTranslateKey: 'COUNTRIES.NAME_BHS',  originTranslateKey: 'COUNTRIES.ORIGIN_BHS'},
        {
            country: 'Bahrain',
            code2: 'BH',
            code3: 'BHR',
            countryTranslateKey: 'COUNTRIES.NAME_BHR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BHR'
        },
        {
            country: 'Bangladesh',
            code2: 'BD',
            code3: 'BGD',
            countryTranslateKey: 'COUNTRIES.NAME_BGD',
            originTranslateKey: 'COUNTRIES.ORIGIN_BGD'
        },
        {
            country: 'Barbados',
            code2: 'BB',
            code3: 'BRB',
            countryTranslateKey: 'COUNTRIES.NAME_BRB',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRB'
        },
        // {  country: 'Belarus',  code2: 'BY',  code3: 'BLR',  countryTranslateKey: 'COUNTRIES.NAME_BLR',  originTranslateKey: 'COUNTRIES.ORIGIN_BLR'},
        {
            country: 'Belgium',
            code2: 'BE',
            code3: 'BEL',
            countryTranslateKey: 'COUNTRIES.NAME_BEL',
            originTranslateKey: 'COUNTRIES.ORIGIN_BEL'
        },
        {
            country: 'Belize',
            code2: 'BZ',
            code3: 'BLZ',
            countryTranslateKey: 'COUNTRIES.NAME_BLZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLZ'
        },
        {
            country: 'Benin',
            code2: 'BJ',
            code3: 'BEN',
            countryTranslateKey: 'COUNTRIES.NAME_BEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BEN'
        },
        // {  country: 'Bermuda',  code2: 'BM',  code3: 'BMU',  countryTranslateKey: 'COUNTRIES.NAME_BMU',  originTranslateKey: 'COUNTRIES.ORIGIN_BMU'},
        {
            country: 'Bhutan',
            code2: 'BT',
            code3: 'BTN',
            countryTranslateKey: 'COUNTRIES.NAME_BTN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BTN'
        },
        {
            country: 'Bolivia (Plurinational State of)',
            code2: 'BO',
            code3: 'BOL',
            countryTranslateKey: 'COUNTRIES.NAME_BOL',
            originTranslateKey: 'COUNTRIES.ORIGIN_BOL'
        },
        {
            country: 'Bonaire, Sint Eustatius and Saba',
            code2: 'BQ',
            code3: 'BES',
            countryTranslateKey: 'COUNTRIES.NAME_BES',
            originTranslateKey: 'COUNTRIES.ORIGIN_BES'
        },
        {
            country: 'Bosnia and Herzegovina',
            code2: 'BA',
            code3: 'BIH',
            countryTranslateKey: 'COUNTRIES.NAME_BIH',
            originTranslateKey: 'COUNTRIES.ORIGIN_BIH'
        },
        {
            country: 'Botswana',
            code2: 'BW',
            code3: 'BWA',
            countryTranslateKey: 'COUNTRIES.NAME_BWA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BWA'
        },
        {
            country: 'Bouvet Island',
            code2: 'BV',
            code3: 'BVT',
            countryTranslateKey: 'COUNTRIES.NAME_BVT',
            originTranslateKey: 'COUNTRIES.ORIGIN_BVT'
        },
        {
            country: 'Brazil',
            code2: 'BR',
            code3: 'BRA',
            countryTranslateKey: 'COUNTRIES.NAME_BRA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRA'
        },
        {
            country: 'British Indian Ocean Territory (the)',
            code2: 'IO',
            code3: 'IOT',
            countryTranslateKey: 'COUNTRIES.NAME_IOT',
            originTranslateKey: 'COUNTRIES.ORIGIN_IOT'
        },
        {
            country: 'Brunei Darussalam',
            code2: 'BN',
            code3: 'BRN',
            countryTranslateKey: 'COUNTRIES.NAME_BRN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRN'
        },
        {
            country: 'Bulgaria',
            code2: 'BG',
            code3: 'BGR',
            countryTranslateKey: 'COUNTRIES.NAME_BGR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BGR'
        },
        {
            country: 'Burkina Faso',
            code2: 'BF',
            code3: 'BFA',
            countryTranslateKey: 'COUNTRIES.NAME_BFA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BFA'
        },
        // {  country: 'Burundi',  code2: 'BI',  code3: 'BDI',  countryTranslateKey: 'COUNTRIES.NAME_BDI',  originTranslateKey: 'COUNTRIES.ORIGIN_BDI'},
        {
            country: 'Cabo Verde',
            code2: 'CV',
            code3: 'CPV',
            countryTranslateKey: 'COUNTRIES.NAME_CPV',
            originTranslateKey: 'COUNTRIES.ORIGIN_CPV'
        },
        {
            country: 'Cambodia',
            code2: 'KH',
            code3: 'KHM',
            countryTranslateKey: 'COUNTRIES.NAME_KHM',
            originTranslateKey: 'COUNTRIES.ORIGIN_KHM'
        },
        {
            country: 'Cameroon',
            code2: 'CM',
            code3: 'CMR',
            countryTranslateKey: 'COUNTRIES.NAME_CMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_CMR'
        },
        {
            country: 'Canada',
            code2: 'CA',
            code3: 'CAN',
            countryTranslateKey: 'COUNTRIES.NAME_CAN',
            originTranslateKey: 'COUNTRIES.ORIGIN_CAN'
        },
        // {  country: 'Cayman Islands (the)',  code2: 'KY',  code3: 'CYM',  countryTranslateKey: 'COUNTRIES.NAME_CYM',  originTranslateKey: 'COUNTRIES.ORIGIN_CYM'},
        // {  country: 'Central African Republic (the)',  code2: 'CF',  code3: 'CAF',  countryTranslateKey: 'COUNTRIES.NAME_CAF',  originTranslateKey: 'COUNTRIES.ORIGIN_CAF'},
        {country: 'Chad', code2: 'TD', code3: 'TCD', countryTranslateKey: 'COUNTRIES.NAME_TCD', originTranslateKey: 'COUNTRIES.ORIGIN_TCD'},
        {
            country: 'Chile',
            code2: 'CL',
            code3: 'CHL',
            countryTranslateKey: 'COUNTRIES.NAME_CHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHL'
        },
        // {  country: 'China',  code2: 'CN',  code3: 'CHN',  countryTranslateKey: 'COUNTRIES.NAME_CHN',  originTranslateKey: 'COUNTRIES.ORIGIN_CHN'},
        {
            country: 'Christmas Island',
            code2: 'CX',
            code3: 'CXR',
            countryTranslateKey: 'COUNTRIES.NAME_CXR',
            originTranslateKey: 'COUNTRIES.ORIGIN_CXR'
        },
        {
            country: 'Cocos (Keeling) Islands (the)',
            code2: 'CC',
            code3: 'CCK',
            countryTranslateKey: 'COUNTRIES.NAME_CCK',
            originTranslateKey: 'COUNTRIES.ORIGIN_CCK'
        },
        {
            country: 'Colombia',
            code2: 'CO',
            code3: 'COL',
            countryTranslateKey: 'COUNTRIES.NAME_COL',
            originTranslateKey: 'COUNTRIES.ORIGIN_COL'
        },
        {
            country: 'Comoros (the)',
            code2: 'KM',
            code3: 'COM',
            countryTranslateKey: 'COUNTRIES.NAME_COM',
            originTranslateKey: 'COUNTRIES.ORIGIN_COM'
        },
        // {  country: 'Congo (the Democratic Republic of the)',  code2: 'CD',  code3: 'COD',  countryTranslateKey: 'COUNTRIES.NAME_COD',  originTranslateKey: 'COUNTRIES.ORIGIN_COD'},
        // {  country: 'Congo (the)',  code2: 'CG',  code3: 'COG',  countryTranslateKey: 'COUNTRIES.NAME_COG',  originTranslateKey: 'COUNTRIES.ORIGIN_COG'},
        // {  country: 'Cook Islands (the)',  code2: 'CK',  code3: 'COK',  countryTranslateKey: 'COUNTRIES.NAME_COK',  originTranslateKey: 'COUNTRIES.ORIGIN_COK'},
        {
            country: 'Costa Rica',
            code2: 'CR',
            code3: 'CRI',
            countryTranslateKey: 'COUNTRIES.NAME_CRI',
            originTranslateKey: 'COUNTRIES.ORIGIN_CRI'
        },
        {
            country: 'Cote d`Ivoire',
            code2: 'CI',
            code3: 'CIV',
            countryTranslateKey: 'COUNTRIES.NAME_CIV',
            originTranslateKey: 'COUNTRIES.ORIGIN_CIV'
        },
        {
            country: 'Croatia',
            code2: 'HR',
            code3: 'HRV',
            countryTranslateKey: 'COUNTRIES.NAME_HRV',
            originTranslateKey: 'COUNTRIES.ORIGIN_HRV'
        },
        {country: 'Cuba', code2: 'CU', code3: 'CUB', countryTranslateKey: 'COUNTRIES.NAME_CUB', originTranslateKey: 'COUNTRIES.ORIGIN_CUB'},
        {
            country: 'Curacao',
            code2: 'CW',
            code3: 'CUW',
            countryTranslateKey: 'COUNTRIES.NAME_CUW',
            originTranslateKey: 'COUNTRIES.ORIGIN_CUW'
        },
        {
            country: 'Cyprus',
            code2: 'CY',
            code3: 'CYP',
            countryTranslateKey: 'COUNTRIES.NAME_CYP',
            originTranslateKey: 'COUNTRIES.ORIGIN_CYP'
        },
        {
            country: 'Czech Republic (the)',
            code2: 'CZ',
            code3: 'CZE',
            countryTranslateKey: 'COUNTRIES.NAME_CZE',
            originTranslateKey: 'COUNTRIES.ORIGIN_CZE'
        },
        {
            country: 'Denmark',
            code2: 'DK',
            code3: 'DNK',
            countryTranslateKey: 'COUNTRIES.NAME_DNK',
            originTranslateKey: 'COUNTRIES.ORIGIN_DNK'
        },
        // {  country: 'Djibouti',  code2: 'DJ',  code3: 'DJI',  countryTranslateKey: 'COUNTRIES.NAME_DJI',  originTranslateKey: 'COUNTRIES.ORIGIN_DJI'},
        {
            country: 'Dominica',
            code2: 'DM',
            code3: 'DMA',
            countryTranslateKey: 'COUNTRIES.NAME_DMA',
            originTranslateKey: 'COUNTRIES.ORIGIN_DMA'
        },
        {
            country: 'Dominican Republic (the)',
            code2: 'DO',
            code3: 'DOM',
            countryTranslateKey: 'COUNTRIES.NAME_DOM',
            originTranslateKey: 'COUNTRIES.ORIGIN_DOM'
        },
        {
            country: 'Ecuador',
            code2: 'EC',
            code3: 'ECU',
            countryTranslateKey: 'COUNTRIES.NAME_ECU',
            originTranslateKey: 'COUNTRIES.ORIGIN_ECU'
        },
        // {  country: 'Egypt',  code2: 'EG',  code3: 'EGY',  countryTranslateKey: 'COUNTRIES.NAME_EGY',  originTranslateKey: 'COUNTRIES.ORIGIN_EGY'},
        {
            country: 'El Salvador',
            code2: 'SV',
            code3: 'SLV',
            countryTranslateKey: 'COUNTRIES.NAME_SLV',
            originTranslateKey: 'COUNTRIES.ORIGIN_SLV'
        },
        // {  country: 'Equatorial Guinea',  code2: 'GQ',  code3: 'GNQ',  countryTranslateKey: 'COUNTRIES.NAME_GNQ',  originTranslateKey: 'COUNTRIES.ORIGIN_GNQ'},
        // {  country: 'Eritrea',  code2: 'ER',  code3: 'ERI',  countryTranslateKey: 'COUNTRIES.NAME_ERI',  originTranslateKey: 'COUNTRIES.ORIGIN_ERI'},
        {
            country: 'Estonia',
            code2: 'EE',
            code3: 'EST',
            countryTranslateKey: 'COUNTRIES.NAME_EST',
            originTranslateKey: 'COUNTRIES.ORIGIN_EST'
        },
        // {  country: 'Ethiopia',  code2: 'ET',  code3: 'ETH',  countryTranslateKey: 'COUNTRIES.NAME_ETH',  originTranslateKey: 'COUNTRIES.ORIGIN_ETH'},
        // {  country: 'Falkland Islands (the) [Malvinas]',  code2: 'FK',  code3: 'FLK',  countryTranslateKey: 'COUNTRIES.NAME_FLK',  originTranslateKey: 'COUNTRIES.ORIGIN_FLK'},
        {
            country: 'Faroe Islands (the)',
            code2: 'FO',
            code3: 'FRO',
            countryTranslateKey: 'COUNTRIES.NAME_FRO',
            originTranslateKey: 'COUNTRIES.ORIGIN_FRO'
        },
        {country: 'Fiji', code2: 'FJ', code3: 'FJI', countryTranslateKey: 'COUNTRIES.NAME_FJI', originTranslateKey: 'COUNTRIES.ORIGIN_FJI'},
        {
            country: 'Finland',
            code2: 'FI',
            code3: 'FIN',
            countryTranslateKey: 'COUNTRIES.NAME_FIN',
            originTranslateKey: 'COUNTRIES.ORIGIN_FIN'
        },
        {
            country: 'France',
            code2: 'FR',
            code3: 'FRA',
            countryTranslateKey: 'COUNTRIES.NAME_FRA',
            originTranslateKey: 'COUNTRIES.ORIGIN_FRA'
        },
        {
            country: 'French Guiana',
            code2: 'GF',
            code3: 'GUF',
            countryTranslateKey: 'COUNTRIES.NAME_GUF',
            originTranslateKey: 'COUNTRIES.ORIGIN_GUF'
        },
        {
            country: 'French Polynesia',
            code2: 'PF',
            code3: 'PYF',
            countryTranslateKey: 'COUNTRIES.NAME_PYF',
            originTranslateKey: 'COUNTRIES.ORIGIN_PYF'
        },
        {
            country: 'French Southern Territories (the)',
            code2: 'TF',
            code3: 'ATF',
            countryTranslateKey: 'COUNTRIES.NAME_ATF',
            originTranslateKey: 'COUNTRIES.ORIGIN_ATF'
        },
        {
            country: 'Gabon',
            code2: 'GA',
            code3: 'GAB',
            countryTranslateKey: 'COUNTRIES.NAME_GAB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GAB'
        },
        {
            country: 'Gambia (the)',
            code2: 'GM',
            code3: 'GMB',
            countryTranslateKey: 'COUNTRIES.NAME_GMB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GMB'
        },
        {
            country: 'Georgia',
            code2: 'GE',
            code3: 'GEO',
            countryTranslateKey: 'COUNTRIES.NAME_GEO',
            originTranslateKey: 'COUNTRIES.ORIGIN_GEO'
        },
        {
            country: 'Germany',
            code2: 'DE',
            code3: 'DEU',
            countryTranslateKey: 'COUNTRIES.NAME_DEU',
            originTranslateKey: 'COUNTRIES.ORIGIN_DEU'
        },
        {
            country: 'Ghana',
            code2: 'GH',
            code3: 'GHA',
            countryTranslateKey: 'COUNTRIES.NAME_GHA',
            originTranslateKey: 'COUNTRIES.ORIGIN_GHA'
        },
        {
            country: 'Gibraltar',
            code2: 'GI',
            code3: 'GIB',
            countryTranslateKey: 'COUNTRIES.NAME_GIB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GIB'
        },
        {
            country: 'Greece',
            code2: 'GR',
            code3: 'GRC',
            countryTranslateKey: 'COUNTRIES.NAME_GRC',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRC'
        },
        {
            country: 'Greenland',
            code2: 'GL',
            code3: 'GRL',
            countryTranslateKey: 'COUNTRIES.NAME_GRL',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRL'
        },
        {
            country: 'Grenada',
            code2: 'GD',
            code3: 'GRD',
            countryTranslateKey: 'COUNTRIES.NAME_GRD',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRD'
        },
        {
            country: 'Guadeloupe',
            code2: 'GP',
            code3: 'GLP',
            countryTranslateKey: 'COUNTRIES.NAME_GLP',
            originTranslateKey: 'COUNTRIES.ORIGIN_GLP'
        },
        {country: 'Guam', code2: 'GU', code3: 'GUM', countryTranslateKey: 'COUNTRIES.NAME_GUM', originTranslateKey: 'COUNTRIES.ORIGIN_GUM'},
        {
            country: 'Guatemala',
            code2: 'GT',
            code3: 'GTM',
            countryTranslateKey: 'COUNTRIES.NAME_GTM',
            originTranslateKey: 'COUNTRIES.ORIGIN_GTM'
        },
        // {  country: 'Guernsey',  code2: 'GG',  code3: 'GGY',  countryTranslateKey: 'COUNTRIES.NAME_GGY',  originTranslateKey: 'COUNTRIES.ORIGIN_GGY'},
        // {  country: 'Guinea',  code2: 'GN',  code3: 'GIN',  countryTranslateKey: 'COUNTRIES.NAME_GIN',  originTranslateKey: 'COUNTRIES.ORIGIN_GIN'},
        {
            country: 'Guinea-Bissau',
            code2: 'GW',
            code3: 'GNB',
            countryTranslateKey: 'COUNTRIES.NAME_GNB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GNB'
        },
        {
            country: 'Guyana',
            code2: 'GY',
            code3: 'GUY',
            countryTranslateKey: 'COUNTRIES.NAME_GUY',
            originTranslateKey: 'COUNTRIES.ORIGIN_GUY'
        },
        {
            country: 'Haiti',
            code2: 'HT',
            code3: 'HTI',
            countryTranslateKey: 'COUNTRIES.NAME_HTI',
            originTranslateKey: 'COUNTRIES.ORIGIN_HTI'
        },
        {
            country: 'Heard Island and McDonald Islands',
            code2: 'HM',
            code3: 'HMD',
            countryTranslateKey: 'COUNTRIES.NAME_HMD',
            originTranslateKey: 'COUNTRIES.ORIGIN_HMD'
        },
        {
            country: 'Holy See (the)',
            code2: 'VA',
            code3: 'VAT',
            countryTranslateKey: 'COUNTRIES.NAME_VAT',
            originTranslateKey: 'COUNTRIES.ORIGIN_VAT'
        },
        {
            country: 'Honduras',
            code2: 'HN',
            code3: 'HND',
            countryTranslateKey: 'COUNTRIES.NAME_HND',
            originTranslateKey: 'COUNTRIES.ORIGIN_HND'
        },
        {
            country: 'Hong Kong',
            code2: 'HK',
            code3: 'HKG',
            countryTranslateKey: 'COUNTRIES.NAME_HKG',
            originTranslateKey: 'COUNTRIES.ORIGIN_HKG'
        },
        {
            country: 'Hungary',
            code2: 'HU',
            code3: 'HUN',
            countryTranslateKey: 'COUNTRIES.NAME_HUN',
            originTranslateKey: 'COUNTRIES.ORIGIN_HUN'
        },
        // {  country: 'Iceland',  code2: 'IS',  code3: 'ISL',  countryTranslateKey: 'COUNTRIES.NAME_ISL',  originTranslateKey: 'COUNTRIES.ORIGIN_ISL'},
        {
            country: 'India',
            code2: 'IN',
            code3: 'IND',
            countryTranslateKey: 'COUNTRIES.NAME_IND',
            originTranslateKey: 'COUNTRIES.ORIGIN_IND'
        },
        {
            country: 'Indonesia',
            code2: 'ID',
            code3: 'IDN',
            countryTranslateKey: 'COUNTRIES.NAME_IDN',
            originTranslateKey: 'COUNTRIES.ORIGIN_IDN'
        },
        // {  country: 'Iran (Islamic Republic of)',  code2: 'IR',  code3: 'IRN',  countryTranslateKey: 'COUNTRIES.NAME_IRN',  originTranslateKey: 'COUNTRIES.ORIGIN_IRN'},
        // {  country: 'Iraq',  code2: 'IQ',  code3: 'IRQ',  countryTranslateKey: 'COUNTRIES.NAME_IRQ',  originTranslateKey: 'COUNTRIES.ORIGIN_IRQ'},
        {
            country: 'Ireland',
            code2: 'IE',
            code3: 'IRL',
            countryTranslateKey: 'COUNTRIES.NAME_IRL',
            originTranslateKey: 'COUNTRIES.ORIGIN_IRL'
        },
        // {  country: 'Isle of Man',  code2: 'IM',  code3: 'IMN',  countryTranslateKey: 'COUNTRIES.NAME_IMN',  originTranslateKey: 'COUNTRIES.ORIGIN_IMN'},
        // {  country: 'Israel',  code2: 'IL',  code3: 'ISR',  countryTranslateKey: 'COUNTRIES.NAME_ISR',  originTranslateKey: 'COUNTRIES.ORIGIN_ISR'},
        {
            country: 'Italy',
            code2: 'IT',
            code3: 'ITA',
            countryTranslateKey: 'COUNTRIES.NAME_ITA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ITA'
        },
        {
            country: 'Jamaica',
            code2: 'JM',
            code3: 'JAM',
            countryTranslateKey: 'COUNTRIES.NAME_JAM',
            originTranslateKey: 'COUNTRIES.ORIGIN_JAM'
        },
        // {  country: 'Japan',  code2: 'JP',  code3: 'JPN',  countryTranslateKey: 'COUNTRIES.NAME_JPN',  originTranslateKey: 'COUNTRIES.ORIGIN_JPN'},
        // {  country: 'Jersey',  code2: 'JE',  code3: 'JEY',  countryTranslateKey: 'COUNTRIES.NAME_JEY',  originTranslateKey: 'COUNTRIES.ORIGIN_JEY'},
        {
            country: 'Jordan',
            code2: 'JO',
            code3: 'JOR',
            countryTranslateKey: 'COUNTRIES.NAME_JOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_JOR'
        },
        {
            country: 'Kazakhstan',
            code2: 'KZ',
            code3: 'KAZ',
            countryTranslateKey: 'COUNTRIES.NAME_KAZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_KAZ'
        },
        {
            country: 'Kenya',
            code2: 'KE',
            code3: 'KEN',
            countryTranslateKey: 'COUNTRIES.NAME_KEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_KEN'
        },
        // {  country: 'Kiribati',  code2: 'KI',  code3: 'KIR',  countryTranslateKey: 'COUNTRIES.NAME_KIR',  originTranslateKey: 'COUNTRIES.ORIGIN_KIR'},
        // {  country: 'Korea (the Democratic People`s Republic of)',  code2: 'KP',  code3: 'PRK',  countryTranslateKey: 'COUNTRIES.NAME_PRK',  originTranslateKey: 'COUNTRIES.ORIGIN_PRK'},
        // {  country: 'Korea (the Republic of)',  code2: 'KR',  code3: 'KOR',  countryTranslateKey: 'COUNTRIES.NAME_KOR',  originTranslateKey: 'COUNTRIES.ORIGIN_KOR'},
        {
            country: 'Kosovo',
            code2: 'XK',
            code3: 'XKK',
            countryTranslateKey: 'COUNTRIES.NAME_XKK',
            originTranslateKey: 'COUNTRIES.ORIGIN_XKK'
        },
        {
            country: 'Kuwait',
            code2: 'KW',
            code3: 'KWT',
            countryTranslateKey: 'COUNTRIES.NAME_KWT',
            originTranslateKey: 'COUNTRIES.ORIGIN_KWT'
        },
        {
            country: 'Kyrgyzstan',
            code2: 'KG',
            code3: 'KGZ',
            countryTranslateKey: 'COUNTRIES.NAME_KGZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_KGZ'
        },
        {
            country: 'Lao People`s Democratic Republic',
            code2: 'LA',
            code3: 'LAO',
            countryTranslateKey: 'COUNTRIES.NAME_LAO',
            originTranslateKey: 'COUNTRIES.ORIGIN_LAO'
        },
        {
            country: 'Latvia',
            code2: 'LV',
            code3: 'LVA',
            countryTranslateKey: 'COUNTRIES.NAME_LVA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LVA'
        },
        // {  country: 'Lebanon',  code2: 'LB',  code3: 'LBN',  countryTranslateKey: 'COUNTRIES.NAME_LBN',  originTranslateKey: 'COUNTRIES.ORIGIN_LBN'},
        // {  country: 'Lesotho',  code2: 'LS',  code3: 'LSO',  countryTranslateKey: 'COUNTRIES.NAME_LSO',  originTranslateKey: 'COUNTRIES.ORIGIN_LSO'},
        // {  country: 'Liberia',  code2: 'LR',  code3: 'LBR',  countryTranslateKey: 'COUNTRIES.NAME_LBR',  originTranslateKey: 'COUNTRIES.ORIGIN_LBR'},
        // {  country: 'Libya',  code2: 'LY',  code3: 'LBY',  countryTranslateKey: 'COUNTRIES.NAME_LBY',  originTranslateKey: 'COUNTRIES.ORIGIN_LBY'},
        {
            country: 'Liechtenstein',
            code2: 'LI',
            code3: 'LIE',
            countryTranslateKey: 'COUNTRIES.NAME_LIE',
            originTranslateKey: 'COUNTRIES.ORIGIN_LIE'
        },
        {
            country: 'Lithuania',
            code2: 'LT',
            code3: 'LTU',
            countryTranslateKey: 'COUNTRIES.NAME_LTU',
            originTranslateKey: 'COUNTRIES.ORIGIN_LTU'
        },
        {
            country: 'Luxembourg',
            code2: 'LU',
            code3: 'LUX',
            countryTranslateKey: 'COUNTRIES.NAME_LUX',
            originTranslateKey: 'COUNTRIES.ORIGIN_LUX'
        },
        {
            country: 'Macao',
            code2: 'MO',
            code3: 'MAC',
            countryTranslateKey: 'COUNTRIES.NAME_MAC',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAC'
        },
        {
            country: 'Macedonia (the former Yugoslav Republic of)',
            code2: 'MK',
            code3: 'MKD',
            countryTranslateKey: 'COUNTRIES.NAME_MKD',
            originTranslateKey: 'COUNTRIES.ORIGIN_MKD'
        },
        // {  country: 'Madagascar',  code2: 'MG',  code3: 'MDG',  countryTranslateKey: 'COUNTRIES.NAME_MDG',  originTranslateKey: 'COUNTRIES.ORIGIN_MDG'},
        {
            country: 'Malawi',
            code2: 'MW',
            code3: 'MWI',
            countryTranslateKey: 'COUNTRIES.NAME_MWI',
            originTranslateKey: 'COUNTRIES.ORIGIN_MWI'
        },
        {
            country: 'Malaysia',
            code2: 'MY',
            code3: 'MYS',
            countryTranslateKey: 'COUNTRIES.NAME_MYS',
            originTranslateKey: 'COUNTRIES.ORIGIN_MYS'
        },
        {
            country: 'Maldives',
            code2: 'MV',
            code3: 'MDV',
            countryTranslateKey: 'COUNTRIES.NAME_MDV',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDV'
        },
        {country: 'Mali', code2: 'ML', code3: 'MLI', countryTranslateKey: 'COUNTRIES.NAME_MLI', originTranslateKey: 'COUNTRIES.ORIGIN_MLI'},
        {
            country: 'Malta',
            code2: 'MT',
            code3: 'MLT',
            countryTranslateKey: 'COUNTRIES.NAME_MLT',
            originTranslateKey: 'COUNTRIES.ORIGIN_MLT'
        },
        {
            country: 'Marshall Islands (the)',
            code2: 'MH',
            code3: 'MHL',
            countryTranslateKey: 'COUNTRIES.NAME_MHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_MHL'
        },
        {
            country: 'Martinique',
            code2: 'MQ',
            code3: 'MTQ',
            countryTranslateKey: 'COUNTRIES.NAME_MTQ',
            originTranslateKey: 'COUNTRIES.ORIGIN_MTQ'
        },
        {
            country: 'Mauritania',
            code2: 'MR',
            code3: 'MRT',
            countryTranslateKey: 'COUNTRIES.NAME_MRT',
            originTranslateKey: 'COUNTRIES.ORIGIN_MRT'
        },
        {
            country: 'Mauritius',
            code2: 'MU',
            code3: 'MUS',
            countryTranslateKey: 'COUNTRIES.NAME_MUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_MUS'
        },
        // {  country: 'Mayotte',  code2: 'YT',  code3: 'MYT',  countryTranslateKey: 'COUNTRIES.NAME_MYT',  originTranslateKey: 'COUNTRIES.ORIGIN_MYT'},
        {
            country: 'Mexico',
            code2: 'MX',
            code3: 'MEX',
            countryTranslateKey: 'COUNTRIES.NAME_MEX',
            originTranslateKey: 'COUNTRIES.ORIGIN_MEX'
        },
        // {  country: 'Micronesia (Federated States of)',  code2: 'FM',  code3: 'FSM',  countryTranslateKey: 'COUNTRIES.NAME_FSM',  originTranslateKey: 'COUNTRIES.ORIGIN_FSM'},
        {
            country: 'Moldova (the Republic of)',
            code2: 'MD',
            code3: 'MDA',
            countryTranslateKey: 'COUNTRIES.NAME_MDA',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDA'
        },
        {
            country: 'Monaco',
            code2: 'MC',
            code3: 'MCO',
            countryTranslateKey: 'COUNTRIES.NAME_MCO',
            originTranslateKey: 'COUNTRIES.ORIGIN_MCO'
        },
        {
            country: 'Mongolia',
            code2: 'MN',
            code3: 'MNG',
            countryTranslateKey: 'COUNTRIES.NAME_MNG',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNG'
        },
        {
            country: 'Montenegro',
            code2: 'ME',
            code3: 'MNE',
            countryTranslateKey: 'COUNTRIES.NAME_MNE',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNE'
        },
        // {  country: 'Montserrat',  code2: 'MS',  code3: 'MSR',  countryTranslateKey: 'COUNTRIES.NAME_MSR',  originTranslateKey: 'COUNTRIES.ORIGIN_MSR'},
        {
            country: 'Morocco',
            code2: 'MA',
            code3: 'MAR',
            countryTranslateKey: 'COUNTRIES.NAME_MAR',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAR'
        },
        {
            country: 'Mozambique',
            code2: 'MZ',
            code3: 'MOZ',
            countryTranslateKey: 'COUNTRIES.NAME_MOZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_MOZ'
        },
        // {  country: 'Myanmar',  code2: 'MM',  code3: 'MMR',  countryTranslateKey: 'COUNTRIES.NAME_MMR',  originTranslateKey: 'COUNTRIES.ORIGIN_MMR'},
        {
            country: 'Namibia',
            code2: 'NA',
            code3: 'NAM',
            countryTranslateKey: 'COUNTRIES.NAME_NAM',
            originTranslateKey: 'COUNTRIES.ORIGIN_NAM'
        },
        {
            country: 'Nauru',
            code2: 'NR',
            code3: 'NRU',
            countryTranslateKey: 'COUNTRIES.NAME_NRU',
            originTranslateKey: 'COUNTRIES.ORIGIN_NRU'
        },
        {
            country: 'Nepal',
            code2: 'NP',
            code3: 'NPL',
            countryTranslateKey: 'COUNTRIES.NAME_NPL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NPL'
        },
        {
            country: 'Netherlands (the)',
            code2: 'NL',
            code3: 'NLD',
            countryTranslateKey: 'COUNTRIES.NAME_NLD',
            originTranslateKey: 'COUNTRIES.ORIGIN_NLD'
        },
        {
            country: 'New Caledonia',
            code2: 'NC',
            code3: 'NCL',
            countryTranslateKey: 'COUNTRIES.NAME_NCL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NCL'
        },
        {
            country: 'New Zealand',
            code2: 'NZ',
            code3: 'NZL',
            countryTranslateKey: 'COUNTRIES.NAME_NZL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NZL'
        },
        {
            country: 'Nicaragua',
            code2: 'NI',
            code3: 'NIC',
            countryTranslateKey: 'COUNTRIES.NAME_NIC',
            originTranslateKey: 'COUNTRIES.ORIGIN_NIC'
        },
        // {  country: 'Niger (the)',  code2: 'NE',  code3: 'NER',  countryTranslateKey: 'COUNTRIES.NAME_NER',  originTranslateKey: 'COUNTRIES.ORIGIN_NER'},
        {
            country: 'Nigeria',
            code2: 'NG',
            code3: 'NGA',
            countryTranslateKey: 'COUNTRIES.NAME_NGA',
            originTranslateKey: 'COUNTRIES.ORIGIN_NGA'
        },
        // {  country: 'Niue',  code2: 'NU',  code3: 'NIU',  countryTranslateKey: 'COUNTRIES.NAME_NIU',  originTranslateKey: 'COUNTRIES.ORIGIN_NIU'},
        {
            country: 'Norfolk Island',
            code2: 'NF',
            code3: 'NFK',
            countryTranslateKey: 'COUNTRIES.NAME_NFK',
            originTranslateKey: 'COUNTRIES.ORIGIN_NFK'
        },
        {
            country: 'Northern Mariana Islands (the)',
            code2: 'MP',
            code3: 'MNP',
            countryTranslateKey: 'COUNTRIES.NAME_MNP',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNP'
        },
        {
            country: 'Norway',
            code2: 'NO',
            code3: 'NOR',
            countryTranslateKey: 'COUNTRIES.NAME_NOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_NOR'
        },
        {country: 'Oman', code2: 'OM', code3: 'OMN', countryTranslateKey: 'COUNTRIES.NAME_OMN', originTranslateKey: 'COUNTRIES.ORIGIN_OMN'},
        {
            country: 'Pakistan',
            code2: 'PK',
            code3: 'PAK',
            countryTranslateKey: 'COUNTRIES.NAME_PAK',
            originTranslateKey: 'COUNTRIES.ORIGIN_PAK'
        },
        // {  country: 'Palau',  code2: 'PW',  code3: 'PLW',  countryTranslateKey: 'COUNTRIES.NAME_PLW',  originTranslateKey: 'COUNTRIES.ORIGIN_PLW'},
        // {  country: 'Palestine, State of',  code2: 'PS',  code3: 'PSE',  countryTranslateKey: 'COUNTRIES.NAME_PSE',  originTranslateKey: 'COUNTRIES.ORIGIN_PSE'},
        {
            country: 'Panama',
            code2: 'PA',
            code3: 'PAN',
            countryTranslateKey: 'COUNTRIES.NAME_PAN',
            originTranslateKey: 'COUNTRIES.ORIGIN_PAN'
        },
        {
            country: 'Papua New Guinea',
            code2: 'PG',
            code3: 'PNG',
            countryTranslateKey: 'COUNTRIES.NAME_PNG',
            originTranslateKey: 'COUNTRIES.ORIGIN_PNG'
        },
        {
            country: 'Paraguay',
            code2: 'PY',
            code3: 'PRY',
            countryTranslateKey: 'COUNTRIES.NAME_PRY',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRY'
        },
        {country: 'Peru', code2: 'PE', code3: 'PER', countryTranslateKey: 'COUNTRIES.NAME_PER', originTranslateKey: 'COUNTRIES.ORIGIN_PER'},
        {
            country: 'Philippines (the)',
            code2: 'PH',
            code3: 'PHL',
            countryTranslateKey: 'COUNTRIES.NAME_PHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_PHL'
        },
        {
            country: 'Pitcairn',
            code2: 'PN',
            code3: 'PCN',
            countryTranslateKey: 'COUNTRIES.NAME_PCN',
            originTranslateKey: 'COUNTRIES.ORIGIN_PCN'
        },
        {
            country: 'Poland',
            code2: 'PL',
            code3: 'POL',
            countryTranslateKey: 'COUNTRIES.NAME_POL',
            originTranslateKey: 'COUNTRIES.ORIGIN_POL'
        },
        {
            country: 'Portugal',
            code2: 'PT',
            code3: 'PRT',
            countryTranslateKey: 'COUNTRIES.NAME_PRT',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRT'
        },
        {
            country: 'Puerto Rico',
            code2: 'PR',
            code3: 'PRI',
            countryTranslateKey: 'COUNTRIES.NAME_PRI',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRI'
        },
        {
            country: 'Qatar',
            code2: 'QA',
            code3: 'QAT',
            countryTranslateKey: 'COUNTRIES.NAME_QAT',
            originTranslateKey: 'COUNTRIES.ORIGIN_QAT'
        },
        // {  country: 'Réunion',  code2: 'RE',  code3: 'REU',  countryTranslateKey: 'COUNTRIES.NAME_REU',  originTranslateKey: 'COUNTRIES.ORIGIN_REU'},
        {
            country: 'Romania',
            code2: 'RO',
            code3: 'ROU',
            countryTranslateKey: 'COUNTRIES.NAME_ROU',
            originTranslateKey: 'COUNTRIES.ORIGIN_ROU'
        },
        // {  country: 'Russian Federation (the)',  code2: 'RU',  code3: 'RUS',  countryTranslateKey: 'COUNTRIES.NAME_RUS',  originTranslateKey: 'COUNTRIES.ORIGIN_RUS'},
        {
            country: 'Rwanda',
            code2: 'RW',
            code3: 'RWA',
            countryTranslateKey: 'COUNTRIES.NAME_RWA',
            originTranslateKey: 'COUNTRIES.ORIGIN_RWA'
        },
        {
            country: 'Saint Barthélemy',
            code2: 'BL',
            code3: 'BLM',
            countryTranslateKey: 'COUNTRIES.NAME_BLM',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLM'
        },
        // {  country: 'Saint Helena, Ascension and Tristan da Cunha',  code2: 'SH',  code3: 'SHN',  countryTranslateKey: 'COUNTRIES.NAME_SHN',  originTranslateKey: 'COUNTRIES.ORIGIN_SHN'},
        {
            country: 'Saint Kitts and Nevis',
            code2: 'KN',
            code3: 'KNA',
            countryTranslateKey: 'COUNTRIES.NAME_KNA',
            originTranslateKey: 'COUNTRIES.ORIGIN_KNA'
        },
        {
            country: 'Saint Lucia',
            code2: 'LC',
            code3: 'LCA',
            countryTranslateKey: 'COUNTRIES.NAME_LCA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LCA'
        },
        {
            country: 'Saint Martin (French part)',
            code2: 'MF',
            code3: 'MAF',
            countryTranslateKey: 'COUNTRIES.NAME_MAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAF'
        },
        {
            country: 'Saint Pierre and Miquelon',
            code2: 'PM',
            code3: 'SPM',
            countryTranslateKey: 'COUNTRIES.NAME_SPM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SPM'
        },
        // {  country: 'Saint Vincent and the Grenadines',  code2: 'VC',  code3: 'VCT',  countryTranslateKey: 'COUNTRIES.NAME_VCT',  originTranslateKey: 'COUNTRIES.ORIGIN_VCT'},
        // {  country: 'Samoa',  code2: 'WS',  code3: 'WSM',  countryTranslateKey: 'COUNTRIES.NAME_WSM',  originTranslateKey: 'COUNTRIES.ORIGIN_WSM'},
        {
            country: 'San Marino',
            code2: 'SM',
            code3: 'SMR',
            countryTranslateKey: 'COUNTRIES.NAME_SMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_SMR'
        },
        // {  country: 'Sao Tome and Principe',  code2: 'ST',  code3: 'STP',  countryTranslateKey: 'COUNTRIES.NAME_STP',  originTranslateKey: 'COUNTRIES.ORIGIN_STP'},
        {
            country: 'Saudi Arabia',
            code2: 'SA',
            code3: 'SAU',
            countryTranslateKey: 'COUNTRIES.NAME_SAU',
            originTranslateKey: 'COUNTRIES.ORIGIN_SAU'
        },
        {
            country: 'Senegal',
            code2: 'SN',
            code3: 'SEN',
            countryTranslateKey: 'COUNTRIES.NAME_SEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SEN'
        },
        {
            country: 'Serbia',
            code2: 'RS',
            code3: 'SRB',
            countryTranslateKey: 'COUNTRIES.NAME_SRB',
            originTranslateKey: 'COUNTRIES.ORIGIN_SRB'
        },
        {
            country: 'Seychelles',
            code2: 'SC',
            code3: 'SYC',
            countryTranslateKey: 'COUNTRIES.NAME_SYC',
            originTranslateKey: 'COUNTRIES.ORIGIN_SYC'
        },
        {
            country: 'Sierra Leone',
            code2: 'SL',
            code3: 'SLE',
            countryTranslateKey: 'COUNTRIES.NAME_SLE',
            originTranslateKey: 'COUNTRIES.ORIGIN_SLE'
        },
        {
            country: 'Singapore',
            code2: 'SG',
            code3: 'SGP',
            countryTranslateKey: 'COUNTRIES.NAME_SGP',
            originTranslateKey: 'COUNTRIES.ORIGIN_SGP'
        },
        {
            country: 'Sint Maarten (Dutch part)',
            code2: 'SX',
            code3: 'SXM',
            countryTranslateKey: 'COUNTRIES.NAME_SXM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SXM'
        },
        {
            country: 'Slovakia',
            code2: 'SK',
            code3: 'SVK',
            countryTranslateKey: 'COUNTRIES.NAME_SVK',
            originTranslateKey: 'COUNTRIES.ORIGIN_SVK'
        },
        {
            country: 'Slovenia',
            code2: 'SI',
            code3: 'SVN',
            countryTranslateKey: 'COUNTRIES.NAME_SVN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SVN'
        },
        // {  country: 'Solomon Islands',  code2: 'SB',  code3: 'SLB',  countryTranslateKey: 'COUNTRIES.NAME_SLB',  originTranslateKey: 'COUNTRIES.ORIGIN_SLB'},
        {
            country: 'Somalia',
            code2: 'SO',
            code3: 'SOM',
            countryTranslateKey: 'COUNTRIES.NAME_SOM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SOM'
        },
        {
            country: 'South Africa',
            code2: 'ZA',
            code3: 'ZAF',
            countryTranslateKey: 'COUNTRIES.NAME_ZAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_ZAF'
        },
        {
            country: 'South Georgia and the South Sandwich Islands',
            code2: 'GS',
            code3: 'SGS',
            countryTranslateKey: 'COUNTRIES.NAME_SGS',
            originTranslateKey: 'COUNTRIES.ORIGIN_SGS'
        },
        {
            country: 'South Sudan',
            code2: 'SS',
            code3: 'SSD',
            countryTranslateKey: 'COUNTRIES.NAME_SSD',
            originTranslateKey: 'COUNTRIES.ORIGIN_SSD'
        },
        {
            country: 'Spain',
            code2: 'ES',
            code3: 'ESP',
            countryTranslateKey: 'COUNTRIES.NAME_ESP',
            originTranslateKey: 'COUNTRIES.ORIGIN_ESP'
        },
        // {  country: 'Sri Lanka',  code2: 'LK',  code3: 'LKA',  countryTranslateKey: 'COUNTRIES.NAME_LKA',  originTranslateKey: 'COUNTRIES.ORIGIN_LKA'},
        // {  country: 'Sudan (the)',  code2: 'SD',  code3: 'SDN',  countryTranslateKey: 'COUNTRIES.NAME_SDN',  originTranslateKey: 'COUNTRIES.ORIGIN_SDN'},
        {
            country: 'Suriname',
            code2: 'SR',
            code3: 'SUR',
            countryTranslateKey: 'COUNTRIES.NAME_SUR',
            originTranslateKey: 'COUNTRIES.ORIGIN_SUR'
        },
        {
            country: 'Svalbard and Jan Mayen',
            code2: 'SJ',
            code3: 'SJM',
            countryTranslateKey: 'COUNTRIES.NAME_SJM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SJM'
        },
        {
            country: 'Swaziland',
            code2: 'SZ',
            code3: 'SWZ',
            countryTranslateKey: 'COUNTRIES.NAME_SWZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_SWZ'
        },
        {
            country: 'Sweden',
            code2: 'SE',
            code3: 'SWE',
            countryTranslateKey: 'COUNTRIES.NAME_SWE',
            originTranslateKey: 'COUNTRIES.ORIGIN_SWE'
        },
        {
            country: 'Switzerland',
            code2: 'CH',
            code3: 'CHE',
            countryTranslateKey: 'COUNTRIES.NAME_CHE',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHE'
        },
        // {  country: 'Syrian Arab Republic',  code2: 'SY',  code3: 'SYR',  countryTranslateKey: 'COUNTRIES.NAME_SYR',  originTranslateKey: 'COUNTRIES.ORIGIN_SYR'},
        // {  country: 'Taiwan (Province of China)',  code2: 'TW',  code3: 'TWN',  countryTranslateKey: 'COUNTRIES.NAME_TWN',  originTranslateKey: 'COUNTRIES.ORIGIN_TWN'},
        {
            country: 'Tajikistan',
            code2: 'TJ',
            code3: 'TJK',
            countryTranslateKey: 'COUNTRIES.NAME_TJK',
            originTranslateKey: 'COUNTRIES.ORIGIN_TJK'
        },
        {
            country: 'Tanzania, United Republic of',
            code2: 'TZ',
            code3: 'TZA',
            countryTranslateKey: 'COUNTRIES.NAME_TZA',
            originTranslateKey: 'COUNTRIES.ORIGIN_TZA'
        },
        {
            country: 'Thailand',
            code2: 'TH',
            code3: 'THA',
            countryTranslateKey: 'COUNTRIES.NAME_THA',
            originTranslateKey: 'COUNTRIES.ORIGIN_THA'
        },
        {
            country: 'Timor-Leste',
            code2: 'TL',
            code3: 'TLS',
            countryTranslateKey: 'COUNTRIES.NAME_TLS',
            originTranslateKey: 'COUNTRIES.ORIGIN_TLS'
        },
        {country: 'Togo', code2: 'TG', code3: 'TGO', countryTranslateKey: 'COUNTRIES.NAME_TGO', originTranslateKey: 'COUNTRIES.ORIGIN_TGO'},
        // {  country: 'Tokelau',  code2: 'TK',  code3: 'TKL',  countryTranslateKey: 'COUNTRIES.NAME_TKL',  originTranslateKey: 'COUNTRIES.ORIGIN_TKL'},
        // {  country: 'Tonga',  code2: 'TO',  code3: 'TON',  countryTranslateKey: 'COUNTRIES.NAME_TON',  originTranslateKey: 'COUNTRIES.ORIGIN_TON'},
        {
            country: 'Trinidad and Tobago',
            code2: 'TT',
            code3: 'TTO',
            countryTranslateKey: 'COUNTRIES.NAME_TTO',
            originTranslateKey: 'COUNTRIES.ORIGIN_TTO'
        },
        // {  country: 'Tunisia',  code2: 'TN',  code3: 'TUN',  countryTranslateKey: 'COUNTRIES.NAME_TUN',  originTranslateKey: 'COUNTRIES.ORIGIN_TUN'},
        {
            country: 'Turkey',
            code2: 'TR',
            code3: 'TUR',
            countryTranslateKey: 'COUNTRIES.NAME_TUR',
            originTranslateKey: 'COUNTRIES.ORIGIN_TUR'
        },
        // {  country: 'Turkmenistan',  code2: 'TM',  code3: 'TKM',  countryTranslateKey: 'COUNTRIES.NAME_TKM',  originTranslateKey: 'COUNTRIES.ORIGIN_TKM'},
        // {  country: 'Turks and Caicos Islands (the)',  code2: 'TC',  code3: 'TCA',  countryTranslateKey: 'COUNTRIES.NAME_TCA',  originTranslateKey: 'COUNTRIES.ORIGIN_TCA'},
        // {  country: 'Tuvalu',  code2: 'TV',  code3: 'TUV',  countryTranslateKey: 'COUNTRIES.NAME_TUV',  originTranslateKey: 'COUNTRIES.ORIGIN_TUV'},
        {
            country: 'Uganda',
            code2: 'UG',
            code3: 'UGA',
            countryTranslateKey: 'COUNTRIES.NAME_UGA',
            originTranslateKey: 'COUNTRIES.ORIGIN_UGA'
        },
        {
            country: 'Ukraine',
            code2: 'UA',
            code3: 'UKR',
            countryTranslateKey: 'COUNTRIES.NAME_UKR',
            originTranslateKey: 'COUNTRIES.ORIGIN_UKR'
        },
        {
            country: 'United Arab Emirates (the)',
            code2: 'AE',
            code3: 'ARE',
            countryTranslateKey: 'COUNTRIES.NAME_ARE',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARE'
        },
        {
            country: 'United Kingdom of Great Britain and Northern Ireland (the)',
            code2: 'GB',
            code3: 'GBR',
            countryTranslateKey: 'COUNTRIES.NAME_GBR',
            originTranslateKey: 'COUNTRIES.ORIGIN_GBR'
        },
        // {  country: 'United States Minor Outlying Islands (the)',  code2: 'UM',  code3: 'UMI',  countryTranslateKey: 'COUNTRIES.NAME_UMI',  originTranslateKey: 'COUNTRIES.ORIGIN_UMI'},
        {
            country: 'United States of America (the)',
            code2: 'US',
            code3: 'USA',
            countryTranslateKey: 'COUNTRIES.NAME_USA',
            originTranslateKey: 'COUNTRIES.ORIGIN_USA'
        },
        {
            country: 'Uruguay',
            code2: 'UY',
            code3: 'URY',
            countryTranslateKey: 'COUNTRIES.NAME_URY',
            originTranslateKey: 'COUNTRIES.ORIGIN_URY'
        },
        // {  country: 'Uzbekistan',  code2: 'UZ',  code3: 'UZB',  countryTranslateKey: 'COUNTRIES.NAME_UZB',  originTranslateKey: 'COUNTRIES.ORIGIN_UZB'},
        // {  country: 'Vanuatu',  code2: 'VU',  code3: 'VUT',  countryTranslateKey: 'COUNTRIES.NAME_VUT',  originTranslateKey: 'COUNTRIES.ORIGIN_VUT'},
        {
            country: 'Venezuela (Bolivarian Republic of)',
            code2: 'VE',
            code3: 'VEN',
            countryTranslateKey: 'COUNTRIES.NAME_VEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_VEN'
        },
        {
            country: 'Viet Nam',
            code2: 'VN',
            code3: 'VNM',
            countryTranslateKey: 'COUNTRIES.NAME_VNM',
            originTranslateKey: 'COUNTRIES.ORIGIN_VNM'
        },
        // {  country: 'Virgin Islands (British)',  code2: 'VG',  code3: 'VGB',  countryTranslateKey: 'COUNTRIES.NAME_VGB',  originTranslateKey: 'COUNTRIES.ORIGIN_VGB'},
        {
            country: 'Virgin Islands (U.S.)',
            code2: 'VI',
            code3: 'VIR',
            countryTranslateKey: 'COUNTRIES.NAME_VIR',
            originTranslateKey: 'COUNTRIES.ORIGIN_VIR'
        },
        // {  country: 'Wallis and Futuna',  code2: 'WF',  code3: 'WLF',  countryTranslateKey: 'COUNTRIES.NAME_WLF',  originTranslateKey: 'COUNTRIES.ORIGIN_WLF'},
        {
            country: 'Western Sahara',
            code2: 'EH',
            code3: 'ESH',
            countryTranslateKey: 'COUNTRIES.NAME_ESH',
            originTranslateKey: 'COUNTRIES.ORIGIN_ESH'
        },
        // {  country: 'Yemen',  code2: 'YE',  code3: 'YEM',  countryTranslateKey: 'COUNTRIES.NAME_YEM',  originTranslateKey: 'COUNTRIES.ORIGIN_YEM'},
        // {  country: 'Zambia',  code2: 'ZM',  code3: 'ZMB',  countryTranslateKey: 'COUNTRIES.NAME_ZMB',  originTranslateKey: 'COUNTRIES.ORIGIN_ZMB'},
        {
            country: 'Zimbabwe',
            code2: 'ZW',
            code3: 'ZWE',
            countryTranslateKey: 'COUNTRIES.NAME_ZWE',
            originTranslateKey: 'COUNTRIES.ORIGIN_ZWE'
        }
    ];

    public dlSupportedCountries: { country: string; code2: string; code3: string; countryTranslateKey: string; originTranslateKey: string; supportedProviders: Array<KycVendors>}[] = [
        // {  country: 'Afghanistan',  code2: 'AF',  code3: 'AFG',  countryTranslateKey: 'COUNTRIES.NAME_AFG',  originTranslateKey: 'COUNTRIES.ORIGIN_AFG' },
        {
            country: 'Aland Islands',
            code2: 'AX',
            code3: 'ALA',
            countryTranslateKey: 'COUNTRIES.NAME_ALA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ALA',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Albania',
            code2: 'AL',
            code3: 'ALB',
            countryTranslateKey: 'COUNTRIES.NAME_ALB',
            originTranslateKey: 'COUNTRIES.ORIGIN_ALB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Algeria',
            code2: 'DZ',
            code3: 'DZA',
            countryTranslateKey: 'COUNTRIES.NAME_DZA',
            originTranslateKey: 'COUNTRIES.ORIGIN_DZA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'American Samoa',
            code2: 'AS',
            code3: 'ASM',
            countryTranslateKey: 'COUNTRIES.NAME_ASM',
            originTranslateKey: 'COUNTRIES.ORIGIN_ASM',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Andorra',
            code2: 'AD',
            code3: 'AND',
            countryTranslateKey: 'COUNTRIES.NAME_AND',
            originTranslateKey: 'COUNTRIES.ORIGIN_AND',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Angola',
            code2: 'AO',
            code3: 'AGO',
            countryTranslateKey: 'COUNTRIES.NAME_AGO',
            originTranslateKey: 'COUNTRIES.ORIGIN_AGO',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Anguilla',  code2: 'AI',  code3: 'AIA',  countryTranslateKey: 'COUNTRIES.NAME_AIA',  originTranslateKey: 'COUNTRIES.ORIGIN_AIA'},
        // {  country: 'Antarctica',  code2: 'AQ',  code3: 'ATA',  countryTranslateKey: 'COUNTRIES.NAME_ATA',  originTranslateKey: 'COUNTRIES.ORIGIN_ATA'},
        {
            country: 'Antigua and Barbuda',
            code2: 'AG',
            code3: 'ATG',
            countryTranslateKey: 'COUNTRIES.NAME_ATG',
            originTranslateKey: 'COUNTRIES.ORIGIN_ATG',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Argentina',
            code2: 'AR',
            code3: 'ARG',
            countryTranslateKey: 'COUNTRIES.NAME_ARG',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARG',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Armenia',
            code2: 'AM',
            code3: 'ARM',
            countryTranslateKey: 'COUNTRIES.NAME_ARM',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Aruba',
            code2: 'AW',
            code3: 'ABW',
            countryTranslateKey: 'COUNTRIES.NAME_ABW',
            originTranslateKey: 'COUNTRIES.ORIGIN_ABW',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Australia',
            code2: 'AU',
            code3: 'AUS',
            countryTranslateKey: 'COUNTRIES.NAME_AUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_AUS',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Austria',
            code2: 'AT',
            code3: 'AUT',
            countryTranslateKey: 'COUNTRIES.NAME_AUT',
            originTranslateKey: 'COUNTRIES.ORIGIN_AUT',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Azerbaijan',
            code2: 'AZ',
            code3: 'AZE',
            countryTranslateKey: 'COUNTRIES.NAME_AZE',
            originTranslateKey: 'COUNTRIES.ORIGIN_AZE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Bahamas (the)',
            code2: 'BS',
            code3: 'BHS',
            countryTranslateKey: 'COUNTRIES.NAME_BHS',
            originTranslateKey: 'COUNTRIES.ORIGIN_BHS',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Bahrain',
            code2: 'BH',
            code3: 'BHR',
            countryTranslateKey: 'COUNTRIES.NAME_BHR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BHR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Bangladesh',
            code2: 'BD',
            code3: 'BGD',
            countryTranslateKey: 'COUNTRIES.NAME_BGD',
            originTranslateKey: 'COUNTRIES.ORIGIN_BGD',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Barbados',
            code2: 'BB',
            code3: 'BRB',
            countryTranslateKey: 'COUNTRIES.NAME_BRB',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Belarus',
            code2: 'BY',
            code3: 'BLR',
            countryTranslateKey: 'COUNTRIES.NAME_BLR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Belgium',
            code2: 'BE',
            code3: 'BEL',
            countryTranslateKey: 'COUNTRIES.NAME_BEL',
            originTranslateKey: 'COUNTRIES.ORIGIN_BEL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Belize',
            code2: 'BZ',
            code3: 'BLZ',
            countryTranslateKey: 'COUNTRIES.NAME_BLZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLZ',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Benin',
            code2: 'BJ',
            code3: 'BEN',
            countryTranslateKey: 'COUNTRIES.NAME_BEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BEN',
            supportedProviders: [KycVendors.VERIFF]
        },
        // {  country: 'Bermuda',  code2: 'BM',  code3: 'BMU',  countryTranslateKey: 'COUNTRIES.NAME_BMU',  originTranslateKey: 'COUNTRIES.ORIGIN_BMU'},
        {
            country: 'Bhutan',
            code2: 'BT',
            code3: 'BTN',
            countryTranslateKey: 'COUNTRIES.NAME_BTN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BTN',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Bolivia (Plurinational State of)',
            code2: 'BO',
            code3: 'BOL',
            countryTranslateKey: 'COUNTRIES.NAME_BOL',
            originTranslateKey: 'COUNTRIES.ORIGIN_BOL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Bonaire, Sint Eustatius and Saba',
            code2: 'BQ',
            code3: 'BES',
            countryTranslateKey: 'COUNTRIES.NAME_BES',
            originTranslateKey: 'COUNTRIES.ORIGIN_BES',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Bosnia and Herzegovina',
            code2: 'BA',
            code3: 'BIH',
            countryTranslateKey: 'COUNTRIES.NAME_BIH',
            originTranslateKey: 'COUNTRIES.ORIGIN_BIH',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Botswana',
            code2: 'BW',
            code3: 'BWA',
            countryTranslateKey: 'COUNTRIES.NAME_BWA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BWA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Bouvet Island',  code2: 'BV',  code3: 'BVT',  countryTranslateKey: 'COUNTRIES.NAME_BVT',  originTranslateKey: 'COUNTRIES.ORIGIN_BVT'},
        {
            country: 'Brazil',
            code2: 'BR',
            code3: 'BRA',
            countryTranslateKey: 'COUNTRIES.NAME_BRA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'British Indian Ocean Territory (the)',
            code2: 'IO',
            code3: 'IOT',
            countryTranslateKey: 'COUNTRIES.NAME_IOT',
            originTranslateKey: 'COUNTRIES.ORIGIN_IOT',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Brunei Darussalam',
            code2: 'BN',
            code3: 'BRN',
            countryTranslateKey: 'COUNTRIES.NAME_BRN',
            originTranslateKey: 'COUNTRIES.ORIGIN_BRN',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Bulgaria',
            code2: 'BG',
            code3: 'BGR',
            countryTranslateKey: 'COUNTRIES.NAME_BGR',
            originTranslateKey: 'COUNTRIES.ORIGIN_BGR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Burkina Faso',
            code2: 'BF',
            code3: 'BFA',
            countryTranslateKey: 'COUNTRIES.NAME_BFA',
            originTranslateKey: 'COUNTRIES.ORIGIN_BFA',
            supportedProviders: [KycVendors.VERIFF]
        },
        // {  country: 'Burundi',  code2: 'BI',  code3: 'BDI',  countryTranslateKey: 'COUNTRIES.NAME_BDI',  originTranslateKey: 'COUNTRIES.ORIGIN_BDI'},
        {
            country: 'Cabo Verde',
            code2: 'CV',
            code3: 'CPV',
            countryTranslateKey: 'COUNTRIES.NAME_CPV',
            originTranslateKey: 'COUNTRIES.ORIGIN_CPV',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Cambodia',
            code2: 'KH',
            code3: 'KHM',
            countryTranslateKey: 'COUNTRIES.NAME_KHM',
            originTranslateKey: 'COUNTRIES.ORIGIN_KHM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Cameroon',
            code2: 'CM',
            code3: 'CMR',
            countryTranslateKey: 'COUNTRIES.NAME_CMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_CMR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Canada',
            code2: 'CA',
            code3: 'CAN',
            countryTranslateKey: 'COUNTRIES.NAME_CAN',
            originTranslateKey: 'COUNTRIES.ORIGIN_CAN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Cayman Islands (the)',
            code2: 'KY',
            code3: 'CYM',
            countryTranslateKey: 'COUNTRIES.NAME_CYM',
            originTranslateKey: 'COUNTRIES.ORIGIN_CYM',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Central African Republic (the)',
            code2: 'CF',
            code3: 'CAF',
            countryTranslateKey: 'COUNTRIES.NAME_CAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_CAF',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Chad',
            code2: 'TD',
            code3: 'TCD',
            countryTranslateKey: 'COUNTRIES.NAME_TCD',
            originTranslateKey: 'COUNTRIES.ORIGIN_TCD',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Chile',
            code2: 'CL',
            code3: 'CHL',
            countryTranslateKey: 'COUNTRIES.NAME_CHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHL',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'China',
            code2: 'CN',
            code3: 'CHN',
            countryTranslateKey: 'COUNTRIES.NAME_CHN',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHN',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Christmas Island',
            code2: 'CX',
            code3: 'CXR',
            countryTranslateKey: 'COUNTRIES.NAME_CXR',
            originTranslateKey: 'COUNTRIES.ORIGIN_CXR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Cocos (Keeling) Islands (the)',
            code2: 'CC',
            code3: 'CCK',
            countryTranslateKey: 'COUNTRIES.NAME_CCK',
            originTranslateKey: 'COUNTRIES.ORIGIN_CCK',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Colombia',
            code2: 'CO',
            code3: 'COL',
            countryTranslateKey: 'COUNTRIES.NAME_COL',
            originTranslateKey: 'COUNTRIES.ORIGIN_COL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Comoros (the)',  code2: 'KM',  code3: 'COM',  countryTranslateKey: 'COUNTRIES.NAME_COM',  originTranslateKey: 'COUNTRIES.ORIGIN_COM'},
        {
            country: 'Congo (the Democratic Republic of the)',
            code2: 'CD',
            code3: 'COD',
            countryTranslateKey: 'COUNTRIES.NAME_COD',
            originTranslateKey: 'COUNTRIES.ORIGIN_COD',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Congo (the)',
            code2: 'CG',
            code3: 'COG',
            countryTranslateKey: 'COUNTRIES.NAME_COG',
            originTranslateKey: 'COUNTRIES.ORIGIN_COG',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Cook Islands (the)',  code2: 'CK',  code3: 'COK',  countryTranslateKey: 'COUNTRIES.NAME_COK',  originTranslateKey: 'COUNTRIES.ORIGIN_COK'},
        {
            country: 'Costa Rica',
            code2: 'CR',
            code3: 'CRI',
            countryTranslateKey: 'COUNTRIES.NAME_CRI',
            originTranslateKey: 'COUNTRIES.ORIGIN_CRI',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Cote d`Ivoire',
            code2: 'CI',
            code3: 'CIV',
            countryTranslateKey: 'COUNTRIES.NAME_CIV',
            originTranslateKey: 'COUNTRIES.ORIGIN_CIV',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Croatia',
            code2: 'HR',
            code3: 'HRV',
            countryTranslateKey: 'COUNTRIES.NAME_HRV',
            originTranslateKey: 'COUNTRIES.ORIGIN_HRV',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Cuba',
            code2: 'CU',
            code3: 'CUB',
            countryTranslateKey: 'COUNTRIES.NAME_CUB',
            originTranslateKey: 'COUNTRIES.ORIGIN_CUB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]},
        {
            country: 'Curacao',
            code2: 'CW',
            code3: 'CUW',
            countryTranslateKey: 'COUNTRIES.NAME_CUW',
            originTranslateKey: 'COUNTRIES.ORIGIN_CUW',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Cyprus',
            code2: 'CY',
            code3: 'CYP',
            countryTranslateKey: 'COUNTRIES.NAME_CYP',
            originTranslateKey: 'COUNTRIES.ORIGIN_CYP',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Czech Republic (the)',
            code2: 'CZ',
            code3: 'CZE',
            countryTranslateKey: 'COUNTRIES.NAME_CZE',
            originTranslateKey: 'COUNTRIES.ORIGIN_CZE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Denmark',
            code2: 'DK',
            code3: 'DNK',
            countryTranslateKey: 'COUNTRIES.NAME_DNK',
            originTranslateKey: 'COUNTRIES.ORIGIN_DNK',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Djibouti',  code2: 'DJ',  code3: 'DJI',  countryTranslateKey: 'COUNTRIES.NAME_DJI',  originTranslateKey: 'COUNTRIES.ORIGIN_DJI'},
        {
            country: 'Dominica',
            code2: 'DM',
            code3: 'DMA',
            countryTranslateKey: 'COUNTRIES.NAME_DMA',
            originTranslateKey: 'COUNTRIES.ORIGIN_DMA',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Dominican Republic (the)',
            code2: 'DO',
            code3: 'DOM',
            countryTranslateKey: 'COUNTRIES.NAME_DOM',
            originTranslateKey: 'COUNTRIES.ORIGIN_DOM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Ecuador',
            code2: 'EC',
            code3: 'ECU',
            countryTranslateKey: 'COUNTRIES.NAME_ECU',
            originTranslateKey: 'COUNTRIES.ORIGIN_ECU',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Egypt',
            code2: 'EG',
            code3: 'EGY',
            countryTranslateKey: 'COUNTRIES.NAME_EGY',
            originTranslateKey: 'COUNTRIES.ORIGIN_EGY',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'El Salvador',
            code2: 'SV',
            code3: 'SLV',
            countryTranslateKey: 'COUNTRIES.NAME_SLV',
            originTranslateKey: 'COUNTRIES.ORIGIN_SLV',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Equatorial Guinea',  code2: 'GQ',  code3: 'GNQ',  countryTranslateKey: 'COUNTRIES.NAME_GNQ',  originTranslateKey: 'COUNTRIES.ORIGIN_GNQ'},
        {
            country: 'Eritrea',
            code2: 'ER',
            code3: 'ERI',
            countryTranslateKey: 'COUNTRIES.NAME_ERI',
            originTranslateKey: 'COUNTRIES.ORIGIN_ERI',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Estonia',
            code2: 'EE',
            code3: 'EST',
            countryTranslateKey: 'COUNTRIES.NAME_EST',
            originTranslateKey: 'COUNTRIES.ORIGIN_EST',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Ethiopia',  code2: 'ET',  code3: 'ETH',  countryTranslateKey: 'COUNTRIES.NAME_ETH',  originTranslateKey: 'COUNTRIES.ORIGIN_ETH'},
        // {  country: 'Falkland Islands (the) [Malvinas]',  code2: 'FK',  code3: 'FLK',  countryTranslateKey: 'COUNTRIES.NAME_FLK',  originTranslateKey: 'COUNTRIES.ORIGIN_FLK'},
        {
            country: 'Faroe Islands (the)',
            code2: 'FO',
            code3: 'FRO',
            countryTranslateKey: 'COUNTRIES.NAME_FRO',
            originTranslateKey: 'COUNTRIES.ORIGIN_FRO',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Fiji',
            code2: 'FJ',
            code3: 'FJI',
            countryTranslateKey: 'COUNTRIES.NAME_FJI',
            originTranslateKey: 'COUNTRIES.ORIGIN_FJI',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Finland',
            code2: 'FI',
            code3: 'FIN',
            countryTranslateKey: 'COUNTRIES.NAME_FIN',
            originTranslateKey: 'COUNTRIES.ORIGIN_FIN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'France',
            code2: 'FR',
            code3: 'FRA',
            countryTranslateKey: 'COUNTRIES.NAME_FRA',
            originTranslateKey: 'COUNTRIES.ORIGIN_FRA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'French Guiana',
            code2: 'GF',
            code3: 'GUF',
            countryTranslateKey: 'COUNTRIES.NAME_GUF',
            originTranslateKey: 'COUNTRIES.ORIGIN_GUF',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'French Polynesia',
            code2: 'PF',
            code3: 'PYF',
            countryTranslateKey: 'COUNTRIES.NAME_PYF',
            originTranslateKey: 'COUNTRIES.ORIGIN_PYF',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'French Southern Territories (the)',  code2: 'TF',  code3: 'ATF',  countryTranslateKey: 'COUNTRIES.NAME_ATF',  originTranslateKey: 'COUNTRIES.ORIGIN_ATF'},
        // {  country: 'Gabon',  code2: 'GA',  code3: 'GAB',  countryTranslateKey: 'COUNTRIES.NAME_GAB',  originTranslateKey: 'COUNTRIES.ORIGIN_GAB'},
        {
            country: 'Gambia (the)',
            code2: 'GM',
            code3: 'GMB',
            countryTranslateKey: 'COUNTRIES.NAME_GMB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GMB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Georgia',
            code2: 'GE',
            code3: 'GEO',
            countryTranslateKey: 'COUNTRIES.NAME_GEO',
            originTranslateKey: 'COUNTRIES.ORIGIN_GEO',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Germany',
            code2: 'DE',
            code3: 'DEU',
            countryTranslateKey: 'COUNTRIES.NAME_DEU_DL',
            originTranslateKey: 'COUNTRIES.ORIGIN_DEU',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Ghana',
            code2: 'GH',
            code3: 'GHA',
            countryTranslateKey: 'COUNTRIES.NAME_GHA',
            originTranslateKey: 'COUNTRIES.ORIGIN_GHA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Gibraltar',
            code2: 'GI',
            code3: 'GIB',
            countryTranslateKey: 'COUNTRIES.NAME_GIB',
            originTranslateKey: 'COUNTRIES.ORIGIN_GIB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Greece',
            code2: 'GR',
            code3: 'GRC',
            countryTranslateKey: 'COUNTRIES.NAME_GRC',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRC',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Greenland',
            code2: 'GL',
            code3: 'GRL',
            countryTranslateKey: 'COUNTRIES.NAME_GRL',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Grenada',
            code2: 'GD',
            code3: 'GRD',
            countryTranslateKey: 'COUNTRIES.NAME_GRD',
            originTranslateKey: 'COUNTRIES.ORIGIN_GRD',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Guadeloupe',
            code2: 'GP',
            code3: 'GLP',
            countryTranslateKey: 'COUNTRIES.NAME_GLP',
            originTranslateKey: 'COUNTRIES.ORIGIN_GLP',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Guam',
            code2: 'GU',
            code3: 'GUM',
            countryTranslateKey: 'COUNTRIES.NAME_GUM',
            originTranslateKey: 'COUNTRIES.ORIGIN_GUM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Guatemala',
            code2: 'GT',
            code3: 'GTM',
            countryTranslateKey: 'COUNTRIES.NAME_GTM',
            originTranslateKey: 'COUNTRIES.ORIGIN_GTM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Guernsey',
            code2: 'GG',
            code3: 'GGY',
            countryTranslateKey: 'COUNTRIES.NAME_GGY',
            originTranslateKey: 'COUNTRIES.ORIGIN_GGY',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Guinea',
            code2: 'GN',
            code3: 'GIN',
            countryTranslateKey: 'COUNTRIES.NAME_GIN',
            originTranslateKey: 'COUNTRIES.ORIGIN_GIN',
            supportedProviders: [KycVendors.VERIFF]
        },
        // {  country: 'Guinea-Bissau',  code2: 'GW',  code3: 'GNB',  countryTranslateKey: 'COUNTRIES.NAME_GNB',  originTranslateKey: 'COUNTRIES.ORIGIN_GNB'},
        {
            country: 'Guyana',
            code2: 'GY',
            code3: 'GUY',
            countryTranslateKey: 'COUNTRIES.NAME_GUY',
            originTranslateKey: 'COUNTRIES.ORIGIN_GUY',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Haiti',
            code2: 'HT',
            code3: 'HTI',
            countryTranslateKey: 'COUNTRIES.NAME_HTI',
            originTranslateKey: 'COUNTRIES.ORIGIN_HTI',
            supportedProviders: [KycVendors.VERIFF]
        },
        // {  country: 'Heard Island and McDonald Islands',  code2: 'HM',  code3: 'HMD',  countryTranslateKey: 'COUNTRIES.NAME_HMD',  originTranslateKey: 'COUNTRIES.ORIGIN_HMD'},
        // {  country: 'Holy See (the)',  code2: 'VA',  code3: 'VAT',  countryTranslateKey: 'COUNTRIES.NAME_VAT',  originTranslateKey: 'COUNTRIES.ORIGIN_VAT'},
        {
            country: 'Honduras',
            code2: 'HN',
            code3: 'HND',
            countryTranslateKey: 'COUNTRIES.NAME_HND',
            originTranslateKey: 'COUNTRIES.ORIGIN_HND',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Hong Kong',
            code2: 'HK',
            code3: 'HKG',
            countryTranslateKey: 'COUNTRIES.NAME_HKG',
            originTranslateKey: 'COUNTRIES.ORIGIN_HKG',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Hungary',
            code2: 'HU',
            code3: 'HUN',
            countryTranslateKey: 'COUNTRIES.NAME_HUN',
            originTranslateKey: 'COUNTRIES.ORIGIN_HUN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Iceland',
            code2: 'IS',
            code3: 'ISL',
            countryTranslateKey: 'COUNTRIES.NAME_ISL',
            originTranslateKey: 'COUNTRIES.ORIGIN_ISL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'India',
            code2: 'IN',
            code3: 'IND',
            countryTranslateKey: 'COUNTRIES.NAME_IND',
            originTranslateKey: 'COUNTRIES.ORIGIN_IND',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Indonesia',
            code2: 'ID',
            code3: 'IDN',
            countryTranslateKey: 'COUNTRIES.NAME_IDN',
            originTranslateKey: 'COUNTRIES.ORIGIN_IDN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Iran (Islamic Republic of)',
            code2: 'IR',
            code3: 'IRN',
            countryTranslateKey: 'COUNTRIES.NAME_IRN',
            originTranslateKey: 'COUNTRIES.ORIGIN_IRN',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Iraq',
            code2: 'IQ',
            code3: 'IRQ',
            countryTranslateKey: 'COUNTRIES.NAME_IRQ',
            originTranslateKey: 'COUNTRIES.ORIGIN_IRQ',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Ireland',
            code2: 'IE',
            code3: 'IRL',
            countryTranslateKey: 'COUNTRIES.NAME_IRL',
            originTranslateKey: 'COUNTRIES.ORIGIN_IRL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Isle of Man',
            code2: 'IM',
            code3: 'IMN',
            countryTranslateKey: 'COUNTRIES.NAME_IMN',
            originTranslateKey: 'COUNTRIES.ORIGIN_IMN',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Israel',
            code2: 'IL',
            code3: 'ISR',
            countryTranslateKey: 'COUNTRIES.NAME_ISR',
            originTranslateKey: 'COUNTRIES.ORIGIN_ISR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Italy',
            code2: 'IT',
            code3: 'ITA',
            countryTranslateKey: 'COUNTRIES.NAME_ITA',
            originTranslateKey: 'COUNTRIES.ORIGIN_ITA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Jamaica',
            code2: 'JM',
            code3: 'JAM',
            countryTranslateKey: 'COUNTRIES.NAME_JAM',
            originTranslateKey: 'COUNTRIES.ORIGIN_JAM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Japan',
            code2: 'JP',
            code3: 'JPN',
            countryTranslateKey: 'COUNTRIES.NAME_JPN',
            originTranslateKey: 'COUNTRIES.ORIGIN_JPN',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Jersey',
            code2: 'JE',
            code3: 'JEY',
            countryTranslateKey: 'COUNTRIES.NAME_JEY',
            originTranslateKey: 'COUNTRIES.ORIGIN_JEY',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Jordan',
            code2: 'JO',
            code3: 'JOR',
            countryTranslateKey: 'COUNTRIES.NAME_JOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_JOR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Kazakhstan',
            code2: 'KZ',
            code3: 'KAZ',
            countryTranslateKey: 'COUNTRIES.NAME_KAZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_KAZ',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Kenya',
            code2: 'KE',
            code3: 'KEN',
            countryTranslateKey: 'COUNTRIES.NAME_KEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_KEN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Kiribati',
            code2: 'KI',
            code3: 'KIR',
            countryTranslateKey: 'COUNTRIES.NAME_KIR',
            originTranslateKey: 'COUNTRIES.ORIGIN_KIR',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Korea (the Democratic People`s Republic of)',
            code2: 'KP',
            code3: 'PRK',
            countryTranslateKey: 'COUNTRIES.NAME_PRK',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRK',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Korea (the Republic of)',
            code2: 'KR',
            code3: 'KOR',
            countryTranslateKey: 'COUNTRIES.NAME_KOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_KOR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Kosovo',
            code2: 'XK',
            code3: 'XKK',
            countryTranslateKey: 'COUNTRIES.NAME_XKK',
            originTranslateKey: 'COUNTRIES.ORIGIN_XKK',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Kuwait',
            code2: 'KW',
            code3: 'KWT',
            countryTranslateKey: 'COUNTRIES.NAME_KWT',
            originTranslateKey: 'COUNTRIES.ORIGIN_KWT',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Kyrgyzstan',
            code2: 'KG',
            code3: 'KGZ',
            countryTranslateKey: 'COUNTRIES.NAME_KGZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_KGZ',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Lao People`s Democratic Republic',  code2: 'LA',  code3: 'LAO',  countryTranslateKey: 'COUNTRIES.NAME_LAO',  originTranslateKey: 'COUNTRIES.ORIGIN_LAO'},
        {
            country: 'Latvia',
            code2: 'LV',
            code3: 'LVA',
            countryTranslateKey: 'COUNTRIES.NAME_LVA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LVA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Lebanon',
            code2: 'LB',
            code3: 'LBN',
            countryTranslateKey: 'COUNTRIES.NAME_LBN',
            originTranslateKey: 'COUNTRIES.ORIGIN_LBN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Lesotho',
            code2: 'LS',
            code3: 'LSO',
            countryTranslateKey: 'COUNTRIES.NAME_LSO',
            originTranslateKey: 'COUNTRIES.ORIGIN_LSO',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Liberia',
            code2: 'LR',
            code3: 'LBR',
            countryTranslateKey: 'COUNTRIES.NAME_LBR',
            originTranslateKey: 'COUNTRIES.ORIGIN_LBR',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        // {  country: 'Libya',  code2: 'LY',  code3: 'LBY',  countryTranslateKey: 'COUNTRIES.NAME_LBY',  originTranslateKey: 'COUNTRIES.ORIGIN_LBY'},
        {
            country: 'Liechtenstein',
            code2: 'LI',
            code3: 'LIE',
            countryTranslateKey: 'COUNTRIES.NAME_LIE',
            originTranslateKey: 'COUNTRIES.ORIGIN_LIE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Lithuania',
            code2: 'LT',
            code3: 'LTU',
            countryTranslateKey: 'COUNTRIES.NAME_LTU',
            originTranslateKey: 'COUNTRIES.ORIGIN_LTU',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Luxembourg',
            code2: 'LU',
            code3: 'LUX',
            countryTranslateKey: 'COUNTRIES.NAME_LUX',
            originTranslateKey: 'COUNTRIES.ORIGIN_LUX',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Macao',
            code2: 'MO',
            code3: 'MAC',
            countryTranslateKey: 'COUNTRIES.NAME_MAC',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAC',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Macedonia (the former Yugoslav Republic of)',
            code2: 'MK',
            code3: 'MKD',
            countryTranslateKey: 'COUNTRIES.NAME_MKD',
            originTranslateKey: 'COUNTRIES.ORIGIN_MKD',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Madagascar',
            code2: 'MG',
            code3: 'MDG',
            countryTranslateKey: 'COUNTRIES.NAME_MDG',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDG',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Malawi',
            code2: 'MW',
            code3: 'MWI',
            countryTranslateKey: 'COUNTRIES.NAME_MWI',
            originTranslateKey: 'COUNTRIES.ORIGIN_MWI',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Malaysia',
            code2: 'MY',
            code3: 'MYS',
            countryTranslateKey: 'COUNTRIES.NAME_MYS',
            originTranslateKey: 'COUNTRIES.ORIGIN_MYS',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Maldives',
            code2: 'MV',
            code3: 'MDV',
            countryTranslateKey: 'COUNTRIES.NAME_MDV',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDV',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Mali',
            code2: 'ML',
            code3: 'MLI',
            countryTranslateKey: 'COUNTRIES.NAME_MLI',
            originTranslateKey: 'COUNTRIES.ORIGIN_MLI',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Malta',
            code2: 'MT',
            code3: 'MLT',
            countryTranslateKey: 'COUNTRIES.NAME_MLT',
            originTranslateKey: 'COUNTRIES.ORIGIN_MLT',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Marshall Islands (the)',  code2: 'MH',  code3: 'MHL',  countryTranslateKey: 'COUNTRIES.NAME_MHL',  originTranslateKey: 'COUNTRIES.ORIGIN_MHL'},
        // {  country: 'Martinique',  code2: 'MQ',  code3: 'MTQ',  countryTranslateKey: 'COUNTRIES.NAME_MTQ',  originTranslateKey: 'COUNTRIES.ORIGIN_MTQ'},
        // {  country: 'Mauritania',  code2: 'MR',  code3: 'MRT',  countryTranslateKey: 'COUNTRIES.NAME_MRT',  originTranslateKey: 'COUNTRIES.ORIGIN_MRT'},
        {
            country: 'Mauritius',
            code2: 'MU',  code3: 'MUS',
            countryTranslateKey: 'COUNTRIES.NAME_MUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_MUS',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        // {  country: 'Mayotte',  code2: 'YT',  code3: 'MYT',  countryTranslateKey: 'COUNTRIES.NAME_MYT',  originTranslateKey: 'COUNTRIES.ORIGIN_MYT'},
        {
            country: 'Mexico',
            code2: 'MX',
            code3: 'MEX',
            countryTranslateKey: 'COUNTRIES.NAME_MEX',
            originTranslateKey: 'COUNTRIES.ORIGIN_MEX',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Micronesia (Federated States of)',  code2: 'FM',  code3: 'FSM',  countryTranslateKey: 'COUNTRIES.NAME_FSM',  originTranslateKey: 'COUNTRIES.ORIGIN_FSM'},
        {
            country: 'Moldova (the Republic of)',
            code2: 'MD',
            code3: 'MDA',
            countryTranslateKey: 'COUNTRIES.NAME_MDA',
            originTranslateKey: 'COUNTRIES.ORIGIN_MDA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Monaco',
            code2: 'MC',
            code3: 'MCO',
            countryTranslateKey: 'COUNTRIES.NAME_MCO',
            originTranslateKey: 'COUNTRIES.ORIGIN_MCO',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Mongolia',
            code2: 'MN',
            code3: 'MNG',
            countryTranslateKey: 'COUNTRIES.NAME_MNG',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNG',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Montenegro',
            code2: 'ME',
            code3: 'MNE',
            countryTranslateKey: 'COUNTRIES.NAME_MNE',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Montserrat',  code2: 'MS',  code3: 'MSR',  countryTranslateKey: 'COUNTRIES.NAME_MSR',  originTranslateKey: 'COUNTRIES.ORIGIN_MSR'},
        {
            country: 'Morocco',
            code2: 'MA',
            code3: 'MAR',
            countryTranslateKey: 'COUNTRIES.NAME_MAR',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Mozambique',
            code2: 'MZ',
            code3: 'MOZ',
            countryTranslateKey: 'COUNTRIES.NAME_MOZ',
            originTranslateKey: 'COUNTRIES.ORIGIN_MOZ',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Myanmar',
            code2: 'MM',
            code3: 'MMR',
            countryTranslateKey: 'COUNTRIES.NAME_MMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_MMR',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Namibia',
            code2: 'NA',
            code3: 'NAM',
            countryTranslateKey: 'COUNTRIES.NAME_NAM',
            originTranslateKey: 'COUNTRIES.ORIGIN_NAM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Nauru',  code2: 'NR',  code3: 'NRU',  countryTranslateKey: 'COUNTRIES.NAME_NRU',  originTranslateKey: 'COUNTRIES.ORIGIN_NRU'},
        {
            country: 'Nepal',
            code2: 'NP',
            code3: 'NPL',
            countryTranslateKey: 'COUNTRIES.NAME_NPL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NPL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Netherlands (the)',
            code2: 'NL',
            code3: 'NLD',
            countryTranslateKey: 'COUNTRIES.NAME_NLD',
            originTranslateKey: 'COUNTRIES.ORIGIN_NLD',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'New Caledonia',  code2: 'NC',  code3: 'NCL',  countryTranslateKey: 'COUNTRIES.NAME_NCL',  originTranslateKey: 'COUNTRIES.ORIGIN_NCL'},
        {
            country: 'New Zealand',
            code2: 'NZ',
            code3: 'NZL',
            countryTranslateKey: 'COUNTRIES.NAME_NZL',
            originTranslateKey: 'COUNTRIES.ORIGIN_NZL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Nicaragua',
            code2: 'NI',
            code3: 'NIC',
            countryTranslateKey: 'COUNTRIES.NAME_NIC',
            originTranslateKey: 'COUNTRIES.ORIGIN_NIC',
            supportedProviders: [KycVendors.VERIFF]
        },
        // {  country: 'Niger (the)',  code2: 'NE',  code3: 'NER',  countryTranslateKey: 'COUNTRIES.NAME_NER',  originTranslateKey: 'COUNTRIES.ORIGIN_NER'},
        {
            country: 'Nigeria',
            code2: 'NG',
            code3: 'NGA',
            countryTranslateKey: 'COUNTRIES.NAME_NGA',
            originTranslateKey: 'COUNTRIES.ORIGIN_NGA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Niue',  code2: 'NU',  code3: 'NIU',  countryTranslateKey: 'COUNTRIES.NAME_NIU',  originTranslateKey: 'COUNTRIES.ORIGIN_NIU'},
        {
            country: 'Norfolk Island',
            code2: 'NF',
            code3: 'NFK',
            countryTranslateKey: 'COUNTRIES.NAME_NFK',
            originTranslateKey: 'COUNTRIES.ORIGIN_NFK',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Northern Mariana Islands (the)',
            code2: 'MP',
            code3: 'MNP',
            countryTranslateKey: 'COUNTRIES.NAME_MNP',
            originTranslateKey: 'COUNTRIES.ORIGIN_MNP',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Norway',
            code2: 'NO',
            code3: 'NOR',
            countryTranslateKey: 'COUNTRIES.NAME_NOR',
            originTranslateKey: 'COUNTRIES.ORIGIN_NOR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Oman',
            code2: 'OM',
            code3: 'OMN',
            countryTranslateKey: 'COUNTRIES.NAME_OMN',
            originTranslateKey: 'COUNTRIES.ORIGIN_OMN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Pakistan',
            code2: 'PK',
            code3: 'PAK',
            countryTranslateKey: 'COUNTRIES.NAME_PAK',
            originTranslateKey: 'COUNTRIES.ORIGIN_PAK',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Palau',  code2: 'PW',  code3: 'PLW',  countryTranslateKey: 'COUNTRIES.NAME_PLW',  originTranslateKey: 'COUNTRIES.ORIGIN_PLW'},
        {
            country: 'Palestine, State of',
            code2: 'PS',
            code3: 'PSE',
            countryTranslateKey: 'COUNTRIES.NAME_PSE',
            originTranslateKey: 'COUNTRIES.ORIGIN_PSE',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Panama',
            code2: 'PA',
            code3: 'PAN',
            countryTranslateKey: 'COUNTRIES.NAME_PAN',
            originTranslateKey: 'COUNTRIES.ORIGIN_PAN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Papua New Guinea',
            code2: 'PG',
            code3: 'PNG',
            countryTranslateKey: 'COUNTRIES.NAME_PNG',
            originTranslateKey: 'COUNTRIES.ORIGIN_PNG',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Paraguay',
            code2: 'PY',
            code3: 'PRY',
            countryTranslateKey: 'COUNTRIES.NAME_PRY',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRY',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Peru',
            code2: 'PE',
            code3: 'PER',
            countryTranslateKey: 'COUNTRIES.NAME_PER',
            originTranslateKey: 'COUNTRIES.ORIGIN_PER',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Philippines (the)',
            code2: 'PH',
            code3: 'PHL',
            countryTranslateKey: 'COUNTRIES.NAME_PHL',
            originTranslateKey: 'COUNTRIES.ORIGIN_PHL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Pitcairn',  code2: 'PN',  code3: 'PCN',  countryTranslateKey: 'COUNTRIES.NAME_PCN',  originTranslateKey: 'COUNTRIES.ORIGIN_PCN'},
        {
            country: 'Poland',
            code2: 'PL',
            code3: 'POL',
            countryTranslateKey: 'COUNTRIES.NAME_POL',
            originTranslateKey: 'COUNTRIES.ORIGIN_POL',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Portugal',
            code2: 'PT',
            code3: 'PRT',
            countryTranslateKey: 'COUNTRIES.NAME_PRT',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRT',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Puerto Rico',
            code2: 'PR',
            code3: 'PRI',
            countryTranslateKey: 'COUNTRIES.NAME_PRI',
            originTranslateKey: 'COUNTRIES.ORIGIN_PRI',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Qatar',
            code2: 'QA',
            code3: 'QAT',
            countryTranslateKey: 'COUNTRIES.NAME_QAT',
            originTranslateKey: 'COUNTRIES.ORIGIN_QAT',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Réunion',  code2: 'RE',  code3: 'REU',  countryTranslateKey: 'COUNTRIES.NAME_REU',  originTranslateKey: 'COUNTRIES.ORIGIN_REU'},
        {
            country: 'Romania',
            code2: 'RO',
            code3: 'ROU',
            countryTranslateKey: 'COUNTRIES.NAME_ROU',
            originTranslateKey: 'COUNTRIES.ORIGIN_ROU',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Russian Federation (the)',
            code2: 'RU',
            code3: 'RUS',
            countryTranslateKey: 'COUNTRIES.NAME_RUS',
            originTranslateKey: 'COUNTRIES.ORIGIN_RUS',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Rwanda',
            code2: 'RW',
            code3: 'RWA',
            countryTranslateKey: 'COUNTRIES.NAME_RWA',
            originTranslateKey: 'COUNTRIES.ORIGIN_RWA',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Saint Barthélemy',
            code2: 'BL',
            code3: 'BLM',
            countryTranslateKey: 'COUNTRIES.NAME_BLM',
            originTranslateKey: 'COUNTRIES.ORIGIN_BLM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Saint Helena, Ascension and Tristan da Cunha',  code2: 'SH',  code3: 'SHN',  countryTranslateKey: 'COUNTRIES.NAME_SHN',  originTranslateKey: 'COUNTRIES.ORIGIN_SHN'},
        {
            country: 'Saint Kitts and Nevis',
            code2: 'KN',
            code3: 'KNA',
            countryTranslateKey: 'COUNTRIES.NAME_KNA',
            originTranslateKey: 'COUNTRIES.ORIGIN_KNA',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Saint Lucia',
            code2: 'LC',
            code3: 'LCA',
            countryTranslateKey: 'COUNTRIES.NAME_LCA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LCA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Saint Martin (French part)',
            code2: 'MF',
            code3: 'MAF',
            countryTranslateKey: 'COUNTRIES.NAME_MAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_MAF',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Saint Pierre and Miquelon',
            code2: 'PM',
            code3: 'SPM',
            countryTranslateKey: 'COUNTRIES.NAME_SPM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SPM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Saint Vincent and the Grenadines',  code2: 'VC',  code3: 'VCT',  countryTranslateKey: 'COUNTRIES.NAME_VCT',  originTranslateKey: 'COUNTRIES.ORIGIN_VCT'},
        // {  country: 'Samoa',  code2: 'WS',  code3: 'WSM',  countryTranslateKey: 'COUNTRIES.NAME_WSM',  originTranslateKey: 'COUNTRIES.ORIGIN_WSM'},
        {
            country: 'San Marino',
            code2: 'SM',
            code3: 'SMR',
            countryTranslateKey: 'COUNTRIES.NAME_SMR',
            originTranslateKey: 'COUNTRIES.ORIGIN_SMR',
            supportedProviders: [KycVendors.VERIFF]
        },
        // {  country: 'Sao Tome and Principe',  code2: 'ST',  code3: 'STP',  countryTranslateKey: 'COUNTRIES.NAME_STP',  originTranslateKey: 'COUNTRIES.ORIGIN_STP'},
        {
            country: 'Saudi Arabia',
            code2: 'SA',
            code3: 'SAU',
            countryTranslateKey: 'COUNTRIES.NAME_SAU',
            originTranslateKey: 'COUNTRIES.ORIGIN_SAU',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Senegal',
            code2: 'SN',
            code3: 'SEN',
            countryTranslateKey: 'COUNTRIES.NAME_SEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SEN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Serbia',
            code2: 'RS',
            code3: 'SRB',
            countryTranslateKey: 'COUNTRIES.NAME_SRB',
            originTranslateKey: 'COUNTRIES.ORIGIN_SRB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Seychelles',  code2: 'SC',  code3: 'SYC',  countryTranslateKey: 'COUNTRIES.NAME_SYC',  originTranslateKey: 'COUNTRIES.ORIGIN_SYC'},
        {
            country: 'Sierra Leone',
            code2: 'SL',
            code3: 'SLE',
            countryTranslateKey: 'COUNTRIES.NAME_SLE',
            originTranslateKey: 'COUNTRIES.ORIGIN_SLE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Singapore',
            code2: 'SG',
            code3: 'SGP',
            countryTranslateKey: 'COUNTRIES.NAME_SGP',
            originTranslateKey: 'COUNTRIES.ORIGIN_SGP',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Sint Maarten (Dutch part)',
            code2: 'SX',
            code3: 'SXM',
            countryTranslateKey: 'COUNTRIES.NAME_SXM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SXM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Slovakia',
            code2: 'SK',
            code3: 'SVK',
            countryTranslateKey: 'COUNTRIES.NAME_SVK',
            originTranslateKey: 'COUNTRIES.ORIGIN_SVK',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Slovenia',
            code2: 'SI',
            code3: 'SVN',
            countryTranslateKey: 'COUNTRIES.NAME_SVN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SVN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Solomon Islands',  code2: 'SB',  code3: 'SLB',  countryTranslateKey: 'COUNTRIES.NAME_SLB',  originTranslateKey: 'COUNTRIES.ORIGIN_SLB'},
        {
            country: 'Somalia',
            code2: 'SO',
            code3: 'SOM',
            countryTranslateKey: 'COUNTRIES.NAME_SOM',
            originTranslateKey: 'COUNTRIES.ORIGIN_SOM',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'South Africa',
            code2: 'ZA',
            code3: 'ZAF',
            countryTranslateKey: 'COUNTRIES.NAME_ZAF',
            originTranslateKey: 'COUNTRIES.ORIGIN_ZAF',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'South Georgia and the South Sandwich Islands',  code2: 'GS',  code3: 'SGS',  countryTranslateKey: 'COUNTRIES.NAME_SGS',  originTranslateKey: 'COUNTRIES.ORIGIN_SGS'},
        {
            country: 'South Sudan',
             code2: 'SS',
             code3: 'SSD',
             countryTranslateKey: 'COUNTRIES.NAME_SSD',
             originTranslateKey: 'COUNTRIES.ORIGIN_SSD',
             supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Spain',
            code2: 'ES',
            code3: 'ESP',
            countryTranslateKey: 'COUNTRIES.NAME_ESP',
            originTranslateKey: 'COUNTRIES.ORIGIN_ESP',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Sri Lanka',
            code2: 'LK',
            code3: 'LKA',
            countryTranslateKey: 'COUNTRIES.NAME_LKA',
            originTranslateKey: 'COUNTRIES.ORIGIN_LKA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Sudan (the)',
            code2: 'SD',
            code3: 'SDN',
            countryTranslateKey: 'COUNTRIES.NAME_SDN',
            originTranslateKey: 'COUNTRIES.ORIGIN_SDN',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Suriname',
            code2: 'SR',
            code3: 'SUR',
            countryTranslateKey: 'COUNTRIES.NAME_SUR',
            originTranslateKey: 'COUNTRIES.ORIGIN_SUR',
            supportedProviders: [KycVendors.VERIFF]
        },
        // {  country: 'Svalbard and Jan Mayen',  code2: 'SJ',  code3: 'SJM',  countryTranslateKey: 'COUNTRIES.NAME_SJM',  originTranslateKey: 'COUNTRIES.ORIGIN_SJM'},
        // {  country: 'Swaziland',  code2: 'SZ',  code3: 'SWZ',  countryTranslateKey: 'COUNTRIES.NAME_SWZ',  originTranslateKey: 'COUNTRIES.ORIGIN_SWZ'},
        {
            country: 'Sweden',
            code2: 'SE',
            code3: 'SWE',
            countryTranslateKey: 'COUNTRIES.NAME_SWE',
            originTranslateKey: 'COUNTRIES.ORIGIN_SWE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Switzerland',
            code2: 'CH',
            code3: 'CHE',
            countryTranslateKey: 'COUNTRIES.NAME_CHE',
            originTranslateKey: 'COUNTRIES.ORIGIN_CHE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Syrian Arab Republic',  code2: 'SY',  code3: 'SYR',  countryTranslateKey: 'COUNTRIES.NAME_SYR',  originTranslateKey: 'COUNTRIES.ORIGIN_SYR'},
        {
            country: 'Taiwan (Province of China)',
            code2: 'TW',
            code3: 'TWN',
            countryTranslateKey: 'COUNTRIES.NAME_TWN',
            originTranslateKey: 'COUNTRIES.ORIGIN_TWN',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Tajikistan',
            code2: 'TJ',
            code3: 'TJK',
            countryTranslateKey: 'COUNTRIES.NAME_TJK',
            originTranslateKey: 'COUNTRIES.ORIGIN_TJK',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Tanzania, United Republic of',
            code2: 'TZ',
            code3: 'TZA',
            countryTranslateKey: 'COUNTRIES.NAME_TZA',
            originTranslateKey: 'COUNTRIES.ORIGIN_TZA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Thailand',
            code2: 'TH',
            code3: 'THA',
            countryTranslateKey: 'COUNTRIES.NAME_THA',
            originTranslateKey: 'COUNTRIES.ORIGIN_THA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Timor-Leste',
            code2: 'TL',
            code3: 'TLS',
            countryTranslateKey: 'COUNTRIES.NAME_TLS',
            originTranslateKey: 'COUNTRIES.ORIGIN_TLS',
            supportedProviders: [KycVendors.VERIFEYE]
        },
        {
            country: 'Togo',
            code2: 'TG',
            code3: 'TGO',
            countryTranslateKey: 'COUNTRIES.NAME_TGO',
            originTranslateKey: 'COUNTRIES.ORIGIN_TGO',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Tokelau',  code2: 'TK',  code3: 'TKL',  countryTranslateKey: 'COUNTRIES.NAME_TKL',  originTranslateKey: 'COUNTRIES.ORIGIN_TKL'},
        {
            country: 'Tonga',
            code2: 'TO',
            code3: 'TON',
            countryTranslateKey: 'COUNTRIES.NAME_TON',
            originTranslateKey: 'COUNTRIES.ORIGIN_TON',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Trinidad and Tobago',
            code2: 'TT',
            code3: 'TTO',
            countryTranslateKey: 'COUNTRIES.NAME_TTO',
            originTranslateKey: 'COUNTRIES.ORIGIN_TTO',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Tunisia',
            code2: 'TN',
            code3: 'TUN',
            countryTranslateKey: 'COUNTRIES.NAME_TUN',
            originTranslateKey: 'COUNTRIES.ORIGIN_TUN',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Turkey',
            code2: 'TR',
            code3: 'TUR',
            countryTranslateKey: 'COUNTRIES.NAME_TUR',
            originTranslateKey: 'COUNTRIES.ORIGIN_TUR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Turkmenistan',
            code2: 'TM',
            code3: 'TKM',
            countryTranslateKey: 'COUNTRIES.NAME_TKM',
            originTranslateKey: 'COUNTRIES.ORIGIN_TKM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Turks and Caicos Islands (the)',  code2: 'TC',  code3: 'TCA',  countryTranslateKey: 'COUNTRIES.NAME_TCA',  originTranslateKey: 'COUNTRIES.ORIGIN_TCA'},
        // {  country: 'Tuvalu',  code2: 'TV',  code3: 'TUV',  countryTranslateKey: 'COUNTRIES.NAME_TUV',  originTranslateKey: 'COUNTRIES.ORIGIN_TUV'},
        {
            country: 'Uganda',
            code2: 'UG',
            code3: 'UGA',
            countryTranslateKey: 'COUNTRIES.NAME_UGA',
            originTranslateKey: 'COUNTRIES.ORIGIN_UGA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Ukraine',
            code2: 'UA',
            code3: 'UKR',
            countryTranslateKey: 'COUNTRIES.NAME_UKR',
            originTranslateKey: 'COUNTRIES.ORIGIN_UKR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'United Arab Emirates (the)',
            code2: 'AE',
            code3: 'ARE',
            countryTranslateKey: 'COUNTRIES.NAME_ARE',
            originTranslateKey: 'COUNTRIES.ORIGIN_ARE',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'United Kingdom of Great Britain and Northern Ireland (the)',
            code2: 'GB',
            code3: 'GBR',
            countryTranslateKey: 'COUNTRIES.NAME_GBR',
            originTranslateKey: 'COUNTRIES.ORIGIN_GBR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'United States Minor Outlying Islands (the)',  code2: 'UM',  code3: 'UMI',  countryTranslateKey: 'COUNTRIES.NAME_UMI',  originTranslateKey: 'COUNTRIES.ORIGIN_UMI'},
        {
            country: 'United States of America (the)',
            code2: 'US',
            code3: 'USA',
            countryTranslateKey: 'COUNTRIES.NAME_USA',
            originTranslateKey: 'COUNTRIES.ORIGIN_USA',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Uruguay',
            code2: 'UY',
            code3: 'URY',
            countryTranslateKey: 'COUNTRIES.NAME_URY',
            originTranslateKey: 'COUNTRIES.ORIGIN_URY',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Uzbekistan',
            code2: 'UZ',
            code3: 'UZB',
            countryTranslateKey: 'COUNTRIES.NAME_UZB',
            originTranslateKey: 'COUNTRIES.ORIGIN_UZB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Vanuatu',
            code2: 'VU',
            code3: 'VUT',
            countryTranslateKey: 'COUNTRIES.NAME_VUT',
            originTranslateKey: 'COUNTRIES.ORIGIN_VUT',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Venezuela (Bolivarian Republic of)',
            code2: 'VE',
            code3: 'VEN',
            countryTranslateKey: 'COUNTRIES.NAME_VEN',
            originTranslateKey: 'COUNTRIES.ORIGIN_VEN',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Viet Nam',
            code2: 'VN',
            code3: 'VNM',
            countryTranslateKey: 'COUNTRIES.NAME_VNM',
            originTranslateKey: 'COUNTRIES.ORIGIN_VNM',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        {
            country: 'Virgin Islands (British)',
            code2: 'VG',
            code3: 'VGB',
            countryTranslateKey: 'COUNTRIES.NAME_VGB',
            originTranslateKey: 'COUNTRIES.ORIGIN_VGB',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Virgin Islands (U.S.)',
            code2: 'VI',
            code3: 'VIR',
            countryTranslateKey: 'COUNTRIES.NAME_VIR',
            originTranslateKey: 'COUNTRIES.ORIGIN_VIR',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        },
        // {  country: 'Wallis and Futuna',  code2: 'WF',  code3: 'WLF',  countryTranslateKey: 'COUNTRIES.NAME_WLF',  originTranslateKey: 'COUNTRIES.ORIGIN_WLF'},
        // {  country: 'Western Sahara',  code2: 'EH',  code3: 'ESH',  countryTranslateKey: 'COUNTRIES.NAME_ESH',  originTranslateKey: 'COUNTRIES.ORIGIN_ESH'},
        {
            country: 'Yemen',
            code2: 'YE',
            code3: 'YEM',
            countryTranslateKey: 'COUNTRIES.NAME_YEM',
            originTranslateKey: 'COUNTRIES.ORIGIN_YEM',
            supportedProviders: [KycVendors.VERIFF]
        },
        {
            country: 'Zambia',
            code2: 'ZM',
            code3: 'ZMB',
            countryTranslateKey: 'COUNTRIES.NAME_ZMB',
            originTranslateKey: 'COUNTRIES.ORIGIN_ZMB',
            supportedProviders: [KycVendors.VERIFF, KycVendors.VERIFEYE]
        }
        // ,{  country: 'Zimbabwe',  code2: 'ZW',  code3: 'ZWE',  countryTranslateKey: 'COUNTRIES.NAME_ZWE',  originTranslateKey: 'COUNTRIES.ORIGIN_ZWE'}
    ];

    public identityCountries: { country: string; code2: string; code3: string; countryTranslateKey: string; originTranslateKey: string; supportedDocuments: Array<ProviderDocument>}[] = [
        {
           "country":"American Samoa",
           "code2":"AS",
           "code3":"ASM",
           "countryTranslateKey":"COUNTRIES.NAME_ASM",
           "originTranslateKey":"COUNTRIES.ORIGIN_ASM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
            ]
        },
        {
           "country":"Antarctica",
           "code2":"AQ",
           "code3":"ATA",
           "countryTranslateKey":"COUNTRIES.NAME_ATA",
           "originTranslateKey":"COUNTRIES.ORIGIN_ATA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Armenia",
           "code2":"AM",
           "code3":"ARM",
           "countryTranslateKey":"COUNTRIES.NAME_ARM",
           "originTranslateKey":"COUNTRIES.ORIGIN_ARM",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Aruba",
           "code2":"AW",
           "code3":"ABW",
           "countryTranslateKey":"COUNTRIES.NAME_ABW",
           "originTranslateKey":"COUNTRIES.ORIGIN_ABW",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Austria",
           "code2":"AT",
           "code3":"AUT",
           "countryTranslateKey":"COUNTRIES.NAME_AUT",
           "originTranslateKey":"COUNTRIES.ORIGIN_AUT",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Azerbaijan",
           "code2":"AZ",
           "code3":"AZE",
           "countryTranslateKey":"COUNTRIES.NAME_AZE",
           "originTranslateKey":"COUNTRIES.ORIGIN_AZE",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bahamas (the)",
           "code2":"BS",
           "code3":"BHS",
           "countryTranslateKey":"COUNTRIES.NAME_BHS",
           "originTranslateKey":"COUNTRIES.ORIGIN_BHS",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Belarus",
           "code2":"BY",
           "code3":"BLR",
           "countryTranslateKey":"COUNTRIES.NAME_BLR",
           "originTranslateKey":"COUNTRIES.ORIGIN_BLR",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Belgium",
           "code2":"BE",
           "code3":"BEL",
           "countryTranslateKey":"COUNTRIES.NAME_BEL",
           "originTranslateKey":"COUNTRIES.ORIGIN_BEL",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Benin",
           "code2":"BJ",
           "code3":"BEN",
           "countryTranslateKey":"COUNTRIES.NAME_BEN",
           "originTranslateKey":"COUNTRIES.ORIGIN_BEN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bonaire, Sint Eustatius and Saba",
           "code2":"BQ",
           "code3":"BES",
           "countryTranslateKey":"COUNTRIES.NAME_BES",
           "originTranslateKey":"COUNTRIES.ORIGIN_BES",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
           ]
        },
        {
           "country":"Bouvet Island",
           "code2":"BV",
           "code3":"BVT",
           "countryTranslateKey":"COUNTRIES.NAME_BVT",
           "originTranslateKey":"COUNTRIES.ORIGIN_BVT",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Brazil",
           "code2":"BR",
           "code3":"BRA",
           "countryTranslateKey":"COUNTRIES.NAME_BRA",
           "originTranslateKey":"COUNTRIES.ORIGIN_BRA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"British Indian Ocean Territory (the)",
           "code2":"IO",
           "code3":"IOT",
           "countryTranslateKey":"COUNTRIES.NAME_IOT",
           "originTranslateKey":"COUNTRIES.ORIGIN_IOT",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Brunei Darussalam",
           "code2":"BN",
           "code3":"BRN",
           "countryTranslateKey":"COUNTRIES.NAME_BRN",
           "originTranslateKey":"COUNTRIES.ORIGIN_BRN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bulgaria",
           "code2":"BG",
           "code3":"BGR",
           "countryTranslateKey":"COUNTRIES.NAME_BGR",
           "originTranslateKey":"COUNTRIES.ORIGIN_BGR",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Cameroon",
           "code2":"CM",
           "code3":"CMR",
           "countryTranslateKey":"COUNTRIES.NAME_CMR",
           "originTranslateKey":"COUNTRIES.ORIGIN_CMR",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Canada",
           "code2":"CA",
           "code3":"CAN",
           "countryTranslateKey":"COUNTRIES.NAME_CAN",
           "originTranslateKey":"COUNTRIES.ORIGIN_CAN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
            "country": 'Cook Islands (the)',
            "code2": 'CK',
            "code3": 'COK',
            "countryTranslateKey": 'COUNTRIES.NAME_COK',
            "originTranslateKey": 'COUNTRIES.ORIGIN_COK',
            "supportedDocuments": [
                { provider: KycVendors.VERIFF, documents: [] }
                ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
               ]
        },
        {
            country: 'Tokelau',
            code2: 'TK',
            code3: 'TKL',
            countryTranslateKey: 'COUNTRIES.NAME_TKL',
            originTranslateKey: 'COUNTRIES.ORIGIN_TKL',
            "supportedDocuments": [
                { provider: KycVendors.VERIFF, documents: [] }
                ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
               ]
        },
        {
           "country":"Comoros (the)",
           "code2":"KM",
           "code3":"COM",
           "countryTranslateKey":"COUNTRIES.NAME_COM",
           "originTranslateKey":"COUNTRIES.ORIGIN_COM",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Costa Rica",
           "code2":"CR",
           "code3":"CRI",
           "countryTranslateKey":"COUNTRIES.NAME_CRI",
           "originTranslateKey":"COUNTRIES.ORIGIN_CRI",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Croatia",
           "code2":"HR",
           "code3":"HRV",
           "countryTranslateKey":"COUNTRIES.NAME_HRV",
           "originTranslateKey":"COUNTRIES.ORIGIN_HRV",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Curacao",
           "code2":"CW",
           "code3":"CUW",
           "countryTranslateKey":"COUNTRIES.NAME_CUW",
           "originTranslateKey":"COUNTRIES.ORIGIN_CUW",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Cyprus",
           "code2":"CY",
           "code3":"CYP",
           "countryTranslateKey":"COUNTRIES.NAME_CYP",
           "originTranslateKey":"COUNTRIES.ORIGIN_CYP",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Czech Republic (the)",
           "code2":"CZ",
           "code3":"CZE",
           "countryTranslateKey":"COUNTRIES.NAME_CZE",
           "originTranslateKey":"COUNTRIES.ORIGIN_CZE",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Denmark",
           "code2":"DK",
           "code3":"DNK",
           "countryTranslateKey":"COUNTRIES.NAME_DNK",
           "originTranslateKey":"COUNTRIES.ORIGIN_DNK",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"El Salvador",
           "code2":"SV",
           "code3":"SLV",
           "countryTranslateKey":"COUNTRIES.NAME_SLV",
           "originTranslateKey":"COUNTRIES.ORIGIN_SLV",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Estonia",
           "code2":"EE",
           "code3":"EST",
           "countryTranslateKey":"COUNTRIES.NAME_EST",
           "originTranslateKey":"COUNTRIES.ORIGIN_EST",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Faroe Islands (the)",
           "code2":"FO",
           "code3":"FRO",
           "countryTranslateKey":"COUNTRIES.NAME_FRO",
           "originTranslateKey":"COUNTRIES.ORIGIN_FRO",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Finland",
           "code2":"FI",
           "code3":"FIN",
           "countryTranslateKey":"COUNTRIES.NAME_FIN",
           "originTranslateKey":"COUNTRIES.ORIGIN_FIN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"France",
           "code2":"FR",
           "code3":"FRA",
           "countryTranslateKey":"COUNTRIES.NAME_FRA",
           "originTranslateKey":"COUNTRIES.ORIGIN_FRA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"French Guiana",
           "code2":"GF",
           "code3":"GUF",
           "countryTranslateKey":"COUNTRIES.NAME_GUF",
           "originTranslateKey":"COUNTRIES.ORIGIN_GUF",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"French Polynesia",
           "code2":"PF",
           "code3":"PYF",
           "countryTranslateKey":"COUNTRIES.NAME_PYF",
           "originTranslateKey":"COUNTRIES.ORIGIN_PYF",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"French Southern Territories (the)",
           "code2":"TF",
           "code3":"ATF",
           "countryTranslateKey":"COUNTRIES.NAME_ATF",
           "originTranslateKey":"COUNTRIES.ORIGIN_ATF",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Gabon",
           "code2":"GA",
           "code3":"GAB",
           "countryTranslateKey":"COUNTRIES.NAME_GAB",
           "originTranslateKey":"COUNTRIES.ORIGIN_GAB",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Gambia (the)",
           "code2":"GM",
           "code3":"GMB",
           "countryTranslateKey":"COUNTRIES.NAME_GMB",
           "originTranslateKey":"COUNTRIES.ORIGIN_GMB",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Georgia",
           "code2":"GE",
           "code3":"GEO",
           "countryTranslateKey":"COUNTRIES.NAME_GEO",
           "originTranslateKey":"COUNTRIES.ORIGIN_GEO",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Germany",
           "code2":"DE",
           "code3":"DEU",
           "countryTranslateKey":"COUNTRIES.NAME_DEU",
           "originTranslateKey":"COUNTRIES.ORIGIN_DEU",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Greece",
           "code2":"GR",
           "code3":"GRC",
           "countryTranslateKey":"COUNTRIES.NAME_GRC",
           "originTranslateKey":"COUNTRIES.ORIGIN_GRC",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Guam",
           "code2":"GU",
           "code3":"GUM",
           "countryTranslateKey":"COUNTRIES.NAME_GUM",
           "originTranslateKey":"COUNTRIES.ORIGIN_GUM",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT'] }
           ]
        },
        {
           "country":"Haiti",
           "code2":"HT",
           "code3":"HTI",
           "countryTranslateKey":"COUNTRIES.NAME_HTI",
           "originTranslateKey":"COUNTRIES.ORIGIN_HTI",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Heard Island and McDonald Islands",
           "code2":"HM",
           "code3":"HMD",
           "countryTranslateKey":"COUNTRIES.NAME_HMD",
           "originTranslateKey":"COUNTRIES.ORIGIN_HMD",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Holy See (the)",
           "code2":"VA",
           "code3":"VAT",
           "countryTranslateKey":"COUNTRIES.NAME_VAT",
           "originTranslateKey":"COUNTRIES.ORIGIN_VAT",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Honduras",
           "code2":"HN",
           "code3":"HND",
           "countryTranslateKey":"COUNTRIES.NAME_HND",
           "originTranslateKey":"COUNTRIES.ORIGIN_HND",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Hungary",
           "code2":"HU",
           "code3":"HUN",
           "countryTranslateKey":"COUNTRIES.NAME_HUN",
           "originTranslateKey":"COUNTRIES.ORIGIN_HUN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Iceland",
           "code2":"IS",
           "code3":"ISL",
           "countryTranslateKey":"COUNTRIES.NAME_ISL",
           "originTranslateKey":"COUNTRIES.ORIGIN_ISL",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Indonesia",
           "code2":"ID",
           "code3":"IDN",
           "countryTranslateKey":"COUNTRIES.NAME_IDN",
           "originTranslateKey":"COUNTRIES.ORIGIN_IDN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Iraq",
           "code2":"IQ",
           "code3":"IRQ",
           "countryTranslateKey":"COUNTRIES.NAME_IRQ",
           "originTranslateKey":"COUNTRIES.ORIGIN_IRQ",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Ireland",
           "code2":"IE",
           "code3":"IRL",
           "countryTranslateKey":"COUNTRIES.NAME_IRL",
           "originTranslateKey":"COUNTRIES.ORIGIN_IRL",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Italy",
           "code2":"IT",
           "code3":"ITA",
           "countryTranslateKey":"COUNTRIES.NAME_ITA",
           "originTranslateKey":"COUNTRIES.ORIGIN_ITA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Japan",
           "code2":"JP",
           "code3":"JPN",
           "countryTranslateKey":"COUNTRIES.NAME_JPN",
           "originTranslateKey":"COUNTRIES.ORIGIN_JPN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Jordan",
           "code2":"JO",
           "code3":"JOR",
           "countryTranslateKey":"COUNTRIES.NAME_JOR",
           "originTranslateKey":"COUNTRIES.ORIGIN_JOR",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Kazakhstan",
           "code2":"KZ",
           "code3":"KAZ",
           "countryTranslateKey":"COUNTRIES.NAME_KAZ",
           "originTranslateKey":"COUNTRIES.ORIGIN_KAZ",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Kenya",
           "code2":"KE",
           "code3":"KEN",
           "countryTranslateKey":"COUNTRIES.NAME_KEN",
           "originTranslateKey":"COUNTRIES.ORIGIN_KEN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Korea (the Republic of)",
           "code2":"KR",
           "code3":"KOR",
           "countryTranslateKey":"COUNTRIES.NAME_KOR",
           "originTranslateKey":"COUNTRIES.ORIGIN_KOR",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
            "country": 'Kosovo',
            "code2": 'XK',
            "code3": 'XKK',
            "countryTranslateKey": 'COUNTRIES.NAME_XKK',
            "originTranslateKey": 'COUNTRIES.ORIGIN_XKK',
            "supportedDocuments": [
                { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
                ,{ provider: KycVendors.VERIFEYE, documents: [] }
            ]
        },
        {
           "country":"Kuwait",
           "code2":"KW",
           "code3":"KWT",
           "countryTranslateKey":"COUNTRIES.NAME_KWT",
           "originTranslateKey":"COUNTRIES.ORIGIN_KWT",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Latvia",
           "code2":"LV",
           "code3":"LVA",
           "countryTranslateKey":"COUNTRIES.NAME_LVA",
           "originTranslateKey":"COUNTRIES.ORIGIN_LVA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Liechtenstein",
           "code2":"LI",
           "code3":"LIE",
           "countryTranslateKey":"COUNTRIES.NAME_LIE",
           "originTranslateKey":"COUNTRIES.ORIGIN_LIE",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Lithuania",
           "code2":"LT",
           "code3":"LTU",
           "countryTranslateKey":"COUNTRIES.NAME_LTU",
           "originTranslateKey":"COUNTRIES.ORIGIN_LTU",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Luxembourg",
           "code2":"LU",
           "code3":"LUX",
           "countryTranslateKey":"COUNTRIES.NAME_LUX",
           "originTranslateKey":"COUNTRIES.ORIGIN_LUX",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Macedonia (the former Yugoslav Republic of)",
           "code2":"MK",
           "code3":"MKD",
           "countryTranslateKey":"COUNTRIES.NAME_MKD",
           "originTranslateKey":"COUNTRIES.ORIGIN_MKD",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Malaysia",
           "code2":"MY",
           "code3":"MYS",
           "countryTranslateKey":"COUNTRIES.NAME_MYS",
           "originTranslateKey":"COUNTRIES.ORIGIN_MYS",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Malta",
           "code2":"MT",
           "code3":"MLT",
           "countryTranslateKey":"COUNTRIES.NAME_MLT",
           "originTranslateKey":"COUNTRIES.ORIGIN_MLT",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Mexico",
           "code2":"MX",
           "code3":"MEX",
           "countryTranslateKey":"COUNTRIES.NAME_MEX",
           "originTranslateKey":"COUNTRIES.ORIGIN_MEX",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Moldova (the Republic of)",
           "code2":"MD",
           "code3":"MDA",
           "countryTranslateKey":"COUNTRIES.NAME_MDA",
           "originTranslateKey":"COUNTRIES.ORIGIN_MDA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Monaco",
           "code2":"MC",
           "code3":"MCO",
           "countryTranslateKey":"COUNTRIES.NAME_MCO",
           "originTranslateKey":"COUNTRIES.ORIGIN_MCO",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Mongolia",
           "code2":"MN",
           "code3":"MNG",
           "countryTranslateKey":"COUNTRIES.NAME_MNG",
           "originTranslateKey":"COUNTRIES.ORIGIN_MNG",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Montenegro",
           "code2":"ME",
           "code3":"MNE",
           "countryTranslateKey":"COUNTRIES.NAME_MNE",
           "originTranslateKey":"COUNTRIES.ORIGIN_MNE",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Morocco",
           "code2":"MA",
           "code3":"MAR",
           "countryTranslateKey":"COUNTRIES.NAME_MAR",
           "originTranslateKey":"COUNTRIES.ORIGIN_MAR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Mozambique",
           "code2":"MZ",
           "code3":"MOZ",
           "countryTranslateKey":"COUNTRIES.NAME_MOZ",
           "originTranslateKey":"COUNTRIES.ORIGIN_MOZ",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Netherlands (the)",
           "code2":"NL",
           "code3":"NLD",
           "countryTranslateKey":"COUNTRIES.NAME_NLD",
           "originTranslateKey":"COUNTRIES.ORIGIN_NLD",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Nicaragua",
           "code2":"NI",
           "code3":"NIC",
           "countryTranslateKey":"COUNTRIES.NAME_NIC",
           "originTranslateKey":"COUNTRIES.ORIGIN_NIC",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Northern Mariana Islands (the)",
           "code2":"MP",
           "code3":"MNP",
           "countryTranslateKey":"COUNTRIES.NAME_MNP",
           "originTranslateKey":"COUNTRIES.ORIGIN_MNP",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Norway",
           "code2":"NO",
           "code3":"NOR",
           "countryTranslateKey":"COUNTRIES.NAME_NOR",
           "originTranslateKey":"COUNTRIES.ORIGIN_NOR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Oman",
           "code2":"OM",
           "code3":"OMN",
           "countryTranslateKey":"COUNTRIES.NAME_OMN",
           "originTranslateKey":"COUNTRIES.ORIGIN_OMN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Panama",
           "code2":"PA",
           "code3":"PAN",
           "countryTranslateKey":"COUNTRIES.NAME_PAN",
           "originTranslateKey":"COUNTRIES.ORIGIN_PAN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Paraguay",
           "code2":"PY",
           "code3":"PRY",
           "countryTranslateKey":"COUNTRIES.NAME_PRY",
           "originTranslateKey":"COUNTRIES.ORIGIN_PRY",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Peru",
           "code2":"PE",
           "code3":"PER",
           "countryTranslateKey":"COUNTRIES.NAME_PER",
           "originTranslateKey":"COUNTRIES.ORIGIN_PER",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Pitcairn",
           "code2":"PN",
           "code3":"PCN",
           "countryTranslateKey":"COUNTRIES.NAME_PCN",
           "originTranslateKey":"COUNTRIES.ORIGIN_PCN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Poland",
           "code2":"PL",
           "code3":"POL",
           "countryTranslateKey":"COUNTRIES.NAME_POL",
           "originTranslateKey":"COUNTRIES.ORIGIN_POL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Portugal",
           "code2":"PT",
           "code3":"PRT",
           "countryTranslateKey":"COUNTRIES.NAME_PRT",
           "originTranslateKey":"COUNTRIES.ORIGIN_PRT",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Puerto Rico",
           "code2":"PR",
           "code3":"PRI",
           "countryTranslateKey":"COUNTRIES.NAME_PRI",
           "originTranslateKey":"COUNTRIES.ORIGIN_PRI",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Qatar",
           "code2":"QA",
           "code3":"QAT",
           "countryTranslateKey":"COUNTRIES.NAME_QAT",
           "originTranslateKey":"COUNTRIES.ORIGIN_QAT",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Réunion",
           "code2":"RE",
           "code3":"REU",
           "countryTranslateKey":"COUNTRIES.NAME_REU",
           "originTranslateKey":"COUNTRIES.ORIGIN_REU",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Romania",
           "code2":"RO",
           "code3":"ROU",
           "countryTranslateKey":"COUNTRIES.NAME_ROU",
           "originTranslateKey":"COUNTRIES.ORIGIN_ROU",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Russian Federation (the)",
           "code2":"RU",
           "code3":"RUS",
           "countryTranslateKey":"COUNTRIES.NAME_RUS",
           "originTranslateKey":"COUNTRIES.ORIGIN_RUS",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Rwanda",
           "code2":"RW",
           "code3":"RWA",
           "countryTranslateKey":"COUNTRIES.NAME_RWA",
           "originTranslateKey":"COUNTRIES.ORIGIN_RWA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Saint Barthélemy",
           "code2":"BL",
           "code3":"BLM",
           "countryTranslateKey":"COUNTRIES.NAME_BLM",
           "originTranslateKey":"COUNTRIES.ORIGIN_BLM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Saint Pierre and Miquelon",
           "code2":"PM",
           "code3":"SPM",
           "countryTranslateKey":"COUNTRIES.NAME_SPM",
           "originTranslateKey":"COUNTRIES.ORIGIN_SPM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Singapore",
           "code2":"SG",
           "code3":"SGP",
           "countryTranslateKey":"COUNTRIES.NAME_SGP",
           "originTranslateKey":"COUNTRIES.ORIGIN_SGP",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Slovakia",
           "code2":"SK",
           "code3":"SVK",
           "countryTranslateKey":"COUNTRIES.NAME_SVK",
           "originTranslateKey":"COUNTRIES.ORIGIN_SVK",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Slovenia",
           "code2":"SI",
           "code3":"SVN",
           "countryTranslateKey":"COUNTRIES.NAME_SVN",
           "originTranslateKey":"COUNTRIES.ORIGIN_SVN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"South Georgia and the South Sandwich Islands",
           "code2":"GS",
           "code3":"SGS",
           "countryTranslateKey":"COUNTRIES.NAME_SGS",
           "originTranslateKey":"COUNTRIES.ORIGIN_SGS",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Spain",
           "code2":"ES",
           "code3":"ESP",
           "countryTranslateKey":"COUNTRIES.NAME_ESP",
           "originTranslateKey":"COUNTRIES.ORIGIN_ESP",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Svalbard and Jan Mayen",
           "code2":"SJ",
           "code3":"SJM",
           "countryTranslateKey":"COUNTRIES.NAME_SJM",
           "originTranslateKey":"COUNTRIES.ORIGIN_SJM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Sweden",
           "code2":"SE",
           "code3":"SWE",
           "countryTranslateKey":"COUNTRIES.NAME_SWE",
           "originTranslateKey":"COUNTRIES.ORIGIN_SWE",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Switzerland",
           "code2":"CH",
           "code3":"CHE",
           "countryTranslateKey":"COUNTRIES.NAME_CHE",
           "originTranslateKey":"COUNTRIES.ORIGIN_CHE",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Taiwan (Province of China)",
           "code2":"TW",
           "code3":"TWN",
           "countryTranslateKey":"COUNTRIES.NAME_TWN",
           "originTranslateKey":"COUNTRIES.ORIGIN_TWN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Tajikistan",
           "code2":"TJ",
           "code3":"TJK",
           "countryTranslateKey":"COUNTRIES.NAME_TJK",
           "originTranslateKey":"COUNTRIES.ORIGIN_TJK",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Turkey",
           "code2":"TR",
           "code3":"TUR",
           "countryTranslateKey":"COUNTRIES.NAME_TUR",
           "originTranslateKey":"COUNTRIES.ORIGIN_TUR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Ukraine",
           "code2":"UA",
           "code3":"UKR",
           "countryTranslateKey":"COUNTRIES.NAME_UKR",
           "originTranslateKey":"COUNTRIES.ORIGIN_UKR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"United Kingdom of Great Britain and Northern Ireland (the)",
           "code2":"GB",
           "code3":"GBR",
           "countryTranslateKey":"COUNTRIES.NAME_GBR",
           "originTranslateKey":"COUNTRIES.ORIGIN_GBR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"United States Minor Outlying Islands (the)",
           "code2":"UM",
           "code3":"UMI",
           "countryTranslateKey":"COUNTRIES.NAME_UMI",
           "originTranslateKey":"COUNTRIES.ORIGIN_UMI",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"United States of America (the)",
           "code2":"US",
           "code3":"USA",
           "countryTranslateKey":"COUNTRIES.NAME_USA",
           "originTranslateKey":"COUNTRIES.ORIGIN_USA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Uzbekistan",
           "code2":"UZ",
           "code3":"UZB",
           "countryTranslateKey":"COUNTRIES.NAME_UZB",
           "originTranslateKey":"COUNTRIES.ORIGIN_UZB",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT','RESIDENCE_PERMIT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Viet Nam",
           "code2":"VN",
           "code3":"VNM",
           "countryTranslateKey":"COUNTRIES.NAME_VNM",
           "originTranslateKey":"COUNTRIES.ORIGIN_VNM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Virgin Islands (U.S.)",
           "code2":"VI",
           "code3":"VIR",
           "countryTranslateKey":"COUNTRIES.NAME_VIR",
           "originTranslateKey":"COUNTRIES.ORIGIN_VIR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Western Sahara",
           "code2":"EH",
           "code3":"ESH",
           "countryTranslateKey":"COUNTRIES.NAME_ESH",
           "originTranslateKey":"COUNTRIES.ORIGIN_ESH",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT','RESIDENCE_PERMIT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Afghanistan",
           "code2":"AF",
           "code3":"AFG",
           "countryTranslateKey":"COUNTRIES.NAME_AFG",
           "originTranslateKey":"COUNTRIES.ORIGIN_AFG",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Albania",
           "code2":"AL",
           "code3":"ALB",
           "countryTranslateKey":"COUNTRIES.NAME_ALB",
           "originTranslateKey":"COUNTRIES.ORIGIN_ALB",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Algeria",
           "code2":"DZ",
           "code3":"DZA",
           "countryTranslateKey":"COUNTRIES.NAME_DZA",
           "originTranslateKey":"COUNTRIES.ORIGIN_DZA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Andorra",
           "code2":"AD",
           "code3":"AND",
           "countryTranslateKey":"COUNTRIES.NAME_AND",
           "originTranslateKey":"COUNTRIES.ORIGIN_AND",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Angola",
           "code2":"AO",
           "code3":"AGO",
           "countryTranslateKey":"COUNTRIES.NAME_AGO",
           "originTranslateKey":"COUNTRIES.ORIGIN_AGO",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Anguilla",
           "code2":"AI",
           "code3":"AIA",
           "countryTranslateKey":"COUNTRIES.NAME_AIA",
           "originTranslateKey":"COUNTRIES.ORIGIN_AIA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Antigua and Barbuda",
           "code2":"AG",
           "code3":"ATG",
           "countryTranslateKey":"COUNTRIES.NAME_ATG",
           "originTranslateKey":"COUNTRIES.ORIGIN_ATG",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Argentina",
           "code2":"AR",
           "code3":"ARG",
           "countryTranslateKey":"COUNTRIES.NAME_ARG",
           "originTranslateKey":"COUNTRIES.ORIGIN_ARG",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Australia",
           "code2":"AU",
           "code3":"AUS",
           "countryTranslateKey":"COUNTRIES.NAME_AUS",
           "originTranslateKey":"COUNTRIES.ORIGIN_AUS",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bahrain",
           "code2":"BH",
           "code3":"BHR",
           "countryTranslateKey":"COUNTRIES.NAME_BHR",
           "originTranslateKey":"COUNTRIES.ORIGIN_BHR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bangladesh",
           "code2":"BD",
           "code3":"BGD",
           "countryTranslateKey":"COUNTRIES.NAME_BGD",
           "originTranslateKey":"COUNTRIES.ORIGIN_BGD",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Barbados",
           "code2":"BB",
           "code3":"BRB",
           "countryTranslateKey":"COUNTRIES.NAME_BRB",
           "originTranslateKey":"COUNTRIES.ORIGIN_BRB",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Belize",
           "code2":"BZ",
           "code3":"BLZ",
           "countryTranslateKey":"COUNTRIES.NAME_BLZ",
           "originTranslateKey":"COUNTRIES.ORIGIN_BLZ",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bermuda",
           "code2":"BM",
           "code3":"BMU",
           "countryTranslateKey":"COUNTRIES.NAME_BMU",
           "originTranslateKey":"COUNTRIES.ORIGIN_BMU",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Bhutan",
           "code2":"BT",
           "code3":"BTN",
           "countryTranslateKey":"COUNTRIES.NAME_BTN",
           "originTranslateKey":"COUNTRIES.ORIGIN_BTN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bolivia (Plurinational State of)",
           "code2":"BO",
           "code3":"BOL",
           "countryTranslateKey":"COUNTRIES.NAME_BOL",
           "originTranslateKey":"COUNTRIES.ORIGIN_BOL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Bosnia and Herzegovina",
           "code2":"BA",
           "code3":"BIH",
           "countryTranslateKey":"COUNTRIES.NAME_BIH",
           "originTranslateKey":"COUNTRIES.ORIGIN_BIH",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Botswana",
           "code2":"BW",
           "code3":"BWA",
           "countryTranslateKey":"COUNTRIES.NAME_BWA",
           "originTranslateKey":"COUNTRIES.ORIGIN_BWA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Burkina Faso",
           "code2":"BF",
           "code3":"BFA",
           "countryTranslateKey":"COUNTRIES.NAME_BFA",
           "originTranslateKey":"COUNTRIES.ORIGIN_BFA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Burundi",
           "code2":"BI",
           "code3":"BDI",
           "countryTranslateKey":"COUNTRIES.NAME_BDI",
           "originTranslateKey":"COUNTRIES.ORIGIN_BDI",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Cabo Verde",
           "code2":"CV",
           "code3":"CPV",
           "countryTranslateKey":"COUNTRIES.NAME_CPV",
           "originTranslateKey":"COUNTRIES.ORIGIN_CPV",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Cambodia",
           "code2":"KH",
           "code3":"KHM",
           "countryTranslateKey":"COUNTRIES.NAME_KHM",
           "originTranslateKey":"COUNTRIES.ORIGIN_KHM",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Cayman Islands (the)",
           "code2":"KY",
           "code3":"CYM",
           "countryTranslateKey":"COUNTRIES.NAME_CYM",
           "originTranslateKey":"COUNTRIES.ORIGIN_CYM",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Central African Republic (the)",
           "code2":"CF",
           "code3":"CAF",
           "countryTranslateKey":"COUNTRIES.NAME_CAF",
           "originTranslateKey":"COUNTRIES.ORIGIN_CAF",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Chad",
           "code2":"TD",
           "code3":"TCD",
           "countryTranslateKey":"COUNTRIES.NAME_TCD",
           "originTranslateKey":"COUNTRIES.ORIGIN_TCD",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Chile",
           "code2":"CL",
           "code3":"CHL",
           "countryTranslateKey":"COUNTRIES.NAME_CHL",
           "originTranslateKey":"COUNTRIES.ORIGIN_CHL",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"China",
           "code2":"CN",
           "code3":"CHN",
           "countryTranslateKey":"COUNTRIES.NAME_CHN",
           "originTranslateKey":"COUNTRIES.ORIGIN_CHN",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Colombia",
           "code2":"CO",
           "code3":"COL",
           "countryTranslateKey":"COUNTRIES.NAME_COL",
           "originTranslateKey":"COUNTRIES.ORIGIN_COL",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Congo (the Democratic Republic of the)",
           "code2":"CD",
           "code3":"COD",
           "countryTranslateKey":"COUNTRIES.NAME_COD",
           "originTranslateKey":"COUNTRIES.ORIGIN_COD",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Congo (the)",
           "code2":"CG",
           "code3":"COG",
           "countryTranslateKey":"COUNTRIES.NAME_COG",
           "originTranslateKey":"COUNTRIES.ORIGIN_COG",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Cote d`Ivoire",
           "code2":"CI",
           "code3":"CIV",
           "countryTranslateKey":"COUNTRIES.NAME_CIV",
           "originTranslateKey":"COUNTRIES.ORIGIN_CIV",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Cuba",
           "code2":"CU",
           "code3":"CUB",
           "countryTranslateKey":"COUNTRIES.NAME_CUB",
           "originTranslateKey":"COUNTRIES.ORIGIN_CUB",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Djibouti",
           "code2":"DJ",
           "code3":"DJI",
           "countryTranslateKey":"COUNTRIES.NAME_DJI",
           "originTranslateKey":"COUNTRIES.ORIGIN_DJI",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Dominica",
           "code2":"DM",
           "code3":"DMA",
           "countryTranslateKey":"COUNTRIES.NAME_DMA",
           "originTranslateKey":"COUNTRIES.ORIGIN_DMA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Dominican Republic (the)",
           "code2":"DO",
           "code3":"DOM",
           "countryTranslateKey":"COUNTRIES.NAME_DOM",
           "originTranslateKey":"COUNTRIES.ORIGIN_DOM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Ecuador",
           "code2":"EC",
           "code3":"ECU",
           "countryTranslateKey":"COUNTRIES.NAME_ECU",
           "originTranslateKey":"COUNTRIES.ORIGIN_ECU",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Egypt",
           "code2":"EG",
           "code3":"EGY",
           "countryTranslateKey":"COUNTRIES.NAME_EGY",
           "originTranslateKey":"COUNTRIES.ORIGIN_EGY",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Equatorial Guinea",
           "code2":"GQ",
           "code3":"GNQ",
           "countryTranslateKey":"COUNTRIES.NAME_GNQ",
           "originTranslateKey":"COUNTRIES.ORIGIN_GNQ",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Eritrea",
           "code2":"ER",
           "code3":"ERI",
           "countryTranslateKey":"COUNTRIES.NAME_ERI",
           "originTranslateKey":"COUNTRIES.ORIGIN_ERI",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Ethiopia",
           "code2":"ET",
           "code3":"ETH",
           "countryTranslateKey":"COUNTRIES.NAME_ETH",
           "originTranslateKey":"COUNTRIES.ORIGIN_ETH",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Falkland Islands (the) [Malvinas]",
           "code2":"FK",
           "code3":"FLK",
           "countryTranslateKey":"COUNTRIES.NAME_FLK",
           "originTranslateKey":"COUNTRIES.ORIGIN_FLK",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Fiji",
           "code2":"FJ",
           "code3":"FJI",
           "countryTranslateKey":"COUNTRIES.NAME_FJI",
           "originTranslateKey":"COUNTRIES.ORIGIN_FJI",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Ghana",
           "code2":"GH",
           "code3":"GHA",
           "countryTranslateKey":"COUNTRIES.NAME_GHA",
           "originTranslateKey":"COUNTRIES.ORIGIN_GHA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Gibraltar",
           "code2":"GI",
           "code3":"GIB",
           "countryTranslateKey":"COUNTRIES.NAME_GIB",
           "originTranslateKey":"COUNTRIES.ORIGIN_GIB",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT'] }
           ]
        },
        {
           "country":"Greenland",
           "code2":"GL",
           "code3":"GRL",
           "countryTranslateKey":"COUNTRIES.NAME_GRL",
           "originTranslateKey":"COUNTRIES.ORIGIN_GRL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Grenada",
           "code2":"GD",
           "code3":"GRD",
           "countryTranslateKey":"COUNTRIES.NAME_GRD",
           "originTranslateKey":"COUNTRIES.ORIGIN_GRD",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Guadeloupe",
           "code2":"GP",
           "code3":"GLP",
           "countryTranslateKey":"COUNTRIES.NAME_GLP",
           "originTranslateKey":"COUNTRIES.ORIGIN_GLP",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT'] }
           ]
        },
        {
           "country":"Guatemala",
           "code2":"GT",
           "code3":"GTM",
           "countryTranslateKey":"COUNTRIES.NAME_GTM",
           "originTranslateKey":"COUNTRIES.ORIGIN_GTM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Guernsey",
           "code2":"GG",
           "code3":"GGY",
           "countryTranslateKey":"COUNTRIES.NAME_GGY",
           "originTranslateKey":"COUNTRIES.ORIGIN_GGY",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Guinea",
           "code2":"GN",
           "code3":"GIN",
           "countryTranslateKey":"COUNTRIES.NAME_GIN",
           "originTranslateKey":"COUNTRIES.ORIGIN_GIN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Guinea-Bissau",
           "code2":"GW",
           "code3":"GNB",
           "countryTranslateKey":"COUNTRIES.NAME_GNB",
           "originTranslateKey":"COUNTRIES.ORIGIN_GNB",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Guyana",
           "code2":"GY",
           "code3":"GUY",
           "countryTranslateKey":"COUNTRIES.NAME_GUY",
           "originTranslateKey":"COUNTRIES.ORIGIN_GUY",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Hong Kong",
           "code2":"HK",
           "code3":"HKG",
           "countryTranslateKey":"COUNTRIES.NAME_HKG",
           "originTranslateKey":"COUNTRIES.ORIGIN_HKG",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"India",
           "code2":"IN",
           "code3":"IND",
           "countryTranslateKey":"COUNTRIES.NAME_IND",
           "originTranslateKey":"COUNTRIES.ORIGIN_IND",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Iran (Islamic Republic of)",
           "code2":"IR",
           "code3":"IRN",
           "countryTranslateKey":"COUNTRIES.NAME_IRN",
           "originTranslateKey":"COUNTRIES.ORIGIN_IRN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Isle of Man",
           "code2":"IM",
           "code3":"IMN",
           "countryTranslateKey":"COUNTRIES.NAME_IMN",
           "originTranslateKey":"COUNTRIES.ORIGIN_IMN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Israel",
           "code2":"IL",
           "code3":"ISR",
           "countryTranslateKey":"COUNTRIES.NAME_ISR",
           "originTranslateKey":"COUNTRIES.ORIGIN_ISR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Jamaica",
           "code2":"JM",
           "code3":"JAM",
           "countryTranslateKey":"COUNTRIES.NAME_JAM",
           "originTranslateKey":"COUNTRIES.ORIGIN_JAM",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Jersey",
           "code2":"JE",
           "code3":"JEY",
           "countryTranslateKey":"COUNTRIES.NAME_JEY",
           "originTranslateKey":"COUNTRIES.ORIGIN_JEY",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Kiribati",
           "code2":"KI",
           "code3":"KIR",
           "countryTranslateKey":"COUNTRIES.NAME_KIR",
           "originTranslateKey":"COUNTRIES.ORIGIN_KIR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Korea (the Democratic People`s Republic of)",
           "code2":"KP",
           "code3":"PRK",
           "countryTranslateKey":"COUNTRIES.NAME_PRK",
           "originTranslateKey":"COUNTRIES.ORIGIN_PRK",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Kyrgyzstan",
           "code2":"KG",
           "code3":"KGZ",
           "countryTranslateKey":"COUNTRIES.NAME_KGZ",
           "originTranslateKey":"COUNTRIES.ORIGIN_KGZ",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Lao People`s Democratic Republic",
           "code2":"LA",
           "code3":"LAO",
           "countryTranslateKey":"COUNTRIES.NAME_LAO",
           "originTranslateKey":"COUNTRIES.ORIGIN_LAO",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Lebanon",
           "code2":"LB",
           "code3":"LBN",
           "countryTranslateKey":"COUNTRIES.NAME_LBN",
           "originTranslateKey":"COUNTRIES.ORIGIN_LBN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Lesotho",
           "code2":"LS",
           "code3":"LSO",
           "countryTranslateKey":"COUNTRIES.NAME_LSO",
           "originTranslateKey":"COUNTRIES.ORIGIN_LSO",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Liberia",
           "code2":"LR",
           "code3":"LBR",
           "countryTranslateKey":"COUNTRIES.NAME_LBR",
           "originTranslateKey":"COUNTRIES.ORIGIN_LBR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Libya",
           "code2":"LY",
           "code3":"LBY",
           "countryTranslateKey":"COUNTRIES.NAME_LBY",
           "originTranslateKey":"COUNTRIES.ORIGIN_LBY",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Macao",
           "code2":"MO",
           "code3":"MAC",
           "countryTranslateKey":"COUNTRIES.NAME_MAC",
           "originTranslateKey":"COUNTRIES.ORIGIN_MAC",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Madagascar",
           "code2":"MG",
           "code3":"MDG",
           "countryTranslateKey":"COUNTRIES.NAME_MDG",
           "originTranslateKey":"COUNTRIES.ORIGIN_MDG",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Malawi",
           "code2":"MW",
           "code3":"MWI",
           "countryTranslateKey":"COUNTRIES.NAME_MWI",
           "originTranslateKey":"COUNTRIES.ORIGIN_MWI",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Maldives",
           "code2":"MV",
           "code3":"MDV",
           "countryTranslateKey":"COUNTRIES.NAME_MDV",
           "originTranslateKey":"COUNTRIES.ORIGIN_MDV",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Mali",
           "code2":"ML",
           "code3":"MLI",
           "countryTranslateKey":"COUNTRIES.NAME_MLI",
           "originTranslateKey":"COUNTRIES.ORIGIN_MLI",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Marshall Islands (the)",
           "code2":"MH",
           "code3":"MHL",
           "countryTranslateKey":"COUNTRIES.NAME_MHL",
           "originTranslateKey":"COUNTRIES.ORIGIN_MHL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Martinique",
           "code2":"MQ",
           "code3":"MTQ",
           "countryTranslateKey":"COUNTRIES.NAME_MTQ",
           "originTranslateKey":"COUNTRIES.ORIGIN_MTQ",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Mauritania",
           "code2":"MR",
           "code3":"MRT",
           "countryTranslateKey":"COUNTRIES.NAME_MRT",
           "originTranslateKey":"COUNTRIES.ORIGIN_MRT",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Mauritius",
           "code2":"MU",
           "code3":"MUS",
           "countryTranslateKey":"COUNTRIES.NAME_MUS",
           "originTranslateKey":"COUNTRIES.ORIGIN_MUS",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Mayotte",
           "code2":"YT",
           "code3":"MYT",
           "countryTranslateKey":"COUNTRIES.NAME_MYT",
           "originTranslateKey":"COUNTRIES.ORIGIN_MYT",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Micronesia (Federated States of)",
           "code2":"FM",
           "code3":"FSM",
           "countryTranslateKey":"COUNTRIES.NAME_FSM",
           "originTranslateKey":"COUNTRIES.ORIGIN_FSM",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Montserrat",
           "code2":"MS",
           "code3":"MSR",
           "countryTranslateKey":"COUNTRIES.NAME_MSR",
           "originTranslateKey":"COUNTRIES.ORIGIN_MSR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Myanmar",
           "code2":"MM",
           "code3":"MMR",
           "countryTranslateKey":"COUNTRIES.NAME_MMR",
           "originTranslateKey":"COUNTRIES.ORIGIN_MMR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Namibia",
           "code2":"NA",
           "code3":"NAM",
           "countryTranslateKey":"COUNTRIES.NAME_NAM",
           "originTranslateKey":"COUNTRIES.ORIGIN_NAM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Nauru",
           "code2":"NR",
           "code3":"NRU",
           "countryTranslateKey":"COUNTRIES.NAME_NRU",
           "originTranslateKey":"COUNTRIES.ORIGIN_NRU",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Nepal",
           "code2":"NP",
           "code3":"NPL",
           "countryTranslateKey":"COUNTRIES.NAME_NPL",
           "originTranslateKey":"COUNTRIES.ORIGIN_NPL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"New Caledonia",
           "code2":"NC",
           "code3":"NCL",
           "countryTranslateKey":"COUNTRIES.NAME_NCL",
           "originTranslateKey":"COUNTRIES.ORIGIN_NCL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"New Zealand",
           "code2":"NZ",
           "code3":"NZL",
           "countryTranslateKey":"COUNTRIES.NAME_NZL",
           "originTranslateKey":"COUNTRIES.ORIGIN_NZL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Niger (the)",
           "code2":"NE",
           "code3":"NER",
           "countryTranslateKey":"COUNTRIES.NAME_NER",
           "originTranslateKey":"COUNTRIES.ORIGIN_NER",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Nigeria",
           "code2":"NG",
           "code3":"NGA",
           "countryTranslateKey":"COUNTRIES.NAME_NGA",
           "originTranslateKey":"COUNTRIES.ORIGIN_NGA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Norfolk Island",
           "code2":"NF",
           "code3":"NFK",
           "countryTranslateKey":"COUNTRIES.NAME_NFK",
           "originTranslateKey":"COUNTRIES.ORIGIN_NFK",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Pakistan",
           "code2":"PK",
           "code3":"PAK",
           "countryTranslateKey":"COUNTRIES.NAME_PAK",
           "originTranslateKey":"COUNTRIES.ORIGIN_PAK",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Palau",
           "code2":"PW",
           "code3":"PLW",
           "countryTranslateKey":"COUNTRIES.NAME_PLW",
           "originTranslateKey":"COUNTRIES.ORIGIN_PLW",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Palestine, State of",
           "code2":"PS",
           "code3":"PSE",
           "countryTranslateKey":"COUNTRIES.NAME_PSE",
           "originTranslateKey":"COUNTRIES.ORIGIN_PSE",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Papua New Guinea",
           "code2":"PG",
           "code3":"PNG",
           "countryTranslateKey":"COUNTRIES.NAME_PNG",
           "originTranslateKey":"COUNTRIES.ORIGIN_PNG",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Philippines (the)",
           "code2":"PH",
           "code3":"PHL",
           "countryTranslateKey":"COUNTRIES.NAME_PHL",
           "originTranslateKey":"COUNTRIES.ORIGIN_PHL",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Saint Helena, Ascension and Tristan da Cunha",
           "code2":"SH",
           "code3":"SHN",
           "countryTranslateKey":"COUNTRIES.NAME_SHN",
           "originTranslateKey":"COUNTRIES.ORIGIN_SHN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Saint Kitts and Nevis",
           "code2":"KN",
           "code3":"KNA",
           "countryTranslateKey":"COUNTRIES.NAME_KNA",
           "originTranslateKey":"COUNTRIES.ORIGIN_KNA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Saint Lucia",
           "code2":"LC",
           "code3":"LCA",
           "countryTranslateKey":"COUNTRIES.NAME_LCA",
           "originTranslateKey":"COUNTRIES.ORIGIN_LCA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Saint Martin (French part)",
           "code2":"MF",
           "code3":"MAF",
           "countryTranslateKey":"COUNTRIES.NAME_MAF",
           "originTranslateKey":"COUNTRIES.ORIGIN_MAF",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT'] }
           ]
        },
        {
           "country":"Saint Vincent and the Grenadines",
           "code2":"VC",
           "code3":"VCT",
           "countryTranslateKey":"COUNTRIES.NAME_VCT",
           "originTranslateKey":"COUNTRIES.ORIGIN_VCT",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Samoa",
           "code2":"WS",
           "code3":"WSM",
           "countryTranslateKey":"COUNTRIES.NAME_WSM",
           "originTranslateKey":"COUNTRIES.ORIGIN_WSM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"San Marino",
           "code2":"SM",
           "code3":"SMR",
           "countryTranslateKey":"COUNTRIES.NAME_SMR",
           "originTranslateKey":"COUNTRIES.ORIGIN_SMR",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Sao Tome and Principe",
           "code2":"ST",
           "code3":"STP",
           "countryTranslateKey":"COUNTRIES.NAME_STP",
           "originTranslateKey":"COUNTRIES.ORIGIN_STP",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Saudi Arabia",
           "code2":"SA",
           "code3":"SAU",
           "countryTranslateKey":"COUNTRIES.NAME_SAU",
           "originTranslateKey":"COUNTRIES.ORIGIN_SAU",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Senegal",
           "code2":"SN",
           "code3":"SEN",
           "countryTranslateKey":"COUNTRIES.NAME_SEN",
           "originTranslateKey":"COUNTRIES.ORIGIN_SEN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Serbia",
           "code2":"RS",
           "code3":"SRB",
           "countryTranslateKey":"COUNTRIES.NAME_SRB",
           "originTranslateKey":"COUNTRIES.ORIGIN_SRB",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Seychelles",
           "code2":"SC",
           "code3":"SYC",
           "countryTranslateKey":"COUNTRIES.NAME_SYC",
           "originTranslateKey":"COUNTRIES.ORIGIN_SYC",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Sierra Leone",
           "code2":"SL",
           "code3":"SLE",
           "countryTranslateKey":"COUNTRIES.NAME_SLE",
           "originTranslateKey":"COUNTRIES.ORIGIN_SLE",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Sint Maarten (Dutch part)",
           "code2":"SX",
           "code3":"SXM",
           "countryTranslateKey":"COUNTRIES.NAME_SXM",
           "originTranslateKey":"COUNTRIES.ORIGIN_SXM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT'] }
           ]
        },
        {
           "country":"Solomon Islands",
           "code2":"SB",
           "code3":"SLB",
           "countryTranslateKey":"COUNTRIES.NAME_SLB",
           "originTranslateKey":"COUNTRIES.ORIGIN_SLB",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Somalia",
           "code2":"SO",
           "code3":"SOM",
           "countryTranslateKey":"COUNTRIES.NAME_SOM",
           "originTranslateKey":"COUNTRIES.ORIGIN_SOM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"South Africa",
           "code2":"ZA",
           "code3":"ZAF",
           "countryTranslateKey":"COUNTRIES.NAME_ZAF",
           "originTranslateKey":"COUNTRIES.ORIGIN_ZAF",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"South Sudan",
           "code2":"SS",
           "code3":"SSD",
           "countryTranslateKey":"COUNTRIES.NAME_SSD",
           "originTranslateKey":"COUNTRIES.ORIGIN_SSD",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Sri Lanka",
           "code2":"LK",
           "code3":"LKA",
           "countryTranslateKey":"COUNTRIES.NAME_LKA",
           "originTranslateKey":"COUNTRIES.ORIGIN_LKA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Sudan (the)",
           "code2":"SD",
           "code3":"SDN",
           "countryTranslateKey":"COUNTRIES.NAME_SDN",
           "originTranslateKey":"COUNTRIES.ORIGIN_SDN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Suriname",
           "code2":"SR",
           "code3":"SUR",
           "countryTranslateKey":"COUNTRIES.NAME_SUR",
           "originTranslateKey":"COUNTRIES.ORIGIN_SUR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Swaziland",
           "code2":"SZ",
           "code3":"SWZ",
           "countryTranslateKey":"COUNTRIES.NAME_SWZ",
           "originTranslateKey":"COUNTRIES.ORIGIN_SWZ",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Syrian Arab Republic",
           "code2":"SY",
           "code3":"SYR",
           "countryTranslateKey":"COUNTRIES.NAME_SYR",
           "originTranslateKey":"COUNTRIES.ORIGIN_SYR",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Tanzania, United Republic of",
           "code2":"TZ",
           "code3":"TZA",
           "countryTranslateKey":"COUNTRIES.NAME_TZA",
           "originTranslateKey":"COUNTRIES.ORIGIN_TZA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Thailand",
           "code2":"TH",
           "code3":"THA",
           "countryTranslateKey":"COUNTRIES.NAME_THA",
           "originTranslateKey":"COUNTRIES.ORIGIN_THA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Timor-Leste",
           "code2":"TL",
           "code3":"TLS",
           "countryTranslateKey":"COUNTRIES.NAME_TLS",
           "originTranslateKey":"COUNTRIES.ORIGIN_TLS",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Togo",
           "code2":"TG",
           "code3":"TGO",
           "countryTranslateKey":"COUNTRIES.NAME_TGO",
           "originTranslateKey":"COUNTRIES.ORIGIN_TGO",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Tonga",
           "code2":"TO",
           "code3":"TON",
           "countryTranslateKey":"COUNTRIES.NAME_TON",
           "originTranslateKey":"COUNTRIES.ORIGIN_TON",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Trinidad and Tobago",
           "code2":"TT",
           "code3":"TTO",
           "countryTranslateKey":"COUNTRIES.NAME_TTO",
           "originTranslateKey":"COUNTRIES.ORIGIN_TTO",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Tunisia",
           "code2":"TN",
           "code3":"TUN",
           "countryTranslateKey":"COUNTRIES.NAME_TUN",
           "originTranslateKey":"COUNTRIES.ORIGIN_TUN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Turkmenistan",
           "code2":"TM",
           "code3":"TKM",
           "countryTranslateKey":"COUNTRIES.NAME_TKM",
           "originTranslateKey":"COUNTRIES.ORIGIN_TKM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Turks and Caicos Islands (the)",
           "code2":"TC",
           "code3":"TCA",
           "countryTranslateKey":"COUNTRIES.NAME_TCA",
           "originTranslateKey":"COUNTRIES.ORIGIN_TCA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Tuvalu",
           "code2":"TV",
           "code3":"TUV",
           "countryTranslateKey":"COUNTRIES.NAME_TUV",
           "originTranslateKey":"COUNTRIES.ORIGIN_TUV",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Uganda",
           "code2":"UG",
           "code3":"UGA",
           "countryTranslateKey":"COUNTRIES.NAME_UGA",
           "originTranslateKey":"COUNTRIES.ORIGIN_UGA",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"United Arab Emirates (the)",
           "code2":"AE",
           "code3":"ARE",
           "countryTranslateKey":"COUNTRIES.NAME_ARE",
           "originTranslateKey":"COUNTRIES.ORIGIN_ARE",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Uruguay",
           "code2":"UY",
           "code3":"URY",
           "countryTranslateKey":"COUNTRIES.NAME_URY",
           "originTranslateKey":"COUNTRIES.ORIGIN_URY",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Vanuatu",
           "code2":"VU",
           "code3":"VUT",
           "countryTranslateKey":"COUNTRIES.NAME_VUT",
           "originTranslateKey":"COUNTRIES.ORIGIN_VUT",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Venezuela (Bolivarian Republic of)",
           "code2":"VE",
           "code3":"VEN",
           "countryTranslateKey":"COUNTRIES.NAME_VEN",
           "originTranslateKey":"COUNTRIES.ORIGIN_VEN",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Virgin Islands (British)",
           "code2":"VG",
           "code3":"VGB",
           "countryTranslateKey":"COUNTRIES.NAME_VGB",
           "originTranslateKey":"COUNTRIES.ORIGIN_VGB",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Wallis and Futuna",
           "code2":"WF",
           "code3":"WLF",
           "countryTranslateKey":"COUNTRIES.NAME_WLF",
           "originTranslateKey":"COUNTRIES.ORIGIN_WLF",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Yemen",
           "code2":"YE",
           "code3":"YEM",
           "countryTranslateKey":"COUNTRIES.NAME_YEM",
           "originTranslateKey":"COUNTRIES.ORIGIN_YEM",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Zambia",
           "code2":"ZM",
           "code3":"ZMB",
           "countryTranslateKey":"COUNTRIES.NAME_ZMB",
           "originTranslateKey":"COUNTRIES.ORIGIN_ZMB",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['PASSPORT'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['ID_CARD','RESIDENCE_PERMIT','PASSPORT'] }
           ]
        },
        {
           "country":"Zimbabwe",
           "code2":"ZW",
           "code3":"ZWE",
           "countryTranslateKey":"COUNTRIES.NAME_ZWE",
           "originTranslateKey":"COUNTRIES.ORIGIN_ZWE",
           "supportedDocuments": [
               { provider: KycVendors.VERIFF, documents: ['ID_CARD','PASSPORT'] }
               ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Aland Islands",
           "code2":"AX",
           "code3":"ALA",
           "countryTranslateKey":"COUNTRIES.NAME_ALA",
           "originTranslateKey":"COUNTRIES.ORIGIN_ALA",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD'] }
            ,{ provider: KycVendors.VERIFEYE, documents: ['PASSPORT'] }
           ]
        },
        {
           "country":"Christmas Island",
           "code2":"CX",
           "code3":"CXR",
           "countryTranslateKey":"COUNTRIES.NAME_CXR",
           "originTranslateKey":"COUNTRIES.ORIGIN_CXR",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD'] }
            ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        },
        {
           "country":"Cocos (Keeling) Islands (the)",
           "code2":"CC",
           "code3":"CCK",
           "countryTranslateKey":"COUNTRIES.NAME_CCK",
           "originTranslateKey":"COUNTRIES.ORIGIN_CCK",
           "supportedDocuments": [
            { provider: KycVendors.VERIFF, documents: ['ID_CARD'] }
            ,{ provider: KycVendors.VERIFEYE, documents: [] }
           ]
        }
     ];

    private destroy$ = new Subject();

    private stylesheetId = 'select-hide';

    // Total number of countries in array = 249

    constructor(
        private nav: NavController,
        private router: Router,
        private injector: Injector,
        private api: ApiProviderService,
        private userProviderService: UserProviderService,
        private storageProvider: StorageProviderService,
        private secureStorage: SecureStorageService,
        private crypto: CryptoProviderService,
        private storage: StorageProviderService,
        private kycService: KycService,
        private appState: AppStateService,
        private settingsService: SettingsService,
        private translateService: TranslateService,
        private deviceService: DeviceService,
        private loaderProviderService: LoaderProviderService,
        private appVersion: AppVersion,
        private platform: Platform,
        public _SQLStorageService: SQLStorageService,
        private vaultSecretsService: VaultSecretsServiceAkita,
        private kycMediaServiceAkita: KycMediaServiceAkita,
        private userPhotoServiceAkita: UserPhotoServiceAkita,
        private walletService: WalletsService,
    ) {
        this.translateService.onLangChange.asObservable()
            .pipe(
                takeUntil(this.destroy$)
            ).subscribe(({lang}) => {
            this.countries.forEach((countryDataSet, index) => {
                this.countries[index][lang + '_country'] = this.translateService.instant(countryDataSet.countryTranslateKey);
                // this.countries[index][lang + '_origin'] = this.translateService.instant(countryDataSet.originTranslateKey);
            });
            this.countries = sortBy(this.countries, lang + '_country');
            this.dlSupportedCountries.forEach((countryDataSet, index) => {
                this.dlSupportedCountries[index][lang + '_country'] = this.translateService.instant(countryDataSet.countryTranslateKey);
            });
            this.dlSupportedCountries = sortBy(this.dlSupportedCountries, lang + '_country');
            this.idCardSupportedCountries.forEach((countryDataSet, index) => {
                this.idCardSupportedCountries[index][lang + '_country'] =
                    this.translateService.instant(countryDataSet.countryTranslateKey);
            });
            this.idCardSupportedCountries = sortBy(this.idCardSupportedCountries, lang + '_country');
            this.identityCountries.forEach((countryDataSet, index) => {
                this.identityCountries[index][lang + '_country'] =
                    this.translateService.instant(countryDataSet.countryTranslateKey);
            });
        });
    }

    checkIsSuccess(): boolean {
        return this.registerSuccess;
    }

    checkIsPersonalInformationSuccess(): boolean {
        return this.personalInfoSuccess;
    }


    async setOnboardingHasRun(status: boolean) {
        await this.secureStorage.setValue(SecureStorageKey.onboardingHasRun, (status ? "true" : "false"));
        return status;
    }

    async getOnboardingHasRun() {
        let onboardingHasRun = await this.secureStorage.getValue(SecureStorageKey.onboardingHasRun, false);
        if ( !onboardingHasRun ) {
            await this.secureStorage.setValue(SecureStorageKey.onboardingHasRun, "false");
        }
        return (onboardingHasRun == "true");
    }

    resetSignForm() {
        this.signUpForm = {
            username: '',
            password: '',
            confirm_password: '',
            first_name: '',
            title: '',
            last_name: '',
            email_id: '',
            OTP: ''
        };
    }

    resetPersonalInfoForm() {
        this.personalInformation = {
            gender: '',
            maritalstatus: '',
            date_of_birth: '',
            place_of_birth: '',
            province_of_birth: '',
            country_of_birth: '',
            citizenship: '',
            address_residence: '',
            street_number_residence: '',
            postal_code_residence: '',
            city_residence: '',
            province_residence: '',
            country_residence: '',
            passport_number: '',
            passport_issue_date: '',
            passport_expiry_date: '',
            passport_issue_country: '',
            dl_number: '',
            dl_issue_date: '',
            dl_expiry_date: '',
            dl_issue_country: '',
            idcard_number: '',
            idcard_issue_date: '',
            idcard_expiry_date: '',
            idcard_issue_country: '',
            residence_permit_number: '',
            residence_permit_issue_date: '',
            residence_permit_expiry_date: '',
            residence_permit_issue_country: '',
        };
    }

    /** @inheritDoc */
    public ngOnDestroy() {
        this.destroy$.next(0);
    }

    async skipReg(step: number): Promise<void> {
        this.resetSignForm();
        this.resetPersonalInfoForm();
        this.userPhotoServiceAkita.clear();
        await this.secureStorage.removeValue(SecureStorageKey.sign, false);
        await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false);
        await this.settingsService.set(Settings.wizardNotRun, true.toString(), true);
        if ( (step > 0 && step <= 6) || step === 9 ) {
            await this.nav.navigateBack('/login');
        } else {
            // this.signUpForm = {};
            // this.personalInformation = {};
            // this.resetSignForm();
            // this.resetPersonalInfoForm();
            // this.userPhotoServiceAkita.clear();
            // await this.secureStorage.removeValue(SecureStorageKey.sign, false);
            // await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false);
            // await this.settingsService.set(Settings.wizardNotRun, true.toString());
            await this.nav.navigateBack('/dashboard');
        }
    }

    addFlags(mode: CountryMode, origin = false) {
        return new Promise<void>(resolve => {
            let searchDictionary;
            switch ( mode ) {
                case CountryMode.STANDARD:
                    searchDictionary = this.countries;
                    break;
                case CountryMode.DRIVERLICENSE:
                    searchDictionary = this.dlSupportedCountries;
                    break;
                case CountryMode.IDCARD:
                    searchDictionary = this.idCardSupportedCountries;
                    break;
                case CountryMode.IDENTITY:
                    searchDictionary = this.identityCountries;
                    break;
            }
            const addFlagsTimeout: SubscriptionLike = of(true).pipe(delay(50)).subscribe(() => {
                const document: Document = this.injector.get(DOCUMENT);
                const items = document.querySelectorAll('.alert-button-inner');
                items.forEach((i) => {
                    if ( i.childNodes[1] ) {
                        const country = (i.childNodes[1] as any).outerText.trim();
                        const index = searchDictionary
                            .findIndex(c => c[`${this.translateService.currentLang}${origin ? '_origin' : '_country'}`] === country);
                        const img = document.createElement('img');
                        img.src = `../../../../assets/flags/${searchDictionary[index].code3}.png`;
                        const className = `custom-image-flag custom-image-${searchDictionary[index].code3}`;
                        img.className = className;
                        if ( !document.getElementsByClassName(className).length ) {
                            i.childNodes[1].insertBefore(img, i.childNodes[1].childNodes[0]);
                        }
                    }
                });
                resolve();
                addFlagsTimeout.unsubscribe();
            });
        });
    }

    addSearchBar() {
        return new Promise<void>(resolve => {
            const addSearchBarTimeout: SubscriptionLike = of(true).pipe(delay(50)).subscribe(() => {
                const document: Document = this.injector.get(DOCUMENT);
                const bar: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-head');
                const text: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-title');
                const message: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-message');
                const itemsContainer: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-radio-group');
                if ( bar && text ) {
                    bar.forEach((i, index) => {
                        if ( document.querySelector('.alert-wrapper ion-searchbar') ) {
                            return;
                        }
                        const searchBar = document.createElement('ion-searchbar');
                        searchBar.placeholder = (text && text.length > 0 && text[index]) ? text[index].innerText : '';
                        i.appendChild(searchBar);
                        if (text && text.length > 0 && text[index]) {
                            text[index].style.display = 'none';
                        }
                        message[index].style.display = 'none';

                        const itemsDummy = Array.from(itemsContainer);
                        const items = Array.from(itemsDummy[0].childNodes);

                        searchBar.addEventListener('ionInput', handleInput);

                        function handleInput(event) {
                            const query = event.target.value.toLowerCase();
                            requestAnimationFrame(() => {
                                items.forEach((item) => {
                                    const myNode = (item.childNodes[0].childNodes[1] as HTMLScriptElement);
                                    const elem = item as HTMLElement;
                                    const shouldShow = myNode ? myNode.innerText.toLowerCase().indexOf(query) > -1 : null;
                                    elem.style.display = shouldShow ? 'block' : 'none';
                                });
                            });
                        }
                    });
                }
                resolve();
                addSearchBarTimeout.unsubscribe();
            });
        });
    }

    /** Adding the style stylesheet for hiding alert elements to the head */
    public addStyleSelectToHead(): void {
        const styles = `
            ion-alert .alert-radio-group .alert-radio-button {
                opacity: 0
            }
        `;
        UtilityService.addStyleToHead(styles, this.stylesheetId);
    }

    /** Method to add country flags and search bar to select boxes */
    public addSelectBoxAdditionalElements(mode: CountryMode = CountryMode.STANDARD): void {
        if (!document.querySelector(`#${this.stylesheetId}`)) {
            this.addStyleSelectToHead();
        }
        interval(1)
            .pipe(
                filter(_ => !!document.querySelector('.alert-radio-group')),
                take(1)
            ).toPromise().then(_ => {
            return this.createLoadingSpinner();
        }).then(_ => {
            return Promise.all([
                this.addFlags(mode),
                this.addSearchBar(),
            ]);
        }).then(_ => {
            UtilityService.setTimeout(100)
                .subscribe(_ => {
                    this.removeLoadingSpinner();
                    UtilityService.removeStyleFromHead(this.stylesheetId);
                });
        });
    }

    async getSignData(): Promise<SignUp> {
        const userResponse = await this.secureStorage.getValue(SecureStorageKey.sign, false);

        return userResponse ? JSON.parse(userResponse) : null;
    }

    async getPersonalData(): Promise<PersonalInformation> {
        const userResponse = await this.secureStorage.getValue(SecureStorageKey.personalInfo, false);

        return userResponse ? JSON.parse(userResponse) : null;
    }

    // user/create
    async provideInitialOnboarding() {
        const registerData: SignUp = await this.getSignData() || null;
        const phoneNumber = await this.userProviderService.getApprovedPhoneNumber() || '';
        const langSetting = await this.secureStorage.getValue(SecureStorageKey.language ,false) || 'en';
        const encryptedPassword = await this.crypto.encryptPasswordAsBase64(registerData.password);
        const packageName = !this.platform.is('hybrid') ? 'com.jobgrader.app' :  await this.appVersion.getPackageName();

        if ( !!registerData ) {
            const data = {
                user: {
                    username: registerData.username,
                    password: encryptedPassword,
                    email: registerData.email_id,
                    source: packageName
                },
                basicdata: {
                    email: registerData.email_id,
                    phone: phoneNumber,
                    firstname: registerData.first_name,
                    title: registerData.title,
                    termsofuse: 'true',
                    termsofusethirdparties: 'true',
                    lastname: registerData.last_name,
                    callname: (registerData.first_name.length > 14 ? registerData.first_name.substring(0, 14) : registerData.first_name),
                },
                settings: [{
                    settingName: 'language',
                    value: langSetting
                }]
                // ,attachment: { }
            };
            // console.log(JSON.stringify(data));
            await this.api.createUser(data).then((res) => {
                // if (res.status === 1) {
                this.registerSuccess = true;
                console.log('/user/create successful! ');
                // console.log(JSON.stringify(res));
                if(!!res.values.chatuser) {
                    this.secureStorage.setValue(SecureStorageKey.chatUserData, JSON.stringify(res.values.chatuser), false);
                }
                if(!!res.values.didDocument) {
                    this.crypto.encryptGenericData(JSON.stringify(res.values.didDocument)).then(encryptedDidDocument => {
                        this.secureStorage.setValue(SecureStorageKey.didDocument, encryptedDidDocument).then(() => {
                            if(!!res.values.didDocument.id) {
                                this.secureStorage.setValue(SecureStorageKey.did, res.values.didDocument.id);
                            }
                        })
                    });
                }
                // Ansik: Add DID and VC Retrieval Code here: Done
                return true;

            })
                .catch(err => {
                    console.log(err);
                    this.registerSuccess = false;
                    console.log('/user/create failed!!');
                    return false;
                });
        }
    }

    private createLoadingSpinner(): void {
        const div = document.createElement('DIV');
        const img: any = document.createElement('IMG');
        img.src = '../../../../assets/spinner.gif';
        div.appendChild(img);
        div.style.position = 'absolute';
        div.style.top = '0';
        div.style.bottom = '0';
        div.style.left = '0';
        div.style.right = '0';
        div.style.background = 'var(--background)';
        div.classList.add('loading-overlay-select');
        img.style.width = '40px';
        img.style.marginLeft = 'calc(50% - 20px)';
        img.style.marginTop = 'calc(50% - 20px)';
        const group: any = document.querySelector('.alert-radio-group');
        group.style.position = 'relative';
        group.appendChild(div);
    }

    private removeLoadingSpinner(): void {
        document.querySelector('.loading-overlay-select').remove();
    }

    // user/save
    async provideUserDataUpdate() {
        const userRegData: SignUp = await this.getSignData();
        const user: User = await this.userProviderService.getUser();
        const phoneNumber = await this.userProviderService.getApprovedPhoneNumber() || '';
        const userPersonalData: PersonalInformation = await this.getPersonalData();
        const symmetricKey = await this.crypto.returnUserSymmetricKey();
        const chatPublicKey = await this.secureStorage.getValue(SecureStorageKey.chatPublicKey, false);
        const chatPrivateKey = await this.secureStorage.getValue(SecureStorageKey.chatPrivateKey, false);
        const encryptedChatPrivateKey = await this.crypto.symmetricEncrypt(chatPrivateKey, symmetricKey).catch(e => console.log(e));
        const packageName = !this.platform.is('hybrid') ? 'com.jobgrader.app' :  await this.appVersion.getPackageName();

        if ( this.imageSelectionPayload &&
            this.imageSelectionPayload.fileName !== 'Default' &&
            !!this.imageSelectionPayload.format &&
            !!this.imageSelectionPayload.base64Image ) {
            await this.api.updatePicture(
                user,
                this.imageSelectionPayload.base64Image,
                this.imageSelectionPayload.fileName,
                this.imageSelectionPayload.format);
            // if(imageUrl) {
            //     this.userPhotoServiceAkita.updatePhoto(imageUrl);
            // }
        }

        if ( !this.appState.isAuthorized ) {
            if ( !!userRegData && !!userPersonalData ) {
                // console.log("!this.appState.isAuthorized")
                const data: any = {
                    user: {
                        username: userRegData.username,
                        password: null,
                        email: userRegData.email_id,
                        id: user.userid,
                        chatPublicKey: chatPublicKey,
                        encryptedChatPrivateKey: encryptedChatPrivateKey,
                        source: packageName
                    },
                    basicdata: {
                        userid: user.userid,
                        email: userRegData.email_id,
                        phone: phoneNumber,
                        firstname: userRegData.first_name,
                        termsofuse: 'true',
                        termsofusethirdparties: 'true',
                        lastname: userRegData.last_name,
                        title: userRegData.title,
                        callname: (userRegData.first_name.length > 14 ? userRegData.first_name.substring(0, 14) : userRegData.first_name),
                        dateofbirth: !!userPersonalData.date_of_birth ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.date_of_birth)) : null,
                        gender: !!userPersonalData.gender ? userPersonalData.gender : null,
                        maritalstatus: !!userPersonalData.maritalstatus ? userPersonalData.maritalstatus : null,
                        citizenship: !!userPersonalData.citizenship ? userPersonalData.citizenship : null,
                        countryofbirth: !!userPersonalData.country_of_birth ? userPersonalData.country_of_birth : null,
                        cityofbirth: !!userPersonalData.place_of_birth ? userPersonalData.place_of_birth : null,
                        street: !!userPersonalData.address_residence ?  userPersonalData.address_residence + (userPersonalData.street_number_residence ? (' ' + userPersonalData.street_number_residence) : '') : null,
                        zip: !!userPersonalData.postal_code_residence ? userPersonalData.postal_code_residence : null,
                        city: !!userPersonalData.city_residence ? userPersonalData.city_residence : null,
                        state: !!userPersonalData.province_residence ? userPersonalData.province_residence : null,
                        country: !!userPersonalData.country_residence ? userPersonalData.country_residence : null,
                        identificationdocumenttype: 'ID_CARD',
                        passportnumber: !!userPersonalData.passport_number ? userPersonalData.passport_number : null,
                        passportissuedate: !!userPersonalData.passport_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.passport_issue_date)) : null,
                        passportexpirydate: !!userPersonalData.passport_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.passport_expiry_date)) : null,
                        passportissuecountry: !!userPersonalData.passport_issue_country ? userPersonalData.passport_issue_country : null,
                        driverlicencedocumentnumber: !!userPersonalData.dl_number ? userPersonalData.dl_number : null,
                        driverlicenceissuedate: !!userPersonalData.dl_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.dl_issue_date)) : null,
                        driverlicenceexpirydate: !!userPersonalData.dl_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.dl_expiry_date)) : null,
                        driverlicencecountry: !!userPersonalData.dl_issue_country ? userPersonalData.dl_issue_country : null,
                        identificationdocumentnumber: !!userPersonalData.idcard_number ? userPersonalData.idcard_number : null,
                        identificationissuedate: !!userPersonalData.idcard_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.idcard_issue_date)) : null,
                        identificationexpirydate: !!userPersonalData.idcard_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.idcard_expiry_date)) : null,
                        identificationissuecountry: !!userPersonalData.idcard_issue_country ? userPersonalData.idcard_issue_country : null,
                        residencepermitnumber: !!userPersonalData.residence_permit_number ? userPersonalData.residence_permit_number : null,
                        residencepermitissuedate: !!userPersonalData.residence_permit_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.residence_permit_issue_date)) : null,
                        residencepermitexpirydate: !!userPersonalData.residence_permit_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.residence_permit_expiry_date)) : null,
                        residencepermitissuecountry: !!userPersonalData.residence_permit_issue_country ? userPersonalData.residence_permit_issue_country : null,
                    }
                    // ,attachment: { }
                };
                // console.log("data: " + JSON.stringify(data))
                await this.api.saveUser(this.appState.basicAuthToken, data).then(_ => {
                    if ( userPersonalData.idcard_number && userPersonalData.idcard_issue_date && userPersonalData.idcard_expiry_date && userPersonalData.idcard_issue_country ) {
                        this.settingsService.set(Settings.idCardSet, true.toString(), true);
                    }
                    if ( userPersonalData.dl_number && userPersonalData.dl_issue_date && userPersonalData.dl_expiry_date && userPersonalData.dl_issue_country ) {
                        this.settingsService.set(Settings.driverLicenseSet, true.toString(), true);
                    }
                    if ( userPersonalData.passport_number && userPersonalData.passport_issue_date && userPersonalData.passport_expiry_date && userPersonalData.passport_issue_country ) {
                        this.settingsService.set(Settings.passportSet, true.toString(), true);
                    }
                    if ( userPersonalData.residence_permit_number && userPersonalData.residence_permit_issue_date && userPersonalData.residence_permit_expiry_date && userPersonalData.residence_permit_issue_country ) {
                        this.settingsService.set(Settings.residencePermitSet, true.toString(), true);
                    }
                });
            }
        } else {
            // console.log("this.appState.isAuthorized")
            const object: any = {
                user: {
                    username: await this.secureStorage.getValue(SecureStorageKey.userName, false),
                    password: null,
                    email: user.email,
                    id: user.userid,
                    chatPublicKey: chatPublicKey,
                    encryptedChatPrivateKey: encryptedChatPrivateKey,
                    source: packageName
                },
                basicdata: {
                    userid: user.userid,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    title: user.title,
                    middlename: user.middlename,
                    maidenname: user.maidenname,
                    phone: user.phone,
                    email: user.email,
                    callname: user.callname,
                    gender: !!userPersonalData.gender ? userPersonalData.gender : null,
                    dateofbirth: !!userPersonalData.date_of_birth ? userPersonalData.date_of_birth : null,
                    street: !!userPersonalData.address_residence ? userPersonalData.address_residence + (userPersonalData.street_number_residence ? (' ' + userPersonalData.street_number_residence) : '') : null,
                    city: !!userPersonalData.city_residence ? userPersonalData.city_residence : null,
                    state: !!userPersonalData.province_residence ? userPersonalData.province_residence : '',
                    zip: !!userPersonalData.postal_code_residence ? userPersonalData.postal_code_residence : null,
                    maritalstatus: !!userPersonalData.maritalstatus ? userPersonalData.maritalstatus : null,
                    termsofuse: 'true',
                    termsofusethirdparties: 'true',
                    cityofbirth: !!userPersonalData.place_of_birth ? userPersonalData.place_of_birth : null,
                    countryofbirth: !!userPersonalData.country_of_birth ? userPersonalData.country_of_birth : null,
                    country: !!userPersonalData.country_residence ? userPersonalData.country_residence : null,
                    citizenship: !!userPersonalData.citizenship ? userPersonalData.citizenship : null,
                    identificationdocumenttype: 'ID_CARD',
                    passportnumber: !!userPersonalData.passport_number ? userPersonalData.passport_number : null,
                    passportissuedate: !!userPersonalData.passport_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.passport_issue_date)) : null,
                    passportexpirydate: !!userPersonalData.passport_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.passport_expiry_date)) : null,
                    passportissuecountry: !!userPersonalData.passport_issue_country ? userPersonalData.passport_issue_country : null,
                    driverlicencedocumentnumber: !!userPersonalData.dl_number ? userPersonalData.dl_number : null,
                    driverlicenceissuedate: !!userPersonalData.dl_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.dl_issue_date)) : null,
                    driverlicenceexpirydate: !!userPersonalData.dl_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.dl_expiry_date)) : null,
                    driverlicencecountry: !!userPersonalData.dl_issue_country ? userPersonalData.dl_issue_country : null,
                    identificationdocumentnumber: !!userPersonalData.idcard_number ? userPersonalData.idcard_number : null,
                    identificationissuedate: !!userPersonalData.idcard_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.idcard_issue_date)) : null,
                    identificationexpirydate: !!userPersonalData.idcard_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.idcard_expiry_date)) : null,
                    identificationissuecountry: !!userPersonalData.idcard_issue_country ? userPersonalData.idcard_issue_country : null,
                    residencepermitnumber: !!userPersonalData.residence_permit_number ? userPersonalData.residence_permit_number : null,
                    residencepermitissuedate: !!userPersonalData.residence_permit_issue_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.residence_permit_issue_date)) : null,
                    residencepermitexpirydate: !!userPersonalData.residence_permit_expiry_date ? this.getFormattedDayForRequest(this.getFormattedDay(userPersonalData.residence_permit_expiry_date)) : null,
                    residencepermitissuecountry: !!userPersonalData.residence_permit_issue_country ? userPersonalData.residence_permit_issue_country : null,
                }
            };
            // console.log("object: " + JSON.stringify(object))
            await this.api.saveUser(this.appState.basicAuthToken, object).then(_ => {
                if ( userPersonalData.idcard_number && userPersonalData.idcard_issue_date && userPersonalData.idcard_expiry_date && userPersonalData.idcard_issue_country ) {
                    this.settingsService.set(Settings.idCardSet, true.toString(), true);
                }
                if ( userPersonalData.dl_number && userPersonalData.dl_issue_date && userPersonalData.dl_expiry_date && userPersonalData.dl_issue_country ) {
                    this.settingsService.set(Settings.driverLicenseSet, true.toString(), true);
                }
                if ( userPersonalData.passport_number && userPersonalData.passport_issue_date && userPersonalData.passport_expiry_date && userPersonalData.passport_issue_country ) {
                    this.settingsService.set(Settings.passportSet, true.toString(), true);
                }
                if ( userPersonalData.residence_permit_number && userPersonalData.residence_permit_issue_date && userPersonalData.residence_permit_expiry_date && userPersonalData.residence_permit_issue_country ) {
                    this.settingsService.set(Settings.residencePermitSet, true.toString(), true);
                }
            });
        }
    }

    async pushChatKeyPairs() {
        // await this.keyBackupRecovery.decryptEncryptedChatPrivateKey()
        const [ chatPrivateKey, userKeyEncrypted ] = await Promise.all([
            await this.secureStorage.getValue(SecureStorageKey.chatPrivateKey, false),
            await this.secureStorage.getValue(SecureStorageKey.userKeyEncryptedUser, false)
        ]);
        // console.log("chatPrivateKey: " + chatPrivateKey);
        // console.log("userKeyEncrypted: " + userKeyEncrypted);
        if(!userKeyEncrypted) {
            await this.crypto.getUserSymmetricKeyIfNeeded('userKey');
        }
        if( chatPrivateKey ) {
            const symmetricKey = await this.crypto.returnUserSymmetricKey();
            const chatPublicKey = await this.secureStorage.getValue(SecureStorageKey.chatPublicKey, false);
            const encryptedChatPrivateKey = await this.crypto.symmetricEncrypt(chatPrivateKey, symmetricKey);
            const userData: User = JSON.parse(await this.secureStorage.getValue(SecureStorageKey.userData, false));
            const userName: string = await this.secureStorage.getValue(SecureStorageKey.userName, false);
            const packageName = !this.platform.is('hybrid') ? 'com.io.helix.id' :  await this.appVersion.getPackageName();
            const saveData: any = {
                user: {
                    password: null,
                    username: userName,
                    email: userData.email,
                    chatPublicKey: chatPublicKey,
                    encryptedChatPrivateKey: encryptedChatPrivateKey,
                    id: userData.userid,
                    source: packageName
                },
                basicdata: {
                    userid: userData.userid,
                    email: userData.email,
                    phone: userData.phone,
                    firstname: userData.firstname,
                    termsofuse: userData.termsofuse,
                    termsofusethirdparties: userData.termsofusethirdparties,
                    lastname: userData.lastname,
                    callname: userData.callname,
                    title: userData.title,
                    dateofbirth: userData.dateofbirth,
                    gender: userData.gender,
                    middlename: userData.middlename,
                    maidenname: userData.maidenname,
                    maritalstatus: userData.maritalstatus,
                    citizenship: userData.citizenship,
                    countryofbirth: userData.countryofbirth,
                    cityofbirth: userData.cityofbirth,
                    street: userData.street,
                    zip: userData.zip,
                    city: userData.city,
                    state: userData.state,
                    country: userData.country,
                    identificationdocumenttype: userData.identificationdocumenttype,
                    identificationdocumentnumber: userData.identificationdocumentnumber,
                    identificationissuedate: userData.identificationissuedate,
                    identificationexpirydate: userData.identificationexpirydate,
                    identificationissuecountry: userData.identificationissuecountry,
                    driverlicencedocumentnumber: userData.driverlicencedocumentnumber,
                    driverlicenceissuedate: userData.driverlicenceissuedate,
                    driverlicenceexpirydate: userData.driverlicenceexpirydate,
                    driverlicencecountry: userData.driverlicencecountry,
                    passportnumber: userData.passportnumber,
                    passportissuedate: userData.passportissuedate,
                    passportexpirydate: userData.passportexpirydate,
                    passportissuecountry: userData.passportissuecountry,
                    residencepermitnumber: userData.residencepermitnumber,
                    residencepermitissuedate: userData.residencepermitissuedate,
                    residencepermitexpirydate: userData.residencepermitexpirydate,
                    residencepermitissuecountry: userData.residencepermitissuecountry,
                }
            }
            // console.log(JSON.stringify(saveData));
            return await this.api.saveUser(this.appState.basicAuthToken, saveData);
        }
        else {
            return console.log("chatPrivateKey is missing");
        }
    }


    async getBasicAuthToken(): Promise<string> {
        return await this.secureStorage.getValue(SecureStorageKey.basicAuthToken, false);
    }

    async saveSignData(data: any, step: number): Promise<any> {
        const dataFromStorage: SignUp = await this.getSignData();
        if ( !!dataFromStorage ) {
            this.signUpForm = dataFromStorage;
        }

        switch ( step ) {
            case 1:
                this.signUpForm.first_name = data.firstName;

                await this.secureStorage.setValue(SecureStorageKey.sign, JSON.stringify(this.signUpForm));
                break;

            case 2: {
                this.signUpForm.username = data.username;

                await this.secureStorage.setValue(SecureStorageKey.sign, JSON.stringify(this.signUpForm));
                break;
            }
            case 3: {
                this.signUpForm.password = data.password;

                await this.secureStorage.setValue(SecureStorageKey.sign, JSON.stringify(this.signUpForm));
                break;
            }
            case 5: {
                this.signUpForm.email_id = data.email_id;

                await this.secureStorage.setValue(SecureStorageKey.sign, JSON.stringify(this.signUpForm));
                break;
            }
            case 6: {
                this.signUpForm.OTP = data.otp;

                await this.secureStorage.setValue(SecureStorageKey.sign, JSON.stringify(this.signUpForm));
                break;
            }

            default:
                break;
        }
    }

    /** Method to brand a device to a user */
    public async brandDeviceToUser(username, password): Promise<void> {
        const nonce = this.crypto.generateNonce(25 - username.length);
        await this.secureStorage.setValue(SecureStorageKey.loginCheck, this.crypto.generateHash(username, nonce));
        await this.secureStorage.setValue(SecureStorageKey.passwordHash, this.crypto.generateHash(password, nonce));
        await this.secureStorage.setValue(SecureStorageKey.nonce, nonce);
    }

    /** Method to unbrand a device from a user */
    public async unbrandDeviceFromUser(): Promise<void> {
        if (this.platform.is('hybrid')) {
            // this.deviceService.updateFirebaseTokenInDeviceTable(null);
            var deviceId = await this.secureStorage.getValue(SecureStorageKey.deviceInfo, false);
            if(deviceId && deviceId != "undefined") {
                await this.api.updateFirebaseToken(null, deviceId).catch(e => console.log(e))
            }
        }
        await this.secureStorage.removeValue(SecureStorageKey.basicAuthToken, false); // User Auth Info complete
        await this.secureStorage.removeValue(SecureStorageKey.token, false); // User Auth Info complete

        try {
            this.userPhotoServiceAkita.clear(); // User Personal Info complete
        } catch(e) {
            console.log(e);
        } 
    
        // await this.secureStorage.removeValue(SecureStorageKey.devicePublicKeyDelegate, false); // User Device Keys
        // await this.secureStorage.removeValue(SecureStorageKey.devicePrivateKeyDelegate, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.devicePublicKeyEncryption, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.devicePrivateKeyEncryption, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.userKeyEncryptedVC, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.userKeyEncryptedBackup, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.userKeyEncryptedUser, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.deviceInfo, false); // User Device Info
        // await this.secureStorage.removeValue(SecureStorageKey.deviceMnemonic, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.mnemonic, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.chatPublicKey, false); // User Device Keys
        await this.secureStorage.removeValue(SecureStorageKey.chatPrivateKey, false); // User Device Keys complete
        await this.secureStorage.removeValue(SecureStorageKey.encryptedChatPrivateKey, false); // User Device Keys complete
        await this.secureStorage.removeValue(SecureStorageKey.userActivities, false); // User Device Keys complete

        // var isBrowseMode = await this.secureStorage.getValue(SecureStorageKey.browseMode, false);
        // if( isBrowseMode != 'true' ) {
            await this.secureStorage.removeValue(SecureStorageKey.language, false); // App Settings
        // }

        await this.secureStorage.removeValue(SecureStorageKey.faioEnabled, false); // App Settings complete
        await this.secureStorage.removeValue(SecureStorageKey.loginCheck, false); // loginCheck
        await this.secureStorage.removeValue(SecureStorageKey.passwordHash, false); // loginCheck
        await this.secureStorage.removeValue(SecureStorageKey.nonce, false); // loginCheck
        await this.secureStorage.removeValue(SecureStorageKey.sign, false); // registration form 1
        await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false); // registration form 2
        await this.secureStorage.removeValue(SecureStorageKey.userName, false);
        await this.secureStorage.removeValue(SecureStorageKey.userData, false);
        await this.secureStorage.removeValue(SecureStorageKey.userTrustData, false);
        await this.secureStorage.removeValue(SecureStorageKey.verifiableCredentialEncrypted, false);
        await this.secureStorage.removeValue(SecureStorageKey.didDocument, false);
        await this.secureStorage.removeValue(SecureStorageKey.vcId, false);
        await this.secureStorage.removeValue(SecureStorageKey.did, false);
        await this.secureStorage.removeValue(SecureStorageKey.loginStatusAfterSignup, false);
        await this.secureStorage.removeValue(SecureStorageKey.languageSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.kycStatus, false);
        await this.secureStorage.removeValue(SecureStorageKey.kycStatusTimestamp, false);
        try {
            this.kycMediaServiceAkita.clear();
        } catch(e) {
            console.log(e);
        }
        await this.secureStorage.removeValue(SecureStorageKey.kycMediaTimestamp, false);
        await this.secureStorage.removeValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, false);
        await this.secureStorage.removeValue(SecureStorageKey.chatKeyPairs, false);
        await this.secureStorage.removeValue(SecureStorageKey.chatUserData, false);
        await this.secureStorage.removeValue(SecureStorageKey.marketplace, false);
        await this.secureStorage.removeValue(SecureStorageKey.marketplaceLastModified, false);
        await this.secureStorage.removeValue(SecureStorageKey.certificatesMedia, false);
        await this.secureStorage.removeValue(SecureStorageKey.certificates, false);
        await this.secureStorage.removeValue(SecureStorageKey.lockScreenToggle, false);
        await this.secureStorage.removeValue(SecureStorageKey.lockScreenToggleSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.userIsAllowedToAccessChatMarketplaceSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.powerUserMode, false);
        await this.secureStorage.removeValue(SecureStorageKey.securePINEncrypted, false);
        await this.secureStorage.removeValue(SecureStorageKey.hasSeenMerchantTutorial, false);
        await this.secureStorage.removeValue(SecureStorageKey.hasSeenMarketplaceTutorial, false);
        await this.secureStorage.removeValue(SecureStorageKey.encryptedBasicAuthToken, false);
        await this.secureStorage.removeValue(SecureStorageKey.healthCertificates, false);
        await this.secureStorage.removeValue(SecureStorageKey.cardInformations, false);
        await this.secureStorage.removeValue(SecureStorageKey.marketplaceCertificates, false);
        await this.secureStorage.removeValue(SecureStorageKey.kycUsingVerifeyeCompleted, false);
        await this.secureStorage.removeValue(SecureStorageKey.kycUsingVerifeyeCompletedSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.kycUsingOndatoCompleted, false);
        await this.secureStorage.removeValue(SecureStorageKey.kycUsingOndatoCompletedSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.voucherCodePage1, false);
        await this.secureStorage.removeValue(SecureStorageKey.voucherCodePage2, false);
        await this.secureStorage.removeValue(SecureStorageKey.dataSharingRequestsReceived, false);
        await this.secureStorage.removeValue(SecureStorageKey.dataSharingRequestsSent, false);
        await this.secureStorage.removeValue(SecureStorageKey.marketplaceUpdate, false);
        await this.secureStorage.removeValue(SecureStorageKey.browseMode, false);
        await this.secureStorage.removeValue(SecureStorageKey.videoSeen, false);
        await this.secureStorage.removeValue(SecureStorageKey.themeModeSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.helixPublicKey, false);
        await this.secureStorage.removeValue(SecureStorageKey.themeMode, false);
        await this.secureStorage.removeValue(SecureStorageKey.firebase, false);
        await this.secureStorage.removeValue(SecureStorageKey.hashUDID, false);
        await this.secureStorage.removeValue(SecureStorageKey.gifSetting, false);
        await this.secureStorage.removeValue(SecureStorageKey.userPublicKey, false);
        await this.secureStorage.removeValue(SecureStorageKey.userPrivateKey, false);
        await this.secureStorage.removeValue(SecureStorageKey.userMnemonic, false);
        await this.secureStorage.removeValue(SecureStorageKey.cryptoPaymentRequestsReceived, false);
        await this.secureStorage.removeValue(SecureStorageKey.cryptoPaymentRequestsSent, false);
        await this.secureStorage.removeValue(SecureStorageKey.importedWallets, false);
        await this.secureStorage.removeValue(SecureStorageKey.carIdentities, false);
        await this.secureStorage.removeValue(SecureStorageKey.walletConnectSessions, false);
        await this.secureStorage.removeValue(SecureStorageKey.walletConnect2Sessions, false);
        await this.secureStorage.removeValue(SecureStorageKey.nftNumbersCache, false);
        await this.secureStorage.removeValue(SecureStorageKey.helixidBalance, false);
        await this.secureStorage.removeValue(SecureStorageKey.cryptoFiatConversion, false);
        await this.secureStorage.removeValue(SecureStorageKey.cryptoPaymentsSent, false);
        await this.secureStorage.removeValue(SecureStorageKey.cryptoBalance, false);
        await this.secureStorage.removeValue(SecureStorageKey.web3WalletMnemonic, false);
        await this.secureStorage.removeValue(SecureStorageKey.web3WalletPrivateKey, false);
        await this.secureStorage.removeValue(SecureStorageKey.web3WalletPublicKey, false);
        await this.secureStorage.removeValue(SecureStorageKey.web3WalletName, false);
        await this.secureStorage.removeValue(SecureStorageKey.paperBackupTimestamp, false);
        await this.secureStorage.removeValue(SecureStorageKey.cloudBackupTimestamp, false);
        await this.secureStorage.removeValue(SecureStorageKey.paperBackupTimestampSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.cloudBackupTimestampSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.cryptoConversions, false);
        await this.secureStorage.removeValue(SecureStorageKey.navChoice, false);
        await this.secureStorage.removeValue(SecureStorageKey.nfts, false);
        await this.secureStorage.removeValue(SecureStorageKey.poaps, false);
        await this.secureStorage.removeValue(SecureStorageKey.instagram, false);
        await this.secureStorage.removeValue(SecureStorageKey.shortcut1, false);
        await this.secureStorage.removeValue(SecureStorageKey.shortcut2, false);
        await this.secureStorage.removeValue(SecureStorageKey.shortcut3, false);
        await this.secureStorage.removeValue(SecureStorageKey.shortcut4, false);
        await this.secureStorage.removeValue(SecureStorageKey.kassenbeleg, false);
        await this.secureStorage.removeValue(SecureStorageKey.viewCategoryText, false);
        await this.secureStorage.removeValue(SecureStorageKey.walletCurrencies, false);
        try {
            await this.vaultSecretsService.clear();
        } catch(e) {
            console.log(e);
        }
        await this.secureStorage.removeValue(SecureStorageKey.currency, false);
        await this.secureStorage.removeValue(SecureStorageKey.hCaptchaDashboard, false);
        await this.secureStorage.removeValue(SecureStorageKey.notifications, false);

        await this.secureStorage.removeValue(SecureStorageKey.broadcasts, false);
        await this.secureStorage.removeValue(SecureStorageKey.notificationsTimestamp, false);

        await this.secureStorage.removeValueOfExceptionKey();

        await this.removeAllSQLData();
        // await this.kycService.reset();

        await this.secureStorage.removeValue(SecureStorageKey.onboardingHasRun, false); // onboardingHasRun
        await this.secureStorage.removeValue(SecureStorageKey.wizardNotRunSettingId, false); // wizardNotRun setting
        await this.secureStorage.removeValue(SecureStorageKey.wizardNotRun, false); // wizardNotRun
        await this.secureStorage.removeValue(SecureStorageKey.idCardSet, false);
        await this.secureStorage.removeValue(SecureStorageKey.driverLicenseSet, false);
        await this.secureStorage.removeValue(SecureStorageKey.passportSet, false);
        await this.secureStorage.removeValue(SecureStorageKey.residencePermitSet, false);
        await this.secureStorage.removeValue(SecureStorageKey.idCardSetSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.driverLicenseSetSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.passportSetSettingId, false);
        await this.secureStorage.removeValue(SecureStorageKey.residencePermitSetSettingId, false);

        await this.walletService.clear();
    }

    async removeAllSQLData() {
        this._SQLStorageService.clearTables();
    }

    async savePersonalData(data: any, step: number): Promise<any> {
        const dataFromStorage: PersonalInformation = (await this.getPersonalData()) || null;
        if ( !!dataFromStorage ) {
            this.personalInformation = dataFromStorage;
        }

        switch ( step ) {
            case 6:
                this.personalInformation.gender = data.gender;
                this.personalInformation.maritalstatus = data.maritalstatus;
                this.personalInformation.date_of_birth = (new Date(data.birthday)).toISOString();
                this.personalInformation.place_of_birth = data.place;
                this.personalInformation.province_of_birth = data.province;
                this.personalInformation.country_of_birth = data.country.trim();
                this.personalInformation.citizenship = data.citizenship.trim();
                await this.secureStorage.setValue(SecureStorageKey.personalInfo, JSON.stringify(this.personalInformation));
                break;

            case 7: {
                this.personalInformation.address_residence = data.address;
                this.personalInformation.street_number_residence = data.street;
                this.personalInformation.postal_code_residence = data.postalCode;
                this.personalInformation.city_residence = data.city;
                this.personalInformation.province_residence = data.province;
                this.personalInformation.country_residence = data.country.trim();

                await this.secureStorage.setValue(SecureStorageKey.personalInfo, JSON.stringify(this.personalInformation));
                break;
            }

            case 8: {
                this.personalInformation.passport_number = data.passportNumber;
                this.personalInformation.passport_issue_date = data.passportIssueDate ? (new Date(data.passportIssueDate)).toISOString() : undefined;
                this.personalInformation.passport_expiry_date = data.passportExpiryDate ? (new Date(data.passportExpiryDate)).toISOString() : undefined;
                this.personalInformation.passport_issue_country = data.passportIssueCountry;

                this.personalInformation.dl_number = data.dlNumber;
                this.personalInformation.dl_issue_date = data.dlIssueDate ? (new Date(data.dlIssueDate)).toISOString() : undefined;
                this.personalInformation.dl_expiry_date = data.dlExpiryDate ? (new Date(data.dlExpiryDate)).toISOString() : undefined;
                this.personalInformation.dl_issue_country = data.dlIssueCountry;

                this.personalInformation.idcard_number = data.idCardNumber;
                this.personalInformation.idcard_issue_date = data.idCardIssueDate ? (new Date(data.idCardIssueDate)).toISOString() : undefined;
                this.personalInformation.idcard_expiry_date = data.idCardExpiryDate ? (new Date(data.idCardExpiryDate)).toISOString() : undefined;
                this.personalInformation.idcard_issue_country = data.idCardIssueCountry;

                this.personalInformation.residence_permit_number = data.residencePermitNumber;
                this.personalInformation.residence_permit_issue_date = data.residencePermitIssueDate ? (new Date(data.residencePermitIssueDate)).toISOString() : undefined;
                this.personalInformation.residence_permit_expiry_date = data.residencePermitExpiryDate ? (new Date(data.residencePermitExpiryDate)).toISOString() : undefined;
                this.personalInformation.residence_permit_issue_country = data.residencePermitIssueCountry;

                await this.secureStorage.setValue(SecureStorageKey.personalInfo, JSON.stringify(this.personalInformation));
                break;
            }

            case 9: {
                // this.personalInformation.document_issue_place = data.issuePlace;
                // this.personalInformation.document_issue_agency = data.issueAgency;
                // this.personalInformation.document_scanned_image = data.scannedImage;

                await this.secureStorage.setValue(SecureStorageKey.personalInfo, JSON.stringify(this.personalInformation));
                break;
            }

            default:
                break;
        }
    }

    private getFormattedDay(date: string): string {
        const datePipe = new DatePipe('en-US');

        return datePipe.transform(new Date(date), 'dd MMM yyyy');
    }

    private getFormattedDayForRequest(date: string): number {
        return +new Date(date);
    }

    public documentSetter(user: User) {

        if(user.identificationdocumentnumber !== '' || user.identificationissuecountry !== null || user.identificationissuedate !== null || user.identificationexpirydate !== null) {
            this.settingsService.set(Settings.idCardSet, "true", true);
        } else {
            this.settingsService.set(Settings.idCardSet, "false", true);
        }

        if(user.driverlicencedocumentnumber !== '' || user.driverlicencecountry !== null || user.driverlicenceissuedate !== null || user.driverlicenceexpirydate !== null) {
            this.settingsService.set(Settings.driverLicenseSet, "true", true);
        } else {
            this.settingsService.set(Settings.driverLicenseSet, "false", true);
        }

        if(user.residencepermitnumber !== '' || user.residencepermitissuecountry !== null || user.residencepermitissuedate !== null || user.residencepermitexpirydate !== null) {
            this.settingsService.set(Settings.residencePermitSet, "true", true);
        } else {
            this.settingsService.set(Settings.residencePermitSet, "false", true);
        }

        if(user.passportnumber !== '' || user.passportissuecountry !== null || user.passportissuedate !== null || user.passportexpirydate !== null) {
            this.settingsService.set(Settings.passportSet, "true", true);
        } else {
            this.settingsService.set(Settings.passportSet, "false", true);
        }

        if(user.identificationdocumentnumber !== '' || user.identificationissuecountry !== null || user.identificationissuedate !== null || user.identificationexpirydate !== null ||
        user.driverlicencedocumentnumber !== '' || user.driverlicencecountry !== null || user.driverlicenceissuedate !== null || user.driverlicenceexpirydate !== null ||
        user.residencepermitnumber !== '' || user.residencepermitissuecountry !== null || user.residencepermitissuedate !== null || user.residencepermitexpirydate !== null ||
        user.passportnumber !== '' || user.passportissuecountry !== null || user.passportissuedate !== null || user.passportexpirydate !== null ||
        user.citizenship !== null || user.city !== '' || user.cityofbirth !== '' || user.country !== null || user.countryofbirth !== null || user.dateofbirth !== null
        ) {
            this.settingsService.set(Settings.wizardNotRun, "false", true)
        }
    }

}
