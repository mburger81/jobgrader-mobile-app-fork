import { TestBed } from '@angular/core/testing';

import { SignProviderService } from './sign-provider.service';

describe('UserProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignProviderService = TestBed.get(SignProviderService);
    expect(service).toBeTruthy();
  });
});
