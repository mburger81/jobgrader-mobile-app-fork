import { Injectable } from '@angular/core';
import { User } from '../../models/User';
import { ToastController } from '@ionic/angular';
import { CryptojsProviderService } from '../cryptojs/cryptojs-provider.service';
import { ApiProviderService } from '../api/api-provider.service';
import { BarcodeService } from '../barcode/barcode.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { EuDgc, EuDgcCert, EuDgcVaccincation } from '../barcode/eudgc';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { DatePipe } from '@angular/common';

const CovidTestIntervalDuration = 24*60*60*1000;

@Injectable({
  providedIn: 'root'
})
export class DeeplinkProviderService {

  public verificationDeeplinkStringValue: string;
  public signatureDeeplinkStringValue: string;
  private readonly deeplinkSecretKey = '287498hsdakjfna';

  public minAge: any;
  public maxAge: any;

  private toast: HTMLIonToastElement;

  constructor(
    private readonly cryptojsProviderService: CryptojsProviderService,
    private readonly apiProviderService: ApiProviderService,
    private readonly toastController: ToastController,
    private readonly _SecureStorageService: SecureStorageService,
    private readonly _TranslateProviderService: TranslateProviderService,
    private readonly _CryptoProviderService: CryptoProviderService,
    private readonly _LoaderProviderService: LoaderProviderService
  ) { }

  public resetVerificationDeeplinkStringValue() {
    this.verificationDeeplinkStringValue = undefined;
  }

  public resetSignatureDeeplinkStringValue() {
    this.signatureDeeplinkStringValue = undefined;
  }

  public temporaryVerifiedDeeplinkResponse(
    sourceId: string,
    accountId: string,
    verificationPath: string,
    identiyId: string,
    name: string,
    phoneNumber: string,
    status: number
  ): any {
    return {
      daimlerString: (this.decodeDeeplinkStringValue(this.verificationDeeplinkStringValue) || {}).publicKey || null,
      sourceId,
      accountId,
      name,
      identiyId,
      phoneNumber,
      verificationPath,
      status
    };
  }

  public temporarySignaturedDeeplinkResponse(
    accountId: string,
    signedDateTime: string,
    expiryDateTime: string,
    status: number
  ): any {
    const signatureDeeplinkStringValue = this.decodeDeeplinkStringValue(this.signatureDeeplinkStringValue) || {};

    return {
      sourceId: signatureDeeplinkStringValue.sourceId,
      accountId,
      status,
      signatureKey: {
        signedDateTime,
        expiryDateTime
      },
    };
  }

  public async verify(basicAuthToken: string, verificationDeeplinkStringValue: string): Promise<any> {
    const decodedDeeplink = this.decodeDeeplinkStringValue(verificationDeeplinkStringValue);

    let verifyDeeplinkPayloadResponse = null;
    if (decodedDeeplink) {
      try {
        console.log('Verify Function, Payload sent to helix mw API call' + JSON.stringify(decodedDeeplink));
        console.log('ageMin: ' + decodedDeeplink.ageMin);
        this.minAge = decodedDeeplink.ageMin;
        this.maxAge = decodedDeeplink.ageMax;
        verifyDeeplinkPayloadResponse = await this.apiProviderService.verifyDeeplinkPayload(basicAuthToken, decodedDeeplink);
        console.log('Verification Response from helix mw: ' + JSON.stringify(verifyDeeplinkPayloadResponse));
      } catch (e) {
        console.log(e);
      }
    }

    return verifyDeeplinkPayloadResponse;
  }

  public async verifyByUserId(basicAuthToken: string, userId: string): Promise<any> {
    let verifyDeeplinkPayloadResponse = null;
    if (userId) {
      try {
        //
        verifyDeeplinkPayloadResponse = await this.apiProviderService.getUserById(basicAuthToken, userId);
      } catch (e) {
        console.log(e);
      }
    }

    return verifyDeeplinkPayloadResponse;
  }

  public async claim(basicAuthToken: string, verificationDeeplinkStringValue: string): Promise<any> {
    const decodedDeeplink = this.decodeDeeplinkStringValue(verificationDeeplinkStringValue);

    let claimDeeplinkPayloadResponse = null;
    if (decodedDeeplink) {
      try {
        claimDeeplinkPayloadResponse = await this.apiProviderService.claimDeeplinkPayload(basicAuthToken, decodedDeeplink);
      } catch (e) {
        console.log(e);
      }
    }

    return claimDeeplinkPayloadResponse;
  }

  public async processPretixTicket(code: string): Promise<void> {

    var oldFormat = {
      "baseurl":"http://localhost:8000",
      "organizer":"BCHX",
      "event":"kcukc",
      "code":"P0F9X",
      "helixMerchantDid":"did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b"
   };

    var newFormat = {
      "baseurl":"https://ticket-int.helixid.io/",
      "organizer":"loewen-frankfurt",
      "event":"earkp",
      "code":"EUELC",
      "userId":"2",
      "helixMerchantDid":"did:evan:testcore:0x6cfe8ee45aa5138adf27c81b189390940956186b"
    };

    var ticketDeeplinkBody = {
      "merchantId": 143
    }

    var prodFormat = {
      "baseurl": "https://ticket.helixid.io/",
      "organizer":"helix-id",
      "event":"qupxs",
      "code":"JTCET",
      "userId":"3",
      "helixMerchantDid":"did:evan:0x2a8A40A91C44941F0305eB793eaf8e10a6303948"
    }

    // console.log(this.encodePayload(newFormat));
    // console.log(this.encodePayload(ticketDeeplinkBody));
    // console.log(this.encodePayload(prodFormat));


    const decodedDeeplink = this.decodeDeeplinkStringValue(code);
    if(decodedDeeplink) {
      try {

        var netData = {};

        var organizerInfo = await this.apiProviderService.pretixMwGetOrganizerInfo(decodedDeeplink.organizer);

        if(organizerInfo) {
          var eventInfo = await this.apiProviderService.pretixMwGetEventInfo(decodedDeeplink.organizer, decodedDeeplink.event);

          if(eventInfo) {

            var orderInfo = await this.apiProviderService.pretixMwGetOrderInfo(decodedDeeplink.organizer, decodedDeeplink.event, decodedDeeplink.code);

              if(orderInfo) {
                Object.assign(netData, { "@context": "https://www.w3.org/2018/credentials/v1" });
                Object.assign(netData, { "type": [ "VerifiableCredential", "HelixMarketplace" ] });
                Object.assign(netData, { "qrCode": (orderInfo as any).positions[0].secret });
                Object.assign(netData, { "credentialSubject": {
                  "data": [{
                    "helixMerchantDid": decodedDeeplink.helixMerchantDid,
                    "companyName": (organizerInfo as any).name,
                    "header": (eventInfo as any).name['en'],
                    // "logo": "",
                    "location": (eventInfo as any).location['en'],
                    "issuer": (organizerInfo as any).name,
                    "issueDate": (orderInfo as any).datetime,
                    "expiryDate": (orderInfo as any).expires,
                    "startDate": (eventInfo as any).date_from,
                    "endDate": (eventInfo as any).date_to,
                    "ticketId": (orderInfo as any).positions[0].order,
                    "pretixTicket": (orderInfo as any).positions[0].secret,
                    "paymentStatus": (orderInfo as any).payments[0].state
                  }]
                } });

                var st = await this._SecureStorageService.getValue(SecureStorageKey.marketplaceCertificates, false);
                var sst = !!st ? JSON.parse(st) : [];
                var f = sst.find(k => k.qrCode == (orderInfo as any).positions[0].secret);
                if(!f) {
                    sst.push(netData);
                    // this._BarcodeService.healthCertificates = sst;
                    await this._SecureStorageService.setValue(SecureStorageKey.marketplaceCertificates, JSON.stringify(sst));
                } else {
                    // this._BarcodeService.healthCertificates = sst;
                    this.presentToast(this._TranslateProviderService.instant('HEALTHCERTIFICATE.alreadyExists'));
                }
              } else {
                alert('Order for this event does not exist')
              }
          } else {
            alert('Event does not exist');
          }
        } else {
          alert('Organizer does not exist')
        }
      } catch (e) {
        console.log(e);
      }
    } else {
      this.presentToast(this._TranslateProviderService.instant('SIGNATURE.invalid-string'));
    }
  }

  public async processEudgcString(code: string): Promise<void> {
    await this._LoaderProviderService.loaderCreate();
    try {
      var decodedDeeplink = this.decodeDeeplinkStringValue(code);
    } catch(e) {
      decodedDeeplink = code;
    }

    console.log("decodedDeeplink", decodedDeeplink);

    if(decodedDeeplink) {
      try {

        var netData = {};

        var dateOfResult = null;
        var dateOfResultToastText = '';

        if(decodedDeeplink.includes("HC1:")) {
            var parseData = await EuDgc.parse(decodedDeeplink);
            console.log("parseData: ");
            console.log(parseData);

            if(!!parseData["t"]){
              dateOfResult = +new Date(parseData["t"][0]["sc"]);
            }

            try {
                var signData = await EuDgc.validate(decodedDeeplink,{
                  certFilter: (certInfo) => { return true; }
              });
                console.log("signData: ");
                console.log(signData);
                netData = Object.assign(netData, { proof: signData });
            } catch (e) {
                netData = Object.assign(netData, { proof: {} });
                console.log(e);
            }
            netData = Object.assign(netData, { qrCode: decodedDeeplink });
            netData = Object.assign(netData, parseData);

        } else {
            try {
                var url = new URL(decodedDeeplink);
                var base64URLstring = url.hash.substring(1);
                if(url.origin == "https://s.coronawarn.app") {
                    var base64string = this._CryptoProviderService.base64urlTobase64(base64URLstring);
                    var usefulBase64String = base64string;
                    var decodedString = window.atob(usefulBase64String);
                    var ob = JSON.parse(decodedString);
                    Object.assign(netData, { qrCode : decodedDeeplink })
                    Object.assign(netData, { t : [
                        ob
                    ] })

                    if(!!ob["timestamp"]){
                      dateOfResult = +new Date(ob["timestamp"]);
                    }

                } else if (url.origin == "https://testverify.io") {
                    var base64string = this._CryptoProviderService.base64urlTobase64(base64URLstring);
                    var usefulBase64String = base64string.split(".")[1];
                    var decodedString = window.atob(usefulBase64String);
                    var ob = JSON.parse(decodedString);
                    Object.assign(netData, { qrCode : decodedDeeplink })
                    Object.assign(netData, { t : [
                        ob
                      ] })

                    if(!!ob["t"]){
                        dateOfResult = +new Date(ob["t"] * 1000);
                    }

                } else if (url.origin == "https://verify.govdigital.de") {
                    var usefulArray = base64URLstring.split(";");
                    var bb = {};
                    usefulArray.forEach(u => {
                        var key = u.split("=")[0];
                        var value = u.split("=")[1];
                        Object.assign(bb, {[key]: value });
                        if(usefulArray.indexOf(u) == usefulArray.length - 1) {
                            Object.assign(netData, { qrCode : decodedDeeplink })
                            Object.assign(netData, { t : [
                                bb
                              ] })
                            if(!!bb["d"]){
                                dateOfResult = +new Date(bb["d"].substring(0,4)+"-"+bb["d"].substring(4,6)+"-"+bb["d"].substring(6,8)+" "+bb["d"].substring(8,10)+":"+bb["d"].substring(10));
                            }
                        }
                    })
                }
            } catch(e) {
                await this.presentToast(this._TranslateProviderService.instant('HEALTHCERTIFICATE.unable-scan'));
                await this._LoaderProviderService.loaderDismiss();
                return;
            }
        }

        if(!!dateOfResult) {
          if(+new Date() > dateOfResult + CovidTestIntervalDuration) {
            dateOfResultToastText = this._TranslateProviderService.instant('HEALTHCERTIFICATE.oldTestCertificate');
          }
        }

        var st = await this._SecureStorageService.getValue(SecureStorageKey.healthCertificates, false);
        var sst = !!st ? JSON.parse(st) : [];
        var f = sst.find(k => k.qrCode == decodedDeeplink);
        if(!f) {
            sst.push(netData);
            // this._BarcodeService.healthCertificates = sst;
            await this._SecureStorageService.setValue(SecureStorageKey.healthCertificates, JSON.stringify(sst));
            await this.presentToast(this._TranslateProviderService.instant('CERTIFICATES.certAddSuccess') + ' ' + dateOfResultToastText);
        } else {
            // this._BarcodeService.healthCertificates = sst;
            await this.presentToast(this._TranslateProviderService.instant('HEALTHCERTIFICATE.alreadyExists'));
        }
      } catch (e) {
        console.log(e);
      }
    } else {
      this.presentToast(this._TranslateProviderService.instant('SIGNATURE.invalid-string'));
    }
    await this._LoaderProviderService.loaderDismiss();
  }

  public decodeDeeplinkStringValue(deeplinkStringValue: string, key?: string) {

    var decKey = !!key ? key : this.deeplinkSecretKey;

    const decodedDeeplink = this.cryptojsProviderService.decrypt(
      deeplinkStringValue,
      decKey
    );
    console.log('Decoded Deeplink String Value' + JSON.stringify(decodedDeeplink));
    return decodedDeeplink;
  }

  public encodePayload(payload: any) {
    const deeplinkStringValue = this.cryptojsProviderService.encrypt(
      payload,
      this.deeplinkSecretKey
    );

    return deeplinkStringValue;
  }

  async presentToast(message: string) {
    this.toast = await this.toastController.create({
      message,
      duration: 3000,
      position: 'top',
    });
    this.toast.present();
  }

}
