import { TestBed } from '@angular/core/testing';

import { DeeplinkProviderService } from './deeplink-provider.service';

describe('DeeplinkProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeeplinkProviderService = TestBed.get(DeeplinkProviderService);
    expect(service).toBeTruthy();
  });
});
