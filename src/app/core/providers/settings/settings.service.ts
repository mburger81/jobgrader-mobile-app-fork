import { Injectable } from '@angular/core';
import { StorageKeys, StorageProviderService } from '../storage/storage-provider.service';
import { ApiProviderService } from '../api/api-provider.service';
import { AppStateService } from '../app-state/app-state.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { EventsService, EventsList } from '../events/events.service';

export enum Settings {
    wizardNotRun = 'wizardNotRun',
    lockScreenToggle = 'lockScreenToggle',
    onboardingHasRun = 'onboardingHasRun',
    userIsAllowedToAccessChatMarketplace = 'userIsAllowedToAccessChatMarketplace',
    kycUsingVerifeyeCompleted = 'kycUsingVerifeyeCompleted',
    kycUsingOndatoCompleted = 'kycUsingOndatoCompleted',
    language = 'language',
    themeMode = 'themeMode',
    driverLicenseSet = 'driverLicenseSet',
    idCardSet = 'idCardSet',
    passportSet = 'passportSet',
    residencePermitSet = 'residencePermitSet',
    paperBackupTimestamp = 'paperBackupTimestamp',
    cloudBackupTimestamp = 'cloudBackupTimestamp'
}

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    constructor(
        private appStateService: AppStateService,
        private storage: StorageProviderService,
        private secureStorage: SecureStorageService,
        private apiProviderService: ApiProviderService,
        private _CryptoProviderService: CryptoProviderService,
        private _EventsService: EventsService,
        private translateService: TranslateProviderService) {}

    public async set(settingName: Settings, value: string, storeSecure = false): Promise<void> {
        let settingId = await (!storeSecure ?
            this.storage.getItem(StorageKeys[settingName + 'SettingId']) :
            this.secureStorage.getValue(SecureStorageKey[settingName + 'SettingId'], false)
        );
        // console.log(settingId)
        let settings;
        if (!settingId) {
            settings = await this.apiProviderService.getUserSettings();
            const setting = settings.find(s => {
                return s.settingName === settingName;
            });
            settingId = setting ? setting.settingId : undefined;
        }
        const o = {
            settingName,
            value,
            modified: +new Date(),
            settingId
        };
        // console.log(JSON.stringify(o))
         this.apiProviderService.sendUserSettings(o).then(settings => {
            if (!storeSecure) {
                this.storage.setItem(StorageKeys[settingName], value);
                this.storage.setItem(StorageKeys[settingName + 'SettingId'], settings.settingId);
            } else {
                this.secureStorage.setValue(SecureStorageKey[settingName], value, false);
                this.secureStorage.setValue(SecureStorageKey[settingName + 'SettingId'], settings.settingId, false);
            }
         }).catch(e => {
            console.log(e);
         })
        
    }

    public async get(settingName: Settings.language): Promise<string>;
    public async get(settingName: Settings.wizardNotRun): Promise<boolean>;
    public async get(settingName: Settings): Promise<boolean | string> {
        return from(this.apiProviderService.getUserSettings())
            .pipe(
                map(settings => settings.filter(s => s.settingName === settingName)),
                map(settings => settings && settings.length ? settings[0].value : undefined),
                map(setting => {
                    if (setting) {
                        if (setting === 'true') {
                            return true;
                        } else if (setting === 'false') {
                            return false;
                        } else {
                            return setting;
                        }
                    } else {
                        return setting;
                    }
                })
            ).toPromise();
    }

    /** Method to get and proceed all settings */
    public async getAll(): Promise<void> {
        const settings = await this.apiProviderService.getUserSettings();
        for ( const setting of settings ) {
            if ( setting.settingName === Settings.language ) {
                await this.secureStorage.setValue(SecureStorageKey.language, setting.value);
                await this.secureStorage.setValue(SecureStorageKey[setting.settingName + 'SettingId'], setting.settingId);
                await this.translateService.changeLanguage(setting.value);
            }
            if ( setting.settingName === Settings.userIsAllowedToAccessChatMarketplace ) {
                await this.secureStorage.setValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, setting.value);
                await this.secureStorage.setValue(SecureStorageKey[setting.settingName + 'SettingId'], setting.settingId);
            }
            if ( setting.settingName === Settings.themeMode ) {
                await this.secureStorage.setValue(SecureStorageKey.themeMode, setting.value);
                await this.secureStorage.setValue(SecureStorageKey[setting.settingName + 'SettingId'], setting.settingId);
                this._EventsService.publish(EventsList.themeChange, setting.value);
            }
            if ( [
                Settings.wizardNotRun, 
                Settings.idCardSet,
                Settings.driverLicenseSet,
                Settings.passportSet,
                Settings.residencePermitSet,
                Settings.onboardingHasRun
            ].includes(setting.settingName) ) {
                await this.secureStorage.setValue(SecureStorageKey[setting.settingName], setting.value);
                await this.secureStorage.setValue(SecureStorageKey[setting.settingName + 'SettingId'], setting.settingId);
            }
            if ( setting.settingName === Settings.lockScreenToggle ) {
                await this.secureStorage.setValue(SecureStorageKey.lockScreenToggle, setting.value);
                await this.secureStorage.setValue(SecureStorageKey[setting.settingName + 'SettingId'], setting.settingId);
            }
        }
    }
}
