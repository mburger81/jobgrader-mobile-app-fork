import { TestBed } from '@angular/core/testing';

import { ResetPasswordProviderService } from './reset-password-provider.service';

describe('ResetPasswordProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResetPasswordProviderService = TestBed.get(ResetPasswordProviderService);
    expect(service).toBeTruthy();
  });
});
