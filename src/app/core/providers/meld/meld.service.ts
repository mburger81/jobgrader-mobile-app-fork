import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';

@Injectable({
  providedIn: 'root'
})
export class MeldService {

  constructor(
    private _Vault: VaultService
  ) { }

  async getApiURL(address: string) {
    const MELD_API_KEY = await this._Vault.getSecret(VaultSecretKeys.MELD_API_KEY);
    const MELD_API_SECRET = await this._Vault.getSecret(VaultSecretKeys.MELD_API_SECRET);
    const url = `https://sb.fluidmoney.xyz/?publicKey=${MELD_API_KEY}:${MELD_API_SECRET}&walletAddress=${address}`;
    return url;
  }

  

}
