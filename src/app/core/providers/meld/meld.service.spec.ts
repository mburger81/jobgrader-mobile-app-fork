import { TestBed } from '@angular/core/testing';

import { MeldService } from './meld.service';

describe('MeldService', () => {
  let service: MeldService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MeldService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
