
export interface MerchantDetails {
    id: any;
    displayToggle: boolean;
    dateModified: string;
    productBackgroundImage: string;
    merchantLogo: string;
    productCategories: Array<string>;
    merchantName: string;
    productName: string;
    merchantClaim: string;
    merchantCity: string;
    merchantCountry: string;
    productPage1Headline: string;
    productPage1BodyText: string;
    productPage1CtaText: string;
    productPage1CtaTargetURL: string;
    productPage1CtaColorcode: string;
    productPage2Headline: string;
    productPage2BodyText: string;
    productPage2CtaText: string;
    productPage2CtaTargetURL: string;
    productPage2CtaColorcode: string;
    productPage3Headline: string;
    productPage3BodyText?: string;
    productHistory?: Array<MerchantPage3HistoryItem>;
    termsConditions?: string;
    merchantRepresentativeXmppUsername?: string;
    productPage1SubHeading?: string; // Need to work on it
    productPage2SubHeading?: string; // Need to work on it
    did?: string;
    // The following are the new keys introduced:
    // userMerchantInteraction?: string; // 20: keys separated by commas in WooCommerce, processed to display on the App side
    merchantWebsite?: string; // 21: URL hyperlinked in the app
    merchantPrivacyPolicy?: string; // 22: URL hyperlinked in the app
    merchantEmail?: string; // 23: mailID deeplinked to the app's default email client
    merchantLegalName?: string;
    merchantAddressStreet?: string; // 24: HTML stored in WooCommerce, displayed in the app directly
    merchantAddressZip?: string; // 24: HTML stored in WooCommerce, displayed in the app directly
    merchantAddressCity?: string; // 24: HTML stored in WooCommerce, displayed in the app directly
    merchantLegalIdentifierHgb?: string; // 24: HTML stored in WooCommerce, displayed in the app directly
    merchantChatUsername?: string;
    displayVoucherCTA1?: boolean; // New attribute 
    displayVoucherCTA2?: boolean; // New attribute
    // New attributes for vouchers
    uniqueVouchersPage1?: Array<string>;
    uniqueVouchersPage2?: Array<string>;
    genericVouchersPage1?: Array<string>;
    genericVouchersPage2?: Array<string>;
    qrCode?: string;
    qrCodeDescription?: string;
    // Discount
    discountInfoPage1?: string;
    discountInfoPage2?: string;
    // Modifications for the Web3 DApps
    openInExternalBrowser1?: boolean;
    openInExternalBrowser2?: boolean;
    kycRequired?: boolean;
    siteKey?: string;
  }
  
  interface MerchantPage3HistoryItem {
    from: Date;
    to: Date;
    object: string;
    place: string;
  }