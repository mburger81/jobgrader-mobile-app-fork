import { Injectable } from '@angular/core';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { MerchantDetails } from './marketplace-model';
import { SQLStorageService } from '../sql/sqlStorage.service';
import { environment } from 'src/environments/environment';
import { UDIDNonce } from '../device/udid.enum';
import * as CryptoJS from 'crypto-js';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
const crypto = require('crypto');
const axios = require("axios");

@Injectable({
  providedIn: 'root'
})
export class MarketplaceService {

  public merchants: MerchantDetails[] = [];

  constructor(
    private _ApiProviderService: ApiProviderService,
    private _SecureStorageService: SecureStorageService,
    private _CryptoProviderService: CryptoProviderService,
    private _Vault: VaultService,
    public _SQLStorageService: SQLStorageService
  ) { }


  private async getMarketpaceBasicAuthToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MARKETPLACE_CONSUMER_KEY).then((marketplace_consumer_key) => {
        this._Vault.getSecret(VaultSecretKeys.MARKETPLACE_CONSUMER_SECRET).then((marketplace_consumer_secret) => {
          resolve(window.btoa(`${marketplace_consumer_key}:${marketplace_consumer_secret}`));
        })
      })
    })
  }

  getMarketplaceData(timestamp?: string): Promise<any> {
    var baseUrl = `${environment.marketplace}/wp-json/wc/v3/products?per_page=100&category=71`
    if(!!timestamp) {
        baseUrl = `${baseUrl}?after=${timestamp}`
    }
    return new Promise((resolve, reject) => {
      this.getMarketpaceBasicAuthToken().then(basicAuthToken => {
        axios.get(baseUrl, {
            headers: {
                Authorization: `Basic ${basicAuthToken}`
            }
        }).then(res => {
          console.log("/wp-json/wc/v3/products?per_page=100&category=20", res.data);
          resolve(res.data)
        }
          ).catch(err => reject(err));
      })
    });
  }

  getMarketplaceAttributeData(id: string): Promise<any> {
      var basicAuthToken = this.getMarketpaceBasicAuthToken()
      var baseUrl = `${environment.marketplace}/wp-json/wc/v3/products/${id}`
      return new Promise((resolve, reject) => {
          axios.get(baseUrl, {
              headers: {
                  Authorization: `Basic ${basicAuthToken}`
              }
          }).then(res => resolve(res.data)).catch(err => reject(err));
      });
  }

  getMarketplaceCoupons(): Promise<any> {
    var basicAuthToken = this.getMarketpaceBasicAuthToken()
    var baseUrl = `${environment.marketplace}/wp-json/wc/v3/coupons`
    return new Promise((resolve, reject) => {
        axios.get(baseUrl, {
            headers: {
                Authorization: `Basic ${basicAuthToken}`
            }
        }).then(res => resolve(res.data)).catch(err => reject(err));
    });
  }

  init(): Promise<void> {
    // console.log('MarketplaceService#init');

    return new Promise<void>((resolve, reject) => {
      this._SecureStorageService.getValue(SecureStorageKey.marketplaceLastModified, false).then(marketplaceLastModified => {
        // console.log('MarketplaceService#init; marketplaceLastModified', marketplaceLastModified);

          this.getMarketplaceData().then(res => {
            // console.log('MarketplaceService#init; data?', (res) ? 'true' : 'false');
            // console.log(res);

            if(marketplaceLastModified) {
              this.updateMarketplace(res, marketplaceLastModified).then(() => {
                this.setMerchants().then(() => {
                  resolve();
                });
              });
            }
            else {
              this.updateMarketplace(res).then(() => {
                this.setMerchants().then(() => {
                  resolve();
                });
              });
            }
        }).catch((e) => reject())
      }).catch((e) => reject())
  })
}

  setMerchants(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this._SQLStorageService.getMarketPlaceMerchants().then(merchants => {
        this.merchants = merchants;
        // console.log(this.merchants);
        resolve()
      }).catch((e) => reject());
    })
  }

  forcePull(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.getMarketplaceData().then(res => {
        if(res.length == 0) {
          resolve()
        } else {
          var lastmodifed = new Date(Math.max(...Array.from(res, k => +new Date((k as any).date_modified_gmt)))).toISOString();
          this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
            var processed = 0;
            res.forEach(_ => {
              var merchant: MerchantDetails = this.processObject(_);
              this._SQLStorageService.addOrUpdateMerchant(merchant).then(() => {
                this.setMerchants();
                processed++;
                if (processed == res.length) { resolve(); }
              })
            })
          })
        }
      })
    })
  }

  getMerchantsByCategory(key: string) {
    // console.log('MarketplaceService#getMerchantsByCategory; key', key);
    // console.log('MarketplaceService#getMerchantsByCategory; this.merchants', this.merchants);

    return this.merchants.filter(m => { return m.productCategories.indexOf(key) > -1; })
  }

  getMerchantById(id: string) {
    if (!this.merchants || (this.merchants && this.merchants.length == 0)) { return null; }
    return this.merchants.find(m => { return m.id == id; })
  }

  private updateMarketplace(res: any, marketplaceLastModified?: string): Promise<void> {
    return new Promise<void>(resolve => {
      if(res.length == 0) {
        resolve();
      }
      else {
        var lastmodifed = new Date(Math.max(...Array.from(res, k => +new Date((k as any).date_modified_gmt)))).toISOString();
        // console.log(lastmodifed);
        // this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
          var processed: number = 0;
          var merchant: MerchantDetails;
          // console.log(res);
          // console.log(marketplaceLastModified);
          res.forEach(_ => {
            if(!!marketplaceLastModified){
              if(this.checkLastmodified(marketplaceLastModified, _)) {
                merchant = this.processObject(_);
                // console.log(merchant);
                if (merchant) {
                  this._SQLStorageService.addOrUpdateMerchant(merchant).then(() => {
                    processed++;
                    // console.log(processed);
                    if (processed == res.length) {
                      this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
                        resolve();
                        })
                      }
                  });
                }
                else {
                  processed++;
                  // console.log(processed);
                  if (processed == res.length) {
                    this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
                      resolve();
                    })
                  }
                }
              } else {
                // var test = this.getMerchantById(merchant.id.toString()); // This is where error occurs
                merchant = this.processObject(_);
                // console.log(merchant);
                var test = this.merchants.find(m => m.id == merchant.id);
                if(!test) {
                  this._SQLStorageService.getMarketPlaceMerchants().then(merchants => {
                    this.merchants = merchants;
                    var test2 = this.merchants.find(m => m.id == merchant.id);
                    if(test2) {
                      processed++;
                      // console.log(processed);
                      if (processed == res.length) {
                        this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
                          resolve();
                        })
                      }
                    } else {
                      merchant = this.processObject(_);
                      // console.log(merchant);
                      this._SQLStorageService.addOrUpdateMerchant(merchant).then(() => {
                        processed++;
                        // console.log(processed);
                        if (processed == res.length) {
                          this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
                            resolve();
                          })
                        }
                      });
                    }

                  })
                } else {
                  processed++;
                  // console.log(processed);
                  if (processed == res.length) {
                    this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
                      resolve();
                    })
                  }
                }
              }
            } else {
              merchant = this.processObject(_);
              if (merchant) {
                this._SQLStorageService.addOrUpdateMerchant(merchant).then(() => {
                  processed++;
                  // console.log(processed);
                  if (processed == res.length) {
                    this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
                      resolve();
                    })
                  }
                });
              }
              else {
                processed++;
                // console.log(processed);
                if (processed == res.length) {
                  this._SecureStorageService.setValue(SecureStorageKey.marketplaceLastModified, lastmodifed).then(() => {
                    resolve();
                  })
                }
              }
            }
          });
        // });
      }
    });
  }

  private checkLastmodified(marketplaceLastModified: string, _: any) {
    // return true;
    return (+new Date(marketplaceLastModified) < (_.date_modified_gmt ? +new Date(_.date_modified_gmt) : +new Date()))
  }

  public returnCouponCodes(id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getMarketplaceCoupons().then((l) => {
        // console.log(l)
        var emptyArray = [];
        if(l.length > 0 && Array.isArray(l)) {
          emptyArray = Array.from(l, ll => (ll  as any).product_ids.includes(Number(id)) ? (ll as any).code : null).filter(x => !!x);
          // console.log('case 1');
          // console.log(emptyArray);
          resolve(emptyArray);
        } else {
          if(!Array.isArray(l)) {
            emptyArray.push(l);
            emptyArray = l.code;
            // console.log('case 2');
            // console.log(emptyArray);
            resolve(emptyArray);
          } else {
            // console.log('case 3');
            // console.log(emptyArray);
            resolve(emptyArray);
          }
        }
      }).catch(e => {
        // console.log(e);
        reject();
      })
    })
  }

  private processObject(_: any) {
    var thirdpageContent = `<strong>Please read the following information carefully</strong> <br><br>In the interests of transparency, we would like to provide you with some legal information about this offer. <br><br>Firstly, the display of this offer is considered advertising and Jobgrader may be remunerated for the display. In addition, we may process the following data as part of this offer, in accordance with our privacy policy:`;
    var modifiedThirdpageContent = `<strong>Please read the following information carefully</strong> <br><br>In the interests of transparency, we would like to provide you with some legal information about this offer. <br><br>The cryptocurrency dApps and trading platforms on the Marketplace operate independently and without our influence. Jobgrader does not recommend that you buy, sell or hold cryptocurrencies. Do your own research and consult your financial advisor before making any investment decisions. Cryptocurrencies are inherently volatile and trading and interacting with dApps involves increased risk. <br><br>We do not provide any guarantees and you interact and trade at your own risk. In the course of providing this service, we may process the following data in accordance with our privacy policy.`;
    try {
      var p = <MerchantDetails> {
        id: _.id,
        displayToggle: _.status ? (_.status == 'publish') : false,
        dateModified: _.date_modified_gmt ? _.date_modified_gmt : new Date().toISOString(),
        productBackgroundImage: _.images ? (_.images.find(k => k.alt == "background") ? _.images.find(k => k.alt == "background").src : '') : '',
        merchantLogo: _.images ? (_.images.find(k => k.alt == "logo") ? _.images.find(k => k.alt == "logo").src : '') : '',
        productCategories: Array.from(_.categories, k => (k as any).name) ?? [],
        merchantName: _.name,
        productName: _.attributes.find(k => k.name == "productName") ? _.attributes.find(k => k.name == "productName").options[0] : '',
        merchantClaim: _.attributes.find(k => k.name == "merchantClaim") ? _.attributes.find(k => k.name == "merchantClaim").options[0] : '',
        merchantCity: _.attributes.find(k => k.name == "merchantCity") ? _.attributes.find(k => k.name == "merchantCity").options[0] : '',
        merchantCountry: _.attributes.find(k => k.name == "merchantCountry") ? _.attributes.find(k => k.name == "merchantCountry").options[0] : '',
        productPage1Headline: _.attributes.find(k => k.name == "productPage1Headline") ? _.attributes.find(k => k.name == "productPage1Headline").options[0] : '',
        productPage1BodyText: _.description ?? '',
        productPage1CtaText: _.attributes.find(k => k.name == "productPage1CtaText") ? _.attributes.find(k => k.name == "productPage1CtaText").options[0] : '',
        productPage1CtaTargetURL: _.attributes.find(k => k.name == "productPage1CtaTargetURL") ? _.attributes.find(k => k.name == "productPage1CtaTargetURL").options[0] : '',
        productPage1CtaColorcode: _.attributes.find(k => k.name == "productPage1CtaColorcode") ? _.attributes.find(k => k.name == "productPage1CtaColorcode").options[0] : '#0021ff',
        productPage2CtaColorcode: _.attributes.find(k => k.name == "productPage2CtaColorcode") ? _.attributes.find(k => k.name == "productPage2CtaColorcode").options[0] : '#0021ff',
        productPage2Headline: _.attributes.find(k => k.name == "productPage2Headline") ? _.attributes.find(k => k.name == "productPage2Headline").options[0] : '',
        productPage2BodyText: _.short_description ?? '',
        productPage2CtaText: _.attributes.find(k => k.name == "productPage2CtaText") ? _.attributes.find(k => k.name == "productPage2CtaText").options[0] : '',
        productPage2CtaTargetURL: _.attributes.find(k => k.name == "productPage2CtaTargetURL") ? _.attributes.find(k => k.name == "productPage2CtaTargetURL").options[0] : '',
        productPage3Headline: _.attributes.find(k => k.name == "productName") ? _.attributes.find(k => k.name == "productName").options[0] : '',
        productPage3BodyText: _.attributes.find(k => k.name == "thirdPageAltContent") ? ((_.attributes.find(k => k.name == "thirdPageAltContent").options[0].trim() == "true") ? modifiedThirdpageContent : thirdpageContent) : thirdpageContent,
        productPage1SubHeading: _.attributes.find(k => k.name == "productPage1SubHeading") ? _.attributes.find(k => k.name == "productPage1SubHeading").options[0] : '',
        productPage2SubHeading: _.attributes.find(k => k.name == "productPage2SubHeading") ? _.attributes.find(k => k.name == "productPage2SubHeading").options[0] : '',
        merchantRepresentativeXmppUsername: _.attributes.find(k => k.name == "merchantRepresentativeXmppUsername") ? _.attributes.find(k => k.name == "merchantRepresentativeXmppUsername").options[0] : null,
        qrCode: _.attributes.find(k => k.name == "qrCode") ? _.attributes.find(k => k.name == "qrCode").options[0] : null,
        qrCodeDescription: _.attributes.find(k => k.name == "qrCodeDescription") ? _.attributes.find(k => k.name == "qrCodeDescription").options[0] : '',
        did: _.attributes.find(k => k.name == "did") ? _.attributes.find(k => k.name == "did").options[0] : '',
        productHistory: [],
        merchantWebsite: _.attributes.find(k => k.name == "merchantWebsite") ? _.attributes.find(k => k.name == "merchantWebsite").options[0] : '', // 21: URL hyperlinked in the app
        merchantPrivacyPolicy: _.attributes.find(k => k.name == "merchantPrivacyPolicy") ? _.attributes.find(k => k.name == "merchantPrivacyPolicy").options[0] : '', // 22: URL hyperlinked in the app
        merchantEmail: _.attributes.find(k => k.name == "merchantEmail") ? _.attributes.find(k => k.name == "merchantEmail").options[0] : '', // 23: mailID deeplinked to the app's default email client
        merchantLegalName: _.attributes.find(k => k.name == "merchantLegalName") ? _.attributes.find(k => k.name == "merchantLegalName").options[0] : '',
        merchantAddressStreet: _.attributes.find(k => k.name == "merchantAddressStreet") ? _.attributes.find(k => k.name == "merchantAddressStreet").options[0] : '',
        merchantAddressZip: _.attributes.find(k => k.name == "merchantAddressZip") ? _.attributes.find(k => k.name == "merchantAddressZip").options[0] : '',
        merchantAddressCity: _.attributes.find(k => k.name == "merchantAddressCity") ? _.attributes.find(k => k.name == "merchantAddressCity").options[0] : '',
        merchantLegalIdentifierHgb: _.attributes.find(k => k.name == "merchantLegalIdentifierHgb") ? _.attributes.find(k => k.name == "merchantLegalIdentifierHgb").options[0] : '',
        merchantChatUsername: _.attributes.find(k => k.name == "merchantChatUsername") ? _.attributes.find(k => k.name == "merchantChatUsername").options[0] : '',
        discountInfoPage1: _.attributes.find(k => k.name == "discountInfoPage1") ? _.attributes.find(k => k.name == "discountInfoPage1").options[0] : null,
        discountInfoPage2: _.attributes.find(k => k.name == "discountInfoPage2") ? _.attributes.find(k => k.name == "discountInfoPage2").options[0] : null,
        uniqueVouchersPage1: _.attributes.find(k => k.name == "uniqueVouchersPage1") ? Array.from(_.attributes.find(k => k.name == "uniqueVouchersPage1").options[0].split(","), m => (m as string).trim()).filter(l => l != "") : [],
        uniqueVouchersPage2: _.attributes.find(k => k.name == "uniqueVouchersPage2") ? Array.from(_.attributes.find(k => k.name == "uniqueVouchersPage2").options[0].split(","), m => (m as string).trim()).filter(l => l != "") : [],
        genericVouchersPage1: _.attributes.find(k => k.name == "genericVouchersPage1") ? Array.from(_.attributes.find(k => k.name == "genericVouchersPage1").options[0].split(","), m => (m as string).trim()).filter(l => l != "") : [],
        genericVouchersPage2: _.attributes.find(k => k.name == "genericVouchersPage2") ? Array.from(_.attributes.find(k => k.name == "genericVouchersPage2").options[0].split(","), m => (m as string).trim()).filter(l => l != "") : [],
        openInExternalBrowser1: _.attributes.find(k => k.name == "openInExternalBrowser1") ? (_.attributes.find(k => k.name == "openInExternalBrowser1").options[0].toLowerCase() == "true") : false,
        openInExternalBrowser2: _.attributes.find(k => k.name == "openInExternalBrowser2") ? (_.attributes.find(k => k.name == "openInExternalBrowser2").options[0].toLowerCase() == "true") : false,
        kycRequired: _.attributes.find(k => k.name == "kycRequired") ? (_.attributes.find(k => k.name == "kycRequired").options[0].toLowerCase() == "true") : false,
      };

      p.uniqueVouchersPage1 = _.attributes.find(k => k.name == "uniqueVouchersPage1Encrypted") ? this.tryCouponDecryption(_.attributes.find(k => k.name == "uniqueVouchersPage1Encrypted").options[0], p.uniqueVouchersPage1) : p.uniqueVouchersPage1;
      p.uniqueVouchersPage2 = _.attributes.find(k => k.name == "uniqueVouchersPage2Encrypted") ? this.tryCouponDecryption(_.attributes.find(k => k.name == "uniqueVouchersPage2Encrypted").options[0], p.uniqueVouchersPage2) : p.uniqueVouchersPage2;
      p.genericVouchersPage1 = _.attributes.find(k => k.name == "genericVouchersPage1Encrypted") ? this.tryCouponDecryption(_.attributes.find(k => k.name == "genericVouchersPage1Encrypted").options[0], p.genericVouchersPage1) : p.genericVouchersPage1;
      p.genericVouchersPage2 = _.attributes.find(k => k.name == "genericVouchersPage2Encrypted") ? this.tryCouponDecryption(_.attributes.find(k => k.name == "genericVouchersPage2Encrypted").options[0], p.genericVouchersPage2) : p.genericVouchersPage2;

      p.displayVoucherCTA1 = _.attributes.find(k => k.name == "displayVoucherCTA1") ? (_.attributes.find(k => k.name == "displayVoucherCTA1").options[0].toLowerCase() == "true") : ((p.uniqueVouchersPage1.length + p.genericVouchersPage1.length) > 0);
      p.displayVoucherCTA2 = _.attributes.find(k => k.name == "displayVoucherCTA2") ? (_.attributes.find(k => k.name == "displayVoucherCTA2").options[0].toLowerCase() == "true") : ((p.uniqueVouchersPage2.length + p.genericVouchersPage2.length) > 0);

      if(environment.helixEnv == 'integration') {
        if(p.id == "144" || p.id == "151" || p.id == 144 || p.id == 151 || p.id == "180" || p.id == 180) {
          p.productPage3BodyText = `<strong>Please read the following information carefully.</strong> <br><br>In the interest of transparency, <strong>${_.attributes.find(k => k.name == "productName") ? _.attributes.find(k => k.name == "productName").options[0] : ''}</strong> would like to provide you with some legal information about the offer. <br><br>First, the display of this offer is considered advertising and <strong>${_.attributes.find(k => k.name == "productName") ? _.attributes.find(k => k.name == "productName").options[0] : ''}</strong> may compensate helix id for the display. <br><br>Furthermore, in the course of providing this offer, the following data will be processed and shared with <strong>${_.attributes.find(k => k.name == "productName") ? _.attributes.find(k => k.name == "productName").options[0] : ''}</strong> in accordance with our privacy policy:`
        }
      }

      return p;
    }
    catch(e) {
      // console.log('*** Error in processObject: ***')
      // console.log(e);
      return null;
    }
  }

  tryCouponDecryption(encryptedCoupons: string, failSafe: Array<string>) {
    try {
      return Array.from(this.symmetricDecrypt(encryptedCoupons.trim(), UDIDNonce.coupons).split(","), m => (m as any).trim()).filter(l => l != "");
    } catch(e) {
      return failSafe;
    }
  }

  base64Tobase64url = (input) => {
    if(input){
        input = input
        .replace(/\+/g, '-')
        .replace(/\//g, '_');
    }
    return input;
}

  base64ToHex = (str) => {
    const raw = atob(str);
    let result = '';
    for (let i = 0; i < raw.length; i++) {
      const hex = raw.charCodeAt(i).toString(16);
      result += (hex.length === 2 ? hex : '0' + hex);
    }
    return result.toUpperCase();
  }

  base64urlTobase64 = (input) => {
    if (input) {
      input = input
          .replace(/-/g, '+')
          .replace(/_/g, '/');

      // Pad out with standard base64 required padding characters
      const pad = input.length % 4;
      if (pad) {
        if (pad === 1) {
          throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
        }
        input += new Array(5 - pad).join('=');
      }
    }
    return input;
  }

  uInt8ArrayToBase64 = ( bytes ) => {
      let binary = '';
      const len = bytes.length;
      for (let i = 0; i < len; i++) {
          binary += String.fromCharCode( bytes[ i ] );
      }
      return btoa( binary );
  }

  hex2i = (hexString) => {
      return new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
  }

  symmetricDecrypt = (text, symmetricKey) => {
      var hexwith0C = this.base64ToHex(this.base64urlTobase64(text));
      var byteArrayResult = this.hex2i(hexwith0C.slice(2));
      var byteArrayObject = {
          iv: byteArrayResult.slice(0, 12),
          ciphertext: byteArrayResult.slice(12, byteArrayResult.length - 16),
          authTag: byteArrayResult.slice(byteArrayResult.length - 16)
      };
      var byteArrayObjectHex = {
          iv: Buffer.from(this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.iv)), 'hex'),
          ciphertext: Buffer.from(this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.ciphertext)), 'hex'),
          authTag: Buffer.from(this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.authTag)), 'hex')
      };
      var key = Buffer.from(symmetricKey, 'hex');
      var decipher = crypto.createDecipheriv('aes-256-gcm', key, byteArrayObjectHex.iv);
      decipher.setAuthTag(byteArrayObjectHex.authTag);
      let decrypted = decipher.update(byteArrayObjectHex.ciphertext, 'hex', 'utf8');
      decrypted += decipher.final();
      return decrypted;
  }

}
