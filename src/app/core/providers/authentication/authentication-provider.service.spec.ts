import { TestBed } from '@angular/core/testing';

import { AuthenticationProviderService } from './authentication-provider.service';

describe('AuthenticationProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthenticationProviderService = TestBed.get(AuthenticationProviderService);
    expect(service).toBeTruthy();
  });
});
