import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';

declare var window: any;

@Injectable({
  providedIn: 'root'
})
export class IcloudService {

  constructor(
    private platform: Platform
  ) { }

  cloudKitInit() {
    if(this.platform.is('ios') && this.platform.is('hybrid')) {
        var config = window.CloudKit.configure({
            containers: [{
                containerIdentifier: environment.icloud,
                apiTokenAuth: {
                    apiToken: 'e8f4adeec95581fec6a459b837d2c19f3bb2c4b499e16b32c6ccee42945b289a'
                },
                environment: 'development'
            }]
        });
        console.log(config);
    }
  }

  // iCloudTesting() {
  //   if(this.platform.is('hybrid') && this.platform.is('ios')) {
  //       console.log(this._File.documentsDirectory); // on my phone / helix id
  //       console.log(this._File.syncedDataDirectory); // iCloud / helix id
  //       console.log(this._File.dataDirectory); // ?? Don't know
  //   }
  // }

}
