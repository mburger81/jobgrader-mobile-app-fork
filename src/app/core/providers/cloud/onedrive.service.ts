import { Injectable } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
const axios = require('axios');

@Injectable({
  providedIn: 'root'
})

export class OnedriveService {

  user = null;
  access_token = null;

  constructor(
    private _Platform: Platform,
    private _ToastController: ToastController
  ) { 
   
  }


  async signInToOnedrive() {    
  
  }

  presentToast(message: string) {
    this._ToastController.create({
      position: 'top',
      duration: 3000,
      message,
    }).then(toast => toast.present())
  }

}
