import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { OtpVerificationComponent } from './otp-verification.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CodeInputModule } from 'angular-code-input';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    CodeInputModule,
    TranslateModule.forChild(),
  ],
  declarations: [OtpVerificationComponent],
  exports: [OtpVerificationComponent]
})
export class OtpVerificationModule {}
