import { Injectable, OnDestroy } from '@angular/core';
import { Device as DeviceModel } from '../../../core/models/Device';
import { User } from '../../../core/models/User';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../../../core/providers/crypto/crypto-provider.service';
import { AppStateService } from '../../../core/providers/app-state/app-state.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { Subject } from 'rxjs';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from './udid.enum';
import { Capacitor } from '@capacitor/core';
import { Device, DeviceId, DeviceInfo } from '@capacitor/device';
import { Platform } from '@ionic/angular';


declare var window: any;

interface DevicePluginInformation {
  capacitor?: boolean;
  model?: string;
  platform?: string;
  uuid?: string;
  version?: string;
  manufacturer?: string;
  isVirtual?: boolean;
  // serial?: string;
}

@Injectable({
  providedIn: 'root'
})

export class DeviceService implements OnDestroy {

  // public crypto: Crypto;
  private destroy$ = new Subject();
  private user: User;
  private device: DeviceModel;
  private index = -1;
  private hashUDID = '';
  private deviceList: Array<DeviceModel>;
  private devicePluginInformation: DevicePluginInformation = {};

  constructor(
    private secureStorageService: SecureStorageService,
    private cryptoProviderService: CryptoProviderService,
    private apiProviderService: ApiProviderService,
    private appStateService: AppStateService,
    private appVersion: AppVersion,
    private _Platform: Platform
  ) {  }

  public ngOnDestroy() {
      this.destroy$.next(0);
  }

  async getDeviceId() {
    var deviceId = '';
    const userbody = await this.secureStorageService.getValue(SecureStorageKey.userData, false);
    this.hashUDID = await this.secureStorageService.getValue(SecureStorageKey.hashUDID, false) || '';
    const user = JSON.parse(userbody);
    this.deviceList = await this.apiProviderService.getUserDevices(this.appStateService.basicAuthToken, user.userid);
    var deviceIndex = this.returnDeviceID();
    // console.log(deviceIndex)
    if (deviceIndex >= 0) {
      deviceId = this.deviceList[deviceIndex].deviceId;
      await this.secureStorageService.setValue(SecureStorageKey.deviceInfo, deviceId);
    }
    else {
      deviceId = 'newDevice'
    }
    return deviceId;
  }

  async pushDeviceInfo(otp?: string) {
    var parsedData = {};
    var userbody = await this.secureStorageService.getValue(SecureStorageKey.userData, false);
    this.hashUDID = await this.secureStorageService.getValue(SecureStorageKey.hashUDID, false) || '';
    this.user = JSON.parse(userbody);
    var devicebody = await this.getDeviceBody(parsedData);
    this.device = devicebody;
    this.deviceList = await this.apiProviderService.getUserDevices(this.appStateService.basicAuthToken, this.user.userid);
    var deviceIndex = this.returnDeviceID();
    console.log(deviceIndex);
    if( deviceIndex >= 0 ) {
      this.device.deviceId = this.deviceList[deviceIndex].deviceId;
      this.device.modified = this.device.created;
      this.device.created = this.deviceList[deviceIndex].created;
    }
    console.log('POST device body' + JSON.stringify(this.device));
    if(!!this.device.deviceId) {
      await this.secureStorageService.setValue(SecureStorageKey.deviceInfo, this.device.deviceId);
    }
    const response = !!otp ? await this.apiProviderService.sendUserDeviceInfo(this.appStateService.basicAuthToken, this.user.userid, this.device, otp).catch(e => {
      return false;
    }) : await this.apiProviderService.sendUserDeviceInfo(this.appStateService.basicAuthToken, this.user.userid, this.device).catch(e => {
      return false;
    });
    if(response) {
      if(!response.errorFound) {
        this.deviceList = await this.apiProviderService.getUserDevices(this.appStateService.basicAuthToken, this.user.userid);
        var deviceIndex = this.returnDeviceID();
        if(deviceIndex > -1) {
          await this.secureStorageService.setValue(SecureStorageKey.deviceInfo, this.deviceList[deviceIndex].deviceId);
        }
        await this.cryptoProviderService.getUserSymmetricKeyIfNeeded().catch(e => {
          return false;
        });
        // console.log("This should get executed first");
        return true;
      }
      else {
        return false;
      }
    } else {
      return false;
    }
    
  }

  async getDeviceBody(parsedData) {

    let device: DeviceInfo;
    let deviceId: DeviceId;

    if (this._Platform.is('hybrid') && Capacitor.isPluginAvailable('Device')) {
      device = await Device.getInfo();
      deviceId = await Device.getId();
    }


    this.devicePluginInformation = {
      capacitor: !device ? false : true,
      model: !device ? (window["navigator"].appName + " " + window["navigator"].appCodeName) : device.model,
      platform: !device ? window["navigator"].platform : device.platform,
      uuid: !device ? '' : deviceId.identifier,
      version: !device ? window["navigator"].appVersion.split(" ")[0] : device.osVersion,
      manufacturer: !device ? window["navigator"].vendor : device.manufacturer,
      // isVirtual: !device ? true : device.isVirtual,
      // TOTO mburger: serial does not exist on capacitor plugin, do we need this?
      // serial: !device ? '' : device.serial
    }

    let app_version = '';
    let app_build = '';
    if(!!device) {
      app_version = await this.appVersion.getVersionNumber();
      app_build = (await this.appVersion.getVersionCode()).toString();
    }

    this.hashUDID = await this.secureStorageService.getValue(SecureStorageKey.hashUDID, false) || '';

    const dev: DeviceModel = {
      activeSubscriptionInfoCount : !!parsedData.activeSubscriptionInfoCount ? parsedData.activeSubscriptionInfoCount : null,
      activeSubscriptionInfoCountMax : !!parsedData.activeSubscriptionInfoCountMax ? parsedData.activeSubscriptionInfoCountMax : null,
      callState : !!parsedData.callState ? parsedData.callState : null,
      carrierName : !!parsedData.carrierName ? parsedData.carrierName : null,
      countryCode : !!parsedData.countryCode ? parsedData.countryCode : null,
      created : +new Date(),
      dataActivity : !!parsedData.dataActivity ? parsedData.dataActivity : null,
      deviceId : '',
      deviceSoftwareVersion : !!parsedData.deviceSoftwareVersion ? parsedData.deviceSoftwareVersion : null,
      displayName : !!parsedData.displayName ? parsedData.displayName : null,
      udid: this.hashUDID,
      appVersion: app_version,
      appBuild: app_build,
      osPlatform: this.devicePluginInformation.platform,
      osVersion: this.devicePluginInformation.version,
      model: this.devicePluginInformation.model,
      manufacturer: this.devicePluginInformation.manufacturer,
      virtual: this.devicePluginInformation.isVirtual,
      firebaseToken :  await this.secureStorageService.getValue(SecureStorageKey.firebase, false),
      isNetworkRoaming : !!parsedData.isNetworkRoaming ? parsedData.isNetworkRoaming : null,
      mcc : !!parsedData.mcc ? parsedData.mcc : null,
      mnc : !!parsedData.mnc ? parsedData.mnc : null,
      modified : null,
      networkType : !!parsedData.networkType ? parsedData.networkType : null,
      phone : !!parsedData.phone ? parsedData.phone : null,
      phoneCount : !!parsedData.phoneCount ? parsedData.phoneCount : null,
      phoneNumber : !!parsedData.phoneNumber ? parsedData.phoneNumber : null,
      phoneType : !!parsedData.phoneType ? parsedData.phoneType : null,
      publicKeyDelegate : null ,
      publicKeyEncryption : await this.secureStorageService.getValue(SecureStorageKey.devicePublicKeyEncryption, false) ,
      sealoneId : 'sealoneId',
      simSerialNumber : !!parsedData.simSerialNumber ? parsedData.simSerialNumber : null,
      simState : !!parsedData.simState ? parsedData.simState : null,
      subscriberId : !!parsedData.subscriberId ? parsedData.subscriberId : '',
      userId : this.user.userid
    };
    return dev;
  }

  returnDeviceID() {
    this.index = -1;
    // if(!device) {
    //   this.index = 0;
    //   return 0;
    // }
    if(this.deviceList.length == 1 && (!this.deviceList[0].udid || this.deviceList[0].udid == '')){
      this.index = 0;
      return 0;
    }
    var listofudid = Array.from(this.deviceList, x => x.udid);
    // console.log(listofudid);
    this.index = listofudid.indexOf(this.hashUDID);
    // console.log("index: " + this.index);
    return this.index;
  }

  obtainUDID(): Promise<void> {
  return new Promise((resolve, reject) => {
    if (this._Platform.is('hybrid') && Capacitor.isPluginAvailable('Device')) {
      Device.getId().then((deviceId: DeviceId) => {
        console.info('AuthenticationProvider#obtainUDID; deviceId', deviceId);

        var crypted = CryptoJS.SHA256(deviceId.identifier, UDIDNonce.helix as any).toString(CryptoJS.enc.Hex);
        this.secureStorageService.setValue(SecureStorageKey.hashUDID, crypted).then(() => {
          resolve();
        }).catch(err => {
          console.log(err);
          resolve();
        });

      },
      (err) => {
        console.log('err: ' + JSON.stringify(err));
        resolve();
      });
    }
    else {
        var udid = window["navigator"].vendor + window["navigator"].appName + window["navigator"].appCodeName;
        var crypted = CryptoJS.SHA256(udid, UDIDNonce.helix as any).toString(CryptoJS.enc.Hex);
        this.secureStorageService.setValue(SecureStorageKey.hashUDID, crypted).then(() => {
          resolve();
        }).catch(err => {
          console.log(err);
          resolve();
        })
    }
  })
  }

  updateFirebaseTokenInDeviceTable(firebaseToken: any) {
    this.secureStorageService.getValue(SecureStorageKey.deviceInfo).then(deviceId => {
      console.log("firebaseToken, deviceId: " + deviceId);
      if(deviceId && this.appStateService.basicAuthToken && deviceId != "undefined") {
        this.apiProviderService.updateFirebaseToken(typeof(firebaseToken) == "string" ? (firebaseToken as string) : firebaseToken, deviceId).then(res => {
          console.log("firebaseToken: " + firebaseToken + " :: updated: " + JSON.stringify(res));
        }).catch(e => {
          console.log("firebaseToken: " + firebaseToken + " :: error: " + JSON.stringify(e));
        });
      }
    })
  }

}
