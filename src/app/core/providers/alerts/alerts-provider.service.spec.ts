import { TestBed } from '@angular/core/testing';

import { AlertsProviderService } from './alerts-provider.service';

describe('AlertsProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlertsProviderService = TestBed.get(AlertsProviderService);
    expect(service).toBeTruthy();
  });
});
