import { Injectable } from '@angular/core';

import { AlertController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class AlertsProviderService {

  public alertWindow: any;

  constructor(private readonly alertController: AlertController) { }

  async alertCreate(header: string, subHeader: string, message: string, buttons: any[] = ['Cancel']) {
    await this.alertDismiss();
    this.alertWindow = await this.alertController.create({
      mode: 'ios',
      header,
      subHeader,
      message,
      buttons,
      backdropDismiss: false
    });

    await this.alertWindow.present();
  }

  async alertDismiss() {
    if (this.alertWindow) {
      await this.alertWindow.dismiss();
    }
  }

}
