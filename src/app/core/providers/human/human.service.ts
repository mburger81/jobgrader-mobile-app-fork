import { Injectable } from '@angular/core';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { UserProviderService } from '../user/user-provider.service';

// import * as Human from "@human-protocol/core/typechain-types";
// import Manifest from "human-protocol/packages/examples/fortune/manifest.json";
// import HMToken from "@human-protocol/core/abis/HMToken.json";
// import Escrow from "@human-protocol/core/abis/Escrow.json";
// import sendFortune from "human-protocol/packages/examples/fortune/exchange/src/services/RecordingOracle/RecordingClient";
// import * as Human from "human-protocol/packages/examples/fortune/exchange/src";
import axios from 'axios';
import { AlertController, Platform, ToastController } from '@ionic/angular';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { ethers } from 'ethers';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { values } from 'lodash';
import { environment } from '../../../../environments/environment';

const HUMAN_ACCOUNTS = "https://foundation-yellow-accounts.hmt.ai";
const HUMAN_EXCHANGE = "https://foundation-yellow-exchange.hmt.ai";
const HUMAN_DASH = "https://foundation-yellow-dash.hmt.ai";

const api_key = ""; // Fetch from Vault

export interface FortuneJob {
  address: string;
  chainId: number;
  title: string;
  description: string;
  token: string;
  estimatedPrice: number;
  solutionsRequired: number;
  type: number;
}

@Injectable({
  providedIn: 'root'
})
export class HumanService {

  private browser: InAppBrowserObject;
  browserOptions = 'zoom=no,footer=no,hideurlbar=yes,footercolor=#BF7B54,hidenavigationbuttons=yes,presentationstyle=pagesheet';

  constructor(
    private _UserProviderService: UserProviderService,
    private _ToastController: ToastController,
    private _Translate: TranslateProviderService,
    private _AlertController: AlertController,
    private _InAppBrowser: InAppBrowser,
    private _Platform: Platform,
    private _SafariViewController: SafariViewController,
    private _SecureStorageService: SecureStorageService,
    private http: HttpClient
  ) { }

  ObtainSiteKeys(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._UserProviderService.getUser().then(user => {
        console.log(user);
        this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then(eth_addr => {
          var email = user.email;
          var country = user.country;
          var userId = user.userid;
          console.log(eth_addr);
          var body = { email, language: 'en', country: "DE", eth_addr, userId };
          console.log(body);
          axios.post(`https://foundation-yellow-accounts.hmt.ai/labeler/register`, body ,
          { params: { api_key } }).then(response => {
            console.log(response.data);
            resolve(response.data);
          }).catch(error => {
            reject(error);
          })
        })
      })
    })
  }

  VerifyCaptcha( secret: string, sitekey: string, response: string, remoteip: string ): Promise<any> {
    return new Promise((resolve, reject) => {
      axios.post(`${HUMAN_EXCHANGE}/siteverify`, null, {
        params: {
          secret, // : 0xF02479585571241EA12996B27F996dAf4f9Bb32A, // secret key from test_enterprise_publisher@test.com acc
          sitekey, // : "30564186-e300-473b-a21d-f3411e7a21c2", // sitekey
          response, // : token, // token from solving captcha
          remoteip // : ip address
        }
      }).then(response => {
          console.log(response.data);
          resolve(response.data);
      }).catch(error => {
          console.log(error);
          reject(error);
      })
    })
  }

  RegisterLebeller( email: string, language: string, country: string, eth_addr: string, userId: string, api_key: string ): Promise<any> {
    return new Promise((resolve, reject) => {
      axios.post(`${HUMAN_ACCOUNTS}/labeler/register`, {
        email,
        language, //: 'en',
        country,
        eth_addr,
        userId
      }, {
        params: {
          api_key
        }
      }).then(response => {
          console.log(response.data);
          resolve(response.data);
      }).catch(error => {
          console.log(error);
          reject(error);
      })
    })
  }

  RetrieveLebellerStats(email: string, api_key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      axios.get(`${HUMAN_ACCOUNTS}/support/labeler/${email}`, {
        params: {
          api_key
        }
      }).then(response => {
        console.log(response.data);
        resolve(response.data);
      }).catch(error => {
        console.log(error);
        reject(error);
      })
    })
  }

  ShowCaptcha() {
    var hcaptcha = ((window as any).hcaptcha);
    hcaptcha.render(`h-captcha`, {
        sitekey: '', // Fetch from Vault
        theme: 'dark',
        'error-callback': 'onError',
      }).then(() => {
        hcaptcha.execute(`#h-captcha-execute`, { async: true }).then(({ response, key}) => {
            console.log(response, key);
        })
      }).catch(() => {
        hcaptcha.execute(`#h-captcha-execute`, { async: true }).then(({ response, key}) => {
            console.log(response, key);
        })
      })
  }

  presentToast(message: string) {
    this._ToastController.create({ message, duration: 2000, position: 'top' }).then((toast) => {
      toast.present();
    })
  }

  FecthFortuneTasks(address: string): Observable<FortuneJob[]> {

    return this.http.get<FortuneJob[]>(`${environment.fortune.url}/api/jobs?workerAddress=${address}`, {
        headers: {

        }
    });
  }

  FecthFortuneJob(workerAddress: string, escrowAddress: string): Observable<Array<any>> {

    return this.http.get<Array<any>>(`${environment.fortune.url}/api/jobs/${escrowAddress}?workerAddress=${workerAddress}`, {
        headers: {

        }
    });
  }

  sendFortunes(body: {}) {
    return this.http.post(`${environment.fortune.url}/api/send-fortunes`, body);
  }

  SendFortuneSolutions(): Promise<any> {
    return new Promise((resolve, reject) => {
      axios.post(`https://fortune-exchange-oracle-server.vercel.app/jobs/soutions`, {}).then((res) => {
        console.log("FecthFortuneTasks", res.data);
        resolve(res.data);
      }).catch(e => {
        reject();
      })
    })
  }

  async fortuneInit() {

    // console.log(Manifest);
    // console.log(Escrow);
    // console.log(HMToken);

    const FORTUNE = {
      GLOBAL: "https://exchange-oracle.vercel.app/",  // "https://fortune-exchange-mumbai.vercel.app/",
      LAUNCHER: "http://localhost:3000/",
      EXCHANGE: "http://localhost:3001/",
      RECORDING: "http://localhost:3005/",
      REPUTATION: "http://localhost:3006/",
    };
    console.log(FORTUNE);

    // var mnemonic = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false);

    // if(!mnemonic) {

    //   var importedWalletString = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
    //   if(!importedWalletString) {
    //     return this.presentToast(this._Translate.instant('IMPORTKEY.initiate-heading') + this._Translate.instant('IMPORTKEY.initiate-message'))
    //   }
    //   var importedWallet = JSON.parse(importedWalletString);
    //   console.log(importedWallet);
    //   mnemonic = importedWallet[0].mnemonic;

    // }

    // var wallet = ethers.Wallet.fromMnemonic(mnemonic);
    // console.table(wallet);

    // const escrowContract = "0x0376D26246Eb35FF4F9924cF13E6C05fd0bD7Fb4";
    // const provider = "https://polygon-rpc.com";
    // var contract = new ethers.Contract(escrowContract, Escrow);
    // console.log(contract);
    // var escrowConnection = contract.connect(ethers.getDefaultProvider(provider));
    // console.log(escrowConnection);
    // var balance = await escrowConnection.balanceOf(wallet.address);
    // console.log(balance);

    const alerty = await this._AlertController.create({
      header: "Select scope",
      message: "Please choose which scope you wish to connect to.",
      buttons: [

        { text: "Local", handler: async () => {
          const alerto = await this._AlertController.create({
            header: "IP Address",
            message: "Please enter the Local IP Address where the fortune dApp is launched",
            inputs: [
              { type: 'text', name: 'ip', placeholder: '192.168.0.1' }
            ],
            buttons: [
              { text: "Cancel", role: "cancel", handler: () => {} },
              { text: "Connect", handler: (data) => {
                console.log(data);
                this.openBrowser(data.ip);
              } }
            ]
          });
          await alerto.present();
        } },
        { text: "Global", handler: async () => {
          this.openBrowser(FORTUNE.GLOBAL);
        } },
        { text: "Cancel", role: "cancel", handler: () => {} },
      ]
    })

    await alerty.present();



    // const web3 = getWeb3();
    // const [escrow, setEscrow] = useState('');
    // const [fortune, setFortune] = useState('');
    // const [escrowStatus, setEscrowStatus] = useState('');
    // const [balance, setBalance] = useState('');
    // const [recordingOracleUrl, setRecordingOracleUrl] = useState('');

    // const setMainEscrow = useCallback(async (address: string) => {
    //   setEscrow(address);
    //   const Escrow = new web3.eth.Contract(EscrowABI as [], address);

    //   const escrowSt = await Escrow.methods.status().call();
    //   setEscrowStatus(statusesMap[escrowSt]);

    //   const balance = await Escrow.methods.getBalance().call();
    //   setBalance(web3.utils.fromWei(balance, 'ether'));

      // const manifestUrl = await (Escrow as any).methods.manifestUrl().call();
      // if (manifestUrl) {
      //   const manifestContent = (await axios.get(manifestUrl)).data;
      //   console.log(manifestContent);
      //   setRecordingOracleUrl(manifestContent.recording_oracle_url);
      // }
    //   return;
    // }, [web3.eth.Contract, web3.utils])

    // useEffect(() => {
    //   const qs: any = parseQuery(window.location.search);
    //   const address = qs.address;
    //   if (web3.utils.isAddress(address)) {
    //     setMainEscrow(web3.utils.toChecksumAddress(address));
    //   }
    // }, [setMainEscrow, web3.utils]);

    // const send = async () => {
    //   await sendFortune(escrow, fortune, recordingOracleUrl);
    //   alert('Your fortune has been submitted');
    //   setFortune('');
    //   return;
    // }

    // console.log(sendFortune);
  }

  showSafariInstance(url: string) {
    this._SafariViewController.isAvailable().then((available: boolean) => {
            if ( available ) {
                this._SafariViewController.show({
                    url,
                    hidden: false,
                    animated: true,
                    transition: 'slide',
                    enterReaderModeIfAvailable: false,
                    tintColor: '#54BF7B'
                })
                    .subscribe((result: any) => {
                        },
                        (error: any) => console.error(error));
            } else {
                this.browser = this._InAppBrowser.create(url, '_self', this.browserOptions);
            }
        }
    );
  }

  openBrowser(url: string) {
    console.log(url);
    if ( this._Platform.is('ios') && this._Platform.is('hybrid') ) {
          this.showSafariInstance(url);
      } else if ( this._Platform.is('android') && this._Platform.is('hybrid') ) {
          this.browser = this._InAppBrowser.create(url, '_self', this.browserOptions);
      } else if ( !this._Platform.is('hybrid') ) {
          this.browser = this._InAppBrowser.create(url, '_system');
      }
  }

}
