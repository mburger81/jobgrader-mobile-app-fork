export enum SecureStorageErrorCode {
    storeCouldNotBeCreated = -1,
    errorWhileStoringAValue = 0,
    accessedValueNotAvailable = 1,
    valueCouldNotBeDeleted = 2
}
