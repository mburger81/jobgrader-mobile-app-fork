export interface SecureStorageError {
    errorCode: number;
    errorMessage: string;
}
