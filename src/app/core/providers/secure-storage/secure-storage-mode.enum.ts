export enum SecureStorageMode {
    secure = 'secure',
    fallback = 'fallback'
}
