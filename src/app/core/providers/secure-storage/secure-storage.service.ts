import { Injectable, OnDestroy } from '@angular/core';;
import { from, Observable, of, Subject } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
// import * as CryptoJS from 'crypto-js';

//
import { SecureStorageError } from './secure-storage-error.interface';
import { SecureStorageMode } from './secure-storage-mode.enum';
import { find } from 'lodash';
import { SecureStorageErrorCode } from './secure-storage-error-code.enum';
import { SecureStorageKey } from './secure-storage-key.enum';
import { SecureStorageServiceAkita } from '../../../shared/state/secure-storage/secure-storage.service';
import { SecureStorageQuery } from '../../../shared/state/secure-storage/secure-storage.query';
import { persistStateReady } from '../../../../main';


const DB_NAME: string = '__JobgraderSecureStorageDB';

@Injectable({
    providedIn: 'root'
})
export class SecureStorageService implements OnDestroy {

    private static ERRORS: Array<SecureStorageError> = [
        {
            errorCode: SecureStorageErrorCode.storeCouldNotBeCreated,
            errorMessage: 'The secure storage could no be created. Falling back to insecure localStorage.'
        },
        {
            errorCode: SecureStorageErrorCode.errorWhileStoringAValue,
            errorMessage: 'An error occured while storing a value.'
        },
        {
            errorCode: SecureStorageErrorCode.accessedValueNotAvailable,
            errorMessage: 'An error occured while retieving a value. The value you tried to access is not available.'
        },
        {
            errorCode: SecureStorageErrorCode.valueCouldNotBeDeleted,
            errorMessage: 'An error occured while deleting a value.'
        }
    ];
    private static STORE_NAME = 'jobgrader';

    /**
     * The mode the store is running in
     */
    public mode: SecureStorageMode = SecureStorageMode.fallback;

    /**
     * Subject to inform the component which called
     * create that local data was secured
     */
    public localDataSecured$: Subject<any> = new Subject<any>();

    /**
     * Subject to inform the component which called
     * create that an error occured while securing local
     * data
     */
    public errorWhileSecuringLocalData$: Subject<any> = new Subject<any>();


    private destroy$ = new Subject();


    constructor(
        // private platform: Platform
        private secureStorageService: SecureStorageServiceAkita,
        private secureStorageQuery: SecureStorageQuery
    ) {
    }

    /**
     * Service method to create a store
     */
    public create(): Promise<any> {
    //    return new Promise((resolve, reject: (error: SecureStorageError) => void) => {
    //         if ( this.platform.is('hybrid')) {
    //             this.secureStorage.create(SecureStorageService.STORE_NAME)
    //                 .then((storage: SecureStorageObject) => {
    //                     this.storage = storage;
    //                     this.mode = SecureStorageMode.secure;
    //                     const localDataKeysWithoutFirebase = without(this.localDataKeys, SecureStorageKey.firebase);
    //                     if (this.localDataKeys.length > 0) {
    //                         this.secureLocalData().then(
    //                             () => localDataKeysWithoutFirebase.length && this.localDataSecured$.next(undefined),
    //                             () => this.errorWhileSecuringLocalData$.next(undefined)
    //                         );
    //                     }
    //                     this.initialized$.next(true);
    //                     resolve(void(0));
    //                 }, () => {
    //                     this.mode = SecureStorageMode.fallback;
    //                     reject(
    //                         find(
    //                             SecureStorageService.ERRORS,
    //                             {errorCode: SecureStorageErrorCode.storeCouldNotBeCreated}
    //                         )
    //                     );
    //                     this.initialized$.next(true);
    //                 });
    //         } else {
    //             this.mode = SecureStorageMode.fallback;
    //             this.initialized$.next(true);
    //             resolve(void(0));
    //         }
    //     });

        return new Promise(async (resolve, reject: (error: SecureStorageError) => void) => {

        //     this._db = await this.sqLiteService.createConnection(
        //         DB_NAME,
        //         true,
        //         'encryption',
        //         1,
        //       )

        //     await this._db.open();
        //     console.log('SecureStorageService#init; this._db', this._db);

        //     await this.makeTables();

            resolve(void(0));
        });
    }


//   makeTables() {
//     var p = [];

//     // KV table
//     p.push(this.createTable('KV', 'key TEXT primary key, value TEXT'));

//     return Promise.all(p);
//   }

    /**
     * Service method to write a key value pair
     * to the secure storage
     */
    public setValue(key: SecureStorageKey, value: string, rejectCase = true, encrypt: boolean = true, keySuffix?: string): Promise<string | SecureStorageError> {
        // return new Promise<any>((resolve: (value: any) => any, reject) => {
        //     var hashedValue = CryptoJS.AES.encrypt(value, UDIDNonce.secureStorage).toString();
        //     // console.log("hashedValue: key: " + key + ": " + hashedValue.length +  ": " + hashedValue);
        //     if ( this.platform.is('hybrid') && this.mode === SecureStorageMode.secure ) {
        //         var fullKey = (keySuffix) ? key + keySuffix : key;
        //         // this.storage.set(fullKey, value).then(
        //             this.storage.set(fullKey, (encrypt ? hashedValue : value) ).then(
        //                 (storeValue: string) => {
        //                     resolve(storeValue);
        //                 },
        //                 () => {
        //                     if (rejectCase) {
        //                         reject(
        //                             find(
        //                                 SecureStorageService.ERRORS,
        //                                 {errorCode: SecureStorageErrorCode.errorWhileStoringAValue}
        //                             )
        //                         );
        //                     } else {
        //                      resolve(undefined);
        //                     }
        //                 }
        //             );
        //     } else {
        //         var fullKey = `${SecureStorageService.STORE_NAME}.${key}`;
        //         fullKey = (keySuffix) ? fullKey + keySuffix : fullKey;
        //         // localStorage.setItem(fullKey, value);
        //         // resolve(value);
        //         localStorage.setItem(fullKey, (encrypt ? hashedValue : value));
        //         resolve((encrypt ? hashedValue : value));
        //     }

        // });

        return new Promise<any>((resolve: (value: any) => any, reject) => {
            // const fullKey = (keySuffix) ? key + keySuffix : key;

            this.secureStorageService.updateValue(key, value);

            resolve(value);

        });
    }

    /**
     * Service method to get the value for a
     * specified key
     */
    public getValue(key: SecureStorageKey, rejectCase?: false, keySuffix?: string): Promise<string>;
    public getValue(key: SecureStorageKey, rejectCase?: false): Promise<string>;
    public getValue(key: SecureStorageKey, rejectCase?: true): Promise<string | SecureStorageError>;
    public getValue(key: SecureStorageKey, rejectCase?: boolean, keySuffix?: string): Promise<string | SecureStorageError> {
        // return new Promise((resolve: (value: string) => void, reject: (error: SecureStorageError) => void) => {
        //     if ( this.platform.is('hybrid') && this.mode === SecureStorageMode.secure ) {
        //        var fullKey = (keySuffix) ? key + keySuffix : key;
        //        this.storage.get(fullKey).then((value: string) => {
        //             try {
        //                 var decryptedValue = CryptoJS.AES.decrypt(value, UDIDNonce.secureStorage).toString(CryptoJS.enc.Utf8);
        //             } catch(e) {
        //                 decryptedValue = value;
        //             }
        //             resolve(decryptedValue)
        //         }, () => {
        //            if (rejectCase) {
        //                reject(
        //                    find(
        //                        SecureStorageService.ERRORS,
        //                        {errorCode: SecureStorageErrorCode.accessedValueNotAvailable}
        //                    )
        //                );
        //            } else {
        //                resolve(undefined);
        //            }
        //        });
        //     } else {
        //         var fullKey = `${SecureStorageService.STORE_NAME}.${key}`;
        //         fullKey = (keySuffix) ? fullKey + keySuffix : fullKey;
        //         const value: string = localStorage.getItem(fullKey);
        //         if ( value ) {
        //             try {
        //                 var decryptedValue = CryptoJS.AES.decrypt(value, UDIDNonce.secureStorage).toString(CryptoJS.enc.Utf8);
        //             } catch (e) {
        //                 decryptedValue = value;
        //             }

        //             // resolve(value);
        //             resolve(decryptedValue);
        //         } else {
        //             if (rejectCase) {
        //                 reject(
        //                     find(
        //                         SecureStorageService.ERRORS,
        //                         {errorCode: SecureStorageErrorCode.accessedValueNotAvailable}
        //                     )
        //                 );
        //             } else {
        //                 resolve(undefined);
        //             }
        //         }
        //     }
        // });

        return new Promise((resolve: (value: string) => void, reject: (error: SecureStorageError) => void) => {
                // const fullKey = (keySuffix) ? key + keySuffix : key;

                persistStateReady()
                    .pipe(
                        take(1)
                    )
                    .subscribe(
                        () => {

                            this.secureStorageQuery.select((state ) => state[key])
                                .pipe(
                                    take(1)
                                )
                                .subscribe(
                                    (v) => {
                                        if (!v) {
                                            if (rejectCase) {
                                                reject(
                                                    find(
                                                        SecureStorageService.ERRORS,
                                                        {errorCode: SecureStorageErrorCode.accessedValueNotAvailable}
                                                    )
                                                );
                                            } else {
                                                resolve(undefined);
                                            }
                                        } else {

                                            resolve(v);

                                        }
                                    }
                                )
                        //     .then((v) => {
                        //         resolve(v.value)
                        //     })
                        //     .catch((error) => {
                        //         if (rejectCase) {
                        //             reject(
                        //                 find(
                        //                     SecureStorageService.ERRORS,
                        //                     {errorCode: SecureStorageErrorCode.accessedValueNotAvailable}
                        //                 )
                        //             );
                        //         } else {
                        //             resolve(undefined);
                        //         }
                        //     });
                        }
                    );
            }
        );
    }

    public getValueOfExceptionKey(): Promise<string | SecureStorageError> {
        // return new Promise((resolve: (value: string) => void, reject: (error: SecureStorageError) => void) => {
        //     if ( this.platform.is('hybrid') && this.mode === SecureStorageMode.secure ) {
        //        this.storage.get(this.exceptionKey).then((value: string) => {
        //             resolve(value)
        //         }, () => {
        //             resolve(undefined);
        //        });
        //     } else {
        //         const value: string = localStorage.getItem(this.exceptionKey);
        //         if ( value ) {
        //             resolve(value);
        //         } else {
        //             resolve(undefined);
        //         }
        //     }
        // });

        return this.getValue(SecureStorageKey.exceptionKey);

    }

    public removeValueOfExceptionKey(): Promise<string | SecureStorageError> {
        // return new Promise<any>((resolve: (value: string) => void, reject: (error: SecureStorageError) => void) => {
        //     if ( this.platform.is('hybrid') && this.mode === SecureStorageMode.secure) {
        //         this.getValueOfExceptionKey().then(value => {
        //             if (!value) {
        //                 resolve(undefined);
        //             } else {
        //                 return this.storage.remove(this.exceptionKey);
        //             }
        //         }).then((value: string) => resolve(value), () => {
        //             resolve(undefined);
        //        });
        //     } else {
        //         this.getValueOfExceptionKey().then((value: string) => {
        //             localStorage.removeItem(this.exceptionKey);
        //             if ( value ) {
        //                 resolve(value);
        //             } else {
        //                 resolve(undefined);
        //             }
        //         });
        //     }
        // });

        return new Promise<any>((resolve: (value: string) => void, reject: (error: SecureStorageError) => void) => {
            this.getValueOfExceptionKey().then(value => {
                if (!value) {
                    resolve(undefined);
                } else {
                    return this.removeValue(SecureStorageKey.exceptionKey);
                }
            // }).then((value: string) => resolve(value), () => {
            //     resolve(undefined);
            });
        });
    }

    /** Checks whether or not the user has changed and if so clean the secure-storage */
    public checkWhetherUserHasSwitched(email: string): Promise<void> {
        return new Promise(resolve => {
            from(this.getValue(SecureStorageKey.userName))
                .pipe(
                    take(1)
                ).subscribe(sEmail => {
                    if (email !== sEmail) {
                        this.cleanSecureStorage();
                    }
                    resolve();
                });
        });
    }

    /** Allows to read the values async *** Pass raw true to get uiInt8Array *** */
    public getValueAsync$(key: SecureStorageKey): Observable<string>;
    public getValueAsync$(key: SecureStorageKey.devicePublicKeyEncryption | SecureStorageKey.devicePrivateKeyEncryption, raw: boolean): Observable<Uint8Array>;
    public getValueAsync$(key: SecureStorageKey, raw = false): Observable<string | Uint8Array> {
        return from(this.getValue(key))
            .pipe(
                map(value => {
                    if (typeof value === 'string') {
                        return value;
                    // } else {
                    //     return (value as SecureStorageError).errorMessage;
                    }
                }),
                catchError((error: SecureStorageError) => {
                    return of(error.errorMessage);
                })
            );
    }

    /** Convert from hex to uint8Array */
    public hexi2(hex: string): Uint8Array {
        const bytes = new Uint8Array(Math.ceil(hex.length / 2));
        for (let i = 0; i < bytes.length; i++) {
            bytes[i] = parseInt(hex.substr(i * 2, 2), 16);
        }
        return bytes;
    }

    /** @inheritDoc */
    public ngOnDestroy() {
        this.destroy$.next(0);
    }

    /**
     * Service method to remove a
     * value from the secure storage
     */
    public removeValue(key: SecureStorageKey, rejectCase = true, keySuffix?: string): Promise<SecureStorageError | string> {
        // return new Promise<any>((resolve: (value: string) => void, reject: (error: SecureStorageError) => void) => {
        //     if ( this.platform.is('hybrid') && this.mode === SecureStorageMode.secure) {
        //         this.getValue(key, false, keySuffix).then(value => {
        //             if (!value && !rejectCase) {
        //                 resolve(undefined);
        //             } else if (!value && rejectCase) {
        //                 reject(
        //                     find(
        //                         SecureStorageService.ERRORS,
        //                         {errorCode: SecureStorageErrorCode.valueCouldNotBeDeleted}
        //                     )
        //                 );
        //             } else {
        //                 var fullKey = (keySuffix) ? key + keySuffix : key;
        //                 return this.storage.remove(fullKey);
        //             }
        //         }).then((value: string) => resolve(value), () => {
        //            if (rejectCase) {
        //                reject(
        //                    find(
        //                        SecureStorageService.ERRORS,
        //                        {errorCode: SecureStorageErrorCode.valueCouldNotBeDeleted}
        //                    )
        //                );
        //            } else {
        //             resolve(undefined);
        //            }
        //        });
        //     } else {
        //         this.getValue(key, false, keySuffix).then((value: string) => {
        //             var fullKey = `${SecureStorageService.STORE_NAME}.${key}`;
        //             fullKey = (keySuffix) ? fullKey + keySuffix : fullKey;

        //             localStorage.removeItem(fullKey);
        //             if ( value ) {
        //                 resolve(value);
        //             } else {
        //                 if (rejectCase) {
        //                     reject(
        //                         find(
        //                             SecureStorageService.ERRORS,
        //                             {errorCode: SecureStorageErrorCode.valueCouldNotBeDeleted}
        //                         )
        //                     );
        //                 } else {
        //                     resolve(undefined);
        //                 }
        //             }
        //         });
        //     }
        // });

        return new Promise<any>((resolve: (value: string) => void, reject: (error: SecureStorageError) => void) => {
            this.getValue(key, false, keySuffix).then(value => {
                if (!value && !rejectCase) {
                    resolve(undefined);
                } else if (!value && rejectCase) {
                    reject(
                        find(
                            SecureStorageService.ERRORS,
                            {errorCode: SecureStorageErrorCode.valueCouldNotBeDeleted}
                        )
                    );
                } else {
                    // var fullKey = (keySuffix) ? key + keySuffix : key;

                    this.secureStorageService.updateValue(key, null);

                    resolve(value);
                }
            });
        });
    }

    // private secureLocalData(): Promise<void> {
    //     return new Promise((resolve: () => void, reject: () => void) => {
    //         const all: Promise<string | SecureStorageError>[] = this.localDataKeys.map((key) => {
    //             const value = localStorage.getItem(`${SecureStorageService.STORE_NAME}.${key}`);
    //             return this.setValue(key, value, true);
    //         });
    //         // Only all together when no error occurs should be removed
    //         Promise.all(all).then(() => {
    //             this.localDataKeys.forEach((key) => localStorage.removeItem(`${SecureStorageService.STORE_NAME}.${key}`));
    //             resolve();
    //         }, () => reject());
    //     });
    // }

    private cleanSecureStorage(): void {
        // Object.keys(SecureStorageKey).filter( k => k !== SecureStorageKey.firebase)
        //     .forEach(k => void this.platform.is('hybrid') ?
        //         this.storage.remove(`${SecureStorageService.STORE_NAME}.${k}`) :
        //         localStorage.removeItem(`${SecureStorageService.STORE_NAME}.${k}`)
        //     );



    }


    // private get localDataKeys(): Array<SecureStorageKey> {
    //     return Object.keys(SecureStorageKey).filter(
    //         (key: string) => !!localStorage.getItem(`${SecureStorageService.STORE_NAME}.${key}`)
    //     ) as any;
    // }

}
