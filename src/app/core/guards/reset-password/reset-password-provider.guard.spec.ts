import { TestBed, inject } from '@angular/core/testing';

import { ResetPasswordProviderGuard } from './reset-password-provider.guard';

describe('ResetPasswordProviderGuard', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ResetPasswordProviderGuard],
  }));

  it('should be created', inject([ResetPasswordProviderGuard], (guard: ResetPasswordProviderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
