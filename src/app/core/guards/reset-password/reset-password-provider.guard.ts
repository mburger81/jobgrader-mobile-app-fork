import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { SignProviderService } from '../../providers/sign/sign-provider.service';
import { SignUp, PersonalInformation } from '../../../core/models/SignUp';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordProviderGuard implements CanActivate {

  constructor(
    private router: Router,
    private signService: SignProviderService
  ) { }

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const signUpDataFromStorage: SignUp = await this.signService.getSignData();
    const personalInformationDataFromStorage: PersonalInformation = await this.signService.getPersonalData();
    const url = state.url;

    return this.checkActivateAccess(url, signUpDataFromStorage, personalInformationDataFromStorage);
  }

  private checkActivateAccess(
    url: string,
    signUpData: SignUp,
    personalInformationData: PersonalInformation
  ): boolean {
   return true;
  }

}
