import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { SignProviderService } from '../../providers/sign/sign-provider.service';
import { SignUp, PersonalInformation } from '../../../core/models/SignUp';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SignProviderGuard implements CanActivate {

  constructor(
    private nav: NavController,
    private router: Router,
    private signService: SignProviderService
  ) { }

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const signUpDataFromStorage: SignUp = await this.signService.getSignData();
    const personalInformationDataFromStorage: PersonalInformation = await this.signService.getPersonalData();
    const url = state.url;

    return this.checkActivateAccess(url, signUpDataFromStorage, personalInformationDataFromStorage);
  }

  private checkActivateAccess(
    url: string,
    signUpData: SignUp,
    personalInformationData: PersonalInformation
  ): boolean {
    if (url === '/sign-up/step-2') {
      if (
        signUpData &&
        signUpData.username
        //signUpData.password &&
        //signUpData.confirm_password
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-1');
        return false;
      }
    } else if (url === '/sign-up/step-3') {
      if (
        signUpData &&
        signUpData.username &&
        signUpData.password &&
        signUpData.first_name &&
        signUpData.last_name
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-2');
        return false;
      }
    } else if (url === '/sign-up/step-4') {
      if (
        signUpData &&
        signUpData.username &&
        signUpData.password &&
        signUpData.first_name &&
        signUpData.last_name &&
        signUpData.email_id
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-3');
        return false;
      }
    } else if (url === '/sign-up/step-5') {
      if (
        signUpData &&
        signUpData.username &&
        signUpData.password &&
        signUpData.first_name &&
        signUpData.last_name &&
        signUpData.email_id &&
        signUpData.OTP
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-4');
        return false;
      }
    } else if (url === '/sign-up/step-6') {
      if (this.signService.registerSuccess) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-1');
        return false;
      }
    } else if (url === '/sign-up/step-7' && this.signService.registerSuccess) {
      if (
        personalInformationData &&
        // personalInformationData.gender &&
        personalInformationData.date_of_birth &&
        // personalInformationData.place_of_birth &&
        // personalInformationData.province_of_birth &&
        // personalInformationData.country_of_birth &&
        personalInformationData.citizenship
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-6');
        return false;
      }
    } else if (url === '/sign-up/step-8' && this.signService.registerSuccess) {
      if (
        personalInformationData &&
        // personalInformationData.gender &&
        personalInformationData.date_of_birth &&
        // personalInformationData.place_of_birth &&
        // personalInformationData.province_of_birth &&
        // personalInformationData.country_of_birth &&
        personalInformationData.citizenship &&
        personalInformationData.address_residence &&
        // personalInformationData.postal_code_residence &&
        // personalInformationData.city_residence &&
        // personalInformationData.province_residence &&
        personalInformationData.country_residence
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-7');
        return false;
      }
    } else if (url === '/sign-up/step-9' && this.signService.registerSuccess) {
      if (
        personalInformationData &&
        // personalInformationData.gender &&
        personalInformationData.date_of_birth &&
        // personalInformationData.place_of_birth &&
        // personalInformationData.province_of_birth &&
        // personalInformationData.country_of_birth &&
        personalInformationData.citizenship &&
        personalInformationData.address_residence &&
        // personalInformationData.postal_code_residence &&
        // personalInformationData.city_residence
        // personalInformationData.province_residence &&
        personalInformationData.country_residence
        // personalInformationData.document_type &&
        // personalInformationData.document_number &&
        // personalInformationData.document_issue_date &&
        // personalInformationData.document_expiry_date &&
        // personalInformationData.document_issue_country
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-8');
        return false;
      }
    } else if (url === '/sign-up/step-10' && this.signService.registerSuccess) {
      if (
        personalInformationData &&
        // personalInformationData.gender &&
        personalInformationData.date_of_birth &&
        // personalInformationData.place_of_birth &&
        // personalInformationData.province_of_birth &&
        // personalInformationData.country_of_birth &&
        personalInformationData.citizenship &&
        personalInformationData.address_residence &&
        // personalInformationData.postal_code_residence &&
        // personalInformationData.city_residence
        // personalInformationData.province_residence &&
        personalInformationData.country_residence
        // personalInformationData.document_type &&
        // personalInformationData.document_number &&
        // personalInformationData.document_issue_date &&
        // personalInformationData.document_expiry_date &&
        // personalInformationData.document_issue_country &&
        // personalInformationData.document_issue_place &&
        // personalInformationData.document_issue_agency
        // replace last live with this code
        // personalInformationData.document_issue_agency &&
        // personalInformationData.document_scanned_image
      ) {
        return true;
      } else {
        this.nav.navigateBack('/sign-up/step-9');
        return false;
      }
    } else {
      this.nav.navigateBack('/sign-up/step-1');
      return false;
    }
  }

}
