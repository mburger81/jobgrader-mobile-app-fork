import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AppStateService } from '../../providers/app-state/app-state.service';
import { NavController } from '@ionic/angular';
import { AuthenticationProviderService } from '../../providers/authentication/authentication-provider.service';
import { UserProviderService } from '../../providers/user/user-provider.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(
    private nav: NavController,
    private appStateService: AppStateService,
    private authenticationProviderService: AuthenticationProviderService,
    private userProviderService: UserProviderService
  ) { }

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    // console.log('*** authenticating on: ' + window.location.href)

    // We don't need to parse the user each time, just check we've got one in storage.
    var unParsedUser = await this.userProviderService.getUserNonParsed();

    if (unParsedUser) {
      this.appStateService.isAuthorized = true;
      
      if (!this.appStateService.basicAuthToken) {
        // Only wait for this promise to resolve if we haven't already set an app token on this app load cycle.
        var authToken = await this.authenticationProviderService.getBasicAuthToken();
        this.appStateService.basicAuthToken = authToken;
      }

      return true;
    } 
    else {
      this.nav.navigateRoot('/login');
    }
    return false;
  }

}
