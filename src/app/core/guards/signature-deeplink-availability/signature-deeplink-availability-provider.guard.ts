import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { DeeplinkProviderService } from '../../providers/deeplink/deeplink-provider.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SignatureDeeplinkAvailabilityProviderGuard implements CanActivate {

  constructor(
    private nav: NavController,
    private router: Router,
    private deeplinkProviderService: DeeplinkProviderService,
  ) {}

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    if (this.deeplinkProviderService.signatureDeeplinkStringValue) {
      return true;
    }

    this.nav.navigateRoot('/dashboard');

    return false;
  }

}
