import { TestBed, inject } from '@angular/core/testing';

import { SignatureDeeplinkAvailabilityProviderGuard } from './signature-deeplink-availability-provider.guard';

describe('SignatureDeeplinkAvailabilityProviderGuard', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [SignatureDeeplinkAvailabilityProviderGuard],
  }));

  it('should be created', inject([SignatureDeeplinkAvailabilityProviderGuard], (guard: SignatureDeeplinkAvailabilityProviderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
