import { TestBed, inject } from '@angular/core/testing';

import { VerificationDeeplinkAvailabilityProviderGuard } from './verification-deeplink-availability-provider.guard';

describe('VerificationDeeplinkAvailabilityProviderGuard', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [VerificationDeeplinkAvailabilityProviderGuard],
  }));

  it('should be created', inject([VerificationDeeplinkAvailabilityProviderGuard], (guard: VerificationDeeplinkAvailabilityProviderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
