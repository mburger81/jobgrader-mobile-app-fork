import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { UserProviderService } from '../../providers/user/user-provider.service';
import { DeeplinkProviderService } from '../../providers/deeplink/deeplink-provider.service';
import { AuthenticationProviderService } from '../../providers/authentication/authentication-provider.service';
import { LoaderProviderService } from '../../providers/loader/loader-provider.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class VerifyDeeplinkProviderResolver implements Resolve<Promise<any>> {

    constructor(
      private nav: NavController,
      private router: Router,
      private deeplinkProviderService: DeeplinkProviderService,
      private authenticationProviderService: AuthenticationProviderService,
      private userProviderService: UserProviderService,
      private loaderProviderService: LoaderProviderService
    ) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<any> {
      if (this.deeplinkProviderService.verificationDeeplinkStringValue) {
        await this.loaderProviderService.loaderCreate();

        const basicAuthToken = await this.authenticationProviderService.getBasicAuthToken();
        const user = await  this.userProviderService.getUser();
        const obj = {
              byUsrId: '',
              byLink: '',
          };

        obj.byUsrId = await this.deeplinkProviderService.verifyByUserId(
            basicAuthToken,
            user.id
          );

        obj.byLink = await this.deeplinkProviderService.verify(
              basicAuthToken,
              this.deeplinkProviderService.verificationDeeplinkStringValue
          );

        const verifiedDeeplinkPayloadResponse = obj;

        if (verifiedDeeplinkPayloadResponse) {
          await this.loaderProviderService.loaderDismiss();

          return verifiedDeeplinkPayloadResponse;
        }
      }

      await this.loaderProviderService.loaderDismiss();
      this.nav.navigateRoot('/dashboard');

      return null;
    }

}
