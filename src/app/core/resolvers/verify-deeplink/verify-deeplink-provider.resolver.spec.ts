import { TestBed, inject } from '@angular/core/testing';

import { VerifyDeeplinkProviderResolver } from './verify-deeplink-provider.resolver';

describe('VerifyDeeplinkProviderResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [VerifyDeeplinkProviderResolver],
  }));

  it('should be created', inject([VerifyDeeplinkProviderResolver], (resolver: VerifyDeeplinkProviderResolver) => {
    expect(resolver).toBeTruthy();
  }));
});
