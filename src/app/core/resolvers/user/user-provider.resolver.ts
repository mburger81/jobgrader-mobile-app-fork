import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { User } from '../../models/User';
import { UserProviderService } from '../../providers/user/user-provider.service';

@Injectable({
  providedIn: 'root'
})
export class UserProviderResolver implements Resolve<Promise<User>> {

    constructor(private userProviderService: UserProviderService) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<User> {
      const user: User = await this.userProviderService.getUser();
      if (user) {
        return user;
      }

      return {};
    }

}
