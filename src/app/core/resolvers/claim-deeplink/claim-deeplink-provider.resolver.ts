import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { DeeplinkProviderService } from '../../providers/deeplink/deeplink-provider.service';
import { AuthenticationProviderService } from '../../providers/authentication/authentication-provider.service';
import { LoaderProviderService } from '../../providers/loader/loader-provider.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ClaimDeeplinkProviderResolver implements Resolve<Promise<any>> {

    constructor(
      private nav: NavController,
      private router: Router,
      private deeplinkProviderService: DeeplinkProviderService,
      private authenticationProviderService: AuthenticationProviderService,
      private loaderProviderService: LoaderProviderService
    ) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<any> {
      if (this.deeplinkProviderService.verificationDeeplinkStringValue) {
        await this.loaderProviderService.loaderCreate();

        const basicAuthToken = await this.authenticationProviderService.getBasicAuthToken();
        const claimedDeeplinkPayloadResponse =
          await this.deeplinkProviderService.claim(
            basicAuthToken,
            this.deeplinkProviderService.verificationDeeplinkStringValue
          );

        if (claimedDeeplinkPayloadResponse) {
          await this.loaderProviderService.loaderDismiss();

          return claimedDeeplinkPayloadResponse;
        }
      }

      await this.loaderProviderService.loaderDismiss();
      this.nav.navigateRoot('/dashboard');

      return null;
    }

}
