import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { MarketplaceService } from '../../models/MarketplaceService';

@Injectable({
  providedIn: 'root'
})
export class MarketplaceServiceProviderResolver implements Resolve<Promise<MarketplaceService>> {

  private items: MarketplaceService[] = [
    {
      title: {
        en: '',
        de: ''
      },
      slug: 'stuggi-cars',
      name: {
        en: 'car',
        de: 'car'
      },
      icon: '../../../../assets/icon/marketplace/stuggi-cars-hq.png',
    }
  ];

  constructor() { }

  async resolve(route: ActivatedRouteSnapshot): Promise<MarketplaceService> {
    const { slug } = route.params;
    const item: MarketplaceService = this.items.find(i => i.slug === slug);
    if (item) {
      return item;
    }

    return {};
  }

}
