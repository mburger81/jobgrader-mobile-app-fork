import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpInterceptorService } from './interceptors/http.interceptors';
import { AppStateService } from './providers/app-state/app-state.service';
import { TranslateProviderService } from './providers/translate/translate-provider.service';
import { StorageProviderService } from './providers/storage/storage-provider.service';
import { TabDirective } from '../shared/directives/tab-index.directive';
import { BarcodeService } from './providers/barcode/barcode.service';
import { CryptoProviderService } from './providers/crypto/crypto-provider.service';
// import { CustomErrorHandler } from './providers/error/custom-error-handler.service';
import { NetworkService } from './providers/network/network-service';
// import { ChatService } from './providers/chat/chat.service';
import { SQLStorageService } from './providers/sql/sqlStorage.service';
import { EventsService } from './providers/events/events.service';


@NgModule({
  imports: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    AppStateService,
    TranslateProviderService,
    StorageProviderService,
    BarcodeService,
    CryptoProviderService,
    NetworkService,
    // ChatService,
    SQLStorageService,
    EventsService
  ]
})
export class CoreModule { }
