export interface Feedback {
  subject?: string;
  message?: string;
}
