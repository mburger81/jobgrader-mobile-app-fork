interface ResetPassword {
  reset_username?: string;
  reset_OTP?: string;
  reset_password?: string;
  reset_confirm_password?: string;
}

export {
  ResetPassword
};
