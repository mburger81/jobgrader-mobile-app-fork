export interface KycProcess {
  kycProcessStatus?: KycProcessStatus;
  kycProvider?: KycProvider;
  kycInitTimestamp?: number; // (datetime)
  kycSessionToken?: string;
  kycProcessTimer?: number; // (datetime)
  kycNextCheckTimer?: number; // (datetime)
}

export enum KycProcessStatus {
  KYC_NEVER_DONE = 0,
  KYC_INITIATED = 1, //  (Variable “kycProcessTimer” set to now + 72 hours)
  MULTIPLE_KYC_INITIATED = 2, // need to be defined that only one KYC process until success or cancellation;
  RETURN_FROM_KYC_PROVIDER = 4, // KYC process coming back from KYC Provider Web Service
  KYC_TIME_OUT = 8, // KYC process times-out
  KYC_SUCCESSFUL = 16, // KYC successful
  KYC_NEXT_CHECK_TIMER = 32, // KYC timer “kycNextCheckTimer” set for next KYC check
  KYC_NOT_VALID = 64, // KYC not valid // Technical problem with the KYC Provider Payload
  KYC_WITHDRAWN = 128 // KYC withdrawn
}

export enum KycProvider {
  VERIFF_PROD_IDCARD = 1,  // Veriff Prod ID Card
  VERIFF_PROD_DRIVINGLICENCE = 2, // Veriff Prod Driving License
  VERIFF_PROD_PASSPORT = 9,  // Veriff Prod ID Card
  VERIFF_PROD_RESIDENCEPERMIT = 10, // Veriff Prod Driving License
  VERIFF_TEST_IDCARD = 3, // Veriff Test ID Card
  VERIFF_TEST_DRIVINGLICENCE = 4, // Veriff Test Driving License
  VERIFF_TEST_PASSPORT = 11, // Veriff Test ID Card
  VERIFF_TEST_RESIDENCEPERMIT = 12, // Veriff Test Driving License
  AUTHADA_PROD = 5, // Authada Prod
  AUTHADA_TEST = 6, // Authada Test
  UK_GOV = 7, // UK Gov
  MOBILE_CONNECT = 8, // Mobile Connect
  VERIFEYE_TEST_IDCARD = 9, // Verifeye Test ID Card
  VERIFEYE_TEST_DRIVINGLICENCE = 10, // Verifeye Test Driving License
  VERIFEYE_TEST_PASSPORT = 11, // Verifeye Test Passport
  VERIFEYE_PROD_IDCARD = 12, // Verifeye Prod ID Card
  VERIFEYE_PROD_DRIVINGLICENCE = 13, // Verifeye Prod Driving License
  VERIFEYE_PROD_PASSPORT = 14, // Verifeye Prod Passport
  VERIFEYE_TEST_RESIDENCEPERMIT = 15, // Verifeye Test Passport
  VERIFEYE_PROD_RESIDENCEPERMIT = 16, // Verifeye Test Passport
  VERIFEYE_TEST = 17,
  VERIFEYE_PROD = 18,
  ONDATO_TEST = 19,
  ONDATO_PROD = 20,
  ONDATO_TEST_IDCARD = 21,
  ONDATO_TEST_DRIVINGLICENCE = 22,
  ONDATO_TEST_PASSPORT = 23,
  ONDATO_TEST_RESIDENCEPERMIT = 24,
  ONDATO_PROD_IDCARD = 25,
  ONDATO_PROD_DRIVINGLICENCE = 26,
  ONDATO_PROD_PASSPORT = 27,
  ONDATO_PROD_RESIDENCEPERMIT = 28
}

export enum KycProcessStatusCodeFromMW {
  VERIFF_POSITIVE = 9001, // 9001	Positive: Person was verified. The verification process is complete. Accessing the sessionURL again will show the client that nothing is to be done here.
  VERIFF_NEGATIVE = 9102, // 9102	Negative: Person has not been verified. The verification process is complete. Either it was a fraud case or some other severe reason that the person can not be verified. You should investigate the session further and read the "reason". If you decide to give the client another try you need to create a new session.
  VERIFF_RESUBMIT = 9103, // 9103	Resubmitted: Resubmission has been requested. The verification process is not completed. Something was missing from the client and she or he needs to go through the flow once more. The same sessionURL can and should be used for this purpose.
  VERIFF_EXPIRED = 9104, // 9104	Negative: Verification has been expired. The verification process is complete. After 7 days the session get's expired. If the client started the verification process we reply "abandoned" here, otherwise if the client never arrived in our environment the status will be "expired".
  VERIFF_INTERMEDIATE = 9151, // 9151	Intermediate Positive: SelfID was successful - this code is only send if the configuration flag is set.
}

export enum KycProcessErrorCodeFromMW {
  AUTHADA_SESSION_TOKEN_INVALID = 10001, // 10001	Invalid sessionToken
  AUTHADA_PRODUCT_INVALID = 10002, // 10002	Invalid product
  AUTHADA_PROFILE_INVALID = 10003, // 10003	Invalid profile
  AUTHADA_SESSION_INCOMPLETE = 10004, // 10004	Session is not completed
  AUTHADA_RESULT_TOKEN_INVALID = 10005, // 10005	Invalid resultToken
  AUTHADA_RESULT_TOKEN_MISSING = 10006, // 10006	ResultToken is required for this session
  AUTHADA_RESULT_TOKEN_EXPIRED = 10007, // 10007	ResultToken expired: no tries left
  AUTHADA_CONFIGURATION_ERROR = 10008, // 10008	Configuration error
  AUTHADA_CONFUGURATION_UNAVAILABLE_FEATURE_PROVIDED = 30001, // 30001	Configuration for unavailable feature provided

  VERIFF_PHYSICAL_DOCUMENT_NOT_USED = 101, // 101	Physical document not used
  VERIFF_DOCUMENT_TAMPERING_SUSPECTED = 102, // 102	Suspected document tampering
  VERIFF_PERSON_PHOTO_MISMATCH = 103, // 103	Person showing the document does not appear to match document photo
  VERIFF_SUSPICIOUS_BEHAVIOUR = 105, // 105	Suspicious behaviour
  VERIFF_KNOWN_FRAUD = 106, // 106	Known fraud
  VERIFF_VELOCITY_ABUSE = 107, // 107	Velocity/abuse
  VERIFF_VIDEO_PHOTO_MISSING = 201, // 201	Video and/or photos missing
  VERIFF_FACE_VISIBILITY_UNCLEAR = 202, // 202	Face not clearly visible
  VERIFF_DOCUMENT_VISIBILITY_INCOMPLETE = 203, // 203	Full document not visible
  VERIFF_IMAGE_POOR_QUALITY = 204, // 204	Poor image quality
  VERIFF_DOCUMENT_DAMAGED = 205, // 205	Document damaged
  VERIFF_DOCUMENT_UNSUPPORTED = 206, // 206	Document type not supported
  VERIFF_DOCUMENT_EXPIRED = 207, // 207	Document expired.
}
