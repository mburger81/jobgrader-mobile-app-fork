interface TrustContainer {
    status: number;
    trustDate: Date;
    trustType: number;
    trustedBy: any; // ??
    trustedById: number;
    value: any;
}

export interface UserData {
    confirmed: boolean;
    data: {
        citizenship: TrustContainer;
        city: TrustContainer;
        cityofbirth: TrustContainer;
        country: TrustContainer;
        countryofbirth: TrustContainer;
        dateofbirth: TrustContainer;
        driverlicencecountry: TrustContainer;
        driverlicencedocumentnumber: TrustContainer;
        driverlicenceexpirydate: TrustContainer;
        driverlicenceissuedate: TrustContainer;
        email: TrustContainer;
        emailtype: TrustContainer;
        firstname: TrustContainer;
        gender: TrustContainer;
        id: string;
        identificationdocumentnumber: TrustContainer;
        identificationdocumenttype: TrustContainer;
        identificationexpirydate: TrustContainer;
        identificationissuecountry: TrustContainer;
        identificationissuedate: TrustContainer;
        lastname: TrustContainer;
        maidenname: TrustContainer;
        maritalstatus: TrustContainer;
        middlename: TrustContainer;
        parent: any; // ???
        phone: TrustContainer;
        phonetype: TrustContainer;
        revision: TrustContainer;
        state: TrustContainer;
        street: TrustContainer;
        taxnumber: TrustContainer;
        taxresidency: TrustContainer;
        termsofuse: TrustContainer;
        termsofusethirdparties: TrustContainer;
        title: TrustContainer;
        tpHash: string;
        type: any; // ???
        zip: any;
    };
    id: string;
    identityid: string;
    revision: string;
    type: any; // ???
    userid: string;
}

