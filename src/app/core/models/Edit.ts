export interface Edit {
  firstname?: string;
  // middlename?: string;
  lastname?: string;
  title?: string; // new
  maritalstatus?: string; // new
  gender?: string;
  address?: string;
  zipcode?: string;
  city?: string; // new
  dateofbirth?: any;
  cityofbirth?: string;
  state?: string;
  countryofbirth?: string;
  citizenship?: string;
  residencecountry?: string;
  email?: string;
  phone?: string;
  idcardnumber?: string;
  idcardissuecountry?: string;
  idcardexpirydate?: any;
  idcardissuedate?: any;
  dlnumber?: string;
  dlissuecountry?: string;
  dlexpirydate?: any;
  dlissuedate?: any;
  passportnumber?: string;
  passportissuecountry?: string;
  passportexpirydate?: any;
  passportissuedate?: any;
  residencepermitnumber?: string;
  residencepermitissuecountry?: string;
  residencepermitexpirydate?: any;
  residencepermitissuedate?: any;
}
