interface SignUp {
  username?: string;
  password?: string;
  confirm_password?: string;
  title?: string;
  first_name?: string;
  middle_name?: string;
  last_name?: string;
  email_id?: string;
  OTP?: string;
}

interface PersonalInformation {
  gender?: string;
  maritalstatus?: string;
  date_of_birth?: any;
  place_of_birth?: string;
  province_of_birth?: string;
  country_of_birth?: string;
  citizenship?: string;
  address_residence?: string;
  street_number_residence?: string;
  postal_code_residence?: string;
  city_residence?: string;
  province_residence?: string;
  country_residence?: string;
  
  passport_number?: string;
  passport_issue_date?: any;
  passport_expiry_date?: any; 
  passport_issue_country?: string;
  
  dl_number?: string;
  dl_issue_date?: any;
  dl_expiry_date?: any; 
  dl_issue_country?: string;

  idcard_number?: string;
  idcard_issue_date?: any;
  idcard_expiry_date?: any; 
  idcard_issue_country?: string;

  residence_permit_number?: string;
  residence_permit_issue_date?: any;
  residence_permit_expiry_date?: any; 
  residence_permit_issue_country?: string;
}

export {
  SignUp,
  PersonalInformation
};
