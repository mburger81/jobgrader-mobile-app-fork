export interface idCore {
  idCore?: {
    did?: any;
    vc?: any;
    basicdata?: any;
    chatPublicKey?: string;
    chatPrivateKey?: string;
    chatPassword?: string;
    symmetricKey?: string;
  };
  approvedPhoneNumber?: string;
  deviceInfo?: any;
  contacts?: [ {
    username?: string;
    chatPublicKey?: string;
    firstname?: string;
    lastname?: string;
    displayPicture?: any;
  } ];
  lastActions?: [ {
    timestamp?: any;
    string?: string;
    category?: string;
  } ];
}
