export interface MarketplaceService {
  title?: {
    en?: string;
    de?: string;
  };
  slug?: string;
  name?: {
    en?: string;
    de?: string;
  };
  icon?: string;
}
