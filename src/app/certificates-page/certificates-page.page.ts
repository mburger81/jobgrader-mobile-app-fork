import { Component } from '@angular/core';
import { AlertController, NavController, ModalController, ToastController } from '@ionic/angular';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { SmartAgentURI } from 'src/app/core/models/api-smart-agent.enum';
import { EventsService, EventsList } from 'src/app/core/providers/events/events.service';
import { environment } from 'src/environments/environment';
import { BarcodeService } from 'src/app/core/providers/barcode/barcode.service';
import { NetworkService } from '../core/providers/network/network-service';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { KycService } from '../kyc/services/kyc.service';
import { CertificatesComponent } from '../chat/certificates/certificates.component';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

interface TnTAsset {
  md5?: string;
  mimeType?: string;
  name?: string;
  size?: number;
  url?: string;
}

interface TntRequest {
    prover?: string;
    verifier?: string;
    schemaDid?: string;
    credentialValues?: {
      approved?: string;
      timestamp?: string;
      step?: string;
      nda?: string;
      medicalDocument?: string;
    };
    revealedAttributes?: Array<string>;
    credentialSubjectRaw?: {
       logo?: TnTAsset;
       country?: string;
       postalCode?: string;
       city?: string;
       address?: string;
       companyName?: string;
       region?: string;
       // new definition for covid use case:
       idNumber?: string; // T01000322
       testIdentification?: string; // 3CF75K8D0L
       testIssuer?: string; // did:evan:helix892743iughf8
       dateOfTest?: string; // 202008261430
       expirationDate?: string; // 202009261429
       testMethod?: string; // PCR
       testResult?: string; //  (e.g. p = positive, n = negative, u = unknown):
    };
    attachments?: Array<{
      uri?: string;
      tag?: string;
      mimeType?: string;
    }>;
    imageBlob?: string;
    documentBlob?: string;
    threadId?: string;
    dummy?: boolean;
    signatureHash?: string;
    vc?: any;
    vp?: any;
}

@Component({
  selector: 'app-certificates-page',
  templateUrl: './certificates-page.page.html',
  styleUrls: ['./certificates-page.page.scss'],
})
export class CertificatesPagePage {
  public tntUseCaseList: Array<TntRequest> = [];
  public certificateMediaList = {}
  public profilePictureSrc = '../../assets/job-user.svg';
  public emptyCertificateProfile = '../../assets/job-user.svg';
  public moreMode: boolean = false;
  public displayVerifiedTick = false;
  public displayMoreButton: boolean = false;
  public carUseCaseList = [
    // {
    //     qrCode:"U2FsdGVkX1+0xFfeR5KlCL/J1alxAmkbdcVPnPetJUr/xKH/3YXuhMbD8nx8CjkvsiB8OZpAnE7yIwVlx08Ghm43vUx5PuXhhQYjRtrl7JPUGvu9XFK0lRu6+8omEXSo7dpoT+Khiw71aN9PJBw1cY1TGQbjcrM5W9sZBksX793uVw8fc6GkaDPmpduE7l+snn/3+zpVZGt0+LYTQoq1par9ugfggt/XSawi08NW3FzNB5vBzS5tLjQAbjJiSY2iOy3LF3K8PWm6zofgELakWTfV4Q+/LB3Ys8cpd84blrGIW/jWogtjmVWMAbMBTpPlulXfHmTdhvr2GkJQWr0CICv+TJYPloYASSxtl9zfx8XeygPE6aGDeBUV8P6ESYcoWmwG8SGzbJ52nNq7pYJfcWqH4xnt8JoFqjIHOSLIjewZnxfANb7rOpioeldxoRFAIvhNUuV3Hyw2ShqMbBPXvk9H49RP+OUfTtFuV817phDq1tZA3/yC9qeZUn8usA5PXcM2lE5MOYekMTWlNUh3KHMLYHsuWOFaA3iMs4oUEuEpfZuyZBHwmvw8hjMJ39ncAmu3ITzu64tYlTczQ0MWd77G9m20YK9b97LUtxGcdppcugtofdXLM1iYQAJ+MUT8+oLz/O5p2wJLuPJnPwDzrSZ8VsfmjSOaX2LJ/RwKyIsCQhJRA33EvKTjHmW/JZQ5LRCnz+MPs4UI4NB40DmYHopuDVbubCSinSeDihzDeyf21kqYA2t61d6LsMVXoeI52ejPS4lMo4Eb4Z/s8g/ZDe4dk/TMuRQIEF08pBhst3tihLQmX3fJ6Jdnyky7t986dYLcVv/MRJyHiIz/LeQyDvnG/Vik5ALHet6g9/B4/APhDIuNa10oRYQ42YzmIzITJimDRAax6n9Adhc2iqta6vibqCE7sAYk9rIHMKQQ7vTsrahahU87ub8HmlsLJVJ2xM5KPqtvWQZsup0l2QltCORxAL7DpMgqrI9FgYx7PE51g34NUmRe1308VOuxkIrSgeUtuMwqSNK8YncMDsqyZIHCIBG8f/mbwCT1nsicRLYl6PdEppEsP+H5klf+FAyY+rz3Blw9ZJXeN43SnrrgfGLgTGGC0Z5B5UWiu/HKZAuR9aJpXuM9pQdKcikjCJOpSptcTva0fs9fbyeCHthDaHzjfHV5JfbBgzUGa8SKBbbLFXJ2a+zryqsdPW8VBxYMfNcq+06myOwLUrW9awSkF8l1ztdD5FDfG/Q3c6xfvGsgRSOCIsmOYiw3HCv5h9AWi746Zch0NDtk7VOYUAkdSq1r4YydW/vx6DqmiO4sOMjMUPwk/j4N7mGxn4iEtIrIJTdvJ1lyw4DGalYLpe8iKw==",
    //     prover:"did:evan:testcore:0xD9A64Af32bED799c021c5d3FcF002d1316c302b0",
    //     verifier:"did:evan:testcore:0x6Ed6d9B1e6d0b913827BAC3ab24100F25b50e11F",
    //     schemaDid:"did:evan:zkp:0x52186c31c13e4f45f0b52b34ab24ed65c422ee9fe709e97d1e26a20bea2ba6d4",
    //     didDocument: {
    //         "@context": "https://w3id.org/did/v1",
    //         "id": "did:evan:0xc8DCD8F01E8E0FDE164853214779270cD8b7e17D",
    //         "publicKey": [
    //             {
    //                 "id": "did:evan:0x9ff919c320867ecafaf8fc30f22a296a5bd306d4#key-1",
    //                 "type": "Secp256k1VerificationKey2018",
    //                 "controller": "did:evan:0x9fF919C320867ecaFAF8fC30f22A296a5bd306d4",
    //                 "ethereumAddress": "0x2a80b6ffffc3c54d987c3c35700f2caff41757d4"
    //             }
    //         ],
    //         "authentication": [
    //             "did:evan:0x9ff919c320867ecafaf8fc30f22a296a5bd306d4#key-1"
    //         ]
    //     },
    //     credentialValues:{
    //       "approved":"true",
    //       "timestamp":"1610467186537",
    //       "car":"[]"
    //     },
    //     credentialSubjectRaw:{
    //       "logo": "https://drive.google.com/uc?export=view&id=1T4QLS5hTBhH0vokmDJDa4K0dlVnuNyOT",
    //       "manufacturer": "BMW",
    //       "model": "iX3",
    //       "creation_date": "02-03-2022",
    //       "expiration_date": "02-04-2022",
    //       "vin": "5UXFE83507LZ40758",
    //       "insurance": "Allianz",
    //       "insurance_id": "ALL6127/HDOI",
    //       "registration_plate": "BD51 SMR",
    //       "public_key": "0xdcc703c0E500B653Ca82273B7BFAd8045D85a470",
    //       "qr_code_type": "",
    //     }
    //   }
  ];


  constructor(
    private nav: NavController,
    private secureStorage: SecureStorageService,
    private modalController: ModalController,
    private translateProviderService: TranslateProviderService,
    private apiProviderService: ApiProviderService,
    private loader: LoaderProviderService,
    private _ToastController: ToastController,
    private _EventsService: EventsService,
    private alertController: AlertController,
    public _BarcodeService: BarcodeService,
    private _NetworkService: NetworkService,
    private _KycService: KycService,
    private _ImageSelectionService: ImageSelectionService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
   }

  public async ionViewWillEnter() {

    if(!this._NetworkService.checkConnection()) {
        this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    this.displayMoreButton = !environment.production;

    // await this.loader.loaderCreate()
    this._EventsService.publish(EventsList.hideProfileTabBadge);
    var tntUseCase = await this.secureStorage.getValue(SecureStorageKey.certificates, false);
    var cars = await this.secureStorage.getValue(SecureStorageKey.carIdentities, false);
    await this._BarcodeService.setCertificates();
    this.carUseCaseList = !!cars ? JSON.parse(cars) : this.carUseCaseList;

    for(let i=0; i<this.carUseCaseList.length; i++) {
      
      if(!(this.carUseCaseList[i] as any).didDocument){
        var carDid = await this.apiProviderService.convertGenericDid(this.carUseCaseList[i].credentialSubjectRaw.vin);

        if(!carDid || carDid == "") {
            carDid = await this.apiProviderService.generateGenericDid(this.carUseCaseList[i].credentialSubjectRaw.vin);
        }

        this.carUseCaseList[i]['didDocument'] = carDid;
        this.carUseCaseList[i].credentialSubjectRaw['did'] = carDid.id;
        this.carUseCaseList[i].credentialSubjectRaw['public_key'] = carDid.publicKey[0].ethereumAddress;

        await this.secureStorage.setValue(SecureStorageKey.carIdentities, JSON.stringify(this.carUseCaseList));

      }

      if(!(this.carUseCaseList[i] as any).vc){
        var vc = await this.apiProviderService.generateGenericVc(this.carUseCaseList[i].credentialSubjectRaw);
        this.carUseCaseList[i]['vc'] = vc;
        this.carUseCaseList[i].credentialSubjectRaw['vcId'] = vc.id;

        await this.secureStorage.setValue(SecureStorageKey.carIdentities, JSON.stringify(this.carUseCaseList));
        
      }
      
    }
    
    if( tntUseCase ) {
      this.tntUseCaseList = JSON.parse(tntUseCase)
      this.tntUseCaseList.forEach(k => Object.assign(k, { dummy: false }))
    }
    else {
//       this.tntUseCaseList.push({
//           prover:"did:evan:testcore:0x86032e3b196360a1DE6A230c0a46972dE1e3cB7a",
//           verifier:"did:evan:testcore:0xf9d6778ea5c5EbD6C47bceB7921049AfB7f9683a",
//           schemaDid:"did:evan:zkp:0x470cd915c86bf89f80c92c1b9215a90c3d5e31f78f5760c8b8c433b9418b24ac",
//           credentialValues:{
//             "approved":"true",
//             "timestamp":"1610467186537",
//             "nda":"{\"approved\":\"\",\"timestamp\":\"\",\"nda\":\"[\\n  {\\n    \\\"md5\\\": \\\"485e3d118a45211fb65f8ae896950ee7\\\",\\n    \\\"mimeType\\\": \\\"application/pdf\\\",\\n    \\\"name\\\": \\\"TNTTESTASSET.PDF\\\",\\n    \\\"size\\\": 47205,\\n    \\\"url\\\": \\\"https://tntservices-c43a.azurewebsites.net/file/af72d5d9-b30a-452d-97a8-abe3ea891875?token=841206e306fe115e7d3789837c3deb81e4e1bc6ce05a1574\\\"\\n  }\\n]\"}"
//           },
//           revealedAttributes:[
//             "approved",
//             "timestamp",
//             "nda"
//           ],
//           credentialSubjectRaw:{
//             "postalCode":"60399",
//             "country":"de",
//             "region":"",
//             "city":"Frankfurt am Main",
//             "address":"Gref-Völsing-Strasse 88",
//             "companyName":"Demo 1",
//             "logo": {}
//           },
//           attachments:[
//             {
//                 "uri": environment.helixEnv == "test" ? "https://tntservices-c43a.azurewebsites.net/file/6ced64a5-5849-4a25-a616-9f39e64288e1?token=01d050ca907d5234b62a6db18e6ac726d877fd8bcc0ba626" : "https://tntservices-e9d2.azurewebsites.net/file/febf1870-e8f6-4034-9d6f-0bd19f5d552d?token=f1344991cc107354b657cc42676c9a5b6b50bb13bcdc56d0",
//                 "mimeType": "application/pdf",
//                 "tag":"nda"
//             }
//           ],
//           threadId: "1a93a7ad-cb1e-496f-bfb0-04f3976a40e9",
//           signatureHash: '0x828134ddd3dcd4fe5aef7dd659f5cdede33f482c392ed866d6baef063925299c3dd51b16322757656ddfc878a0d78c7fdcc6acf4471c6365a694afffe421de361b'
//       },{
//           prover:"did:evan:testcore:0x86032e3b196360a1DE6A230c0a46972dE1e3cB7a",
//           verifier:"did:evan:testcore:0xf9d6778ea5c5EbD6C47bceB7921049AfB7f9683a",
//           schemaDid:"did:evan:zkp:0x470cd915c86bf89f80c92c1b9215a90c3d5e31f78f5760c8b8c433b9418b24ac",
//           credentialValues:{
//             "approved":"true",
//             "timestamp":"1610467186537",
//             "nda":"{\"approved\":\"\",\"timestamp\":\"\",\"nda\":\"[\\n  {\\n    \\\"md5\\\": \\\"485e3d118a45211fb65f8ae896950ee7\\\",\\n    \\\"mimeType\\\": \\\"application/pdf\\\",\\n    \\\"name\\\": \\\"TNTTESTASSET.PDF\\\",\\n    \\\"size\\\": 47205,\\n    \\\"url\\\": \\\"https://tntservices-c43a.azurewebsites.net/file/af72d5d9-b30a-452d-97a8-abe3ea891875?token=841206e306fe115e7d3789837c3deb81e4e1bc6ce05a1574\\\"\\n  }\\n]\"}"
//           },
//           revealedAttributes:[
//             "approved",
//             "timestamp",
//             "nda"
//           ],
//           credentialSubjectRaw:{
//             "postalCode":"60399",
//             "country":"de",
//             "region":"",
//             "city":"Frankfurt am Main",
//             "address":"Gref-Völsing-Strasse 88",
//             "companyName":"Demo 2",
//             "logo": environment.helixEnv == "test" ? {
//               "md5":"9d4098f304860a9e458a26ed41718455",
//               "mimeType":"image/png",
//               "name":"yin-yang.jpg",
//               "size":90452,
//               "url":"https://tntservices-c43a.azurewebsites.net/file/7a6ddc64-2259-4a47-b595-af4e6f702c08?token=dc60a285021a66c7d381a06c21953fb0b61fe9863232e207"
//             } : null
//           },
//           attachments:[
//             {
//                 "uri": environment.helixEnv == "test" ? "https://tntservices-c43a.azurewebsites.net/file/6ced64a5-5849-4a25-a616-9f39e64288e1?token=01d050ca907d5234b62a6db18e6ac726d877fd8bcc0ba626" : "https://tntservices-e9d2.azurewebsites.net/file/febf1870-e8f6-4034-9d6f-0bd19f5d552d?token=f1344991cc107354b657cc42676c9a5b6b50bb13bcdc56d0",
//                 "mimeType": "application/pdf",
//                 "tag":"nda"
//             }
//           ],
//           threadId: "1a93a7ad-cb1e-496f-bfb0-04f3976a40e9",
//           signatureHash: '0x828134ddd3dcd4fe5aef7dd659f5cdede33f482c392ed866d6baef063925299c3dd51b16322757656ddfc878a0d78c7fdcc6acf4471c6365a694afffe421de361b',
//           vc: {
//             "@context": "https://www.w3.org/2018/credentials/v1",
//             "id": "vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
//             "type": [
//               "VerifiableCredential",
//               "KYC"
//             ],
//             "issuer": {
//               "id": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b"
//             },
//             "credentialSubject": {
//               "id": "did:evan:testcore:0x1ee6bfe7034fdbc5ac5d57fb171b3820297d7711",
//               "data": [
//                 {
//                   "trustedBy": "Authada",
//                   "trustLevel": 1,
//                   "trustValue": 1,
//                   "identificationdocumentnumber": "C5NVPCZ5E",
//                   "identificationissuecountry": "germany",
//                   "identificationissuedate": "2015-02-28T23:00:00Z",
//                   "identificationexpirydate": "2025-02-28T23:00:00Z",
//                   "driverlicencedocumentnumber": "A17181920",
//                   "driverlicencecountry": "germany",
//                   "driverlicenceissuedate": "2020-01-31T23:00:00Z",
//                 }
//               ]
//             },
//             "validFrom": "1970-01-19T09:08:03Z",
//             "validTo": "1970-01-20T02:39:15Z",
//             "credentialStatus": {
//               "id": "https://testcore.evan.network/smart-agents/smart-agent-did-resolver/vc/status/vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
//               "type": "evan:evanCredential"
//             },
//             "proof": {
//               "type": "EcdsaPublicKeySecp256k1",
//               "created": "2020-04-28T14:14:34.023Z",
//               "jws": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NkstUiJ9.eyJpYXQiOjE1ODgwODMyNzMsInZjIjp7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L2NyZWRlbnRpYWxzL3YxIl0sInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJLWUMiXSwiaWQiOiJ2YzpldmFuOnRlc3Rjb3JlOjB4MTM5ZDk2ZTEyN2U1YjI1OGJmZTY2ZTQ2MTZhNGFjMmEyNzFjMGU4NjQ0MmJiMGIxMTljYWFkODVlN2EzYmU1YiIsImlzc3VlciI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9LCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4MWVlNmJmZTcwMzRmZGJjNWFjNWQ1N2ZiMTcxYjM4MjAyOTdkNzcxMSIsImRhdGEiOnsidHJ1c3RlZEJ5IjoiQXV0aGFkYSIsInRydXN0TGV2ZWwiOjEsInRydXN0VmFsdWUiOjEsImlkIjpudWxsLCJ1c2VyIjpudWxsLCJsYXN0TW9kaWZlZCI6bnVsbCwiZ2VuZGVyIjoiZGl2ZXJzIiwidGl0bGUiOiJQcm9mLiBEci4iLCJjYWxsbmFtZSI6InRoZSByb2NrIiwiZmlyc3RuYW1lIjoiRXJpa2EiLCJtaWRkbGVuYW1lIjoiTWFyaWEiLCJsYXN0bmFtZSI6Ik11c3Rlcm1hbiIsIm1hcml0YWxzdGF0dXMiOiJ3aWRvd2VkIiwibWFpZGVubmFtZSI6Ik11c3RlcmZyYXUiLCJlbWFpbHR5cGUiOiJ0ZXh0LW9ubHkiLCJlbWFpbCI6ImVyaWthLm11c3Rlcm1hbkBibG9ja2NoYWluLWhlbGl4LmNvbSIsInBob25ldHlwZSI6Im1vYmxpZSIsInBob25lIjoiMDE3NTY3NDUyMyIsImRhdGVvZmJpcnRoIjoiMTkyMC0wNi0yMlQyMzowMDowMFoiLCJjaXR5b2ZiaXJ0aCI6IlN0b2NraG9sbSIsImNvdW50cnlvZmJpcnRoIjoiU2Nod2VkZW4iLCJjaXRpemVuc2hpcCI6Imdlcm1hbiIsInN0cmVldCI6IkxpbmRlbnN0cmFzZSA0MiIsImNpdHkiOiJnZXJtYW4iLCJ6aXAiOiIxMDk2OSIsInN0YXRlIjoiQmVybGluIiwiY291bnRyeSI6IkRldXRzY2hsYW5kIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudHR5cGUiOiJpZC1jYXJkIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudG51bWJlciI6IkM1TlZQQ1o1RSIsImlkZW50aWZpY2F0aW9uaXNzdWVjb3VudHJ5IjoiZ2VybWFueSIsImlkZW50aWZpY2F0aW9uaXNzdWVkYXRlIjoiMjAxNS0wMi0yOFQyMzowMDowMFoiLCJpZGVudGlmaWNhdGlvbmV4cGlyeWRhdGUiOiIyMDI1LTAyLTI4VDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2Vkb2N1bWVudG51bWJlciI6IkExNzE4MTkyMCIsImRyaXZlcmxpY2VuY2Vjb3VudHJ5IjoiZ2VybWFueSIsImRyaXZlcmxpY2VuY2Vpc3N1ZWRhdGUiOiIyMDIwLTAxLTMxVDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2VleHBpcnlkYXRlIjoiMjAzMC0wMS0zMVQyMzowMDowMFoiLCJ0YXhyZXNpZGVuY3kiOiJnZXJtYW55IiwidGF4bnVtYmVyIjoiWDQzN04yMzg2NyIsInRlcm1zb2Z1c2UiOiJUZXJtc29mdXNlIiwidGVybXNvZnVzZXRoaXJkcGFydGllcyI6IlRlcm1zb2Z1c2V0aGlyZHBhcnRpZXMifX0sInZhbGlkRnJvbSI6IjE5NzAtMDEtMTlUMDk6MDg6MDNaIiwidmFsaWRUbyI6IjE5NzAtMDEtMjBUMDI6Mzk6MTVaIiwiY3JlZGVudGlhbFN0YXR1cyI6eyJpZCI6Imh0dHBzOi8vdGVzdGNvcmUuZXZhbi5uZXR3b3JrL3NtYXJ0LWFnZW50cy9zbWFydC1hZ2VudC1kaWQtcmVzb2x2ZXIvdmMvc3RhdHVzL3ZjOmV2YW46dGVzdGNvcmU6MHgxMzlkOTZlMTI3ZTViMjU4YmZlNjZlNDYxNmE0YWMyYTI3MWMwZTg2NDQyYmIwYjExOWNhYWQ4NWU3YTNiZTViIiwidHlwZSI6ImV2YW46ZXZhbkNyZWRlbnRpYWwifX0sImlzcyI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9.ABk8RBlpCAX-gY47_aRxskCCrl_AsluEDbYNEY8BNlY2NS78aoIWXDNNhoJe0J5EpwnWQyR02I9qMn_lqL0qFwE",
//               "proofPurpose": "assertionMethod",
//               "verificationMethod": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b#key-1"
//             }
//           },
//           vp: {
//             "uuid":"182bdbf0-54e9-4a82-821f-ec095f867b0c",
//             "principalUuid":"f4172822-401d-441c-9917-51e34715bb77",
//             "assetRefId":"856f7fe1-8ba8-4d0d-8e34-6342e6f05935",
//             "type":"PRESENTATION",
//             "status":"DRAFT",
//             "value":"{\"eventId\":\"ec99948e-b86c-4f21-8d04-7ab87d66c985\",\"verifiableCredential\":[{\"eventId\":\"50628359-515e-45c9-97c8-63c2e8b0dba4\",\"context\":[\"https://www.w3.org/2018/credentials/v1\"],\"credentialSchema\":{\"id\":\"did:evan:zkp:0x447af36504bac062a22615387e3ae1685239ff4f8404f9878b86e801b56e2369\",\"type\":\"cert.master-data.address\"},\"credential\":{\"credentialSubject\":{\"data\":{\"approved\":{\"raw\":false,\"encoded\":\"\"},\"timestamp\":{\"raw\":1606912631817,\"encoded\":\"\"},\"step\":{\"raw\":\"ceoSign\",\"encoded\":\"\"}},\"id\":\"did:evan:testcore:0x60dc09221B0184853a106A480e90aCBABA0107bA\"}},\"issuer\":\"9c85607c-ef31-4a5b-9ad6-155f2f25d930\",\"credentialSubject\":{\"id\":\"7d50f4e3-5f37-4f92-a0bf-6df31a89bcf3\"}}]}",
//             "subject":"7d50f4e3-5f37-4f92-a0bf-6df31a89bcf3",
//             "issuer":"9c85607c-ef31-4a5b-9ad6-155f2f25d930",
//             "asset":[
//               {
//                   "uuid":"9c4e573e-2a4b-403d-b353-ad7859241be6",
//                   "principalUuid":"f4172822-401d-441c-9917-51e34715bb77",
//                   "issuer":"9c85607c-ef31-4a5b-9ad6-155f2f25d930",
//                   "referenceUri":"9c85607c-ef31-4a5b-9ad6-155f2f25d930/ec99948e-b86c-4f21-8d04-7ab87d66c985",
//                   "type":"DIDCOMM_MESSAGE",
//                   "status":"ACTIVE",
//                   "size":825,
//                   "mimeType":"application/json",
//                   "action":{
//                     "uuid":"23906da9-cbec-4c63-b402-6dcebdd04ae3"
//                   },
//                   "event":{
//                     "uuid":"d0e85f45-b3b4-4d3d-b902-60aa0aaee72d"
//                   },
//                   "createdBy":null,
//                   "updatedBy":null,
//                   "name":null,
//                   "tags":null,
//                   "createdAt":"2020-12-02T12:37:17.574Z",
//                   "updatedAt":"2020-12-02T12:37:17.574Z"
//               }
//             ],
//             "verifier":"7d50f4e3-5f37-4f92-a0bf-6df31a89bcf3",
//             "createdBy":null,
//             "updatedBy":null,
//             "issueDate":null,
//             "expirationDate":null,
//             "createdAt":"2020-12-02T12:37:17.652Z",
//             "updatedAt":"2020-12-02T12:37:17.652Z"
//           },
//           dummy: true
//           },{
//             prover:"did:evan:testcore:0xD9A64Af32bED799c021c5d3FcF002d1316c302b0",
//             verifier:"did:evan:testcore:0x6Ed6d9B1e6d0b913827BAC3ab24100F25b50e11F",
//             schemaDid:"did:evan:zkp:0x52186c31c13e4f45f0b52b34ab24ed65c422ee9fe709e97d1e26a20bea2ba6d4",
//             credentialValues:{
//               "approved":"true",
//               "timestamp":"1610467186537",
//               "medicalDocument":"[\"2d842ed4-7493-49bc-9dc2-431614e5cc91\"]"
//             },
//             revealedAttributes:[
//               "approved",
//               "timestamp",
//               "medicalDocument"
//             ],
//             credentialSubjectRaw:{
//               "companyName": "Bioscientia Labor Mittelhessen",
//               "idNumber": "T01000322", // T01000322
//               "testIdentification": "3CF75K8D0L", // 3CF75K8D0L
//               "testIssuer": "did:evan:helix892743iughf8", // did:evan:helix892743iughf8
//               "dateOfTest": "202008261430", // 202008261430
//               "expirationDate": "202009261429", // 202009261429
//               "testMethod": "PCR", // PCR
//               "testResult": "n", //  (e.g. p = positive, n = negative, u = unknown):
//               "logo": environment.helixEnv == "test" ? {
//                 "md5":"9d4098f304860a9e458a26ed41718455",
//                 "mimeType":"image/png",
//                 "name":"yin-yang.jpg",
//                 "size":90452,
//                 "url":"https://tntservices-c43a.azurewebsites.net/file/010fc87d-676a-4c2e-a1e5-477b1ecfd161?token=0e3f74ce4b83183413c73c294097f708353cc4172034869c"
//               } : null
//             },
//             attachments:[
//               {
//                   "uri": environment.helixEnv == "test" ? "/api/v1/tnt/file/b0352ee8-e9b3-4455-8a7c-709ff7e9c960?token=451a3279b6ac3fed3b2d0ab33e342db20b190a06eeb829ec" : "/api/v1/tnt/file/cd5d4ad2-5664-4be7-bb70-0bc56dc93eb2?token=35b933fb88a26f953044eda69e561c675231f3142c604c14",
//                   "mimeType": "application/pdf",
//                   "tag":"medicalDocument"
//               }
//             ],
//             threadId: "2a93a7ad-cb1e-496f-bfb0-04f3976a40e9"
//           }
// )
    }

    var certificateMedia = await this.secureStorage.getValue(SecureStorageKey.certificatesMedia, false);

    if( !certificateMedia ) {
    //   this.certificateMediaList = certificateMedia ? JSON.parse(certificateMedia) : {}
    //   this.tntUseCaseList.forEach(k => {
    //     if(!!k.attachments) {
    //       k.attachments.forEach(l => {
    //         if(l.uri.includes("https://drive.google.com/")) {
    //           k.documentBlob = l.uri
    //         } else {
    //           k.documentBlob = this.certificateMediaList[this.returnFileIdFromAssetURL(l.uri)];
    //         }
    //       })
    //     }
    //   })
    // } else {
      this.tntUseCaseList.forEach(k => {
        if(!!k.attachments) {
          k.attachments.forEach(l => {
            if(!l.uri.includes("https://drive.google.com")){
              var fileId = this.returnFileIdFromAssetURL(l.uri);
              var token = this.returnTokenFromAssetURL(l.uri);
              var mimeType = !!k.credentialValues.nda ? JSON.parse(k.credentialValues.nda).mimeType : (!!k.credentialValues.medicalDocument ? JSON.parse(k.credentialValues.medicalDocument).mimeType : 'application/pdf');
              console.log("fileId: " + fileId)
              console.log("token: " + token)
              console.log("mimeType: " + mimeType)
              if(!mimeType) {
                mimeType = 'application/pdf';
              }
              if(fileId && token && mimeType) {
                this.apiProviderService.getAssetFromFileIdToken(fileId, token, mimeType).then(asset => {
                  k.documentBlob = asset
                  console.log("k.documentBlob")
                  console.log(k.documentBlob)
                }).catch(e => {
                  console.error(e);
                  // this.loader.loaderDismiss();
                  this.presentToast(this.translateProviderService.instant('CERTIFICATES.errorFetchingAsset'));
                })
                console.log(k.documentBlob);
              }
            } else {
              k.documentBlob = l.uri
            }
          })
        }
        if(!!k.credentialSubjectRaw.logo){
          var fileId = !!k.credentialSubjectRaw.logo.url ? this.returnFileIdFromAssetURL(k.credentialSubjectRaw.logo.url) : null;
          var token = !!k.credentialSubjectRaw.logo.url ? this.returnTokenFromAssetURL(k.credentialSubjectRaw.logo.url) : null;
          var mimeType = k.credentialSubjectRaw.logo.mimeType ?? 'image/png';
          // var assetId = this.returnAssetIdFromPartialURI(k.credentialSubjectRaw.logo)
          // console.log(assetId)
          if(fileId && token && mimeType) {
            this.apiProviderService.getAssetFromFileIdToken(fileId, token, mimeType).then((base64) => {
              k.imageBlob = base64;
            }).catch(async e => {
              console.error(e);
              // this.loader.loaderDismiss();
              this.presentToast(this.translateProviderService.instant('CERTIFICATES.errorFetchingAsset'));
            })
          }
        }
      })
    }

    var certMedia = await this.secureStorage.getValue(SecureStorageKey.certificatesMedia, false)
    this.certificateMediaList = !!certMedia ? JSON.parse(certMedia) : {};
    this.tntUseCaseList.forEach(k => {
      k.imageBlob = !!k.credentialSubjectRaw.logo ? (!!k.credentialSubjectRaw.logo.url ? this.certificateMediaList[this.returnFileIdFromAssetURL(k.credentialSubjectRaw.logo.url)] : this.emptyCertificateProfile) : this.emptyCertificateProfile;
      k.documentBlob = !!k.attachments ?  this.certificateMediaList[this.returnFileIdFromAssetURL(k.attachments[0].uri)] : null;
    });
    console.log(this.tntUseCaseList);
    console.log(this.certificateMediaList);

    // this.profilePictureSrc = '../../assets/Image-placeholder.svg';
    const photo = this.userPhotoServiceAkita.getPhoto();
    if (photo) { this.profilePictureSrc = photo };

    this._KycService.isUserAllowedToUseChatMarketplace().then(displayVerifiedTick => {
      this.displayVerifiedTick = displayVerifiedTick;
    })

    // this.loader.loaderDismiss();
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 2000, position: 'top' });
    await toast.present();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  goBack(){
   this.nav.navigateBack('/dashboard/personal-details');
  }

  returnAssetIdFromPartialURI(partialUri) {
    return new URL(SmartAgentURI.uri + partialUri).searchParams.get('assetId');
  }

  returnFileIdFromAssetURL(uri: string) {
    if(uri) {
      try {
        var u = new URL(uri);
      } catch (e) {
        var u = new URL(SmartAgentURI.uri + uri);
      }
      return uri.replace(u.origin, "").replace(u.search, "").replace("/file/","").replace("/api/v1/tnt", "");
    } else {
      return null;
    }
  }

  returnTokenFromAssetURL(uri: string) {
    if(uri) {
      try {
        var u = new URL(uri).searchParams.get('token');
      } catch (e) {
        var u = new URL(SmartAgentURI.uri + uri).searchParams.get('token');
      }
      return u;
    } else {
      return null;
    }
  }

  public toggleMore() {
      this.moreMode = !this.moreMode;
  }

  async onPress($event, element: any) {
    const alert = await this.alertController.create({
      mode: 'ios',
      header: '',
      message: this.translateProviderService.instant('CERTIFICATES.deleteOneCertificate'),
      buttons: [{
          text: this.translateProviderService.instant('SETTINGS.yes'),
          cssClass: 'primary',
          handler: async () => {
            this.tntUseCaseList = this.tntUseCaseList.filter(k => k != element)
          }
        },
        {
          text: this.translateProviderService.instant('SETTINGS.no'),
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    await alert.present();
}

public async clear() {
  const alert = await this.alertController.create({
    mode: 'ios',
    header: '',
    message: this.translateProviderService.instant('CERTIFICATES.secureStorageAlert'),
    buttons: [{
        text: this.translateProviderService.instant('SETTINGS.yes'),
        cssClass: 'primary',
        handler: async () => {
          await this.secureStorage.removeValue(SecureStorageKey.certificates, false)
          await this.secureStorage.removeValue(SecureStorageKey.certificatesMedia, false)
          this.goBack()
        }
      },
      {
        text: this.translateProviderService.instant('SETTINGS.no'),
        role: 'cancel',
        handler: () => {}
      }
    ]
  });
  await alert.present();
}

public showSecureStorage() {
  this.secureStorage.getValue(SecureStorageKey.certificates, false).then(secsto => {
    if(secsto) {
      alert(secsto)
    }
    else {
      alert(this.translateProviderService.instant('CERTIFICATES.showSecureStorageEmpty'))
    }
  })
}

public showSecureStorageMedia() {
  this.secureStorage.getValue(SecureStorageKey.certificatesMedia, false).then(secsto => {
    if(secsto) {
      var cleanStuf = JSON.parse(secsto)
      Object.keys(cleanStuf).forEach(k => cleanStuf[k] = cleanStuf[k].substring(0, 30))
      secsto = JSON.stringify(cleanStuf)
      alert(secsto)
    }
    else {
      alert(this.translateProviderService.instant('CERTIFICATES.showSecureMediaEmpty'))
    }
  })
}

public async showModal(input: any) {
  const modal = await this.modalController.create({
    component: CertificatesComponent,
    componentProps: {
      data: {
        input: input,
        tntUseCaseList: this.tntUseCaseList
      }
    }
  })
  modal.onDidDismiss().then(async () => {

  })
  await modal.present();
}

public async showCar(car: any) {
  const modal = await this.modalController.create({
    component: CertificatesComponent,
    componentProps: {
      data: {
        input: car,
        carUseCaseList: this.carUseCaseList
      }
    }
  })
  modal.onDidDismiss().then(async () => {
    var cars = await this.secureStorage.getValue(SecureStorageKey.carIdentities, false);
    this.carUseCaseList = !!cars ? JSON.parse(cars) : [];
  })
  await modal.present();
}

async displayPictureOptions() {
  console.log('Should work!')
  this._ImageSelectionService.showChangePicture().then((photo) => {
    this.profilePictureSrc = !!photo ? photo : '../../assets/Image-placeholder.svg';
  });
}

addCertificate() {
  this._BarcodeService.shortcutsPageModification().then(async () => {
    var cars = await this.secureStorage.getValue(SecureStorageKey.carIdentities, false);
    await this._BarcodeService.setCertificates();
    this.carUseCaseList = !!cars ? JSON.parse(cars) : this.carUseCaseList;
  })
}



}
