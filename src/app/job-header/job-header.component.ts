import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { EventsList, EventsService } from '../core/providers/events/events.service';
import { BroadcastService } from '../core/providers/broadcast/broadcast.service';

@Component({
  selector: 'app-job-header',
  templateUrl: './job-header.component.html',
  styleUrls: ['./job-header.component.scss'],
})
export class JobHeaderComponent implements OnInit {
  @Input("showBackButton") showBackButton: boolean;
  @Input("userImage") userImage: string; // '../../assets/header/promo.svg';
  // @Input() backEvent: () => any;
  @Output("backEvent") backEvent: EventEmitter<any> = new EventEmitter();

  constructor(
    private _ImageSelectionService: ImageSelectionService,
    private _AppStateService: AppStateService,
    private _ApiProviderService: ApiProviderService,
    private _EventsService: EventsService,
    public _Broadcast: BroadcastService
  ) { }

  ngOnInit() {}

  onClickEvent() {
    this.backEvent.emit();
  }

  changePic() {
    if (this._AppStateService.isAuthorized) {
      this._ImageSelectionService.showChangePicture().then((photo) => {
        this.userImage = !!photo ? photo : '../../assets/job-user.svg';
        this._EventsService.publish(EventsList.changeDp, photo);
      });
    } else {
      this._ApiProviderService.noUserLoggedInAlert();
    }
  }

}
