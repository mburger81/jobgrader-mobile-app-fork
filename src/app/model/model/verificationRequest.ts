/**
 * HELIX ID
 * A sample test API for HELIX ID App
 *
 * OpenAPI spec version: 1.0.11
 * Contact: lothar@blockchain-helix.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface VerificationRequest {
    /**
     * Public Key generated by the third party App
     */
    publicKey: string;
    /**
     * Unique ID of the third party App
     */
    sourceId: string;
    /**
     * Name of the user
     */
    name: string;
    /**
     * Phone number of the user
     */
    phoneNumber: string;
    /**
     * Minimum Age condition
     */
    ageMin: number;
    /**
     * Maximum Age condition
     */
    ageMax: number;
    /**
     * Issuer country whitelist ISO 3166-1 alpha-2
     */
    driverLicence: string;
    /**
     * Residence country whitelist ISO 3166-1 alpha-2
     */
    residenceCountry: string;
}
