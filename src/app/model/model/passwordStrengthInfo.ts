export interface PasswordStrengthInfo {
    password0Type?: string;
    password1Type: string;
    password1Strength?: number;
    password1StrengthText?: string;
    password2Type?: string;
    password2Strength?: number;
    password2StrengthText?: string;
    okButtonEnabled?: boolean;
    okButtonClass?: string;
}
