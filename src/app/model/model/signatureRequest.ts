/**
 * HELIX ID
 * A sample test API for HELIX ID App
 *
 * OpenAPI spec version: 1.0.11
 * Contact: lothar@blockchain-helix.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface SignatureRequest {
    /**
     * Public Key generated by the third party App
     */
    publicKey: string;
    /**
     * Unique ID of the third party App
     */
    sourceId: string;
    /**
     * JSON object providing the contractual details which needs to be signed.
     */
    signatureContract: any;
}
