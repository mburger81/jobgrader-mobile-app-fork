/**
 * HELIX ID
 * A sample test API for HELIX ID App
 *
 * OpenAPI spec version: 1.0.11
 * Contact: lothar@blockchain-helix.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface ClaimLog {
    /**
     * UserId of User
     */
    userIds?: string;
    /**
     * couchdb document type,  claimlog
     */
    type?: string;
    /**
     * Public Key generated by the third party App
     */
    publicKey?: string;
    /**
     * Unique ID of the third party App
     */
    sourceId?: string;
    /**
     * Name value send during claim request
     */
    name?: string;
    /**
     * Phonenumber value send during claim request
     */
    phoneNumber?: string;
    /**
     * DriverLicence value send during claim request
     */
    driverLicence?: string;
    /**
     * ResidenceCountry value send during claim request
     */
    residenceCountry?: string;
    /**
     * AgeMin value send during claim request
     */
    ageMin?: string;
    /**
     * AgeMax value send during claim request
     */
    ageMax?: string;
    /**
     * HELIX Account ID (ETH) address which is required to access the claim and verification service
     */
    accountId?: string;
    /**
     * Returned status for Claim Request
     */
    status?: string;
    /**
     * Returned verificationPath from Evan
     */
    verificationPath?: string;
    /**
     * Generated PseudonymID for claim
     */
    pseudonymID?: string;
    /**
     * expirationDate of claim
     */
    expirationDate?: number;
}
