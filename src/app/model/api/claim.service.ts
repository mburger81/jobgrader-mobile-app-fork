/**
 * HELIX ID
 * A sample test API for HELIX ID App
 *
 * OpenAPI spec version: 1.0.11
 * Contact: lothar@blockchain-helix.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';


import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class ClaimService {

    protected basePath = 'https://virtserver.swaggerhub.com/Blockchain-HELIX/helixid/1.0.0';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     *
     * User click Apply / ready to create claim
     * @param conditions The HELIX mobile app user has confirmed to send the confirmation back via the deeplink to the thrid party mobile app and requires the claim details. This is the event which will kick start the claim creating for this business process   &#x60;&#x60;&#x60;  {   &#39;processID&#39;: &#39;xhdsgfjvkshe536747&#39; // unique business process identifier   &#39;pubTp&#39;: &#39;0x298012jehdjwq7mnakds&#39;, //public key comming from the third party app   &#39;verificationRequest&#39;: true, //confirms please create the claim   }   &#x60;&#x60;&#x60;
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiV1ClaimPost(conditions?: any, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiV1ClaimPost(conditions?: any, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiV1ClaimPost(conditions?: any, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiV1ClaimPost(conditions?: any, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let headers = this.defaultHeaders;

        // authentication (basicAuth) required
        if (this.configuration.username || this.configuration.password) {
            headers = headers.set('Authorization', 'Basic ' + btoa(this.configuration.username + ':' + this.configuration.password));
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.basePath}/api/v1/claim`,
            conditions,
            {
                withCredentials: this.configuration.withCredentials,
                headers,
                observe,
                reportProgress
            }
        );
    }

    /**
     *
     * User click Apply / ready to create claim
     * @param pseudonymId Generated unique pseudonymId, returned by claim post call
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiV1ClaimPseudonymIdGet(pseudonymId: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiV1ClaimPseudonymIdGet(pseudonymId: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiV1ClaimPseudonymIdGet(pseudonymId: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiV1ClaimPseudonymIdGet(pseudonymId: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (pseudonymId === null || pseudonymId === undefined) {
            throw new Error('Required parameter pseudonymId was null or undefined when calling apiV1ClaimPseudonymIdGet.');
        }

        let headers = this.defaultHeaders;

        // authentication (basicAuth) required
        if (this.configuration.username || this.configuration.password) {
            headers = headers.set('Authorization', 'Basic ' + btoa(this.configuration.username + ':' + this.configuration.password));
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<any>(`${this.basePath}/api/v1/claim/${encodeURIComponent(String(pseudonymId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers,
                observe,
                reportProgress
            }
        );
    }

    /**
     *
     * Gets all Claimlog entries of current user
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public apiV1ClaimlogGet(observe?: 'body', reportProgress?: boolean): Observable<any>;
    public apiV1ClaimlogGet(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public apiV1ClaimlogGet(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public apiV1ClaimlogGet(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (basicAuth) required
        if (this.configuration.username || this.configuration.password) {
            headers = headers.set('Authorization', 'Basic ' + btoa(this.configuration.username + ':' + this.configuration.password));
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<any>(`${this.basePath}/api/v1/claimlog`,
            {
                withCredentials: this.configuration.withCredentials,
                headers,
                observe,
                reportProgress
            }
        );
    }

}
