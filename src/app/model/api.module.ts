import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { ClaimService } from './api/claim.service';
import { LastmodifiedService } from './api/lastmodified.service';
import { UserService } from './api/user.service';
import { VeriffService } from './api/veriff.service';
import { VerificationService } from './api/verification.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    ClaimService,
    LastmodifiedService,
    UserService,
    VeriffService,
    VerificationService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders<ApiModule> {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
