import { Component, ElementRef, OnDestroy, OnInit, ViewChild, NgZone } from '@angular/core';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ToastController, NavController, IonContent, Platform } from '@ionic/angular';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { UtilityService } from '../core/providers/utillity/utility.service';
import { Keyboard, KeyboardInfo } from '@capacitor/keyboard';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { User } from '../core/models/User';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { SignProviderService } from '../core/providers/sign/sign-provider.service';
import { SettingsService } from '../core/providers/settings/settings.service';
import { NetworkService } from '../core/providers/network/network-service';
import { PasswordStrengthInfo } from '../model/model/passwordStrengthInfo';
import { KycService } from '../kyc/services/kyc.service';
import { KeyRecoveryBackupService } from '../core/providers/key-recovery-backup/key-recovery-backup.service';
import { DeviceService } from '../core/providers/device/device.service';
import { EventsList, EventsService } from '../core/providers/events/events.service';
import { environment } from 'src/environments/environment';
import { SecurePinService } from '../core/providers/secure-pin/secure-pin.service';
import { CodeInputComponent } from 'angular-code-input';
import { UDIDNonce } from '../core/providers/device/udid.enum';
import * as CryptoJS from 'crypto-js';
import { MarketplaceService } from '../core/providers/marketplace/marketplace.service';
import { SQLStorageService } from '../core/providers/sql/sqlStorage.service';
import { BiometricService } from '../core/providers/biometric/biometric.service';
import { take } from 'rxjs/operators';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { Capacitor } from '@capacitor/core';
import { Device, DeviceId } from '@capacitor/device';
import { VaultSecretsServiceAkita } from '../core/providers/state/vault-secrets/vault-secrets.service';
import { SecureStorageQuery } from '../shared/state/secure-storage/secure-storage.query';
import { KycMediaServiceAkita } from '../core/providers/state/kyc-media/kyc-media.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';


declare var window: any;


enum LoginScreenFieldNames {
  password = 'password',
  email = 'email'
}

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {
  @ViewChild(IonContent, {}) content: IonContent;
  @ViewChild('codeInput') codeInput: CodeInputComponent;

  public fieldNames: typeof LoginScreenFieldNames = LoginScreenFieldNames;
  public keyboardOpen = false;
  public loginModePIN = false;
  public encryptedPIN = false;
  public pinNotVisible = true;
  loginForm: UntypedFormGroup;
  authProcessing = false;
  notValidForm = false;
  userCallname: string;
  notUniqueUsername: boolean;
  public fingerprintIsAvailable = false;
  public fingerPrintFailureCount = 0;
  private additionalStyleSheetId: string;
  private nonce: string;
  public loginHash: string;
  private loginMode: boolean = false;

  displayLoadingDots = false;

  public passwordStrengthInfo: PasswordStrengthInfo = {
    password1Type: "password"
  }

  /** @inheritDoc */
  ngOnInit() {
    const tagName = this.elemRef.nativeElement.tagName.toLowerCase();
    const styles: string =
        this.utilityService.calculateWhiteCircleCssParams(`${tagName} .page-content:after`, 70);
    UtilityService.addStyleToHead(styles, this.additionalStyleSheetId = `${tagName}CircleStyle`);
  }

  constructor(
    private _NetworkService: NetworkService,
    private nav: NavController,
    private router: Router,
    private userProviderService: UserProviderService,
    private toastController: ToastController,
    private translateProviderService: TranslateProviderService,
    private authenticationProviderService: AuthenticationProviderService,
    private utilityService: UtilityService,
    private _SQLStorageService: SQLStorageService,
    private elemRef: ElementRef,
    private device: DeviceService,
    private loader: LoaderProviderService,
    private appStateService: AppStateService,
    private secureStorageService: SecureStorageService,
    private crypto: CryptoProviderService,
    private signService: SignProviderService,
    private alertController: AlertController,
    private apiProviderService: ApiProviderService,
    private settingsService: SettingsService,
    private kycService: KycService,
    private keyRecoveryBackupService: KeyRecoveryBackupService,
    private _SecurePinService: SecurePinService,
    public _EventsService: EventsService,
    private _MarketplaceService: MarketplaceService,
    private biometricService: BiometricService,
    private platform: Platform,
    private themeSwitcherService: ThemeSwitcherService,
    private vaultSecretsServiceAkita: VaultSecretsServiceAkita,
    private secureStorageQuery: SecureStorageQuery,
    private kycMediaServiceAkita: KycMediaServiceAkita,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
    this.appStateService.isAuthorized = false;
    this.loginForm = new UntypedFormGroup({
      email: new UntypedFormControl('', [
        Validators.required,
        // Validators.pattern('^[A-Za-z0-9!?@#$%^&*)(+=._-]+$')
      ]),
      password: new UntypedFormControl('', [
        Validators.required,
        // Validators.pattern('^[A-Za-z0-9!?@#$%^&*)(+=._-]+$')
      ])
    });
    if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.addListener('keyboardDidShow', (info: KeyboardInfo) => this.keyboardOpen = true);
    if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.addListener('keyboardDidHide', () => this.keyboardOpen = false);
  }

  async ionViewWillEnter() {
    this.content.scrollToTop();
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    UtilityService.removeStyleFromHead(this.additionalStyleSheetId);
  }

  public ionViewDidEnter() {
    this.fingerPrintFailureCount = 0;
    this.appStateService.isAuthorized = false;
    if(environment.production) {
      this.secureStorageService.setValue(SecureStorageKey.toggleChatEncryption, true.toString());
    }

    this.secureStorageService.getValue(SecureStorageKey.language, false).then((language) => {
      console.log("From login screen: " + language);
      if(language) {
        this.translateProviderService.changeLanguage(language);
      }
    })

    this.secureStorageService.getValue(SecureStorageKey.themeMode, false).then((themeMode) => {
      console.log("From login screen: " + themeMode);
      if(themeMode) {
        this.themeSwitcherService.setTheme(themeMode);
      }
    })

    this.secureStorageService.getValue(SecureStorageKey.onboardingHasRun, false).then(v => {
      console.log('login page ===> status', v)
      if (!v) {
        this.secureStorageService.getValue(SecureStorageKey.loginCheck, false)
            .then(loginHash => {
          this.loginHash = loginHash;
          if(loginHash) {
            this.secureStorageService.getValue(SecureStorageKey.securePINEncrypted, false).then((pin) => {
              if(pin) {
                this.encryptedPIN = true;
                this.loginModePIN = true;
              }
            }).catch(e => console.log(e))
          }
        });
        // console.log("if, loginCheck: " + this.loginHash);
        // console.log("onboardingHasRun value: " + v);
        let checker = this.router.parseUrl(this.router.url).queryParams;
        // console.log(checker);
        if(!(['registration','resetpassword', 'forgot-username', 'introvideo', 'browsemode'].includes(checker.from))){
          this.nav.navigateRoot('onboarding');
        }
      }
      else {
        this.biometricService.biometricAvailable$().pipe(take(1))
            .subscribe(isAvailable => {
              this.fingerprintIsAvailable = isAvailable;
              if (isAvailable) {
                this.useBiometric();
              }
            });

        this.authProcessing = false;
        this.markFormAsValid();

        this.secureStorageService.getValue(SecureStorageKey.userData).then((userDataString) => {
          const userData: User = userDataString ? JSON.parse(userDataString) : {};
          this.userCallname = userData ? userData.callname : undefined;
        });
        this.secureStorageService.getValue(SecureStorageKey.nonce, false).then(nonce => {
          this.nonce = nonce;
        });
        this.secureStorageService.getValue(SecureStorageKey.loginCheck, false)
            .then(loginHash => {
          this.loginHash = loginHash;
          console.log("else, loginCheck: " + this.loginHash);
          if(loginHash) {
            this.secureStorageService.getValue(SecureStorageKey.securePINEncrypted, false).then((pin) => {
              if(pin) {
                this.encryptedPIN = true;
                this.loginModePIN = true;
                // this.loginUsingBiometric();
              }
            }).catch(e => console.log(e))
          }
        });
      }
    }).then(() => {
      this._EventsService.publish(EventsList.hideSplash);
    });
  }

  // /** Callback for showing the biometric */
  public loginUsingBiometric(): void {
    this.authenticationProviderService.logout();
    if (this.platform.is('android')) {
        document.addEventListener('backbutton', (event) => {
            event.preventDefault();
            return;
        }, false);
    }
    this.biometricService.biometricAvailable$().pipe(take(1))
        .subscribe(isAvailable => {
            this.fingerprintIsAvailable = isAvailable;
            if (isAvailable) {
                this.useBiometric();
            }
        });
  }
  public useBiometric(): void {
    this.biometricService.biometricShow()
        .pipe(take(1))
        .subscribe((basicAuthToken) => {

          this.loginOverPinOrBiometric();

          // // Successfully passed the biometric
          // this.authenticationProviderService.login(basicAuthToken, false).then(success => {
          //   this.fingerprintIsAvailable = !success && this.fingerPrintFailureCount < 3;
          //   this.fingerPrintFailureCount++;
          //   // if(this._NetworkService.checkConnection()){
          //   //   this._ChatService.connect(true);
          //   // }
          //   this.keyRecoveryBackupService.recoveryAlert(true).then(() => {
          //     this._MarketplaceService.init().then(() => {
          //       this._SQLStorageService.getMarketPlaceMerchants().then(() => {
          //         console.log('Navigation done!');
          //         this.loader.loaderDismiss();
          //       })
          //     })
          //   }).catch(e => {
          //     console.log(e);
          //     this.codeInput.reset();
          //     this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));
          //   })
          // }).catch(e => {
          //   console.log(e);
          //   this.codeInput.reset();
          //   this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));
          // });
        }, _ => {
          // Error while authorize via biometric
          this.fingerPrintFailureCount++;
        });
  }

  public ionViewDidLeave() {
    this.clearLoginForm();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  async submitLogin(email: string, password: string): Promise<boolean> {

    console.log("Login Page: submitLogin");

    if (!this._NetworkService.checkConnection()) {
      // await this.loader.loaderCreate();
      this.displayLoadingDots = true;
      this._NetworkService.addOfflineFooter('ion-footer');
      var passwordHash = await this.secureStorageService.getValue(SecureStorageKey.passwordHash, false);
      if(!passwordHash) {
        await this.presentToast(this.translateProviderService.instant('NETWORK.checkConnection'));
        // await this.loader.loaderDismiss();
        this.displayLoadingDots = false;
        return;
      }
      var checkPassword = this.crypto.generateHash(password, this.nonce);
      var checkUsername = this.crypto.generateHash(email, this.nonce);
      if(checkPassword == passwordHash) {
        if(this.loginHash !== checkUsername) {
          await this.presentToast(this.translateProviderService.instant('LOGIN.wrongPassword'));
          // await this.loader.loaderDismiss();
          this.displayLoadingDots = false;
          return;
        }
        if (!this.nonce && !this.loginHash) {
          await this.signService.brandDeviceToUser(email, password);
        }
        this.appStateService.isAuthorized = true;
        this.appStateService.basicAuthToken = await this.crypto.generateBasicAuthToken(email, password);
        await this.secureStorageService.setValue(SecureStorageKey.userName, email);
        // await this.loader.loaderDismiss();
        this.displayLoadingDots = false;
        await this.keyRecoveryBackupService.recoveryAlert(true);
        return;
      } else {
        await this.presentToast(this.translateProviderService.instant('LOGIN.wrongPassword'));
        // await this.loader.loaderDismiss();
        this.displayLoadingDots = false;
        return;
      }
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }
    console.log("Login Page: await this.device.obtainUDID();");
    await this.device.obtainUDID();

    if (email == "" || password == ""){
      this.presentToast(this.translateProviderService.instant('LOGIN.missingInfo'));
      return false;
    }
    this.loginForm.markAsDirty();
    console.log("Login Page: await this.apiProviderService.checkUserName();");
    this.notUniqueUsername = await this.apiProviderService.checkUserName(email).then(res => res).catch(err => {
      console.log(err);
      this.presentToast(this.translateProviderService.instant('LOGIN.serverError'));
      return false;
    });

    console.log("Login Page: this.notUniqueUsername", this.notUniqueUsername);

    if (!this.notUniqueUsername) {
      this.presentToast(this.translateProviderService.instant('LOGIN.userdoesnotexist'));
      const alert = await this.alertController.create({
        mode: 'ios',
        header: '',
        message: this.translateProviderService.instant('LOGIN.createnewuser'),
        buttons: [{
            text: this.translateProviderService.instant('SETTINGS.yes'),
            cssClass: 'primary',
            handler: () => {
              this.nav.navigateForward('/sign-up')
            }
          },
          {
            text: this.translateProviderService.instant('SETTINGS.no'),
            role: 'cancel',
            handler: () => {}
          }
        ]
      });
      await alert.present();
      return false;
    }

    const loginHash = this.crypto.generateHash(email, this.nonce);
    console.log("Login Page: loginHash", loginHash);
    this.loginMode = !!this.nonce && !!this.loginHash;
    console.log("Login Page: this.loginMode", this.loginMode);
    if (this.nonce && this.loginHash &&  loginHash !== this.loginHash) {
      const alert = await this.alertController.create({
        mode: 'ios',
        header: '',
        message: this.translateProviderService.instant('LOGIN.deviceRegisteredToOtherUser'),
        buttons: [{
            text: this.translateProviderService.instant('GENERAL.ok'),
            cssClass: 'primary',
            handler: () => {}
          }
        ]
      });
      await alert.present();
      return false;
    }
    if(!this.loginMode) {
      console.log("Login Page: Begin removing secure storage entries");
      await this.secureStorageService.removeValue(SecureStorageKey.devicePublicKeyEncryption, false);
      await this.secureStorageService.removeValue(SecureStorageKey.devicePrivateKeyEncryption, false);
      await this.secureStorageService.removeValue(SecureStorageKey.mnemonic, false); // User Device Keys
      await this.secureStorageService.removeValue(SecureStorageKey.userKeyEncryptedBackup, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userKeyEncryptedUser, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userKeyEncryptedVC, false);
      try {
        this.userPhotoServiceAkita.clear(); // User Personal Info complete
      } catch(e) {
        console.log(e);
      }
      await this.secureStorageService.removeValue(SecureStorageKey.userName, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userData, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userTrustData, false);
      await this.secureStorageService.removeValue(SecureStorageKey.verifiableCredentialEncrypted, false);
      await this.secureStorageService.removeValue(SecureStorageKey.didDocument, false);
      await this.secureStorageService.removeValue(SecureStorageKey.vcId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.did, false);
      await this.secureStorageService.removeValue(SecureStorageKey.kycStatus, false);
      await this.secureStorageService.removeValue(SecureStorageKey.kycStatusTimestamp, false);
      try{
        this.kycMediaServiceAkita.clear();
      } catch(e) {
        console.log(e);
      }
      await this.secureStorageService.removeValue(SecureStorageKey.kycMediaTimestamp, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, false);
      await this.secureStorageService.removeValue(SecureStorageKey.lockScreenToggle, false);
      await this.secureStorageService.removeValue(SecureStorageKey.lockScreenToggleSettingId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userIsAllowedToAccessChatMarketplaceSettingId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.chatKeyPairs, false);
      await this.secureStorageService.removeValue(SecureStorageKey.chatUserData, false);
      await this.secureStorageService.removeValue(SecureStorageKey.marketplace, false);
      await this.secureStorageService.removeValue(SecureStorageKey.certificates, false);
      await this.secureStorageService.removeValue(SecureStorageKey.certificatesMedia, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userPublicKey, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userPrivateKey, false);
      await this.secureStorageService.removeValue(SecureStorageKey.userMnemonic, false);
      await this.secureStorageService.removeValue(SecureStorageKey.web3WalletMnemonic, false);
      await this.secureStorageService.removeValue(SecureStorageKey.web3WalletPrivateKey, false);
      await this.secureStorageService.removeValue(SecureStorageKey.web3WalletPublicKey, false);
      await this.secureStorageService.removeValue(SecureStorageKey.paperBackupTimestamp, false);
      await this.secureStorageService.removeValue(SecureStorageKey.cloudBackupTimestamp, false);
      await this.secureStorageService.removeValue(SecureStorageKey.paperBackupTimestampSettingId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.cloudBackupTimestampSettingId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.walletCurrencies, false);
      try{
        this.vaultSecretsServiceAkita.clear();
      } catch(e) {
        console.log(e);
      }
      await this.secureStorageService.removeValue(SecureStorageKey.currency, false);
      await this.secureStorageService.removeValue(SecureStorageKey.hCaptchaDashboard, false);
      await this.signService.removeAllSQLData();
      await this.secureStorageService.removeValue(SecureStorageKey.onboardingHasRun, false); // onboardingHasRun
      await this.secureStorageService.removeValue(SecureStorageKey.wizardNotRunSettingId, false); // wizardNotRun setting
      await this.secureStorageService.removeValue(SecureStorageKey.wizardNotRun, false); // wizardNotRun
      await this.secureStorageService.removeValue(SecureStorageKey.idCardSet, false);
      await this.secureStorageService.removeValue(SecureStorageKey.driverLicenseSet, false);
      await this.secureStorageService.removeValue(SecureStorageKey.passportSet, false);
      await this.secureStorageService.removeValue(SecureStorageKey.residencePermitSet, false);
      await this.secureStorageService.removeValue(SecureStorageKey.idCardSetSettingId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.driverLicenseSetSettingId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.passportSetSettingId, false);
      await this.secureStorageService.removeValue(SecureStorageKey.residencePermitSetSettingId, false);
      console.log("Login Page: End removing secure storage entries");
    }

    this.markFormAsValid();
    this.authProcessing = true;

    // this.loader.loaderCreate();
    this.displayLoadingDots = true;
    if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();

    var logCheck = await this.authenticationProviderService.loginUsernameAndPassword(email, password, false);

    console.log("Login Page: logCheck", logCheck);

    if (logCheck) {

      await this.userProviderService.storeUsername(email);

      console.log("Login Page: this.settingsService.getAll();");
      this.settingsService.getAll();
      try {
        await this.secureStorageService.checkWhetherUserHasSwitched(email);
      } catch (e) {
        // TODO what to do when error occurs end the app???
        console.log(e);
      }
      if (!this.nonce && !this.loginHash) {
        await this.signService.brandDeviceToUser(email, password);
      }
      await this.secureStorageService.setValue(SecureStorageKey.userName, email);

      console.log("Login Page: this.kycService.syncKyc();");
      this.kycService.syncKyc();

      // Call getKeys only based on the response from the Alert Window
      // await this.keyRecoveryBackupService.getKeys();
      // await this._SecurePinService.fetchEncryptedPIN();
      // await this.loader.loaderDismiss();

      console.log("Login Page: this.keyRecoveryBackupService.recoveryAlert;");
      await this.keyRecoveryBackupService.recoveryAlert(this.loginMode);
      this.displayLoadingDots = false;

    } else {
      this.markFormAsNotValid();
      this.authProcessing = false;
      // this.loader.loaderDismiss();
      this.displayLoadingDots = false;
    }
  }

  async resetDevice() {
    const alert = await this.alertController.create({
      mode: 'ios',
      header: '',
      message: this.translateProviderService.instant('LOGIN.reset-device-warning'),
      buttons: [{
          text: this.translateProviderService.instant('GENERAL.ok'),
          cssClass: 'primary',
          handler: async () => {
            // await this.loader.loaderCreate();
            this.displayLoadingDots = true;
            this.signService.unbrandDeviceFromUser().then(() => {
              // window.location.reload();
              this.presentToast(this.translateProviderService.instant('GENERAL.device-reset-successful'));
              // this.nav.navigateRoot('/login');
              // window.location.reload();
              // console.log(this.router.url);
              // console.log(this.router.parseUrl(this.router.url));
              let checker = this.router.parseUrl(this.router.url).queryParams;
              // console.log('===>', checker);
              // console.log(this.loginHash);
              if(['registration','resetpassword', 'forgot-username', 'browsemode'].includes(checker.from)) {
                // this.loader.loaderDismiss();
                this.displayLoadingDots = false;
                this.nav.navigateRoot('/login');
              } else {
                setTimeout(() => {
                  // this.loader.loaderDismiss();
                  this.displayLoadingDots = false;
                  window.location.href = '/';
                }, 2000);
              }
            })
          }
        }, {
          text: this.translateProviderService.instant('GENERAL.cancel'),
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    await alert.present();
  }

  /** Helper to get the abstract control */
  public get f(): {[key: string]: AbstractControl} {
    return this.loginForm.controls;
  }

  signUp(): void {
    this.nav.navigateForward('/sign-up');
  }

  forgotUsername() {
    this.nav.navigateForward('/forgot-username');
  }

  forgotPassword(): void {
    this.nav.navigateForward('/reset-password');
  }

  clearLoginForm(): void {
    this.loginForm.reset();
  }

  markFormAsValid(): void {
    this.notValidForm = false;
  }

  markFormAsNotValid(): void {
    this.notValidForm = true;
  }

  togglePasswordVisibility() {
    this.passwordStrengthInfo.password1Type = (this.passwordStrengthInfo.password1Type == 'text') ? 'password' : 'text';
  }

  togglePinVisibility() {
    this.pinNotVisible = !this.pinNotVisible;
  }

  onCodeChanged(code: string) {
    console.log("code changed!")
    // console.log(code)
  }

  async onCodeCompleted(code: string) {
    console.log("Login Page: onCodeCompleted!");
    var pin = await this.secureStorageService.getValue(SecureStorageKey.securePINEncrypted, false);
    console.log("Login Page: pin", pin);
    if(pin) {

      var success = await this._SecurePinService.checkPIN(code, pin);

      console.log("Login Page: success", success);

      if(success) {

        this.loginOverPinOrBiometric();

      } else {
        this.presentToast(this.translateProviderService.instant('PIN.incorrectPIN'));
        this.codeInput.reset();
      }

    } else {
      this.presentToast(this.translateProviderService.instant('PIN.difMode'))
    }


  }

  private async loginOverPinOrBiometric() {
    // this.loader.loaderCreate();
    this.displayLoadingDots = true;
        var encryptedBasicAuthToken = await this.secureStorageService.getValue(SecureStorageKey.encryptedBasicAuthToken, false);

        console.log("Login Page: encryptedBasicAuthToken", encryptedBasicAuthToken);

        if(encryptedBasicAuthToken) {
          this.processTokenDecryption(encryptedBasicAuthToken);
          console.log("Login Page: this.processTokenDecryption(encryptedBasicAuthToken);");
          this.settingsService.getAll();
          console.log("Login Page: this.settingsService.getAll();");
        } else {
          var token = await this.secureStorageService.getValue(SecureStorageKey.basicAuthToken, false);
          console.log("Login Page: token", token);
          this.appStateService.basicAuthToken = token;
          this.appStateService.isAuthorized = true;
          var loginStatus = await this.authenticationProviderService.login(token, false);
          console.log("Login Page: loginStatus", loginStatus);
          if(loginStatus) {
            this.settingsService.getAll();
            console.log("Login Page: this.settingsService.getAll();");
            this._MarketplaceService.init();
            console.log("Login Page: this._MarketplaceService.init();");
            await this.keyRecoveryBackupService.recoveryAlert(true);
            console.log("Login Page: await this.keyRecoveryBackupService.recoveryAlert(true);");
            this._SQLStorageService.getMarketPlaceMerchants();
            console.log("Login Page: this._SQLStorageService.getMarketPlaceMerchants();");
            // this.loader.loaderDismiss();
            // this.displayLoadingDots = false;
          } else {
            this.displayLoadingDots = false;
            try {
              this.codeInput.reset();
            } catch(e) {
                console.log(e);
            }
            this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));
          }
        }
  }

  processTokenDecryption(encryptedBasicAuthToken: string) {
    if (this.platform.is('hybrid') && Capacitor.isPluginAvailable('Device')) {
      Device.getId().then((deviceId: DeviceId) => {
        console.info('LoginPage#processTokenDecryption; deviceId', deviceId);

        var string1 = deviceId.identifier.substr(4,9);
        var string2 = UDIDNonce.helix.substr(2,11);
        var key = CryptoJS.SHA256(string1 + string2, UDIDNonce.helix as any).toString(CryptoJS.enc.Hex);
        console.log("Case 1: " + key);
        this.postUdidFetch(encryptedBasicAuthToken, key).then(() => {
          console.log("Login via PIN successful!");
        }).catch(e => {
          console.log(e);
        });
      },
      (err) => {
        console.log('err: ' + JSON.stringify(err));
      });
    }
    else {
        console.log('Could not obtain UDID.');
        var udid = window["navigator"].vendor + window["navigator"].appName + window["navigator"].appCodeName;
        udid = udid.replaceAll(" ","").replaceAll(".","");
        var string1 = udid.substr(4,9);
        var string2 = UDIDNonce.helix.substr(2,11);
        var key = CryptoJS.SHA256(string1 + string2, UDIDNonce.helix as any).toString(CryptoJS.enc.Hex);
        console.log("Case 2: " + key);
        this.postUdidFetch(encryptedBasicAuthToken, key).then(() => {
          console.log("Login via PIN successful!");
        }).catch(e => {
          console.log(e);
        });
    }
  }

  toggleLoginMode() {
    this.loginModePIN = !this.loginModePIN;
  }

  goToBrowseMode() {
    // this.secureStorageService.setValue(SecureStorageKey.browseMode, true.toString()).then(() => {
      this.nav.navigateForward('/dashboard/tab-home')
    // })
  }

  postUdidFetch(encryptedBasicAuthToken: string, key: string): Promise<void> {
    return new Promise((resolve, reject) => {
      console.log("Key obtained after fetching UDID: " + key);
      this.crypto.symmetricDecrypt(encryptedBasicAuthToken, key).then((token) => {
        if(token) {
          console.log("Console log 4");
          if(!this._NetworkService.checkConnection()) {
            this.appStateService.isAuthorized = true;
            this.appStateService.basicAuthToken = token;
              this.secureStorageService.setValue(SecureStorageKey.basicAuthToken, token).then(() => {
                console.log("Console log 5");
                this.keyRecoveryBackupService.recoveryAlert(true).then(() => {
                  console.log('Navigation done!');
                  this.secureStorageService.removeValue(SecureStorageKey.encryptedBasicAuthToken, false).then(() => {
                    // this.loader.loaderDismiss().then(() => {
                      this.displayLoadingDots = false;
                      resolve();
                    // });
                  }).catch(e => reject(e));
                }).catch(e => reject(e));
              }).catch(e => reject(e));
          } else {
            this.authenticationProviderService.login(token, false).then(() => {
              this.appStateService.basicAuthToken = token;
              this.secureStorageService.setValue(SecureStorageKey.basicAuthToken, token).then(() => {
                console.log("Console log 5");
                this.keyRecoveryBackupService.recoveryAlert(true).then(() => {
                  console.log('Navigation done!');
                  this.secureStorageService.removeValue(SecureStorageKey.encryptedBasicAuthToken, false).then(() => {
                    // this.loader.loaderDismiss().then(() => {
                      this.displayLoadingDots = false;
                      resolve();
                    // });
                  }).catch(e => reject(e));
                }).catch(e => reject(e));
              }).catch(e => reject(e));
            })
          }
        } else {
          console.log('Token was undecryptable');
          reject();
        }
      })
    })
  }

}
