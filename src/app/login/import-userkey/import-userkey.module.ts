import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { ImportUserkeyComponent } from './import-userkey.component';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CodeInputModule } from 'angular-code-input';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { SharedModule } from 'src/app/shared/shared.module';
import { KeyExportModule } from 'src/app/sign-up/components/step-5/key-export/key-export.module';

@NgModule({
  imports: [
    CommonModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    CodeInputModule,
    KeyExportModule,
    TranslateModule.forChild()
  ],
  providers: [SocialSharing, File, Chooser],
  declarations: [ImportUserkeyComponent],
//   entryComponents: [ImportUserkeyComponent],
  exports: [ImportUserkeyComponent]
})
export class ImportUserKeyModule {}
