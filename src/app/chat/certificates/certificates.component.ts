import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, ToastController, Platform, AlertController } from '@ionic/angular';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { AlertsProviderService } from 'src/app/core/providers/alerts/alerts-provider.service';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { SmartAgentURI } from 'src/app/core/models/api-smart-agent.enum';
import { CryptojsProviderService } from 'src/app/core/providers/cryptojs/cryptojs-provider.service';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { Clipboard } from '@capacitor/clipboard';
// const stream = require('stream');
// const { promisify } = require('util');
// const fs = require('fs');
// const got = require('got');

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.scss'],
})
export class CertificatesComponent implements OnInit {
  @Input() data: any;
  public displayItems = []
  // public imageUrl: string = '';
  public imageUrl: SafeUrl = '';
  public purpose = '';
  public qrCodeValue: { heading?: string; value?: string } = null;
  private allowedQRParameters = [ 'vc', 'vp', 'signatureHash' ]
  private browser: InAppBrowserObject;
  public carIdentity: boolean = false;
  browserOptions = 'zoom=no,footer=no,hideurlbar=yes,footercolor=#BF7B54,hidenavigationbuttons=yes,presentationstyle=pagesheet';
  constructor(
    private _ModalController: ModalController,
    private _SecureStorageService: SecureStorageService,
    private _ApiProviderService: ApiProviderService,
    private _LoaderProviderService: LoaderProviderService,
    private _CryptoProviderService: CryptoProviderService,
    private _CryptojsProviderService: CryptojsProviderService,
    private _TranslateProviderService: TranslateProviderService,
    private _AlertsProviderService: AlertsProviderService,
    private _ToastController: ToastController,
    private _InAppBrowser: InAppBrowser,
    private _AlertController: AlertController,
    private _ActionSheetController: ActionSheetController,
    private _SafariViewController: SafariViewController,
    private _File: File,
    private sanitizer: DomSanitizer,
    
    private _FileOpener: FileOpener,
    private _Platform: Platform
  ) { }

  public profilePictureSrc = '../../../assets/job-user.svg';
  public emptyCertificateProfile = '../../../assets/job-user.svg';

  async ngOnInit() {
    const urlCreator = (window as any).URL || (window as any).webkitURL;
    console.log(this.data.input);
    if(!this.data.input.credentialValues) {
      this.data.input = JSON.parse(this.data.input);
    }
    var keys = Object.keys(this.data.input.credentialSubjectRaw)
    console.log(keys);
    this.imageUrl = this.data.input.imageBlob ? this.sanitizer.bypassSecurityTrustUrl(this.data.input.imageBlob) : this.emptyCertificateProfile;
    if(Object.keys(this.data.input.credentialValues).includes("car")) {
      if(!!this.data.input.didDocument) {
        this.data.input.credentialSubjectRaw['did'] = this.data.input.didDocument.id;
      }
      this.imageUrl =  this.data.input.credentialSubjectRaw.logo;
      if(!this.data.input.vc) {
        try {
          // var vc = await this._Vade.createVc(this.data.input);
          var vc = await this._ApiProviderService.generateGenericVc(this.data.input.credentialSubjectRaw);
          
        } catch(e) {
          console.log(e);
          // var 
          vc = 
          {
              "@context": "https://www.w3.org/2018/credentials/v1",
              "id": "vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
              "type": [
                "VerifiableCredential",
                "KYC"
              ],
              "issuer": {
                "id": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b"
              },
              "credentialSubject": {
                "id": "did:evan:testcore:0x1ee6bfe7034fdbc5ac5d57fb171b3820297d7711",
                "data": [
                  {
                    "logo": "https://mediapool.bmwgroup.com/cache/P9/202108/P90432173/P90432173-the-new-bmw-ix3-8-2021-2246px.jpg",
                    "manufacturer": "BMW",
                    "model": "iX3",
                    "creation_date": "02-03-2022",
                    "expiration_date": "02-04-2022",
                    "vin": "5UXFE83507LZ40758",
                    "insurance": "Allianz",
                    "insurance_id": "ALL6127/HDOI",
                    "registration_plate": "BD51 SMR",
                    "public_key": "0xdcc703c0E500B653Ca82273B7BFAd8045D85a470",
                    "qr_code_type": "",
                  }
                ]
              },
              "validFrom": "1970-01-19T09:08:03Z",
              "validTo": "2023-01-20T02:39:15Z",
              "credentialStatus": {
                "id": "https://testcore.evan.network/smart-agents/smart-agent-did-resolver/vc/status/vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
                "type": "evan:evanCredential"
              },
              "proof": {
                "type": "EcdsaPublicKeySecp256k1",
                "created": "2020-04-28T14:14:34.023Z",
                "jws": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NkstUiJ9.eyJpYXQiOjE1ODgwODMyNzMsInZjIjp7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L2NyZWRlbnRpYWxzL3YxIl0sInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJLWUMiXSwiaWQiOiJ2YzpldmFuOnRlc3Rjb3JlOjB4MTM5ZDk2ZTEyN2U1YjI1OGJmZTY2ZTQ2MTZhNGFjMmEyNzFjMGU4NjQ0MmJiMGIxMTljYWFkODVlN2EzYmU1YiIsImlzc3VlciI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9LCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4MWVlNmJmZTcwMzRmZGJjNWFjNWQ1N2ZiMTcxYjM4MjAyOTdkNzcxMSIsImRhdGEiOnsidHJ1c3RlZEJ5IjoiQXV0aGFkYSIsInRydXN0TGV2ZWwiOjEsInRydXN0VmFsdWUiOjEsImlkIjpudWxsLCJ1c2VyIjpudWxsLCJsYXN0TW9kaWZlZCI6bnVsbCwiZ2VuZGVyIjoiZGl2ZXJzIiwidGl0bGUiOiJQcm9mLiBEci4iLCJjYWxsbmFtZSI6InRoZSByb2NrIiwiZmlyc3RuYW1lIjoiRXJpa2EiLCJtaWRkbGVuYW1lIjoiTWFyaWEiLCJsYXN0bmFtZSI6Ik11c3Rlcm1hbiIsIm1hcml0YWxzdGF0dXMiOiJ3aWRvd2VkIiwibWFpZGVubmFtZSI6Ik11c3RlcmZyYXUiLCJlbWFpbHR5cGUiOiJ0ZXh0LW9ubHkiLCJlbWFpbCI6ImVyaWthLm11c3Rlcm1hbkBibG9ja2NoYWluLWhlbGl4LmNvbSIsInBob25ldHlwZSI6Im1vYmxpZSIsInBob25lIjoiMDE3NTY3NDUyMyIsImRhdGVvZmJpcnRoIjoiMTkyMC0wNi0yMlQyMzowMDowMFoiLCJjaXR5b2ZiaXJ0aCI6IlN0b2NraG9sbSIsImNvdW50cnlvZmJpcnRoIjoiU2Nod2VkZW4iLCJjaXRpemVuc2hpcCI6Imdlcm1hbiIsInN0cmVldCI6IkxpbmRlbnN0cmFzZSA0MiIsImNpdHkiOiJnZXJtYW4iLCJ6aXAiOiIxMDk2OSIsInN0YXRlIjoiQmVybGluIiwiY291bnRyeSI6IkRldXRzY2hsYW5kIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudHR5cGUiOiJpZC1jYXJkIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudG51bWJlciI6IkM1TlZQQ1o1RSIsImlkZW50aWZpY2F0aW9uaXNzdWVjb3VudHJ5IjoiZ2VybWFueSIsImlkZW50aWZpY2F0aW9uaXNzdWVkYXRlIjoiMjAxNS0wMi0yOFQyMzowMDowMFoiLCJpZGVudGlmaWNhdGlvbmV4cGlyeWRhdGUiOiIyMDI1LTAyLTI4VDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2Vkb2N1bWVudG51bWJlciI6IkExNzE4MTkyMCIsImRyaXZlcmxpY2VuY2Vjb3VudHJ5IjoiZ2VybWFueSIsImRyaXZlcmxpY2VuY2Vpc3N1ZWRhdGUiOiIyMDIwLTAxLTMxVDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2VleHBpcnlkYXRlIjoiMjAzMC0wMS0zMVQyMzowMDowMFoiLCJ0YXhyZXNpZGVuY3kiOiJnZXJtYW55IiwidGF4bnVtYmVyIjoiWDQzN04yMzg2NyIsInRlcm1zb2Z1c2UiOiJUZXJtc29mdXNlIiwidGVybXNvZnVzZXRoaXJkcGFydGllcyI6IlRlcm1zb2Z1c2V0aGlyZHBhcnRpZXMifX0sInZhbGlkRnJvbSI6IjE5NzAtMDEtMTlUMDk6MDg6MDNaIiwidmFsaWRUbyI6IjE5NzAtMDEtMjBUMDI6Mzk6MTVaIiwiY3JlZGVudGlhbFN0YXR1cyI6eyJpZCI6Imh0dHBzOi8vdGVzdGNvcmUuZXZhbi5uZXR3b3JrL3NtYXJ0LWFnZW50cy9zbWFydC1hZ2VudC1kaWQtcmVzb2x2ZXIvdmMvc3RhdHVzL3ZjOmV2YW46dGVzdGNvcmU6MHgxMzlkOTZlMTI3ZTViMjU4YmZlNjZlNDYxNmE0YWMyYTI3MWMwZTg2NDQyYmIwYjExOWNhYWQ4NWU3YTNiZTViIiwidHlwZSI6ImV2YW46ZXZhbkNyZWRlbnRpYWwifX0sImlzcyI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9.ABk8RBlpCAX-gY47_aRxskCCrl_AsluEDbYNEY8BNlY2NS78aoIWXDNNhoJe0J5EpwnWQyR02I9qMn_lqL0qFwE",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b#key-1"
              }
            }
        }

        if(!!vc.id) {
          this.data.input.credentialSubjectRaw['vcId'] = vc.id;
        }

        delete vc.proof;
        this.data.input = Object.assign(this.data.input, { vc });
        
      } else {
        delete this.data.input.vc.proof;
      }

      this.carIdentity = true;
      console.log(this.data.input);
      var qrcodeURL = `helix-id://helix-id.com/caridentity?string=${encodeURIComponent(this._CryptojsProviderService.encrypt(this.data.input.credentialSubjectRaw, UDIDNonce.energy))}`;
      console.log(qrcodeURL);
    }
    // this.imageUrl = this.data.input.imageBlob
    // console.log(this.imageUrl)
    this.purpose = this.data.input.credentialValues ? (this.data.input.credentialValues.nda ? 'nda' : (this.data.input.credentialValues.medicalDocument ? 'medical' : 'authToRepresent')) : 'kycCertificate';
    keys = keys.filter(k => !['logo', 'companyName', 'qr_code_type'].includes(k))
    keys.forEach(k => {
      var tem = Object.assign({}, {key: k, value: this.data.input.credentialSubjectRaw[k]})
      console.log(tem)
      if(tem.key == 'testResult') {
        tem.value = this._TranslateProviderService.instant('CERTIFICATES.keys.testResults.' + this.data.input.credentialSubjectRaw[k])
      }
      this.displayItems.push(tem)
      console.log(this.displayItems)
    })
    console.log(this.displayItems)
  }

  async processRequest(input: any, approval: boolean) {
    await this._LoaderProviderService.loaderCreate(this._TranslateProviderService.instant('LOADER.signingRequest'));
    var index = this.data.tntUseCaseList.indexOf(input);
    input.credentialValues.approved = approval.toString();

    input.credentialValues.timestamp = (+new Date()).toString();
    input.credentialValues.step = 'ceoSign';
    var signedMessage = await this._CryptoProviderService.signMessage(JSON.stringify(input));
    input.signatureHash = signedMessage;
    console.log(input);

    await this._LoaderProviderService.loaderDismiss();
    await this._LoaderProviderService.loaderCreate(this._TranslateProviderService.instant('LOADER.obtainingVP'), 75000);
    this.data.tntUseCaseList[index] = input;

    if(!input.dummy) {
      var tntUseCaseListTemp = (this.data.tntUseCaseList as any);

      delete tntUseCaseListTemp[index].dummy;
      delete tntUseCaseListTemp[index].imageBlob;
      delete tntUseCaseListTemp[index].documentBlob;

      tntUseCaseListTemp[index].schemaDid = "did:evan:zkp:0xe9847153c8899515242d384f470e8b2e2643d2076f69f8f72e3a71293b7f5921";

      await this._SecureStorageService.setValue(SecureStorageKey.certificates, JSON.stringify(this.data.tntUseCaseList));

      console.log("Body sent for request: " + JSON.stringify(tntUseCaseListTemp[index]));
      var response = await this._ApiProviderService.tntSendApproval(tntUseCaseListTemp[index]);
      console.log("Response received: " + JSON.stringify(response));

      input = Object.assign(input, { vp: response.vp });
      input = Object.assign(input, { vc: response.vc });

      this.data.tntUseCaseList[index] = input;
      await this._SecureStorageService.setValue(SecureStorageKey.certificates, JSON.stringify(this.data.tntUseCaseList));
      await this.presentToast(this._TranslateProviderService.instant('CERTIFICATES.vcActive'));
    }
    await this._LoaderProviderService.loaderDismiss();
    this._ModalController.dismiss();
  }

  async copyParameter($event, key: string, value: string) {
      await Clipboard.write({ string: value });
      await this.presentToast(key + this._TranslateProviderService.instant('SETTINGSACCOUNT.copied'))
  }

  showCarDid(didDocument: any) {
    
    this._AlertController.create({
      header: this._TranslateProviderService.instant('CERTIFICATES.didDoc'),
      message: JSON.stringify(didDocument),
      buttons: [
        {
          text: this._TranslateProviderService.instant('CERTIFICATES.openDidResolver'),
          handler: async () => {
            if(this._Platform.is('hybrid')) {
              await Clipboard.write({ string: didDocument.id });
              await this.presentToast('Did' + this._TranslateProviderService.instant('SETTINGSACCOUNT.copied'));
            }
            var url = `https://dev.uniresolver.io/`;
            this._InAppBrowser.create(url, "_system");
          }
        },
        {
          text: this._TranslateProviderService.instant('CERTIFICATES.closeModal'),
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    }).then(al => {
      al.present();
    })
    
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 3000, position: 'top' });
    await toast.present();
  }

  public async shareCredentials(input: any, key: string) {
    console.log(input)
    var buttons = []
    buttons.push({ text: 'JSON String', icon: 'code', handler: async () => {
      await this._AlertsProviderService.alertCreate(key, '', JSON.stringify(input[key]),[this._TranslateProviderService.instant('GENERAL.ok')])
      }
    })
    if(this.allowedQRParameters.includes(key)) {
      var size = new TextEncoder().encode(JSON.stringify(input[key])).length
      var kiloBytes = size / 1024;
      if(kiloBytes < 3) {
        buttons.push({ text: 'QR Code', icon: 'qr-code', handler: () => {
          this.qrCodeValue = {}
          this.qrCodeValue = Object.assign(this.qrCodeValue, { heading: key })
          this.qrCodeValue = Object.assign(this.qrCodeValue, { value: `${JSON.stringify(input[key])}` })
          }
        })
      } else {
        buttons.push({ text: 'QR Code', icon: 'qr-code', handler: () => {
          this.qrCodeValue = {}
          this.qrCodeValue = Object.assign(this.qrCodeValue, { heading: key })
            if(key == 'vp' || key == 'vc') {
              var cleanedVp = !!input[key].value ? input[key].value : JSON.stringify(input[key])
              var parsedCleanedVp = JSON.parse(cleanedVp)
              if(!!parsedCleanedVp.proof){
                delete parsedCleanedVp.proof
              }
              if(!!parsedCleanedVp.verifiableCredential){
                parsedCleanedVp.verifiableCredential.forEach(k => {
                  delete k.proof
                })
              }
              console.log(JSON.stringify(parsedCleanedVp))
              this.qrCodeValue = Object.assign(this.qrCodeValue, { value: `${JSON.stringify(parsedCleanedVp)}` })
            }
          }
        })
      }
    }
    buttons.push({ text: this._TranslateProviderService.instant('GENERAL.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('Cancel') } })
    const alert = await this._ActionSheetController.create({
      mode: 'md',
      header: this._TranslateProviderService.instant('CERTIFICATES.chooseFormat'),
      buttons: buttons
    });
    await alert.present();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  returnFileNameFromAssetURI(uri: string) {
    if(uri) {
      try {
        var u = new URL(uri);
      } catch (e) {
        var u = new URL(SmartAgentURI.uri + uri);
      }
      return uri.replace(u.origin, "").replace(u.search, "").replace("/file/","").replace("/api/v1/tnt", "");
    } else {
      return null;
    }
  }

  public async processNda(input: any, processBoolean: boolean) {

    // alert("Complete Input: " + JSON.stringify(input))
    // alert("document Blob: " + input.documentBlob)
    console.log(input)

    if(!input.documentBlob) {
      return alert(this._TranslateProviderService.instant('CERTIFICATES.noAttachmentFound'));
    }

    if(!processBoolean) {

      if(input.documentBlob.includes("https://drive.google.com/")) {
        this.browser = this._InAppBrowser.create(input.documentBlob, '_system');
        return;
      }

      // console.log(input.documentBlob)

      var url = this.b64toBlob(input.documentBlob, 'application/pdf', 512)
      var blobUrl = URL.createObjectURL(url)
      // var t = base64topdf.base64Decode(input.documentBlob, 'file.pdf');
      console.log(url)
      console.log(blobUrl)

      let path = null;
      if( this._Platform.is('ios') && this._Platform.is('hybrid') ) {
        path = this._File.documentsDirectory;
      } else if ( this._Platform.is('android') && this._Platform.is('hybrid') ) {
        path = this._File.dataDirectory;
      }

      // alert(JSON.stringify(path))
      // alert(JSON.stringify(this.getFileUriWithFilePrefix(path)))

      if(path != null) {

          var fileName = input.attachments[0].tag + "-" + this.returnFileNameFromAssetURI(input.attachments[0].uri) + '.pdf';
          var fullPath = path + fileName;

          if ( this._Platform.is('hybrid') ) {
            this._File.createFile(path, fileName, true).then(() => {
              this._File.writeFile(path, fileName, url, { replace: true }).then(s => {
                // downloadFile(input.documentBlob, fileName).then(() => {
                  this._FileOpener.open(fullPath, 'application/pdf');
                // })
              })
            })
          } else {
              this.browser = this._InAppBrowser.create(blobUrl, '_system');
          }

      } else {
        this.browser = this._InAppBrowser.create(blobUrl, '_system')
      }


    } else {
      await this._LoaderProviderService.loaderCreate(this._TranslateProviderService.instant('CERTIFICATES.signingCertificate'));
      var index = this.data.tntUseCaseList.indexOf(input);
      input.credentialValues.approved = true.toString();
      input.credentialValues.timestamp = (+new Date()).toString();
      var signedMessage = await this._CryptoProviderService.signMessage(JSON.stringify(input));
      input.signatureHash = signedMessage;
      console.log(input);
      if(!input.dummy) {
        var medicalCertificate = false;
        this.data.tntUseCaseList[index] = input;
        await this._LoaderProviderService.loaderCreate(this._TranslateProviderService.instant('LOADER.obtainingVP'), 75000);
        var tntUseCaseListTemp = (this.data.tntUseCaseList as any);

        delete tntUseCaseListTemp[index].dummy;
        delete tntUseCaseListTemp[index].imageBlob;
        delete tntUseCaseListTemp[index].documentBlob;

        if(!!tntUseCaseListTemp[index].credentialValues.medicalDocument) {
          medicalCertificate = true;
          tntUseCaseListTemp[index].credentialValues["nda"] = tntUseCaseListTemp[index].credentialValues.medicalDocument;
          delete tntUseCaseListTemp[index].credentialValues.medicalDocument;
          tntUseCaseListTemp[index].attachments[0].tag = 'nda';
          tntUseCaseListTemp[index]["credentialSubjectRaw"]["country"] = '';
          tntUseCaseListTemp[index]["credentialSubjectRaw"]["postalCode"] = '';
          tntUseCaseListTemp[index]["credentialSubjectRaw"]["city"] = '';
          tntUseCaseListTemp[index]["credentialSubjectRaw"]["address"] = '';
          tntUseCaseListTemp[index]["credentialSubjectRaw"]["region"] = '';
          delete tntUseCaseListTemp[index]["credentialSubjectRaw"]["idNumber"];
          delete tntUseCaseListTemp[index]["credentialSubjectRaw"]["testIdentification"];
          delete tntUseCaseListTemp[index]["credentialSubjectRaw"]["testIssuer"];
          delete tntUseCaseListTemp[index]["credentialSubjectRaw"]["dateOfTest"];
          delete tntUseCaseListTemp[index]["credentialSubjectRaw"]["expirationDate"];
          delete tntUseCaseListTemp[index]["credentialSubjectRaw"]["testMethod"];
          delete tntUseCaseListTemp[index]["credentialSubjectRaw"]["testResult"];
        }

        await this._SecureStorageService.setValue(SecureStorageKey.certificates, JSON.stringify(this.data.tntUseCaseList));
        console.log("Body sent for request: " + JSON.stringify(tntUseCaseListTemp[index]));
        var response = await this._ApiProviderService.tntSendApproval(tntUseCaseListTemp[index]);
        console.log("Response received: " + JSON.stringify(response));

        input = Object.assign(input, { vp: response.vp });
        input = Object.assign(input, { vc: response.vc });

        if(medicalCertificate && response.vp && response.vc) {
          input.credentialValues["medicalDocument"] = input.credentialValues.nda;
          delete input.credentialValues.nda;
          input.attachments[0].tag = 'medicalDocument';
          delete input["credentialSubjectRaw"]["country"];
          delete input["credentialSubjectRaw"]["postalCode"];
          delete input["credentialSubjectRaw"]["city"];
          delete input["credentialSubjectRaw"]["address"];
          delete input["credentialSubjectRaw"]["region"];
          input["credentialSubjectRaw"]["idNumber"] = "T01000322"; // T01000322
          input["credentialSubjectRaw"]["testIdentification"] = "3CF75K8D0L"; // 3CF75K8D0L
          input["credentialSubjectRaw"]["testIssuer"] = "did:evan:helix892743iughf8"; // did:evan:helix892743iughf8
          input["credentialSubjectRaw"]["dateOfTest"] = "202008261430"; // 202008261430
          input["credentialSubjectRaw"]["expirationDate"] = "202009261429"; // 202009261429
          input["credentialSubjectRaw"]["testMethod"] = "PCR"; // PCR
          input["credentialSubjectRaw"]["testResult"] = "n"; //  (e.g. p = positive, n = negative, u = unknown):
        }

        var c = await this._SecureStorageService.getValue(SecureStorageKey.certificatesMedia, false);
        var certificateMedia = !!c ? JSON.parse(c) : {};
        // input.documentBlob = certificateMedia[this.returnFileNameFromAssetURI(input.attachments[0].uri)];
        // input.imageBlob = !!input.credentialSubjectRaw.logo ? (this.validURL(input.credentialSubjectRaw.logo) ? input.credentialSubjectRaw.logo : certificateMedia[this.returnFileNameFromAssetURI(input.credentialSubjectRaw.logo)]) : this.profilePictureSrc;
        input.imageBlob = !!input.credentialSubjectRaw.logo ? (!!input.credentialSubjectRaw.logo.url ? certificateMedia[this.returnFileNameFromAssetURI(input.credentialSubjectRaw.logo.url)] : this.emptyCertificateProfile) : this.emptyCertificateProfile;
        input.documentBlob = !!input.attachments ?  certificateMedia[this.returnFileNameFromAssetURI(input.attachments[0].uri)] : null;
        this.data.tntUseCaseList[index] = input;

        await this._SecureStorageService.setValue(SecureStorageKey.certificates, JSON.stringify(this.data.tntUseCaseList));
        await this.presentToast(this._TranslateProviderService.instant('CERTIFICATES.vcActive'));
        await this._LoaderProviderService.loaderDismiss();
      }
      await this._LoaderProviderService.loaderDismiss();
    }
  }

  getAsWebViewCorrectUrlMobile(fileUri: string) {
    if (this._Platform.is('ios')) {
      return (window as any).Ionic.WebView.convertFileSrc(fileUri);
    }
    else {
      // correct file paths on Android due to https://github.com/ionic-team/cordova-plugin-ionic-webview/issues/598
      var filePathCorrected = (window as any).Ionic.WebView.convertFileSrc(fileUri);
      filePathCorrected = filePathCorrected.replace('file:/', "https://localhost/_app_file_/");
      return filePathCorrected;
    }
  }

  getFileUriWithFilePrefix(fileUri: string) {
    var prefix = 'file:///';
    var filePathCorrected = prefix + fileUri;
    filePathCorrected = filePathCorrected.replace('file:///file:///', prefix);
    filePathCorrected = filePathCorrected.replace('file:///file:/', prefix);
    filePathCorrected = filePathCorrected.replace('file:////', prefix);
    return filePathCorrected;
  }

  showSafariInstance(url: string) {
    this._SafariViewController.isAvailable().then((available: boolean) => {
            if ( available ) {
                this._SafariViewController.show({
                    url,
                    hidden: false,
                    animated: true,
                    transition: 'slide',
                    enterReaderModeIfAvailable: false,
                    tintColor: '#54BF7B'
                })
                    .subscribe((result: any) => {
                        },
                        (error: any) => console.error(error));
            } else {
                this.browser = this._InAppBrowser.create(url, '_self', this.browserOptions);
            }
        }
    );
}

b64toBlob(b64Data, contentType, sliceSize) {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  b64Data = b64Data.replace(/^[^,]+,/, '');
  b64Data = b64Data.replace(/\s/g, '');

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, {
      type: contentType
  });
  // return byteCharacters;
  return blob;
}

  close() {
    this._ModalController.dismiss()
  }

  saveCredentials(a: any) {
    console.log(a);
    this.presentToast(this._TranslateProviderService.instant('CERTIFICATES.missingFeature'));
    this._ModalController.dismiss();
  }

  async deleteCarData(car: any) {
    await this._LoaderProviderService.loaderCreate();
    console.log(car);
    console.log(this.data);
    var carUseCaseList = this.data.carUseCaseList.filter(k => k.qrCode != this.data.input.qrCode);
    await this._SecureStorageService.setValue(SecureStorageKey.carIdentities, JSON.stringify(carUseCaseList));
    await this.presentToast( this._TranslateProviderService.instant('CARIDENTITIES.deleted'));
    await this._LoaderProviderService.loaderDismiss();
    this._ModalController.dismiss();
  }
}
