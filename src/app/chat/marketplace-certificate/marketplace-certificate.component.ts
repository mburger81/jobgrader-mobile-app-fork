import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ModalController, AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { BarcodeService } from 'src/app/core/providers/barcode/barcode.service';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
@Component({
  selector: 'app-marketplace-certificate',
  templateUrl: './marketplace-certificate.component.html',
  styleUrls: ['./marketplace-certificate.component.scss'],
})
export class MarketplaceCertificateComponent implements OnInit {
  @Input() data: any;
  public profilePictureSrc = '../../../assets/job-user.svg';
  public emptyCertificateProfile = '../../../assets/job-user.svg';
  public imageUrl;
  public displayItems = [];

  constructor(
    private sanitizer: DomSanitizer,
    private _ModalController: ModalController,
    public _BarcodeService: BarcodeService,
    private _AlertController: AlertController,
    private _LoaderProviderService: LoaderProviderService,
    private _SecureStorageService: SecureStorageService,
    private _TranslateProviderService: TranslateProviderService
  ) { }

  ngOnInit() {
    var keys = Object.keys(this.data.input.credentialSubject.data[0])
    // console.log(keys);
    this.imageUrl = this.data.input.imageBlob ? this.sanitizer.bypassSecurityTrustUrl(this.data.input.imageBlob) : this.emptyCertificateProfile;
    keys = keys.filter(k => !['logo', 'header', 'companyName'].includes(k))
    keys.forEach(k => {
      var tem = Object.assign({}, {key: k, value: this.data.input.credentialSubject.data[0][k]});
      if(['issueDate', 'expiryDate', 'startDate', 'endDate'].includes(k)) {
        const datePipe = new DatePipe('en-US');
        tem.value = datePipe.transform(tem.value, 'dd.MM.yy HH:mm');
      }
      // console.log(tem);
      this.displayItems.push(tem);
      // console.log(this.displayItems);
    })
  }

  close() {
    this._ModalController.dismiss();
  }

  show() {
    this._AlertController.create({
      mode: 'ios',
      header: this._TranslateProviderService.instant('SIGNATURE.signature'),
      message: JSON.stringify(this.data.input.proof),
      buttons: [{
        text: this._TranslateProviderService.instant('GENERAL.ok'),
        role: 'cancel',
        handler: () => {}
      }]
    }).then(alert => alert.present())
  }

  delete() {
    var original = this.data.input;
    delete original.imageBlob;
    delete original.proof;
    this._AlertController.create({
      mode: 'ios',
      header: this._TranslateProviderService.instant('HEALTHCERTIFICATE.deleteCertificate.title'),
      message: this._TranslateProviderService.instant('HEALTHCERTIFICATE.deleteCertificate.message'),
      buttons: [{
        text: this._TranslateProviderService.instant('SETTINGS.yes'),
        handler: async () => {
          await this._LoaderProviderService.loaderCreate();
          this._BarcodeService.scannedCertificate = this._BarcodeService.scannedCertificate.filter(k => JSON.stringify(k) != JSON.stringify(original));
          // console.log(this._BarcodeService.scannedCertificate);
          await this._SecureStorageService.setValue(SecureStorageKey.marketplaceCertificates, JSON.stringify(this._BarcodeService.scannedCertificate));
          await this._LoaderProviderService.loaderDismiss();
          await this._ModalController.dismiss();
        }
      },{
        text: this._TranslateProviderService.instant('SETTINGS.no'),
        role: 'cancel',
        handler: () => {}
      }]
    }).then(alert => alert.present())
  }

}
