import { NgModule } from '@angular/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { MarketplaceCertificateComponent } from './marketplace-certificate.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';

@NgModule({
  imports: [
    CommonModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
  ],
  providers: [
    SafariViewController,
    File,
    FileOpener
  ],
  declarations: [MarketplaceCertificateComponent],
//   entryComponents: [MarketplaceCertificateComponent],
  exports: [MarketplaceCertificateComponent]
})
export class MarketplaceCertificateModule {}
