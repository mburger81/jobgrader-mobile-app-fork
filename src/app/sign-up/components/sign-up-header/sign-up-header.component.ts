import { Router } from '@angular/router';
import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { bufferWhen, debounceTime, filter, map, scan, take, tap, timeInterval } from 'rxjs/operators';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { IonIcon, NavController } from '@ionic/angular';

@Component({
    selector: 'proto-sign-up-header',
    templateUrl: './sign-up-header.component.html',
    styleUrls: ['./sign-up-header.component.scss'],
})
export class SignUpHeaderComponent implements AfterViewInit {
    @Input('progressBar') public progressBar = true;
    @Input('percentage') public percentage = 15;
    @Input('onGoBack') public goBack: any;
    @Input('iconName') public iconName: string = null;
    @Input('title') public title: string = null;
    @Input() public easter = false;
    @ViewChild('helix') helix: any;

    constructor(
        private nav: NavController,
        private signProviderService: SignProviderService,
        private router: Router
    ) {
    }

    /** @inheritDoc */
    public ngAfterViewInit(): void {
        if ( this.easter ) {
            const single$ = fromEvent(this.helix.el, 'click');
            single$.pipe(
                bufferWhen(() => single$.pipe(debounceTime(500))),
                map(list => {
                    return list.length;
                }),
                tap(count => console.log(count)),
                filter(length => length >= 3),
                take(1)
            ).subscribe(_ => {
                this.signProviderService.unbrandDeviceFromUser().then(__ => {
                    this.nav.navigateRoot('/sign-up');
                });
            });
        }
    }

}
