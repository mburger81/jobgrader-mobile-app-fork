import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../../shared/shared.module';
import { SignUpStepNinePage } from './step-9.page';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';


const routes: Routes = [
  {
    path: '',
    component: SignUpStepNinePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    SignUpStepNinePage
  ],
  providers: [
    SafariViewController
  ]
})
export class SignUpStepNinePageModule {}
