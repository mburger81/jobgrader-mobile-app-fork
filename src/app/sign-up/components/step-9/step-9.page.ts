import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Platform, ToastController, NavController, IonContent } from '@ionic/angular';

import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { PersonalInformation } from '../../../core/models/SignUp';
import { Keyboard } from '@capacitor/keyboard';
import { Capacitor } from '@capacitor/core';

@Component({
    selector: 'app-step-9',
    templateUrl: './step-9.page.html',
    styleUrls: ['./step-9.page.scss']
})
export class SignUpStepNinePage implements OnInit {
    @ViewChild(IonContent, {}) content: IonContent;
    signForm: UntypedFormGroup;
    signProcessing = false;
    selectedLanguage = 'en';

    private ngOnInitExecuted: any;
    private browser: InAppBrowserObject;
    legalResponse = {
        termsandconditions: {
          en: 'https://jobgrader.app/app-termsofuse', // at settings and onboarding
        //   de: 'https://jobgrader.app/app-nutzungsbedingungen'
        },
        dataprivacypolicy: {
          en: 'https://jobgrader.app/app-privacy-policy', // at settings and onboarding
        //   de: 'https://jobgrader.app/app-datenschutzerklaerung'
        },
        gdpr:  'https://jobgrader.app/user-gdpr-compliance', // at onboarding
        thirdparty: {
          en: 'https://jobgrader.app/app-datause', // at onboarding
        //   de: 'https://jobgrader.app/app-datennutzung'
        },
        imprint: {
          en: 'https://jobgrader.app/app-imprint',
        //   de: 'https://jobgrader.app/app-impressum'
        }
    };
    browserOptions = 'zoom=no,footer=no,hideurlbar=yes,footercolor=#BF7B54,hidenavigationbuttons=yes,presentationstyle=pagesheet';
    // browserOptions = 'footer=yes,hideurlbar=no,footercolor=#BF7B54,hidenavigationbuttons=no,usewkwebview=yes,presentationstyle=pagesheet'
    // formsheet or fullscreen

    iabTermsConditions() {
    //   console.log(this.selectedLanguage);
      const url = this.legalResponse.termsandconditions[`${this.selectedLanguage}`];
    //   console.log(url);
      if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
            this.showSafariInstance(url);
        } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system', this.browserOptions);
        } else if ( !this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system');
        }
    }

    iabDataPrivacy() {
      const url = this.legalResponse.dataprivacypolicy[`${this.selectedLanguage}`];
      console.log(url);
      if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
            this.showSafariInstance(url);
        } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system', this.browserOptions);
        } else if ( !this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system');
        }
    }

    iabGdpr() {
        if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
            this.showSafariInstance(this.legalResponse.gdpr);
        } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
            this.browser = this.iab.create(this.legalResponse.gdpr, '_system', this.browserOptions);
        } else if ( !this.platform.is('hybrid') ) {
            this.browser = this.iab.create(this.legalResponse.gdpr, '_system', this.browserOptions);
        }
    }

    iabThirdParty() {
      const url = this.legalResponse.thirdparty[`${this.selectedLanguage}`];
      console.log(url);
      if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
            this.showSafariInstance(url);
        } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system', this.browserOptions);
        } else if ( !this.platform.is('hybrid') ) {
            this.browser = this.iab.create(url, '_system');
        }
    }

    showSafariInstance(url: string) {
        this.safariViewController.isAvailable().then((available: boolean) => {
                if ( available ) {
                    this.safariViewController.show({
                        url,
                        hidden: false,
                        animated: true,
                        transition: 'slide',
                        enterReaderModeIfAvailable: false,
                        tintColor: '#54BF7B'
                    })
                        .subscribe((result: any) => {
                            },
                            (error: any) => console.error(error));
                } else {
                    this.browser = this.iab.create(url, '_self', this.browserOptions);
                }
            }
        );
    }

    constructor(
        // private camera: Camera,
        private nav: NavController,
        private cdr: ChangeDetectorRef,
        private toastController: ToastController,
        private translateProviderService: TranslateProviderService,
        public signService: SignProviderService,
        private iab: InAppBrowser,
        private safariViewController: SafariViewController,
        private platform: Platform
    ) {

    }


    async ionViewWillEnter() {
        this.content.scrollToTop();
        if (!this.ngOnInitExecuted) {
            await this.componentInit();
        }
    }

    async ngOnInit() {
        this.ngOnInitExecuted = true;
        await this.componentInit();
    }

    async ionViewDidLeave() {
        this.ngOnInitExecuted = false;
    }

    async nextStep() {
        if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
        this.signForm.markAsDirty();

        this.signProcessing = true;
        if ( !this.signForm.valid ) {
            this.presentToast();
            return false;
        }

        await this.signService.savePersonalData(this.signForm.value, 9);
        this.goToLastPage(true);
    }

    private goToLastPage(regSuccess: boolean) {
        this.signService.personalInfoSuccess = regSuccess;
        this.signProcessing = false;
        this.nav.navigateForward('/sign-up/step-2');
    }

    goToPreviousStep(): void {
        this.nav.navigateBack('/sign-up/step-1');
    }

    async skipReg() {
        await this.signService.skipReg(9);
    }

    private async componentInit() {
        this.signForm = new UntypedFormGroup({
            termsConditions: new UntypedFormControl(null, [
                Validators.requiredTrue
            ]),
            dataPrivacy: new UntypedFormControl(null, [
                Validators.requiredTrue
            ]),
            // accessThirdParty: new UntypedFormControl(null, [
            //     Validators.requiredTrue
            // ]),

            // "termsConditions": "I agree with the terms and conditions",
            // "dataPrivacy": "I agree with the data privacy policy",
            // "gdpr": "I consent with blockchain helix's compliance to GDPR",
            // "accessThirdParty": "I agree that third party applications can process and store my data.",
            // "later": "do it later"
            // issuePlace: new UntypedFormControl('', [
            //   Validators.required
            // ]),
            // issueAgency: new UntypedFormControl('', [
            //   Validators.required
            // ]),
            // scannedImage: new UntypedFormControl('')
            // // scannedImage: new UntypedFormControl('', [
            // //   Validators.required
            // // ])
        });
        this.selectedLanguage = await this.translateProviderService.getLangFromStorage();
        const dataFromStorage: PersonalInformation = await this.signService.getPersonalData();

        this.setFormDataFromStorage(dataFromStorage);
        setTimeout(() => this.detectChanges());
    }

    private setFormDataFromStorage(data: PersonalInformation): void {
        // if (data && data.document_issue_place) {
        //   this.signForm.controls.issuePlace.setValue(data.document_issue_place);
        // }
        // if (data && data.document_issue_agency) {
        //   this.signForm.controls.issueAgency.setValue(data.document_issue_agency);
        // }
        // if (data && data.document_scanned_image) {
        //   this.signForm.controls.scannedImage.setValue(data.document_scanned_image);
        // }
    }

    async presentToast() {
        // const translations = {
        //   en: 'All fields should be checked',
        //   de: 'All fields should be checked',
        //   hi: 'All fields should be checked'
        // };

        const toast = await this.toastController.create({
            message: this.translateProviderService.instant('SIGNSTEPNINE.checkToast'),
            duration: 2000,
            position: 'top',
        });
        toast.present();
    }

    private detectChanges() {
        if (!this.cdr['destroyed']) {
            this.cdr.detectChanges();
        }
    }

}
