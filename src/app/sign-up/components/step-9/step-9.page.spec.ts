import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SignUpStepNinePage } from './step-9.page';

describe('SignUpStepNinePage', () => {
  let component: SignUpStepNinePage;
  let fixture: ComponentFixture<SignUpStepNinePage>;

 beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpStepNinePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpStepNinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
