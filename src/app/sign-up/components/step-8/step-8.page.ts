import { ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController, ModalController, NavController, IonContent } from '@ionic/angular';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { PersonalInformation } from '../../../core/models/SignUp';
import { AuthenticationProviderService } from '../../../core/providers/authentication/authentication-provider.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { Keyboard } from '@capacitor/keyboard';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { User } from '../../../core/models/User';
import * as moment from 'moment';
import { TimesService } from '../../../core/providers/times/times.service';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { AlertsProviderService } from '../../../core/providers/alerts/alerts-provider.service';
import { DocumentSelectorComponent } from './document-selector/document-selector.component';
import { Capacitor } from '@capacitor/core';
import { UserPhotoServiceAkita } from '../../../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-step-8',
  templateUrl: './step-8.page.html',
  styleUrls: ['./step-8.page.scss']
})
export class SignUpStepEightPage implements OnInit {
  @ViewChild(IonContent, {}) content: IonContent;
  signForm: UntypedFormGroup;
  signProcessing = false;
  private ngOnInitExecuted: any;

  avaliableDocuments: any[] = [
    {
      label: this.translateProviderService.instant('PERSONALDETAILS.radioPassport'),
      value: 'PASSPORT',
    },
    {
      label: this.translateProviderService.instant('PERSONALDETAILS.radioIDCard'),
      value: 'ID_CARD'
    },
    {
      label: this.translateProviderService.instant('PERSONALDETAILS.radioResidencePermit'),
      value: 'RESIDENCE_PERMIT'
    },
    {
      label: this.translateProviderService.instant('PERSONALDETAILS.radioDL'),
      value: 'DRIVERS_LICENSE'
    },
  ];

  public documentDisplayArray = [];

  constructor(
    private nav: NavController,
    public timesService: TimesService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    public signService: SignProviderService,
    public authenticationProviderService: AuthenticationProviderService,
    public secureStore: SecureStorageService,
    private translateProviderService: TranslateProviderService,
    public apiProviderService: ApiProviderService,
    private toastController: ToastController,
    private loader: LoaderProviderService,
    private alertService: AlertsProviderService,
    private _ModalController: ModalController,
    private _NetworkService: NetworkService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) { }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }


  async nextStep() {
    await this.loader.loaderCreate();
    if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
    this.signForm.markAsDirty();
    this.signProcessing = true;
    if (!this.signForm.valid) {
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.fillToast'));
      await this.loader.loaderDismiss();
      return false;
    }

    // Save personal data from this page
    await this.signService.savePersonalData(this.signForm.value, 8);

    if (!this._NetworkService.checkConnection()) {
      await this.loader.loaderDismiss();
      this._NetworkService.addOfflineFooter('ion-footer');
      return; } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
      }

    // Finally save user data
    await this.signService.provideUserDataUpdate()
    .then(() => {
      console.info('Successfully provided user data update!');
    })
    .catch(err => {
      console.info(`Error providing user data update: ${JSON.stringify(err)}.`);
    });

    const userString: string = await this.secureStore.getValue(SecureStorageKey.userData, false);
    const user: User = userString ? JSON.parse(userString) : {};
    // console.log(userString);
    await this.signService.savePersonalData(this.signForm.value, 8);
    const userResponse = await this.apiProviderService
        .getUserById(await this.authenticationProviderService.getBasicAuthToken(), user.userid);

    if (userResponse) {
      await this.secureStore.setValue(SecureStorageKey.userData, JSON.stringify(userResponse));
      const userPhotosResponse =
          await this.apiProviderService.getUserPhotos(await this.authenticationProviderService.getBasicAuthToken(), userResponse.userid);
      let userPhotoId = null;
      try {
        userPhotoId = Object.keys(userPhotosResponse)[0];
      } catch (e) {
        console.log(e);
      }
      if (userPhotoId) {
        const userPhotoResponse = `/user/basicdata/getimage/${userResponse.id}?imageid=${userPhotoId}`;
        this.userPhotoServiceAkita.updatePhoto(userPhotoResponse);
      }
    }
    await this.loader.loaderDismiss();
    this.signProcessing = false;
    this.nav.navigateForward('/sign-up/step-10');
  }

  prevStep() {
    this.nav.navigateBack('/sign-up/step-7');
  }

  async skipReg() {
    await this.signService.skipReg(8);
  }

  private async componentInit() {
    this.signForm = new UntypedFormGroup({
      dlNumber: new UntypedFormControl('', []),
      dlIssueDate: new UntypedFormControl('', []),
      dlExpiryDate: new UntypedFormControl('', []),
      dlIssueCountry: new UntypedFormControl('', []),
      idCardNumber: new UntypedFormControl('', []),
      idCardIssueDate: new UntypedFormControl('', []),
      idCardExpiryDate: new UntypedFormControl('', []),
      idCardIssueCountry: new UntypedFormControl('', []),
      passportNumber: new UntypedFormControl('', []),
      passportIssueDate: new UntypedFormControl('', []),
      passportExpiryDate: new UntypedFormControl('', []),
      passportIssueCountry: new UntypedFormControl('', []),
      residencePermitNumber: new UntypedFormControl('', []),
      residencePermitIssueDate: new UntypedFormControl('', []),
      residencePermitExpiryDate: new UntypedFormControl('', []),
      residencePermitIssueCountry: new UntypedFormControl('', [])
    });
    const dataFromStorage: PersonalInformation = await this.signService.getPersonalData();

    this.setFormDataFromStorage(dataFromStorage);
    setTimeout(() => this.detectChanges());
  }

  async presentToast(message: string) {

    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  private setFormDataFromStorage(data: PersonalInformation): void {
    // console.log(data)
    if (data && data.passport_number) {
      this.signForm.controls.passportNumber.setValue(data.passport_number);
    }
    if (data && data.passport_issue_date) {
      this.signForm.controls.passportIssueDate.setValue(data.passport_issue_date);
    }
    if (data && data.passport_expiry_date) {
      this.signForm.controls.passportExpiryDate.setValue(data.passport_expiry_date);
    }
    if (data && data.passport_issue_country) {
      this.signForm.controls.passportIssueCountry.setValue(data.passport_issue_country);
    }

    if (data && data.dl_number) {
      this.signForm.controls.dlNumber.setValue(data.dl_number);
    }
    if (data && data.dl_issue_date) {
      this.signForm.controls.dlIssueDate.setValue(data.dl_issue_date);
    }
    if (data && data.dl_expiry_date) {
      this.signForm.controls.dlExpiryDate.setValue(data.dl_expiry_date);
    }
    if (data && data.dl_issue_country) {
      this.signForm.controls.dlIssueCountry.setValue(data.dl_issue_country);
    }

    if (data && data.idcard_number) {
      this.signForm.controls.idCardNumber.setValue(data.idcard_number);
    }
    if (data && data.idcard_issue_date) {
      this.signForm.controls.idCardIssueDate.setValue(data.idcard_issue_date);
    }
    if (data && data.idcard_expiry_date) {
      this.signForm.controls.idCardExpiryDate.setValue(data.idcard_expiry_date);
    }
    if (data && data.idcard_issue_country) {
      this.signForm.controls.idCardIssueCountry.setValue(data.idcard_issue_country);
    }

    if (data && data.residence_permit_number) {
      this.signForm.controls.residencePermitNumber.setValue(data.residence_permit_number);
    }
    if (data && data.residence_permit_issue_date) {
      this.signForm.controls.residencePermitIssueDate.setValue(data.residence_permit_issue_date);
    }
    if (data && data.residence_permit_expiry_date) {
      this.signForm.controls.residencePermitExpiryDate.setValue(data.residence_permit_expiry_date);
    }
    if (data && data.residence_permit_issue_country) {
      this.signForm.controls.residencePermitIssueCountry.setValue(data.residence_permit_issue_country);
    }

    var passportUpdate = data.passport_number && data.passport_issue_date && data.passport_expiry_date && data.passport_issue_country
    var dlUpdate = data.dl_number && data.dl_issue_date && data.dl_expiry_date && data.dl_issue_country
    var residencePermitUpdate = data.residence_permit_number && data.residence_permit_issue_date && data.residence_permit_expiry_date && data.residence_permit_issue_country
    var idCardUpdate = data.idcard_number && data.idcard_issue_date && data.idcard_expiry_date && data.idcard_issue_country

    if (passportUpdate) {
        if(!this.documentDisplayArray.includes('PASSPORT')) {
          this.documentDisplayArray.push('PASSPORT')
        }
      } else {
        if(this.documentDisplayArray.includes('PASSPORT')) {
          this.documentDisplayArray = this.documentDisplayArray.filter(k => k != 'PASSPORT')
        }
      }

    if (dlUpdate) {
        if(!this.documentDisplayArray.includes('DRIVERS_LICENSE')) {
          this.documentDisplayArray.push('DRIVERS_LICENSE')
        }
      } else {
        if(this.documentDisplayArray.includes('DRIVERS_LICENSE')) {
          this.documentDisplayArray = this.documentDisplayArray.filter(k => k != 'DRIVERS_LICENSE')
        }
      }

    if (residencePermitUpdate) {
        if(!this.documentDisplayArray.includes('RESIDENCE_PERMIT')) {
          this.documentDisplayArray.push('RESIDENCE_PERMIT')
        }
      } else {
        if(this.documentDisplayArray.includes('RESIDENCE_PERMIT')) {
          this.documentDisplayArray = this.documentDisplayArray.filter(k => k != 'RESIDENCE_PERMIT')
        }
      }

    if (idCardUpdate) {
        if(!this.documentDisplayArray.includes('ID_CARD')) {
          this.documentDisplayArray.push('ID_CARD')
        }
      } else {
        if(this.documentDisplayArray.includes('ID_CARD')) {
          this.documentDisplayArray = this.documentDisplayArray.filter(k => k != 'ID_CARD')
        }
      }
    // console.log(this.documentDisplayArray)
  }

  @HostListener('document:click', ['$event', '$event.target'])
  public onClick(event: MouseEvent, targetElement: HTMLElement): void {
    if (!targetElement) {
        return;
    }

    const ids = ['select-flag-4'];
    if (ids.indexOf(targetElement.id) !== -1) {
      this.signService.addSelectBoxAdditionalElements();
    }
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

  async addDocument() {
    let buttonArray = [];
    for(let i of [{
      bool: this.documentDisplayArray.includes('PASSPORT'),
      methodArg: 'PASSPORT',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioPassport')
    },{
      bool: this.documentDisplayArray.includes('ID_CARD'),
      methodArg: 'ID_CARD',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioIDCard')
    },{
      bool: this.documentDisplayArray.includes('DRIVERS_LICENSE'),
      methodArg: 'DRIVERS_LICENSE',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioDL')
    },{
      bool: this.documentDisplayArray.includes('RESIDENCE_PERMIT'),
      methodArg: 'RESIDENCE_PERMIT',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioResidencePermit')
    }]){
      if(!i.bool){
        buttonArray.push(
          {
            text: i.document,
            handler: () => {
              // console.log(i.document + "selected");
              this.showModal(i.methodArg, false);
            }
          }
        )
      }
    }
    buttonArray.push({
      text: this.translateProviderService.instant('BUTTON.CANCEL'),
      role: 'cancel',
      handler: () => {
        console.log("cancelled");
      }
    })
    await this.alertService.alertCreate(this.translateProviderService.instant('SIGNSTEPEIGHT.documentType'),'','',buttonArray);
  }

  async showModal(header: string, showMode: boolean) {
    var additionalData: any = {}
    var translatedHeader: string = ''
    var transferFilledData = await this.signService.getPersonalData()
    switch(header) {
      case 'PASSPORT':
        additionalData.number = !showMode ? null : transferFilledData.passport_number
        additionalData.issuedate = !showMode ? null : transferFilledData.passport_issue_date
        additionalData.expirydate = !showMode ? null : transferFilledData.passport_expiry_date
        additionalData.issuecountry = !showMode ? null : transferFilledData.passport_issue_country
        translatedHeader = this.translateProviderService.instant('PERSONALDETAILS.radioPassport')
        break;
      case 'ID_CARD':
        additionalData.number = !showMode ? null : transferFilledData.idcard_number
        additionalData.issuedate = !showMode ? null : transferFilledData.idcard_issue_date
        additionalData.expirydate = !showMode ? null : transferFilledData.idcard_expiry_date
        additionalData.issuecountry = !showMode ? null : transferFilledData.idcard_issue_country
        translatedHeader = this.translateProviderService.instant('PERSONALDETAILS.radioIDCard')
        break;
      case 'DRIVERS_LICENSE':
        additionalData.number = !showMode ? null : transferFilledData.dl_number
        additionalData.issuedate = !showMode ? null : transferFilledData.dl_issue_date
        additionalData.expirydate = !showMode ? null : transferFilledData.dl_expiry_date
        additionalData.issuecountry = !showMode ? null : transferFilledData.dl_issue_country
        translatedHeader = this.translateProviderService.instant('PERSONALDETAILS.radioDL')
        break;
      case 'RESIDENCE_PERMIT':
        additionalData.number = !showMode ? null : transferFilledData.residence_permit_number
        additionalData.issuedate = !showMode ? null : transferFilledData.residence_permit_issue_date
        additionalData.expirydate = !showMode ? null : transferFilledData.residence_permit_expiry_date
        additionalData.issuecountry = !showMode ? null : transferFilledData.residence_permit_issue_country
        translatedHeader = this.translateProviderService.instant('PERSONALDETAILS.radioResidencePermit')
        break;
      default: break;
    }

    const modal = await this._ModalController.create({
      component: DocumentSelectorComponent,
      componentProps: {
        data: {
          documentType: header,
          translatedHeader: translatedHeader,
          showMode: showMode,
          additionalData: additionalData
        }
      }
    })
    modal.onDidDismiss().then(async () => {
      await this.processModalInfo()
      // this.nav.navigateRoot('sign-up/step-8');
    })
    await modal.present();
  }

  async processModalInfo() {
    var signData = await this.signService.getPersonalData()
    // console.log(signData)
    this.setFormDataFromStorage(signData)
  }


}
