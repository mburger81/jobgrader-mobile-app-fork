import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { DocumentSelectorComponent } from './document-selector.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { TooltipsModule } from 'ionic-tooltips';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        // TooltipsModule,
        SharedModule,
        TranslateModule.forChild()
    ],
    declarations: [DocumentSelectorComponent],
    exports: [DocumentSelectorComponent]
})
export class DocumentSelectorPageModule {}
