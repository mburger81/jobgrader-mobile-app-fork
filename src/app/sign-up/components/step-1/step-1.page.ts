import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { UntypedFormGroup, Validators, UntypedFormControl } from '@angular/forms';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { SignUp } from '../../../core/models/SignUp';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
// import { Keyboard } from '@capacitor/keyboard';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';


@Component({
  selector: 'app-step-1',
  templateUrl: './step-1.page.html',
  styleUrls: ['./step-1.page.scss']
})
export class SignUpStepOnePage implements OnInit {
  @ViewChild(IonContent, {}) content: IonContent;
  public displayName = 'Maxim';
  public step = 1;
  public progress = this.step/6;

  public displayRequirements = {
    firstName: false
  };

  signForm: UntypedFormGroup;
  verificationStatus: number;
  notValidForm = false;
  signProcessing = false;
  private ngOnInitExecuted: any;

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private toastController: ToastController,
    private translateProviderService: TranslateProviderService,
    private signService: SignProviderService,
    private secureStorage: SecureStorageService,
    private loader: LoaderProviderService,
    private _NetworkService: NetworkService
  ) { }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  public onFocus() {
    console.log('onFocus');
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async nextStep() {
    // await this.loader.loaderCreate();
    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();

    if (!this._NetworkService.checkConnection()) {
      // await this.loader.loaderDismiss();

      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    this.signForm.markAsDirty();
    this.signProcessing = true;

    if((this.signForm.controls.firstName.value).length >= 50) {
      this.presentToast(this.translateProviderService.instant('SIGNSTEPTWO.error'), 3000);
      // await this.loader.loaderDismiss();
      return false;
    }

    if (!this.signForm.valid) {
      this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.fillToast'), 3000);
      // await this.loader.loaderDismiss();
      return false;
    }

    await this.signService.saveSignData(this.signForm.value, 1);
    this.signProcessing = false;
    // await this.loader.loaderDismiss();
    return this.nav.navigateForward('/sign-up/step-2');
  }

  async goToPreviousStep() {
    try {
      this.signService.signUpForm = {};
      this.signService.personalInformation = {};
      await this.secureStorage.removeValue(SecureStorageKey.sign, false); // registration form 1
      await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false); // registration form 2
      await this.secureStorage.removeValue(SecureStorageKey.loginCheck, false); // avoid disappearance of registration button
      await this.secureStorage.removeValue(SecureStorageKey.passwordHash, false); // avoid disappearance of registration button
      await this.secureStorage.removeValue(SecureStorageKey.nonce, false); // avoid disappearance of registration button
    } catch (e) {
        console.log(e);
    }
    this.nav.navigateBack('/login?from=registration');
  }

  async skipReg() {
    this.signForm.reset();
    await this.signService.skipReg(1);
  }

  async presentToast(message: string, duration: number) {
    // const translations = {
    //   en: 'All fields should be checked',
    //   de: 'All fields should be checked',
    //   hi: 'All fields should be checked'
    // };

    const toast = await this.toastController.create({
      message,
      duration,
      position: 'top',
    });
    toast.present();
  }

  private async componentInit() {
    this.signForm = new UntypedFormGroup({
      firstName: new UntypedFormControl('', [
          Validators.required
      ])
    });
    const dataFromStorage: SignUp = await this.signService.getSignData();

    this.setFormDataFromStorage(dataFromStorage);
    setTimeout(() => this.detectChanges());
  }

  private setFormDataFromStorage(data: SignUp): void {
    if (data && data.first_name) {
      this.signForm.controls.firstName.setValue(data.first_name);
    }
  }

  goToBrowseMode() {
    // this.secureStorage.setValue(SecureStorageKey.browseMode, true.toString()).then(() => {
      this.nav.navigateForward('/dashboard/tab-home')
    // })
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
