import { ApiProviderService } from './../../../core/providers/api/api-provider.service';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { AuthenticationProviderService } from '../../../core/providers/authentication/authentication-provider.service';
import { SignUp } from '../../../core/models/SignUp';
// import { Keyboard } from '@capacitor/keyboard';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { CodeInputComponent } from 'angular-code-input';
import { DeviceService } from 'src/app/core/providers/device/device.service';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { Settings, SettingsService } from 'src/app/core/providers/settings/settings.service';
import { KeyRecoveryBackupService } from 'src/app/core/providers/key-recovery-backup/key-recovery-backup.service';


@Component({
  selector: 'app-step-6',
  templateUrl: './step-6.page.html',
  styleUrls: ['./step-6.page.scss']
})
export class SignUpStepSixPage implements OnInit {

  @ViewChild(IonContent, {}) content: IonContent;
  @ViewChild('codeInput') codeInput: CodeInputComponent;
  public step = 6;
  public progress = this.step/6;

  signForm: UntypedFormGroup;
  signProcessing = false;
  selectedLanguage = 'en';
  otpValid = false;
  private ngOnInitExecuted: any;
  notUniqueUsername = false;
  email = '';

  public visibility = {
    input: true
  }

  isButtonDisabled: boolean = true;

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private toastController: ToastController,
    private signService: SignProviderService,
    private translateProviderService: TranslateProviderService,
    private authenticationProviderService: AuthenticationProviderService,
    private loaderProviderService: LoaderProviderService,
    private apiService: ApiProviderService,
    private _DeviceService: DeviceService,
    private _CryptoProviderService: CryptoProviderService,
    private _KeyRecoveryBackupService: KeyRecoveryBackupService,
    private secureStorageService: SecureStorageService,
    private settingsService: SettingsService,
    private _NetworkService: NetworkService
  ) { }


  async ionViewWillEnter() {
    this.content.scrollToTop();
    this.isButtonDisabled = true;
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  async nextStep() {

    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();

    if(!this.signForm.value.otp || this.signForm.value.otp == "") {
      return this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.fillToast'));
    }

    // console.log(this.signForm);

    this.signForm.markAsDirty();
    this.signProcessing = true;
    // if (!this.signForm.valid) {
    //   this.codeInput.reset();
    //   return false;
    // }

    // Get email from local storage
    const {email_id} = await this.signService.getSignData();

    // Get entered OTP value from form
    const OTP = this.signForm.value.otp;

    if (!this._NetworkService.checkConnection()) {
      this.codeInput.reset();
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
      }

    // Validate OTP
    this.apiService.validateOtp(OTP, email_id).then(async otpIsValid => {

      console.log("otpIsValid");
      console.log(otpIsValid);

      if (otpIsValid.errorFound) {
        this.codeInput.reset();
        console.error('The entered OTP is invalid.');
        return await this.presentToast(this.translateProviderService.instant('SIGNSTEPFOUR.otpError'));
      }

      this.signService.registerSuccess = true;

      // All good, create user
      await this.signService.saveSignData(this.signForm.value, 6)
          .then(async () => {
            // await this.loaderProviderService.loaderCreate();
            // console.log('Intitiating Intial Onboarding');
            // await this.signService.provideInitialOnboarding();
            // console.log('Intitiating User Login');
            // const signDataString = await this.secureStorageService.getValue(SecureStorageKey.sign, false);
            // console.log("signDataString", signDataString);
            // const signData = JSON.parse(signDataString);
            // console.log("signData", signData);
            // await this.signService.brandDeviceToUser(signData.username, signData.password);
            // await this.loginUser();
            // console.log("await this.loginUser();");
            // await this._DeviceService.obtainUDID();
            // console.log("await this._DeviceService.obtainUDID();");
            // var success = this.signService.checkIsSuccess();
            // var loginSuccess = (await this.secureStorageService.getValue(SecureStorageKey.loginStatusAfterSignup, false) === 'true');
            // console.log('Sign Up (/user/create) Success? ' + success);
            // console.log('Login (/user) after Sign Up? ' + loginSuccess);

            // if (!loginSuccess) {
            //   await this.presentToast(this.translateProviderService.instant('SIGNSTEPFIVE.loginStatus'));
            //   await this.loaderProviderService.loaderDismiss();
            //   return;
            // }

            // // await this.registrationProcess1();
            // // await this.registrationProcess2();


            // await this._CryptoProviderService.generateUserKeyPair(); // Process 1
            // console.log("await this._CryptoProviderService.generateUserKeyPair();");
            // await this._CryptoProviderService.generateDeviceEncryptionKeyPair(); // Process 2
            // console.log("await this._CryptoProviderService.generateDeviceEncryptionKeyPair();");
            // // await this.didCommService.generateChatKeyPair(); // Process 3
            // // console.log("await this.didCommService.generateChatKeyPair();");
            // await this._DeviceService.pushDeviceInfo(); // Process 2
            // console.log("await this._DeviceService.pushDeviceInfo();");
            // await this._KeyRecoveryBackupService.sendUserMnemonic(); // Process 1
            // console.log("await this._KeyRecoveryBackupService.sendUserMnemonic();");
            // await this._KeyRecoveryBackupService.sendUserPublicEthereumAddress(); // Process 1
            // console.log("await this._KeyRecoveryBackupService.sendUserPublicEthereumAddress();");
            // // await this.registrationProcess4();
            // await this.settingsService.set(Settings.language, await this.translateProviderService.getLangFromStorage() , true); // Process 4
            // await this.settingsService.set(Settings.wizardNotRun, true.toString(), true); // Process 4
            // await this.secureStorageService.removeValue(SecureStorageKey.loginStatusAfterSignup, false); // Process 4
            // await this.secureStorageService.removeValue(SecureStorageKey.kycStatus, false); // Process 4
            // await this.secureStorageService.removeValue(SecureStorageKey.did, false); // Process 4
            // await this.secureStorageService.removeValue(SecureStorageKey.didDocument, false); // Process 4
            // await this.secureStorageService.removeValue(SecureStorageKey.verifiableCredentialEncrypted, false); // Process 4

            // await this._KeyRecoveryBackupService.pushKeys(); // Process 2
            // console.log("await this._KeyRecoveryBackupService.pushKeys();");
            // // await this.signService.pushChatKeyPairs();
            // // console.log("await this.signService.pushChatKeyPairs();");

            // // await this.presentToast(this.translateProviderService.instant('LOCALNOTIFICATION.heading'));
            // await this.secureStorageService.setValue(SecureStorageKey.faioEnabled, 'true');  // Process 5
            // var storedPin = await this.secureStorageService.getValue(SecureStorageKey.securePINEncrypted, false); // Process 5
            // if( storedPin ) {
            //   await this._SecurePinService.pushEncryptedPIN(storedPin);
            //   var encryptedPin = await this._SecurePinService.encryptPIN(storedPin);
            //   await this.secureStorageService.setValue(SecureStorageKey.securePINEncrypted, encryptedPin);
            // }
            // await this._UserActivity.updateActivity(ActivityKeys.USER_CREATED); // Process 6
            // await this.loaderProviderService.loaderDismiss();

            // this.lockService.addResume();

            // this.signProcessing = false;
            // this.signService.signUpForm = {};
            // this.signService.personalInformation = {};

            this.nav.navigateForward('/sign-up/step-7');

          })
          .catch(async (err) => {
            this.signService.registerSuccess = false;
            this.codeInput.reset();
            // await this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));
            await this.loaderProviderService.loaderDismiss();
            this.signProcessing = false;
            this.signService.signUpForm = {};
            this.signService.personalInformation = {};
            this.nav.navigateBack('/sign-up/step-7');
          });
    }, async error => {
      this.isButtonDisabled = true;
      this.codeInput.reset();
      console.error('The entered OTP is invalid.');
      return await this.presentToast(this.translateProviderService.instant('SIGNSTEPFOUR.otpError'));
    });
  }

  registrationProcess1(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._CryptoProviderService.generateUserKeyPair().then(() => {
        Promise.all([
          this._KeyRecoveryBackupService.sendUserPublicEthereumAddress(),
          this._KeyRecoveryBackupService.sendUserMnemonic(),
        ]).then(() => {
          resolve();
        })
      })
    })
  }

  registrationProcess2(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._DeviceService.obtainUDID().then(() => {
        this._CryptoProviderService.generateDeviceEncryptionKeyPair().then(() => {
          Promise.all([
            this._DeviceService.pushDeviceInfo(),
            this._KeyRecoveryBackupService.pushKeys(),
          ]).then(() => {
            resolve();
          })
        })
      })
    })
  }

  registrationProcess4(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.translateProviderService.getLangFromStorage().then(language => {
        Promise.all([
          this.settingsService.set(Settings.language, language , true),
          this.settingsService.set(Settings.wizardNotRun, true.toString(), true),
          this.secureStorageService.removeValue(SecureStorageKey.loginStatusAfterSignup, false),
          this.secureStorageService.removeValue(SecureStorageKey.kycStatus, false),
          this.secureStorageService.removeValue(SecureStorageKey.did, false),
          this.secureStorageService.removeValue(SecureStorageKey.didDocument, false),
          this.secureStorageService.removeValue(SecureStorageKey.verifiableCredentialEncrypted, false),
        ]).then(() => {
          resolve();
        })
      }).catch(e => {
        Promise.all([
          this.settingsService.set(Settings.wizardNotRun, true.toString(), true),
          this.secureStorageService.removeValue(SecureStorageKey.loginStatusAfterSignup, false),
          this.secureStorageService.removeValue(SecureStorageKey.kycStatus, false),
          this.secureStorageService.removeValue(SecureStorageKey.did, false),
          this.secureStorageService.removeValue(SecureStorageKey.didDocument, false),
          this.secureStorageService.removeValue(SecureStorageKey.verifiableCredentialEncrypted, false),
        ]).then(() => {
          resolve();
        })
      })
    })


  }

  private async loginUser() {
    const dataFromStorage: SignUp = await this.signService.getSignData();
    console.log(JSON.stringify(dataFromStorage));
    await this.authenticationProviderService.loginUsernameAndPassword(dataFromStorage.username, dataFromStorage.password, false);
  }

  goToPreviousStep(): void {
    // this.codeInput.reset();
    this.nav.navigateBack('/sign-up/step-5');
  }

  async skipReg() {
    this.codeInput.reset();
    await this.signService.skipReg(4);
  }

  async resendOtp() {
    console.log('resendOtp');

    // Get email from local storage
    const {first_name, email_id} = await this.signService.getSignData();
    const callname = first_name;
    const email = email_id;

    this.apiService.validateEmail(email, callname)
    .then(async (res) => {
      // console.log(`Email validation successful: ${JSON.stringify(res)}`);

      const toast = await this.toastController.create({
        message: this.translateProviderService.instant('SIGNSTEPFOUR.newOTP'),
        duration: 2000,
        position: 'top',
      });
      toast.present();
    });

  }

  private setFormDataFromStorage(data: SignUp): void {
    if (data && data.OTP) {
      this.signForm.controls.otp.setValue(data.OTP);
    }
  }

  private async componentInit() {
    this.signForm = new UntypedFormGroup({
      otp: new UntypedFormControl('', [
        Validators.required,
        this.validateOTP
      ])
    });
    this.selectedLanguage = await this.translateProviderService.getLangFromStorage();
    const dataFromStorage: SignUp = await this.signService.getSignData();

    this.setFormDataFromStorage(dataFromStorage);
    await this.signService.getSignData().then(res => { console.log(res.email_id); this.email = res.email_id; });
    setTimeout(() => this.detectChanges());
  }
  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

  public onCodeChanged(code: string) {
    // console.log(code);
    this.signForm.value.otp = code;
  }

  public onCodeCompleted(code: string) {
    // console.log(code);
    this.signForm.value.otp = code;
    this.isButtonDisabled = false;
  }

  public togglePINVisibility(input) {
    this.visibility[input] = !this.visibility[input];
  }


  private validateOTP(formControl: UntypedFormControl) {
    var el: any = document.getElementById('txtOtp');
    var isValid: boolean = false;

    if (el) {
      var otp = el.value;
      isValid = (otp != null && otp.length >= 6);
    }

    return (isValid) ? null : {
      validateOTP: {
        valid: false
      }
    };

  }
}
