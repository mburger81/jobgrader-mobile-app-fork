import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CodeInputModule } from 'angular-code-input';

import { SharedModule } from '../../../shared/shared.module';
import { SignUpStepSixPage } from './step-6.page';

const routes: Routes = [
  {
    path: '',
    component: SignUpStepSixPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule,
    CodeInputModule
  ],
  declarations: [SignUpStepSixPage],

})
export class SignUpStepSixPageModule {}
