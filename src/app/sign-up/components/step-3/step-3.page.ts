import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormGroup, Validators, UntypedFormControl } from '@angular/forms';
import { ToastController, NavController, IonContent, Platform } from '@ionic/angular';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { SignUp } from '../../../core/models/SignUp';
import { BiometricService } from '../../../core/providers/biometric/biometric.service';
import { AlertsProviderService } from '../../../core/providers/alerts/alerts-provider.service';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { TranslateService } from '@ngx-translate/core';
// import { Keyboard } from '@capacitor/keyboard';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import * as zxcvbn from 'zxcvbn';
import { PasswordStrengthInfo } from 'src/app/model/model/passwordStrengthInfo';
import { take, switchMap } from 'rxjs/operators';
import { FingerprintAIO, FingerprintOptions } from '@awesome-cordova-plugins/fingerprint-aio/ngx';
import { UtilityService } from 'src/app/core/providers/utillity/utility.service';

@Component({
  selector: 'app-step-3',
  templateUrl: './step-3.page.html',
  styleUrls: ['./step-3.page.scss']
})
export class SignUpStepThreePage implements OnInit {

  @ViewChild(IonContent, {}) content: IonContent;
  public step = 3;
  public progress = this.step/6;

  fingerprintOptions: FingerprintOptions = {
    title: 'Jobgrader', // (Android Only) | optional | Default: "<APP_NAME> Biometric Sign On"
    subtitle: 'home of digital identity', // (Android Only) | optional | Default: null
    description: 'Please Authenticate yourself',  // optional | Default: null
    fallbackButtonTitle: 'Use Pin' // optional | When disableBackup is false defaults to "Use Pin".
  };

  public displayRequirements = {
    password: false,
    password1: false
  };

  signForm = new UntypedFormGroup({
    password: new UntypedFormControl('', [
      Validators.required,
      this.validatePassword
      ])
    }, {
      validators: this.passwordConfirming
  })
  verificationStatus: number;
  notValidForm = false;
  signProcessing = false;
  private ngOnInitExecuted: any;

  public passwordStrengthInfo: PasswordStrengthInfo = {
    password1Strength: 0,
    password1StrengthText: this.translateProviderService.instant('LEANWALKTHROUGH.password.weak'),
    password1Type: "password",
    password2Strength: 0,
    password2StrengthText: "",
    password2Type: "password"
  };

  /** Model value for the toggle */
  public toggleValue = false;
  /** View model if faio is available */
  public faioIsAvailable = false;

  constructor(
    public platform: Platform,
    private nav: NavController,
    public biometricService: BiometricService,
    private toastController: ToastController,
    private alertService: AlertsProviderService,
    private translateProviderService: TranslateProviderService,
    private translateService: TranslateService,
    private signService: SignProviderService,
    private secureStorage: SecureStorageService,
    private loader: LoaderProviderService,
    private _FingerprintAIO: FingerprintAIO,
    private _NetworkService: NetworkService,
    private cdr: ChangeDetectorRef,
  ) { }


  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  public onFocus() {
    console.log('onFocus');
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();

  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  public async storeFaio(value: boolean): Promise<void> {
    // if (this.platform.is('android')) {
      // mburger: idk why we need to check for the permissions first on android
      // await this.getPermission();
    // }
    try {
      await this.secureStorage.setValue(SecureStorageKey.faioEnabled, value.toString());
    } catch (e) {
      try {
        await this.alertService.alertCreate(
            this.translateService.instant('GENERAL.error'),
            null,
            this.translateService.instant(
                'SECURITY.error-storing-message',
                {faio: this.translateService.instant('SECURITY.faio_' + (this.platform.is('android') ? 'android' : 'ios'))}),
            [{
              text: this.translateService.instant('GENERAL.ok'),
              role: 'Cancel'
            }]
        );
      } catch (e) {
        throw new Error('There was an error with opening the alert message in the security page');
      }
    }
  }

  // private getPermission(): Promise<any> {
  //   return this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BIOMETRIC).then(
  //       noop,
  //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BIOMETRIC)
  //   );
  // }

  async nextStep() {
    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
    // await this.loader.loaderCreate();

    if (!this._NetworkService.checkConnection()) {
      await this.loader.loaderDismiss();

      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    this.signForm.markAsDirty();
    this.signProcessing = true;

    if (this.signForm.value.password === '' || !this.signForm.value.password) {
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.REQUIREMENTS.FILLALL'), 3000);
      await this.loader.loaderDismiss();
      return false;
    }

    const passwordIsValid = this.signForm.get('password').valid;
    if (!passwordIsValid) {
      console.error('Password does not comply to our policies.');
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.PASSWORD'), 3000);
      await this.loader.loaderDismiss();
      return false;
    }

    if (this.passwordStrengthInfo.password1Strength < 2) {
      console.error('Password is too weak.');
      await this.presentToast(this.translateProviderService.instant('PASSWORDSTRENGTH.error_too_weak'), 3000);
      await this.loader.loaderDismiss();
      return false;
    }

    await this.signService.saveSignData(this.signForm.value, 3);
    this.signProcessing = false;
    await this.loader.loaderDismiss();

    var isFaioEnabled = await this.secureStorage.getValue(SecureStorageKey.faioEnabled, false);

    if(isFaioEnabled == 'true') {
      UtilityService.setTimeout(500).pipe(switchMap(_ =>
        this.biometricService.biometricShow()),take(1)).subscribe(() => {
          // this.viewLockedContent = true;
      }, _ => {
        // this.viewLockedContent = false;
      });
    }

    return this.nav.navigateForward('/sign-up/step-4');
  }

  async goToPreviousStep() {
    this.nav.navigateBack('/sign-up/step-2');
  }

  async presentToast(message: string, duration: number) {
    const toast = await this.toastController.create({
      message,
      duration,
      position: 'top',
    });
    toast.present();
  }


  private validatePassword(formControl: UntypedFormControl) {
    const password = formControl.value;
    const hasEmptySpaces = password.match(/[\s]/);
    const hasBetweenMinAndMaxCharacters = password.match(/^.{8,25}$/);
    const hasBlockLetters = password.match(/[A-Z]/);
    const hasSmallLetter = password.match(/[a-z]/);
    const hasEitherUpperAndOrLowerCase = (hasBlockLetters !== null || hasSmallLetter !== null);
    const hasAccentedCharacters = password.match(/[\u00C0-\u024F\u1E00-\u1EFF]/);

    const isValid = (
      hasEmptySpaces === null &&
      hasBetweenMinAndMaxCharacters !== null &&
      hasEitherUpperAndOrLowerCase !== null &&
      hasAccentedCharacters == null
    );

    return (
      isValid
      ? null : {
        validatePassword: {
          valid: false
        }
      }
    );
  }

  checkPassword1Strength() {
    var control = this.signForm.controls.password;
    var score = zxcvbn(control.value).score;
    // if we don't confirm to our own validation, override the score to zero.
    score = (control.valid) ? score : 0;
    this.passwordStrengthInfo.password1Strength = score;
    this.passwordStrengthInfo.password1StrengthText = (score > 1) ? this.translateProviderService.instant('LEANWALKTHROUGH.password.strong') : this.translateProviderService.instant('LEANWALKTHROUGH.password.weak');
    this.getPassword1StrengthClass();
  }

  checkPassword2Strength() {
    var control = this.signForm.controls.passwordConfirm;
    var score = zxcvbn(control.value).score;
    // if we don't confirm to our own validation, override the score to zero.
    score = (control.valid) ? score : 0;
    this.passwordStrengthInfo.password2Strength = score;
    this.getPassword2StrengthClass();
  }

  getPassword1StrengthClass() {
    var obj = {
      val: this.passwordStrengthInfo.password1Strength,
      lbl: this.passwordStrengthInfo.password1StrengthText
    };

    var cssClass = this.getPasswordStrengthClass(obj, false);
    this.passwordStrengthInfo.password1StrengthText = (obj.val >= 2) ? this.translateProviderService.instant('LEANWALKTHROUGH.password.strong') : this.translateProviderService.instant('LEANWALKTHROUGH.password.weak');
    console.log(this.passwordStrengthInfo.password1StrengthText);
    return cssClass;
  }

  getPassword2StrengthClass() {
    var obj = {
      val: this.passwordStrengthInfo.password2Strength,
      lbl: this.passwordStrengthInfo.password2StrengthText
    };

    var cssClass = this.getPasswordStrengthClass(obj, true);
    this.passwordStrengthInfo.password2StrengthText = obj.lbl;
    return cssClass;
  }

  getPasswordStrengthClass(obj: any, overrideLabels: boolean) {
    console.log('getPasswordStrengthClass ===>', this.signForm.value.passwordConfirm);

    var className: string;
    var prefix: string = this.translateProviderService.instant('PASSWORDSTRENGTH.strength') + ": ";
    switch(obj.val) {
      case 0: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.na'); className = "red"; break;
      case 1: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.weak'); className = "red"; break;
      case 2: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.good'); className = "green"; break;
      case 3: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.great'); className = "green"; break;
      case 4: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.superb'); className = "green"; break;
    }

    // console.log(obj.val);
    console.log('getPasswordStrengthClass ===>', this.signForm.value.passwordConfirm?.trim());

    var pw2Empty = (this.signForm.value.passwordConfirm?.trim().length == 0);
    var matching = (this.signForm.value.password === this.signForm.value.passwordConfirm);

    if (overrideLabels) {
      className = (matching) ? "green" : "red";
      if (this.signForm.value.passwordConfirm?.trim().length == 0) {
        obj.lbl = null;
      }
      else {
        obj.lbl = (matching) ?
          this.translateProviderService.instant('PASSWORDSTRENGTH.passwords_match') :
          this.translateProviderService.instant('PASSWORDSTRENGTH.passwords_do_not_match');
      }
    }

    this.passwordStrengthInfo.okButtonEnabled = (!pw2Empty && matching);
    this.passwordStrengthInfo.okButtonClass = (this.passwordStrengthInfo.okButtonEnabled) ? '' : 'save-button-disabled';

    return className;
  }

  togglePassword1Visibility() {
    this.passwordStrengthInfo.password1Type = (this.passwordStrengthInfo.password1Type == 'text') ? 'password' : 'text';
  }

  togglePassword2Visibility() {
    this.passwordStrengthInfo.password2Type = (this.passwordStrengthInfo.password2Type == 'text') ? 'password' : 'text';
  }


  private async componentInit() {

    var isAvailable = await this._FingerprintAIO.isAvailable();
    if(isAvailable) {
      console.log("this._FingerprintAIO.isAvailable()", isAvailable);
      var biometricOptions = ['biometric', 'face', 'finger'];
      var matchingBio = biometricOptions.find(opt => opt == isAvailable );
      this.faioIsAvailable = (matchingBio != null);
    } else {
      this.faioIsAvailable = false;
    }

    // var value = await this.secureStorage.getValue(SecureStorageKey.faioEnabled, false);
    // this.toggleValue = (value === 'true');

    this.toggleValue = true;
    await this.secureStorage.setValue(SecureStorageKey.faioEnabled, 'true');

    const dataFromStorage: SignUp = await this.signService.getSignData();

    this.setFormDataFromStorage(dataFromStorage);
    this.checkPassword1Strength();
    //this.checkPassword2Strength();
    setTimeout(() => this.detectChanges());
  }

  private setFormDataFromStorage(data: SignUp): void {
    if (data && data.password) {
      this.signForm.controls.password.setValue(data.password);
    }
    if (data && data.confirm_password) {
      this.signForm.controls.passwordConfirm.setValue(data.confirm_password);
    }
  }

  private passwordConfirming() {
    return (formGroup: UntypedFormGroup) => {
      const control = formGroup.controls.password;
      const matchingControl = formGroup.controls.passwordConfirm;

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
         console.log('passwordConfirmation failed');
        return;
      }

      console.log('passwordConfirmation succeeded');
    };
  }

  goToBrowseMode() {
    // this.secureStorage.setValue(SecureStorageKey.browseMode, true.toString()).then(() => {
      this.nav.navigateForward('/dashboard/tab-home')
    // })
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }
}
