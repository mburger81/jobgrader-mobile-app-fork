import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import JSConfetti from 'js-confetti';
import { SignUp } from 'src/app/core/models/SignUp';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { AuthenticationProviderService } from 'src/app/core/providers/authentication/authentication-provider.service';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { DeviceService } from 'src/app/core/providers/device/device.service';
import { KeyRecoveryBackupService } from 'src/app/core/providers/key-recovery-backup/key-recovery-backup.service';
import { PushService } from 'src/app/core/providers/push/push.service';
import { SecurePinService } from 'src/app/core/providers/secure-pin/secure-pin.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { Settings, SettingsService } from 'src/app/core/providers/settings/settings.service';
import { SignProviderService } from 'src/app/core/providers/sign/sign-provider.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { ActivityKeys, UserActivitiesService } from 'src/app/core/providers/user-activities/user-activities.service';
import { LockService } from 'src/app/lock-screen/lock.service';


@Component({
  selector: 'app-step-7',
  templateUrl: './step-7.page.html',
  styleUrls: ['./step-7.page.scss']
})
export class SignUpStepSevenPage implements OnInit {

  @ViewChild('canvasx') canvas: HTMLCanvasElement;

  jsConfetti: JSConfetti;

  interval: any;

  registrationSteps = {
    ACCOUNT_CREATED: false,
    DEVICE_BRANDED: false,
    LOGGED_IN: false,
    GEN_ENCRYPTION: false,
    BACKUP_ENCRYPTION: false,
    FINISHING_UP: false
  }



  constructor(
    private nav: NavController,
    private apiService: ApiProviderService,
    private _CryptoProviderService: CryptoProviderService,
    private secureStorageService: SecureStorageService,
    private signService: SignProviderService,
    private _DeviceService: DeviceService,
    private _Toast: ToastController,
    private _KeyRecoveryBackupService: KeyRecoveryBackupService,
    private _SecurePinService: SecurePinService,
    private lockService: LockService,
    private _Push: PushService,
    private settingsService: SettingsService,
    private _UserActivity: UserActivitiesService,
    private _Translate: TranslateProviderService,
    private _Auth: AuthenticationProviderService
  ) { }

  async ngOnInit() {

    this.jsConfetti = new JSConfetti({canvas: this.canvas});

    console.log('Creating user account');
    await this.signService.provideInitialOnboarding();
    this.registrationSteps.ACCOUNT_CREATED = true;

    console.log('Branding device to user account');
    const signDataString = await this.secureStorageService.getValue(SecureStorageKey.sign, false);
    const signData = JSON.parse(signDataString);
    await this.signService.brandDeviceToUser(signData.username, signData.password);
    this.registrationSteps.DEVICE_BRANDED = true;

    console.log('Logging in');
    await this.loginUser();
    await this._DeviceService.obtainUDID();
    var success = this.signService.checkIsSuccess();
    var loginSuccess = (await this.secureStorageService.getValue(SecureStorageKey.loginStatusAfterSignup, false) === 'true');

    if (!loginSuccess) {
      await this.presentToast(this._Translate.instant('SIGNSTEPFIVE.loginStatus'));
      return;
    }

    this.registrationSteps.LOGGED_IN = true;

    console.log('Generating Encryption Keypairs');
    await this._CryptoProviderService.generateUserKeyPair(); // Process 1
    await this._CryptoProviderService.generateDeviceEncryptionKeyPair(); // Process 2
    // await this.didCommService.generateChatKeyPair(); // Process 3
    // console.log("await this.didCommService.generateChatKeyPair();");
    await this._DeviceService.pushDeviceInfo().catch(e => {
      console.log("await this._DeviceService.pushDeviceInfo() Error", e);
    }); // Process 2
    await this._KeyRecoveryBackupService.sendUserMnemonic().catch(e => {
      console.log("await this._KeyRecoveryBackupService.sendUserMnemonic()", e);
    }); // Process 1
    await this._KeyRecoveryBackupService.sendUserPublicEthereumAddress().catch(e => {
      console.log("await this._KeyRecoveryBackupService.sendUserPublicEthereumAddress()", e);
    }); // Process 1
    // await this.registrationProcess4();

    this.registrationSteps.GEN_ENCRYPTION = true;

    console.log('Backing up your encryption keys');
    await this.settingsService.set(Settings.language, await this._Translate.getLangFromStorage() , true); // Process 4
    await this.settingsService.set(Settings.wizardNotRun, true.toString(), true); // Process 4
    await this.secureStorageService.removeValue(SecureStorageKey.loginStatusAfterSignup, false); // Process 4
    await this.secureStorageService.removeValue(SecureStorageKey.kycStatus, false); // Process 4
    await this.secureStorageService.removeValue(SecureStorageKey.did, false); // Process 4
    await this.secureStorageService.removeValue(SecureStorageKey.didDocument, false); // Process 4
    await this.secureStorageService.removeValue(SecureStorageKey.verifiableCredentialEncrypted, false); // Process 4
    await this._KeyRecoveryBackupService.pushKeys(); // Process 2

    // var token = await this.secureStorageService.getValue(SecureStorageKey.firebase, false);

    // if(token) {

    //   var deviceId = await this.secureStorageService.getValue(SecureStorageKey.deviceInfo, false);
    //   console.log("tab-home firebase 3: " + deviceId);

    //   if(deviceId && deviceId != "undefined") {
    //       this.apiService.updateFirebaseToken(token, deviceId).then(() => {
    //           console.log(`Firebase token ${token} updated for the deviceId ${deviceId}`)
    //       })
    //   }


    // } else {

    await this._Push.initPush();

    // }

    this.registrationSteps.BACKUP_ENCRYPTION = true;
    // await this.signService.pushChatKeyPairs();
    // console.log("await this.signService.pushChatKeyPairs();");

    // await this.presentToast(this.translateProviderService.instant('LOCALNOTIFICATION.heading'));
    console.log('Finiship up...');
    await this.secureStorageService.setValue(SecureStorageKey.faioEnabled, 'true');  // Process 5
    var storedPin = await this.secureStorageService.getValue(SecureStorageKey.securePINEncrypted, false); // Process 5
    if( storedPin ) {
      await this._SecurePinService.pushEncryptedPIN(storedPin);
      var encryptedPin = await this._SecurePinService.encryptPIN(storedPin);
      await this.secureStorageService.setValue(SecureStorageKey.securePINEncrypted, encryptedPin);
    }
    await this._UserActivity.updateActivity(ActivityKeys.USER_CREATED); // Process 6

    this.registrationSteps.FINISHING_UP = true;



    // this.interval = setInterval(()=>{
      this.jsConfetti.addConfetti();
    // }, 1000);

    this.lockService.addResume();


    this.signService.signUpForm = {};
    this.signService.personalInformation = {};



    console.log('Creating DID Document');
    var didDocument = await this.apiService.putDid();
    if(didDocument) {
      var encryptedDID = await this._CryptoProviderService.encryptGenericData(JSON.stringify(didDocument));
      await this.secureStorageService.setValue(SecureStorageKey.didDocument, encryptedDID);
      if(didDocument.id) {
        await this.secureStorageService.setValue(SecureStorageKey.did, didDocument.id);
      }
    }

  }

  private async loginUser() {
    const dataFromStorage: SignUp = await this.signService.getSignData();
    console.log(JSON.stringify(dataFromStorage));
    await this._Auth.loginUsernameAndPassword(dataFromStorage.username, dataFromStorage.password, false);
  }

  async presentToast(message: string) {
    const toast = await this._Toast.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  async nextStep() {

    // clearInterval(this.interval);

    this.jsConfetti.clearCanvas();

    this.nav.navigateRoot('/wallet-generation/step-1?source=registration');
  }
}