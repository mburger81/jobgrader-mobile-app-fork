import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
// import { Keyboard } from '@capacitor/keyboard';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { SecurePinService } from 'src/app/core/providers/secure-pin/secure-pin.service';
import { CodeInputComponent } from 'angular-code-input';

enum Steps {
  step1 = 'step1',
  step2 = 'step2'
}

@Component({
  selector: 'app-step-4',
  templateUrl: './step-4.page.html',
  styleUrls: ['./step-4.page.scss']
})
export class SignUpStepFourPage implements OnInit {

  @ViewChild('codeInput1') codeInput1: CodeInputComponent;
  @ViewChild('codeInput2') codeInput2: CodeInputComponent;

  public stage = 4;
  public progress = this.stage/6;

  // public setPinForm: UntypedFormGroup;
  public visibility = {
    input1: true,
    input2: true
  }
  public steps = Steps;
  public step: string = this.steps.step1;
  public isButtonDisabled: boolean = true;
  private tempPin: string = '';
  private code: string = '';

  constructor(
    private nav: NavController,
    private _ToastController: ToastController,
    private _SecureStorageService: SecureStorageService,
    private _SecurePinService: SecurePinService,
    private _Translate: TranslateProviderService
  ) { }



  ngOnInit() {
    this.step = this.steps.step1;
    // this.setPinForm = new UntypedFormGroup({
    //   newPin: new UntypedFormControl('',[ Validators.required ]),
    //   confirmPin: new UntypedFormControl('',[ Validators.required ]),
    // })
  }

  onCodeChanged1(code: string) {
    // console.log(code)
  }

  onCodeChanged2(code: string) {
    // console.log(code)
  }

  onCodeCompleted1(code: string) {
    this.tempPin = code;
    this.codeInput1.reset();
    this.step = this.steps.step2;
  }

  onCodeCompleted2(code: string) {
    this.code = code;
    this.isButtonDisabled = false;
  }

  // public async savePin() {
  //   console.log(this.setPinForm.value);
  //   if(this.setPinForm.value.newPin.length != 5 ||
  //     this.setPinForm.value.confirmPin.length != 5) {
  //       return await this.presentToast(this._Translate.instant('PIN.incorrectLength'));
  //     }
  //   if(this.setPinForm.value.newPin !== this.setPinForm.value.confirmPin) {
  //     return await this.presentToast(this._Translate.instant('PIN.noMatch'));
  //   }
  //   var c = await this._SecurePinService.pushEncryptedPIN(this.setPinForm.value.newPin);
  //   console.log(c);
  //   await this._SecureStorageService.setValue(SecureStorageKey.securePINEncrypted, c);
  //   await this.presentToast(this._Translate.instant('PIN.success'));
  //   this._ModalController.dismiss();
  // }

  private presentToast(message) {
    this._ToastController.create({
      message: message,
      duration: 2000,
      position: 'top'
    }).then((toast) => {
      toast.present();
    })
  }

  public togglePINVisibility(v) {
    this.visibility[v] = !this.visibility[v];
  }


  async nextStep(){


    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();

    if(!this.code || !this.tempPin) {
      return this.presentToast(this._Translate.instant('SIGNSTEPNINE.fillToast'));
    }


    // console.log(this.tempPin);
    // console.log(code);
    if(this.code != this.tempPin) {
      this.codeInput2.reset();
      return this.presentToast(this._Translate.instant('PIN.noMatch'));
    } else {
      // this._SecurePinService.pushEncryptedPIN(this.tempPin).then((c) => {
        // console.log(c);
        await this._SecureStorageService.setValue(SecureStorageKey.securePINEncrypted, this.tempPin);
        await this._SecureStorageService.setValue(SecureStorageKey.lockScreenToggle, "true");
        //this.presentToast(this._Translate.instant('PIN.success'));
        this.codeInput2.reset();
        this.nav.navigateForward('/sign-up/step-5');
      // }).catch((e) => {
      //   console.log(e);
      //   this.presentToast(this._Translate.instant('PIN.failure'));
      //   this.codeInput2.reset();
      // });
    }
  }

  skip(){
    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
    this._SecureStorageService.removeValue(SecureStorageKey.securePINEncrypted, false).then(() => {
      // this.codeInput1.reset();
      // this.codeInput2.reset();
      this.nav.navigateForward('/sign-up/step-5');
    })
  }

  goToPreviousStep(): void {
    // this.codeInput.reset();
    this.nav.navigateBack('/sign-up/step-3');
  }
}
