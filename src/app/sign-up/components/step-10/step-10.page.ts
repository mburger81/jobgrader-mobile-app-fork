import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { LockService } from '../../../lock-screen/lock.service';
import { Settings, SettingsService } from '../../../core/providers/settings/settings.service';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { AppStateService } from '../../../core/providers/app-state/app-state.service';


@Component({
  selector: 'app-step-10',
  templateUrl: './step-10.page.html',
  styleUrls: ['./step-10.page.scss']
})
export class SignUpStepTenPage implements OnInit {
  @ViewChild(IonContent, {}) content: IonContent;
  signForm: UntypedFormGroup;
  success: boolean;
  private ngOnInitExecuted: any;

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private signService: SignProviderService,
    private secureStorage: SecureStorageService,
    private toastController: ToastController,
    private translateProviderService: TranslateProviderService,
    private apiProviderService: ApiProviderService,
    private lockService: LockService,
    private appState: AppStateService,
    private settingsService: SettingsService
  ) { }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
    if (this.success) {
      await this.settingsService.set(Settings.wizardNotRun, false.toString(), true);
      const { photo, userData, userTrust } = await this.apiProviderService.getUserImageAndData(this.appState.basicAuthToken, true, true);

      await this.secureStorage.setValue(SecureStorageKey.userData, JSON.stringify(userData));
      await this.secureStorage.setValue(SecureStorageKey.userTrustData, JSON.stringify(userTrust));
      this.signService.documentSetter(userData);

      await this.presentToast(this.translateProviderService.instant('LOCALNOTIFICATION.heading'), "");
    }
  }

  async presentToast(header:string, message: string) {
    const toast = await this.toastController.create({
      header: header,
      message: message,
      duration: 2000,
      position: 'top',
    });
    await toast.present();
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async nextStep() {
    await this.cleaner();
    this.lockService.addResume();
    this.nav.navigateRoot('/dashboard').then(() => {
      this.lockService.restart();
    });
  }

  async getVerified() {
    await this.cleaner();
    this.lockService.addResume();
    this.nav.navigateRoot('/kyc').then(_ => {
      this.lockService.restart();
    });
  }

  prevStep(): void {
    this.nav.navigateBack('/sign-up/step-6');
  }

  skipReg(): void {
    this.nav.navigateRoot('/dashboard').then(() => {
      this.lockService.restart();
    });
    this.lockService.addResume();
  }

  private async componentInit() {
    this.success = this.signService.checkIsPersonalInformationSuccess();
    setTimeout(() => this.detectChanges());
  }

  async cleaner(){
    await this.secureStorage.removeValue(SecureStorageKey.sign, false);
    await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false);

    this.signService.imageSelectionPayload = {base64Image: null, fileName: 'Default', format: null};
    this.signService.resetSignForm();
    this.signService.resetPersonalInfoForm();
  }


  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
