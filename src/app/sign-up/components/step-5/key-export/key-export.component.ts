import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, Platform, ToastController } from '@ionic/angular';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { UtilityService } from 'src/app/core/providers/utillity/utility.service';
import { BiometricService } from 'src/app/core/providers/biometric/biometric.service';
import { take, switchMap } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { ethers } from 'ethers';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { SettingsService, Settings } from 'src/app/core/providers/settings/settings.service';
import { Clipboard } from '@capacitor/clipboard';
import { UserProviderService } from 'src/app/core/providers/user/user-provider.service';
import { GdriveService } from 'src/app/core/providers/cloud/gdrive.service';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { environment } from 'src/environments/environment';
import { DropboxService } from 'src/app/core/providers/cloud/dropbox.service';

declare var window: any;
declare var cordova: any;
declare var iCloudDocStorage: any;

@Component({
  selector: 'app-key-export',
  templateUrl: './key-export.component.html',
  styleUrls: ['./key-export.component.scss'],
})
export class KeyExportComponent implements OnInit {
  @Input() data?: any;
  @Input() new?: boolean;
  @Input() exception?: boolean;
  @Input() stage?: string;
  @Input() bothFiles?: any;
  private browser: InAppBrowserObject;
  public viewLockedContent = false;
  public userPublicKey = '';
  private username = '';
  private isFaioAvailable;

  public header = '';
  public message = '';
  public button = '';

  private path = null;
  private cloudKit = null;
  private docStorage = null;
  private fileName = null;
  private fullPath = null;
  private blobUrl = null;
  private encryptedMnemonic2 = null;

  constructor(
    private _ModalController: ModalController,
    private _SocialSharing: SocialSharing,
    private _File: File,
    private _Translate: TranslateProviderService,
    private _Platform: Platform,
    private _SecureStorageService: SecureStorageService,
    private _AlertController: AlertController,
    private _ToastController: ToastController,
    private _CryptoProviderService: CryptoProviderService,
    private biometricService: BiometricService,
    private _SettingService: SettingsService,
    private _FileOpener: FileOpener,
    private _UserProviderService: UserProviderService,
    private _LoaderProviderService: LoaderProviderService,
    
    private _GDrive: GdriveService,
    private _Dropbox: DropboxService,
    private _InAppBrowser: InAppBrowser
  ) { }

  async ngOnInit() {

    this.username = await this._SecureStorageService.getValue(SecureStorageKey.userName, false);

    if( this._Platform.is('ios') && this._Platform.is('hybrid') ) {
      this.path = this._File.documentsDirectory;
    } else if ( this._Platform.is('android') && this._Platform.is('hybrid') ) {
      this.path = this._File.dataDirectory;
    }

    this.header = !!this.exception ? this._Translate.instant('FILEBACKUPDISCLAIMER.heading') : this._Translate.instant('EXPORTKEY.title');
    this.message = !!this.exception ? this._Translate.instant('FILEBACKUPDISCLAIMER.body') : `${this._Translate.instant('EXPORTKEY.heading')} (${this._Translate.instant('EXPORTKEY.tap-to-unlock')})`;
    this.button = !!this.exception ? this._Translate.instant('FILEBACKUPDISCLAIMER.button') : this._Translate.instant('EXPORTKEY.button');

    console.log("this.stage", this.stage);

    if(!this.stage || this.stage != "4") {
      this.isFaioAvailable = (await this._SecureStorageService.getValue(SecureStorageKey.faioEnabled, false) == 'true');
      var wallet = ethers.Wallet.fromMnemonic(this.data);

      this.userPublicKey = wallet.address;


      this.fileName = `helixid-userkey-${this.username}-${this.userPublicKey}.json`;
      this.fullPath = this.path + this.fileName;

      var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();
      var encryptedMnemonic1 = CryptoJS.AES.encrypt(JSON.stringify(this.data), backupSymmetricKey).toString();
      this.encryptedMnemonic2 = JSON.stringify(CryptoJS.AES.encrypt(encryptedMnemonic1, UDIDNonce.userKey).toString());
      this.blobUrl = URL.createObjectURL(new Blob([this.encryptedMnemonic2], {type: "application/json"}));

      if(this._Platform.is('hybrid')) {
        if(this._Platform.is('ios')) {
          await this._File.writeFile(this.path, this.fileName, this.encryptedMnemonic2, { replace: true });

          try {
            var presentToastOpener = () => {
              iCloudDocStorage.fileList("Cloud", (a) => {
                var aaa = Array.from(a, aaaa => Object.keys(aaaa)[0]);
                var check = aaa.find(aa => aa.includes(this.fileName) );
                if(check) {
                  this.presentToastWithOpen(this._Translate.instant('CLOUDBACKUP.wallet-success'), check);
                }
              }, (b) => {
                console.log("b", b);
              })
            }

            iCloudDocStorage.initUbiquitousContainer(environment.icloud, (s) => {
              iCloudDocStorage.syncToCloud(this.fullPath, (s) => {
                presentToastOpener();
              }, (e) => {
                presentToastOpener();
              });
            }, (e) => {
              console.log(e)
            });

          } catch(e) {
            console.log(e);
          }

          if(this._GDrive.user) {
            if(this._GDrive.user.email) {
              this._GDrive.init([{
                fileName: this.fileName,
                contents: this.encryptedMnemonic2
              }], this._Translate.instant('CLOUDBACKUP.wallet-success'));
            }
          }
        }
        else if(this._Platform.is('android')) {
          await this._File.writeFile(this.path, this.fileName, this.encryptedMnemonic2, { replace: true });

          this._GDrive.init([{
            fileName: this.fileName,
            contents: this.encryptedMnemonic2
          }], this._Translate.instant('CLOUDBACKUP.wallet-success'));
        }
      } else {

        this._GDrive.init([{
          fileName: this.fileName,
          contents: this.encryptedMnemonic2
        }], this._Translate.instant('CLOUDBACKUP.wallet-success'));
        
      }

      this._Dropbox.backupFiles([{
        fileName: this.fileName,
        contents: this.encryptedMnemonic2
      }], this._Translate.instant('CLOUDBACKUP.wallet-success'));
    }



  }

  async socialSharing() {

    var verifiedWallet = ethers.Wallet.fromMnemonic(this.bothFiles.verified);
    var unverifiedWallet = ethers.Wallet.fromMnemonic(this.bothFiles.unverified);

    var verifiedWalletAddress = verifiedWallet.address;
    var unverifiedWalletAddress = unverifiedWallet.address;

    var username = await this._SecureStorageService.getValue(SecureStorageKey.userName, false);

    var verifiedWalletAddressFileName = `helixid-userkey-${username}-${verifiedWalletAddress}.json`;
    var unverifiedWalletAddressFileName = `helixid-userkey-${username}-${unverifiedWalletAddress}.json`;

    var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();

    var verifiedWalletEncryptedMnemonic1 = CryptoJS.AES.encrypt(JSON.stringify(this.bothFiles.verified), backupSymmetricKey).toString();
    var verifiedWalletEncryptedMnemonic2 = JSON.stringify(CryptoJS.AES.encrypt(verifiedWalletEncryptedMnemonic1, UDIDNonce.userKey).toString());
    var blobUrl1 = URL.createObjectURL(new Blob([verifiedWalletEncryptedMnemonic2], {type: "application/json"}));

    var unverifiedWalletEncryptedMnemonic1 = CryptoJS.AES.encrypt(JSON.stringify(this.bothFiles.unverified), backupSymmetricKey).toString();
    var unverifiedWalletEncryptedMnemonic2 = JSON.stringify(CryptoJS.AES.encrypt(unverifiedWalletEncryptedMnemonic1, UDIDNonce.userKey).toString());
    var blobUrl2 = URL.createObjectURL(new Blob([unverifiedWalletEncryptedMnemonic2], {type: "application/json"}));

    if(this._Platform.is('hybrid')) {
      this._SocialSharing.shareWithOptions({
        subject: 'File Backups',
        files: [ this.path + verifiedWalletAddressFileName, this.path + unverifiedWalletAddressFileName ]
      }).then(async () => {
        if(this.new) {
          await this._SettingService.set(Settings.cloudBackupTimestamp, new Date().toISOString(), true);
        }
        this.dismissAfterSocialSharing();
      })
    } else {
      var browser1 = this._InAppBrowser.create(blobUrl1, '_system');
      var browser2 = this._InAppBrowser.create(blobUrl2, '_system');
      this.dismissAfterSocialSharing();
    }
  }

  exportKeys() {
      if(this._Platform.is('hybrid')) {
        this._SocialSharing.shareWithOptions({
          subject: this.fileName,
          files: [ this.path + this.fileName ]
        }).then(async () => {
          if(this.new) {
            await this._SettingService.set(Settings.cloudBackupTimestamp, new Date().toISOString(), true);
          }
          this.close();
        })
      } else {
        this.browser = this._InAppBrowser.create(this.blobUrl, '_system');
        this.close();
      }

  }

  onPress(event: any, mnemonic: any) {
    Clipboard.write({ string: mnemonic.toString() }).then(() => {
      this.presentToast(this._Translate.instant('SETTINGSACCOUNT.copied'))
    })
  }

  unlockContent() {
    if(this.isFaioAvailable) {
      this.faceIdToViewSecrets();
    } else {
      this.checkWithPassword();
    }
  }

  faceIdToViewSecrets() {
    UtilityService.setTimeout(500).pipe(switchMap(_ =>
      this.biometricService.biometricShow()),take(1)).subscribe(() => {
        this.viewLockedContent = true;
    }, _ => {
      this.viewLockedContent = false;
    });
  }

  checkWithPassword() {
    this._AlertController.create({
      message: this._Translate.instant('EXPORTKEY.alert-heading'),
      inputs: [{
        type: 'password',
        placeholder: "...",
        name: 'password'
      }],
      buttons: [
        {
          text: this._Translate.instant('BUTTON.CANCEL'),
          role: 'cancel',
          handler: () => {
            this.viewLockedContent = false;
          }
        },
        {
          text: this._Translate.instant('GENERAL.ok'),
          handler: async (data) => {
            var plaintextPassword = data.password;
            var passwordHash = await this._SecureStorageService.getValue(SecureStorageKey.passwordHash, false);
            var nonce = await this._SecureStorageService.getValue(SecureStorageKey.nonce, false);
            if(this._CryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {
              this.viewLockedContent = true;
            } else {
              this.viewLockedContent = false;
              this.presentToast(this._Translate.instant('EXPORTKEY.incorrect-password'));
            }
          }
        }
      ]
    }).then(alerti => alerti.present())
  }

  presentToast(message: string) {
    this._ToastController.create({
      position: 'top',
      duration: 2000,
      message
    }).then(toast => toast.present())
  }

  presentToastWithOpen(message: string, filePath: string) {
    this._ToastController.create({
      position: 'top',
      duration: 3000,
      message,
      buttons: [
        {
          side: 'end',
          icon: 'folder',
          text: this._Translate.instant('APPSTORE.open'),
          handler: () => {
            this._FileOpener.open(filePath, "application/json").then((aaa) => {
              console.log("aaa", aaa);
            }).catch(bbb => {
              console.log("bbb", bbb);
            })
          },
        }
      ]
    }).then(toast => toast.present())
  }

  dismissAfterSocialSharing() {
    this._ModalController.dismiss({ value: true});
  }

  close() {
    this._ModalController.dismiss();
  }

}
