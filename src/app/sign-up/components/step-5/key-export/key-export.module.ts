import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { KeyExportComponent } from './key-export.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CodeInputModule } from 'angular-code-input';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';

@NgModule({
    imports: [
    CommonModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    CodeInputModule,
    TranslateModule.forChild()
  ],
  providers: [SocialSharing, File, FileOpener],
  declarations: [KeyExportComponent],
//   entryComponents: [KeyExportComponent],
  exports: [KeyExportComponent]
})
export class KeyExportModule {}
