import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SignUpStepFivePage } from './step-5.page';

describe('SignUpStepFivePage', () => {
  let component: SignUpStepFivePage;
  let fixture: ComponentFixture<SignUpStepFivePage>;

 beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpStepFivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpStepFivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
