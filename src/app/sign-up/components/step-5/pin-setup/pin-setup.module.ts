import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { PinSetupComponent } from './pin-setup.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CodeInputModule } from 'angular-code-input';

@NgModule({
    imports: [
    CommonModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    CodeInputModule,
    TranslateModule.forChild()
  ],
  declarations: [PinSetupComponent],
//   entryComponents: [PinSetupComponent],
  exports: [PinSetupComponent]
})
export class PinSetupModule {}
