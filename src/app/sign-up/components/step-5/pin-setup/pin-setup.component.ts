import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastController, ModalController } from '@ionic/angular';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecurePinService } from 'src/app/core/providers/secure-pin/secure-pin.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { CodeInputComponent } from 'angular-code-input';

interface FormElements {
  existingPin?: string;
  newPin?: string;
  confirmPin?: string;
}

enum Steps {
  step1 = 'step1',
  step2 = 'step2'
}

@Component({
  selector: 'app-pin-setup',
  templateUrl: './pin-setup.component.html',
  styleUrls: ['./pin-setup.component.scss'],
})
export class PinSetupComponent implements OnInit {
  @ViewChild('codeInput1') codeInput1: CodeInputComponent;
  @ViewChild('codeInput2') codeInput2: CodeInputComponent;

  // public setPinForm: FormGroup;
  public visibility = {
    input1: true,
    input2: true
  }
  public steps = Steps;
  public step: string = this.steps.step1;
  private tempPin: string = '';

  constructor(
    private _ModalController: ModalController,
    private _ToastController: ToastController,
    private _SecureStorageService: SecureStorageService,
    private _SecurePinService: SecurePinService,
    private _Translate: TranslateProviderService
  ) { }

  

  ngOnInit() {
    this.step = this.steps.step1;
    // this.setPinForm = new FormGroup({
    //   newPin: new FormControl('',[ Validators.required ]),
    //   confirmPin: new FormControl('',[ Validators.required ]),
    // })
  }

  onCodeChanged1(code: string) { 
    // console.log(code) 
  }
  
  onCodeChanged2(code: string) { 
    // console.log(code) 
  }

  onCodeCompleted1(code: string) {
    this.tempPin = code;
    this.codeInput1.reset();
    this.step = this.steps.step2;
  }

  onCodeCompleted2(code: string) {
    // console.log(this.tempPin);
    // console.log(code);
    if(code != this.tempPin) {
      this.codeInput2.reset();
      return this.presentToast(this._Translate.instant('PIN.noMatch'));
    } else {
      this._SecurePinService.pushEncryptedPIN(this.tempPin).then((c) => {
        // console.log(c);
        this._SecureStorageService.setValue(SecureStorageKey.securePINEncrypted, c);
        this.presentToast(this._Translate.instant('PIN.success'));
        this.codeInput2.reset();
        this._ModalController.dismiss();
      }).catch((e) => {
        console.log(e);
        this.presentToast(this._Translate.instant('PIN.failure'));
        this.codeInput2.reset();
        this._ModalController.dismiss();
      });
    }
  }

  public close(){
    this._ModalController.dismiss()
  }

  // public async savePin() {
  //   console.log(this.setPinForm.value);
  //   if(this.setPinForm.value.newPin.length != 5 || 
  //     this.setPinForm.value.confirmPin.length != 5) {
  //       return await this.presentToast(this._Translate.instant('PIN.incorrectLength'));
  //     }
  //   if(this.setPinForm.value.newPin !== this.setPinForm.value.confirmPin) {
  //     return await this.presentToast(this._Translate.instant('PIN.noMatch'));
  //   }
  //   var c = await this._SecurePinService.pushEncryptedPIN(this.setPinForm.value.newPin);
  //   console.log(c);
  //   await this._SecureStorageService.setValue(SecureStorageKey.securePINEncrypted, c);
  //   await this.presentToast(this._Translate.instant('PIN.success'));
  //   this._ModalController.dismiss();
  // }

  private presentToast(message) {
    this._ToastController.create({
      message: message,
      duration: 2000,
      position: 'top'
    }).then((toast) => {
      toast.present();
    })
  }

  public togglePINVisibility(v) {
    this.visibility[v] = !this.visibility[v];
  }

}
