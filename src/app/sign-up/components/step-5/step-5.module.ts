
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { KeyExportModule } from './key-export/key-export.module'; 
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PinSetupModule } from './pin-setup/pin-setup.module';
import { SharedModule } from '../../../shared/shared.module';
import { SignUpStepFivePage } from './step-5.page';

const routes: Routes = [
  {
    path: '',
    component: SignUpStepFivePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PinSetupModule,
    KeyExportModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [SignUpStepFivePage]
})
export class SignUpStepFivePageModule {}
