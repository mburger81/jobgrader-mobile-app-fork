import { TranslateProviderService } from './../../../core/providers/translate/translate-provider.service';
import { AlertController, ToastController, NavController, IonContent, IonInput } from '@ionic/angular';
import { ApiProviderService } from './../../../core/providers/api/api-provider.service';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { SignUp } from '../../../core/models/SignUp';
// import { Keyboard } from '@capacitor/keyboard';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
// const ValidatorPizzaClient = require("validator-pizza-node");
/*
import { ChangeDetectorRef, Component, OnInit, NgZone } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController, ModalController, NavController, AlertController } from '@ionic/angular';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { UserProviderService } from '../../../core/providers/user/user-provider.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { DeviceService } from '../../../core/providers/device/device.service';
import { CryptoProviderService } from '../../../core/providers/crypto/crypto-provider.service';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { LockService } from '../../../lock-screen/lock.service';
import { Settings, SettingsService } from '../../../core/providers/settings/settings.service';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { KeyRecoveryBackupService } from '../../../core/providers/key-recovery-backup/key-recovery-backup.service';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { SecurePinService } from 'src/app/core/providers/secure-pin/secure-pin.service';
import { PinSetupComponent } from './pin-setup/pin-setup.component';
import { KycService } from 'src/app/kyc/services/kyc.service';
import { KycState } from 'src/app/kyc/enums/kyc-state.enum';
import { KycProvider } from 'src/app/kyc/enums/kyc-provider.enum';
import { environment } from 'src/environments/environment';
import { KeyExportComponent } from './key-export/key-export.component';
import { ethers } from 'ethers';

declare var window: any;
*/

const axios = require("axios");

@Component({
  selector: 'app-step-5',
  templateUrl: './step-5.page.html',
  styleUrls: ['./step-5.page.scss']
})
export class SignUpStepFivePage implements OnInit {

  @ViewChild(IonContent, {}) content: IonContent;
  @ViewChild('inputElem') inputElem: IonInput;
  public step = 5;
  public progress = this.step/6;

  signForm: UntypedFormGroup;
  signProcessing = false;
  setPhones = [];
  emailValidationImplemented = true;
  private ngOnInitExecuted: any;

  validEmailDomains = [ "gmail.com", "yahoo.com", "hotmail.com", "aol.com", "hotmail.co.uk", "hotmail.fr", "msn.com", "yahoo.fr", "wanadoo.fr", "orange.fr", "comcast.net", "yahoo.co.uk", "yahoo.com.br", "yahoo.co.in", "live.com", "rediffmail.com", "free.fr", "gmx.de", "web.de", "yandex.ru", "ymail.com", "libero.it", "outlook.com", "uol.com.br", "bol.com.br", "mail.ru", "cox.net", "hotmail.it", "sbcglobal.net", "sfr.fr", "live.fr", "verizon.net", "live.co.uk", "googlemail.com", "yahoo.es", "ig.com.br", "live.nl", "bigpond.com", "terra.com.br", "yahoo.it", "neuf.fr", "yahoo.de", "alice.it", "rocketmail.com", "att.net", "laposte.net", "facebook.com", "bellsouth.net", "yahoo.in", "hotmail.es", "charter.net", "yahoo.ca", "yahoo.com.au", "rambler.ru", "hotmail.de", "tiscali.it", "shaw.ca", "yahoo.co.jp", "sky.com", "earthlink.net", "optonline.net", "freenet.de", "t-online.de", "aliceadsl.fr", "virgilio.it", "home.nl", "qq.com", "telenet.be", "me.com", "yahoo.com.ar", "tiscali.co.uk", "yahoo.com.mx", "voila.fr", "gmx.net", "mail.com", "planet.nl", "tin.it", "live.it", "ntlworld.com", "arcor.de", "yahoo.co.id", "frontiernet.net", "hetnet.nl", "live.com.au", "yahoo.com.sg", "zonnet.nl", "club-internet.fr", "juno.com", "optusnet.com.au", "blueyonder.co.uk", "bluewin.ch", "skynet.be", "sympatico.ca", "windstream.net", "mac.com", "centurytel.net", "chello.nl", "live.ca", "aim.com", "bigpond.net.au"];

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private signService: SignProviderService,
    private apiService: ApiProviderService,
    private toastController: ToastController,
    private translateProviderService: TranslateProviderService,
    private alertController: AlertController,
    private loader: LoaderProviderService,
    private _NetworkService: NetworkService
  ) { }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
    this.inputElem.setFocus();
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }


  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  async nextStep() {
    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
    // await this.loader.loaderCreate();
    this.signForm.markAsDirty();
    this.signProcessing = true;
    if (!this.signForm.valid) {
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPTHREE.ERROR.INVALID'));
      await this.loader.loaderDismiss();
      return false;
    }

    if (!this._NetworkService.checkConnection()) {
      await this.loader.loaderDismiss();
      this._NetworkService.addOfflineFooter('ion-footer');
      return; } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
      }

    // Validate email
    const email = this.signForm.value.email_id;
    if (this.emailValidationImplemented) {

      const splitEmail = email.split("@");
      // const validatorPizzaClient = new ValidatorPizzaClient();
      // const trashValidation = await validatorPizzaClient.validate(splitEmail[0], splitEmail[1]);

      if(!this.validEmailDomains.includes(splitEmail[1])) {
        try {
          const trashValidation = await axios.get(`https://api.mailcheck.ai/domain/${splitEmail[1]}`);
          console.log(trashValidation);
  
          if(trashValidation.data) {
            if(trashValidation.data.disposable) {
              await this.loader.loaderDismiss();
              await this.presentToast(this.translateProviderService.instant('SIGNSTEPFIVE.disposableMailError1') + " " + this.translateProviderService.instant('SIGNSTEPFIVE.disposableMailError2'));
              return;
            }
          }
        } catch(e) {
          console.log(e);
        }
      }

        const mwResponse = await this.apiService.checkEmail(encodeURIComponent(email));
        if (mwResponse) {
          await this.loader.loaderDismiss();
          return await this.presentAlertConfirm();
        } else {
          console.log('Email is unique!');
        }
    }
    const {first_name} = await this.signService.getSignData();
    const callname = first_name;
    console.log('callname: ' + callname);
    await this.apiService.validateEmail(email, callname)
    .then((res) => {
      console.log(`Email validation successful: ${JSON.stringify(res)}`);
    })
    .catch(async err => {
      console.error(`Error validating email: ${JSON.stringify(err)}`);
      await this.loader.loaderDismiss();
      return await this.presentToast(this.translateProviderService.instant('SIGNSTEPTHREE.ERROR.NOT_UNIQUE')).then(res => res);
    });
    // Save to local storage
    await this.signService.saveSignData(this.signForm.value, 5);
    await this.loader.loaderDismiss();
    this.signProcessing = false;
    this.nav.navigateForward('/sign-up/step-6');
  }

  goToPreviousStep(): void {
    this.nav.navigateBack('/sign-up/step-4');
  }

  async skipReg() {
    this.signForm.reset();
    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
    await this.signService.skipReg(3);
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      mode: 'ios',
      header: this.translateProviderService.instant('SIGNSTEPTHREE.ERROR.NOT_UNIQUE'),
      buttons: [
        {
          text: this.translateProviderService.instant('SIGNSTEPTHREE.goToLogin'),
          handler: () => {
            this.nav.navigateRoot('/login');
          }
        }, {
          text: this.translateProviderService.instant('SIGNSTEPTHREE.goToReset'),
          handler: () => {
            this.nav.navigateRoot('/reset-password/step-1');
          }
        },
        {
          text: this.translateProviderService.instant('SETTINGS.cancel'),
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
    }

  private setFormDataFromStorage(data: SignUp): void {
    if (data && data.email_id) {
      this.signForm.controls.email_id.setValue(data.email_id);
    }
  }

  // regex from here: https://emailregex.com
  private async componentInit() {
    this.signForm = new UntypedFormGroup({
      email_id: new UntypedFormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)
      ])
    });
    const dataFromStorage: SignUp = await this.signService.getSignData();

    this.setFormDataFromStorage(dataFromStorage);
    setTimeout(() => this.detectChanges());
  }


  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
