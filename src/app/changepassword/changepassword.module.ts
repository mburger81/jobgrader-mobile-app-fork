import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ChangePasswordPage } from './changepassword.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
    {
        path: '',
        component: ChangePasswordPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        JobHeaderModule,
        RouterModule.forChild(routes),
        TranslateModule,
        SharedModule
    ],
    declarations: [ChangePasswordPage]
})
export class ChangePasswordPageModule {
}
