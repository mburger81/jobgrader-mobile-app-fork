import { Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ResetPassword } from '../../../core/models/ResetPassword';
import { ResetPasswordProviderService } from '../../../core/providers/reset-password/reset-password-provider.service';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../../../core/providers/crypto/crypto-provider.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { PasswordStrengthInfo } from 'src/app/model/model/passwordStrengthInfo';
import * as zxcvbn from 'zxcvbn';

@Component({
  selector: 'app-password-step-3',
  templateUrl: './step-3.page.html',
  styleUrls: ['./step-3.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResetPasswordStepThreePage implements OnInit, OnDestroy {
  @ViewChild(IonContent, {}) content: IonContent;
  resetPasswordForm: UntypedFormGroup;
  signProcessing = false;
  private ngOnInitExecuted: any;
  private additionalStyleSheetId: string;

  public displayRequirements = {
    username: false,
    password1: false,
    password2: false,
  };

  public passwordStrengthInfo: PasswordStrengthInfo = {
    password1Strength: 0,
    password1StrengthText: "",
    password1Type: "password",
    password2Strength: 0,
    password2StrengthText: "",
    password2Type: "password"
  }

  constructor(
    private nav: NavController,
    private resetPasswordProvider: ResetPasswordProviderService,
    private utilityService: UtilityService,
    private elemRef: ElementRef,
    private api: ApiProviderService,
    private translateProviderService: TranslateProviderService,
    private toastController: ToastController,
    private secureStorage: SecureStorageService,
    private cryptoProviderService: CryptoProviderService,
    private _NetworkService: NetworkService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  /** @inheritDoc */
  async ngOnInit() {
    this.ngOnInitExecuted = true;
    const tagName = this.elemRef.nativeElement.tagName.toLowerCase();
    const styles: string =
        this.utilityService.calculateWhiteCircleCssParams(`${tagName} .page-content:after`);
    UtilityService.addStyleToHead(styles, this.additionalStyleSheetId = `${tagName}CircleStyle`);
    await this.componentInit();
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    UtilityService.removeStyleFromHead(this.additionalStyleSheetId);
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async nextStep() {
    this.resetPasswordForm.markAsDirty();
    this.signProcessing = true;

    if (this.resetPasswordForm.value.resetPassword !== this.resetPasswordForm.value.resetPasswordConfirm) {
      console.error('Entered passwords do not match.');
      return await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.MISMATCH'));
    }

    if (this.passwordStrengthInfo.password1Strength < 2) {
      console.error('Password is too weak.');
      return await this.presentToast(this.translateProviderService.instant('PASSWORDSTRENGTH.error_too_weak'));
    }

    if (!this.resetPasswordForm.valid) {
      console.error('Password does not comply to our policies.');
      return await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.PASSWORD'));
    }

    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    const dataFromStorage: ResetPassword = await this.resetPasswordProvider.getResetPasswordData();
    const resetPasswordChangeBody = {
      username: dataFromStorage.reset_username,
      password: await this.cryptoProviderService.encryptPasswordAsBase64(this.resetPasswordForm.value.resetPassword)
    };
    console.log(resetPasswordChangeBody);
    await this.api.resetPasswordReset(resetPasswordChangeBody).then((response) => {
      // console.log(response);
      this.secureStorage.setValue(SecureStorageKey.resetPasswordStatus, (!response.errorFound).toString());
    });
    await this.resetPasswordProvider.saveResetPasswordData(this.resetPasswordForm.value, 3);
    this.signProcessing = false;
    this.nav.navigateForward('/reset-password/step-4');
  }

  async presentToast(message: string) {
    // const translations = {
    //   en: 'All fields should be checked',
    //   de: 'All fields should be checked',
    //   hi: 'All fields should be checked'
    // };

    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  prevStep(): void {
    this.nav.navigateBack('/reset-password/step-2');
  }

  async skipReg() {
    await this.resetPasswordProvider.skipReset();
  }

  private async componentInit() {
    this.resetPasswordForm = new UntypedFormGroup({
      resetPassword: new UntypedFormControl('', [
        Validators.required,
        this.validatePassword
      ]),
      resetPasswordConfirm: new UntypedFormControl('', [
        Validators.required,
        this.validatePassword
      ])
    }, {
      validators: this.passwordConfirming
    });
    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.checkPassword1Strength();
      this.checkPassword2Strength();
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  private validatePassword(formControl: UntypedFormControl) {
    const password = formControl.value;
    const hasEmptySpaces = password.match(/[\s]/);
    const hasBetweenMinAndMaxCharacters = password.match(/^.{8,25}$/);
    const hasBlockLetters = password.match(/[A-Z]/);
    const hasSmallLetter = password.match(/[a-z]/);
    const hasEitherUpperAndOrLowerCase = (hasBlockLetters !== null || hasSmallLetter !== null);
    const hasAccentedCharacters = password.match(/[\u00C0-\u024F\u1E00-\u1EFF]/);

    const isValid = (
      hasEmptySpaces === null &&
      hasBetweenMinAndMaxCharacters !== null &&
      hasEitherUpperAndOrLowerCase !== null &&
      hasAccentedCharacters === null
    );

    return (
      isValid
      ? null : {
        validatePassword: {
          valid: false
        }
      }
    );
  }

  private passwordConfirming() {
    return (formGroup: UntypedFormGroup) => {
      const control = formGroup.controls.resetPassword;
      const matchingControl = formGroup.controls.resetPasswordConfirm;

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
    };
  }

  checkPassword1Strength() {
    var control = this.resetPasswordForm.controls.resetPassword;
    var score = zxcvbn(control.value).score;
    // if we don't confirm to our own validation, override the score to zero.
    score = (control.valid) ? score : 0;
    this.passwordStrengthInfo.password1Strength = score;
    this.getPassword1StrengthClass();
  }

  checkPassword2Strength() {
    var control = this.resetPasswordForm.controls.resetPasswordConfirm;
    var score = zxcvbn(control.value).score;
    // if we don't confirm to our own validation, override the score to zero.
    score = (control.valid) ? score : 0;
    this.passwordStrengthInfo.password2Strength = score;
    this.getPassword2StrengthClass();
  }

  getPassword1StrengthClass() {
    var obj = {
      val: this.passwordStrengthInfo.password1Strength,
      lbl: this.passwordStrengthInfo.password1StrengthText
    };

    var cssClass = this.getPasswordStrengthClass(obj, false);
    this.passwordStrengthInfo.password1StrengthText = obj.lbl;
    return cssClass;
  }

  getPassword2StrengthClass() {
    var obj = {
      val: this.passwordStrengthInfo.password2Strength,
      lbl: this.passwordStrengthInfo.password2StrengthText
    };

    var cssClass = this.getPasswordStrengthClass(obj, true);
    this.passwordStrengthInfo.password2StrengthText = obj.lbl;
    return cssClass;
  }

  getPasswordStrengthClass(obj: any, overrideLabels: boolean) {
    var className: string;
    var prefix: string = this.translateProviderService.instant('PASSWORDSTRENGTH.strength') + ": ";
    switch(obj.val) {
      case 0: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.na'); className = "red"; break;
      case 1: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.weak'); className = "red"; break;
      case 2: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.good'); className = "green"; break;
      case 3: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.great'); className = "green"; break;
      case 4: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.superb'); className = "green"; break;
    }

    var pw2Empty = (this.resetPasswordForm.value.resetPasswordConfirm.trim().length == 0);
    var matching = (this.resetPasswordForm.value.resetPassword === this.resetPasswordForm.value.resetPasswordConfirm);

    if (overrideLabels) {
      className = (matching) ? "green" : "red";
      if (this.resetPasswordForm.value.resetPasswordConfirm.trim().length == 0) {
        obj.lbl = null;
      }
      else {
        obj.lbl = (matching) ?
          this.translateProviderService.instant('PASSWORDSTRENGTH.passwords_match') :
          this.translateProviderService.instant('PASSWORDSTRENGTH.passwords_do_not_match');
      }
    }

    this.passwordStrengthInfo.okButtonEnabled = (!pw2Empty && matching);
    this.passwordStrengthInfo.okButtonClass = (this.passwordStrengthInfo.okButtonEnabled) ? '' : 'save-button-disabled';

    return className;
  }

  togglePassword1Visibility() {
    this.passwordStrengthInfo.password1Type = (this.passwordStrengthInfo.password1Type == 'text') ? 'password' : 'text';
  }

  togglePassword2Visibility() {
    this.passwordStrengthInfo.password2Type = (this.passwordStrengthInfo.password2Type == 'text') ? 'password' : 'text';
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }
}
