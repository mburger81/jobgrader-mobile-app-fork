import { Component, OnInit, ElementRef, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { UntypedFormGroup, Validators, UntypedFormControl } from '@angular/forms';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ResetPassword } from '../../../core/models/ResetPassword';
import { ResetPasswordProviderService } from '../../../core/providers/reset-password/reset-password-provider.service';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { NetworkService } from 'src/app/core/providers/network/network-service';

@Component({
  selector: 'app-password-step-2',
  templateUrl: './step-2.page.html',
  styleUrls: ['./step-2.page.scss']
})
export class ResetPasswordStepTwoPage implements OnInit, OnDestroy {
  @ViewChild(IonContent, {}) content: IonContent;
  resetPasswordForm: UntypedFormGroup;
  signProcessing = false;
  private ngOnInitExecuted: any;
  private additionalStyleSheetId: string;

  constructor(
    private nav: NavController,
    private resetPasswordProvider: ResetPasswordProviderService,
    private utilityService: UtilityService,
    private elemRef: ElementRef,
    private api: ApiProviderService,
    private translateProviderService: TranslateProviderService,
    private toastController: ToastController,
    private _NetworkService: NetworkService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  /** @inheritDoc */
  async ngOnInit() {
    this.ngOnInitExecuted = true;
    const tagName = this.elemRef.nativeElement.tagName.toLowerCase();
    const styles: string =
        this.utilityService.calculateWhiteCircleCssParams(`${tagName} .page-content:after`);
    UtilityService.addStyleToHead(styles, this.additionalStyleSheetId = `${tagName}CircleStyle`);
    await this.componentInit();
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    UtilityService.removeStyleFromHead(this.additionalStyleSheetId);
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async nextStep() {
    this.resetPasswordForm.markAsDirty();
    this.signProcessing = true;
    if (!this.resetPasswordForm.valid) {
      return false;
    }

    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    const dataFromStorage: ResetPassword = await this.resetPasswordProvider.getResetPasswordData();
    const otpBody = {
      username: dataFromStorage.reset_username,
      otp: this.resetPasswordForm.controls.resetOTP.value
    };
    // console.log(otpBody);
    const otpIsValid = await this.api.resetPasswordOtp(otpBody).catch(e => {
      console.log(e);
      const { message } = e.error.errors[0];
      console.error(`Error validating OTP: ${JSON.stringify(e)}`);
    });
    if (!otpIsValid) {
      console.error('The entered OTP is invalid.');
      return await this.presentToast(this.translateProviderService.instant('SIGNSTEPFOUR.otpError'));
    }

    this.resetPasswordProvider.otpValid =
      this.resetPasswordForm.value.resetOTP === '1111' ? true : false;
    await this.resetPasswordProvider.saveResetPasswordData(this.resetPasswordForm.value, 2);
    this.nav.navigateForward('/reset-password/step-3');
    this.signProcessing = false;
  }

  async presentToast(message: string) {
    // const translations = {
    //   en: 'All fields should be checked',
    //   de: 'All fields should be checked',
    //   hi: 'All fields should be checked'
    // };

    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  prevStep(): void {
    this.nav.navigateBack('/reset-password/step-1');
  }

  async skipReg() {
    await this.resetPasswordProvider.skipReset();
  }

  private async componentInit() {
    this.resetPasswordForm = new UntypedFormGroup({
      resetOTP: new UntypedFormControl('', [
        Validators.required
      ])
    });
    const dataFromStorage: ResetPassword = await this.resetPasswordProvider.getResetPasswordData();

    this.setFormDataFromStorage(dataFromStorage);
    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  private setFormDataFromStorage(data: ResetPassword): void {
    if (data && data.reset_OTP) {
      this.resetPasswordForm.controls.resetOTP.setValue(data.reset_OTP);
    }
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
