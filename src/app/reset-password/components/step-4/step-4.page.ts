import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ResetPasswordProviderService } from '../../../core/providers/reset-password/reset-password-provider.service';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { NavController, IonContent } from '@ionic/angular';

@Component({
  selector: 'app-password-step-4',
  templateUrl: './step-4.page.html',
  styleUrls: ['./step-4.page.scss']
})
export class ResetPasswordStepFourPage implements OnInit, OnDestroy {
  @ViewChild(IonContent, {}) content: IonContent;
  success: boolean;
  resetPasswordStatus: boolean;
  private ngOnInitExecuted: any;
  private additionalStyleSheetId: string;

  constructor(
    private nav: NavController,
    private secureStore: SecureStorageService,
    private resetPasswordProviderService: ResetPasswordProviderService,
    private utilityService: UtilityService,
    private elemRef: ElementRef,
    private cdr: ChangeDetectorRef,
  ) {
  }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  /** @inheritDoc */
  async ngOnInit() {
    this.ngOnInitExecuted = true;
    const tagName = this.elemRef.nativeElement.tagName.toLowerCase();
    const styles: string =
        this.utilityService.calculateWhiteCircleCssParams(`${tagName} .page-content:after`);
    UtilityService.addStyleToHead(styles, this.additionalStyleSheetId = `${tagName}CircleStyle`);
    await this.componentInit();
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    UtilityService.removeStyleFromHead(this.additionalStyleSheetId);
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async nextStep() {
    await this.secureStore.removeValue(SecureStorageKey.resetPassword, false).then(() => {
      this.nav.navigateRoot('/login?from=resetpassword');
    });
  }

  prevStep(): void {
    this.nav.navigateBack('/reset-password/step-1');
  }

  async skipReg() {
    await this.resetPasswordProviderService.skipReset();
  }

  private async componentInit() {
    this.success = this.resetPasswordProviderService.checkIsOtpValid();
    this.resetPasswordStatus = (await this.secureStore.getValue(SecureStorageKey.resetPasswordStatus, false)) === 'true';
    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
