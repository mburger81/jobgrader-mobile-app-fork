import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { ResetPasswordPage } from './reset-password.page';
// import { ResetPasswordStepOnePageModule } from '../reset-password/components';

import { ResetPasswordProviderGuard } from '../core/guards/reset-password/reset-password-provider.guard';

const routes: Routes = [
  {
    path: '',
    component: ResetPasswordPage,
    children:
    [
      {
        path: 'step-1',
        loadChildren: () => import('../reset-password/components/step-1/step-1.module').then(m => m.ResetPasswordStepOnePageModule)
      },
      {
        path: 'step-2',
        // canActivate: [ResetPasswordProviderGuard],
        loadChildren: () => import('../reset-password/components/step-2/step-2.module').then(m => m.ResetPasswordStepTwoPageModule)
      },
      {
        path: 'step-3',
        // canActivate: [ResetPasswordProviderGuard],
        loadChildren: () => import('../reset-password/components/step-3/step-3.module').then(m => m.ResetPasswordStepThreePageModule)
      },
      {
        path: 'step-4',
        // canActivate: [ResetPasswordProviderGuard],
        loadChildren: () => import('../reset-password/components/step-4/step-4.module').then(m => m.ResetPasswordStepFourPageModule)
      },
      {
        path: '',
        redirectTo: 'step-1',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [ResetPasswordPage]
})
export class ResetPasswordPageModule {}

