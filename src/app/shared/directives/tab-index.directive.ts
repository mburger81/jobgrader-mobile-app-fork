import { Directive, Input, ElementRef, HostListener, OnInit } from '@angular/core';
import { TabService } from './tab.service';
import { UtilityService } from '../../core/providers/utillity/utility.service';
type IKNOWISNUMBER = any;
type IKNOWISSTRING = any;

@Directive({
    selector: '[appTabIndex]'
})
export class TabDirective implements OnInit {

    private _index: number;
    public get index(): IKNOWISNUMBER {
        return this._index;
    }
    /** Setting the index of the element */
    @Input('appTabIndex')
    public set index(i: IKNOWISSTRING) {
        this._index = parseInt(i, 10);
    }
    /** Host listener for on enter */
    @HostListener('keydown', ['$event'])
    public onInput(e: any) {
        if (e.which === 13) {
            this.tabService.selectedInput.next(this.index + 1);
            e.preventDefault();
        }
    }
    constructor(private el: ElementRef, private tabService: TabService) {}

    /** @inheritDoc */
    ngOnInit() {
        this.tabService.selectedInput.subscribe((i) => {
            if (i === this.index) {
                UtilityService.setTimeout().subscribe(_ => {
                    this.el.nativeElement.setFocus  ? this.el.nativeElement.setFocus() : this.el.nativeElement.focus();
                    // this.el.nativeElement.children[0].focus();
                });
            }
        });
    }
}
