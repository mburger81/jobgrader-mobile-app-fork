import {
  AfterViewInit,
  Component,
  EventEmitter,
  forwardRef,
  Host,
  HostListener,
  Input,
  Optional,
  Output,
  SkipSelf,
  ViewChild
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from '@angular/forms';
import { Country } from '../../../kyc/interfaces/country.interface';
import { IonSelect, AlertController } from '@ionic/angular';
import { CountryMode, SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { environment } from 'src/environments/environment';
import { KycPins } from 'src/app/core/providers/device/udid.enum';
import { KycService } from 'src/app/kyc/services/kyc.service';

@Component({
  selector: 'app-country-selection-item',
  templateUrl: './country-selection-item.component.html',
  styleUrls: ['./country-selection-item.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CountrySelectionItemComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CountrySelectionItemComponent),
      multi: true
    }
  ]
})
export class CountrySelectionItemComponent implements AfterViewInit, ControlValueAccessor, Validator {

  /** The name of the form control */
  @Input()
  public formControlName: string;

  /** If the field is required */
  @Input()
  public required: boolean;

  /** The label to put out */
  @Input()
  public label = '';

  /** If the value should be set back after select */
  @Input()
  public setBackAfterSelect = false;

  /**
   * Sets the mode for the flags
   */
  @Input()
  public mode: CountryMode = CountryMode.STANDARD;

  /** Reference to the ion select */
  @ViewChild('select')
  public select: IonSelect;

  /** Emit the value to the outside */
  @Output()
  public valueChanged: EventEmitter<Country> = new EventEmitter<Country>();
  
  @Output()
  public selectOndatoEmitter: EventEmitter<string> = new EventEmitter<string>();

  /** The abstract control */
  public control: AbstractControl;

  constructor(
      @Optional() @Host() @SkipSelf()
      private controlContainer: ControlContainer,
      public signService: SignProviderService,
      private _AlertController: AlertController,
      private _SecureStorageService: SecureStorageService,
      private _KycService: KycService,
      private _TranslateProviderService: TranslateProviderService
  ) { }

  /** @inheritDoc */
  public ngAfterViewInit(): void {
    this.control = this.controlContainer.control.get(this.formControlName);
  }

  /** Callback for changes that happen to the control */
  public onChange = (selection: number[]) => { };

  /**
   * The control value accessors on touched
   * callback
   */
  private onTouched: () => void = () => { };


  /**
   * Registering the onChange handler for later use
   */
  public registerOnChange(fn: (selection: number[]) => void): void {
    this.onChange = fn;
  }

  /**
   * Registering the onTouched handler for later use
   */
  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  /**
   * Possibility to write a value from outside
   */
  public writeValue(selection: Country): void {
    this.selectChange({detail: {value: selection}});
  }

  /** Callback for selection change */
  public selectChange(value: Partial<CustomEvent>): void {
    if (value) {
      this.valueChanged.emit(value.detail.value);
      if (this.setBackAfterSelect && this.select) {
        this.select.value = undefined;
      }
    }
  }

  /**
   * emit Ondato selection as the KYC provider
   */

  private selectOndato() {
    console.log('Emiter code begins');
    this.selectOndatoEmitter.emit('ONDATO');
    console.log('Emiter code ends');
  }

  /** Select open from item */
  public async openSelect(event: Event): Promise<void> {
    var proceedAsUsual = () => {
      this.signService.addStyleSelectToHead();
      event.stopPropagation();
      event.preventDefault();
      this.select.open().then(_ => {
        this.signService.addSelectBoxAdditionalElements(this.mode);
      });
    }
    var proceedAsUsualwithOndato = () => {

      this._KycService.isUserAllowedToUseChatMarketplace().then((val) => {  //})
      
      // this._SecureStorageService.getValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, false).then((val) => {
        
        if(val) {
          var placeholder = environment.production ? '' : KycPins.veriff;
          this._AlertController.create({
            mode: 'ios',
            header: this._TranslateProviderService.instant('KYC.KYCPIN.header'),
            message: this._TranslateProviderService.instant('KYC.KYCPIN.message1'),
            inputs: [ { name: 'kycpin', placeholder: placeholder, type: 'tel'} ],
            buttons: [ {
              text: this._TranslateProviderService.instant('SETTINGS.ok'), 
              handler: (data) => {
                  (data.kycpin == KycPins.veriff) ? proceedAsUsual() :
                    this._AlertController.create({
                      mode: 'ios',
                      message: this._TranslateProviderService.instant('KYC.KYCPIN.message2'),
                      buttons: [ { text: this._TranslateProviderService.instant('SETTINGS.ok'), role: 'cancel', handler: () => { } } ]
                    }).then((al) => al.present());
                }
              }, { text: this._TranslateProviderService.instant('SETTINGS.cancel'), role: 'cancel', handler: () => { } }
            ]
          }).then((alert) => {
            alert.present();
          })
        } else {
          // this.selectOndato();
          proceedAsUsual();
        }
      })
    }

    proceedAsUsualwithOndato();

  }

  public validate(control: AbstractControl): ValidationErrors | null {
    if (this.required) {
      if (this.control && !this.control.value) {
        return { required: 'Country select is required' };
      }
    }
    return null;
  }

  /** Callback for click on the document */
  @HostListener('document:click', ['$event', '$event.target'])
  public onClick(event: MouseEvent, targetElement: HTMLElement): void {
    if ( !targetElement ) {
      return;
    }
    const ids = ['select-flag-1', 'select-flag-2'];
    if ( ids.indexOf(targetElement.id) !== -1 ) {
      this.signService.addSelectBoxAdditionalElements(this.mode);
    }
  }

  /** Getter for the country list as reference */
  public get countryList() {
    switch (this.mode) {
      case CountryMode.STANDARD:
        return this.signService.countries;
      case CountryMode.IDCARD:
        return this.signService.idCardSupportedCountries;
      case CountryMode.DRIVERLICENSE:
        return this.signService.dlSupportedCountries
      case CountryMode.IDENTITY:
        return this.signService.identityCountries
    }
  }
}
