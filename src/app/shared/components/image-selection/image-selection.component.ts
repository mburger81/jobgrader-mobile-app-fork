import { Component, EventEmitter, Input, NgZone, OnInit, Output } from '@angular/core';
import { ActionSheetController, LoadingController, Platform } from '@ionic/angular';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { Camera, CameraDirection, CameraResultType, CameraSource, ImageOptions, Photo } from '@capacitor/camera';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { LockService } from '../../../lock-screen/lock.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { AppStateService } from '../../../core/providers/app-state/app-state.service';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import * as gifshot from 'gifshot';
import { KycService } from 'src/app/kyc/services/kyc.service';

declare var window: any;
declare var VideoEditor: any;

export interface GifSetting {
  noOfFrames?: number;
  frameFloor?: boolean;
  frameDuration?: number;
  numFrames?: number;
  interval?: number;
  gifWidth?: number;
  gifHeight?: number;
  videoDuration?: number;
  gifInterval?: number;
  reverse?: boolean;
  aspectRatio?: number;
}

export interface ImageSelectionPayload {
  base64Image: string;
  fileName: string;
  format: string;
  reloadApp?: boolean
}

@Component({
  selector: 'app-image-selection',
  templateUrl: './image-selection.component.html',
  styleUrls: ['./image-selection.component.scss'],
})
export class ImageSelectionComponent implements OnInit {
  private static defaultImage = '../../../../assets/job-user.svg';

  /** If the user in an innovator */

  private gifSetting: GifSetting = null;

  /** The picture to be shown */
  @Input()
  public set profilePictureSrc(src: string) {
    this.pictureSrc = !!src ? src : ImageSelectionComponent.defaultImage;
  }

  /** Event emitter to tell the outer component that image has changed */
  @Output()
  public imageChanged = new EventEmitter<ImageSelectionPayload>();

  /** If is in editMode */
  @Input()
  public editMode = false;

  /** If the innovator badge should be shown */
  @Input()
  public showInnovatorBadge = false;

  public displayVerifiedTick = false;

  /** The variable to use in the src tag of the view */
  public pictureSrc = ImageSelectionComponent.defaultImage;

  constructor(
      private _platform: Platform,
      private _Zone: NgZone,
      private _LoadingController: LoadingController,
      private actionSheetController: ActionSheetController,
      private translateService: TranslateProviderService,
      private lockService: LockService,
      private _KycService: KycService,
      private apiProviderService: ApiProviderService,
      private secureStorageService: SecureStorageService,
      private appStateService: AppStateService
  ) { }

  public ngOnInit(): void {

    this.secureStorageService.getValue(SecureStorageKey.gifSetting, false).then(g =>
      { this.gifSetting = !!g ? JSON.parse(g) : {
        noOfFrames: 15,
        numFrames: 15,
        frameFloor: false,
        frameDuration: 0.05,
        interval: 0.05,
        gifWidth: 150,
        gifHeight: 150,
        videoDuration: 2,
        gifInterval: 0.0001,
        reverse: true
    }});

    this._KycService.isUserAllowedToUseChatMarketplace().then((displayVerifiedTick) => {
      this.displayVerifiedTick = displayVerifiedTick;
    })

  }

  public async showChangePicture() {
    if ( this.profilePictureSrc !== ImageSelectionComponent.defaultImage ) {
      var buttons1 = ( this._platform.is('android') ? [
        { text: this.translateService.instant('PERSONALDETAILS.takePicture'), icon: 'camera', handler: async () =>{ this.takePicture(); return true;} },
        // { text: this.translateService.instant('PERSONALDETAILS.takeVideo'), icon: 'videocam', handler: async () => { this.takeVideo(); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.choosePicture'), icon: 'images', handler: async () => { console.log('choosePicture'); await this.choosePicture(); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.removePicture'), icon: 'trash', handler: () => { console.log('removePicture'); this.removePicture(); return true; } },
        { text: this.translateService.instant('SETTINGS.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('cancel'); } }
      ] : [
        { text: this.translateService.instant('PERSONALDETAILS.takePicture'), icon: 'camera', handler: async () => { await this.takePicture(); return true; } },
        // { text: this.translateService.instant('PERSONALDETAILS.takeVideo'), icon: 'videocam', handler: async () => { this.takeVideo(); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.choosePicture'), icon: 'images', handler: async () => { console.log('choosePicture'); await this.choosePicture(); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.removePicture'), icon: 'trash', handler: () => { console.log('removePicture'); this.removePicture(); return true; } },
        { text: this.translateService.instant('SETTINGS.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('cancel'); } }
      ] );
      const alert = await this.actionSheetController.create({
        mode: 'md',
        header: this.translateService.instant('GENERAL.pleaseChoose'),
        buttons: buttons1
      });
      await alert.present();
    } else {
      var buttons2 = this._platform.is('android') ? [
        { text: this.translateService.instant('PERSONALDETAILS.takePicture'), icon: 'camera', handler: async () =>{ this.takePicture(); return true;} },
        // { text: this.translateService.instant('PERSONALDETAILS.takeVideo'), icon: 'videocam', handler: async () => { await this.takeVideo(); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.choosePicture'), icon: 'images', handler: async () => { await this.choosePicture(); return true; } },
        { text: this.translateService.instant('SETTINGS.cancel'), icon: 'close', role: 'cancel', handler: () => {} }
      ] : [
        { text: this.translateService.instant('PERSONALDETAILS.takePicture'), icon: 'camera', handler: async () => { await this.takePicture(); return true; } },
        // { text: this.translateService.instant('PERSONALDETAILS.takeVideo'), icon: 'videocam', handler: async () => { await this.takeVideo(); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.choosePicture'), icon: 'images', handler: async () => { await this.choosePicture(); return true; } },
        { text: this.translateService.instant('SETTINGS.cancel'), icon: 'close', role: 'cancel', handler: () => {} }
      ]
      const alert = await this.actionSheetController.create({
        mode: 'md',
        header: this.translateService.instant('GENERAL.pleaseChoose'),
        buttons: buttons2
      });

      await alert.present();
    }
  }

  // 2020-12-18 18:52:48.392529+0100 helix id[776:187806] ERROR: error JSON.stringify()ing argument: TypeError: JSON.stringify cannot serialize cyclic structures.

  async takePicture() {
    const options: ImageOptions
     = {
      quality: this.isiOS() ? 25 : 100,
      source: CameraSource.Camera,
      resultType: CameraResultType.DataUrl,
      // mburger: not used on capacitor plugin
      // encodingType: this.camera.EncodingType.JPEG,
      direction: CameraDirection.Front,
      // allowEditing: true,
      saveToGallery: true,
      // mburger: not used on capacitor plugin
      // mediaType: this.camera.MediaType.PICTURE,
      height: 500,
      width: 500
    };
    // TODO mburger: why just for iOS?
    if(this.isiOS()){
      options.allowEditing = true;
    }
    if(this.isAndroid()){
      options.correctOrientation = true;
    }
    this.lockService.removeResume();
    UtilityService.setTimeout(1200).subscribe(_ => {
      try {
        Camera.getPhoto(options).then(async (imageData: Photo) => {
          console.info("=====> imageData.dataUrl", imageData.dataUrl);
          console.info("=====> imageData.exif", imageData.exif);
          console.info("=====> imageData.format", imageData.format);
          console.info("=====> imageData.saved", imageData.saved);
          console.info("=====> imageData.path", imageData.path);
          console.info("=====> imageData.webPath", imageData.webPath);

          const base64Image = imageData.dataUrl
          let currentTime = +new Date();
          const fileName = 'image_' + currentTime.toString();
          // const fileName = 'image_' + UtilityService.randomStringGenerator(6);
          try {
            this.imageChanged.emit({base64Image, fileName, format: 'image/jpeg'});
          } catch (e) {
            console.log(e);
            // TODO @Ansik React on errors
          } finally {
            this.lockService.addResume();
          }
        }, _ => {
          this.lockService.addResume();
        });
      } catch (e) {
        this.lockService.addResume();
      }
    });
  }


  // async takeVideo() {
  //   this.lockService.removeResume();

  //   window.plugins.videocaptureplus.captureVideo((mediaFiles) => {
  //     if (mediaFiles && mediaFiles.length > 0) {
  //       var fileUri = mediaFiles[0].fullPath;
  //       if (!fileUri) {
  //         // alert("Could not get video path");
  //         return;
  //       }

  //       // Only convert if on iOS as we need mp4. Android is always mp4.
  //       if (this.isiOS()) {
  //         // Transcode to mp4 so we can use the gifshot library after.
  //         this.compressVideo(fileUri).then((compressedFileUri) => {
  //           if (compressedFileUri) {
  //             // Get info, the stills from info, then make gif.
  //             VideoEditor.getVideoInfo(
  //               (info) => {
  //                 console.log("video info: " + JSON.stringify(info))
  //                 this.gifSetting.aspectRatio = info.height / info.width
  //                 this.getStills(compressedFileUri, info.duration).then((thumbs) => {
  //                   this.getGifByThumbs(thumbs);
  //                   this.lockService.addResume();
  //                 });
  //               },
  //               (err) => {
  //                 // alert(JSON.stringify(err));
  //               },
  //               {
  //                 fileUri: this.getFileUriWithFilePrefix(compressedFileUri)
  //               }
  //             );
  //           }
  //         });
  //       }
  //       else {
  //         // Android - do it with the video src, as the thumbnail method is wonky as hell.
  //         this.getGifByVideo(fileUri);
  //         this.lockService.addResume();
  //       }

  //     }
  //     else {
  //       // alert("No media file received");
  //       return;
  //     }
  //   },
  //     (error) => {
  //       // alert(JSON.stringify(error));
  //     },
  //     {
  //       limit: 1,
  //       duration: this.gifSetting ? this.gifSetting.videoDuration : 6,
  //       highquality: false,
  //       frontcamera: true,
  //       // orientation: 'square',
  //       // height: this.gifSetting ? this.gifSetting.gifHeight : 200,
  //       // width: this.gifSetting ? this.gifSetting.gifWidth : 200
  //     }
  //   );
  // }

  // getStills(compressedFileUri: string, duration: number): Promise<string[]> {
  //   return new Promise(resolve => {

  //     if (this.isiOS()) {
  //       var factor = 73.3964; //god knows why: https://github.com/jbavari/cordova-plugin-video-editor/issues/116
  //       duration = (duration/factor);
  //     }

  //     var noOfFrames = this.gifSetting ? this.gifSetting.noOfFrames : 6;
  //     console.log("noOfFrames: " + noOfFrames)
  //     var interval = duration / noOfFrames;
  //     var frameTimes: number[] = [];
  //     var reverseArray: number[] = [];
  //     var atTime: number;
  //     for(var i = 0; i < noOfFrames; i++) {
  //       atTime = (i + 1) * interval;
  //       if(this.gifSetting.frameFloor){
  //         frameTimes.push(Math.floor(atTime));
  //         reverseArray.push(Math.floor(atTime));
  //       } else {
  //         frameTimes.push(Math.floor(atTime) + (Math.floor((atTime - Math.floor(atTime))*100)/100));
  //         reverseArray.push(Math.floor(atTime) + (Math.floor((atTime - Math.floor(atTime))*100)/100));
  //       }
  //     }

  //     if(this.gifSetting.reverse){
  //       reverseArray.reverse()
  //       reverseArray.forEach(l => frameTimes.push(l))
  //     }

  //     // first frame always seems to be black, so let's ignore the first frame for now.
  //     frameTimes.splice(0, 1);

  //     console.log('Real duration: ' + duration);
  //     console.log('Frame interval: ' + interval);
  //     console.log('Frame atTimes: ' + JSON.stringify(frameTimes));

  //     compressedFileUri = this.getFileUriWithFilePrefix(compressedFileUri);

  //     var thumbs: any[] = [];
  //     for(var i = 0; i < frameTimes.length; i++) {
  //       VideoEditor.createThumbnail(
  //         (success) => {
  //           thumbs.push(success);
  //           if (thumbs.length == frameTimes.length) {
  //             resolve(thumbs);
  //           }
  //         },
  //         (err) => {
  //           // alert('createThumbnail: ' + JSON.stringify(err));
  //           resolve(null);
  //         },
  //         {
  //           fileUri: compressedFileUri,
  //           outputFileName: this.generateUUID(),
  //           atTime: frameTimes[i],
  //           // height: this.gifSetting ? this.gifSetting.gifHeight : 200,
  //           // width: this.gifSetting ? this.gifSetting.gifWidth : 200,
  //         }
  //       );
  //     }
  //   });
  // }

  // getGifByThumbs(thumbs: string[]) {
  //   // Amend all thumb urls
  //   for (var i = 0; i < thumbs.length; i++) {
  //     thumbs[i] = this.getAsWebViewCorrectUrlMobile(thumbs[i]);
  //   }

  //   var options = {
  //     'images': thumbs,
  //     'numFrames': this.gifSetting ? this.gifSetting.gifInterval : 1,
  //     'frameDuration': this.gifSetting ? this.gifSetting.frameDuration : 5,
  //     'gifWidth': this.gifSetting ? this.gifSetting.gifWidth : 200,
  //     'gifHeight': this.gifSetting ? (this.gifSetting.aspectRatio ? (this.gifSetting.aspectRatio * this.gifSetting.gifWidth) : this.gifSetting.gifHeight ) : 200
  //   };
  //   this.showLoading(this.translateService.instant('LOADER.makingGIF')).then(() => {
  //     gifshot.createGIF(options, (result) => {
  //       if (result && !result.error && result.image) {
  //         console.log(JSON.stringify(result.image))
  //         this.userPhotoServiceAkita.updatePhoto(result.image);
  //         this.hideLoading().then(() => {
  //           let currentTime = +new Date();
  //           const fileName = 'image_' + currentTime.toString();
  //           const format = 'image/gif';
  //           try {
  //             this.imageChanged.emit({base64Image: result.image, fileName: fileName, format: format});
  //           } catch (e) {
  //             console.error(e);
  //             // TODO @Ansik React on errors
  //           } finally {
  //             this.lockService.addResume();
  //           }
  //         });
  //       }
  //       else {
  //         // alert(result.error + ": " + result.errorMsg);
  //       }
  //     });
  //   });
  // }

  // getGifByVideo(videoSrcUri: string) {
  //   var options = {
  //     'video': this.getAsWebViewCorrectUrlMobile(videoSrcUri),
  //     'numFrames': this.gifSetting ? this.gifSetting.numFrames : 5,
  //     'frameDuration': this.gifSetting ? this.gifSetting.frameDuration : 5,
  //     'interval': this.gifSetting ? this.gifSetting.interval : 1,
  //     'gifWidth': this.gifSetting ? this.gifSetting.gifWidth : 200,
  //     'gifHeight': this.gifSetting ? (this.gifSetting.aspectRatio ? (this.gifSetting.aspectRatio * this.gifSetting.gifWidth) : this.gifSetting.gifHeight ) : 200
  //   }

  //   this.showLoading('Making gif...').then(() => {

  //     try {
  //       gifshot.createGIF(options, (result) => {
  //         if (result && !result.error && result.image) {
  //           this.hideLoading().then(() => {
  //             let currentTime = +new Date();
  //             const fileName = 'image_' + currentTime.toString();
  //             const format = 'image/gif';
  //             try {
  //               this.imageChanged.emit({base64Image: result.image, fileName: fileName, format: format, reloadApp: true });
  //             } catch (e) {
  //               console.error(e);
  //               // TODO @Ansik React on errors
  //             } finally {
  //               // this.lockService.addResume();
  //             }
  //           });
  //         }
  //         else {
  //           // alert(result.error + ": " + result.errorMsg);
  //         }
  //       });
  //     }
  //     catch(e) {
  //       // alert(e)
  //     }

  //   });
  // }

  getAsWebViewCorrectUrlMobile(fileUri: string) {
    if (this.isiOS()) {
      return window.Ionic.WebView.convertFileSrc(fileUri);
    }
    else {
      // correct file paths on Android due to https://github.com/ionic-team/cordova-plugin-ionic-webview/issues/598
      var filePathCorrected = window.Ionic.WebView.convertFileSrc(fileUri);
      filePathCorrected = filePathCorrected.replace('file:/', "https://localhost/_app_file_/");
      return filePathCorrected;
    }
  }

  // compressVideo(inputFileUri: string): Promise<string> {
  //   return new Promise(resolve => {
  //     this.showLoading('Starting compression job').then(() => {
  //       setTimeout(() => {
  //         var settings = { name: 'Best', width: 1920, bitrate: 3000000, fps: 30 };
  //         var guid = this.generateUUID();
  //         inputFileUri = this.getFileUriWithFilePrefix(inputFileUri);

  //         VideoEditor.transcodeVideo(
  //           (successFileUrl) => {
  //             this.hideLoading().then(() => {
  //               resolve(successFileUrl);
  //             });
  //           },
  //           (err) => {
  //             // alert('err: ' + JSON.stringify(err));
  //             resolve(null);
  //           },
  //           <VideoEditorTranscodeProperties>{
  //             fileUri: inputFileUri,
  //             outputFileName: 'helix-' + guid,
  //             outputFileType: VideoEditorOptions.OutputFileType.MPEG4,
  //             optimizeForNetworkUse: VideoEditorOptions.OptimizeForNetworkUse.YES, // ios only
  //             saveToLibrary: true,
  //             deleteInputFile: false, // android only - defaults to false
  //             width: settings.width,
  //             videoBitrate: settings.bitrate,
  //             fps: settings.fps,
  //             maintainAspectRatio: true, // ios only - defaults to true
  //             progress: (progress) => {
  //               this._Zone.run(() => {
  //                 // Workaround that Android emits the progress from 0 to 1, but iOS is 0-100.
  //                 let percentComplete = (this.isAndroid()) ? parseInt((progress * 100).toString()) : parseInt(progress);
  //                 this.setLoadingContent('Progress: ' + percentComplete + '%');
  //               });
  //             }
  //           }
  //         );
  //       }, 150);
  //     });
  //   });
  // }

  isAndroid() {
    return this._platform.is('android');
  }

  isiOS() {
    return this._platform.is('ios');
  }

  generateUUID() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
      d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  getFileUriWithFilePrefix(fileUri: string) {
    var prefix = 'file:///';
    var filePathCorrected = prefix + fileUri;
    filePathCorrected = filePathCorrected.replace('file:///file:///', prefix);
    filePathCorrected = filePathCorrected.replace('file:///file:/', prefix);
    filePathCorrected = filePathCorrected.replace('file:////', prefix);
    return filePathCorrected;
  }

  setLoadingContent(content: string) {
    this._LoadingController.getTop().then(loading => {
      if (loading) {
        loading.message = content;
      }
    });
  }

  async showLoading(msg: string) {
    const loading = await this._LoadingController.create({
      message: msg,
      mode: 'ios'
    });
    await loading.present();
  }

  hideLoading() {
    return new Promise<void>(resolve => {
      this._LoadingController.getTop().then(topLoadingComponent => {
        if (!topLoadingComponent) {
          resolve();
        }
        else {
          this._LoadingController.dismiss().then(() => {
            resolve();
          })
        }
      });
    });
  }


  async choosePicture() {
    const options: ImageOptions = {
      quality: this.isiOS() ? 25 : 100,
      source: CameraSource.Photos,
      resultType: CameraResultType.DataUrl,
      // mburger: not used on capacitor plugin
      // encodingType: this.camera.EncodingType.JPEG,
      // allowEditing: true,
      // mburger: not used on capacitor plugin
      // mediaType: this.camera.MediaType.PICTURE,
      width: 500,
      height: 500
    };
    // TODO mburger: why just for iOS?
    if(this.isiOS()){
      options.allowEditing = true;
    }
    if(this.isAndroid()) {
      options.correctOrientation = true;
    }
    this.lockService.removeResume();
    UtilityService.setTimeout(500).subscribe(_ => {
      try {
        Camera.getPhoto(options).then(async (imageData: Photo) => {
          const base64Image = imageData.dataUrl;
          let currentTime = +new Date();
          const fileName = 'image_' + currentTime.toString();
          // const fileName = 'image_' + UtilityService.randomStringGenerator(6);
          const format = imageData.format;
          try {
            this.imageChanged.emit({base64Image, fileName, format});
          } catch (e) {
            console.error(e);
            // TODO @Ansik React on errors
          } finally {
            this.lockService.addResume();
          }
        }, _ => {
          this.lockService.addResume();
        });
      } catch (e) {
        this.lockService.addResume();
      }
    });
  }

  /** Method to delete the user picture */
  public removePicture(): void {
    this.imageChanged.emit({base64Image: null, fileName: 'Default', format: null});
  }

}
