import { Injectable } from '@angular/core';
import { NgZone } from '@angular/core';
import { ActionSheetController, ToastController, LoadingController, Platform } from '@ionic/angular';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { Camera, CameraDirection, CameraResultType, CameraSource, ImageOptions, Photo } from '@capacitor/camera';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { LockService } from '../../../lock-screen/lock.service';
import { NetworkService } from '../../../core/providers/network/network-service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { AppStateService } from '../../../core/providers/app-state/app-state.service';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import * as gifshot from 'gifshot';
import { User } from '../../../core/models/User';
import { UserProviderService } from '../../../core/providers/user/user-provider.service';
import { UserPhotoServiceAkita } from '../../../core/providers/state/user-photo/user-photo.service';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';
import { AlertsProviderService } from 'src/app/core/providers/alerts/alerts-provider.service';

declare var window: any;
declare var VideoEditor: any;


export interface GifSetting {
  noOfFrames?: number;
  frameFloor?: boolean;
  frameDuration?: number;
  numFrames?: number;
  interval?: number;
  gifWidth?: number;
  gifHeight?: number;
  videoDuration?: number;
  gifInterval?: number;
  reverse?: boolean;
  aspectRatio?: number;
}

export interface ImageSelectionPayload {
  base64Image: string;
  fileName: string;
  format: string;
  reloadApp?: boolean
}

@Injectable({
  providedIn: 'root'
})

export class ImageSelectionService {

  private gifSetting: GifSetting = null;
  // private defaultImage = '../../assets/job-user.svg';
  private user: User;

  constructor(
    private _platform: Platform,
    private _Zone: NgZone,
    private _LoadingController: LoadingController,
    private actionSheetController: ActionSheetController,
    private translateService: TranslateProviderService,
    private lockService: LockService,
    private apiProviderService: ApiProviderService,
    private secureStorageService: SecureStorageService,
    private appStateService: AppStateService,
    private _NetworkService: NetworkService,
    private _ToastController: ToastController,
    private alertService: AlertsProviderService,
    private _OpenNativeSettings: OpenNativeSettings,
    private _UserProviderService: UserProviderService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) { }

  private async init() {
    var g = await this.secureStorageService.getValue(SecureStorageKey.gifSetting, false);
    this.gifSetting = !!g ? JSON.parse(g) : {
      noOfFrames: 15,
      numFrames: 15,
      frameFloor: false,
      frameDuration: 0.05,
      interval: 0.05,
      gifWidth: 150,
      gifHeight: 150,
      videoDuration: 2,
      gifInterval: 0.0001,
      reverse: true
  };
    this.user = await this._UserProviderService.getUser();
  }

  public async showChangePicture(): Promise<string> {
    await this.init();
    return new Promise((resolve, reject) => {
      var buttons = (this._platform.is('android') ? [
        // { text: this.translateService.instant('PERSONALDETAILS.takePicture'), icon: 'camera', handler: async () => { console.log('takePicture'); resolve(await this.takePicture()); } },
        // { text: this.translateService.instant('PERSONALDETAILS.takeVideo'), icon: 'videocam', handler: async () => { resolve(await this.takeVideo()); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.choosePicture'), icon: 'images', handler: async () => { console.log('choosePicture'); resolve(await this.choosePicture()); } },
        { text: this.translateService.instant('PERSONALDETAILS.removePicture'), icon: 'trash', handler: async () => { console.log('removePicture'); resolve(await this.removePicture()); } },
        { text: this.translateService.instant('SETTINGS.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('cancel'); reject(); } }
      ] : [
        { text: this.translateService.instant('PERSONALDETAILS.takePicture'), icon: 'camera', handler: async () => { console.log('takePicture'); resolve(await this.takePicture()); } },
        // { text: this.translateService.instant('PERSONALDETAILS.takeVideo'), icon: 'videocam', handler: async () => { resolve(await this.takeVideo()); return true; } },
        { text: this.translateService.instant('PERSONALDETAILS.choosePicture'), icon: 'images', handler: async () => { console.log('choosePicture'); resolve(await this.choosePicture()); } },
        { text: this.translateService.instant('PERSONALDETAILS.removePicture'), icon: 'trash', handler: async () => { console.log('removePicture'); resolve(await this.removePicture()); } },
        { text: this.translateService.instant('SETTINGS.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('cancel'); reject(); } }
      ] );
      this.actionSheetController.create({
        mode: 'md',
        header: this.translateService.instant('GENERAL.pleaseChoose'),
        buttons
      }).then(alert => alert.present());
    })
  }

  // 2020-12-18 18:52:48.392529+0100 helix id[776:187806] ERROR: error JSON.stringify()ing argument: TypeError: JSON.stringify cannot serialize cyclic structures.

  async takePicture(): Promise<string> {
    console.log("async takePicture() selected!");
    return new Promise((resolve, reject) => {
      var options: ImageOptions = {
        quality: this.isiOS() ? 25 : 100,
        source: CameraSource.Camera,
        resultType: CameraResultType.DataUrl,
        // mburger: not used on capacitor plugin
        // encodingType: this.camera.EncodingType.JPEG,
        direction: CameraDirection.Front,
        // allowEditing: true,
        saveToGallery: true,
        // mburger: not used on capacitor plugin
        // mediaType: this.camera.MediaType.PICTURE,
        width: 500,
        height: 500
      };
      console.log("options 1", options);
      // TODO mburger: why just for iOS?
      if(this.isiOS()){
        options.allowEditing = true;
      }
      if(this.isAndroid()){
        options.correctOrientation = true;
      }
      console.log("options 2", options);
      this.lockService.removeResume();
      UtilityService.setTimeout(1200).subscribe(_ => {
        try {
          Camera.checkPermissions().then(permissions => {
            console.log("Camera permissions", permissions);
            console.log("Camera permissions, camera", permissions.camera);
            if(permissions.camera == 'denied') {
              console.log("Camera.requestPermissions camera init");
              // var status = await Camera.requestPermissions({ permissions: ['camera'] });
              this.showSettingsPrompt()
            } else {
              Camera.getPhoto(options).then(async (imageData: Photo) => {
                console.info("=====> imageData.dataUrl", imageData.dataUrl);
                console.info("=====> imageData.exif", imageData.exif);
                console.info("=====> imageData.format", imageData.format);
                console.info("=====> imageData.saved", imageData.saved);
                console.info("=====> imageData.path", imageData.path);
                console.info("=====> imageData.webPath", imageData.webPath);
    
                let currentTime = +new Date();
                const fileName = 'image_' + currentTime.toString();
                // const fileName = 'image_' + UtilityService.randomStringGenerator(6);
                try {
                  const base64Image = imageData.dataUrl;
                  var im = await this.onImageChanged({base64Image, fileName, format: 'image/jpeg'});
                  resolve(im);
                } catch (e) {
                  console.log("Camera error 3", e);
                  // TODO @Ansik React on errors
                } finally {
                  this.lockService.addResume();
                }
              }).catch(e => {
                console.log("Camera error 2", _);
                this.lockService.addResume();
              });
            }
          })
          // Camera.getPhoto(options).then(async (imageData: Photo) => {
          //   console.info("=====> imageData.dataUrl", imageData.dataUrl);
          //   console.info("=====> imageData.exif", imageData.exif);
          //   console.info("=====> imageData.format", imageData.format);
          //   console.info("=====> imageData.saved", imageData.saved);
          //   console.info("=====> imageData.path", imageData.path);
          //   console.info("=====> imageData.webPath", imageData.webPath);

          //   let currentTime = +new Date();
          //   const fileName = 'image_' + currentTime.toString();
          //   // const fileName = 'image_' + UtilityService.randomStringGenerator(6);
          //   try {
          //     const base64Image = imageData.dataUrl;
          //     var im = await this.onImageChanged({base64Image, fileName, format: 'image/jpeg'});
          //     resolve(im);
          //   } catch (e) {
          //     console.log("Camera error 3", e);
          //     // TODO @Ansik React on errors
          //   } finally {
          //     this.lockService.addResume();
          //   }
          // }, _ => {
          //   console.log("Camera error 2", _);
          //   this.lockService.addResume();
          // });
        } catch (e) {
          console.log("Camera error 1", e);
          this.lockService.addResume();
        }
      });
    })
  }


  // async takeVideo(): Promise<string> {
  //   this.lockService.removeResume();
  //   return new Promise((resolve, reject) => {
  //     // return new Promise((resolve, reject) => {
  //   window.plugins.videocaptureplus.captureVideo((mediaFiles) => {
  //     if (mediaFiles && mediaFiles.length > 0) {
  //       var fileUri = mediaFiles[0].fullPath;
  //       if (!fileUri) {
  //         // alert("Could not get video path");
  //         reject();
  //         return;
  //       }

  //       // Only convert if on iOS as we need mp4. Android is always mp4.
  //       if (this.isiOS()) {
  //         // Transcode to mp4 so we can use the gifshot library after.
  //         this.compressVideo(fileUri).then((compressedFileUri) => {
  //           if (compressedFileUri) {
  //             // Get info, the stills from info, then make gif.
  //             VideoEditor.getVideoInfo(
  //               (info) => {
  //                 console.log("video info: " + JSON.stringify(info))
  //                 this.gifSetting.aspectRatio = info.height / info.width
  //                 this.getStills(compressedFileUri, info.duration).then((thumbs) => {
  //                   var resp = this.getGifByThumbs(thumbs);
  //                   this.lockService.addResume();
  //                   resolve(resp);
  //                 });
  //               },
  //               (err) => {
  //                 // alert(JSON.stringify(err));
  //                 reject();
  //               },
  //               {
  //                 fileUri: this.getFileUriWithFilePrefix(compressedFileUri)
  //               }
  //             );
  //           }
  //         });
  //       }
  //       else {
  //         // Android - do it with the video src, as the thumbnail method is wonky as hell.
  //         this.getGifByVideo(fileUri);
  //         this.lockService.addResume();
  //       }

  //     }
  //     else {
  //       // alert("No media file received");
  //       return;
  //     }
  //   },
  //     (error) => {
  //       // alert(JSON.stringify(error));
  //     },
  //     {
  //       limit: 1,
  //       duration: this.gifSetting ? this.gifSetting.videoDuration : 6,
  //       highquality: false,
  //       frontcamera: true,
  //       // orientation: 'square',
  //       // height: this.gifSetting ? this.gifSetting.gifHeight : 200,
  //       // width: this.gifSetting ? this.gifSetting.gifWidth : 200
  //     }
  //   );
  //   // })
  //   })
  // }

  // getStills(compressedFileUri: string, duration: number): Promise<string[]> {
  //   return new Promise(resolve => {

  //     if (this.isiOS()) {
  //       var factor = 73.3964; //god knows why: https://github.com/jbavari/cordova-plugin-video-editor/issues/116
  //       duration = (duration/factor);
  //     }

  //     var noOfFrames = this.gifSetting ? this.gifSetting.noOfFrames : 6;
  //     console.log("noOfFrames: " + noOfFrames)
  //     var interval = duration / noOfFrames;
  //     var frameTimes: number[] = [];
  //     var reverseArray: number[] = [];
  //     var atTime: number;
  //     for(var i = 0; i < noOfFrames; i++) {
  //       atTime = (i + 1) * interval;
  //       if(this.gifSetting.frameFloor){
  //         frameTimes.push(Math.floor(atTime));
  //         reverseArray.push(Math.floor(atTime));
  //       } else {
  //         frameTimes.push(Math.floor(atTime) + (Math.floor((atTime - Math.floor(atTime))*100)/100));
  //         reverseArray.push(Math.floor(atTime) + (Math.floor((atTime - Math.floor(atTime))*100)/100));
  //       }
  //     }

  //     if(this.gifSetting.reverse){
  //       reverseArray.reverse()
  //       reverseArray.forEach(l => frameTimes.push(l))
  //     }

  //     // first frame always seems to be black, so let's ignore the first frame for now.
  //     frameTimes.splice(0, 1);

  //     console.log('Real duration: ' + duration);
  //     console.log('Frame interval: ' + interval);
  //     console.log('Frame atTimes: ' + JSON.stringify(frameTimes));

  //     compressedFileUri = this.getFileUriWithFilePrefix(compressedFileUri);

  //     var thumbs: any[] = [];
  //     for(var i = 0; i < frameTimes.length; i++) {
  //       VideoEditor.createThumbnail(
  //         (success) => {
  //           thumbs.push(success);
  //           if (thumbs.length == frameTimes.length) {
  //             resolve(thumbs);
  //           }
  //         },
  //         (err) => {
  //           // alert('createThumbnail: ' + JSON.stringify(err));
  //           resolve(null);
  //         },
  //         {
  //           fileUri: compressedFileUri,
  //           outputFileName: this.generateUUID(),
  //           atTime: frameTimes[i],
  //           // height: this.gifSetting ? this.gifSetting.gifHeight : 200,
  //           // width: this.gifSetting ? this.gifSetting.gifWidth : 200,
  //         }
  //       );
  //     }
  //   });
  // }

  // getGifByThumbs(thumbs: string[]): Promise<string> {
  //   return new Promise((resolve, reject) => {
  //     // Amend all thumb urls
  //     for (var i = 0; i < thumbs.length; i++) {
  //       thumbs[i] = this.getAsWebViewCorrectUrlMobile(thumbs[i]);
  //     }

  //     var options = {
  //       'images': thumbs,
  //       'numFrames': this.gifSetting ? this.gifSetting.gifInterval : 1,
  //       'frameDuration': this.gifSetting ? this.gifSetting.frameDuration : 5,
  //       'gifWidth': this.gifSetting ? this.gifSetting.gifWidth : 200,
  //       'gifHeight': this.gifSetting ? (this.gifSetting.aspectRatio ? (this.gifSetting.aspectRatio * this.gifSetting.gifWidth) : this.gifSetting.gifHeight ) : 200
  //     };
  //     this.showLoading(this.translateService.instant('LOADER.makingGIF')).then(() => {
  //       gifshot.createGIF(options, (result) => {
  //         if (result && !result.error && result.image) {
  //           console.log(JSON.stringify(result.image))
  //           this.userPhotoServiceAkita(result.image);
  //           this.hideLoading().then(async () => {
  //             let currentTime = +new Date();
  //             const fileName = 'image_' + currentTime.toString();
  //             const format = 'image/gif';
  //             try {
  //               resolve(await this.onImageChanged({base64Image: result.image, fileName: fileName, format: format}));
  //             } catch (e) {
  //               console.error(e);
  //               // TODO @Ansik React on errors
  //             } finally {
  //               this.lockService.addResume();
  //             }
  //           });
  //         }
  //         else {
  //           // alert(result.error + ": " + result.errorMsg);
  //           reject();
  //         }
  //       });
  //     });
  //   })
  // }

  // getGifByVideo(videoSrcUri: string) {
  //   var options = {
  //     'video': this.getAsWebViewCorrectUrlMobile(videoSrcUri),
  //     'numFrames': this.gifSetting ? this.gifSetting.numFrames : 5,
  //     'frameDuration': this.gifSetting ? this.gifSetting.frameDuration : 5,
  //     'interval': this.gifSetting ? this.gifSetting.interval : 1,
  //     'gifWidth': this.gifSetting ? this.gifSetting.gifWidth : 200,
  //     'gifHeight': this.gifSetting ? (this.gifSetting.aspectRatio ? (this.gifSetting.aspectRatio * this.gifSetting.gifWidth) : this.gifSetting.gifHeight ) : 200
  //   }

  //   this.showLoading('Making gif...').then(() => {

  //     try {
  //       gifshot.createGIF(options, (result) => {
  //         if (result && !result.error && result.image) {
  //           this.hideLoading().then(async () => {
  //             let currentTime = +new Date();
  //             const fileName = 'image_' + currentTime.toString();
  //             const format = 'image/gif';
  //             try {
  //               await this.onImageChanged({base64Image: result.image, fileName: fileName, format: format, reloadApp: true });
  //             } catch (e) {
  //               console.error(e);
  //               // TODO @Ansik React on errors
  //             } finally {
  //               // this.lockService.addResume();
  //             }
  //           });
  //         }
  //         else {
  //           // alert(result.error + ": " + result.errorMsg);
  //         }
  //       });
  //     }
  //     catch(e) {
  //       // alert(e)
  //     }

  //   });
  // }

  getAsWebViewCorrectUrlMobile(fileUri: string) {
    if (this.isiOS()) {
      return window.Ionic.WebView.convertFileSrc(fileUri);
    }
    else {
      // correct file paths on Android due to https://github.com/ionic-team/cordova-plugin-ionic-webview/issues/598
      var filePathCorrected = window.Ionic.WebView.convertFileSrc(fileUri);
      filePathCorrected = filePathCorrected.replace('file:/', "https://localhost/_app_file_/");
      return filePathCorrected;
    }
  }

  // compressVideo(inputFileUri: string): Promise<string> {
  //   return new Promise(resolve => {
  //     this.showLoading('Starting compression job').then(() => {
  //       setTimeout(() => {
  //         var settings = { name: 'Best', width: 1920, bitrate: 3000000, fps: 30 };
  //         var guid = this.generateUUID();
  //         inputFileUri = this.getFileUriWithFilePrefix(inputFileUri);

  //         VideoEditor.transcodeVideo(
  //           (successFileUrl) => {
  //             this.hideLoading().then(() => {
  //               resolve(successFileUrl);
  //             });
  //           },
  //           (err) => {
  //             // alert('err: ' + JSON.stringify(err));
  //             resolve(null);
  //           },
  //           <VideoEditorTranscodeProperties>{
  //             fileUri: inputFileUri,
  //             outputFileName: 'helix-' + guid,
  //             outputFileType: VideoEditorOptions.OutputFileType.MPEG4,
  //             optimizeForNetworkUse: VideoEditorOptions.OptimizeForNetworkUse.YES, // ios only
  //             saveToLibrary: true,
  //             deleteInputFile: false, // android only - defaults to false
  //             width: settings.width,
  //             videoBitrate: settings.bitrate,
  //             fps: settings.fps,
  //             maintainAspectRatio: true, // ios only - defaults to true
  //             progress: (progress) => {
  //               this._Zone.run(() => {
  //                 // Workaround that Android emits the progress from 0 to 1, but iOS is 0-100.
  //                 let percentComplete = (this.isAndroid()) ? parseInt((progress * 100).toString()) : parseInt(progress);
  //                 this.setLoadingContent('Progress: ' + percentComplete + '%');
  //               });
  //             }
  //           }
  //         );
  //       }, 150);
  //     });
  //   });
  // }

  isAndroid() {
    return this._platform.is('android');
  }

  isiOS() {
    return this._platform.is('ios');
  }

  generateUUID() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
      d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  getFileUriWithFilePrefix(fileUri: string) {
    var prefix = 'file:///';
    var filePathCorrected = prefix + fileUri;
    filePathCorrected = filePathCorrected.replace('file:///file:///', prefix);
    filePathCorrected = filePathCorrected.replace('file:///file:/', prefix);
    filePathCorrected = filePathCorrected.replace('file:////', prefix);
    return filePathCorrected;
  }

  setLoadingContent(content: string) {
    this._LoadingController.getTop().then(loading => {
      if (loading) {
        loading.message = content;
      }
    });
  }

  async showLoading(msg: string) {
    const loading = await this._LoadingController.create({
      message: msg,
      mode: 'ios'
    });
    await loading.present();
  }

  hideLoading() {
    return new Promise<void>(resolve => {
      this._LoadingController.getTop().then(topLoadingComponent => {
        if (!topLoadingComponent) {
          resolve();
        }
        else {
          this._LoadingController.dismiss().then(() => {
            resolve();
          })
        }
      });
    });
  }


  async choosePicture(): Promise<string> {
    var options: ImageOptions = {
      quality: this.isiOS() ? 25 : 100,
      source: CameraSource.Photos,
      resultType: CameraResultType.DataUrl,
      // mburger: not used on capacitor plugin
      // encodingType: this.camera.EncodingType.JPEG,
      // allowEditing: true,
      // mburger: not used on capacitor plugin
      // mediaType: this.camera.MediaType.PICTURE,
      width: 500,
      height: 500
    };
    // TODO mburger: why just for iOS?
    if(this.isiOS()){
      options.allowEditing = true;
    }
    if(this.isAndroid()) {
      options.correctOrientation = true;
    }
    this.lockService.removeResume();
    return new Promise((resolve, reject) => {
      UtilityService.setTimeout(500).subscribe(_ => {
        try {
          Camera.checkPermissions().then(permissions => {
            console.log("Camera permissions", permissions);
            console.log("Camera permissions, photos", permissions.photos);
            if(permissions.photos == 'denied' || permissions.photos == 'limited') {
              console.log("Camera.requestPermissions init");
              // var status = await Camera.requestPermissions({ permissions: ['photos'] });
              this.showSettingsPrompt();
            } else {
              Camera.getPhoto(options).then(async (imageData: Photo) => {
                const base64Image = imageData.dataUrl;
                let currentTime = +new Date();
                const fileName = 'image_' + currentTime.toString();
                // const fileName = 'image_' + UtilityService.randomStringGenerator(6);
                const format = imageData.format;
                try {
                  resolve(await this.onImageChanged({base64Image, fileName, format}));
                } catch (e) {
                  console.error(e);
                  // TODO @Ansik React on errors
                  reject()
                } finally {
                  this.lockService.addResume();
                }
              }).catch(e => {
                this.lockService.addResume();
              });
            }
          })
          // Camera.getPhoto(options).then(async (imageData: Photo) => {
          //   const base64Image = imageData.dataUrl;
          //   let currentTime = +new Date();
          //   const fileName = 'image_' + currentTime.toString();
          //   // const fileName = 'image_' + UtilityService.randomStringGenerator(6);
          //   const format = imageData.format;
          //   try {
          //     resolve(await this.onImageChanged({base64Image, fileName, format}));
          //   } catch (e) {
          //     console.error(e);
          //     // TODO @Ansik React on errors
          //     reject()
          //   } finally {
          //     this.lockService.addResume();
          //   }
          // }, _ => {
          //   this.lockService.addResume();
          // });
        } catch (e) {
          this.lockService.addResume();
        }
      });
    })
  }

  /** Method to delete the user picture */
  public async removePicture(): Promise<string> {
    return await this.onImageChanged({base64Image: null, fileName: 'Default', format: null})
  }

  public onImageChanged(imageSelectionPayload: ImageSelectionPayload): Promise<string> {
    return new Promise((resolve, reject) => {
      console.log("imageSelectionPayload", imageSelectionPayload);
      if (imageSelectionPayload.fileName !== 'Default') {
        if (!this._NetworkService.checkConnection()) {
            this._NetworkService.addOfflineFooter('ion-footer');
            // resolve(null);
            return;
        } else {
            this._NetworkService.removeOfflineFooter('ion-footer');
        }
        console.log("this.apiProviderService.updatePicture");
        this.apiProviderService.updatePicture(
            this.user,
            imageSelectionPayload.base64Image,
            imageSelectionPayload.fileName,
            imageSelectionPayload.format).then(imageUrl => {
            console.log("imageUrl", imageUrl);
            // this.userPhotoServiceAkita.updatePhoto(imageUrl);
            // console.log('image stored in secure-storage');

            // dirty hack for android after video gif maker runs.
            if (imageSelectionPayload.reloadApp) {
                window.location.reload();
            }
            resolve(imageUrl);
        });
      } else {
          this.removePictureLocal();
          // reject();
          resolve(null);
      }
    })
}

/** Method to delete the user picture */
private removePictureLocal(): void {
    this.apiProviderService.deleteImages(this.appStateService.basicAuthToken, this.user.userid)
        .then(async _ => {
            const toast = await this._ToastController.create({
                message: this.translateService.instant('PERSONALDETAILS.deleteImageSuccess'),
                duration: 2000,
                position: 'top',
            });
            await toast.present();
            // this.apiProviderService.globalProfilePicture = null;
            this.userPhotoServiceAkita.clear();
        }, async _ => {
            const toast = await this._ToastController.create({
                message: this.translateService.instant('PERSONALDETAILS.deleteImageError'),
                duration: 2000,
                position: 'top',
            });
            await toast.present();
        });
  }

  async showSettingsPrompt() {
    await this.alertService.alertCreate(
      this.translateService.instant('CAMERASETTINGS.header'),
      null,
      this.translateService.instant('CAMERASETTINGS.message-1') + " " + this.translateService.instant('CAMERASETTINGS.message-2'),
      [{
        text: this.translateService.instant('GENERAL.ok'),
        role: 'Cancel',
        handler: () => {
          this._OpenNativeSettings.open("settings");
        }
      }]
    );
  }
}
