import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss'],
})
export class ErrorPageComponent implements OnInit {
  public error: any;

  constructor(private modalController: ModalController) { }

  /** @inheritDoc */
  public ngOnInit(): void {}

  /** If the erorr is an http error */
  public get isHttpError(): boolean {
    return this.error instanceof HttpErrorResponse;
  }

  /** Callback for dismissing the modal */
  public async onDismiss(): Promise<void> {
    await this.modalController.dismiss();
  }

}
