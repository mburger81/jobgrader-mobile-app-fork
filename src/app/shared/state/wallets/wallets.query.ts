import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';


// custom imports
import { WalletsStore, WalletsState } from './wallets.store';


@Injectable({ providedIn: 'root' })
export class WalletsQuery extends QueryEntity<WalletsState> {

  constructor(protected store: WalletsStore) {
    super(store);
  }

}
