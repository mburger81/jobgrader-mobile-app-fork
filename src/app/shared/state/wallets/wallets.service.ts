// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
// import { arrayAdd, arrayRemove, arrayUpdate, guid, ID } from '@datorama/akita';
// import structuredClone from '@ungap/structured-clone';


// custom imports
// import { Trx } from './trx.model';
import { Wallet } from './wallet.model';
// import { WalletsQuery } from './wallets.query';
import { WalletsStore } from './wallets.store';
// import { tokens } from '../../services/web3/contracts/tokens';
// import { Web3Service } from '../../services/web3/web3.service';
// import { PortfolioService } from '../portfolio/portfolio.service';


@Injectable({ providedIn: 'root' })
export class WalletsService {

  // private t = tokens;

  private _value: number = 0;


  constructor(
    // custom
    // private portfolioService: PortfolioService,
    // private web3Service: Web3Service,
    // private walletsQuery: WalletsQuery,
    private store: WalletsStore
  ) {

    // this.init();

  }


  // get() {
  //   return this.http.get<Wallet[]>('https://api.com').pipe(tap(entities => {
  //     this.store.set(entities);
  //   }));
  // }

  async add(wallet: any) {
    this.store.add(wallet);
  }

  update(id, wallet: Partial<Wallet>) {
    let x = this.store.update(id, wallet);
  }

  public setWallets(wallet: Wallet[]) {
    this.store.set(wallet);
  }

  public clear() {
    this.setWallets([]);
  }

  // remove(id: ID) {
  //   const value = this.walletsQuery.getEntity(id).value;
  //   this.store.remove(id);

  //   this.portfolioService.addValue(value * -1);
  // }

  // move(fromIndex: number, toIndex: number) {
  //   this.store.move(fromIndex, toIndex);
  // }

  // async updateName(address: string, name: string) {
  //   this.store.update(address, { name: name });
  // }


  // public duplicateWalletValidator(): ValidatorFn {

  //   return (control: AbstractControl): ValidationErrors | null => {

  //     const duplicateWallet = this.walletsQuery.hasEntity(( { address }) => address?.toLowerCase() === control.value?.toLowerCase());

  //     return duplicateWallet ? { duplicateWallet: { value: control.value } } : null;
  //   };
  // }

  // public duplicateWalletNameValidator(): ValidatorFn {

  //   return (control: AbstractControl): ValidationErrors | null => {

  //     const duplicateWalletName = this.walletsQuery.hasEntity(( { name }) => name?.toLowerCase() === control.value?.toLowerCase());

  //     return duplicateWalletName ? { duplicateWalletName: { value: control.value } } : null;
  //   };
  // }

  // public async addTransaction(address: string, newTrx: Partial<Trx>) {

  //   this.store.update(address, ({ trx }) => ({
  //     trx: arrayAdd(trx, {id: guid(), ...newTrx })
  //   }));

  // }

  // public async updateTransaction(address: string, id, newTrx: Partial<Trx>) {

  //   this.store.update(address, ({ trx }) => ({
  //     trx: arrayUpdate(trx, id, newTrx )
  //   }));

  // }

  // public async deleteTransaction(address: string, id: ID) {

  //   this.store.update(address, ({ trx }) => ({
  //     trx: arrayRemove(trx, id)
  //   }));

  // }


  // public async refresh() {
  //   return this.init(true);
  // }

  // private async init(refresh: boolean = false) {

  //   if (!refresh) {

  //     await this.web3Service.start();

  //   } else {

  //     await this.web3Service.refresh();

  //   }

  //   this._value = 0;


  //   const promises = [];

  //   this.walletsQuery.getAll().forEach(
  //     (wallet: Wallet) => {

  //       const _wallet = structuredClone(wallet);

  //       promises.push(this.initWallet(_wallet));

  //     }
  //   );


  //   await Promise.all(promises);

  // }

  // private async initWallet(wallet: Wallet) {
  //   // console.log('WalletsSevice#initWallet; wallet:', wallet);

  //   wallet.value = 0;

  //   for (const [key, token] of Object.entries(this.t)) {
  //     // console.log('WalletsSevice#initWallet;', key, token);
  //     // console.log('WalletsSevice#initWallet;', key, token);

  //     if(token.chains) {
  //       for (const [keyC, chain] of Object.entries(token.chains)) {
  //         // console.log('WalletsSevice#initWallet;', keyC, chain);

  //         const [ balance, baseDividends ] =
  //           await Promise.all(
  //             [
  //               (chain.isNative === true)
  //                 ? this.web3Service.getNativeTokenBalance(wallet.address, token.decimals)
  //                 : this.web3Service.getBalance(wallet.address, chain.address, token.decimals, token.abi)
  //               ,
  //               (chain.dividends?.base)
  //                 ? this.web3Service.getDividendsInfo(wallet.address, chain.address, token.abi, tokens[chain.dividends.base.token.toLowerCase()].decimals)
  //                 : null
  //             ]
  //           );


  //         if (!wallet.tokens[key]) {
  //           wallet.tokens[key] = { };
  //         }

  //         if (!wallet.tokens[key].chains) {
  //           wallet.tokens[key].chains = { };
  //         }

  //         if (!wallet.tokens[key].chains || !wallet.tokens[key].chains[keyC]) {
  //           wallet.tokens[key].chains[keyC] = { balance: 0, value: 0 };
  //         }


  //         wallet.tokens[key].chains[keyC].balance = balance;
  //         wallet.tokens[key].chains[keyC].value = balance * token.price?.inUSD;
  //         if (baseDividends) {
  //           wallet.tokens[key].chains[keyC].dividends = {};
  //           wallet.tokens[key].chains[keyC].dividends.base = baseDividends;
  //         }


  //         wallet.value = wallet.value + wallet.tokens[key].chains[keyC].value;

  //       }

  //       this._value = this._value + wallet.value;
  //     }
  //   }

  //   this.store.update(wallet.address, wallet);

  //   // console.log('WalletsSevice#initWallet; wallet', wallet);
  // }

}
