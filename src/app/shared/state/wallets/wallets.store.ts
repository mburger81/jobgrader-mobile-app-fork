import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';


// custom imports
import { Wallet } from './wallet.model';
import { produce } from 'immer';


export interface WalletsState extends EntityState<Wallet> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'wallets', idKey: 'address', producerFn: produce })
export class WalletsStore extends EntityStore<WalletsState> {

  constructor() {
    super();
  }

}
