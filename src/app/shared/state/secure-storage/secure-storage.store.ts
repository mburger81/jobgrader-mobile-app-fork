import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';


// custom imports
import { createSecureStorage, SecureStorageState,  } from './secure-storage.model';


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'secure-storage' })
export class SecureStorageStore extends Store<SecureStorageState> {

  constructor() {
    super(createSecureStorage());
  }

}
