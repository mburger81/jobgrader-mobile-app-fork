let _getSerializerPromise;

export function getSerializerPromise(localForageInstance) {

    if (!_getSerializerPromise) {
        _getSerializerPromise = {};
    }

    if (_getSerializerPromise.result) {
        return _getSerializerPromise.result;
    }
    if (!localForageInstance || typeof localForageInstance.getSerializer !== 'function') {
        return Promise.reject(new Error(
            'localforage.getSerializer() was not available! ' +
            'localforage v1.4+ is required!'));
    }
    _getSerializerPromise.result = localForageInstance.getSerializer();
    return _getSerializerPromise.result;
}

export function executeCallback(promise, callback) {
    if (callback) {
        promise.then(function(result) {
            callback(null, result);
        }, function(error) {
            callback(error);
        });
    }
}