import { executeCallback, getSerializerPromise } from './utils';
import { getOpenDatabasePromise } from './cap-data-storage-sqlite';
import { CapacitorDataStorageSqlite } from 'capacitor-data-storage-sqlite';


function _initStorage(options) {
    // console.log('CapacitorDataStorageSQLiteDriver#_initStorage');

    var self = this;
    var dbInfo = {
        db: null,
        location: null,
        name: null,
        version: null,
        size: null,
        description: null,
        dbKey: null,
        storeName: null,
        serializer: null
    };


    if (options) {
        for (var i in options) {
            dbInfo[i] = typeof(options[i]) !== 'string' ?
                        options[i].toString() : options[i];
        }
    }

    var dbInfoPromise =
        new Promise<void>(
            (resolve, reject) => {
                try {

                    dbInfo.location = dbInfo.location || 'default';

                    const store = CapacitorDataStorageSqlite;

                    store.openStore(
                        {
                            /**
                             * The storage database name
                             */
                            database: dbInfo.name,
                            /**
                             * The storage table name
                             */
                            // table?: string,
                            /**
                             * Set to true for database encryption
                             */
                            encrypted: false,
                            /***
                             * Set the mode for database encryption
                             * ["encryption", "secret","newsecret"]
                             */
                            mode: 'no-encryption'
                        }
                    )
                    .then(() => {

                        dbInfo.db = store;
                        self._dbInfo = dbInfo;

                        resolve();

                    })
                    .catch((error) => {
                        reject(error);
                    });

                    // sqlite.initWebStore()
                    //     .then(
                    //         () => {
                    //             sqlite.createConnection(
                    //                 dbInfo.name,
                    //                 false,
                    //                 'no-encryption',
                    //                 dbInfo.version,
                    //             ).then((
                    //                 (db: SQLiteDBConnection) => {
                    //                     console.log('3 dbInfoPromise');

                    //                     db.open()
                    //                         .then(
                    //                             () => {


                    //                                 const executeSql = (sqlStatement, args, success, error) => {
                    //                                     console.log('executeSQL');

                    //                                     dbInfo.db.run(sqlStatement, args, false)
                    //                                         .then((s) => success({ executeSql }, { rows:[], ...s }))
                    //                                         .catch((e) => error({ executeSql }, e));
                    //                                 }

                    //                                 dbInfo.db = db;
                    //                                 dbInfo.db.transaction = (callback, _error, _success) => {

                    //                                     callback({
                    //                                         executeSql
                    //                                     });


                    //                                     // try {

                    //                                     // //   statments.forEach((s) => { console.log(s)});
                    //                                     //   success();

                    //                                     // } catch(e) {
                    //                                     //   error(e);
                    //                                     // }


                    //                                 };


                    //                                 dbInfo.db.transaction((t) => {
                    //                                     t.executeSql(
                    //                                         'CREATE TABLE IF NOT EXISTS ' + dbInfo.storeName + ' (id INTEGER PRIMARY KEY, key unique, value)',
                    //                                         [],
                    //                                         () => {
                    //                                             console.log('YES XXXXXXX');
                    //                                             self._dbInfo = dbInfo;
                    //                                             resolve(null);
                    //                                         },
                    //                                         (t, error) => {
                    //                                             console.log('NOOOOOOOOOOO', t, error);
                    //                                             reject(error);
                    //                                         }
                    //                                     );
                    //                                 });

                    //                             }
                    //                         );

                    //                 }
                    //             ));
                    //         }
                    //     );


                } catch (e) {
                    reject(e);
                }

            }
        );

    var serializerPromise = getSerializerPromise(self);

    return Promise.all([
        serializerPromise,
        dbInfoPromise
    ]).then(function(results) {
        dbInfo.serializer = results[0];
        return dbInfoPromise;
    });
}

var cordovaSQLiteDriver: LocalForageDriver = {
    _driver: 'capacitorDataStorageSQLiteDriver',
    _initStorage: _initStorage,
    _support: function () {
        return getOpenDatabasePromise().then(function (openDatabase) {
            return !!openDatabase;
        }).catch(function () {
            return false;
        });
    },
    getItem: function <T>(key: string, callback?: (err: any, value: T) => void): Promise<T> {
        key = normalizeKey(key);

        // console.log('CapacitorDataStorageSQLiteDriver#getItem; key:', key);

        const promise = new Promise<any>(
            (resolve, reject) => {
                if (key.length > 0) {
                    try {
                        const dbInfo = this._dbInfo;

                        dbInfo.db.get({ key }).then(
                            (value) => {

                                if (value.value) {
                                    value = dbInfo.serializer.deserialize(value.value);
                                }

                                console.log('CapacitorDataStorageSQLiteDriver#getItem; value:', value);
                                resolve(value);
                            }
                        );
                    } catch (err) {
                        // console.log(`CapacitorDataStorageSQLiteDriver#getItem; error key: ${key} err: ${JSON.stringify(err)}`);
                        return reject(err);
                    }
                } else {
                    return reject(new Error("CapacitorDataStorageSQLiteDriver#getItem; Must give a key"));
                }
            }
        );
        executeCallback(promise, callback);
        return promise;
    },
    setItem: function <T>(key: string, value: T, callback?: (err: any, value: T) => void): Promise<T> {
        key = normalizeKey(key);

        const promise = new Promise<T>(
            (resolve, reject) => {
                if (key.length > 0) {
                    try {

                        const originalValue = value;

                        const dbInfo = this._dbInfo;

                        dbInfo.serializer.serialize(
                            value,
                            (value, error) => {
                                if (error) {
                                    reject(error);
                                } else {
                                    dbInfo.db.set({ key, value }).then(
                                        () => {
                                            resolve(originalValue);
                                        }
                                    );
                                }
                            }
                        );
                    } catch (err) {
                        reject(err);
                    }
                } else {
                    return reject(new Error("CapacitorDataStorageSQLiteDriver#setItem; Must give a key"));
                }
            }
        );
        executeCallback(promise, callback);
        return promise;
    },
    removeItem: function (key: string, callback?: (err: any) => void): Promise<void> {
        key = normalizeKey(key);

        const promise = new Promise<void>(
            (resolve, reject) => {
                if (key.length > 0) {
                    try {
                        const dbInfo = this._dbInfo;

                        dbInfo.db.remove({ key }).then(
                            () => {
                                resolve();
                            }
                        );
                    } catch (err) {
                        reject(err);
                    }
                } else {
                    return reject(new Error("CapacitorDataStorageSQLiteDriver#removeItem; Must give a key"));
                }
            }
        );
        executeCallback(promise, callback);
        return promise;
    },
    clear: function (callback?: (err: any) => void): Promise<void> {

        const promise = new Promise<void>(
            (resolve, reject) => {
                try {
                    const dbInfo = this._dbInfo;

                    dbInfo.db.clear().then(
                        () => {
                            resolve();
                        }
                    );
                } catch (err) {
                    reject(err.message);
                }
            }
        );
        executeCallback(promise, callback);
        return promise;
    },
    length: function (callback?: (err: any, numberOfKeys: number) => void): Promise<number> {
        throw new Error('Function not implemented: length');
    },
    key: function (keyIndex: number, callback?: (err: any, key: string) => void): Promise<string> {
        throw new Error('Function not implemented: key');
    },
    keys: function (callback?: (err: any, keys: string[]) => void): Promise<string[]> {
        throw new Error('Function not implemented: keys');

        // var promise = new Promise<[]>(function (resolve, reject) { resolve([]); });

        // executeCallback(promise, callback);
        // return promise;
    },
    iterate: function <T, U>(iteratee: (value: T, key: string, iterationNumber: number) => U, callback?: (err: any, result: U) => void): Promise<U> {
        throw new Error('Function not implemented: iterate');
    }
};

function normalizeKey(key) {
    // Cast the key to a string, as that's all we can set as a key.
    if (typeof key !== 'string') {
        console.warn(`CapacitorDataStorageSQLiteDriver#normalizeKey; ${key} used as a key, but it is not a string.`);
        key = String(key);
    }

    return key;
}

export default cordovaSQLiteDriver;