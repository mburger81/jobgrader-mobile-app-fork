import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NotificationsPage } from './notifications.page';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
    {
        path: '',
        component: NotificationsPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        JobHeaderModule,
        RouterModule.forChild(routes),
        TranslateModule,
        SharedModule
    ],
    declarations: [NotificationsPage]
})
export class NotificationsPageModule {
}
