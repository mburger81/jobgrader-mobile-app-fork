import { Component, OnDestroy } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AlertsProviderService } from '../core/providers/alerts/alerts-provider.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
// import { PushNotifications } from '@capacitor/push-notifications';
import { FirebaseMessaging } from "@capacitor-firebase/messaging";
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';
import { Subject, throwIfEmpty } from 'rxjs';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { PushService } from '../core/providers/push/push.service';
import { SubscriptionTags } from '../core/providers/broadcast/broadcast.service';

declare var window: any;

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnDestroy {

  private destroy$ = new Subject();

  notifications;

  userImage;
  public toggleValue = false;
  public cordovaUnavailable = false;
  public topicSubscription;

  constructor(
      public platform: Platform,
      private navCtrl: NavController,
      private translateService: TranslateService,
      private secureStorage: SecureStorageService,
      private alertService: AlertsProviderService,
      private _OpenNativeSettings: OpenNativeSettings,
      private userPhotoServiceAkita: UserPhotoServiceAkita,
      private _Toast: ToastController,
      private _Push: PushService
    ) {
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    this.destroy$.next(0);
    this.destroy$.complete();
  }

  async ionViewWillEnter() {

    var userImage = this.userPhotoServiceAkita.getPhoto();
    this.userImage = userImage;

    var notifications = await this.secureStorage.getValue(SecureStorageKey.notifications, false);
    this.notifications = !!notifications ? JSON.parse(notifications).reverse() : [
// {
//   title: 'Test Title',
//   message: 'Test Message',
//   timestamp: +new Date()
// }
    ];

    var topicSubscription = await this.secureStorage.getValue(SecureStorageKey.subscribeToNewsUpdates, false);
    this.topicSubscription = !!topicSubscription;

    this.cordovaUnavailable = ((window as any).cordova) ? true : false;
    if (!this.cordovaUnavailable) {
      this.toggleValue = false;
      return;
    }

    let permStatus = await FirebaseMessaging.checkPermissions();

    if (permStatus.receive !== 'granted') {
      this.toggleValue = false;

      this.showSettingsPrompt();

    } else {
      this.toggleValue = true;
    }

  }

  public async save(value: boolean): Promise<void> {
    if (value) {
      FirebaseMessaging.requestPermissions().catch(e => {
        this.showError();
      });
    }
    else {
      FirebaseMessaging.removeAllListeners().catch(e => {
        this.showError();
      });
    }
  }

  subscribe(value: boolean) {
    if(value) {
      this._Push.subscribeToTopic(SubscriptionTags.AppUpdateENEU);
      this.presentToast(this.translateService.instant('BROADCAST.yes'));
      this.secureStorage.setValue(SecureStorageKey.subscribeToNewsUpdates, "true");
    } else {
      this._Push.unsubscribeFromTopic(SubscriptionTags.AppUpdateENEU);
      this.presentToast(this.translateService.instant('BROADCAST.no'));
      this.secureStorage.removeValue(SecureStorageKey.subscribeToNewsUpdates, false);
    }
  }

  async showError() {
    await this.alertService.alertCreate(
      this.translateService.instant('GENERAL.error'),
      null,
      this.translateService.instant('NOTIFICATIONS.error-storing-message'),
      [{
        text: this.translateService.instant('GENERAL.ok'),
        role: 'Cancel'
      }]
    );
  }

  async showSettingsPrompt() {
    await this.alertService.alertCreate(
      this.translateService.instant('SETTINGS.notifications'),
      null,
      this.translateService.instant('NOTIFICATIONS.permissionsNeeded'),
      [{
        text: this.translateService.instant('GENERAL.ok'),
        role: 'Cancel',
        handler: () => {
          this._OpenNativeSettings.open("notification_id");
        }
      }]
    );
    this.goBack();
  }

  goBack() {
    return this.navCtrl.navigateBack('/dashboard/tab-settings');
  }

  isAndroid() {
    return ;
  }

  presentToast(message: string) {
    this._Toast.create({
      message,
      position: 'top',
      duration: 2000
    }).then(toast => { toast.present() })
  }

}
