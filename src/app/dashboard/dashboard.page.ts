import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
// import { ChatService } from '../core/providers/chat/chat.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
// import { NetworkService } from '../core/providers/network/network-service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  private ngOnInitExecuted: any;

  constructor(
    private cdr: ChangeDetectorRef,
    public _SecureStorageService: SecureStorageService,
    // public _ChatService: ChatService,
    // private _NetworkService: NetworkService
  ) {

  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }


  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }

    // Avoid the page transition from chat to dashboard re-invoking chat connect if we're pausing the app.
    if (window['skipAutoReconnect'] != undefined && window['skipAutoReconnect'] == true) {
      return window['skipAutoReconnect'] = false;
    }

    // if (!this._ChatService.isConnected()) {
    //   if(this._NetworkService.checkConnection()) {
    //     this._ChatService.connect(true);
    //   }
    // }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  private async componentInit() {
    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
