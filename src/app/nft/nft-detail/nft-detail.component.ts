import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';

@Component({
  selector: 'app-nft-detail',
  templateUrl: './nft-detail.component.html',
  styleUrls: ['./nft-detail.component.scss'],
})
export class NftDetailComponent implements OnInit {
  @Input() data: any;
  @Input() source: string;

  public keyValue = [];
  public displayImage;
  public bannerImage;
  public profileImage;
  public buttonText;
  private datePipe = new DatePipe('en-US');

  constructor(
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService
  ) { }

  ngOnInit() {
    console.log(this.data);
    console.log(this.source);
    var keys = Object.keys(this.data);
    keys = keys.filter(key => !!this.data[key]);    
    this.displayImage = (this.source == 'openSea') ? this.data['image_url'] : (this.source == 'decentralandCollectible' ? this.data['image'] : null) ;
    // this.bannerImage = this.data['banner_image_url'];
    this.buttonText = (this.source == 'openSea') ? 'View in OpenSea' : (this.source == 'decentralandCollectible' ? 'View in Decentraland' : 'Visit Event') ;
    this.profileImage = (this.source == 'openSea') ? this.data['profile_img_url'] : (this.source == 'poap' ? this.data['event_image_url'] : null) ;
    if(!!this.data.attributes) {
      this.data.attributes = this.data.attributes.filter(k => !!k.value);
      for(let j=0; j<this.data.attributes.length; j++) {
        var attribute = this.data.attributes[j];
        if(!!attribute.display_type) {
          if(attribute.display_type == 'date') {
            this.data.attributes[j].value = this.datePipe.transform(this.data.attributes[j].value, 'dd.MM.yyyy HH:mm:ss');
          }
        }
      }
      
    }
    keys.forEach(key => {
      if(!['attributes', 'metadata_url', 'image','image_url', 'banner_image_url', 'profile_img_url', 'permalink', 'event_event_url', 'event_image_url', 'chain', 'tokenId', 'event_supply', 'event_year', 'event_id', 'event_fancy_id', 'event_name', 'event_description', 'event_city', 'event_country', 'owner', 'name', 'description', 'url'
    ].includes(key)) {
      if(['created_date', 'created', 'event_start_date', 'event_expiry_date', 'event_end_date', 'createdAt', 'created_at', 'updated_at'].includes(key)) {
          this.keyValue.push({
            key: this._Translate.instant('NFT.keys.'+key),
            value: this.datePipe.transform(this.data[key], 'dd.MM.yyyy HH:mm')
          })
      } else {
          this.keyValue.push({
            key: this._Translate.instant('NFT.keys.'+key),
            value: this.data[key]
          })
        }
      }
    })
  }

  viewInOpenSea() {
    var a = document.createElement("a");
    a.style.display = "none";
    a.href = (this.source == 'openSea') ? this.data.permalink : (this.source == 'poap' ? this.data.event_event_url : `https://market.decentraland.org${this.data.url}`) ;
    a.target = "_blank";
    a.click();
    a.remove();
  }

  close() {
    this._ModalController.dismiss();
  }

}
