import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { WalletConnectService } from 'src/app/core/providers/wallet-connect/wallet-connect.service';

@Component({
  selector: 'app-wc2-session',
  templateUrl: './wc2-session.component.html',
  styleUrls: ['./wc2-session.component.scss'],
})
export class Wc2SessionComponent implements OnInit {
  @Input() data: any;
  public profileImage;
  public keyValue = [];
  public peerName: string = '';
  public peerAddress: string = '';
  public peerDescription: string = '';

  constructor(
    private _ModalController: ModalController,
    private _AlertController: AlertController,
    private _Translate: TranslateProviderService,
    private _WalletConnect: WalletConnectService
  ) { }

  ngOnInit() {
    console.log(this.data);

    this.profileImage = !!this.data.peer ? this.data.peer.metadata.icons[0] : this.data.peer.metadata.icons[0];
    this.peerName = !!this.data._peer ? this.data._peer.metadata.name : this.data.peer.metadata.name;
    this.peerAddress =  !!this.data._peer ? this.data._peer.publicKey : this.data.peer.publicKey;
    this.peerDescription = !!this.data._peer ? this.data._peer.metadata.description : this.data.peer.metadata.description;
    
    this.keyValue.push({
      key: 'WalletConnect version',
      value: '2'
    },{
      key: 'Chains',
      value: JSON.stringify(this.data.namespaces.eip155.chains)
    },{
      key: 'Methods',
      value: JSON.stringify(this.data.namespaces.eip155.methods)
    },{
      key: this._Translate.instant('WALLETCONNECT.connected-question'),
      value: Object.keys(this.data).includes('_acknowledged') ? ((this.data._acknowledged.toString() == 'true') ? this._Translate.instant('WALLETCONNECT.yes') : this._Translate.instant('WALLETCONNECT.no')) : ((this.data.acknowledged.toString() == 'true') ? this._Translate.instant('WALLETCONNECT.yes') : this._Translate.instant('WALLETCONNECT.no'))
    })

  }

  endSession() {
    this.dismiss(false);
  }

  close() {
    this.dismiss(true);
  }

  dismiss(value: boolean) {
    this._ModalController.dismiss({ value });
  }

}
