import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CryptoActivitiesPage } from './crypto-activities.page';
import { PaymentModalModule } from '../payments/payment-modal/payment-modal.module';
import { TransactionDetailModule } from '../payments/transaction-detail/transaction-detail.module';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    component: CryptoActivitiesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule,
    PaymentModalModule,
    TransactionDetailModule,
    JobHeaderModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
  ],
  declarations: [CryptoActivitiesPage]
})
export class CryptoActivitiesPageModule {}
