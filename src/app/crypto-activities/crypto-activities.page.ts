import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { PaymentRequests } from '../core/providers/data-sharing/data-sharing.service';
import { PaymentModalComponent } from '../payments/payment-modal/payment-modal.component';
import { TransactionDetailComponent } from '../payments/transaction-detail/transaction-detail.component';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { NetworkService } from '../core/providers/network/network-service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
@Component({
  selector: 'app-crypto-activities',
  templateUrl: './crypto-activities.page.html',
  styleUrls: ['./crypto-activities.page.scss'],
})
export class CryptoActivitiesPage {

  public profilePictureSrc: string;
  public address: string = null;

  public requestsReceived: Array<PaymentRequests> = [
    // {
    //   userPublicKey: "0x7c9869d04F15300deC55CB0E3308d0898D51a59D",
    //   amount: "8.11",
    //   currency: "MATIC",
    //   timestamp: 1663856923382,
    //   status: 0,
    //   pendingProcesses: [],
    //   privateKey: null,
    //   paymentRequestId: '73207b58-647e-41e6-9e2c-1e60a5ea4048'
    // },
    // {
    //   userPublicKey: "0x7c9869d04F15300deC55CB0E3308d0898D51a59D",
    //   amount: "9.88",
    //   currency: "EVE",
    //   timestamp: 1663856925314,
    //   status: 1,
    //   pendingProcesses: [],
    //   privateKey: null,
    //   paymentRequestId: '73207b58-647e-41e6-9e2c-1e60a5ea4048'
    // },
    // {
    //   userPublicKey: "0x5a1B53309a7Cd58814a3732C7a58Dd48ea386B0e",
    //   amount: "5.18",
    //   currency: "ETH",
    //   timestamp: 1663856926216,
    //   status: -1,
    //   pendingProcesses: [],
    //   privateKey: null,
    //   paymentRequestId: '73207b58-647e-41e6-9e2c-1e60a5ea4048'
    // },
  ];

  public requestsSent = [
    // {
    //   userPublicKey: "0xd78D4Bdf7AB5231676c698C8fE538cafA2912777",
    //   from: "0x5a1B53309a7Cd58814a3732C7a58Dd48ea386B0e",
    //   amount: "8.11",
    //   currency: "MATIC",
    //   timestamp: 1663856923382,
    //   status: 0,
    //   pendingProcesses: [],
    //   privateKey: null,
    //   paymentRequestId: '73207b58-647e-41e6-9e2c-1e60a5ea4048'
    // },
    // {
    //   userPublicKey: "0xe564D4Bdf7AB5231676c698C8fE538cafA291277",
    //   from: "0x5a1B53309a7Cd58814a3732C7a58Dd48ea386B0e",
    //   amount: "9.88",
    //   currency: "EVE",
    //   timestamp: 1663856925314,
    //   status: 1,
    //   pendingProcesses: [],
    //   privateKey: null
    // },
    // {
    //   userPublicKey: "0xf89D4Bdf7AB5231676c698C8fE538cafA2912651",
    //   from: "0x7c9869d04F15300deC55CB0E3308d0898D51a59D",
    //   amount: "5.18",
    //   currency: "ETH",
    //   timestamp: 1663856926216,
    //   status: -1,
    //   pendingProcesses: [],
    //   privateKey: null
    // },
  ];

  public paymentsSent = [
    // {
    //   "nonce":0,
    //   "gasPrice":{
    //      "type":"BigNumber",
    //      "hex":"0x2e90edd000"
    //   },
    //   "gasLimit":{
    //      "type":"BigNumber",
    //      "hex":"0x0186a0"
    //   },
    //   "to":"0x7c9869d04F15300deC55CB0E3308d0898D51a59D",
    //   "value":{
    //      "type":"BigNumber",
    //      "hex":"0x0de0b6b3a7640000"
    //   },
    //   "data":"0x",
    //   "chainId":508674158,
    //   "v":1017348351,
    //   "r":"0x51345b597295d6e95789a9d5ce4dd7378b365c6eb90d22542b3c7d1ca334b6d8",
    //   "s":"0x5a873f6aba41474f441a11980c3d533a274c720556108d01fe44e6196aebe7b",
    //   "from":"0x1B0e0c8433468fe925f48a0e72AF82A24d81d499",
    //   "hash":"0xb3cd71255d4828421bb7db79a8fd82d19c55c084537a545c9f1594ef2d8e4810",
    //   "type":null,
    //   "timestamp": +new Date(),
    //   "confirmations":0
    // },
    // {
    //   "type":2,
    //   "chainId": 80001,
    //   "nonce":0,
    //   "maxPriorityFeePerGas": {
    //     "type": "BigNumber",
    //     "hex": "0x9502f90a"
    //   },
    //   "maxFeePerGas": {
    //     "type": "BigNumber",
    //     "hex":"0x9502f90a"
    //   },
    //   "gasPrice":{
    //      "type":"BigNumber",
    //      "hex":"0x2e90edd000"
    //   },
    //   "gasLimit": {
    //     "type": "BigNumber",
    //     "hex": "0x0186a0"
    //   },
    //   "to": "0x7c9869d04F15300deC55CB0E3308d0898D51a59D",
    //   "value": {
    //     "type": "BigNumber",
    //     "hex": "0x2386f26fc10000"
    //   },
    //   "data": "0x",
    //   "accessList": [],
    //   "hash": "0x764295ced106441546dbf9ea40985f7ca04993081d836680fd0b2d9fa3437a23",
    //   "v": 1,
    //   "r": "0x86ac2772a3fae4ff4cd:ad51a9e47463b441ca49e6b30359e29a76b562217d86",
    //   "s": "0x1e51d7dca7fa57114d7916ffff017b3e1761849acad60adddf9dac27f49622db",
    //   "from": "0xe37E4Bdf7AB2001676c698C8fE538cafA2912651",
    //   "timestamp": +new Date(),
    //   "confirmations":0
    // },
    // {
    //   "type":2,
    //   "chainId": 4,
    //   "nonce":0,
    //   "maxPriorityFeePerGas": {
    //     "type": "BigNumber",
    //     "hex": "0x9502f90a"
    //   },
    //   "maxFeePerGas": {
    //     "type": "BigNumber",
    //     "hex":"0x9502f90a"
    //   },
    //   "gasPrice":{
    //      "type":"BigNumber",
    //      "hex":"0x2e90edd000"
    //   },
    //   "gasLimit": {
    //     "type": "BigNumber",
    //     "hex": "0x0186a0"
    //   },
    //   "to": "0x36D744d37356182422EleeDf5967eFEA1489C182",
    //   "value": {
    //     "type": "BigNumber",
    //     "hex": "0x2386f26fc10000"
    //   },
    //   "data": "0x",
    //   "accessList": [],
    //   "hash": "0x764295ced106441546dbf9ea40985f7ca04993081d836680fd0b2d9fa3437a23",
    //   "v": 1,
    //   "r": "0x86ac2772a3fae4ff4cd:ad51a9e47463b441ca49e6b30359e29a76b562217d86",
    //   "s": "0x1e51d7dca7fa57114d7916ffff017b3e1761849acad60adddf9dac27f49622db",
    //   "from": "0x7c9869d04F15300deC55CB0E3308d0898D51a59D",
    //   "timestamp": +new Date(),
    //   "confirmations":0
    // }
  ];

  public compartmentChoice = 'sentPaymentSegment';

  constructor(
    private _NavController: NavController,
    private _SecureStorageService: SecureStorageService,
    private _ModalController: ModalController,
    private _ImageSelectionService: ImageSelectionService,
    private _Router: Router,
    private _NetworkService: NetworkService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
   }

  async ionViewWillEnter() {

    if( !this._NetworkService.checkConnection() ){
        this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    let checker = this._Router.parseUrl(this._Router.url).queryParams;
    console.log(checker);

    if(checker.address) {
      console.log(checker.address);
      this.address = checker.address;
    }

    var photo = this.userPhotoServiceAkita.getPhoto();
    this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg';

    var cryptoPaymentRequestsReceived = await this._SecureStorageService.getValue(SecureStorageKey.cryptoPaymentRequestsReceived, false)
    this.requestsReceived = !!cryptoPaymentRequestsReceived ? JSON.parse(cryptoPaymentRequestsReceived) : this.requestsReceived;

    var cryptoPaymentRequestsSent = await this._SecureStorageService.getValue(SecureStorageKey.cryptoPaymentRequestsSent, false);
    this.requestsSent = !!cryptoPaymentRequestsSent ? JSON.parse(cryptoPaymentRequestsSent) : this.requestsSent;

    var cryptoPaymentsSent = await this._SecureStorageService.getValue(SecureStorageKey.cryptoPaymentsSent, false);
    this.paymentsSent = !!cryptoPaymentsSent ? JSON.parse(cryptoPaymentsSent) : this.paymentsSent;

    console.log(this.requestsReceived);
    console.log(this.requestsSent);
    console.log(this.paymentsSent);

    try {
      this.requestsReceived = this.sortRequests(this.requestsReceived);
      this.requestsSent = this.sortRequests(this.requestsSent);
      this.paymentsSent = this.paymentsSent.reverse();
    } catch(e) {
      console.log(e);
    }

    console.log(this.requestsReceived);
    console.log(this.requestsSent);
    console.log(this.paymentsSent);

    if(this.address) {
      // this.requestsReceived = this.requestsReceived.filter(ll => ll.userPublicKey == this.address);
      this.requestsSent = this.requestsSent.filter(ll => {if(!!ll.from){ return ll.from == this.address} else { return true;} });
      this.paymentsSent = this.paymentsSent.filter(ll => ((ll.to == this.address) || (ll.from == this.address)));
    }

    console.log(this.requestsReceived);
    console.log(this.requestsSent);
    console.log(this.paymentsSent);

  }

  private sortRequests(ar: any) {
    if(ar.length > 1) {
      var tA1 = Array.from(ar, k => (k as any).timestamp);
      tA1.sort().reverse();
      var rr = [];
      for(let ia1 = 0; ia1 < tA1.length; ia1++) {
        var ta11 = tA1[ia1];
        rr.push(ar.find(rrd => rrd.timestamp == ta11));
      }
      return rr;
    } else {
      return ar;
    }
  }

  close() {
    this._NavController.navigateBack('/dashboard/payments');
  }

  async show(request: PaymentRequests) {
    console.log(request);
    const modal = await this._ModalController.create({
        component: PaymentModalComponent,
        componentProps: {
          data: request,
          total: this.requestsReceived,
          done: false
        }
      })
      modal.onDidDismiss().then(async () => {

      })
      await modal.present();
  }

  async showSentRequest(sentRequest) {
    console.log(sentRequest);
    const modal = await this._ModalController.create({
      component: PaymentModalComponent,
      componentProps: {
        data: sentRequest,
        total: this.requestsSent,
        done: true
      }
    })
    modal.onDidDismiss().then(async () => {

    })
    await modal.present();
  }

  returnIcon(ps: any) {
    var chainId = ps.chainId;
    var icon = '';
    switch(chainId) {
      case 508674158: icon = '../../assets/crypto/vector/EVE.svg'; break;
      case 49262: icon = '../../assets/crypto/vector/EVE.svg'; break;
      case 80001: icon = '../../assets/crypto/vector/MATIC.svg'; break;
      case 137: icon = '../../assets/crypto/vector/MATIC.svg'; break;
      case 100: icon = '../../assets/crypto/vector/XDAI.png'; break;
      case 4: icon = '../../assets/crypto/vector/ETH.svg'; break;
      case 1: icon = '../../assets/crypto/vector/ETH.svg'; break;
      default: icon = '../../assets/crypto/vector/MATIC.svg'; break;
    }
    return icon;
  }

  showTransactionDetail(ps: any) {
    this._ModalController.create({
      component: TransactionDetailComponent,
      componentProps: {
        data: ps
      }
    }).then(modal => {
      modal.onDidDismiss().then(() => {

      })
      modal.present();
    })
  }

  compartmentChange(event: any) {
    console.log(event.detail.value);
    this.compartmentChoice = event.detail.value;
  }

  displayPictureOptions() {
    this._ImageSelectionService.showChangePicture().then((photo) => {
      this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg';
    });
  }

}
