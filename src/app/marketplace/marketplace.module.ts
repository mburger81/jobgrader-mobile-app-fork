import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { SharedModule } from '../shared/shared.module';
import { MarketplacePage } from './marketplace.page';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
import { MarketPlaceElementComponent } from './market-place-element.component';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthenticationGuard],
    component: MarketplacePage,
    resolve: {
      user: UserProviderResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
  ],
  declarations: [
      MarketplacePage,
      MarketPlaceElementComponent
  ],
  providers: [SafariViewController]
})
export class MarketplacePageModule {}
