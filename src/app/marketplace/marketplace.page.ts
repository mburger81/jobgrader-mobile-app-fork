import { AfterViewChecked, Component, ViewChild } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { IonContent, ToastController, NavController, ModalController } from '@ionic/angular';
import { MarketplaceService } from '../core/providers/marketplace/marketplace.service';
import { MerchantDetails } from '../core/providers/marketplace/marketplace-model';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { environment } from 'src/environments/environment';
import { NetworkService } from '../core/providers/network/network-service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';


@Component({
    selector: 'app-marketplace',
    templateUrl: './marketplace.page.html',
    styleUrls: ['./marketplace.page.scss'],
})
export class MarketplacePage implements AfterViewChecked {

    @ViewChild(IonContent) public content: IonContent;

    private source;
    private address;
    profilePictureSrc;

    public logos: any = {
        mobility: 'mobility.svg',
        finance: 'Finance_Icon_Home_white.svg',
        event: 'events.svg',
        trustedPartner: 'trusted-partner.svg',
        eCommerce: 'e-commerce.svg',
        health: 'health.svg',
        crypto: 'crypto.svg',
        jobgrader: 'crypto.svg',
        communication: 'community.svg',
        publicSector: 'public-sector.svg',
        myCity: 'city.svg'
    }

    public placeholderMerchants: MerchantDetails[] = [
        // <MerchantDetails> { id: -1, merchantName: 'ShareMe', merchantLogo: '/assets/icon/marketplace/shareme.png'},
        <MerchantDetails> { id: -2, merchantName: 'Authada', merchantLogo: '../../assets/icon/marketplace/authada.png'},
        <MerchantDetails> { id: -2, merchantName: 'Blockchain Reallabor', merchantLogo: '../../assets/icon/marketplace/blockchain-reallabor_logo.png'},
        <MerchantDetails> { id: -2, merchantName: 'msg', merchantLogo: '../../assets/icon/marketplace/msg_og_logo.png'},
        <MerchantDetails> { id: -2, merchantName: 'Seal One', merchantLogo: '../../assets/icon/marketplace/sealone.png'},
        <MerchantDetails> { id: -2, merchantName: 'Swisscom', merchantLogo: '../../assets/icon/marketplace/swisscom.png'}
        // <MerchantDetails> { id: -2, merchantName: 'Exceet', merchantLogo: '/assets/icon/marketplace/exceet.png'},
        // <MerchantDetails> { id: -2, merchantName: 'Evan network', merchantLogo: '/assets/icon/marketplace/evan.png'},
        // <MerchantDetails> { id: -2, merchantName: 'City Network', merchantLogo: '/assets/icon/marketplace/city.png'},
        // <MerchantDetails> { id: -2, merchantName: 'Interxion', merchantLogo: '/assets/icon/marketplace/interxion.png'},
        // <MerchantDetails> { id: -2, merchantName: 'Veriff', merchantLogo: '/assets/icon/marketplace/veriff.png'},
    ];

    public merchants: MerchantDetails[];
    public title: string;
    public logoSrc: string;
    public isDemoPage: boolean = false;

    constructor(
        private router: Router,
        private sanitizer: DomSanitizer,
        private activatedRoute: ActivatedRoute,
        private translateProviderService: TranslateProviderService,
        private toastController: ToastController,
        private translate: TranslateProviderService,
        private nav: NavController,
        public _MarketplaceService: MarketplaceService,
        public _SecureStorageService: SecureStorageService,
        private _NetworkService: NetworkService,
        public _ModalController: ModalController,
        private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) {
    }

    ngAfterViewChecked() {}

    public ionViewWillEnter() {

        if( !this._NetworkService.checkConnection() ){
            this._NetworkService.addOfflineFooter('ion-footer');
        } else {
            this._NetworkService.removeOfflineFooter('ion-footer');
        }

        let checker = this.router.parseUrl(this.router.url).queryParams;
        console.log(checker);
        if(checker.source) {
            this.source = checker.source;
            console.log(this.source);
        }
        if(checker.address) {
            this.address = checker.address;
            console.log(this.address);
        }

        this.profilePictureSrc = this.userPhotoServiceAkita.getPhoto();

        var key = this.activatedRoute.snapshot.params.key;
        this.isDemoPage = (key == 'trustedPartner');
        this.title = this.translate.instant(`HOME.${key}`);
        this.logoSrc = this.logos[key];
        var thisCategoryMerchants = this._MarketplaceService.getMerchantsByCategory(key);
        // console.log('MarketplacePage#ionViewWillEnter; thisCategoryMerchants:', thisCategoryMerchants);
        thisCategoryMerchants = thisCategoryMerchants.filter(m => m.displayToggle);
        this.merchants = (this.isDemoPage) ? this.placeholderMerchants.concat(thisCategoryMerchants) : thisCategoryMerchants;
        // console.log('MarketplacePage#ionViewWillEnter; this.merchants:', this.merchants);
        this.translateProviderService.getLangFromStorage().then((lang) => {
            // console.log(lang);
            if(environment.helixEnv == 'integration') {
                this.merchants = (lang == 'de') ? this.merchants.filter(m => m.id != 180 ) : this.merchants.filter(m => m.id != 163 );
            }
        });
        this.content.scrollToTop(0);
    }

    async search() {
        var toast = await this.toastController.create({
            message: this.translate.instant('MARKETPLACE.searchSoonAvailable'),
            duration: 3000,
            position: 'top',
        });
        toast.present();
    }

    async sort() {
        var toast = await this.toastController.create({
            message: this.translate.instant('MARKETPLACE.sortSoonAvailable'),
            duration: 3000,
            position: 'top',
        });
        toast.present();
    }

    goHome() {

        if(this.source) {
            this.nav.navigateBack(this.source);
        } else {
            this.nav.navigateBack('/dashboard/tab-home');
        }

    }

    trustImage(src: string): SafeStyle {
        return this.sanitizer.bypassSecurityTrustStyle(`url('${src}')`);
    }

    viewMerchant(merchant: MerchantDetails): void {
        if(merchant.id == -2) {
            return;
        }

        if(this.address) {
            var url = (!!this.source ? `/dashboard/merchant-details/${this.activatedRoute.snapshot.params.key}/${merchant.id}?address=${this.address}&source=${encodeURIComponent(this.source)}` : `/dashboard/merchant-details/${this.activatedRoute.snapshot.params.key}/${merchant.id}?address=${this.address}`) ;
        } else {
            url = (!!this.source ? `/dashboard/merchant-details/${this.activatedRoute.snapshot.params.key}/${merchant.id}?source=${encodeURIComponent(this.source)}` : `/dashboard/merchant-details/${this.activatedRoute.snapshot.params.key}/${merchant.id}`) ;
        }

        this.nav.navigateForward(url);
    }


}
