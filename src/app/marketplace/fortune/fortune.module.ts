import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FortunePageRoutingModule } from './fortune-routing.module';

import { FortunePage } from './fortune.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { JobPage } from './job/job.page';
import { SwiperJobPage } from './swiper/swiper-job.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    FortunePageRoutingModule
  ],
  declarations: [ FortunePage, JobPage, SwiperJobPage ]
})
export class FortunePageModule {}
