import { Component, ElementRef, NgZone, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { GestureController, NavController, Platform, ToastController } from '@ionic/angular';
import { FortuneJob, HumanService } from 'src/app/core/providers/human/human.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { UserPhotoServiceAkita } from '../../../core/providers/state/user-photo/user-photo.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-fortune-swiper-job',
  templateUrl: './swiper-job.page.html',
  styleUrls: ['./swiper-job.page.scss'],
})
export class SwiperJobPage implements OnInit {

  userImage;
  private workerAddress: string;
  private escrowAddress: string;
  jobs: any[] = [];


  // startX: number = 0; // starting point of the swipe
  // endX: number = 0; //ending point of the swipe

  @ViewChildren('card', { read: ElementRef }) cards: QueryList<ElementRef>;
  @ViewChild('up', { read: ElementRef }) up: ElementRef;
  @ViewChild('down', { read: ElementRef }) down: ElementRef;


  constructor(
    private route: ActivatedRoute,
    private _NavController: NavController,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private humanSevice: HumanService,
    private gestureCrtl: GestureController,
    private plt: Platform,
    private renderer: Renderer2,
    private zone: NgZone,
    private _SecureStorage: SecureStorageService,
    private humanService: HumanService
  ) { }

  async ngOnInit() {
    this.userImage = this.userPhotoServiceAkita.getPhoto();
    this.workerAddress = await this._SecureStorage.getValue(SecureStorageKey.web3WalletPublicKey, false);
    this.escrowAddress = this.route.snapshot.paramMap.get('id');


    this.humanService.FecthFortuneJob(this.workerAddress, this.escrowAddress)
          .subscribe({
            next: (job: Array<any>) => {
              this.jobs = job;
              setTimeout(() => this.setGestureForCards(), 200);
            },
            error: (err: any) => {
              this.jobs = [];
            }
          });
  }

  setGestureForCards() {
    const cardArray = this.cards.toArray();

    for (let i  = 0; i < cardArray.length; i++) {
      const card = cardArray[i];

      const gesture = this.gestureCrtl.create({
        el: card.nativeElement,
        gestureName: 'fortune-swipe',
        onStart: (ev) => {
        },
        onMove: (ev) => {

          this.renderer.setStyle(card.nativeElement, 'transform', `translateX(${ev.deltaX}px) rotate(${ev.deltaX / 10}deg)`);

          if (ev.deltaX < 0) {

            this.renderer.setStyle(this.down.nativeElement, 'opacity', String(-ev.deltaX / 100));

          } else {

            this.renderer.setStyle(this.up.nativeElement, 'opacity', String(ev.deltaX / 100));

          }

        },
        onEnd: (ev) => {
          this.renderer.setStyle(card.nativeElement, 'transition', '.5s ease out');

          if (ev.deltaX > 150) {

            this.renderer.setStyle(card.nativeElement, 'transform', `translateX(${+this.plt.width()*2}px) rotate(${ev.deltaX / 2}deg)`);

            setTimeout(() => {
              this.zone.run(() => {
                this.jobs.splice(0, 1);
              })
            }, 500);

          } else if (ev.deltaX < -150) {

            this.renderer.setStyle(card.nativeElement, 'transform', `translateX(-${+this.plt.width()*2}px) rotate(${ev.deltaX / 2}deg)`);

            setTimeout(() => {
              this.zone.run(() => {
                this.jobs.splice(0, 1);
              })
            }, 500);

          } else {

            this.renderer.removeStyle(card.nativeElement, 'transform');

          }

          setTimeout(() => {
            if (this.down) this.renderer.setStyle(this.down.nativeElement, 'opacity', '0');
            if (this.up) this.renderer.setStyle(this.up.nativeElement, 'opacity', '0');
          }, 500);

        }
      });

      gesture.enable(true);
    }
  }

  goBack() {
    this._NavController.navigateBack('/dashboard/fortune');
  }

  sendFortunes() {
    console.log('SwiperJobPage#sendFortunes');

    this.humanSevice.sendFortunes({})
      .subscribe({
        next: (value) => { console.log('juhu'); }
      });
  }

  // touchStart(evt: any) {
  //   console.log('===> touchStart', evt);

  //   // this.startX = evt.touches[0].pageX;
  //   this.startX = evt.currentX;

  //   console.log('xxxxxxxxxxx');
  //   console.log('x');
  //   console.log('x');
  //   console.log('x');
  //   console.log('xxxxxxxxxxx');
  // }

  // touchMove(evt: any, index: number) {
  //   let deltaX = this.startX - evt.touches[0].pageX;
  //   let deg = deltaX / 10;
  //   this.endX = evt.touches[0].pageX;

  //   //Swipe gesture

  //   (<HTMLStyleElement>document.getElementById("card-" + index)).style.transform = "translateX(" + -deltaX + "px) rotate(" + -deg + "deg)";


  //   if ((this.endX - this.startX) < 0) { // show dislike icon
  //     (<HTMLStyleElement>document.getElementById("reject-icon")).style.opacity = String(deltaX / 100);
  //   }
  //   else {
  //     (<HTMLStyleElement>document.getElementById("accept-icon")).style.opacity = String(-deltaX / 100);
  //   }
  // }

  // touchEnd(index: number) {
  //   if (this.endX > 0) { //to avoid removing card on click
  //     let finalX = this.endX - this.startX;

  //     if (finalX > -100 && finalX < 100) { // reset card back to position
  //       (<HTMLStyleElement>document.getElementById("card-" + index)).style.transition = ".3s";
  //       (<HTMLStyleElement>document.getElementById("card-" + index)).style.transform = "translateX(0px) rotate(0deg)";

  //       // remove transition after 350 ms

  //       setTimeout(() => {
  //         (<HTMLStyleElement>document.getElementById("card-" + index)).style.transition = "0s";
  //       }, 350);
  //     }
  //     else if (finalX <= -100) {  //Reject Card
  //       (<HTMLStyleElement>document.getElementById("card-" + index)).style.transition = "1s";
  //       (<HTMLStyleElement>document.getElementById("card-" + index)).style.transform = "translateX(-1000px) rotate(-30deg)";

  //       //remove job from jobs array

  //       setTimeout(() => {
  //         this.jobs.splice(index, 1);
  //       }, 100);
  //     }
  //     else if (finalX >= 100) { //Accept Card
  //       (<HTMLStyleElement>document.getElementById("card-" + index)).style.transition = "1s";
  //       (<HTMLStyleElement>document.getElementById("card-" + index)).style.transform = "translateX(1000px) rotate(30deg)";

  //       //remove job from jobs array

  //       setTimeout(() => {
  //         this.jobs = this.jobs.splice(index, 1);
  //       }, 100);
  //     }

  //     //reset all

  //     this.startX = 0;
  //     this.endX = 0;
  //     (<HTMLStyleElement>document.getElementById("reject-icon")).style.opacity = "0";
  //     (<HTMLStyleElement>document.getElementById("accept-icon")).style.opacity = "0";
  //   }
  // }
}
