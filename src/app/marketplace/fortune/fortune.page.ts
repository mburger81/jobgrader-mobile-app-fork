import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FortuneJob, HumanService } from 'src/app/core/providers/human/human.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { UserPhotoServiceAkita } from '../../core/providers/state/user-photo/user-photo.service';
import { ApiProviderService } from '../../core/providers/api/api-provider.service';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';

@Component({
  selector: 'app-fortune',
  templateUrl: './fortune.page.html',
  styleUrls: ['./fortune.page.scss'],
})
export class FortunePage implements OnInit {

  userImage;
  loading = false;

  jobs = <Array<FortuneJob>>[];
  dashBoardData = {
    // available: '-',
    // estimated: '-',
    // recent: '-',
    total: '-',
    served: '-',
    solved: '-',
    verified: '-'
  }

  constructor(
    private _Human: HumanService,
    private _NavController: NavController,
    private _ToastController: ToastController,
    private _SecureStorageService: SecureStorageService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private _ApiProviderService: ApiProviderService,
    public _Theme: ThemeSwitcherService
  ) {}

  async ngOnInit() {
    this.userImage = this.userPhotoServiceAkita.getPhoto();

    this.methodForUpdatingDashboard();
  }

  async fetchJobs() {
    var address = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    this._Human.FecthFortuneTasks(address)
          .subscribe({
            next: (jobs: FortuneJob[]) => {
              this.jobs = jobs;
            },
            error: (err: any) => {
              this.jobs = [];
            }
          });
  }

  presentToast(message: string) {
    this._ToastController.create({
      message,
      position: 'top',
      duration: 2000
    }).then((toast) => {
      toast.present();
    })
  }

  goHome() {
    this._NavController.navigateBack('/dashboard/tab-home');
  }

  openJob(job: FortuneJob) {
    console.log('job', job);

    if (job.type === 1) {

      this._NavController.navigateBack('/dashboard/fortune/job/' + job.address);

    } else if (job.type === 2) {

      this._NavController.navigateBack('/dashboard/fortune/swiper/' + job.address);

    }
  }

  methodForUpdatingDashboard() {
    this.loading = true;
    this._SecureStorageService.getValue(SecureStorageKey.hCaptchaDashboard, false).then((dashboardString) => {
      this.dashBoardData = !!dashboardString ? JSON.parse(dashboardString) : this.dashBoardData;
      this._ApiProviderService.ObtainhCaptchaUserDashboardData().then((response) => {
        this.dashBoardData = {
          total: `${!!response.balance.total ? Number(Number(response.balance.total).toFixed(8)) : 0}`,
          served: response.served,
          solved: response.solved,
          verified: response.verified
        }
        this._SecureStorageService.setValue(SecureStorageKey.hCaptchaDashboard, JSON.stringify(this.dashBoardData)).then(() => {
          // console.log('Done!');
          this.loading = false;
        })
      }).catch((e) => {
        // this.presentToast(this._Translate.instant('WALLET.endpointError'));
        this.loading = false;
      })
    })
  }
}
