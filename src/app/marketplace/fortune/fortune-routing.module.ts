import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FortunePage } from './fortune.page';
import { JobPage } from './job/job.page';
import { SwiperJobPage } from './swiper/swiper-job.page';

const routes: Routes = [
  {
    path: '',
    component: FortunePage
  },
  { path: 'job/:id', component: JobPage },
  { path: 'swiper/:id', component: SwiperJobPage },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FortunePageRoutingModule {}
