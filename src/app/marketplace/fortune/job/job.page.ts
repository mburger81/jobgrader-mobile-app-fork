import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FortuneJob, HumanService } from 'src/app/core/providers/human/human.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { UserPhotoServiceAkita } from '../../../core/providers/state/user-photo/user-photo.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fortune-job',
  templateUrl: './job.page.html',
  styleUrls: ['./job.page.scss'],
})
export class JobPage implements OnInit {

  userImage;
  address: string;

  constructor(
    private route: ActivatedRoute,
    private _NavController: NavController,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private humanSevice: HumanService
  ) { }

  async ngOnInit() {
    this.userImage = this.userPhotoServiceAkita.getPhoto();
    this.address = this.route.snapshot.paramMap.get('id');
  }

  goBack() {
    this._NavController.navigateBack('/dashboard/fortune');
  }

  sendFortunes() {
    console.log('JobPage#sendFortunes');

    this.humanSevice.sendFortunes({})
      .subscribe({
        next: (value) => { console.log('juhu'); }
      });
  }
}
