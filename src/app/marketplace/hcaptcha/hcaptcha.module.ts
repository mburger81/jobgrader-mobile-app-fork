import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HcaptchaPageRoutingModule } from './hcaptcha-routing.module';

import { HcaptchaPage } from './hcaptcha.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    HcaptchaPageRoutingModule
  ],
  declarations: [HcaptchaPage]
})
export class HcaptchaPageModule {}
