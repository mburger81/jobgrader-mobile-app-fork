import { Component, OnInit } from '@angular/core';
// import '@hcaptcha/types';
import { NavController, ToastController } from '@ionic/angular';
import * as Sentry from '@sentry/angular-ivy';
import { Subscription } from 'rxjs';


// custom imports
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { AppStateService } from 'src/app/core/providers/app-state/app-state.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { UserPhotoServiceAkita } from '../../core/providers/state/user-photo/user-photo.service';
import { environment } from '../../../environments/environment';

declare var hcaptcha: any;
declare var window: any;

@Component({
  selector: 'app-hcaptcha',
  templateUrl: './hcaptcha.page.html',
  styleUrls: ['./hcaptcha.page.scss'],
})
export class HcaptchaPage implements OnInit {

  userImage;
  loading = false;

  private siteKey;
  private secret;

  private timestampSubscription: Subscription;

  public keys = [];

  public dashBoardData = {
    // available: '-',
    // estimated: '-',
    // recent: '-',
    total: '-',
    served: '-',
    solved: '-',
    verified: '-'
  }

  hCaptchaShowing = false;
  jobReloading = false;

  private captchaId;
  private _consoleError;

  constructor(
    private _ApiProviderService: ApiProviderService,
    private _SecureStorageService: SecureStorageService,
    private _NavController: NavController,
    public _Theme: ThemeSwitcherService,
    private _ToastController: ToastController,
    private _Translate: TranslateProviderService,
    private _AppStateService: AppStateService,
    private _NetworkService: NetworkService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) { }

  ngOnInit() {

    this.userImage = this.userPhotoServiceAkita.getPhoto();

    this._ApiProviderService.ObtainSiteKeys().then(response => {
      if(response != '') {
          var siteKey = response;
          this._SecureStorageService.setValue(SecureStorageKey.siteKey, siteKey, true, false);
          this.siteKey = siteKey;
          this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then((address) => {
            this.secret = address;
          })
      } else {
          this.presentToast(this._Translate.instant('SITEKEY_ERROR'));
          this._NavController.navigateBack('/dashboard/tab-home');
      }
    }).catch(e => {
        this.presentToast(this._Translate.instant('SITEKEY_ERROR'));
        this._NavController.navigateBack('/dashboard/tab-home');
    });

    if( !this._NetworkService.checkConnection() ){
        this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    this.keys = Object.keys(this.dashBoardData);

    if (this.timestampSubscription) {
      if (!this.timestampSubscription.closed) {
        this.timestampSubscription.unsubscribe();
      }
    }

    // this.timestampSubscription = this.timestampObserver.subscribe(() =>
      this.methodForUpdatingDashboard();
    // );

  }

  ionViewWillEnter() {
    this.hCaptchaShowing = false;
    this.jobReloading = false;

    this._consoleError = (window as any).console.error;

    (window as any).console.error = (error) => { this.onError(error); };
  }

  ionViewWillLeave() {
    if (this._consoleError) {
      (window as any).console.error = this._consoleError;
      this._consoleError = null;
    }

    if (this.timestampSubscription) {
      if (!this.timestampSubscription.closed) {
        this.timestampSubscription.unsubscribe();
      }
    }

    if (this.captchaId) {
      hcaptcha.reset(this.captchaId);
      this.captchaId = null;
    }
    document.querySelector('body > div:last-of-type')?.remove();
  }


  methodForUpdatingDashboard() {
    this.loading = true;
    this._SecureStorageService.getValue(SecureStorageKey.hCaptchaDashboard, false).then((dashboardString) => {
      this.dashBoardData = !!dashboardString ? JSON.parse(dashboardString) : this.dashBoardData;
      this._ApiProviderService.ObtainhCaptchaUserDashboardData().then((response) => {
        this.dataSetChart(response.dropoff_data);
        this.dashBoardData = {
          // available: `${!!response.balance.available ? response.balance.available : 0} HMT`,
          // estimated: `${!!response.balance.estimated ? response.balance.estimated : 0} HMT`,
          // recent: `${!!response.balance.recent ? response.balance.recent : 0} HMT`,
          total: `${!!response.balance.total ? Number(Number(response.balance.total).toFixed(8)) : 0}`,
          served: response.served,
          solved: response.solved,
          verified: response.verified
        }
        this._SecureStorageService.setValue(SecureStorageKey.hCaptchaDashboard, JSON.stringify(this.dashBoardData)).then(() => {
          // console.log('Done!');
          this.loading = false;
        })
      }).catch((e) => {
        // this.presentToast(this._Translate.instant('WALLET.endpointError'));
        this.loading = false;
      })
    })
  }

  presentToast(message: string) {
    this._ToastController.create({
      message,
      position: 'top',
      duration: 2000
    }).then((toast) => {
      toast.present();
    })
  }


  initCaptcha() {

    this.hCaptchaShowing = true;

    if (!this.captchaId) {
      this.captchaId = hcaptcha.render('h-captcha', {
        sitekey: this.siteKey,
        theme: this._Theme.getCurrentTheme(),
        'error-callback': (error) => this.onError(error),
      });
    }

    this.showCaptcha();
  }

  private showCaptcha() {
    // var hcaptcha = !!hcaptcha ? hcaptcha : ((window as any).hcaptcha);
    // console.log("hcaptcha showCaptcha");


    if(!this._AppStateService.basicAuthToken) {
        this._ApiProviderService.noUserLoggedInAlert();
        return;
    }


    const captcha = hcaptcha.execute(this.captchaId, { async: true });

    if (captcha) {
      captcha
        .then((data) => {
          // console.log('HCaptcha#showCaptcha; succes:', data);

          if (data?.success === false) {
            // this sends the error even to Sentry Log
            console.error('HCaptcha#showCaptcha; success === false', data);
          }

          this._ApiProviderService.HCaptchaSiteVerify({ response: data.response, key: data.key, sitekey: this.siteKey, secret: this.secret }).then((afterSubmission) => {
            // console.log("HCaptcha#showCaptcha; afterSubmission:", afterSubmission);
          })
          .catch(e => {

            if (this._consoleError) {
              this._consoleError("HCaptcha#showCaptcha; HCaptchaSiteVerify error:", e);
            } else {
              console.error("HCaptcha#showCaptcha; HCaptchaSiteVerify error:", e);
            }

          });

          this.jobReloading = true;
          this.showCaptcha();
        })
        .catch((error) => {
          // mburger TODO: maybe check which type of error and show a message if needed
          // 'challenge-closed' for example doesnt need any info but
          // 'challenge-error' and others should show an error
          // 'challenge-expired' should maybe reopen the captcha
          // have a look at the other errors here https://docs.hcaptcha.com/configuration
          console.log('HCaptcha#showCaptcha; error:', error);

          this.hCaptchaShowing = false;
          this.jobReloading = false;
        });
    }
  }

  goHome() {
    this._NavController.navigateBack('/dashboard/tab-home');
  }

  onError(error) {
    if (this._consoleError) {
      this._consoleError("HcaptchaPage#onError; error:", error);
    } else {
      console.error("HcaptchaPage#onError; error:", error);
    }

    if(environment.production) {
      // log error to sentry
      try {
          Sentry.captureException(error.originalError || error);
      } catch (e) {
          // do nothing
      }
    }
  }

dataSetChart(chartDataSet: any) {

  console.log(chartDataSet);

  var id = "#dataset";
  var d3 = window.d3;

  d3.select(id).selectAll("*").remove();
  d3.select("#legend2").selectAll("*").remove();
  // d3.select("#demo").selectAll("*").remove();

  var getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  };

  var keys = Object.keys(chartDataSet);
  var preparedObjectArray = [];
  for(let i=0;i<keys.length;i++) {
    var key = keys[i];
    preparedObjectArray.push({
      // year: Math.floor((new Date(key).getFullYear()/100 - Math.floor(new Date(key).getFullYear()/100))*100)*100 + new Date(key).getMonth() + 1, // +new Date(key)
      year: +new Date(key),
      name: 'served',
      n: chartDataSet[key]['served']
    })
    preparedObjectArray.push({
      // year: Math.floor((new Date(key).getFullYear()/100 - Math.floor(new Date(key).getFullYear()/100))*100)*100 + new Date(key).getMonth() + 1, // +new Date(key)
      year: +new Date(key),
      name: 'solved',
      n: chartDataSet[key]['solved']
    })
  }

  console.log("preparedObjectArray", preparedObjectArray);

  // var data = [
  //   { year: 2016, name:"Reputation", n: getRandomInt(0, 80) },
  //   { year: 2016, name:"Leadership", n: getRandomInt(0, 80) },
  //   { year: 2017, name:"Reputation", n: getRandomInt(0, 80) },
  //   { year: 2017, name:"Leadership", n: getRandomInt(0, 80) },
  //   { year: 2018, name:"Reputation", n: getRandomInt(0, 80) },
  //   { year: 2018, name:"Leadership", n: getRandomInt(0, 80) },
  //   { year: 2019, name:"Reputation", n: getRandomInt(0, 80) },
  //   { year: 2019, name:"Leadership", n: getRandomInt(0, 80) },
  //   { year: 2020, name:"Reputation", n: getRandomInt(0, 80) },
  //   { year: 2020, name:"Leadership", n: getRandomInt(0, 80) },
  //   { year: 2021, name:"Reputation", n: getRandomInt(0, 80) },
  //   { year: 2021, name:"Leadership", n: getRandomInt(0, 80) },
  // ]

  var data = preparedObjectArray;
  // var data = preparedObjectArray.splice(preparedObjectArray.length-5,preparedObjectArray.length);

  var margin = {top: 10, right: 60, bottom: 30, left: 30},
  width = window.innerWidth - 100, // 300
  height = 120;

  var svg = d3.select(id)
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

  //console.log(svg);


  var sumstat = d3.nest() // nest function allows to group the calculation per level of a factor
  .key(function(d) { return d.name;})
  .entries(data);

  // Add X axis --> it is a date format
  var x = d3.scaleLinear()
    .domain(d3.extent(data, function(d) { return d.year; }))
    .range([ 0, width ]);
  svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x)
    .ticks(5).tickFormat(d3.timeFormat('%b-%d'))
    // .ticks(d3.time.months, 1).tickFormat(d3.time.format('%m'))
    // .tickArguments([3, "f"])
    )
    .attr("stroke", this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
    .attr("stroke-width", 0.4);

  // Add Y axis
  var y = d3.scaleLinear()
    .domain([0, d3.max(data, function(d) { return +d.n; })])
    .range([ height, 0 ]);
  svg.append("g")
    .call(d3.axisLeft(y).tickFormat(x => `${x.toFixed()}`))
    .attr("stroke", this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
    .attr("stroke-width", 0.4);

  // color palette
  var res = sumstat.map(function(d){ return d.key }) // list of group names
  var color = d3.scaleOrdinal()
    .domain(res)
    .range(['#54BF7B','#FF9C00'])

  // console.log(sumstat);

  // Draw the line
  svg.selectAll(".line")
      .data(sumstat)
      .enter()
      .append("path")
        .attr("fill", "none")
        .attr("stroke", function(d){ return color(d.key) })
        .attr("stroke-width", 1.5)
        .attr("d", function(d){
          return d3.svg.line().interpolate("basis")
            .x(
            function(d) { 
              // console.log("d1", d.year);
              // console.log("d2", x(d.year));
              return x(d.year); 
            })
            .y(function(d) { return y(+d.n); })
            (d.values)
        })

  var legend = d3
  .select('#legend2')
  .append('svg')
  .attr("height", 50)
            .selectAll('.legendItem')
            .data(sumstat);

  legend
    .enter()
    .append('rect')
    .attr('class', 'legendItem')
    .attr('width', 12)
    .attr('height', 12)
    .style('fill', d => color(d.key))
    .attr('transform',
                  (d, i) => {
                      var x = 10;
                      var y = 10 + (12 + 4) * i;
                      return `translate(${x}, ${y})`;
                  });


  legend
    .enter()
    .append('text')
    .attr('x', 10 + 12 + 5)
    .attr('y', (d, i) => 10 + (12 + 4) * i + 12)
    .text(d => this._Translate.instant('HCAPTCHA_DASHBOARD.'+d.key.toLowerCase()) )
    .attr("stroke",  this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
    .attr("stroke-width", 0.2)
    .attr("fill", this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000");

  }

}
