import { KycProvider } from '../enums/kyc-provider.enum';
import { KycState } from '../enums/kyc-state.enum';

export interface KycStatus {
    kycProvider: KycProvider;
    kycInitiatedTimeStamp: number;
    kycState: KycState;
    additionalInfo: any;
}
