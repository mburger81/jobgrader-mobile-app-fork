import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// import { TooltipsModule } from 'ionic-tooltips';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { KycPage } from './kyc.page';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
import { KycService } from './services/kyc.service';
import { OtcModule } from './otc/otc.module';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthenticationGuard],
        component: KycPage,
        resolve: {
            user: UserProviderResolver
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        // TooltipsModule,
        JobHeaderModule,
        OtcModule,
        TranslateModule.forChild(),
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
    providers: [
        SafariViewController
    ],
    declarations: [KycPage]
})
export class KycPageModule {
}
