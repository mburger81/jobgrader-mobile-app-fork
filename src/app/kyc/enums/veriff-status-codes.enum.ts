export enum VeriffStatusCodes {
    VERIFF_9001 = 'VERIFF_9001', // positive
    VERIFF_9102 = 'VERIFF_9102', // negative
    VERIFF_9103 = 'VERIFF_9103', // resubmit
    VERIFF_9104 = 'VERIFF_9104', // expired
    VERIFF_9151 = 'VERIFF_9151' // intermediate
}
