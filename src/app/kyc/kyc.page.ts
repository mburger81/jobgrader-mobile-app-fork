import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../core/models/User';
import { KycService } from './services/kyc.service';
import { KycProvider, KycVendors } from './enums/kyc-provider.enum';
import { KycState } from './enums/kyc-state.enum';
import { AlertsProviderService } from '../core/providers/alerts/alerts-provider.service';
import { environment } from '../../environments/environment';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { CountryMode, SignProviderService, ProviderDocument } from '../core/providers/sign/sign-provider.service';
import { LockService } from '../lock-screen/lock.service';
import { NavController, ModalController, ToastController, AlertController } from '@ionic/angular';
import { OtcComponent } from './otc/otc.component';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { uniq } from 'lodash';
import { NetworkService } from '../core/providers/network/network-service';
import { KycPins } from '../core/providers/device/udid.enum';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

declare var startApp: any;

enum FormControlNames {
  idCard = 'idCard',
  driverLicence = 'driverLicence',
  identity = 'identity'
}

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.page.html',
  styleUrls: ['./kyc.page.scss'],
})
export class KycPage implements OnInit {

  userImage;
  user: User = {};

  public kycForm: UntypedFormGroup;

  /** The flag modes */
  public countryModes = CountryMode;

  /** The form control names */
  public formControlNames: typeof FormControlNames = FormControlNames;

  public avaliableDocuments: any[] = [
    {
      label: this.translateService.instant('PERSONALDETAILS.radioPassport'),
      value: 'PASSPORT',
    },
    {
      label: this.translateService.instant('PERSONALDETAILS.radioIDCard'),
      value: 'ID_CARD'
    },
    {
      label: this.translateService.instant('PERSONALDETAILS.radioResidencePermit'),
      value: 'RESIDENCE_PERMIT'
    },
    {
      label: this.translateService.instant('PERSONALDETAILS.radioDL'),
      value: 'DRIVERS_LICENSE'
    },
  ];

  public displayOTC = true

  constructor(
    private nav: NavController,
      public translateService: TranslateService,
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private kycService: KycService,
      private alertService: AlertsProviderService,
      private formBuilder: UntypedFormBuilder,
      private signService: SignProviderService,
      private lockService: LockService,
      private _ModalController: ModalController,
      private _SecureStorageService: SecureStorageService,
      private _ToastController: ToastController,
      private _NetworkService: NetworkService,
      private _AlertController: AlertController,
      private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
    this.kycForm = this.formBuilder.group({
      [FormControlNames.idCard]: new UntypedFormControl(),
      [FormControlNames.identity]: new UntypedFormControl(),
      [FormControlNames.driverLicence]: new UntypedFormControl()
    });
  }

  /** Callback for the back button */
  public async goBack(): Promise<void> {
      // let checker = this.router.parseUrl(this.router.url).queryParams;
      // console.log(checker);
      // if((['onboarding'].includes(checker.from))){
      //   this.nav.navigateBack('/dashboard/tab-profile?source=smart');
      // } else {
        let checker = this.router.parseUrl(this.router.url).queryParams;
        if(!checker.source) {
          this.nav.navigateBack('/dashboard/personal-details');
        } else {
          this.nav.navigateBack('/dashboard/tab-home');
        }

      // }
  }

  /** @inheritDoc */
  public ngOnInit(): void {}

  public ionViewWillEnter(): void {
    this.user = this.activatedRoute.snapshot.data.user;
    this._SecureStorageService.getValue(SecureStorageKey.powerUserMode, false).then(value => {
      this.displayOTC = (value == 'true')
    })

    this.userImage = this.userPhotoServiceAkita.getPhoto();

    if(!this._NetworkService.checkConnection()) {
        this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    // source=wallet



  }

  /** Check whether driver license is valid */
  public get checkDriverLicense(): boolean {
    return !this.user || !(this.user.driverlicencedocumentnumberStatus === 1 &&
        this.user.driverlicencecountryStatus === 1 &&
        this.user.driverlicenceexpirydateStatus === 1 &&
        this.user.driverlicenceissuedateStatus === 1);
  }

  /** Callback for the verification of id cards */
  public async veriffIdCard(info: string): Promise<void> {
    if (!info) { return; }
    const provider = environment.verificationProd ?
        KycProvider.VERIFF_PROD_IDCARD :
        KycProvider.VERIFF_TEST_IDCARD;
    await this.kycService.addInitialState(provider);
    let {code2}: {code2: string} = this.signService.idCardSupportedCountries.find(c => c.code3 === info);
    code2 = code2.toLowerCase();
    this.lockService.addResume();
    if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
      await this.alreadyRetrieved(provider);
    } else {
      this.kycService.patchState(provider, KycState.KYC_WAITING);
      await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {type: 'ID_CARD', country: code2});
    }
  }

  public async initiateOndato(info: string): Promise<void> {
      if( info == 'ONDATO') {
        const provider = environment.verificationProd ? KycProvider.ONDATO_PROD : KycProvider.ONDATO_TEST;
        await this.kycService.addInitialState(provider);
        // console.log(provider);
        if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
          await this.alreadyRetrieved(provider);
        } else {
          this.kycService.patchState(provider, KycState.KYC_WAITING);
          await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {});
        }
      } else {
        console.log('Wrong emitter passed');
      }
  }

  public async veriffIdentity(info: string): Promise<void> {
    if (!info) { return; }

    if(!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      this._ToastController.create({
        message: this.translateService.instant('NETWORK.checkConnection'),
        duration: 3000,
        position: 'top',
      }).then(toast => toast.present())
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    let {code2, supportedDocuments}: {code2: string, supportedDocuments: Array<ProviderDocument>} = this.signService.identityCountries.find(c => c.code3 === info);
    // console.log(supportedDocuments);
    code2 = code2.toLowerCase();
    var tt = supportedDocuments.map(k => k.documents)
    const unionSet = uniq([...tt[0],...tt[1]]);
    // const unionSet = uniq([...tt[0]]);
    // console.log(unionSet);
    let buttonArray = [];
    unionSet.forEach(l => {
      buttonArray.push({
        text: this.avaliableDocuments[this.avaliableDocuments.findIndex(k => k.value == l)].label,
        value: l,
        handler: async () => {
          await this.processDocument(l, code2, supportedDocuments);
        }
      })
    })
    buttonArray.push({
      text: this.translateService.instant('BUTTON.CANCEL'),
      role: 'cancel',
      cssClass: 'secondary',
      handler: () => {
        console.log('Document Selection Cancelled');
      }
    });
    // console.log(JSON.stringify(buttonArray))
    await this.alertService.alertCreate(this.translateService.instant('KYC.DOCUMENTSELECTOR.header'),
    this.translateService.instant('KYC.DOCUMENTSELECTOR.subheader'),'',
    buttonArray);
    this.lockService.addResume();
  }

  public async processDocument(docType: string, country: string, supportedDocuments: Array<ProviderDocument>){
    // console.log(JSON.stringify({type: docType, country: country}));

    if(!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      this._ToastController.create({
        message: this.translateService.instant('NETWORK.checkConnection'),
        duration: 3000,
        position: 'top',
      }).then(toast => toast.present())
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    var providerArray: Array<KycVendors> = []

    supportedDocuments.forEach(l => {
      if(l.documents.includes(docType)){
        providerArray.push(l.provider)
      }
    })

    providerArray = uniq(providerArray);

    const kycWithVeriff = async () => {
      const docState: {prod: KycProvider; test: KycProvider} = this.getStateArray(docType);
      const provider = environment.verificationProd ? docState.prod : docState.test;
      await this.kycService.addInitialState(provider);
      // console.log(provider);
      if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
        await this.alreadyRetrieved(provider);
      } else {
        this.kycService.patchState(provider, KycState.KYC_WAITING);
        await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {type: docType, country: country});
      }
    }

    const kycWithVerifeye = async () => {
      console.log('Verifeye Begin');
      const docState: {prod: KycProvider; test: KycProvider} = this.getVerfieyeStateArray(docType);
      const provider = environment.verificationProd ? docState.prod : docState.test;
      // console.log(provider);
      // console.log(docState);
      await this.kycService.addInitialState(provider);
      // console.log(provider);
      if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
        // console.log('1');
        await this.alreadyRetrieved(provider);
      } else {
        // console.log('2');
        this.kycService.patchState(provider, KycState.KYC_WAITING);
        await this.kycService.setState(provider, KycState.KYC_INITIATED, country);
      }
    }

    if(providerArray.length == 1) {
      providerArray[0] == KycVendors.VERIFF ? await kycWithVeriff() : await kycWithVerifeye();
    } else {
      // (await this._SecureStorageService.getValue(SecureStorageKey.kycUsingVerifeyeCompleted, false) == "true") ?
      // await this.alertService.alertCreate(
      //   this.translateService.instant('KYC.SELECT_VENDOR.header'),
      //   this.translateService.instant('KYC.SELECT_VENDOR.subheader'),
      //   this.translateService.instant('KYC.SELECT_VENDOR.message'),
      //   [
      //     { text: KycVendors.VERIFF, handler: async () => { await kycWithVeriff(); } },
      //     { text: KycVendors.VERIFEYE, handler: async () => { await kycWithVerifeye(); } },
      //     { text: this.translateService.instant('BUTTON.CANCEL'), role: 'cancel', handler: async () => {} }
      //   ]) :
        await kycWithVerifeye();
    }
  }

  public getVerfieyeStateArray(docType: string): {prod: KycProvider, test: KycProvider}{
    switch (docType) {
      case 'PASSPORT':
        return {
          prod: KycProvider.VERIFEYE_PROD_PASSPORT,
          test: KycProvider.VERIFEYE_TEST_PASSPORT
        };
      case 'ID_CARD':
        return {
          prod: KycProvider.VERIFEYE_PROD_IDCARD,
          test: KycProvider.VERIFEYE_TEST_IDCARD
        };
      case 'RESIDENCE_PERMIT':
        return {
          prod: KycProvider.VERIFEYE_PROD_RESIDENCEPERMIT,
          test: KycProvider.VERIFEYE_TEST_RESIDENCEPERMIT
        };
      case 'DRIVERS_LICENSE':
        return {
          prod: KycProvider.VERIFEYE_PROD_DRIVINGLICENCE,
          test: KycProvider.VERIFEYE_TEST_DRIVINGLICENCE
        };
    }
  }

  public getStateArray(docType: string): {prod: KycProvider, test: KycProvider}{
    switch (docType) {
      case 'PASSPORT':
        return {
          prod: KycProvider.VERIFF_PROD_PASSPORT,
          test: KycProvider.VERIFF_TEST_PASSPORT
        };
      case 'RESIDENCE_PERMIT':
        return {
          prod: KycProvider.VERIFF_PROD_RESIDENCEPERMIT,
          test: KycProvider.VERIFF_TEST_RESIDENCEPERMIT
        };
      case 'ID_CARD':
        return {
          prod: KycProvider.VERIFF_PROD_IDCARD,
          test: KycProvider.VERIFF_TEST_IDCARD
        };
    }
  }

  /** Callback for verification of driver licenses */
  public async veriffDriverLicense(info: string): Promise<void> {
    if (!info) { return; }

    if(!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      this._ToastController.create({
        message: this.translateService.instant('NETWORK.checkConnection'),
        duration: 3000,
        position: 'top',
      }).then(toast => toast.present())
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    let {code2, supportedProviders}: {code2: string, supportedProviders: Array<KycVendors>} = this.signService.dlSupportedCountries.find(c => c.code3 === info);
    code2 = code2.toLowerCase();

    // const kycWithVeriff = async () => {
    //   const provider = environment.verificationProd ? KycProvider.VERIFF_PROD_DRIVINGLICENCE : KycProvider.VERIFF_TEST_DRIVINGLICENCE;
    //   await this.kycService.addInitialState(provider);
    //   this.lockService.addResume();
    //   if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
    //     await this.alreadyRetrieved(provider);
    //   } else {
    //     this.kycService.patchState(provider, KycState.KYC_WAITING);
    //     await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {type: 'DRIVERS_LICENSE', country: code2});
    //   }
    // }

    // const kycWithVerifeye = async () => {
    //   const provider = environment.verificationProd ? KycProvider.VERIFEYE_PROD_DRIVINGLICENCE : KycProvider.VERIFEYE_TEST_DRIVINGLICENCE;
    //   await this.kycService.addInitialState(provider);
    //   this.lockService.addResume();
    //   if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
    //     await this.alreadyRetrieved(provider);
    //   } else {
    //     this.kycService.patchState(provider, KycState.KYC_WAITING);
    //     await this.kycService.setState(provider, KycState.KYC_INITIATED, code2);
    //   }
    // }

    await this.initiateOndato('ONDATO');

    // if(supportedProviders.length == 1){
    //   supportedProviders[0] == KycVendors.VERIFF ? await kycWithVeriff() : await kycWithVerifeye();
    // } else {
    //   // (await this._SecureStorageService.getValue(SecureStorageKey.kycUsingVerifeyeCompleted, false) == "true") ?
    //   // await this.alertService.alertCreate(
    //   //   this.translateService.instant('KYC.SELECT_VENDOR.header'),
    //   //   this.translateService.instant('KYC.SELECT_VENDOR.subheader'),
    //   //   this.translateService.instant('KYC.SELECT_VENDOR.message'),
    //   //   [
    //   //     { text: KycVendors.VERIFF, handler: async () => { await kycWithVeriff(); } },
    //   //     { text: KycVendors.VERIFEYE, handler: async () => { await kycWithVerifeye(); } },
    //   //     { text: this.translateService.instant('BUTTON.CANCEL'), role: 'cancel', handler: () => {} }
    //   // ]) :
    //   await kycWithVerifeye();
    // }
  }

  /** Callback for authada verification */
  public async authada(): Promise<void> {
    var placeholder = environment.production ? '' : KycPins.authada;
    var alert = await this._AlertController.create({
      mode: 'ios',
      header: this.translateService.instant('KYC.KYCPIN.header'),
      message: this.translateService.instant('KYC.KYCPIN.message1'),
      inputs: [ { name: 'kycpin', placeholder: placeholder, type: 'tel'} ],
      buttons: [ {
        text: this.translateService.instant('SETTINGS.ok'),
        handler: async (data) => {
            (data.kycpin == KycPins.authada) ? await this.contiueLoadAuthada() :
              this._AlertController.create({
                mode: 'ios',
                message: this.translateService.instant('KYC.KYCPIN.message2'),
                buttons: [ { text: this.translateService.instant('SETTINGS.ok'), role: 'cancel', handler: () => { } } ]
              }).then((al) => al.present());
          }
        }, { text: this.translateService.instant('SETTINGS.cancel'), role: 'cancel', handler: () => { } }
      ]
    });
    await alert.present();

  }

  private async contiueLoadAuthada() {
    const provider = environment.verificationProd ?
        KycProvider.AUTHADA_PROD :
        KycProvider.AUTHADA_TEST;
    await this.kycService.addInitialState(provider);
    this.lockService.addResume();
    if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
      await this.alreadyRetrieved(provider);
    }
    else {
      await this.kycService.patchState(provider, KycState.KYC_WAITING);
      await this.kycService.setState(provider, KycState.KYC_INITIATED);
    }

    // if (this._Platform.is('ios')) {
    //   startApp.set(appScheme).start();
    // }
    // else if (this._Platform.is('android')) {
    //   startApp.set({ "application": appScheme }).start();
    // }
  }

  public get driverLicenseAdditionalText(): {[key: string]: string} {
    return {}
  }

  private async alreadyRetrieved(provider: KycProvider): Promise<void> {
    await this.alertService.alertCreate(
        this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.header`),
        this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.sub-header`),
        this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.message`),
        [
          {
            text: this.translateService.instant('GENERAL.ok'),
            handler: () => {}
          }]
    );
  }

  private async stillRunningAlert(provider: KycProvider): Promise<void> {
    await this.alertService.alertCreate(
        this.translateService.instant(`KYC.messages.${provider}_RUNNING.header`),
        this.translateService.instant(`KYC.messages.${provider}_RUNNING.sub-header`),
        this.translateService.instant(`KYC.messages.${provider}_RUNNING.message`),
        [
          {
            text: this.translateService.instant('GENERAL.ok'),
            handler: () => {}
          }]
    );
  }

  public async otc() {
    // this.displayOTC = false
    if(this.displayOTC){
      // console.log('otc clicked')
      const modal = await this._ModalController.create({
        component: OtcComponent,
        componentProps: {
          data: {
          }
        }
      })
      modal.onDidDismiss().then(async () => {

      })
      await modal.present();
    }
  }

  async showVerificationOptions() {
    var val = await this.kycService.isUserAllowedToUseChatMarketplace();

      // this._SecureStorageService.getValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, false).then((val) => {

    if(val) {
      var placeholder = environment.production ? '' : KycPins.veriff;
      var alerto = await this._AlertController.create({
        mode: 'ios',
        header: this.translateService.instant('KYC.KYCPIN.header'),
        message: this.translateService.instant('KYC.KYCPIN.message1'),
        inputs: [ { name: 'kycpin', placeholder: placeholder, type: 'tel'} ],
        buttons: [ {
          text: this.translateService.instant('SETTINGS.ok'),
          handler: async (data) => {
              (data.kycpin == KycPins.veriff) ?
              // await this.initiateOndato('ONDATO') :
              this.nav.navigateForward('/kyc-intermediary') :
                this._AlertController.create({
                  mode: 'ios',
                  message: this.translateService.instant('KYC.KYCPIN.message2'),
                  buttons: [ { text: this.translateService.instant('SETTINGS.ok'), role: 'cancel', handler: () => { } } ]
                }).then((al) => al.present());
            }
          }, { text: this.translateService.instant('SETTINGS.cancel'), role: 'cancel', handler: () => { } }
        ]
      })
        await alerto.present();

    } else {
      // this.selectOndato();
      this.nav.navigateForward('/kyc-intermediary');
    }


    // "SELECT_VENDOR": {
    //   "header": "Verify your Identity",
    //   "subheader": "Please select one of the following providers.",
    //   "message": ""
    // },
    // const alerto = await this._AlertController.create({
    //   header: this.translateService.instant('KYC.SELECT_VENDOR.header'),
    //   message: this.translateService.instant('KYC.SELECT_VENDOR.subheader'),
    //   buttons: [
    //     {
    //       text: 'Ondato',
    //       handler: () => {
            // await this.initiateOndato('ONDATO');
    //       }
    //     },
    //     {
    //       text: 'Verifeye',
    //       handler: () => {
    //         this.veriffIdentity(""); // TOdo: Ansik: change this in case options are provided to the user to select a Verification Provider
    //       }
    //     },
    //     {
    //       text: this.translateService.instant('BUTTON.CANCEL'),
    //       role: 'cancel',
    //       handler: () => {}
    //     }
    //   ]
    // });

    // await alerto.present();
  }

}
