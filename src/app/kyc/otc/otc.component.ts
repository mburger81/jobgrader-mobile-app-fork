import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ModalController, IonSlides, ToastController } from '@ionic/angular';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { SecureStorageKey } from '../../core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../../core/providers/secure-storage/secure-storage.service';
import { ApiProviderService } from '../../core/providers/api/api-provider.service';
import { AppStateService } from '../../core/providers/app-state/app-state.service';
import { take, takeUntil } from 'rxjs/operators';
import { ContactService } from '../../contact/shared/contact.service';
import { interval, Subject, Subscription } from 'rxjs';
import { LoaderProviderService } from '../../core/providers/loader/loader-provider.service';
import { CryptoProviderService } from '../../core/providers/crypto/crypto-provider.service';
import { Clipboard } from '@capacitor/clipboard';
import { AlertsProviderService } from '../../core/providers/alerts/alerts-provider.service';
import { UserProviderService } from '../../core/providers/user/user-provider.service';
import { UDIDNonce } from '../../core/providers/device/udid.enum';
import { BarcodeService } from '../../core/providers/barcode/barcode.service';
import { DatePipe } from '@angular/common';
import { SignProviderService } from '../../core/providers/sign/sign-provider.service';
import { ChatService } from '../../core/providers/chat/chat.service';
import { ChatMessage, MessageStructure, MessageTypes } from '../../core/providers/chat/chat-model';
import { Contact } from '../../contact/shared/contact.model';

@Component({
  selector: 'app-otc',
  templateUrl: './otc.component.html',
  styleUrls: ['./otc.component.scss'],
})
export class OtcComponent implements OnInit, OnDestroy {

  public didObject = {
    clearName: '',
    didExists: false,
    did: '',
    didDocument: {}
  }

  private destroy$ = new Subject()

  public timeout = 1;
  public qrCodeValue: string;

  public qrCodeIsHidden = false;

  public qrCodeCaptionGeneration: string;
  public qrCodeCaptionCountdown: string;
  public profilePictureSrc: string;
  private timerSubscription: Subscription;

  public checkBoxArray = [];
  public scannedQRData: any = {};

  private _IonSlides: IonSlides;
  @ViewChild('slides') set content(slides: IonSlides) {
    this._IonSlides = slides;
  }
  constructor(
    private _ModalController: ModalController,
    private _ToastController: ToastController,
    private _TranslateProviderService: TranslateProviderService,
    private _SecureStorageService: SecureStorageService,
    private _ApiProviderService: ApiProviderService,
    private _AppStateService: AppStateService,
    private _AlertsProviderService: AlertsProviderService,
    private _ContactService: ContactService,
    
    private _ChatService: ChatService,
    private _BarcodeService: BarcodeService,
    private _SignProviderService: SignProviderService,
    private _UserProviderService: UserProviderService,
    private _LoaderProviderService: LoaderProviderService,
    private _CryptoProviderService: CryptoProviderService
  ) {
  }

  ngOnInit() {
    // this.startQrTimer();
    this._SecureStorageService.getValue(SecureStorageKey.userName, false).then(userName => {
      this._ApiProviderService.getUserEns().then((ens) => {
        this.didObject.clearName = ens;
      }).catch((err) => {
        console.log(err);
        this.didObject.clearName = `${userName}@helixid_evan`
      })
    })

    this._UserProviderService.getUser().then(user => {
      this._UserProviderService.getUsername().then(displayHelixUsername => {
        this._SecureStorageService.getValue(SecureStorageKey.chatPublicKey, false).then(chatPublicKey => {
          this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false).then((chatUserData) => {
            this._SecureStorageService.getValue(SecureStorageKey.did, false).then(did => {
              if(did) {
                this.didObject.did = did;
                this.generateVP(chatUserData, user, displayHelixUsername, chatPublicKey, did).then(qrData => {
                  // console.log(qrData);
                  this.startQrTimer(qrData);
                })
                this._SecureStorageService.getValue(SecureStorageKey.didDocument, false).then(async didDocument => {
                  if(didDocument) {
                    this.didObject.didDocument = JSON.parse(await this._CryptoProviderService.decryptGenericData(didDocument))
                    this.didObject.didExists = true
                  }
                })
              } else {
                this.generateVP(chatUserData, user, displayHelixUsername, chatPublicKey).then(qrData => {
                  // console.log(qrData);
                  this.startQrTimer(qrData);
                })
              }
            })
          }).catch((e) => {})
        }).catch((e) => {})
      }).catch((e) => {})
    }).catch((e) => {})

  }

  private async startQrTimer(encryptedQRString: string): Promise<void> {

    const INTERVAL = 1000; // ms

    let countdown = 0;

    let validity: number;
    let timeout: number;
    let value: string;

    const data = await this._ContactService.getQRCode(encryptedQRString, new Date(), 'contact');
    // console.log(encryptedQRString);
    // console.log(data);
    // const data = await this.contactService.getQRCode(this.id, new Date());
    // console.log('---' + new Date().toTimeString());
    // console.log(`---value: ${data.encryptedContactIdAsString}, validity: ${data.validity}`);

    validity = data.validity;
    timeout = data.timeout;
    value = data.encryptedContactIdAsString;

    const url = `${value}`;
    this.qrCodeValue = url;

    var size = new TextEncoder().encode(url).length
    var kiloBytes = size / 1024;
    // console.log(`QR code size: ${kiloBytes} KB`);

    const timer = interval(INTERVAL);
    this.timerSubscription = timer.pipe(takeUntil(this.destroy$)).subscribe(() => {
      countdown++;

      // console.log(`countdown: ${countdown}, validity: ${validity}`);

      if (countdown === validity - timeout || countdown === validity - timeout + 1) {
        this.qrCodeIsHidden = true;
        this.qrCodeCaptionCountdown = this._TranslateProviderService.instant("QRSCAN.new-generation");
        return;
      }

      if (countdown === validity) {
        this._ContactService.getQRCode(encryptedQRString, new Date(), 'contact').then(data => {
          validity = data.validity;

          const url = `${data.encryptedContactIdAsString}`;
          this.qrCodeValue = url;

          var size = new TextEncoder().encode(url).length
          var kiloBytes = size / 1024;
          // console.log(`QR code size: ${kiloBytes} KB`);

          this.qrCodeIsHidden = false;
        });

        countdown = 0;
      }

      this.qrCodeCaptionCountdown = this._TranslateProviderService.instant("QRSCAN.expiry-notice.part1")+ `${validity - timeout - countdown}s` + this._TranslateProviderService.instant("QRSCAN.expiry-notice.part2");
    });
  }

  private stopQrTimer(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  ngOnDestroy() {
    this.stopQrTimer()
  }

  async close() {
    await this._ModalController.dismiss();
  }

  async navigateToDidPage() {
    // console.log('button clicked')
    if(this.didObject.didExists) {
      this._IonSlides.slideNext()
    } else {
      await this._LoaderProviderService.loaderCreate(this._TranslateProviderService.instant('CERTIFICATES.didBeingGenerated'), 30000)
      var deviceId = await this._SecureStorageService.getValue(SecureStorageKey.deviceInfo, false);
      if(deviceId) {
        var didDocument = await this._ApiProviderService.generateDID(deviceId)
          if(didDocument) {
            this.didObject.didDocument = didDocument
            await this._SecureStorageService.setValue(SecureStorageKey.didDocument, await this._CryptoProviderService.encryptGenericData(JSON.stringify(this.didObject.didDocument)))
            if(didDocument.id){
              this.didObject.did = didDocument.id
              await this._SecureStorageService.setValue(SecureStorageKey.did, this.didObject.did)
              await this.presentToast(this._TranslateProviderService.instant('CERTIFICATES.didGenerated'))
            }
            this.didObject.didExists = true
            await this._LoaderProviderService.loaderDismiss()
            this._ModalController.dismiss()
          } else {
            alert('Endpoint is not ready yet')
          }
      } else {
        alert('deviceId does not exist')
      }
    }
    await this._LoaderProviderService.loaderDismiss()
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 3000, position: 'top' });
    await toast.present();
  }

  async showDIDDocument() {
    // alert(JSON.stringify(this.didObject.didDocument))
    await this._AlertsProviderService.alertCreate(this._TranslateProviderService.instant('CERTIFICATES.didDoc'), '', JSON.stringify(this.didObject.didDocument),[this._TranslateProviderService.instant('GENERAL.ok')])
  }

  async copyClearname() {
    await Clipboard.write({ string: this.didObject.clearName });
    await this.presentToast('clearName' + this._TranslateProviderService.instant('SETTINGSACCOUNT.copied'))
  }

  async copyDID() {
    await Clipboard.write({ string: this.didObject.did });
    await this.presentToast('DID' + this._TranslateProviderService.instant('SETTINGSACCOUNT.copied'))
  }

  private generateVP(chatUserData: any, user: any, displayHelixUsername: string, chatPublicKey: string, did?: string): Promise<string> {
    return new Promise((resolve) => {
      // this._UserProviderService.getUser().then((user) => {
        var vpRawData = {}
        if(user.genderStatus == 0 && user.gender !== '') { Object.assign(vpRawData, { gender: user.gender }) }
        if(user.maritalstatusStatus == 0 && user.maritalstatus !== ''){ Object.assign(vpRawData, { maritalstatus: user.maritalstatus })}
        if(user.firstnameStatus == 0 && user.firstname !== ''){ Object.assign(vpRawData, { firstname: user.firstname })}
        if(user.lastnameStatus == 0 && user.lastname !== ''){ Object.assign(vpRawData, { lastname: user.lastname })}
        if(user.dateofbirthStatus == 0 && user.dateofbirth !== null){ Object.assign(vpRawData, { dateofbirth: user.dateofbirth })}
        if(user.countryofbirthStatus == 0 && user.countryofbirth !== ''){ Object.assign(vpRawData, { countryofbirth: user.countryofbirth })}
        if(user.citizenshipStatus == 0 && user.citizenship !== ''){ Object.assign(vpRawData, { citizenship: user.citizenship })}
        if(user.streetStatus == 0 && user.street !== ''){ Object.assign(vpRawData, { street: user.street })}
        if(user.cityStatus == 0 && user.city !== ''){ Object.assign(vpRawData, { city: user.city })}
        if(user.zipStatus == 0 && user.zip !== ''){ Object.assign(vpRawData, { zip: user.zip })}
        if(user.stateStatus == 0 && user.state !== ''){ Object.assign(vpRawData, { state: user.state })}
        if(user.cityofbirthStatus == 0 && user.cityofbirth !== ''){ Object.assign(vpRawData, { cityofbirth: user.cityofbirth })}
        if(user.countryStatus == 0 && user.country !== ''){ Object.assign(vpRawData, { country: user.country })}
        if(user.maidennameStatus == 0 && user.maidenname !== ''){ Object.assign(vpRawData, { maidenname: user.maidenname })}
        if(user.middlenameStatus == 0 && user.middlename !== ''){ Object.assign(vpRawData, { middlename: user.middlename })}
        if(user.identificationdocumentnumberStatus == 0 && user.identificationdocumentnumber !== ''){ Object.assign(vpRawData, { identificationdocumentnumber: user.identificationdocumentnumber })}
        if(user.identificationissuecountryStatus == 0 && user.identificationissuecountry !== ''){ Object.assign(vpRawData, { identificationissuecountry: user.identificationissuecountry })}
        if(user.identificationexpirydateStatus == 0 && user.identificationexpirydate !== null){ Object.assign(vpRawData, { identificationexpirydate: user.identificationexpirydate })}
        if(user.identificationissuedateStatus == 0 && user.identificationissuedate !== null){ Object.assign(vpRawData, { identificationissuedate: user.identificationissuedate })}
        if(user.driverlicencedocumentnumberStatus == 0 && user.driverlicencedocumentnumber !== ''){ Object.assign(vpRawData, { driverlicencedocumentnumber: user.driverlicencedocumentnumber })}
        if(user.driverlicencecountryStatus == 0 && user.driverlicencecountry !== ''){ Object.assign(vpRawData, { driverlicencecountry: user.driverlicencecountry })}
        if(user.driverlicenceexpirydateStatus == 0 && user.driverlicenceexpirydate !== null){ Object.assign(vpRawData, { driverlicenceexpirydate: user.driverlicenceexpirydate })}
        if(user.driverlicenceissuedateStatus == 0 && user.driverlicenceissuedate !== null){ Object.assign(vpRawData, { driverlicenceissuedate: user.driverlicenceissuedate })}
        if(user.passportnumberStatus == 0 && user.passportnumber !== ''){ Object.assign(vpRawData, { passportnumber: user.passportnumber })}
        if(user.passportissuecountryStatus == 0 && user.passportissuecountry !== ''){ Object.assign(vpRawData, { passportissuecountry: user.passportissuecountry })}
        if(user.passportexpirydateStatus == 0 && user.passportexpirydate !== null){ Object.assign(vpRawData, { passportexpirydate: user.passportexpirydate })}
        if(user.passportissuedateStatus == 0 && user.passportissuedate !== null){ Object.assign(vpRawData, { passportissuedate: user.passportissuedate })}
        if(user.residencepermitnumberStatus == 0 && user.residencepermitnumber !== ''){ Object.assign(vpRawData, { residencepermitnumber: user.residencepermitnumber })}
        if(user.residencepermitissuecountryStatus == 0 && user.residencepermitissuecountry !== ''){ Object.assign(vpRawData, { residencepermitissuecountry: user.residencepermitissuecountry })}
        if(user.residencepermitexpirydateStatus == 0 && user.residencepermitexpirydate !== null){ Object.assign(vpRawData, { residencepermitexpirydate: user.residencepermitexpirydate })}
        if(user.residencepermitissuedateStatus == 0 && user.residencepermitissuedate !== null){ Object.assign(vpRawData, { residencepermitissuedate: user.residencepermitissuedate })}
        if(!!did) { Object.assign(vpRawData, { did }) }
        if(!!chatUserData) { Object.assign(vpRawData, { xmppChat: JSON.parse(chatUserData).username }) }
        // Object.assign(vpRawData, { chatPublicKey: chatPublicKey })
        Object.assign(vpRawData, { displayHelixUsername: displayHelixUsername })
        // Object.assign(vpRawData, { contactId: user.id })
        // console.log(vpRawData);
        this._CryptoProviderService.symmetricEncrypt(JSON.stringify(vpRawData), UDIDNonce.helix as any).then((encryptedJSON) => {
          resolve(encryptedJSON);
        }).catch((err) => {
          console.log(err);
          resolve(JSON.stringify(vpRawData));
        })
      // })
    })
  }

  public async openScanner() {
    var res = await this._BarcodeService.scanUserDataQR();
    if(res) {
      // console.log(res);
      if(Object.keys(res).length == 0){ return alert('No credential extracted for verification!')}
      this.scannedQRData = res;
      if(!!this.scannedQRData.displayHelixUsername) {
        // if(!this._ChatService.isFriend(this.scannedQRData.xmppChat)){
          this._ChatService.tryToAddUserByNameSearch(this.scannedQRData.displayHelixUsername)
          // this._ChatService.sendFriendRequest(this.scannedQRData.xmppChat, <Contact>{
            // id: this.scannedQRData.contactId,
            // username: this.scannedQRData.xmppChat,
            // chatPublicKey: this.scannedQRData.chatPublicKey,
            // name: this.scannedQRData.displayHelixUsername,
            // photo: null,
            // email: null
          // }).then(() => {
            console.log('Friend request sent')
          // })
        // }
      }

      this.checkBoxArray = [];
      Object.keys(res).forEach(r => {
      if(r !== 'did' && r !== 'xmppChat' && r !== 'chatPublicKey' && r !== 'displayHelixUsername' && r !== 'contactId'){
        if(['dateofbirth', 'identificationissuedate', 'identificationexpirydate', 'driverlicenceexpirydate',
        'driverlicenceissuedate', 'passportexpirydate', 'passportissuedate', 'residencepermitexpirydate', 'residencepermitissuedate'].includes(r)){
          this.checkBoxArray.push({
            parameter: r,
            key: this._TranslateProviderService.instant(`OTC.CHECKBOX.${r}`),
            value: new DatePipe('en-US').transform(new Date(res[r]),'dd.MM.yyyy'),
            isChecked: false
            })
          }
        else if([ 'countryofbirth', 'citizenship', 'country', 'identificationissuecountry', 'driverlicencecountry', 'passportissuecountry', 'residencepermitissuecountry' ].includes(r)){
          this.checkBoxArray.push({
            parameter: r,
            key: this._TranslateProviderService.instant(`OTC.CHECKBOX.${r}`),
            value: this.processCountryDisplay(res[r]),
            isChecked: false
            })
        }
        else {
          this.checkBoxArray.push({
            parameter: r,
            key: this._TranslateProviderService.instant(`OTC.CHECKBOX.${r}`),
            value: res[r],
            isChecked: false
            })
          }
        }
      });
      this._IonSlides.slideNext();
    }
    // else {
    //   this.checkBoxArray = [{
    //     key: this._TranslateProviderService.instant(`OTC.CHECKBOX.citizenship`),
    //     value: this.processCountryDisplay('IND'),
    //     isChecked: false
    //     },{
    //       key: this._TranslateProviderService.instant(`OTC.CHECKBOX.country`),
    //       value: this.processCountryDisplay('DEU'),
    //       isChecked: false
    //     }];
    //   this._IonSlides.slideNext();
    // }
  }

  public verifyMessage() {
    // console.log(this.checkBoxArray);
    var checkedFields = this.checkBoxArray.filter(k => k.isChecked == true);
    // console.log(checkedFields);
    checkedFields = Array.from(checkedFields, k => k.parameter);
    // console.log(checkedFields);
    var m = {}
    var n = {}
    checkedFields.forEach(k => Object.assign(n, { [k]: 'Verified'}))
    // console.log(n);
    Object.assign(m, { verifier: this.didObject.did })
    Object.assign(m, { schemaDid: "did:evan:zkp:0x470cd915c86bf89f80c92c1b9215a90c3d5e31f78f5760c8b8c433b9418b24ac" })
    Object.assign(m, { credentialSubjectRaw: n })
    // console.log(m);
    var messageObject: MessageStructure = {
      type: MessageTypes.verifiableCredential,
      value: JSON.stringify(m)
    }
    // console.log(messageObject);
    // console.log(this._ChatService.stripDNS(this._ChatService.myXmppUser.jid.toString()));
    // console.log(`Chat going to be sent to ${this.scannedQRData.xmppChat.toString()}`);

    // console.log(this._ChatService.stripDNS(this._ChatService.myXmppUser.jid.toString()) == this._ChatService.stripDNS(this.scannedQRData.xmppChat.toString()));

    if(this._ChatService.stripDNS(this._ChatService.myXmppUser.jid.toString()) == this._ChatService.stripDNS(this.scannedQRData.xmppChat.toString())) {
      return this.presentToast(this._TranslateProviderService.instant('CERTIFICATES.TOAST.noSelfie'));
    }

    this._ChatService.myXmppUser.sendMessage(this.scannedQRData.xmppChat.toString(), JSON.stringify(JSON.stringify(messageObject))).then(() => {
      this.presentToast(`Certificate sent to ${this.scannedQRData.xmppChat.toString()}`);
      this._ModalController.dismiss();
    }).catch((e) => {
      this.presentToast(`Could not send certificate to ${this.scannedQRData.xmppChat}`);
      this._ModalController.dismiss();
    })
  }

  processCountryDisplay(countryCode: string) {
    let temp_country = this._SignProviderService.countries.find(d => d.code3 === countryCode);
    return (!!temp_country && !!countryCode) ? this._TranslateProviderService.instant(temp_country["countryTranslateKey"]) : countryCode;
  }

}
