import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KycIntermediaryPageRoutingModule } from './kyc-intermediary-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { KycIntermediaryPage } from './kyc-intermediary.page';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobHeaderModule,
    SharedModule,
    TranslateModule.forChild(),
    KycIntermediaryPageRoutingModule
  ],
  providers: [
    SafariViewController
  ],
  declarations: [KycIntermediaryPage]
})
export class KycIntermediaryPageModule {}
