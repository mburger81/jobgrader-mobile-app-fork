import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KycIntermediaryPage } from './kyc-intermediary.page';

describe('KycIntermediaryPage', () => {
  let component: KycIntermediaryPage;
  let fixture: ComponentFixture<KycIntermediaryPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(KycIntermediaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
