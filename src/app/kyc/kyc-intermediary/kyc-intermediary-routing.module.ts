import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KycIntermediaryPage } from './kyc-intermediary.page';

const routes: Routes = [
  {
    path: '',
    component: KycIntermediaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KycIntermediaryPageRoutingModule {}
