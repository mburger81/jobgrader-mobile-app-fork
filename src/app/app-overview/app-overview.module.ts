import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AppOverviewPage } from './app-overview.page';

import { SharedModule } from '../shared/shared.module';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    component: AppOverviewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [AppOverviewPage]
})
export class AppOverviewPageModule {}
