export interface Contact {
  id: string;
  username: string;
  chatPublicKey: string;
  name: string;
  photo: string;
  email: string;
  merchant: boolean;
}
