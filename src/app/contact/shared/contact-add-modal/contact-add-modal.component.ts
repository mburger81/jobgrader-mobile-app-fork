import { Router, NavigationExtras } from '@angular/router';
import { PopoverController, NavParams, NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/core/providers/chat/chat.service';
import { Contact } from '../contact.model';
import { DomSanitizer } from '@angular/platform-browser';
import { EventsService } from 'src/app/core/providers/events/events.service';
import { KeyExchangeService } from 'src/app/core/providers/chat/key-exchange.service';

@Component({
  selector: 'app-contact-add-modal',
  templateUrl: './contact-add-modal.component.html',
  styleUrls: ['./contact-add-modal.component.scss']
})
export class ContactAddModalComponent {
  public id: string;
  public displayHelixUsername: string;
  public chatUsername: string;
  public chatPublicKey: string;
  public image: string;
  public isLoading: boolean = false;

  constructor(
    private nav: NavController,
    private navParams: NavParams,
    public _ChatService: ChatService,
    public _DomSanitizer: DomSanitizer,
    public _EventsService: EventsService,
    public _PopoverController: PopoverController,
    private _KeyExchangeService: KeyExchangeService,
    private router: Router) {

    this.isLoading = false;
    this.id = this.navParams.get('id');
    this.displayHelixUsername = this.navParams.get('helixUsername');
    this.chatUsername = this.navParams.get('chatUsername');
    this.image = this.navParams.get('displayImage');
    this.chatPublicKey = this.navParams.get('chatPublicKey');
  }


  cancel() {
    this._PopoverController.dismiss();
  }
  
  add() {
    this.isLoading = true;

    var scannedContactInfo: Contact = <Contact> {
      username: this.chatUsername,
      chatPublicKey: this.chatPublicKey,
      name: this.displayHelixUsername,
      photo: this.image
    };

    // console.log(`Generating new keypairs for this user: ${this.chatUsername}`)

    this._KeyExchangeService.generateNewThreadSpecificChatKeyPair(this.chatUsername, null, this._ChatService.threadChatKeyPairs).then(cc => {
      this._KeyExchangeService.updateThreadChatKeyPairs(cc, this._ChatService.threadChatKeyPairs).then((c) => {
        this._ChatService.threadChatKeyPairs = c;
        // console.log(`Keypairs for ${this.chatUsername}: ${JSON.stringify(cc)}`)
        // console.log(`Keypairs for ${this.chatUsername}: ${JSON.stringify(this._ChatService.threadChatKeyPairs)}`)
        if (this._ChatService.isConnected()) {
          this._ChatService.sendFriendRequest(this.chatUsername, scannedContactInfo);
        }
        else {
          this._ChatService.connect(false).then(isFullyLoaded => {
            if (isFullyLoaded) { 
              this._ChatService.sendFriendRequest(this.chatUsername, scannedContactInfo);
            }
          });
        }
      })
    });
  }
}
