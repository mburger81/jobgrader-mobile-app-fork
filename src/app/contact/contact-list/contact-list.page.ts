import { ApiProviderService } from '../../core/providers/api/api-provider.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActionSheetController, ToastController, IonContent, NavController, Platform, ModalController } from '@ionic/angular';
import { AppStateService } from '../../core/providers/app-state/app-state.service';
import { BarcodeService } from '../../core/providers/barcode/barcode.service';
import { SecureStorageService } from '../../core/providers/secure-storage/secure-storage.service';
import { ChatService } from 'src/app/core/providers/chat/chat.service';
import { ChatThread, ChatThreadRequest } from 'src/app/core/providers/chat/chat-model';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { EventsService } from 'src/app/core/providers/events/events.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { Keyboard, KeyboardResize } from '@capacitor/keyboard';
import { SQLStorageService } from 'src/app/core/providers/sql/sqlStorage.service';
import { KycService } from 'src/app/kyc/services/kyc.service';
import { KeyExchangeService } from 'src/app/core/providers/chat/key-exchange.service';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';
import { ContactSelfComponent } from './contact-self/contact-self.component';
import { UserPhotoServiceAkita } from '../../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-tab-contact',
  templateUrl: './contact-list.page.html',
  styleUrls: ['./contact-list.page.scss'],
})
export class ContactListPage implements OnInit {


  @ViewChild(IonContent) content: IonContent;
  public bigTitleGuid: string;
  public bigTitleEl: any;
  public showPageTitle: boolean;
  public pageTitle: string;
  public isInnovator = false;
  public avatarImage = '../../assets/job-user.svg';
  public emptyUserImage = '../../assets/job-user.svg';
  private ngOnInitExecuted = false;
  public timeout: any;
  public currentSection: string = "contacts";
  public spinnerColor: string = "dark";
  // private initDate = +new Date();
  // private timestampObserver = interval(1000);
  // private timestampSubscription = this.timestampObserver.subscribe(() => this.changeTimestamp());

  constructor(
    private nav: NavController,
    public _AppStateService: AppStateService,
    private apiProviderService: ApiProviderService,
    private _SecureStorage: SecureStorageService,
    public _ChatService: ChatService,
    private _ToastController: ToastController,
    private _ActionSheetController: ActionSheetController,
    public _DomSanitizer: DomSanitizer,
    public _TranslateProviderService: TranslateProviderService,
    private _LoaderProviderService: LoaderProviderService,
    public _EventsService: EventsService,
    public _Platform: Platform,
    private _KycService: KycService,
    public _SQLStorageService: SQLStorageService,
    private _KeyExchangeService: KeyExchangeService,
    private theme: ThemeSwitcherService,
    public _BarcodeService: BarcodeService,
    private _ModalController: ModalController,
    public _NetworkService: NetworkService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
  }

  getBigTitleEl() {
    if (!this.bigTitleEl) { this.bigTitleEl = document.getElementById('customBigTitle_' + this.bigTitleGuid); }
    return this.bigTitleEl;
  }
  scrolling(event: any) {
    this.showPageTitle = (!this.isFullyVisible(this.getBigTitleEl()));
  }

  isFullyVisible(el: any) {
    try {
      var rect = el.getBoundingClientRect();
      var elemTop = rect.top;
      var elemBottom = rect.bottom;
      var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
      return isVisible;
    }
    catch (e) {
      return true;
    }
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewWillEnter() {
    // force normal resize again in case stuck on wrong size with missing keyboard.
    if (this._Platform.is('ios')) {
      Keyboard.setResizeMode({ mode: KeyboardResize.Native });
    }

    this.bigTitleGuid = this._SQLStorageService.generateUniqueId();

    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }

    this._ChatService.chatThreadToView = null;
    this._ChatService.receiveMessageCallback = null;
  }

  // changeTimestamp() {
  //   console.log(this.initDate);
  //   console.log(this._ChatService.isFullyLoaded);
  //   console.log(this._ChatService.isOnline);
  //   if(+new Date - this.initDate > 10000) {
  //     if(!this._ChatService.isFullyLoaded) {
  //       this._ChatService.isFullyLoaded = true;
  //       this._ChatService.isOnline = false;
  //       this.timestampSubscription.unsubscribe();
  //     }
  //   }
  // }

  private async componentInit() {
    this._ChatService.isOnline = this._NetworkService.checkConnection();
    this.spinnerColor = (this.theme.getCurrentTheme() == "dark") ? "light" : "dark";
    const photo = this.userPhotoServiceAkita.getPhoto();
    if(photo) {
      this.avatarImage = photo
    }

    if(!this._ChatService.isOnline){
      this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }
  }

  showSelfQRCode() {
    console.log("showSelfQRCode()");
    this._ModalController.create({
      component: ContactSelfComponent
    }).then(modal => {
      modal.onDidDismiss().then(data => {
        console.log(data);
      })
      modal.present();
    })
  }

  presentToast(message: string) {
    return this._ToastController.create({
      message: message,
      position: 'top',
      duration: 2000
    }).then(toast => toast.present())
  }

  /** Callback for opening the scanner */
  public openScanner(): void {
    this._KycService.isUserAllowedToUseChatMarketplace().then(s => {
      if(!s) {
        this.presentToast(this._TranslateProviderService.instant('CHAT.userNotAllowed')).then(() => {})
      } else {
        this._BarcodeService.scanContact();
      }
    })
  }

  public addContactViaUsername(): void {
    this._KycService.isUserAllowedToUseChatMarketplace().then(s => {
      if(!s) {
        this.presentToast(this._TranslateProviderService.instant('CHAT.userNotAllowed')).then(() => {})
      } else {
        this._ChatService.showSearchUserByUsername()
      }
    })
  }

  ionViewWillLeave() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.bigTitleGuid = null;
    this.bigTitleEl = null;
    this.showPageTitle = null;
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  viewDetail(chatThread: ChatThread) {
    this._ChatService.chatThreadToView = chatThread;
    // this._SecureStorage.getValue(SecureStorageKey.chatKeyPairs, false).then(e => this._ChatService.threadChatKeyPairs = !!e ? JSON.parse(e) : {})
    // console.log(this._ChatService.threadChatKeyPairs);
    this.nav.navigateForward('/contact-detail');
  }

  chat(chatThread: ChatThread) {
    this._ChatService.chatThreadToView = chatThread;
    // this._SecureStorage.getValue(SecureStorageKey.chatKeyPairs, false).then(e => this._ChatService.threadChatKeyPairs = !!e ? JSON.parse(e) : {})
    // console.log(this._ChatService.threadChatKeyPairs);
    this._ChatService.navigateToChat();
  }

  reconnect() {
    this._ChatService.connect(true);
  }

  delete(chatThreadRequest: ChatThreadRequest) {
    chatThreadRequest.isProcessing = true;
    this._ChatService.deleteFriend(chatThreadRequest.userWrapper.chatUserName);
  }

  accept(chatThreadRequest: ChatThreadRequest) {
    chatThreadRequest.isProcessing = true;
    this._ChatService.acceptFriendRequest(chatThreadRequest).then(newThread => {
      if (!newThread && chatThreadRequest) {
        chatThreadRequest.isProcessing = false;
      }
      if (newThread) {
        this._KeyExchangeService.initiateKeyExchangeProcess(newThread, this._ChatService.myXmppUser.jid, this._ChatService.threadChatKeyPairs).then((newMessage) => {
          if (this._ChatService.isConnected()) {
            this._ChatService.sendMessage(newMessage).then(() => {
                this._LoaderProviderService.loaderDismiss().then(() => {})
            })
          }
          else {
            this._ChatService.connect(false).then(() => {
              this._ChatService.sendMessage(newMessage).then(() => {
                  this._LoaderProviderService.loaderDismiss().then(() => {
                  })
              })
            })
          }
        })
      }
    });
  }

  showContactAdditionOptions() {
    if(this._AppStateService.isAuthorized) {
      this._ActionSheetController.create({
        mode: 'md',
        header: this._TranslateProviderService.instant('CHAT.contact-addition-options'),
        buttons: [
          { text: this._TranslateProviderService.instant('CHAT.scan-qr-code'), icon: 'qr-code', handler: () => { this.openScanner() } },
          { text: this._TranslateProviderService.instant('CHAT.add-did-contact'), icon: 'at', handler: () => { this.addContactViaUsername() } },
          { text: this._TranslateProviderService.instant('GENERAL.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('Cancel') } }
        ]
      }).then(sheet => {
        sheet.present();
      });
    } else {
      this.apiProviderService.noUserLoggedInAlert();
    }
  }

  goToSignUp() {
    this.nav.navigateRoot('/sign-up/step-1');
  }

  goToLogin() {
    this.nav.navigateRoot('/login?from=browsemode');
  }

  goBack() {
    this.nav.navigateBack('/dashboard/personal-details');
  }

}
