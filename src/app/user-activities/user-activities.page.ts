import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { UserActivitiesService, UserActivity } from '../core/providers/user-activities/user-activities.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-user-activities',
  templateUrl: './user-activities.page.html',
  styleUrls: ['./user-activities.page.scss'],
})
export class UserActivitiesPage implements OnInit {

  public activities: Array<UserActivity> = [];
  userImage;

  constructor(
    private nav: NavController,
    private _UserActivity: UserActivitiesService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) { }

  async ngOnInit() {
    this.userImage = this.userPhotoServiceAkita.getPhoto();
    this.activities = await this._UserActivity.getActivities();
  }

  goBackToSettings() {
    this.nav.navigateBack('/dashboard/tab-settings');
  }

}
