import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, ToastController } from '@ionic/angular';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { Router } from '@angular/router';
import { DataSharingService, Checks, StoredRequest } from '../core/providers/data-sharing/data-sharing.service';
import { ethers, Wallet } from 'ethers';
import { environment } from 'src/environments/environment';
import { NftService } from '../core/providers/nft/nft.service';
import { CryptoCurrency } from '../core/providers/wallet-connect/constants';
import { GasService } from '../core/providers/gas/gas.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-data-sharing-signature',
  templateUrl: './data-sharing-signature.page.html',
  styleUrls: ['./data-sharing-signature.page.scss'],
})
export class DataSharingSignaturePage implements OnInit {

  public infos = [];
  public data: StoredRequest;
  public description = '';
  public KYC_VC_DESC = '';

  public userPhoto = '../../assets/job-user.svg';
  public placeholderImage = '../../assets/job-user.svg';

  constructor(
    private nav: NavController,
    private router: Router,
    private translate: TranslateProviderService,
    private secureStorage: SecureStorageService,
    private appStateService: AppStateService,
    private themeSwitcher: ThemeSwitcherService,
    private dataSharing: DataSharingService,
    private toastController: ToastController,
    private _NftService: NftService,
    private _Gas: GasService,
    private _ModalController: ModalController,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) { }

  async ngOnInit() {
    let checker = this.router.parseUrl(this.router.url).queryParams;
    this.data = JSON.parse(checker.data);
    console.log(this.data);
    this.placeholderImage = this.appStateService.isAuthorized ? '../../assets/job-user.svg' : '../../assets/job-user.svg';
    console.log(this.placeholderImage);
    var photo = this.userPhotoServiceAkita.getPhoto();
    this.userPhoto = !!photo ? photo : this.placeholderImage;
    var theme = this.themeSwitcher.getCurrentTheme();
    this.description = `${this.translate.instant('REQUESTS.status0.message.part1')} ${this.data.username} ${this.translate.instant('REQUESTS.status0.message.part2')}:`;
    var checks = JSON.parse(this.data.checks);
    console.log(checks);
    var checksHeaders = Array.from(checks, c => Object.keys(c)[0]);
    console.log(checksHeaders);

    if(this.data.fields.includes('firstname') || this.data.fields.includes('lastname') || this.data.fields.includes('title')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/name.svg`,
        text: this.translate.instant('VERIFICATION.name')
      })
    }
    if(this.data.fields.includes('phone')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/phone.svg`,
        text: this.translate.instant('VERIFICATION.phone')
      })
    }
    if(checksHeaders.includes(Checks.agecheck) || checksHeaders.includes(Checks.minagecheck) || this.data.fields.includes('dateofbirth')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/age.svg`,
        text: this.translate.instant('VERIFICATION.age')
      })
    }
    if(
      this.data.fields.includes('identificationdocumentnumber') || this.data.fields.includes('identificationissuecountry') || this.data.fields.includes('identificationissuedate') || this.data.fields.includes('identificationexpirydate') ||
      this.data.fields.includes('residencepermitnumber') || this.data.fields.includes('residencepermitissuecountry') || this.data.fields.includes('residencepermitissuedate') || this.data.fields.includes('residencepermitexpirydate') ||
      this.data.fields.includes('passportnumber') || this.data.fields.includes('passportissuecountry') || this.data.fields.includes('passportissuedate') || this.data.fields.includes('passportexpirydate')
    ) {
      this.infos.push({
        image: `../../assets/signature/${theme}/idCard.svg`,
        text: this.translate.instant('VERIFICATION.idCard')
      })
    }
    if(this.data.fields.includes('driverlicencedocumentnumber') || this.data.fields.includes('driverlicencecountry') || this.data.fields.includes('driverlicenceissuedate') || this.data.fields.includes('driverlicenceexpirydate')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/license.svg`,
        text: this.translate.instant('VERIFICATION.license')
      })
    }

    if(this.data.fields.includes('email')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/mail-outline.svg`,
        text: this.translate.instant('VERIFICATION.mail')
      })
    }
    if(this.data.fields.includes('photo')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/image-outline.svg`,
        text: this.translate.instant('VERIFICATION.photo')
      })
    }
    if(this.data.fields.includes('maritalstatus')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/people-circle-outline.svg`,
        text: this.translate.instant('VERIFICATION.marital')
      })
    }
    if(this.data.fields.includes('cityofbirth') || this.data.fields.includes('countryofbirth') || this.data.fields.includes('citizenship')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/egg-outline.svg`,
        text: this.translate.instant('VERIFICATION.citizenship')
      })
    }
    if(this.data.fields.includes('street') || this.data.fields.includes('city') || this.data.fields.includes('zip') || this.data.fields.includes('state') || this.data.fields.includes('country')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/home-outline.svg`,
        text: this.translate.instant('VERIFICATION.residency')
      })
    }
    if(this.data.fields.includes('callname')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/person-circle-outline.svg`,
        text: this.translate.instant('VERIFICATION.callname')
      })
    }
    if(this.data.fields.includes('gender')) {
      this.infos.push({
        image: `../../assets/signature/${theme}/person-outline.svg`,
        text: this.translate.instant('VERIFICATION.gender')
      })
    }
    if(checksHeaders.includes(Checks.KYC_AML)) {
      var f = checks.find(g => Object.keys(g)[0] == Checks.KYC_AML);
      console.log(f);
      var lang = await this.translate.getLangFromStorage();
      this.KYC_VC_DESC = (lang == 'de') ? `(Stellen Sie sicher, dass die Überweisung von ${f[Checks.KYC_AML].amount} ${f[Checks.KYC_AML].currency}) von Ihrem Konto den erforderlichen KYC-AML-Standards entspricht.` : `(Ensure that the transfer of ${f[Checks.KYC_AML].amount} ${f[Checks.KYC_AML].currency}) from your account complies with the required KYC-AML standards.`;
      console.log(this.KYC_VC_DESC);
      this.description = `${this.translate.instant('REQUESTS.status0.message.part1')} ${this.data.username} ${this.translate.instant('REQUESTS.status0.message.part2')} (${this.KYC_VC_DESC}): `;
      console.log(this.description);
      this.infos.push({
        image: `../../assets/signature/${theme}/person-outline.svg`,
        text: 'KYC VC'
      })
    }

    console.log(this.infos);

  }

  async confirm() {
    await this.dataSharing.confirm(this.data);

    var checks = JSON.parse(this.data.checks);
    var checksHeaders = Array.from(checks, c => Object.keys(c)[0]);

    this.nav.navigateBack('/dashboard/requests');

    if(checksHeaders.includes(Checks.KYC_AML)) {
      var paymentRequestsSentString = await this.secureStorage.getValue(SecureStorageKey.cryptoPaymentRequestsSent, false);
      var paymentRequestsSent = !!paymentRequestsSentString ? JSON.parse(paymentRequestsSentString) : [];
      var checkObject = checks.find(g => Object.keys(g)[0] == Checks.KYC_AML);
      var paymentObject = paymentRequestsSent.find(f => checkObject[Checks.KYC_AML].paymentRequestId == f.pendingProcesses[0]);
      var paymentObjectIndex = paymentRequestsSent.findIndex(f => checkObject[Checks.KYC_AML].paymentRequestId == f.pendingProcesses[0]);
      console.log(paymentObject);
      if(paymentObject) {
        var lang = await this.translate.getLangFromStorage();
        var toastText = (lang == 'de') ? `Senden von ${paymentObject.amount} ${paymentObject.currency} an ${paymentObject.userPublicKey}` : `Sending ${paymentObject.amount} ${paymentObject.currency} to ${paymentObject.userPublicKey}`;
        this.presentToast(toastText);

        try {

          var userMnemonic = await this.secureStorage.getValue(SecureStorageKey.web3WalletMnemonic, false);
          var wallet = ethers.Wallet.fromMnemonic(userMnemonic);

          var to = paymentObject.userPublicKey.toLowerCase();
          var from = wallet.address.toLowerCase();

          switch(paymentObject.currency) {
            case CryptoCurrency.ETH: var gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.ETH); break;
            case CryptoCurrency.MATIC: gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.MATIC); break;
            case CryptoCurrency.EVE: gasPrice = await this._Gas.getGasPriceEstimationEVE(); break;
            case CryptoCurrency.GNOSIS: gasPrice = await this._Gas.getGasPriceEstimationXDAI(); break;
            default: gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.ETH); break;
          }

          var tx = await this._NftService.sendEtherJSTransaction(wallet, from, to, paymentObject.amount, paymentObject.currency, Number(ethers.utils.formatEther(ethers.utils.parseUnits(gasPrice.fast, gasPrice.unit)._hex)).toFixed(10) );

          if(tx) {
            var toastText2 = (lang == 'de') ? `${paymentObject.amount} ${paymentObject.currency} an ${to} wurde gesendet. Der Transaktionsbeleg wurde gespeichert.` : `${paymentObject.amount} ${paymentObject.currency} to ${to} has been sent. The transaction receipt has been saved.`;
            this.presentToast(toastText2);

            paymentRequestsSent[paymentObjectIndex].status = 1;

            await this.secureStorage.setValue(SecureStorageKey.cryptoPaymentRequestsSent, JSON.stringify(paymentRequestsSent));
          }

        } catch(e) {
          alert(JSON.stringify(e));
        }

      }
    }


  }

  async deny() {
    await this.dataSharing.deny(this.data);
    this.nav.navigateBack('/dashboard/requests');
  }

  async cancel() {
    await this.dataSharing.cancel(this.data);
    this.nav.navigateBack('/dashboard/requests');
  }

  presentToast(message: string) {
    this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top'
    })
  }

}
