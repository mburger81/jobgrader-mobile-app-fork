import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, Platform, ToastController } from '@ionic/angular';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { UtilityService } from 'src/app/core/providers/utillity/utility.service';
import { BiometricService } from 'src/app/core/providers/biometric/biometric.service';
import { take, switchMap } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { ethers } from 'ethers';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { SettingsService, Settings } from 'src/app/core/providers/settings/settings.service';
import { Clipboard } from '@capacitor/clipboard';
import { UserProviderService } from 'src/app/core/providers/user/user-provider.service';

@Component({
  selector: 'app-recovery-phrase-helper',
  templateUrl: './recovery-phrase-helper.component.html',
  styleUrls: ['./recovery-phrase-helper.component.scss'],
})
export class RecoveryPhraseHelperComponent implements OnInit {
  @Input() data: any;

  constructor(
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService,
    private _ToastController: ToastController,
    
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  onPress(event: any, mnemonic: any) {
    Clipboard.write({ string: mnemonic.toString() }).then(() => {
      this.presentToast(this._Translate.instant('SETTINGSACCOUNT.copied'))
    })
  }

  copy() {
    Clipboard.write({ string: this.data.toString() }).then(() => {
      this.presentToast(this._Translate.instant('SETTINGSACCOUNT.copied'))
    })
  }

  presentToast(message: string) {
    this._ToastController.create({
      position: 'top',
      duration: 2000,
      message
    }).then(toast => toast.present())
  }

  close() {
    this._ModalController.dismiss();
  }

}
