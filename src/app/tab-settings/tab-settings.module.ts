import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { SharedModule } from '../shared/shared.module';
import { TabSettingsPage } from './tab-settings.page';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { RecoveryPhraseHelperModule } from './recovery-phrase-helper/recovery-phrase-helper.module';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthenticationGuard],
    component: TabSettingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobHeaderModule,
    RecoveryPhraseHelperModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [SafariViewController, Chooser],
  declarations: [TabSettingsPage]
})
export class TabSettingsPageModule {}
