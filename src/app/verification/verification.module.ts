import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { VerificationPage } from './verification.page';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
import { VerifyDeeplinkProviderResolver } from '../core/resolvers/verify-deeplink/verify-deeplink-provider.resolver';
import {
  VerificationDeeplinkAvailabilityProviderGuard
} from '../core/guards/verification-deeplink-availability/verification-deeplink-availability-provider.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [
      AuthenticationGuard,
      VerificationDeeplinkAvailabilityProviderGuard
    ],
    component: VerificationPage,
    resolve: {
      user: UserProviderResolver,
      verifiedDeeplinkPayload: VerifyDeeplinkProviderResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [VerificationPage]
})
export class VerificationPageModule {}

