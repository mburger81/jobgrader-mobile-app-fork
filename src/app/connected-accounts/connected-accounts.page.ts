import { Component, OnInit } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { DropboxService } from '../core/providers/cloud/dropbox.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { GdriveService } from '../core/providers/cloud/gdrive.service';
import { OnedriveService } from '../core/providers/cloud/onedrive.service';

@Component({
  selector: 'app-connected-accounts',
  templateUrl: './connected-accounts.page.html',
  styleUrls: ['./connected-accounts.page.scss'],
})
export class ConnectedAccountsPage implements OnInit {
  
  googleDriveOSSupport = true;
  iCloudOSSupport = false;
  dropboxOSSupport = false;
  onedriveOSSupport = true;

  onedriveStatus = false;
  dropboxStatus = false;
  
  profilePictureSrc;

  constructor(
    private _Platform: Platform,
    private _NavController: NavController,
    private _UserPhotoServiceAkita: UserPhotoServiceAkita,
    private _ToastController: ToastController,
    public _Dropbox: DropboxService,
    public _GDrive: GdriveService,
    public _Onedrive: OnedriveService,
  ) { }

  ngOnInit() {
    this.profilePictureSrc = this._UserPhotoServiceAkita.getPhoto();
    this.dropboxOSSupport = !!this._Platform.is('hybrid');
    this.iCloudOSSupport = (this._Platform.is('hybrid') && this._Platform.is('ios'));
    this.dropboxStatus = !!this._Dropbox.access_token; 
  }

  public goBack(): void {
    this._NavController.navigateBack('/dashboard/tab-settings');
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 2000, position: 'top' });
    await toast.present();
  }

  async alterGoogleDriveAuth(value: boolean) {
    if(value) {
      await this._GDrive.signInToGoogle();
    } else {
      await this._GDrive.handleSignoutClick();
    }
  }

  async alterDropbox(value: boolean) {
    if(value) {
      await this._Dropbox.signInToDropbox((this._Platform.is('hybrid') ? "jobgrader://jobgrader.com/connected-accounts" : window.location.href));
      this.dropboxStatus = true;
    } else {
      await this._Dropbox.logOut();
    }
  }

}
