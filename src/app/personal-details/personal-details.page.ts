import { Component, HostListener } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ModalController, NavController, ToastController } from '@ionic/angular';
import { User } from '../core/models/User';
import { Edit } from '../core/models/Edit';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { TimesService } from '../core/providers/times/times.service';
import { AlertsProviderService } from '../core/providers/alerts/alerts-provider.service';
import { ResponseHandlerService } from '../core/providers/response/response-handler.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SignProviderService } from '../core/providers/sign/sign-provider.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { from } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { Trust } from '../core/models/Trust';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { ImageSelectionPayload } from '../shared/components/image-selection/image-selection.component';
import { TranslateService } from '@ngx-translate/core';
import { NetworkService } from '../core/providers/network/network-service';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { EventsList, EventsService } from '../core/providers/events/events.service';
import { KycService } from '../kyc/services/kyc.service';
import { DocumentModalPageComponent } from '../legal-documents/document-modal/document-modal.component';
import { environment } from 'src/environments/environment';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { KycMedia } from '../core/providers/state/kyc-media/kyc-media.model';
import { KycMediaServiceAkita } from '../core/providers/state/kyc-media/kyc-media.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

interface DocumentDisplay {
    passport?: boolean;
    residencepermit?: boolean;
    idcard?: boolean;
    driverlicence?: boolean;
}

interface VcDisplay {
    passport?: Array<any>;
    residencepermit?: Array<any>;
    idcard?: Array<any>;
    driverlicence?: Array<any>;
}

interface DocumentModalObject {
    header?: string;
    documentType?: string;
    media?: any;
    number?: string;
    numberStatus?: number;
    issuedate?: number;
    issuedateStatus?: number;
    expirydate?: number;
    expirydateStatus?: number;
    issuecountry?: string;
    issuecountryStatus?: string;
    vc?: Array<any>;
}

interface CountryDisplay {
    countryofbirth?: string;
    citizenship?: string;
    country?: string;
    identificationissuecountry?: string;
    driverlicencecountry?: string;
    passportissuecountry?: string;
    residencepermitissuecountry?: string;
}

@Component({
    selector: 'app-personal-details',
    templateUrl: './personal-details.page.html',
    styleUrls: ['./personal-details.page.scss'],
})
export class PersonalDetailsPage {
    private static defaultImage = '../../assets/job-user.svg';
    public user: User = {};
    public modelUser: Edit = {};
    public profilePictureSrc = PersonalDetailsPage.defaultImage;
    public editMode;
    public editForm: UntypedFormGroup;
    public signProcessing = false;
    private browser: InAppBrowserObject;
    public duration: number = 3000;
    public displayVerifiedTick = false;
    public countryList = this.signService.countries;
    public countryDisplay: CountryDisplay = {};
    public tooltipEvent: 'click' | 'press' | 'hover' = 'click';
    TOKEN_VALUE = '';

    public avaliableDocuments: any[] = [
      {
        label: this.translateService.instant('PERSONALDETAILS.radioPassport'),
        value: 'PASSPORT',
      },
      {
        label: this.translateService.instant('PERSONALDETAILS.radioIDCard'),
        value: 'ID_CARD'
      },
      {
        label: this.translateService.instant('PERSONALDETAILS.radioResidencePermit'),
        value: 'RESIDENCE_PERMIT'
      },
      {
        label: this.translateService.instant('PERSONALDETAILS.radioDL'),
        value: 'DRIVERS_LICENSE'
      },
    ];

    avaliableGenders: any[] = [
        {
            title: 'Male',
            value: 'Male'
        },
        {
            title: 'Female',
            value: 'Female'
        },
        {
            title: 'Neutral',
            value: 'Neutral'
        },
    ];

    availableTitles: any[] = [
        {label: 'Dr', value: 'Dr.'},
        {label: 'Prof', value: 'Prof.'},
        {label: 'ProfDr', value: 'ProfDr.'},
        {label: 'none', value: ''},
    ];

    avaliableMaritalStatus: any[] = [
        {title: 'single', value: 'Single'},
        {title: 'married', value: 'Married'},
        {title: 'divorced', value: 'Divorced'},
        {title: 'none', value: ''},
    ];


    public isInnovator = false;

    public documentDisplay: DocumentDisplay = {};
    public vcDisplay: VcDisplay = {
        passport: [],
        idcard: [],
        driverlicence: [],
        residencepermit: []
    };

    private passportMedia: Array<any> = [];
    private idCardMedia: Array<any> = [];
    private residencePermitMedia: Array<any> = [];
    private dlMedia: Array<any> = [];

    public allVerified = {
        idcard: false,
        driverlicence: false,
        passport: false,
        residencepermit: false
    }

    public displayDocumentCard = {
      idcard: false,
      driverlicence: false,
      passport: false,
      residencepermit: false
  }

    constructor(
        public signService: SignProviderService,
        private apiProviderService: ApiProviderService,
        private _SecureStorageService: SecureStorageService,
        private _KycService: KycService,
        private responseHandler: ResponseHandlerService,
        private alertsProviderService: AlertsProviderService,
        private loaderProviderService: LoaderProviderService,
        private userProviderService: UserProviderService,
        private toastController: ToastController,
        public translateProviderService: TranslateProviderService,
        public timesService: TimesService,
        private appStateService: AppStateService,
        private nav: NavController,
        private translateService: TranslateService,
        private crypto: CryptoProviderService,
        private _NetworkService: NetworkService,
        private _ModalController: ModalController,
        private _ImageSelectionService: ImageSelectionService,
        private _CryptoProviderService: CryptoProviderService,
        private _EventsService: EventsService,
        private kycMediaServiceAktia: KycMediaServiceAkita,
        private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) {
        this.editForm = new UntypedFormGroup({
            firstname: new UntypedFormControl('', [
                // Validators.required
                // ,Validators.pattern('^[A-Za-z0-9 !?@#$%^&*)(+=._-]+$')
            ]),
            // middlename: new FormControl(''),
            lastname: new UntypedFormControl('', [
                // Validators.required
                // ,Validators.pattern('^[A-Za-z0-9 !?@#$%^&*)(+=._-]+$')
            ]),
            gender: new UntypedFormControl('', [
                // Validators.required
            ]),
            title: new UntypedFormControl(''),
            maritalstatus: new UntypedFormControl(''),
            // email: new FormControl('', [ Validators.email ]),
            // phone: new FormControl('', [ Validators.pattern('^[0-9!?@#$%^&*)(+=._-]+$') ]),
            address: new UntypedFormControl('', [
                // Validators.required
            ]),
            cityofbirth: new UntypedFormControl('', [
                // Validators.required
            ]),
            state: new UntypedFormControl(''),
            city: new UntypedFormControl('', [
                // Validators.required
            ]),
            zipcode: new UntypedFormControl('', [
                // Validators.required,
                Validators.pattern('^[A-Za-z0-9!?@#$%^&*)(+=._-]+$')
            ]),
            dateofbirth: new UntypedFormControl('', [
                // Validators.required
            ]),
            citizenship: new UntypedFormControl(''),
            residencecountry: new UntypedFormControl('', [
                // Validators.required
            ]),
            countryofbirth: new UntypedFormControl('', [
                // Validators.required
            ]),
        }, {
            validators: this.confirmingForm
        });
    }

    public async ionViewWillEnter() {
        var sortedTitleArray = Array.from(this.avaliableGenders, g => this.translateProviderService.instant('SIGNSTEPSIX.' + g.title)).sort();
        var emptyGenderArray = [];
        sortedTitleArray.forEach(s => {
            emptyGenderArray.push(this.avaliableGenders.find(k => this.translateProviderService.instant('SIGNSTEPSIX.' + k.title) == s))
        })
        this.avaliableGenders = emptyGenderArray;

        this.editMode = false;
        this.profilePictureSrc = PersonalDetailsPage.defaultImage;

        if(this.appStateService.basicAuthToken) {
          const photo = this.userPhotoServiceAkita.getPhoto();
          if (photo) { this.profilePictureSrc = photo; }
        }

        var userDataString = await this._SecureStorageService.getValue(SecureStorageKey.userData, false);
        var trustDataString = await this._SecureStorageService.getValue(SecureStorageKey.userTrustData, false);

        if(userDataString || trustDataString) {
          var userData = JSON.parse(userDataString);
          var trustData = JSON.parse(trustDataString);

          this.user = Object.assign({}, userData, this.userProviderService.mapTrustData(trustData));
          this.processCountryDisplay(this.user);
        }

        if(!this.appStateService.basicAuthToken) {
          this.apiProviderService.noUserLoggedInAlert();
          // this._AuthenticationProviderService.displayLoginOptionIfNecessary();
        }

        this.displayVerifiedTick = await this._KycService.isUserAllowedToUseChatMarketplace();

        if(!this._NetworkService.checkConnection()) {
            this._NetworkService.addOfflineFooter('ion-footer');
            return;
        } else {
            this._NetworkService.removeOfflineFooter('ion-footer');
        }
        this._EventsService.subscribe(EventsList.dataProcessedAfterKyc, async () => {
          console.log("Push Service: ", "this._EventsService.subscribe(EventsList.dataProcessedAfterKyc)");
          var userDataString = await this._SecureStorageService.getValue(SecureStorageKey.userData, false);
          var trustDataString = await this._SecureStorageService.getValue(SecureStorageKey.userTrustData, false);
          var userData = !!userDataString ?  JSON.parse(userDataString) : {};
          var trustData = !!trustDataString ?  JSON.parse(trustDataString) : {};
          this.user = Object.assign({}, userData, this.userProviderService.mapTrustData(trustData));
          this.signService.documentSetter(userData);
          this.processCountryDisplay(this.user);
          if ( this.user.title === 'Dr.eh.Dr.' ) {
            this.user.title = 'Dr.';
          }
          console.log("Push Service: this.user", this.user);
          await this.processDocumentDisplay();
          this.populateDocumentMedia();
        })


  // console.log(this.user);

  this.displayDocumentCard = {
    passport: ( this.user.passportnumberStatus == 1 || this.user.passportissuecountryStatus == 1 || this.user.passportexpirydateStatus == 1 || this.user.passportissuedateStatus == 1),
    idcard: ( this.user.identificationdocumentnumberStatus == 1|| this.user.identificationissuecountryStatus == 1 || this.user.identificationexpirydateStatus == 1 || this.user.identificationissuedateStatus == 1),
    driverlicence: ( this.user.driverlicencedocumentnumberStatus == 1 || this.user.driverlicencecountryStatus == 1 || this.user.driverlicenceexpirydateStatus == 1 || this.user.driverlicenceissuedateStatus == 1),
    residencepermit: ( this.user.residencepermitnumberStatus == 1 || this.user.residencepermitissuecountryStatus == 1 || this.user.residencepermitexpirydateStatus == 1 || this.user.residencepermitissuedateStatus == 1),
  }

  this.allVerified = {
    passport: ( this.user.passportnumberStatus == 1 && this.user.passportissuecountryStatus == 1 && this.user.passportexpirydateStatus == 1),
    idcard: ( this.user.identificationdocumentnumberStatus == 1 && this.user.identificationissuecountryStatus == 1 && this.user.identificationexpirydateStatus == 1),
    driverlicence: ( this.user.driverlicencedocumentnumberStatus == 1 && this.user.driverlicencecountryStatus == 1 && this.user.driverlicenceexpirydateStatus == 1),
    residencepermit: ( this.user.residencepermitnumberStatus == 1 && this.user.residencepermitissuecountryStatus == 1 && this.user.residencepermitexpirydateStatus == 1),
  }

  // console.log(this.allVerified)

  if ( this.user.title === 'Dr.eh.Dr.' ) {
    this.user.title = 'Dr.';
  }

  // console.log(this.user)

  await this.processDocumentDisplay();
  this.populateDocumentMedia();

  var vcs = await this._SecureStorageService.getValue(SecureStorageKey.verifiableCredentialEncrypted, false);
  var vcsArray = !!vcs ? JSON.parse(vcs) : [];
  if(vcsArray.length > 0) {

    var passportVcs = [];
    var dlVcs = [];
    var residencePermitVcs = [];
    var idcardVcs = [];

      for(let ijk = 0; ijk < vcsArray.length; ijk ++) {
        var k = vcsArray[ijk];
        var decrypt = await this._CryptoProviderService.decryptVerifiableCredential(k);
        if(!!decrypt.vc) {
          var vc = JSON.parse(decrypt.vc);
          console.log("vc");
          console.log(vc);
          if(!!vc.credentialSubject && !!vc.credentialSubject.data) {
            var keys = !!vc.credentialSubject.data[0] ? Object.keys(vc.credentialSubject.data[0]) : Object.keys(vc.credentialSubject.data);

            if(keys.includes('passportnumber') || keys.includes('passportissuecountry') || keys.includes('passportissuedate') || keys.includes('passportexpirydate')){
              passportVcs.push(vc);
            }
            if(keys.includes('driverlicencedocumentnumber') || keys.includes('driverlicenceissuecountry') || keys.includes('driverlicenceissuedate') || keys.includes('driverlicenceexpirycountry')){
              dlVcs.push(vc);
            }
            if(keys.includes('identificationdocumentnumber') || keys.includes('identificationissuecountry') || keys.includes('identificationissuedate') || keys.includes('identificationexpirydate')){
              idcardVcs.push(vc);
            }
            if(keys.includes('residencepermitnumber') || keys.includes('residencepermitissuecountry') || keys.includes('residencepermitissuedate') || keys.includes('residencepermitexpirydate')){
              residencePermitVcs.push(vc);
            }
          }
        }
      }

      if(passportVcs.length > 0) {
        this.vcDisplay.passport.push(passportVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(passportVcs, bb => +new Date(bb.proof.created)))));
      }
      if(dlVcs.length > 0) {
        this.vcDisplay.driverlicence.push(dlVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(dlVcs, bb => +new Date(bb.proof.created)))));
      }
      if(residencePermitVcs.length > 0) {
        this.vcDisplay.residencepermit.push(residencePermitVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(residencePermitVcs, bb => +new Date(bb.proof.created)))));
      }
      if(idcardVcs.length > 0) {
        this.vcDisplay.idcard.push(idcardVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(idcardVcs, bb => +new Date(bb.proof.created)))));
      }

    }

    console.log(this.vcDisplay);
    }

    goBack() {
        this.nav.navigateBack('/dashboard/tab-profile?source=smart');
    }

    goToVerificationOptions() {
        if(!this.appStateService.basicAuthToken) {
          this.apiProviderService.noUserLoggedInAlert();
          return;
        }
        this.nav.navigateForward('/kyc');
    }

    goToPage(page: string) {
      if(!this.appStateService.basicAuthToken) {
        this.apiProviderService.noUserLoggedInAlert();
        return;
      }
      this.nav.navigateForward(page);
    }

    // public async veriffIdentity(info: string): Promise<void> {
    //   if (!info) { return; }

    //   if(!this._NetworkService.checkConnection()) {
    //     this._NetworkService.addOfflineFooter('ion-footer');
    //     this.presentToast(this.translateService.instant('NETWORK.checkConnection'));
    //     return;
    //   } else {
    //     this._NetworkService.removeOfflineFooter('ion-footer');
    //   }

    //   let {code2, supportedDocuments}: {code2: string, supportedDocuments: Array<ProviderDocument>} = this.signService.identityCountries.find(c => c.code3 === info);
    //   // console.log(supportedDocuments);
    //   code2 = code2.toLowerCase();
    //   var tt = supportedDocuments.map(k => k.documents)
    //   const unionSet = uniq([...tt[0],...tt[1]]);
    //   // const unionSet = uniq([...tt[0]]);
    //   // console.log(unionSet);
    //   let buttonArray = [];
    //   unionSet.forEach(l => {
    //     buttonArray.push({
    //       text: this.avaliableDocuments[this.avaliableDocuments.findIndex(k => k.value == l)].label,
    //       value: l,
    //       handler: async () => {
    //         await this.processDocument(l, code2, supportedDocuments);
    //       }
    //     })
    //   })
    //   buttonArray.push({
    //     text: this.translateService.instant('BUTTON.CANCEL'),
    //     role: 'cancel',
    //     cssClass: 'secondary',
    //     handler: () => {
    //       console.log('Document Selection Cancelled');
    //     }
    //   });
    //   // console.log(JSON.stringify(buttonArray))
    //   await this.alertsProviderService.alertCreate(this.translateService.instant('KYC.DOCUMENTSELECTOR.header'),
    //   this.translateService.instant('KYC.DOCUMENTSELECTOR.subheader'),'',
    //   buttonArray);
    //   this.lockService.addResume();
    // }

    // public async processDocument(docType: string, country: string, supportedDocuments: Array<ProviderDocument>){
    //   // console.log(JSON.stringify({type: docType, country: country}));

    //   if(!this._NetworkService.checkConnection()) {
    //     this._NetworkService.addOfflineFooter('ion-footer');
    //     this.presentToast(this.translateService.instant('NETWORK.checkConnection'));
    //     return;
    //   } else {
    //     this._NetworkService.removeOfflineFooter('ion-footer');
    //   }

    //   var providerArray: Array<KycVendors> = []

    //   supportedDocuments.forEach(l => {
    //     if(l.documents.includes(docType)){
    //       providerArray.push(l.provider)
    //     }
    //   })

    //   providerArray = uniq(providerArray);

    //   const kycWithVeriff = async () => {
    //     const docState: {prod: KycProvider; test: KycProvider} = this.getStateArray(docType);
    //     const provider = environment.verificationProd ? docState.prod : docState.test;
    //     await this._KycService.addInitialState(provider);
    //     // console.log(provider);
    //     if (this._KycService.getState(provider) === KycState.VC_RETRIEVED) {
    //       await this.alreadyRetrieved(provider);
    //     } else {
    //       this._KycService.patchState(provider, KycState.KYC_WAITING);
    //       await this._KycService.setState(provider, KycState.KYC_INITIATED, undefined, {type: docType, country: country});
    //     }
    //   }

    //   const kycWithVerifeye = async () => {
    //     console.log('Verifeye Begin');
    //     const docState: {prod: KycProvider; test: KycProvider} = this.getVerfieyeStateArray(docType);
    //     const provider = environment.verificationProd ? docState.prod : docState.test;
    //     // console.log(provider);
    //     // console.log(docState);
    //     await this._KycService.addInitialState(provider);
    //     // console.log(provider);
    //     if (this._KycService.getState(provider) === KycState.VC_RETRIEVED) {
    //       // console.log('1');
    //       await this.alreadyRetrieved(provider);
    //     } else {
    //       // console.log('2');
    //       this._KycService.patchState(provider, KycState.KYC_WAITING);
    //       await this._KycService.setState(provider, KycState.KYC_INITIATED, country);
    //     }
    //   }

    //   if(providerArray.length == 1) {
    //     providerArray[0] == KycVendors.VERIFF ? await kycWithVeriff() : await kycWithVerifeye();
    //   } else {

    //       await kycWithVerifeye();
    //   }
    // }

    // private async alreadyRetrieved(provider: KycProvider): Promise<void> {
    //   await this.alertsProviderService.alertCreate(
    //       this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.header`),
    //       this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.sub-header`),
    //       this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.message`),
    //       [
    //         {
    //           text: this.translateService.instant('GENERAL.ok'),
    //           handler: () => {}
    //         }]
    //   );
    // }

    // public getStateArray(docType: string): {prod: KycProvider, test: KycProvider}{
    //   switch (docType) {
    //     case 'PASSPORT':
    //       return {
    //         prod: KycProvider.VERIFF_PROD_PASSPORT,
    //         test: KycProvider.VERIFF_TEST_PASSPORT
    //       };
    //     case 'RESIDENCE_PERMIT':
    //       return {
    //         prod: KycProvider.VERIFF_PROD_RESIDENCEPERMIT,
    //         test: KycProvider.VERIFF_TEST_RESIDENCEPERMIT
    //       };
    //     case 'ID_CARD':
    //       return {
    //         prod: KycProvider.VERIFF_PROD_IDCARD,
    //         test: KycProvider.VERIFF_TEST_IDCARD
    //       };
    //   }
    // }

    // public getVerfieyeStateArray(docType: string): {prod: KycProvider, test: KycProvider}{
    //   switch (docType) {
    //     case 'PASSPORT':
    //       return {
    //         prod: KycProvider.VERIFEYE_PROD_PASSPORT,
    //         test: KycProvider.VERIFEYE_TEST_PASSPORT
    //       };
    //     case 'ID_CARD':
    //       return {
    //         prod: KycProvider.VERIFEYE_PROD_IDCARD,
    //         test: KycProvider.VERIFEYE_TEST_IDCARD
    //       };
    //     case 'RESIDENCE_PERMIT':
    //       return {
    //         prod: KycProvider.VERIFEYE_PROD_RESIDENCEPERMIT,
    //         test: KycProvider.VERIFEYE_TEST_RESIDENCEPERMIT
    //       };
    //     case 'DRIVERS_LICENSE':
    //       return {
    //         prod: KycProvider.VERIFEYE_PROD_DRIVINGLICENCE,
    //         test: KycProvider.VERIFEYE_TEST_DRIVINGLICENCE
    //       };
    //   }
    // }

    /** Callback for image name change */
    public async onImageChanged(imageSelectionPayload: ImageSelectionPayload): Promise<void> {
        if (imageSelectionPayload.fileName !== 'Default') {
            if (!this._NetworkService.checkConnection()) {
                this._NetworkService.addOfflineFooter('ion-footer');
                return;
            } else {
                this._NetworkService.removeOfflineFooter('ion-footer');
            }

            console.log("imageSelectionPayload 2", imageSelectionPayload);

            this.apiProviderService.updatePicture(
                this.user,
                imageSelectionPayload.base64Image,
                imageSelectionPayload.fileName,
                imageSelectionPayload.format).then(imageUrl => {

                  console.log("imageUrl 2", imageUrl);

                // dirty hack for android after video gif maker runs.
                if (imageSelectionPayload.reloadApp) {
                    window.location.reload();
                }
                else {
                    this.profilePictureSrc = imageUrl;
                    // Maybe we can assign base64Image to this.profilePictureSrc
                }

            });
        } else {
            this.removePicture();
        }
    }

    /** Method to delete the user picture */
    private removePicture(): void {
        this.apiProviderService.deleteImages(this.appStateService.basicAuthToken, this.user.userid)
            .then(async _ => {
                const toast = await this.toastController.create({
                    message: this.translateProviderService.instant('PERSONALDETAILS.deleteImageSuccess'),
                    duration: 2000,
                    position: 'top',
                });
                await toast.present();
                this.profilePictureSrc = PersonalDetailsPage.defaultImage;
                this.userPhotoServiceAkita.clear();
            }, async _ => {
                const toast = await this.toastController.create({
                    message: this.translateProviderService.instant('PERSONALDETAILS.deleteImageError'),
                    duration: 2000,
                    position: 'top',
                });
                await toast.present();
            });
    }

    async edit() {
        console.log('edit works');
        this.editMode = true;
        this.fillModel(this.modelUser);
        this.setFormDataFromStorage(this.modelUser);
    }

    fillModel(model: Edit) {
        try {
            model.firstname = this.user.firstname;
            model.lastname = this.user.lastname;
            model.gender = this.user.gender;
            model.email = this.user.email;
            model.title = this.user.title;
            model.maritalstatus = this.user.maritalstatus;
            model.email = this.user.email;
            model.phone = this.user.phone;
            model.address = this.user.street;
            model.city = this.user.city;
            model.zipcode = this.user.zip;
            model.cityofbirth = this.user.cityofbirth;
            model.state = this.user.state;
        } catch (e) {
            console.log(e);
        }

        try {
            model.countryofbirth = this.user.countryofbirth;
            model.citizenship = this.user.citizenship;
            model.residencecountry = this.user.country;
        } catch (e) {
            console.log(e);
        }

        try {
            model.dateofbirth = this.user.dateofbirth ? (new Date(this.user.dateofbirth)).toISOString() : null;
        } catch (e) {
            console.log(e);
        }
    }

    private confirmingForm() {
        return (formGroup: UntypedFormGroup) => {

        };
    }

    private setFormDataFromStorage(model: Edit): void {
        if ( model && model.firstname ) {
            this.editForm.controls.firstname.setValue(model.firstname);
        }
        if ( model && model.lastname ) {
            this.editForm.controls.lastname.setValue(model.lastname);
        }
        if ( model && model.gender ) {
            this.editForm.controls.gender.setValue(model.gender);
        }
        if ( model && model.title ) {
            this.editForm.controls.title.setValue(model.title);
        }
        if ( model && model.maritalstatus ) {
            this.editForm.controls.maritalstatus.setValue(model.maritalstatus);
        }
        if ( model && model.address ) {
            this.editForm.controls.address.setValue(model.address);
        }
        if ( model && model.city ) {
            this.editForm.controls.city.setValue(model.city);
        }
        if ( model && model.zipcode ) {
            this.editForm.controls.zipcode.setValue(model.zipcode);
        }
        if ( model && model.countryofbirth ) {
            this.editForm.controls.countryofbirth.setValue(model.countryofbirth);
        }
        if ( model && model.cityofbirth ) {
            this.editForm.controls.cityofbirth.setValue(model.cityofbirth);
        }
        if ( model && model.state ) {
            this.editForm.controls.state.setValue(model.state);
        }
        if ( model && model.citizenship ) {
            this.editForm.controls.citizenship.setValue(model.citizenship);
        }
        if ( model && model.residencecountry ) {
            this.editForm.controls.residencecountry.setValue(model.residencecountry);
        }
        if ( model && model.dateofbirth ) {
            this.editForm.controls.dateofbirth.setValue(model.dateofbirth);
        }
    }

    cancel() {
        console.log('cancel works');
        this.editMode = false;
    }

    async save() {
        if(!this._NetworkService.checkConnection()) {
            this._NetworkService.addOfflineFooter('ion-footer');
            this.toastController.create({
                message: this.translateProviderService.instant('NETWORK.checkConnection'),
                duration: 3000,
                position: 'top',
            }).then(toast => {
                toast.present();
            });
            return;
        } else {
            this._NetworkService.removeOfflineFooter('ion-footer');
        }
        console.log('save works');
        this.editForm.markAsDirty();
        this.signProcessing = true;

        if ( !this.editForm.valid ) {
            this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.fillToast'));
            return false;
        }
        if ( (this.editForm.controls.firstname.value).length >=50 ||
        (this.editForm.controls.lastname.value).length >=50 ||
        (this.editForm.controls.address.value).length >=50 ||
        (this.editForm.controls.zipcode.value).length >=50 ||
        (this.editForm.controls.city.value).length >=50
        // || (this.editForm.controls.idcardnumber.value).length >=50 ||
        // (this.editForm.controls.dlnumber.value).length >=50
        ){
            return this.presentToast(this.translateProviderService.instant('PERSONALDETAILS.lengthError')
            + this.translateProviderService.instant('SIGNSTEPTWO.firstName') + ", "
            + this.translateProviderService.instant('SIGNSTEPTWO.lastName') + ", "
            + this.translateProviderService.instant('SIGNSTEPSEVEN.address') + ", "
            + this.translateProviderService.instant('SIGNSTEPSEVEN.postalCode') + ", "
            + this.translateProviderService.instant('SIGNSTEPSEVEN.city')
            );
        }
        await this.loaderProviderService.loaderCreate();
        this.signProcessing = false;
        const processedJSON = await this.processJSON();
        from(this.apiProviderService.saveUser(this.appStateService.basicAuthToken, processedJSON))
            .pipe(
                switchMap(_ => combineLatest(
                    [from(this.apiProviderService.getTrustData(this.appStateService.basicAuthToken, this.user.userid)),
                    from(this.apiProviderService.getUserByIdSecure(this.appStateService.basicAuthToken, this.user.userid))
                    ])
                ),
                catchError(error => {
                    console.log(error);
                    this.alertsProviderService.alertCreate('', '', this.responseHandler.getResponseTranslation(error), ['Ok']);
                    this.loaderProviderService.loaderDismiss();
                    return error;
                }),
                take(1)
            ).subscribe(([trust, user]: [Trust, User]) => {
            this.loaderProviderService.loaderDismiss();

            this._SecureStorageService.setValue(SecureStorageKey.userData, JSON.stringify(user)).then(()=>{})
            this._SecureStorageService.setValue(SecureStorageKey.userTrustData, JSON.stringify(trust)).then(()=>{})

            this.user = Object.assign({}, user, this.userProviderService.mapTrustData(trust));
            this.processCountryDisplay(this.user);
            this.editMode = false;
        }, _ => void this.loaderProviderService.loaderDismiss());
    }

    processCountryDisplay(user: User) {
        let temp_countryofbirth = this.countryList.find(d => d.code3 === user.countryofbirth);
        let temp_citizenship = this.countryList.find(d => d.code3 === user.citizenship);
        let temp_country = this.countryList.find(d => d.code3 === user.country);

        this.countryDisplay.countryofbirth = (!!user.countryofbirth && !!temp_countryofbirth) ? this.translateProviderService.instant(temp_countryofbirth["countryTranslateKey"]) : user.countryofbirth;
        this.countryDisplay.citizenship = (!!user.citizenship && !!temp_citizenship) ? this.translateProviderService.instant(temp_citizenship["countryTranslateKey"]) : user.citizenship;
        this.countryDisplay.country = (!!user.country && !!temp_country) ? this.translateProviderService.instant(temp_country["countryTranslateKey"]) : user.country;
    }

    async processJSON(): Promise<any> {
        const token = await this._SecureStorageService.getValue(SecureStorageKey.basicAuthToken, false);
        const symmetricKey = await this.crypto.returnUserSymmetricKey();
        const chatPublicKey = await this._SecureStorageService.getValue(SecureStorageKey.chatPublicKey, false);
        const chatPrivateKey = await this._SecureStorageService.getValue(SecureStorageKey.chatPrivateKey, false);
        const encryptedChatPrivateKey = await this.crypto.symmetricEncrypt(chatPrivateKey, symmetricKey);
        const object = {
            user: {
                username: window.atob(token).split(':')[0],
                password: null,
                email: this.user.email,
                id: this.user.userid,
                chatPublicKey: chatPublicKey,
                encryptedChatPrivateKey: encryptedChatPrivateKey
            },
            basicdata: {
                userid: this.user.userid,
                firstname: this.editForm.controls.firstname.value ? this.editForm.controls.firstname.value : (this.editForm.controls.firstname.value == '' ? this.editForm.controls.firstname.value : this.user.firstname),
                lastname: this.editForm.controls.lastname.value ? this.editForm.controls.lastname.value : (this.editForm.controls.lastname.value == '' ? this.editForm.controls.lastname.value : this.user.lastname),
                middlename: this.user.middlename,
                maidenname: this.user.maidenname,
                title: this.editForm.controls.title.value ? this.editForm.controls.title.value : (this.editForm.controls.title.value === '' ? this.editForm.controls.title.value : this.user.title),
                dateofbirth: this.editForm.controls.dateofbirth.value ? this.getFormattedDay(this.editForm.controls.dateofbirth.value) : this.user.dateofbirth,
                gender: this.editForm.controls.gender.value ? this.editForm.controls.gender.value : this.user.gender,
                phone: this.user.phone,
                email: this.user.email,
                callname: this.user.callname,
                street: this.editForm.controls.address.value ? this.editForm.controls.address.value : (this.editForm.controls.address.value == '' ? this.editForm.controls.address.value : this.user.street),
                city: this.editForm.controls.city.value ? this.editForm.controls.city.value : (this.editForm.controls.city.value == '' ? this.editForm.controls.city.value : this.user.city) ,
                zip: this.editForm.controls.zipcode.value ? this.editForm.controls.zipcode.value : (this.editForm.controls.zipcode.value == '' ? this.editForm.controls.zipcode.value : this.user.zip) ,
                maritalstatus: this.editForm.controls.maritalstatus.value ? this.editForm.controls.maritalstatus.value : (this.editForm.controls.maritalstatus.value === '' ? this.editForm.controls.maritalstatus.value : this.user.maritalstatus),
                termsofuse: this.user.termsofuse,
                termsofusethirdparties: this.user.termsofusethirdparties,
                identificationdocumenttype: this.user.identificationdocumenttype,
                cityofbirth: this.editForm.controls.cityofbirth.value ? this.editForm.controls.cityofbirth.value : (this.editForm.controls.cityofbirth.value == '' ? this.editForm.controls.cityofbirth.value : this.user.cityofbirth) ,
                state: this.editForm.controls.state.value ? this.editForm.controls.state.value : (this.editForm.controls.state.value == '' ? this.editForm.controls.state.value : this.user.state) ,
                countryofbirth: this.editForm.controls.countryofbirth.value ? this.editForm.controls.countryofbirth.value : (this.editForm.controls.countryofbirth.value == '' ? this.editForm.controls.countryofbirth.value : this.user.countryofbirth) ,
                country: this.editForm.controls.residencecountry.value ? this.editForm.controls.residencecountry.value : (this.editForm.controls.residencecountry.value == '' ? this.editForm.controls.residencecountry.value : this.user.country) ,
                citizenship: this.editForm.controls.citizenship.value ? this.editForm.controls.citizenship.value : (this.editForm.controls.citizenship.value == '' ? this.editForm.controls.citizenship.value : this.user.citizenship) ,
                identificationdocumentnumber: this.user.identificationdocumentnumber,
                identificationissuecountry: this.user.identificationissuecountry,
                identificationissuedate: this.user.identificationissuedate,
                identificationexpirydate: this.user.identificationexpirydate,
                driverlicencedocumentnumber: this.user.driverlicencedocumentnumber,
                driverlicencecountry: this.user.driverlicencecountry,
                driverlicenceissuedate: this.user.driverlicenceissuedate,
                driverlicenceexpirydate: this.user.driverlicenceexpirydate,
                passportnumber: this.user.passportnumber,
                passportissuecountry: this.user.passportissuecountry,
                passportissuedate: this.user.passportissuedate,
                passportexpirydate: this.user.passportexpirydate,
                residencepermitnumber: this.user.residencepermitnumber,
                residencepermitissuecountry: this.user.residencepermitissuecountry,
                residencepermitissuedate: this.user.residencepermitissuedate,
                residencepermitexpirydate: this.user.residencepermitexpirydate
            }
        };
        return object;
    }

    async presentToast(message: string) {
        const toast = await this.toastController.create({
            message,
            duration: 2000,
            position: 'top',
        });
        toast.present();
    }

    private getFormattedDay(date: string): number {
        const datePipe = new DatePipe('en-US');
        const ms = datePipe.transform(new Date(date), 'SSS');
        const t = datePipe.transform(new Date(date), 'E MMM dd yyyy HH:mm:ss zzzz');
        const nd = +new Date(t).setMilliseconds(parseInt(ms));
        return nd;
    }

    @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        if ( !targetElement ) {
            return;
        }
        const ids = ['select-flag-1', 'select-flag-2', 'select-flag-3', 'select-flag-4', 'select-flag-5'];
        if ( ids.indexOf(targetElement.id) !== -1 ) {
            this.signService.addSelectBoxAdditionalElements();
        }
    }

    compareWith(o1, o2){
        return o1 == o2;
    }

    dateTimeManager(event: any) {
        console.log(event);
        this.modelUser.dateofbirth = event.detail.value;
        this.editForm.controls.dateofbirth.setValue(this.modelUser.dateofbirth);
        console.log(this.editForm);
    }

    async processDocumentDisplay() {
        this.documentDisplay.driverlicence = (await this._SecureStorageService.getValue(SecureStorageKey.driverLicenseSet, false) == "true" ) ? true : false ;
        this.documentDisplay.idcard = (await this._SecureStorageService.getValue(SecureStorageKey.idCardSet, false) == "true" ) ? true : false ;
        this.documentDisplay.passport = (await this._SecureStorageService.getValue(SecureStorageKey.passportSet, false) == "true" ) ? true : false ;
        this.documentDisplay.residencepermit = (await this._SecureStorageService.getValue(SecureStorageKey.residencePermitSet, false) == "true" ) ? true : false ;
    }



        async addDocument() {
          let buttonArray = [];
          for(let i of [{
            bool: this.documentDisplay.passport,
            methodArg: 'PASSPORT',
            document: this.translateProviderService.instant('PERSONALDETAILS.radioPassport')
          },{
            bool: this.documentDisplay.idcard,
            methodArg: 'ID_CARD',
            document: this.translateProviderService.instant('PERSONALDETAILS.radioIDCard')
          },{
            bool: this.documentDisplay.driverlicence,
            methodArg: 'DRIVERS_LICENSE',
            document: this.translateProviderService.instant('PERSONALDETAILS.radioDL')
          },{
            bool: this.documentDisplay.residencepermit,
            methodArg: 'RESIDENCE_PERMIT',
            document: this.translateProviderService.instant('PERSONALDETAILS.radioResidencePermit')
          }]){
            if(!i.bool){
              buttonArray.push(
                {
                  text: i.document,
                  handler: () => {
                    console.log(i.methodArg + "selected");
                    this.showModal(i.methodArg);
                  }
                }
              )
            }
          }
          if(buttonArray.length > 0) {
            buttonArray.push({
              text: this.translateProviderService.instant('BUTTON.CANCEL'),
              role: 'cancel',
              handler: () => {
                // console.log("cancelled");
              }
            })
            await this.alertsProviderService.alertCreate(this.translateProviderService.instant('SIGNSTEPEIGHT.documentType'),'','',buttonArray);
          } else {
            buttonArray.push({
              text: this.translateProviderService.instant('GENERAL.ok'),
              role: 'cancel',
              handler: () => {
                // console.log("cancelled");
              }
            })
            await this.alertsProviderService.alertCreate(this.translateProviderService.instant('CONTACT.title'),'',this.translateProviderService.instant('LEGALDOCUMENTS.noMoreDocs'),buttonArray);
          }

        }



        async showModal(header: string) {
          let documentObject: DocumentModalObject = {};
          switch(header) {
            case 'PASSPORT':
              documentObject = {
                header: this.translateProviderService.instant('PERSONALDETAILS.radioPassport'),
                media: this.passportMedia,
                number: this.user.passportnumber,
                numberStatus: this.user.passportnumberStatus,
                issuedate: this.user.passportissuedate,
                issuedateStatus: this.user.passportissuedateStatus,
                expirydate: this.user.passportexpirydate,
                expirydateStatus: this.user.passportexpirydateStatus,
                issuecountry: this.user.passportissuecountry,
                issuecountryStatus: this.user.passportissuecountryStatus,
                vc: this.vcDisplay.passport
              }
              break;
            case 'ID_CARD':
              documentObject = {
                header: this.translateProviderService.instant('PERSONALDETAILS.radioIDCard'),
                media: this.idCardMedia,
                number: this.user.identificationdocumentnumber,
                numberStatus: this.user.identificationdocumentnumberStatus,
                issuedate: this.user.identificationissuedate,
                issuedateStatus: this.user.identificationissuedateStatus,
                expirydate: this.user.identificationexpirydate,
                expirydateStatus: this.user.identificationexpirydateStatus,
                issuecountry: this.user.identificationissuecountry,
                issuecountryStatus: this.user.identificationissuecountryStatus,
                vc: this.vcDisplay.idcard
              }
              break;
            case 'RESIDENCE_PERMIT':
              documentObject = {
                header: this.translateProviderService.instant('PERSONALDETAILS.radioResidencePermit'),
                media: this.residencePermitMedia,
                number: this.user.residencepermitnumber,
                numberStatus: this.user.residencepermitnumberStatus,
                issuedate: this.user.residencepermitissuedate,
                issuedateStatus: this.user.residencepermitissuedateStatus,
                expirydate: this.user.residencepermitexpirydate,
                expirydateStatus: this.user.residencepermitexpirydateStatus,
                issuecountry: this.user.residencepermitissuecountry,
                issuecountryStatus: this.user.residencepermitissuecountryStatus,
                vc: this.vcDisplay.residencepermit
              }
              break;
            case 'DRIVERS_LICENSE':
              documentObject = {
                header: this.translateProviderService.instant('PERSONALDETAILS.radioDL'),
                media: this.dlMedia,
                number: this.user.driverlicencedocumentnumber,
                numberStatus: this.user.driverlicencedocumentnumberStatus,
                issuedate: this.user.driverlicenceissuedate,
                issuedateStatus: this.user.driverlicenceissuedateStatus,
                expirydate: this.user.driverlicenceexpirydate,
                expirydateStatus: this.user.driverlicenceexpirydateStatus,
                issuecountry: this.user.driverlicencecountry,
                issuecountryStatus: this.user.driverlicencecountryStatus,
                vc: this.vcDisplay.driverlicence
              }
              break;
            default:
              documentObject = {
                header: this.translateProviderService.instant('PERSONALDETAILS.radioPassport'),
                media: this.passportMedia,
                number: this.user.passportnumber,
                numberStatus: this.user.passportnumberStatus,
                issuedate: this.user.passportissuedate,
                issuedateStatus: this.user.passportissuedateStatus,
                expirydate: this.user.passportexpirydate,
                expirydateStatus: this.user.passportexpirydateStatus,
                issuecountry: this.user.passportissuecountry,
                issuecountryStatus: this.user.passportissuecountryStatus,
                vc: this.vcDisplay.passport
              }
              break;
          }
          const modal = await this._ModalController.create({
            component: DocumentModalPageComponent,
            componentProps: {
              data: {
                header: documentObject.header,
                documentType: header,
                close: this.translateProviderService.instant('LEGALDOCUMENTS.closeModal'),
                media: documentObject.media,
                number: documentObject.number,
                numberStatus: documentObject.numberStatus,
                issuedate: documentObject.issuedate,
                issuedateStatus: documentObject.issuedateStatus,
                expirydate: documentObject.expirydate,
                expirydateStatus: documentObject.expirydateStatus,
                issuecountry: documentObject.issuecountry,
                issuecountryStatus: documentObject.issuecountryStatus,
                vc: documentObject.vc
              }
            }
          })
          modal.onDidDismiss().then(() => {
            // await this.loaderProviderService.loaderCreate();
            this.updateDataAfterEdit();
            // await this.loaderProviderService.loaderDismiss();
          })
          await modal.present();
        }

        async displayPictureOptions() {
          // console.log('Should work!')
          this._ImageSelectionService.showChangePicture().then(photo => {
            this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg'
          });
        }


        async updateDataAfterEdit() {
            var userData = JSON.parse(await this._SecureStorageService.getValue(SecureStorageKey.userData, false));
            var trustData = JSON.parse(await this._SecureStorageService.getValue(SecureStorageKey.userTrustData, false));
            this.user = Object.assign({}, userData, this.userProviderService.mapTrustData(trustData));
            this.allVerified = {
              passport: ( this.user.passportnumberStatus == 1 && this.user.passportissuecountryStatus == 1 && this.user.passportexpirydateStatus == 1),
              idcard: ( this.user.identificationdocumentnumberStatus == 1 && this.user.identificationissuecountryStatus == 1 && this.user.identificationexpirydateStatus == 1),
              driverlicence: ( this.user.driverlicencedocumentnumberStatus == 1 && this.user.driverlicencecountryStatus == 1 && this.user.driverlicenceexpirydateStatus == 1),
              residencepermit: ( this.user.residencepermitnumberStatus == 1 && this.user.residencepermitissuecountryStatus == 1 && this.user.residencepermitexpirydateStatus == 1),
            }
            this.displayDocumentCard = {
              passport: ( this.user.passportnumberStatus == 1 || this.user.passportissuecountryStatus == 1 || this.user.passportexpirydateStatus == 1 || this.user.passportissuedateStatus == 1),
              idcard: ( this.user.identificationdocumentnumberStatus == 1|| this.user.identificationissuecountryStatus == 1 || this.user.identificationexpirydateStatus == 1 || this.user.identificationissuedateStatus == 1),
              driverlicence: ( this.user.driverlicencedocumentnumberStatus == 1 || this.user.driverlicencecountryStatus == 1 || this.user.driverlicenceexpirydateStatus == 1 || this.user.driverlicenceissuedateStatus == 1),
              residencepermit: ( this.user.residencepermitnumberStatus == 1 || this.user.residencepermitissuecountryStatus == 1 || this.user.residencepermitexpirydateStatus == 1 || this.user.residencepermitissuedateStatus == 1),
            }
              await this.processDocumentDisplay();
              this.populateDocumentMedia();
          }

          populateDocumentMedia()  {
            // var datePipe = new DatePipe('en-US');
            this.passportMedia = [];
            this.idCardMedia = [];
            this.residencePermitMedia = [];
            this.dlMedia = [];
            const kycMediaArray: KycMedia[] = this.kycMediaServiceAktia.getAllKycMedia();
            // console.log(kycMediaArray)
            for (var i of kycMediaArray) {
              if (!!i.image) {
                 

                  if(i.documentType == 'PASSPORT') {
                    if(this.passportMedia.length < 1) {
                      // this.passportMedia.push(watermarked.src);
                      this.passportMedia.push(i.image);
                    }
                  }
                  if(i.documentType == 'RESIDENCEPERMIT') {
                    if(this.residencePermitMedia.length < 2) {
                      this.residencePermitMedia.push(i.image);
                    }
                  }
                  if(i.documentType == 'IDCARD') {
                    if(this.idCardMedia.length < 2) {
                      this.idCardMedia.push(i.image);
                    }
                  }
                  if(i.documentType == 'DRIVINGLICENCE') {
                    if(this.dlMedia.length < 2) {
                      this.dlMedia.push(i.image);
                    }
                  }
              }
            }
            // console.log("Residence Permit Media: ");
            // console.log(this.residencePermitMedia);
            // console.log("ID Card Media: ");
            // console.log(this.idCardMedia);
            // console.log("Passport Media: ");
            // console.log(this.passportMedia);
            // console.log("DL Media: ");
            // console.log(this.dlMedia);
          }

}
