import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { PersonalDetailsPage } from './personal-details.page';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
// import { TooltipsModule } from 'ionic-tooltips';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
    {
        path: '',
        // canActivate: [AuthenticationGuard],
        component: PersonalDetailsPage,
        resolve: {
            user: UserProviderResolver
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        ReactiveFormsModule,
        // TooltipsModule,
        JobHeaderModule,
        TranslateModule.forChild(),
        RouterModule.forChild(routes)
    ],
    declarations: [PersonalDetailsPage]
})
export class PersonalDetailsPageModule {
}
