import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './core/guards/authentication/authentication-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule) },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule) },
  // { path: 'marketplace/:key', loadChildren: () => import('./marketplace/marketplace.module').then(m => m.MarketplacePageModule) },
  // { path: 'merchant-details/:key/:merchantId', loadChildren: () => import('./merchant-details/merchant-details.module').then(m => m.MerchantDetailsPageModule) },
  { path: 'signature', loadChildren: () => import('./signature/signature.module').then(m => m.SignaturePageModule) },
  { path: 'verification', loadChildren: () => import('./verification/verification.module').then(m => m.VerificationPageModule) },
  { path: 'unrecognized-parameters', loadChildren: () => import('./unrecognized-parameters/unrecognized-parameters.module').then(m => m.UnrecognizedParametersPageModule) },
  // { path: 'personal-details', loadChildren: () => import('./personal-details/personal-details.module').then(m => m.PersonalDetailsPageModule) },
  { path: 'sign-up', loadChildren: () => import('./sign-up/sign-up.module').then(m => m.SignUpPageModule) },
  { path: 'reset-password', loadChildren: () => import('./reset-password/reset-password.module').then(m => m.ResetPasswordPageModule)  },
  { path: 'forgot-username', loadChildren: () => import('./forgot-username/forgot-username.module').then(m => m.ForgotUsernamePageModule) },
  { path: 'settings-account', loadChildren: () => import('./settings-account/settings-account.module').then(m => m.SettingsAccountPageModule) },
  { path: 'security', loadChildren: () => import('./security/security.module').then(m => m.SecurityPageModule) },
  { path: 'notifications', loadChildren: () => import('./notifications/notifications.module').then(m => m.NotificationsPageModule) },
  { path: 'app-overview', loadChildren: () => import('./app-overview/app-overview.module').then(m => m.AppOverviewPageModule) },
  { path: 'onboarding', loadChildren: () => import('./onboarding/onboarding.module').then(m => m.OnboardingPageModule) },
  { path: 'feedback', loadChildren: () => import('./feedback/feedback.module').then(m => m.FeedbackPageModule) },
  { path: 'feedback-thanks', loadChildren: () => import('./feedback-thanks/feedback-thanks.module').then(m => m.FeedbackThanksPageModule) },
  { path: 'kyc', loadChildren: () => import('./kyc/kyc.module').then(m => m.KycPageModule) },
  { path: 'chat', loadChildren: () => import('./chat/chat.module').then(m => m.ChatPageModule) },
  { path: 'veriff-thanks', loadChildren: () => import('./veriff-thanks/veriff-thanks.module').then(m => m.VeriffThanksPageModule) },
  { path: 'veriff-failed', loadChildren: () => import('./veriff-failed/veriff-failed.module').then(m => m.VeriffFailedPageModule) },
  { path: 'contact-detail', loadChildren: () => import('./contact/contact-detail/contact-detail.module').then(m => m.ContactDetailPageModule) },
  { path: 'changepassword', loadChildren: () => import('./changepassword/changepassword.module').then(m => m.ChangePasswordPageModule) },
  // { path: 'legal-documents', loadChildren: () => import('./legal-documents/legal-documents.module').then(m => m.LegalDocumentsPageModule) },
  { path: 'settings-devices', loadChildren: () => import('./settings-devices/settings-devices.module').then(m => m.SettingsDevicesPageModule) },
  { path: 'requests', loadChildren: () => import('./requests/requests.module').then(m => m.RequestsPageModule) },
  // { path: 'alt-walkthrough', loadChildren: () => import('./alt-walkthrough/alt-walkthrough.module').then(m => m.AltWalkthroughPageModule) },
  { path: 'data-sharing-signature', loadChildren: () => import('./data-sharing-signature/data-sharing-signature.module').then(m => m.DataSharingSignaturePageModule) },
  // { path: 'nft', loadChildren: () => import('./nft/nft.module').then(m => m.NftPageModule) },
  // { path: 'payments', loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsPageModule) },
  // { path: 'crypto-account-page', loadChildren: () => import('./payments/crypto-account-page/crypto-account-page.module').then(m => m.CryptoAccountPagePageModule) },
  { path: 'wallet-generation', loadChildren: () => import('./wallet-generation/wallet-generation.module').then(m => m.WalletGenerationPageModule) },
  // { path: 'crypto-activities', loadChildren: () => import('./crypto-activities/crypto-activities.module').then(m => m.CryptoActivitiesPageModule) },
  // { path: 'wallet-settings', loadChildren: () => import('./payments/wallet-settings/wallet-settings.module').then(m => m.WalletSettingsPageModule) },
  // { path: 'network-account-details', loadChildren: () => import('./payments/network-account-details/network-account-details.module').then(m => m.NetworkAccountDetailsPageModule) },
  { path: 'user-activities', loadChildren: () => import('./user-activities/user-activities.module').then(m => m.UserActivitiesPageModule) },
  { path: 'certificates-page', loadChildren: () => import('./certificates-page/certificates-page.module').then( m => m.CertificatesPagePageModule) },  {
    path: 'kyc-intermediary',
    loadChildren: () => import('./kyc/kyc-intermediary/kyc-intermediary.module').then( m => m.KycIntermediaryPageModule)
  },
  {
    path: 'connected-accounts',
    loadChildren: () => import('./connected-accounts/connected-accounts.module').then( m => m.ConnectedAccountsPageModule)
  },

  // { path: 'hcaptcha', loadChildren: () => import('./marketplace/hcaptcha/hcaptcha.module').then( m => m.HcaptchaPageModule) },
  // { path: 'fortune', loadChildren: () => import('./marketplace/fortune/fortune.module').then( m => m.FortunePageModule) }









];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
