import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { SecureStorageService } from './core/providers/secure-storage/secure-storage.service';
import { TranslateProviderService } from './core/providers/translate/translate-provider.service';
import { filter } from 'rxjs/operators';
import { SecureStorageError } from './core/providers/secure-storage/secure-storage-error.interface';
import { noop } from 'lodash';
import { AlertsProviderService } from './core/providers/alerts/alerts-provider.service';
import { Platform } from '@ionic/angular';
import { UtilityService } from './core/providers/utillity/utility.service';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';
import { App } from '@capacitor/app';

let cordova: any;

@Injectable()
export class AppInitializerService implements OnDestroy {

    private destroy$: Subject<void> = new Subject<void>();

    constructor(
        private secureStorageService: SecureStorageService,
        private translateProviderService: TranslateProviderService,
        private alertsProviderService: AlertsProviderService,
        private platform: Platform,
        private openNativeSettings: OpenNativeSettings
    ) {
    }

    public initSecureStorage(): void {
        // TODO if you want secure storage fallback uncomment this
        // this.secureStorageService.localDataSecured$.asObservable()
        //     .pipe(takeUntil(this.destroy$))
        //     .subscribe(async () => {
        //         await this.alertsProviderService.alertCreate(
        //             await this.translateProviderService.translateGet('GENERAL.success'),
        //             undefined,
        //             await this.translateProviderService.translateGet('SECURE_STORAGE.data-secured-message'),
        //             [await this.translateProviderService.translateGet('GENERAL.ok')]
        //         );
        //     });
        //
        // this.secureStorageService.errorWhileSecuringLocalData$.asObservable()
        //     .pipe(takeUntil(this.destroy$))
        //     .subscribe(async () => {
        //         await this.alertsProviderService.alertCreate(
        //             await this.translateProviderService.translateGet('GENERAL.error'),
        //             undefined,
        //             await this.translateProviderService.translateGet('SECURE_STORAGE.error-while-data-secure-message'),
        //             [await this.translateProviderService.translateGet('GENERAL.ok')]
        //         );
        //     });
        this.translateProviderService.localStorageLangSet$
            .pipe(filter(isSet => !!isSet)).subscribe(async _ => {
                // Creation of the secure storage
                this.secureStorageService.create()
                    .then(noop, async (error: SecureStorageError) => {
                        if ( this.platform.is('android') ) {
                            // TODO uncomment this for fallback logic in secure storage
                            // await this.alertsProviderService.alertCreate(
                            //     await this.translateProviderService.translateGet('SECURE_STORAGE.fallback-title-android'),
                            //     undefined,
                            //     await this.translateProviderService.translateGet('SECURE_STORAGE.fallback-message-android'),
                            //     [await this.translateProviderService.translateGet('GENERAL.ok')]
                            // );
                            // TODO And comment this
                            await this.alertsProviderService.alertCreate(
                                await this.translateProviderService.translateGet('SECURE_STORAGE.secure-storage-use-not-allowed-title'),
                                undefined,
                                await this.translateProviderService.translateGet('SECURE_STORAGE.secure-storage-use-not-allowed-message'),
                                [{
                                    text: await this.translateProviderService.translateGet('GENERAL.ok'),
                                    handler: () => {
                                        this.openNativeSettings.open('security').then(_ => {
                                                App.exitApp();
                                        });
                                    }
                                }]
                            ).then(_ => {
                                UtilityService.setTimeout(20000).subscribe(_ => {
                                    this.openNativeSettings.open('security').then(_ => {
                                            App.exitApp();
                                    });
                                });
                            });
                        }
                    });
                });
    }

    /**
     * NG2 hook for destroyment of the service
     */
    public ngOnDestroy() {
        this.destroy$.next();
    }
}
