# Development Setup on macOS

1. Install xcode command line tools: `xcode-select --install`
2. Install brew: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
3. Change ownership of pkgconfig: `sudo chown -R $(whoami) /usr/local/lib/pkgconfig`
4. Install cocoapods: `sudo gem install cocoapods`
5. Install [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
6. Install gradle: `brew install gradle`
7. Install [Android Studio](https://developer.android.com/studio)
8. Open Android Studio and install the latest Android SDK
9. In *~/.bashrc* OR *~/.zshrc* (whatever) files, add the following, and the run `source ~/.bashrc` OR `source ~/.zshrc`:
```
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_311.jdk/Contents/Home
export ANDROID_SDK_ROOT=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_SDK_ROOT/tools/bin
export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools
export PATH=$PATH:$ANDROID_SDK_ROOT/emulator
export PATH=$PATH:$JAVA_HOME/bin
```
10. It is advised that you run the following for a smooth Android build process:
```
cd ~/Library/Android/sdk/build-tools/31.0.0 \
  && mv d8 dx \
  && cd lib  \
  && mv d8.jar dx.jar
```
11. Install [node 18.10.0](https://nodejs.org/download/release/v18.10.0/)
12. Install [nvm](https://github.com/nvm-sh/nvm)
13. Install yarn, ionic and cordova: `npm i -g yarn && yarn install -g @ionic/cli cordova`
14. Clone the project and cd into the project:
```
git clone https://gitlab.com/jobgrader/jobgrader-mobile-app
cd jobgrader-mobile-app
```
15.  Change the ownership of the subfolders: `sudo chown -R $(whoami) .`
16.  Run the bash script: `chmod +x build.sh && ./build.sh`
17.  Run on browser: `ionic serve`
18.  Add platforms: `chmod +x add_platforms.sh && ./add_platforms.sh`
19.  Build iOS project: `ionic cordova build ios --prod`
20.  Build Android project: `ionic cordova build android --prod`


# Sentry

## Login
Run the login command and generate a new token
```
npx sentry-cli login
```
or pass a known token
```
npx sentry-cli login --auth-token .......
```

## Upload
After building your production build for release run the sentry upload script
```
npm run sentry:upload
```

## HOW TO RELEASE A NEW VERSION
1. change in `package.json` the version, like `1.0.0-xx`
2. change (if wanted/needed) in `config.xml` properties `android-versionCode` / `ios-CFBundleVersion`
3. build your app like `ionic cordova build android --prod`
4. `npm run sentry:upload`
5. `npm run changelog`
6. have a look to the `CHANGELOG.md` if everything is good or change it manually if needed
7. add `package.json`, `config.xml` and `CHANGELOG.md` to git (`git add .....` or from `VSCode`)
8. `npm run git:rel`
9. `npm run git:tag`
